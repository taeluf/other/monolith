# This was the example from :help NERDTreeKeymapAPI
call NERDTreeAddKeyMap({
       \ 'key': 'foo',
       \ 'callback': 'NERDTreeEchoPathHandler',
       \ 'quickhelpText': 'echo full path of current node',
       \ 'scope': 'DirNode' })

function! NERDTreeEchoPathHandler(dirnode)
    echo a:dirnode.path.str()
endfunction
