I wanted to add an action to nerdtree. just a single key to do a simple thing. idr what that was.

See: 
- :help NERDTreeKeymapAPI
- https://github.com/preservim/nerdtree


I would still need to put a symlink in ~/.vim/nerdtree_plugin/mymapping.vim
