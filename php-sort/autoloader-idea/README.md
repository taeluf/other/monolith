# PHP-Autoloader
Simple autoloader. Slightly more flexible & less verbose than PSR-4

# Composer Installation
    "require": { "rof/autoloader": "dev-master" }

# Manual Installation
Copy Autoloader.php to wherever you want and `require_once` it in any script before you need it to function.   

# Usage

Example: 
```php
    // \ROF\Autoloader::addDir($absoluteDirPath, $namespaceOne, $namespaceTwo)
    \ROF\Autoloader::addDir('~/project/class','Person', 'Store');
```

The call above will enable auto-loading of all the classes in the example project below
```
project 
    - class/
        - Pet/Cat.php
            - namespace Pet, class Cat
        - Owner.php
            - namespace Person, class Owner
        - Groomer.php
            - namespace Person, class Groomer
        - ToyStore.php
            - namespace Store, class ToyStore
    - view/
    - so-on/
```

# Ideas
- Add auto-namespace & class detection, so you only have to add the directory
- Add namespace file in the class directory (instead of passing through the function call)
- auto-call a static __initialize() function after loading the class
- Cache results - once the class is found, log it's absolute path to a file (along with all the other classes), to use for the next execution of autoloader.
- create an autoload.php file in the target project in order to remove this dependency for release
- Read the project's composer.json file for PSR-4 autoloading