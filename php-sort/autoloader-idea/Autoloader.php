<?php

namespace ROF;

class Autoloader
{

    static protected $isRegistered = false;
    static protected $dirs = [];

    static public function addDir($dir,...$baseNamespaces){
        if (!static::$isRegistered){
            static::$isRegistered = true;
            spl_autoload_register([get_class(),'loadClass']);
        }
        static::$dirs[$dir] = $baseNamespaces;
    }

    static public function loadClass($class){
        foreach (static::$dirs as $dir=>$namespaces){
            $namespaces[] = '';
            foreach ($namespaces as $ns){
                if ($ns!=''&&strpos($class,$ns)!==0)continue;
                $classPath = str_replace('\\','/',substr($class,strlen($ns))).'.php';
                if (file_exists($file = $dir.'/'.$classPath)){
                    require_once($file);
                } else {
                    //DEBUG
                    // echo $file."\n";
                }
            }
        }
    }
    
    // /**
    //  * A bad, old implementation... don't use it
    //  *
    //  * @param  mixed $baseDirectory
    //  * @param  mixed $staticInitializer
    //  * @param  mixed $throw
    //  * @return void
    //  */
    // public static function enable($baseDirectory = null, $staticInitializer = true, $throw = true)
    // {
        // if ($baseDirectory == null) {
            // $baseDirectory = $_SERVER['DOCUMENT_ROOT'] . '/Class';
        // }
        // spl_autoload_register(
            // function ($class) use ($baseDirectory, $staticInitializer, $throw) {
                // if (strpos($class, '\\') !== false) {
                    // $class = str_replace('\\', '/', $class);
                // }
                // $classFile = $baseDirectory . '/' . $class . '.php';
                // if (file_exists($classFile)) {
                    // include $classFile;
                    // if ($staticInitializer == true && method_exists($class, '__initialize')) {
                        // call_user_func(array($class, '__initialize'));
                    // }
                // } else {
                    // $message = "Error auto-loading class '" . $class . "' from file '{$classFile}' with base directory '{$baseDirectory}'";
                    // if ($throw) {
                        // throw new \Exception($message);
                    // } else {
                        // error_log($message);
                    // }
                // }
            // }
        // );

    // }

}
