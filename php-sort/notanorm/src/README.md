# NotAnOrm
Basically doing my best to abstract away the php code & simple mysql, but not make a whole ORM. Simple, small, neat.

I like this a lot. But I think a better approach would be to add these features to my redbean wrapper.

I'm not really doing anything new here. I'm just... writing the api in a way that I like. I want it to be intuitive so I don't have to look up docs. Intuitive API naming is really nice. But... maybe its only intuitive to me!
