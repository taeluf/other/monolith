# Site Starter
A minimal Liaison app, plus a setup script, to help you put up a Liaison powered website in no time.

## Create a new site
0. Go to a download folder: 
    - `cd ~/; mkdir .gitclone; cd .gitclone;`
1. Download the website starter: 
    -`git clone https://gitlab.com/taeluf/php/liaison-app/starter.git`
2. add setup script to your PATH: 
    - `echo "alias liasetup=\"php $(pwd)/starter/liasetup\"" >> ~/.bashrc`
3. (optional)Create copy of `code/apache/default-secret.json` at `code/apache/secret.json` & modify to have global settings for new sites on your system.
3. Go to your website folder: `cd /path/to/website/files`
4. Setup site: `liasetup`
5. Edit `secret.json` in your site directory to point to the correct autoload path
