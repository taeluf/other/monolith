<?php

require(__DIR__.'/Env.php');

$forceShowErrors = false;
$forceDisableErrors = false;

$env = new Env();
//show errors
$env->is(['host.local',$forceShowErrors],[!$forceDisableErrors])->showErrors();
// load secrets
$env->load(__DIR__.'/secret.json');
// require systemwide autoloader for local
$env->is(['host.local'])->require($env->get('autoload.local'));
// require composer autoloader for production
$env->isnt(['host.local'])->require(__DIR__.'/vendor/autoload.php');
// require(__DIR__.'/vendor/autoload.php'); // in case you just always want to use the composer autoload

// setup database if you're using Redbean with Taeluf's RDB extension: tluf.me/php-rdb
// RDB::setup('mysql:host='.$env->get('mysql.host').';dbname='.$env->get('mysql.dbname'),
    // $env->get('mysql.user'), $env->get('mysql.password')
// );

$lia = new \Liaison();
$lia->set('views.conflict', 'overwrite');
$starter = new \Lia\Package($lia, $env->get('Starter.dir'), ['name'=>'Starter']);

$site = new \Lia\Package($lia, __DIR__.'/Site/', ['name'=>'MySite']);


if ($env->is(['host.local'])->asBool()){
  // things to do only on localhost
}

// set the dir for cached files
$lia->set('lia.cacheDir', $site->dir('cache'));
// disable cache usage if we're on localhost
$lia->set('lia.resource.useCache',$env->isnt(['host.local'])->asBool());
$lia->set('lia.resource.forceRecompile',$env->is(['host.local'])->asBool());

$lia->deliver();
exit;
