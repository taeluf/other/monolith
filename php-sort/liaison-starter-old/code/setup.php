<?php

// print_r(get_defined_vars());

$dir = $_SERVER['PWD'];

echo "Dir: ${dir}";
echo "\nStart Liaison site here? (y/no)";
$input = rtrim(fgets(STDIN));
echo "\n";
if ($input!='y'){
  echo "Guess not!\n\n";
  exit;
}

if (file_exists($hta=$dir.'/.htaccess')){
  rename($hta, $dir.'/.htaccess.bak');
}

if (file_exists($deliver=$dir.'/deliver.php')){
  rename($deliver, $dir.'/deliver.php.bak');
}

copy(__DIR__.'/apache/.htaccess', $hta);
copy(__DIR__.'/apache/deliver.php', $deliver);
copy(__DIR__.'/apache/Env.php', $dir.'/Env.php');

mkdir($dir.'/Site', 0775);
mkdir($dir.'/Site/cache', 0775);
mkdir($dir.'/Site/public', 0775);
copy(__DIR__.'/apache/public/index.php', $dir.'/Site/public/index.php');

$secretFile = __DIR__.'/apache/secret.json';
if (!file_exists($secretFile))$secretFile = dirname($secretFile).'/default-secret.json';
$secrets = json_decode(file_get_contents(__DIR__.'/apache/secret.json'), true);
$secrets['Starter.dir'] = __DIR__;

file_put_contents($dir.'/secret.json', json_encode($secrets, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));


echo "\nFiles copied successfully! Now, you may need to setup your apache virtualhost file.\n\n";
