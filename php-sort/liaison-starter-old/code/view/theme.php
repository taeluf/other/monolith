<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?=$lia->getHeadHtml()?>
</head>
<body>
    <?=$lia->view('theme/Header');?>
    <main>
        <?=$content?>
    </main>
    <?=$lia->view('theme/Footer');?>
</body>
</html>
