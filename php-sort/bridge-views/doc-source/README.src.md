# Fresh Component: A PHP View Library
A PHP View library for automatically connecting database content to HTML through markup. Supports inline PHP that auto-exposes the object as a variable. Supports auto-filling of forms as well as any other HTML. It is intended for extension by frameworks & libraries, but you can use it on an individual site as well.

There is still a lot of work to do to fully and nicely document this library, but I've tried to cover things pretty well. [`Liaison Fresh Package`](https://github.com/Taeluf/Liaison-FreshCompo) implements... basically everything, but it's kind of a mess.

## Example
[**View the full example**](0-doc/0-example.md)

A Fresh View:
```html
<div rb-table="note" rb-find="id:<?php echo $id?>">
    <h1 rb-prop="name"></h1>
    <p rb-prop="body" rb-format="markdown"></p>
    <p>My note's name is <?=$note->name?>. See, inline PHP works too!</p>
</div>
```
The PHP to display it:
```php
$note = new \Fresh\Component($componentDir);
$view = $note->view('Note',['id'=>1]);
echo $view;
```
yields
```html
<div>
    <h1>The Note's Name</h1>
    <p>The note's body, formatted with CommonMark</p>
    <p>My note's name is The Note's Name. See, inline PHP works too!</p>
</div>
```

## Install
`composer require taeluf/fresh-compo`
```json
{
    "name":"vendor/a-cool-package",
    "require":{
        "taeluf/fresh-compo": "dev-version_1"
    }
}
```
This will install dependencies [RBDoc](https://github.com/Taeluf/RBDoc) and [CommonMark](https://github.com/thephpleague/commonmark)

## Integrations
- [`Liaison Fresh Package`](https://github.com/Taeluf/Liaison-FreshCompo) integrates this library into [`Liaison`](https://github.com/Taeluf/Liaison) for improved handling of views.

## Attributes
We handle a few attributes, but you can extend it to handle more. [`Liaison Fresh Package`](https://github.com/Taeluf/Liaison-FreshCompo) is a thorough example.

### Entity Attributes
To declare an entity to be autoloaded from the database into an HTML view, you must declare these attributes on the html node.

#### rb-table
The name of the table to load from

#### rb-find 
A simple list of `key:value;key2:value2;` items which will (generally) be parsed into a key=>value array and used to lookup the items with your custom find. You'll add a `find` handler to handle lookups, so I guess you can change the format.


### Property Attributes
These apply to elements which will hold a property value

#### rb-prop
The name of the property on the entity. It will be loaded by your `find` handler.  
Most elements will have their innerHTML set to the loaded object's property by the same name. `<a>` has its `href` set. `<img>` it's `src`. 

#### rb-format
The name of the formatter to use. Currently `markdown` is built in, thanks to [CommonMark](https://github.com/thephpleague/commonmark)

#### tf-format
Declare `tf-format="markdown"` on ANY node containing markdown, to convert to HTML. You can implement your own formatter too.

## find handler, submit handler

@import(Feature.submission)

@import(Feature.uploadFile)

@import(Feature.setup)

@import(Feature.compile)

@import(Feature.viewQueries)

@import(Extend.runtimeHandler)

@import(Extend.compileHandler)