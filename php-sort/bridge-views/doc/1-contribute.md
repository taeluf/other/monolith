# Contributing
I honestly don't know how to manage pull requests & stuff, soooo.... Feel free to contribute & I'll do my best to learn and integrate things. I'll try to provide details here about the main parts so you know what to work on.

## Docs
Docs are housed in dir `8-build/docsource`, then you must run `php 8-build/build.php` to create the documentation. I use [`PHP Documentor`](https://github.com/Taeluf/PHP-Documentor) for this.

## Component
\Fresh\Component is the base class. It holds the views, a handler, and other settings that need to be global. It also processes form submissions before passing to your `submit` handler.
