<?php
array (
  'Contribute.Component' => 
  array (
    'comment' => '## Component
\\Fresh\\Component is the base class. It holds the views, a handler, and other settings that need to be global. It also processes form submissions before passing to your `submit` handler.',
    'key' => 'Contribute.Component',
    'exportline' => '@export(Contribute.Component)',
  ),
  'Feature.uploadFile' => 
  array (
    'comment' => '## Uploading files
To enable automatic file uploads, call `$component->setUpload($storageDir, $urlPrefix)`. You are responsible for routing that file.  
The `$urlPrefix` is used to prepend the file-url when passing formdata to your `submit` handler',
    'key' => 'Feature.uploadFile',
    'exportline' => '@export(Feature.uploadFile)',
  ),
  'Feature.submission' => 
  array (
    'comment' => '## Submitting data sort of automaticallyish
You must still set a `submit` handler like so:
```php
$myDbObj = get_my_db_instance();
$compo->setSubmitHandler(
    function (string $tableName, array $dataToSave, $itemId) use ($myDbObj){
        $myDbObj->save($tableName,$dataToSave,$itemId);
    }
);
```',
    'key' => 'Feature.submission',
    'exportline' => '@export(Feature.submission)',
  ),
  'Feature.setup' => 
  array (
    'comment' => '## Routing, Setting things up
Use the setup step to do routing, basically. This requires you to append setup code to views. Here\'s a simple (bad) example:
```php
$phpCode = \'<?php if ($url==\'/\')echo $this; ?>`
/`$this` refers to the view which is being setup.
$view = $compo->view("Page_Home"); //this points to $compoDir/view/Page_HomeView.php
$view->appendSetupCode($phpCode);
```
This setup code must be appended pre-compile or inside a compile handler.

Then to execute your setup:
```php
$compo = new \\Fresh\\Component($name,$componentDir);
$forceRecompilation = true;// good when developing. Bad for production.
$compo->compile($forceRecompilation); //  Defaults to false, if you don\'t pass a paramater
$passthru = [\'url\'=> cleanMyUrl( $_SERVER[\'REQUEST_URI\'] ) ];
$compo->setup($passthru);
```
All your `setup` code will be run, in no particular order, and your `Page_Home` view will be displayed when `yoursite.atld/` is requested',
    'key' => 'Feature.setup',
    'exportline' => '@export(Feature.setup)',
  ),
  'Feature.compile' => 
  array (
    'comment' => '## Compiling
Call `$compo->compile(true)` to force re-compilation of all views & forms. Call `$compo->compile()` to use the default `$forceRecompile` value of `false` to only compile views that have changed.  
You should use `true` in development probably.  
I\'ve had troubles with conditional recompiling (passing `false`) on my localhost, and I think it was because of file permissions issues, but I\'m not sure.

And you can set a host of your own `compile` handlers.',
    'key' => 'Feature.compile',
    'exportline' => '@export(Feature.compile)',
  ),
  'Extend.runtimeHandler' => 
  array (
    'comment' => '## Runtime Handlers
Use `$compo->setRuntimeHandler(...)` or `$compo->addRuntimeHandler($handlerName,$function)`. What the handler receives varies greatly.  
`setRuntimeHandler` will only allow one handler per `$name` & the callback\'s value will be returned when the runtime handler is called.
`addRuntimeHandler` will allow multiple handlers. When the named handler is called, each callback\'s value will be stored in an array & that array will be returned.

### Noteable runtime handlers:
- `find`: `function($table, $findProperty){return $anArrayOfObjects};`
   - This MUST be declared to use the entity features
- `format_formatterName`: `function($value){return convertToMyFormat($value);}`
   - `$compo->setRuntimeHandler(\'runtime\',\'format_\'.$formatterName,$callback);`
   - or `$compo->view($view)->addFormatter($name,$callback)`
- `addResources`: `function($viewObj, $resourcesList){...use your framework or whatever...}',
    'key' => 'Extend.runtimeHandler',
    'exportline' => '@export(Extend.runtimeHandler)',
  ),
  'Extend.compileHandler' => 
  array (
    'comment' => '## Compile Handlers
These will be run during the compile step.

NEED TO REDO THIS DOC FOR COMPILE HANDLERS

Use `$compo->addCompileHandler($handlerName,$function)`. What the handler receives varies greatly.  

### Noteable Compile handlers: (name: Callable example)
- `preDoc`: `function($cleanSource,$viewContent,$compiler,$view){ return str_replace(\'<bad-tag\', \'<good-tag\',$cleanSource); }`
   - This runs before the RBDoc (DOMDocument) instance is created
   - Generally, you should return a modified version of the $cleanSource
   - $cleanSource is the view\'s source code with the PHP extracted by $compiler
   - $viewContent is the view\'s source code, with inline PHP still in tact
   - $compiler is an instance of \\RBDoc\\Compiler, which extracted the php
   - $view is an instance of the view object
- `form`: `function($formViewObj,$doc,$compiler,$formNode){$formNode->setAttribute(\'action\',\'/some/url/\');}`
   - Return values are discarded, but you can modify the objects.
   - I\'m not sure, but some things might be overwritten by this library, such as the `action` or `method` attributes on the node.
   - $formViewObj is an instance of \\Fresh\\Form
   - $doc is \\RBDoc instance
   - $compiler is an \\RBDoc\\Compiler
   - $formNode is an \\RBDoc\\Node (DOMElement) instance, who\'s tag is `<form ...>`',
    'key' => 'Extend.compileHandler',
    'exportline' => '@export(Extend.compileHandler)',
  ),
  'Feature.viewQueries' => 
  array (
    'comment' => '### View Queries: A compile handler
In [my Liaison Package](https://github.com/Taeluf/Liaison-FreshCompo), I use this for routing. Here\'s a simplified version.
```php
$compo->addViewQuery(\'//lia-route\', //runs on every `<lia-route` tag
    function($rbDoc, $view, $compiler, $node){
        $route = $node->getAttribute(\'pattern\');
        $escRoute = var_export($route, true); // because `$route` is only available during this compile step. 
        $phpCode = \'<?php if ($url==$escRoute)echo $this; ?>`
        $view->appendSetupCode($phpCode);
    }
);
```
This function will be run during the `compile` step for each `<lia-route` tag your view contains. No errors if not found.  
It will append setup code that does routing.  
Then when you call `$compo->setup[\'url\'=>$theUrl]`, the setup code will run and your view will be displayed if the route matches the url.',
    'key' => 'Feature.viewQueries',
    'exportline' => '@export(Feature.viewQueries)',
  ),
)
?>