# AutoSchema
Integrates databasing with HTML Schema-esque notation





Goal:
Take the current version of FormTools & the Schema Edit features and turn it into a singular package.
Clean up the code, so I... have some idea of what's happening.

Steps:
- Find all the code that makes this feature work
- Put it into the same directory
- Move files around as needed, in order to package it as a Compo
- Use the newly constructed LiaComponent to edit the Love Decatur home page
- use it to edit the text on the sign-up page (requires modification to html)

CONTINUE FROM HERE!!!!
- Verify that HTML output is correct (no fragments from the lib) for home page and /sign-up/
- Create a git repo with this new Component
- Set up DecaturForJustice to use Composer
- Include the new Component through Composer
- Start using the new component on the site
- Figure out how to make it more robust and accessible