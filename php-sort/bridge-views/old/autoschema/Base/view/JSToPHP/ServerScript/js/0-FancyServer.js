
class FancyServer extends RB.Autowire {

    __attach(){
        this.func = function(){
                alert('with timeout');
            }
    }
    showSpinnerWheelDelayed(ms){
        setTimeout(this.func,ms);
    }
    cancelSpinnerWheel(){
        clearTimeout(this.func);
    }
    async fetch(method,...args){

        const uploadMethod = "POST";
        const url = '/call_api/';
        const params = {
            "method": method,
            "class":this.class ?? this.constructor.name
        };
        for (const index in args){
            params[index] = args[index];
        }
        // console.log(args);
        // console.log(params);
        var formData = new FormData();
        for(var key in params){
            const param = params[key];
            if (param!=null
                &&typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,params[key]);
        }
        const submitData = {method: uploadMethod, mode: "cors"};
        if (uploadMethod=='POST')submitData['body'] = formData;

        const res = await fetch(url, submitData);
        const json = await res.json();
        console.log(json);
        return json.methods[method];
    }
}