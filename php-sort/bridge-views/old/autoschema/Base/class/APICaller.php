<?php

class APICaller {

    protected $args;
    protected $class;
    protected $function;

    public function __construct($data){
        $method = $data['method'];
        unset($data['method']);
        $class = $data['class'];
        unset($data['class']);
        $args = [];
        foreach ($data as $key=>$arg){
            if (!is_numeric($key))throw new \Exception("Only numeric keys");
            $args[$key] = $arg;
        }
        ksort($args);

        $this->class = $class;
        $this->function = $method;
        $this->args = $args;
    }
    protected function rawResult($event){
        $class = $this->class;
        $func = $this->function;
        $args = $this->args;
        // $args[] = $event;
        $result = $class::$func($event,...$args);
        return $result;
    }
    public function resolve($event){
        $result = $this->rawResult($event);

        $ret = [
            'methods'=>[
                $this->function=>$result
            ]
        ];
        
        return json_encode($ret);
    }
}