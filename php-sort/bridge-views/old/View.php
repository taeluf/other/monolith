<?php

namespace Fresh;

class View extends ViewRoot {

    public function initialize(){
        $this->addQuery('//*[@rb-table]',[$this,'handleEntity']);
        $this->addCompileHandler('entity-prop',[$this,'handleEntityProp']);
        $this->setRuntimeHandler('formatObjectValue',[$this,'formatObjectValue']);
        $this->addRuntimeHandler('find',[$this,'find']);
        $this->addFormatter('markdown',[$this,'mdToHtml']);
    }

    public function getEntities(){
        $viewContent = file_get_contents($this->file);
        // echo $viewContent."\n\n";
        $compiler = new \RBCompiler();
        $cleanSource = $compiler->cleanSource($viewContent);
        $doc = new \RBDoc($cleanSource);

        $entities = $doc->xpath('//*[@rb-table]');
        
        return $entities;
    }

    protected function mdToHtml($markdown){
        return 'MD::::'.$markdown.'::::DM';
    }

    protected function formatObjectValue($object,$param,$formatter){

        // $handler = $this->runtimeHandlers['format_'.$formatter] ?? null;
        $value = $object->$param;
        $method = 'format_'.$formatter;
        $ret = $this->callHandler($method,$value);
        var_dump($ret);
        exit;
        return $this->$method($value);
        // $this->format
        // if (is_callable($handler))$value = $handler($value);
        // return $value;
    }
    protected function handleEntityProp($compiler,$propNode,$entityInfo){

        $varName = $entityInfo['table'];

        $propName = $propNode->getAttribute('rb-prop');
        $format = $propNode->getAttribute('rb-format');

        $propNode->removeAttribute('rb-prop');
        $propNode->removeAttribute('rb-format');
        

        $placeholder = $compiler->placeholderFor('<?php echo $this->formatObjectValue($'.$varName.',\''.$propName.'\',\''.$format.'\'); ?>');
        $propNode->textContent = $placeholder;
    }

    protected function handleEntity(\RBDoc $doc, \View $view, \RBCompiler $compiler, \RBNode $entity){

        //process the entity
        // create a lookup
        // replace the entity node with a code placeholder
        // Set the replacement text to the fully-processed version of the entity
        // but use an array, which will later be imploded
        // this way, objects can be in the array & not be __toString'd until everything is complete

        //
        // $entity->outerHTML = "a_code_placeholder";
        // $compiler->php['a_code_placeholder'] =
            // [
                // $prependedPHPCode,
                // $lookupCode,
                // $entity, // this will __toString at the last possible moment
                // $closeTheLoopCode,
                // $appendedPHPCode
            // ];
        // $viewHtml = $entity;
        // echo 'zee';exit;
        $placeholder = $compiler->placeholderFor($entity);

        $entity->parentNode->replaceChild($doc->createTextNode($placeholder),$entity);
        

        $rbAttrs = [];
        foreach ($entity->attributes() as $attr){
            if (strtolower(substr($attr->name,0,3))=='rb-'){
                $entity->removeAttributeNode($attr);
                $rbAttrs[substr($attr->name,3)] = $doc->fillWithPHP($attr->value);
            }
        }
        $varName = $rbAttrs['table'];
        $xp = new \DOMXpath($doc);
        $props = $xp->query(".//*[@rb-prop]",$entity);
        foreach ($props as $propNode){
            $this->executeCompileHandlers('entity-prop',$compiler,$propNode,$rbAttrs);
        }

        $phpOutput = '<?php ';
        $query = [];
        foreach ($rbAttrs as $var=>$value){
            $line = '';
            $line = 'ob_start();?>'.$value.'<?php $'.$var.' = ob_get_clean();'."\n";
            $phpOutput .= $line;

            if ($var=='table'){
                $query = "\$list = \$this->callHandler('find',\$table,\$find);";
                $queryParam = $value;
            }
        }
        $phpOutput .= ' ?>';
        $phpOutput .= "\n<?php\n".$query."\n?>\n";
        $phpOutput .= "<?php\nforeach(\$list as \${$queryParam}) { \n?>";
        // $phpOutput .= $doc->output();
        $compiler->placeholderPrepend($placeholder,$phpOutput);
        $compiler->placeholderAppend($placeholder,
                "<?php \n} ?>\n"
            );

    }
}