# PHP-Views
A... relatively simple, framework-agnostic, library to compile multiple views together into one.

# NOTE Oct 14, 2019
This needs to be updated with  more simplicity. It's not currently in a great state. It works. But basically, it's crap. Hopefully it will be cool soon. Might just let it go and use TWIG or something. The README sucks too, from the looks of it.

# WARNING
There is currently an error which prevents this lib from working, unless you use the RequestHandler implementation.

## Issues
* the script searches for `view` tags with properties = `type="output"` AND `name=Head`. The `name` part should be removed.
* The instructions are clunky
* It currently assumes that the `Public` directory is being used, for the compiled css & javascript files
* Does not delete old JS & CSS files
* Prints out ugly HTML
* Slow, clunky. Creates multiple instances of `simple_html_dom`
* Code is sloppy (but functional)

## Supported Output Properties
These will be declared in a `<view` tag.
* `title` - ex: `title="The Greatest Article` - this will output a `<meta property="og:title"` and a `<title>` tag
* `description` - ex: `description="It is the greatest ever written"` - this will output a `<meta property="og:description" name="description"` tag
* `image` - ex: `image="/images/the-greatest.jpg"` will output a `<meta property="og:image"` tag with the FULL URL of the image on your server. You can provide a full URL, a path relative to the domain (starts with `/`) or a path relative to the directory (does not start with `/`)
* `ogtype` - ex: `ogtype="article"` will output a `<meta property="og:type"` tag. Couldn't use `type` because that has another function already.
* `fb_app_id` - ex: `fb_app_id="SOME_LONG_NUMBER"` will output a `<meta property="fb:app_id"` tag.

## Goals
* Add option to specify the `compile` directory
* Add instructions for specifying meta-information, such as title, keywords, and ??
* Improve the instructions, like a million times over. They currently seem tough to follow

## Installing
### With Composer
    "require": { "rof/view" : "1.*" }
### Manually
1. Consider an [Autoloader](https://github.com/ReedOverflow/PHP-Autoloader)
2. Copy `Controller.php` from `View` to wherever you'd like it on your server. 

## Usage
2. Choose your Document Root (likely the root path of your website)
3. Create a View
4. Process the view (implementations below)

### ReedOverflow's Request Handler Implementation (recommended)
Use [RequestHandler](https://github.com/ReedOverflow/PHP-Request-Handler) with the custom view controller implementation.  
This way you just create a View at the requested script and you don't have to write extra processing code.

### Single Script Implementation
This way, you will create a singular script that handles the entire process.

    <?php
    ob_start();
    ?>
    VIEW CONTENT GOES IN HERE (see below)
    <?
    $content = ob_get_clean(); //shove the content into a variable so it can be processed
    $documentRoot = $_SERVER['DOCUMENT_ROOT']
    $controller = new \ROF\View\Controller($documentRoot);
    $controller->load($content);
    $controller->display();

### With separate Views directory
This way, you will store Views in a file separate from the script that's being requested. 
Create a folder in your document root called `View`. The name `"ViewName"` correlates to `View/ViewName.php`

    <?php
    $documentRoot = $_SERVER['DOCUMENT_ROOT'];
    $controller = new \ROF\View\Controller($documentRoot);
    $controller->displayView("ViewName");
    ?>
    
You can create views in subdirectories and call `$controller->displayView("Sub/Directory/ViewName");`

## Create a View
1. There are 5 different `type`s of views. They include:
   * `<view type="self"></view>` which is purely used to specify css and javascript files
   * `<view type="include"></view>` which will include another view into a view being loaded
   * `<view type="parent">CONTENT</view>` which will include the `parent` view and place content into it
   * `<view type="child"></view>` which will be within a `parent` view. The `parent` view will paste the CONTENT, mentioned in the lasst type description, to this `child` view tag
   * `<view type="output"></view>` which is to go in the `<head>` tag of your page, to contain stylesheet and script links
2. There are 3 other properties
   * `name`, used only with `parent` type, like `<view type="parent" name="Page">CONTENT</view>`. The child view asking for the parent view specifies the name. The parent view specifies it's name by the file name in the `View` folder.
   * `stylesheets`, to specify css files this view relies upon. Ex: `<view type="self" stylesheets="Page.css,Fonts.css"></view>`
   * `scripts`, to specify javascript files this view relies upon. Ex: `<view type="self" stylesheets="FancyButtons.js,DropdownMenu.js"></view>`
3. Create a `Style` directory and a `Script` directory in your document root. This is where your css and javascript files will be located. They will get compiled into a single file, saved into a `compiled` directory, and then output onto the page as a singular `<link>` and `<script>` tag, respectively.
4. Create the view file according to the implementation you chose above. See "Example View" below.  
**Notes:** 
* You may specify stylesheets & scripts on any `view` tag, but it is generally recommended to place it on the `type="self"` `view` tag.
* View files in the `View` folder must have the `.php` extension or they will not be accessible.

## Including & nesting views
1. Create a `View` directory inside your document root
2. Create a View, like above.
3. Then: 
   * `<view type="include" name="VIEW_NAME"></view>` for a view that will be included from the `View` Directory, which will supply all of its own content.
   * `<view type="parent" name="PARENT_VIEW_NAME">CONTENT GOES IN HERE</view>` which will paste the content into it's `child` view tag


### Example Views
#### Whole Page:
Located at `View/Page.php`

    <view type="self" stylesheets="Page.css" scripts="Page.js">
    <html>
      <head>
        <view type="include" name="Head"></view>
        <view type="output"></view>
      </head>
      <body>
          <view type="include" name="Header"></view>
          <view type="include" name="Navigation"></view>
          <view type="include" name="Content"></view>
          <view type="include" name="Footer"></view>
      </body>
    </html>
      </view>
      
Then there will be several other views. The view with `name="head"` does not display. It will contain a `<meta>` tag and other tags you would place in `<head>`. If you can specify stylesheets and scripts in the outermost `<view>` tag, that is preferred.
Located at `View/Content.php`:

    <view type="self" stylesheets="Content.css">
    <div id="content_area">

    <view type="child"></view>

    </div>
    </view>
    
This is the content area. This is not a necessary extra layer, but it's my personal preference. That `type="child"` view tag will be replaced with the content from the requested script.  
Located at `Public/funny-monkey.php`

    <view type="parent" name="Page"> <!-- this way the whole page HTML will be included around it -->
      <h1>Funny Monkey</h1>
      <p>I'm a page all about this one really funny monkey that I met when I was at the zoo.</p>
      <img src="/pics/funny-monkey.jpg" alt="Funny Monkey" />
    </view>

So, what's happening is:
1. a user requests `mysite.com/funny-monkey.php`
2. my [Request Handler](https://github.com/ReedOverflow/PHP-Request-Handler)(optional) will load the script at `DOCUMENT_ROOT.'/Public/funny-monkey.php'.
3. This library parses the `<view>` tag and includes the script at `DOCUMENT_ROOT.'/View/Page.php'`
4. This library parses the subsequent `<view>` tags and includes the scripts inside `DOCUMENT_ROOT.'/View/'` named Header.php, Navigation.php, Content.php, and Footer.php
5. This library parses the subsequent `<view>` tags (inside `Content.php`). 
6. The `<view type="child"></view>` tag in `Content.php` is replaced with the contents of `funny-monkey.php`
7. The `<view>` tags are all replaced with the content from the relevant files.
8. All scripts and stylesheets from the many `<view>` tags will be compiled into a singular file
9. A `<script>` tag and `<link>` tags will be created, linking to the compiled-versions of the files.
