<?php

namespace Fresh;

class Form extends View {

    /**
     * The View object which this form implicitly references (because they have the same name)
     */
    public $refView;

    public function initialize(){
        $this->addQuery('//form',[$this,'handleForm']);
        $this->addCompileHandler('form-input',[$this,'handleFormInput']);
        // $this->addRuntimeHandler('find',[$this,'find']);
        // $this->addRuntimeHandler('formatObjectValue',[$this,'formatObjectValue']);
        // $this->addFormatter('markdown',[$this,'mdToHtml']);
    }

    public function setView($viewObj){
        $this->refView = $viewObj;
    }

    protected function handleFormInput($compiler,$inputNode,$table){

        $varName = $table;

        $propName = $inputNode->getAttribute('name');        

        $placeholder = $compiler->placeholderFor('<?=$'.$varName.'->'.$propName.'; ?>');
        $inputNode->setAttribute('value',$placeholder);

        if ($inputNode->type=='file'){
            $form = $inputNode->form;
            $form->enctype = "multipart/form-data";
        }
    }

    protected function handleForm(\RBDoc $doc, \FreshForm $form, \RBCompiler $compiler, \RBNode $node){

            //cleanse the form of php
                //load the view which this form relates to
            // get the list of entities from the view
            // get the first entity
            // get the table
            // set input values & innertexts to print the database value
            // output the compiled form

        // Add "smarter" value-setting for input types like select or textarea
        // Add enctype (if file input found),method,action to form

        // compile an id-based lookup for that table

        $placeholder = $compiler->placeholderFor($node);
        $node->parentNode->replaceChild($doc->createTextNode($placeholder),$node);
        $node->addHiddenInput('id','');

        $view = $this->refView;
        $entities = $view->getEntities();
        $entity = $entities[0] ?? null;
        if ($entity==null)throw new \Exception("We could not find a view for this form named '{$form->name}'");

        foreach ($this->compileHandlers['form'] as $handler){
            $handler($form,$doc,$compiler,$node);
        }
        // var_dump($routes);
        // exit;

        $table = $entity->getAttribute('rb-table');
        // $lookup = 'id:<?=$id? >';
        $varName = $table;


        $inputNodes = $doc->xpath('.//*[@name]',$node);
        foreach ($inputNodes as $input){
            $this->executeCompileHandlers('form-input',$compiler,$input,$table);
        }

        $node->action = $compiler->placeholderFor("<?=\$submit_url;?>");
        $node->method = 'POST';
        $node->addHiddenInput('fresh_table',$table);


        $escTable = var_export($table,true);
        $phpOpen = 
        <<<PHP

            <?php
                \$table = {$escTable};
                \$find = 'id:'.\$id.';';
                \$list = \$this->callHandler('find',\$table,\$find);
                foreach (\$list as \${$varName}){ ?>

        PHP;
        $compiler->placeholderPrepend($placeholder,$phpOpen);


        $phpClose = 
        <<<PHP

            <?php 
                }
            ?>

        PHP;
        $compiler->placeholderAppend($placeholder,$phpClose);
        
        
        return;

    }
    
    /**
     * Get an array of elements which are:
     *  1. child of a <form> tag
     *  2. Have a name attribute
     *  3. NOT a <form> tag itself
     *
     * @return an array of elements or an empty array if none found
     */
    public function getInputs(){
        $compiler = new \RBCompiler();
        $dirtySource = file_get_contents($this->file);
        $cleanSource = $compiler->cleanSource($dirtySource);
        $preDocHandlers = $this->compileHandlers['pre-doc'];
        foreach ($preDocHandlers as $handler){
            $cleanSource = $handler($cleanSource,$dirtySource,$compiler,$this);
        }

        $doc = new \RBDoc($cleanSource);
        $inputs = $doc->xpath('//form//*[@name]');
        $list=[];
        foreach ($inputs as $i)if (strtolower($i->tagName)!='form')$list[] = $i;
        return $list;
    }
}