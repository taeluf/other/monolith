<?php

require_once(dirname(__DIR__,3).'/autoload.php');

$libDir = dirname(__DIR__);
$docu = new \TLF\Docu($libDir, 'doc', 'doc-source', 'doc-source/README.src.md');
$docu->document();

$unusedFile = $libDir.'/0-doc/exports/unused-exports.md';
$unusedExports = file_get_contents($unusedFile);
file_put_contents($libDir.'/0-doc/2-Everything-Else.md',$unusedExports);