<?php



if ($message=$this->message()){
	echo '<div class="alert-message">'.$message->content.'</div>';
}

$this->lia->setTitle($article->title);
?>

<div class="article">
<?=$article->htmlBody; ?>
</div>
<div class="article-footer">

</div>
<?php


if ($article!=false
	&&$this->allows(...$article->locks('edit')))
{ ?>
	<a href="<?=$this->url('','edit',$article->slug)?>"><input type="button" value="Edit <?=$article->title?>"/></a>
	
<?php 
}