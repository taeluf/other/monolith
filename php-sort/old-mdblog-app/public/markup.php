<?php

$markdownText = $_POST['markdown_text'];
$method = $_POST['method'];

$parsedown = new \Parsedown();
$markdownHtml = $parsedown->text($markdownText);

if ($method=='json'){
  $data = array(
    "htmlText" => $markdownHtml
    );
  echo json_encode($data);
} else if ($method=="plaintext"||TRUE){

  echo $markdownHtml;
  
}
exit;

?>