function category_changed(select){
    let selected = (select.value || select.options[select.selectedIndex].value);
    if (selected=='_new_'){
        document.getElementById("new_category_box").style.display = "block";
    } else {
        document.getElementById("new_category_box").style.display = "none";
    }
    let catUrl = '/'+selected+'/';
    document.getElementById('article_category_url').innerText = catUrl;
}

function category_added(){
    let box = document.getElementById("new_category_box");
    let new_category = document.getElementById("new_category_input").value;
    let select = document.getElementById("category_selector");
    var newOption = document.createElement("option");
    newOption.value = new_category;
    newOption.text = new_category;
    select.add(newOption);
    select.value = new_category;
    category_changed(select);
    box.style.display = 'none';
}