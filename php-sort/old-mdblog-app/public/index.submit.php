<?php

// echo 
// return;
$_POST['category'] = strtolower($_POST['category'] ?? '');
$_POST['status'] = 'published';
$_POST['author_id'] = 0;
$_POST['url'] = $this->cleanUrl('/'.$_POST['category'].'/'.$_POST['slug'].'/');
$_POST['slug'] = \ROF\Article::cleanSlug($_POST['slug']);
// var_dump($_POST['url']);
// var_dump($_POST); 
// exit;
    $pdo = $this->pdo;//get('db-pdo');
    if (isset($_POST['id'])&&$_POST['id']!=NULL){
        // var_dump('update request');
        $update = $pdo->prepare("UPDATE rof_articles "
        ."SET title=:title, blurb=:blurb, body=:body, slug=:slug, category=:category, `status`=:stat, url=:url, author_id=:author_id, "
        ."updatedAt=:updatedAt "
        ."WHERE id=:id");
        $update->execute(
            array(
                ":title" => $_POST['title'],
                ":blurb" => $_POST['blurb'],
                ":body" => $_POST['article-body'],
                ":id" => $_POST['id'],
                ":slug" => $_POST['slug'],
                ":category" => $_POST['category'],
                ":stat" => $_POST['status'],
                ":url" => $_POST['url'],
                ":author_id" => $_POST['author_id'],
                ":updatedAt" => time(),
                )
            );
        $id = $_POST['id'];
        $url = $this->cleanUrl($_POST['url']);
    } else {
        $insert = $pdo->prepare("INSERT INTO rof_articles(title,blurb,body,slug,url,category,`status`,author_id,createdAt) "
        ."VALUES(:title,:blurb,:body,:slug,:url,:category,:stat,:author_id,:createdAt)");
        $insert->execute(
            array(
                ":title" => $_POST['title'],
                ":blurb" => $_POST['blurb'],
                ":body" => $_POST['article-body'],
                ":slug" => $_POST['slug'],
                ":category" => $_POST['category'],
                ":stat" => $_POST['status'],
                ":author_id" => $_POST['author_id'],
                ":url" => $_POST['url'],
                ":createdAt" => time(),
            )
        );
        $id = $pdo->lastInsertId();
    }

    $this->showAtUrl($_POST['url'],'/');



    ?>