<?php

namespace ROF\Article;

class App extends \Lia\App implements \LiaFace\App {


    static public function create($baseUrl='/article/'){
        $app = '';

        $dir = realpath(__DIR__.'/../');
        $app = ($baseUrl===NULL) ? new \ROF\Article\App($dir) : new \ROF\Article\App($dir,$baseUrl);

        return $app;
    }

    public function setPdo($pdo){
        $this->set('pdo',$pdo);
        \ROF\Article::setPdo($pdo);
    }
}