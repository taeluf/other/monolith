# PHP-Article
A small PHP library for writing Articles or Blogs using Markdown. Supports URL slugs. Pre-made views included.

# Note Oct 14, 2019
This needs to be updated to work with the newest version of Tiny. It also sucks, I'm pretty sure. Not ready for public consumption.

This library is new. It is not battle-tested. There is much left for wanting.

Current Features:
* Write blog articles with Markdown
* Includes URL Slug, Title, & summary of article
* Pre-made views
* MVC architecture, built with [Tiny-MVC](https://github.com/ReedOverflow/PHP-Tiny-MVC)

## Requirements
You must have a mysql database, PHP, and PDO.

## Install
Use composer. non-composer is not viable at this time.
```json
"require": {
        "rof/article": "0.1"
    }
```
It relies upon [Tiny-MVC](https://github.com/ReedOverflow/PHP-Tiny-MVC), which composer takes care of for you.

# Initial Setup
You will need to provide some initial configurations. 

```php
$pdo = new \PDO("mysql:host=mysql.yourhost.com;dbname=my_database","database_user",'database_password');
\ROF\Article\Config::getInstance()->initialize([
            "pdo"=>$pdo,
            "url"=>'/article', 
            "single_script" => '/home/whatever/public_files/article/index.php',                 
           ]);
```                
You will then need to create the `single_script` file which is listed above in the configurations. I know this sucks. I hope to improve it soon. 
```php

     $handler = \ROF\Article\Handler::getInstance();
     $view = $handler->getView();
     $headData = '';
     foreach($view->stylesheets as $url=>$path){
         $headData .= '<link ...'; //put it in the <head> tag of your page
     }
    foreach($view->scripts as $url=>$path){
        $headData .= '<script ...'; //put it in the <head> tag of your page
     }

    display_in_page($view,$headData);

?>
```
Then you will need to call the `single_script`:
```php
\ROF\Article\Handler::getInstance()->routeRequest();
```
And finally, you will need to create the table on your database. Throw this in a script (after configuring), call it once, and then get rid of it:
```php
\ROF\Article\Loader::getInstance()->createTable();
```

## Usage 
If your configurations are correct, then you should be able to go to yoursite.com/article (or whatever you configured) and see a list of articles.
Assuming you've configured `/article` as your url, the following URLs are important:
* /article/ - the index page, displaying a list of articles
* /article/slug - to display an article
* /article-edit/ - to create an article
* /article-edit/slug - to edit an article

There is more to say, which I will add later, as the library gets more refined & robust.


Goal: Develop Article Writing Lib
* Obj: Create markdown quick-tips box
* Obj: Validate input
* Obj: Provide error message on save failure
* Obj: auto-save while writing article
* Obj: Change article status between 'published', 'draft', and 'archived'. 
* Obj: Allow deletion of Articles
* Obj: Create ledger of changes to articles
* Obj: Make the Articles Index prettier
* Obj: Make Articles Searchable (separate library integration?)
* Obj: Integrate a permissions system AND/OR allow for Permissions systems to be integrated
* Obj: Add create / edit link when user is authorized
* Obj: Use a WYSIWYG Editor or create a sub-library or an addon that allows WYSIWYG editing. (keep markdown as an option?)
* Obj: Record & display author details (separate library integration)
* Obj: Allow commenting (separate library integration?)
* Obj: Create sharing buttons for social medial (separate library integration??)
* Obj: Create perma-urls, likely with GET values
