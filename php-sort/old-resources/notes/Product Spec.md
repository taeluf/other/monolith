# PHP Resources Helper
A package for loading resources , whether from files on-disk or from passed-in arrays or strings
The basic syntax is: `$myThing = R('the.key');`
keys are basic. Simply ([a-zA-Z\.]+)

- Load Data:
    - `R("/the/file/path.json")` - 
    - `R(["an"=>"array_of_data"])`
    - `R($arrayOr$path,"namespace");`
    - `R("the.key=","the_value");` - To namespace this, simply add a key. before "the.key"
    - `R(".fileExt",$dataString);` - Set the dataString with the particular format
    - `R(".fileExt",$dataString,'namespace');` - Set the dataString with the particular format & the specified namespace prefix
- Customize resource file type
    // delayed - `R($yamlParserObj,'yaml');`
    - `R( function($fileContent,$fileName){}, ['yaml','myyaml']);` - that's all a parser object really is
- Customize error handling - default uses trigger_error with a... (notice or warning?)
    // delayed - `R($errorHandlerObj)` - the lack of a second paramater identifies this from customizing resource file type
    - `R( function($error){});`
- Load a stored value
    - `$theValue = R("the.key")`
    - `$theValue = R("the.key","default_value")`
    - `$theValue = R("the.key.")` - to get a list

More technical aspect:
- Interpret the input:
    1) $arg1 matches `[a-zA-Z\.]+` means it's retrieval
    2) $arg1 matches `[a-zA-Z\.]+\=$` means set $arg2 to key $arg1
    3) $arg1 is an array means set values. optional $arg2 is a namespace prefix. Keys from $arg1 are set to $namespace.$key = $value
    4) $arg1 is an object or function and $arg2 is a string or array. Set the file-processor to the extension or extensions
    5) $arg1 is an array or object and the only argument. This is an error handler
- Delayed features:
    - Passing an object as your error handler or file parser may be added in a future version
    - P assing a data string directly (as opposed to file path) should be added to a future version
- Questions
    - model allows more customizability, if I want to add additional modifying symbols.
    - Are keys case sensitive? Let's say NO
    - What happens if "key" is set & someone tries "key.subThing"???
- Testing
    - Will use my own testing framework for testing through the browser.











# ROF File Type
- This note belongs in a nother repo
- And... yaknow. YAML looks like nearly everything I want & better.

A configuration file format. Targeted for PHP. 
There are three parts:
    1) A key
    2) A separator
    3) The data
The default functionality is:
    - Parse a string, return an array of key=>data pairs
    - data is always a string first, and it is trimmed, so all surrounding whitespace is removed from the data
    - data can be returned either flat or multi-layered
    - data can be multi-line
    - data is not wrapped in quotes
    - In [key][SEPARATOR], the separator can be repeated. If there are multiple separators, then the following data is ended via writing that separator the same number of times
    - Comments are written at the beginning of a line, preceded with COMMENT_STARTER, by default: `//`

The basic formats for entries are:
name: Reed                       // parses to name=>"Reed"
description: A person.           
He likes to do a lot of things.
description2: A human.
    Not sure why he isn't a goat.
//comment about the description2
    //This is part of the data
reed.Some.Thing: the-data
//this parses into "reed.Some.Thing"=>"the-data"
//or can parse_multi_dim like `"reed"=>["some"=>["Thing"=>"the-data"]]`



key[function][separator][data]

a.key(text):  the-data