<?php

class R {

    protected $data = [];
    protected $errorHandler = null;
    protected $fileHandlers = [
        // '.json'=>'json_decode'
    ];

    public function __construct(){
        $json_decoder = function($content,$ext){
            return json_decode($content,true);
        };
        $this($json_decoder,'.json');
        // exit
        // $this->errorHandler = [$this,'handleError'];
        $this([$this,'handleError']);
        // $this->fileHandlers['.json'] => function($content,$file)
    }

    protected function handleError($errorMessage){
        trigger_error($errorMessage);
    }

    public function set($key,$value){
        $key = strtolower($key);
        $this->data[$key] = $value;
        echo 'set key'.$key."\n";
    }

    public function get($key,$defaultValue=NULL){
        $key = strtolower($key);
        $args = func_get_args();
        if (isset($args[1])){
            return $this->data[$key] ?? $defaultValue;
        } 
        if (substr($key,-1)=='.'){
            $entries = [];
            foreach ($this->data as $entryKey=>$value){
                if (substr($key,0,-1)==$entryKey){
                    $entries['='] = $value;
                }else if (substr($entryKey,0,strlen($key))==substr($key,0)){
                    $entries[substr($entryKey,strlen($key))] = $value;
                }
            }
            return $entries;
        }
        if (!isset($this->data[$key])){
            $handler = $this->errorHandler;
            return call_user_func($handler,"Cannot get. Key '{$key}' has not been set.");
        }
        
        return $this->data[$key];
    }

    public function __invoke(...$args){
        if (count($args)===0)return $this->data;
        $arg1 = $args[0];

        // var_dump($args);
        // var_dump($args[1]);
        // exit;
        // set a data-type-handler
        if (is_callable($arg1)&&isset($args[1])){
            // echo "NA NA NA";
            // var_dump($args[1]);exit;
            $callable = $arg1;
            $extensions = $args[1];
            if (is_string($extensions))$extensions = [$extensions];
            foreach ($extensions as $extension){
                if (substr($extension,0,1)!='.')$extension = '.'.$extension;
                $this->fileHandlers[$extension] = $callable;
            }
            return;
        }
        if (is_callable($arg1)&&!isset($args[1])){
            // echo "\n\nblah\n";
            // var_dump($arg1);
            // exit;
            $this->errorHandler = $arg1;
            return;
        }

        //($dataString, '.ext', $namespace=null)
        if (is_string($arg1)
            &&substr($arg1,0,1)=='.'){
                $content = $args[1];
            $dataStringHandler = $this->fileHandlers[$arg1];
            $data = call_user_func($dataStringHandler,$content,$arg1);
            $forwardArgs = array_slice($args,2);
            $this($data,...$forwardArgs);
            return;
        }
        //('retrieval.key')
        $keyReg = '/^([a-zA-Z\.]+)$/';
        if (is_string($arg1)
            &&preg_match($keyReg,$arg1)){
                return $this->get(...$args);
        }

        //('retrieval.key=','value_to_set')
        $keySetReg = '/^([a-zA-Z\.]+)\=$/';
        $matches = [];
        if (is_string($arg1)
            &&preg_match($keySetReg,$arg1,$matches)){
                $this->set($matches[1],$args[1]);//...$args);
                return;
        }

        //($arrayToSet,'namespace.key') - modify the $arrayToSet
        if (is_array($arg1)&&count($args)==2){
            $correctedData = [];
            $namespace = $args[1];
            if (substr($namespace,-1)!='.')$namespace .='.';
            foreach ($arg1 as $key=>$value)$correctedData[$namespace.$key] = $value;
            $arg1 = $correctedData;
        }
        //($arrayToSet, ?$namespace) - set the array to our internal data structure
        if (is_array($arg1)){
            foreach ($arg1 as $key=>$value){
                if (is_array($value)){
                    // $preKey = '';//isset($args[1]) ? $args[1].'.' : '';
                    $this($value,$key);
                } else $this->set($key,$value);
            }
            return;
        }
        //('/a/file/path.someExt', ?$namespace)
        if (is_string($arg1)&&is_file($arg1)){
            // var_dump($arg1);
            // exit;
            $content = file_get_contents($arg1);
            $ext = pathinfo($arg1,PATHINFO_EXTENSION);
            $this('.'.$ext,$content);
            $dataStringHandler = $this->fileHandlers['.'.$ext];
            // $data = $dataStringHandler($arg1,$content);
            $data = call_user_func($dataStringHandler,$content,'.'.$ext);
            // var_dump($data);
            // exit;
            $forwardArgs = array_slice($args,1);
            $this($data,...$forwardArgs);
            return;
        }
    }
}

// $className = "R";
function R(...$args){
    static $theObject = null;
    $theObject = $theObject ?? new R();
    // $theObject = new R();
    return $theObject(...$args);
};

// R("cats");