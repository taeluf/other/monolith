<?php


require_once(__DIR__.'/../src/R.php');
// require_once(__DIR__.'/../vendor/autoload.php');
require_once(__DIR__.'/../vendor/rof/unit-tester/src/Tester.php');
require_once(__DIR__.'/Tester.php');

\RTester::runAll();


exit;

// set values



// $defOne = R("defOne","?Default 1");
// $defTwo = R("DEFtwo","?Default 2");
// $defNot = R("defNot","?Don't Show This Default");

$over = R("over");    // Should this return "a string" or an array? Should it be an error?
$over = R("over.one");
$over = R("over.two");

/**
 * over could be something like:
 * ['='=>"a string", 'one'=>'String in an array', 'two'=> "Woah Woah Woah"]
 */
