<?php

class RTester extends \RBear\Tester {

    public function prepare(){
        echo "\nstart prep\n";
        // var_dump(R());
        // exit;
        R()->set("one","One Thing");
        // exit;
        R()->put(["two"=>'number 2']);
        R()->put(["one"=>"Sub one", 
            "two"=>"Sub two"], "dim.");

        R()->load(__DIR__."/dim-sub.json");

        R()->load(__DIR__.'/defaults.json');

        R()->set("over", "a string");
        R()->set("over.one", "String in an array");  
        R()->set("over.two", "Woah Woah Woah");
        echo "\n\n<br><br>prep done<br><br>\n\n";
    }

    public function testBaseOne(){
        // var_dump(R());
        // exit;
        // var_dump(R("one"));
        return R("one")=="One Thing";
    }
    public function testBaseTwo(){
        return R("two")=="number 2";
    }
    public function testDimOne(){
        return R("dim.one")=="Sub one";
    }
    public function testDimTwo(){
        return R("dim.two")=="Sub two";
    }
    public function testFileDimSubArray(){
        // echo "\n";  
        // // var_dump(R());
        // var_dump(R("dim.sub."));
        // echo "\n";
        return R("dim.sub.")==["one"=>'The first dim-sub',
                                "two"=>'The 2nd dim-sub'];
    }
    public function testListWithDefault(){
        $defaultArray = ["an"=>"cat", "a"=>"dog", "another"=>"mouse"];
        return R("not.found.", $defaultArray)==$defaultArray;
    }
    public function testFileDimSubNoValue(){
        // var_dump(R());
        // exit;
        return R("dim.sub",null)==null;
    }
    public function testFileDimSubOne(){
        return R("dim.sub.one")=='The first dim-sub';
    }
    public function testFileDimSubTwo(){
        return R("dim.sub.two")=='The 2nd dim-sub';
    }
    public function testDefaultsOne(){
        return R("defOne","Default 1")=='Default 1';
    }
    public function testDefaultsTwo(){
        return R("defTwo","Default 2")=='Default 2';
    }
    public function testDefaultsExisting(){
        // var_dump(R());
        // exit;
        return R("defNot","Don't Show This")=='SHould show this string from defaults.json';
    }
    public function testOverArray(){
        // var_dump(R("over."));
        return R("over.")==[
            '='=>'a string',
            'one'=> 'String in an array',
            'two'=> 'Woah Woah Woah'
        ];
    }
    public function testOverBase(){
        return R("over")=="a string";
    }
    public function testOverOne(){
        return R("over.one")=="String in an array";
    }
    public function testOverTwo(){
        return R("over.two")=="Woah Woah Woah";
    }

    public function testCaseSensitivity(){
        return R("OnE")=="One Thing";
    }
    public function testPartialKeyList(){
        // var_dump(R("o."));
        return R("o.",null)==null;
    }

    // public function testCustomFormat(){
    //     R(function($fileContent, $fileExt),
    //         ['yaml'])
    // }
}