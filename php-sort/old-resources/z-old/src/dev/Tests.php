<?php

namespace ROF\Resource;

class Tests extends \ROF\Tester {

    public function prepare(){
        \ROF\Resource::init();
        R_init(__DIR__.'/sample_resources.json');
        R_init(__DIR__.'/sample_secure_resources.json');
    }
    private function init_resources(){}


    public function getSecureString(){
        R_ns("security-testing");
        $secureString = R("fakepdo.secure.password");
        $userName = R("fakepdo.string.username");
        $dsn = R("fakepdo.string.dsn");
        $data = json_decode(file_get_contents(__DIR__.'/sample_secure_resources.json'),TRUE);
        $storedPasswordKey = $data['security-testing']['fakepdo']['secure']['password'];

        var_dump($dsn,$userName,$secureString,$storedPasswordKey);
        exit;

    }

    public function getAutoFileType(){
        $derivedExtension = R("image.favicon.fileExtension");
        $knownExtension = 'ico';
        if ($derivedExtension!==$knownExtension){
            return FALSE;
        }
        $derivedExtension = R("image.big_png.fileExtension");
        $knownExtension = 'png';
        if ($derivedExtension===$knownExtension){
            return TRUE;
        }
        return FALSE;
    }
    public function checkKeyFailure(){
        R_ns("type-fail");
        try {
            $miskeyed = R("string.misk^()#@%&*eyed@:string");
        } catch (\Exception $e){
            if (trim($e->getMessage())==
                "Lookup key 'type-fail.string.misk^()#@%&*eyed@:string' is not valid. The key 'misk^()#@%&*eyed@:string' is not valid. "
                ."With resource type 'string' and var_dump'd value 'string(28) \"miskeyed_string_should_throw\"'" 
            ){
                return TRUE;
            }
        }
        return false;
    }

    public function throwOnTypeMismatch(){
        R_ns("type-fail");
        try {
            $intAttempt = R("string.misplaced_int");
        } catch (\Exception $e){

            if (trim($e->getMessage())==
                "Lookup key 'type-fail.string.misplaced_int' is not valid. Failed for resource type 'string'"
                ." on key 'misplaced_int' on var_dump'd value 'int(876)'"
            ){
                return TRUE;
            }
        }
        return false;
    }
    public function deepNamespace(){
        $deepestOfText = R("lvl1.lvl2.lvl3.string.deep_text");
        $testStr = "deepest of text";
        if ($testStr==$deepestOfText){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function getAllResources(){
        $all = R();
        $testAgainst = json_decode(file_get_contents(__DIR__.'/sample_resources.json'),TRUE);

        if ($all===$testAgainst
            &&count($all)>3
            &&$all['image']['favicon']['path']=="/am/dumb/path/favicon.ico"){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function getNoNSStringsArray(){
        $strings = R("string");
        if ($strings['dumb_cats']=='cats are dumb'
            &&$strings['dumb_dogs']=='dogs are dumb'){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function changeScopesTwice(){
        $no_scope = R("scoped-function.string.scoped_test");
        R_ns("scoped-function");
        $testStr = R("string.scoped_test");
        R_ns();
        $no_scope_again = R("scoped-function.string.scoped_test");
        try {
            $shouldNotMatch = R("string.scoped_text");
        } catch (\Exception $e){
            $shouldNotMatch = "caught an error";
        }
        $match1 = ($no_scope==$testStr);
        $match2 = ($no_scope==$no_scope_again);
        $match3 = ($testStr==$no_scope_again);
        $nomatch = ($shouldNotMatch!=$no_scope);
        return ($match1&&$match2&&$match3&&$nomatch);
    }
    public function getScopedNSStr(){
        R_ns("scoped-function");
        $testStr = R("string.scoped_test");
        return ($testStr=='scoped test string');
    }
    public function getNSOneStr(){
        $nsOneString = R("ns_1.string.str1");
        return ($nsOneString=="I am the first string of namespace 1");
    }

    public function getDevError(){
        $this->init_resources();
        $error = R("error.db_connection_failed.dev");
        return ($error=="The database connection failed to be created.");
    }

    public function getPublicError(){
        $this->init_resources();
        $error = R("error.db_connection_failed.public");
        return ($error=="There was an internal error on the website.");
    }

    public function getFaviconUrl(){
        $this->init_resources();
        $url = R("image.favicon.url");
        return ($url==="/am/dumb/url/favicon.ico");
    }

    public function getFaviconPath(){
        $this->init_resources();
        $path = R("image.favicon.path");
        return ($path==="/am/dumb/path/favicon.ico");
    }
    public function getStringFromMasterFile(){
        $this->init_resources();
        $catsAreDumb = R("string.dumb_cats");
        if ($catsAreDumb==="cats are dumb"){
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function initialize(){
        $this->init_resources();
        $all = R_all();
        if (is_array($all)){
            return TRUE;
        } else {
            return TRUE;
        }
    }

}

?>