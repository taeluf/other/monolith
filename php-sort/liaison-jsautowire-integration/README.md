# JS Autowire
A [liaison](https://github.com/Taeluf/Liaison) integration for [JS-Autowire](https://github.com/Taeluf/Autowire)

## Status: Unstable
Both JS Autowire & this lia-extension are under development & unstable.

## Install
I'll fill this out when stability comes. The composer package name is `taeluf/lia-jsext-autowire`

## Setup
`true` is default. Pass `false` if you don't want to add the js files immediately. You ca
```php
\Lia\JSExt\Autowire::setup($liaison, true);
// or
\Lia\JSExt\Autowire::setup($liaison, false);
$liaison->view('js-ext/autowire').'';
// I think the `.''` is required, because some stuff happens during the __toString(). This will be improved in Liaison v2
```
