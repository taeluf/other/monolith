<?php

namespace Lia\JSExt;

class Autowire {
    
    /**
     * Get the absolute path to the component directory. 
     * Use $liaison->addComponentDir(\JSAutowire::compoDir());
     *
     * @return string
     */
    static public function compoDir(): string{
        return realpath(__DIR__.'/../');
    }    
    // /**
    //  * Get the absolute path to the non-minified built javascript file
    //  * $code = file_get_contents(\JSAutowire::jsFullPath());
    //  *
    //  * @return string absolute path to the non-minified, compiled javascript
    //  */
    // static public function jsFullPath(): string {
    //     return realpath(__DIR__.'/../../Autowire-full.js');
    // }    
    // /**
    //  * Get the absolute path to the minified built javascript file
    //  * $code = file_get_contents(\JSAutowire::jsMinPath());
    //  *
    //  * @return string absolute path to the non-minified, compiled javascript
    //  */
    // static public function jsMinPath(): string {
    //     return realpath(__DIR__.'/../../Autowire-min.js');
    // }

    static public function setup($liaison, $includeScripts=true){
        $name = 'lia-jsext-autowire';
        $p = $liaison->package($name);
        if ($p==null)$liaison->addPackage(['dir'=>self::compoDir(),'name'=>$name]);
        if ($includeScripts)$liaison->view('js-ext/autowire').'';
    }
}