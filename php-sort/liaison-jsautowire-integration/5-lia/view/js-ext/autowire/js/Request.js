Autowire.Request = class {
    constructor(url, method){
        this.params = {};
        this.url = url;
        this.method = method || 'POST';
    }
    put(key,value){
        if (key in this.params){
            this.params[key] = (typeof this.params[key]==typeof []) ? this.params[key] : [this.params[key]];
            this.params[key].push(value);
        } else {
            this.params[key] = value;
        }
    }

    handleJson(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        const submitData = {method: this.method, mode: "cors"};
        if (this.method=='POST')submitData['body'] = formData;
        fetch(this.url, submitData).then(res => {
            return res.json();
        }).then(json => {
            func(json);
        });
    }
    handleText(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            body: formData
        }).then(res => {
            return res.text();
        }).then(text => {
            func(text);
        });
    }
}