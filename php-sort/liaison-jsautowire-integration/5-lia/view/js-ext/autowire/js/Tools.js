


Autowire.Tools = class {};
Autowire.Tools.ready = false;
Autowire.Tools.onPageLoad = function (func, thisArg, ...args) {

    if (this.ready) {//window.readyState == "complete"
        func.apply(thisArg, args);
    } else if (window.addEventListener != null) {
        document.addEventListener("DOMContentLoaded", function () {
            if (document.readyState == "interactive") {
                this.ready = true;
                func.apply(thisArg, args);
            }
        }.bind(Autowire.Tools));
    } else {
        console.log('window.onload, oh no');
        window.onload = function () {
            this.ready = true;
            func.apply(thisArg, args);

        }.bind(Autowire.Tools);
    }
}
Autowire.Tools.getObjectMethodNames = function(object){
    const properties = new Set();
    let currentObj = object;
    do {
        Object.getOwnPropertyNames(currentObj).map(item => properties.add(item))
    } while ((currentObj = Object.getPrototypeOf(currentObj)))
    return [...properties.keys()].filter(item => typeof object[item] === 'function')
}

Autowire.Tools.objectId = function(object){
    this.objectMap = this.objectMap || new WeakMap();
    if (this.objectMap.has(object)){
        return this.objectMap.get(object);
    }
    
    this.idMap = this.idMap || {};
    this.counter = this.counter || 0;
    const id = object.constructor.name+":"+(new Date()).getMilliseconds() + "-" + Object.keys(this.objectMap).length + "-" + this.counter++ + "-" + Math.random();
    this.idMap[id] = object;

    this.objectMap.set(object,id);
    
    return id;
}.bind(Autowire.Tools);

Autowire.Tools.objectFor = function(id){
    return this.idMap[id];
}.bind(Autowire.Tools);

