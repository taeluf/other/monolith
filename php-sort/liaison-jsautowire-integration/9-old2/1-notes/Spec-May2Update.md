# Spec: May 2 update
I would like to add:
- Simplified Web Requests: this.send(url,data,this.responseMethod)
- Switch To Node: this.value refers to an object. Add a toggle value, such that this.value may refer instead to the node.value & this.object can get the non-node object
- Better Error reporting: When auto-property does not work, report that the desired item is not found in the context & maybe provide a list of items that ARE in the context & what the name of the context is
- Add state & auto-updating of values: 
    - For <button class="Example">{{something}}</button>
        this.state.something = "cats" will update the button text to say "cats"
        OR have _something be a property & 'something' be an auto-property
        OR have __state(){return ['something']}... to return an array of state-properties
- Modify how context works:
    - Declare __context(){return {"contextName1":{"auto_prop_name":'ObjectClassName', "auto_prop2":'.cssClassName'?}, "contextName2":['items in context 2'...]}}...
    OR in __construct() call this.context = {....}