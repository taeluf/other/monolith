# From here to polished
The framework is pretty good. Things would like I'd like them to, and overall I'm very happy with it. A couple of tasks are a little cumbersome, but... overall quite acceptable. The only feature improvement would be adding a default context which is page-wide. Some things need to be improved aesthetically, and the package needs to be made for release.

## Features to Add/Improve
- When not declared, provide a default context which all Autowireds on a web-page will share

## Aesthetic improvements
- Condense `DomWire` and `WireContext`
- Rename the base class to `Autowire` (or something cooler)
- Remove all dead comments & any console.logs that may be remaining
- Provide console.log feedback when features are not implemented properly
    - For auto-props, state if no context is defined on the class
    - There's an error with context where... I think context.values is undefined because it's not an array? Not clear on this one. But it should be handled and give a helpful error instead
- Put a minified version of the framework in a one-liner in the docs page (simplified hosting)
- Write inline-docs on the javascript files
- Document all parts of the package, including internals like Request & Tools

## For Release
1) Create a concatenated & a minified js file
2) List this package on:
    - NPM
    - Github
    - A CDN
    - Composer/Packagist
3) Include a release-version on ReedyBear.com & push the docs to the public web

## What else?
- Write tests 
- Write more examples
- Share it on reddit or somewhere else that might find some interest. Get feedback, integrate changes, look to community for improvement.  