

## Example
1. You have HTML
2. You have a Javascript class that extends `RB.Autowire`
3. You call `YourClass.autowire()` to link 1 & 2 together

```html
<button class="rb-Increment">Add 1</button>
<div class="RB_Count">0</div>
```

```js
class RB_Count extends RB.Autowire{}
    RB_Count.autowire(); // Will connect it to query selector ".RB_Count"
    RB_Count.autowire('div.RB_Count'); //Pass a query selector for more specificity

class Increment extends RB.Autowire {} //we fill this out later
    Increment.autowire('.rb-Increment'); //empty autowire() would look for ".Increment", missing class="rb-Increment"
    //or
    Increment.className = "rb-Increment"; //change the default lookup
    //Increment.querySelector = ".rb-Increment"; //Overrides Increment.className lookup.
    Increment.autowire();
```

Let's add functionality
```js
    class Increment extends RB.WireContext {
        //this.count will now retrieve an RB_Count instance
        _count(){ return 'RB_Count'; } 

        onclick(event){
            const count = parseInt(this.count.node.innerText.trim())+1;
            this.count.node.innerText = count;
        }
    }
```  




### Final Code
This is the final code. You can copy+paste this, along with the minified Autowire code into a fiddle to see it in action.

```html
<script type="text/javascript">
    class Increment extends RB.Autowire {
        _count(){ return 'RB_Count'; } // this.count will return the .RB_Count Object (not the node directly). 

        onclick(event){
            const count = parseInt(this.count.node.innerText.trim())+1;
            this.count.node.innerText = count;
        }
    }
    // Increment.querySelector = ".rb-Increment";
    Increment.className = "rb-Increment";
    Increment.autowire();
    
    //Only here to provide auto-prop referencing
    class RB_Count extends RB.Autowire{}
    RB_Count.autowire();
</script>

<div style="background:lightblue;">
    <button class="rb-Increment">Add 1</button>
    <div class="RB_Count">0</div>
</div>
```

<!-- ### Demonstration
**TODO:** I need to include RB.Autowire on this page... This demonstration won't work without it

<script type="text/javascript">
    class Increment extends RB.Autowire {
        _count(){ return 'RB_Count'; } // this.count will return the .RB_Count Object (not the node directly). 

        onclick(event){
            const count = parseInt(this.count.node.innerText.trim())+1;
            this.count.node.innerText = count;
        }
    }
    // Increment.querySelector = ".rb-Increment";
    Increment.className = "rb-Increment";
    Increment.autowire();
    
    //Only here to provide auto-prop referencing
    class RB_Count extends RB.Autowire{}
    RB_Count.autowire();
</script>

<div style="background:lightblue;">
    <button class="rb-Increment">Add 1</button>
    <div class="RB_Count">0</div>
</div> -->
