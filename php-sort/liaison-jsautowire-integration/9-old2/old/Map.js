
RB.Map = new class {

    nodeMap = [];
    pushObjNode(obj,node){
        this.nodeMap.push({"obj":obj,"node":node});
    }
    getNode(obj){
        const entry = this.getEntry(obj);
        return entry ? entry.node : null;
    }
    getObj(node){
        const entry = this.getEntry(node);
        return entry ? entry.obj : null;
    }
    getEntry(mixed){
        for(const entry of this.nodeMap){
            for (const key in entry){
                const value = entry[key];
                if (value===mixed)return entry;
            }
        }
        return null;
    }


    contextMap = [];
    pushContext(obj,contextAnchor){
        let context;
        if (!(context=this.getContextEntry(contextAnchor)))this.contextMap.push(context={'anchorObj':contextAnchor,'objects':[]});
        if (obj!==null)context.objects.push(obj);
    }
    contextOfObject(object){
        for (const entry of this.contextMap){
            if (entry.objects.indexOf(object)!==-1){
                return entry;
            }
        }
        const nullContext = this.getContextEntry(null);
        this.pushContext(object,nullContext);
        // if (nullContext!==null){
        //     console.log('nullest context');
        // }
        return this.getContextEntry(null);
    }
    getContextEntry(contextAnchor=null){
        if (contextAnchor==null)return (this.nullContext=this.nullContext || {'anchorObj':contextAnchor,'objects':[]});
        for (const entry of this.contextMap){
            if (contextAnchor===entry.anchorObj){
                return entry;
            }
        }
        // console.log('in getCOntextEntry()');
        // console.log(this.contextMap);
        // console.log(contextAnchor);
        // const nullContext = this.getContextEntry(null);
        // if (nullContext===null)this.pushContext(null,null);
        return null;//this.getContextEntry(null);
        // return null;
    }
    objectsInContext(contextAnchor){
        const entry = this.getContextEntry(contextAnchor);
        // console.log(entry);
        return entry ? entry.objects : null;
    }
}
