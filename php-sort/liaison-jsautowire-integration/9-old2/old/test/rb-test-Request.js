
class RB_Test_Request extends RB.Autowire {

    _result(){return "RB_Test_Result";}
    __construct(){
        this.context = "rb-test-request";
    }
    onclick(event){
        this.send('https://api.jsonbin.io/b/5ead8aff07d49135ba49644d',{},this.showData,"GET");
    }
    showData(json){
        this.result.node.innerText += JSON.stringify(json);
    }
}
RB_Test_Request.autowire('.rb-test-Request');


class RB_Test_Result extends RB.Autowire {

    __construct(){
        this.context = "rb-test-request";
    }
}
RB_Test_Result.autowire('.rb-test-Result');
