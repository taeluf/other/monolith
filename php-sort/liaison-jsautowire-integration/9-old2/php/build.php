<?php

ob_start();
require('framework.js.php');
$output = ob_get_clean();

file_put_contents(__DIR__.'/../Autowire-full.js',$output);


//Minify code copied from https://javascript-minifier.com/php
$url = 'https://javascript-minifier.com/raw';
$js = $output;

// init the request, set various options, and send it
$ch = curl_init();

curl_setopt_array($ch, [
    CURLOPT_URL => $url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_POST => true,
    CURLOPT_HTTPHEADER => ["Content-Type: application/x-www-form-urlencoded"],
    CURLOPT_POSTFIELDS => http_build_query([ "input" => $js ])
]);

$minified = curl_exec($ch);

// finally, close the request
curl_close($ch);

// output the $minified JavaScript
file_put_contents(__DIR__.'/../Autowire-min.js',$minified);