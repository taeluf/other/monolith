# DBViews - Liaison Package
Connect pure HTML layouts and forms to database entities with auto-submission. Uses [Liaison](https://github.com/Taeluf/Liaison).

# UNDER DEVELOPMENT
 **Not** ready to use. Liaison is going through major updates, so this will be neglected until after Liaison is finished being updated. It will be improved & well-documented... eventually.

### Setup the package
```php
$liaison = new \Liaison();

$liaison->addPackage(['dir'=>__DIR__.'/YourPackageDir/','name'=>'Site']);
$sitePackage = $liaison->package('Site');
//I'm not sure if I have the public dir implemented on this package yet. You might put it in a package that uses \Lia\Package
$sitePackage->freshCompo->setUpload($uploadDirectory=__DIR__.'/YourPackageDir/public/upload/', $uploadUrlPrefix='/upload/');

if (\YourCustomFunction\canActiveUserEdit()){
    //yeah, you need both... ugh.. I don't really know why
    \Fresh\Autowire::setAllowEdits(true);
    $liaison->set('Fresh.allowEdit',true);
    //edits are disabled by default.
}

$liaison->deliver();
```

### Manually display a view or form (instead of routing)
```php
$liaison->view("ViewName",['lia'=>$liaison,'other_passthru_var'=>"whatever"])
$liaison->form("ViewName",['lia'=>$liaison, "submit_url" => '/your/submit/url/', 'id'=>imcrankyandtiredofwritingdocs])
```

### Manually handling a form
1. Make a view file, ending in `View.php`. Put a `<form>` in it.  Set the action & method & everything. Don't make it an entity. Give it a `<lia-route ...>`.
    - I think you can make it an entity, but then you'd have to put both a `name` and an `rb-prop` because the form-features would be gone. I'm not sure.
2. Create another view file for the submission script, or create a public script (which I don't think this package supports yet)
    - Make a `<lia-route pattern="@POST./the-submision-url/">`, assuming set your `method="POST"`, cause... come on lol.

## Package Structure
- conf.json - MUST contain the package name as json: `{"package": "\\Fresh\\Package"}`
- public/ - I don't think the public dir is active yet, thus you have to also have a Lia\Package... package
- compiled/ - compiled views and forms get put in here, the dir will be created for you
    - setup/ - the setup files, which setup routing will be put here
- class/ - I think this is PSR4 compliant... idk. Your stuff gets autoloaded. I don't think theres any way to specify namespaces... I'm not sure.
- view/
    - Page_HomeView.php
    - Page_HomeView/
        - ContentLayout.css
        - Colors.css
    - Page_HomeForm.php
    - Page_HomeForm.css
    - Page_HomeForm.js


### Views
Look to forms (below) for routing information

"smart" forms (see below) require two files
- `view/TheThingView.php` 
- `view/TheThingForm.php` 
```html
<?php
    // uhh, DONT use someone's IP without their consent
    // And then encrypt it or hash it if you do have consent.
    $ip = $_SERVER['REMOTE_ADDR'];
    //but $ip will now be available to the entire script
?>
<?="<h6>Your ip is {$ip}.</h6>"?>
<!-- 
    rb-table:
        - The table name to load your entity from, also used for the same-named form
        - A same-named variable will be created, so `$TheThingy` will be the object loaded from the db
        - Whatever you write will be converted to lowercase and have all non-alpha characters removed, for the sake of the database portion
    rb-find:
        - You can only use one rb-find at a time, both are shown here just for example. I don't know what happens when both are present.
        - id:* will load all objects from the rb-table
        - id:<?=$id?> will load only the item with id=$id.
            - $id can come from:
                - the passthru, like: `$lia->view('TheThingy',['id'=>33])`
                - the url, for a route pattern like '/thething/$id/'
                - An eariler piece of PHP code in the view script
    tlf-autoform:
        - Indicates that this is the node to reference when the TheThingForm.php is loaded. This MUST be declared for "smart" forms to work
        - I don't now what happens if there are two nodes in the same file with 'tlf-autoform' declared
-->
<section  
    rb-table="TheThingy" 
    rb-find="id:*;" 
    rb-find="id:<?=$id?>"
    tlf-autoform
    >
    <!--
        rb-prop:
            - This h1 will be filled with $TheThingy->title
        rb-format (the div):
            - $TheThingy->description will be converted to markdown, and the markdown will fill the <div>
        rb-prop="cover_image":
            - The src attr will be filled with $TheThingy->cover_image
        <a rb-prop="reference_url">:
            - href will be set to $TheThingy->referenceUrl
        <?=$TheThingy->referenceName?>:
            - This will just print that value
    -->
    <h1 rb-prop="title"></h1>
    <div rb-prop="description" rb-format="markdown"></div>
    <img rb-prop="cover_image"  src="" alt="Depicting an action" style="max-height:100%;max-width:100%;" />
    <a rb-prop="reference_url"><?=$TheThingy->referenceName?></a>
    <div>
        <?php
            // this will display a dependent view. That dependent view has:
            // an rb-find="thething_id:<?=$thethingy_id?>"
            // $lia MUST be passed in order for everything to function.
            echo $lia->view('SomethingElse',['lia'=>$lia,'thethingy_id'=>$TheThingy->id]);
        ?>
    </div>
</section>
```

### Forms
- You can route and create forms manually in views, by simply putting the form in a file ending in `View.php`. You lose the smart form features.
- You cannot even route to a file ending in `Form.php` if there is not a same-named view file.
- I THINK you can manually display a form even if it lacks an associated view... not sure.
- The entity loaded for editing will be identified by `$_GET['id']`

"smart" forms require two files
- `view/TheThingView.php` 
    - The associated view, where entity information is loaded from. 
    - Can have a route, but does not require it
    - MUST have an entity node
- `view/TheThingForm.php` 
    - The file below
```html
<!-- BlockForm is from https://github.com/Taeluf/Lia-Views for nice form...matting (lol) I use it. You don't need to. -->
<?php $lia->view('theme/BlockForm'); ?>  

<!-- 
    <lia-route> creates automatic routing to the view/form
    - pattern, basically, is the url. For more details, see https://github.com/Liaison
    - isform generates the submit url by appending `submit/` to the pattern
       - A route will be created for submission as well, so you will NOT write a submit script
    - redirect is where users will be sent on submission
    - onmatch: PHP Code that runs when the route is matched. Must be wrapped in PHP tags
        - I think this works for forms?? It might only work for views
        - It only works for static patterns. Dynamic patterns like /thething/$id/edit/ won't execute your onmatch
    - You can have multiple routes for views. I think it works for forms too.

-->
<lia-route 
    pattern="/thething/edit/" 
    isform 
    redirect="/"
    >
<lia-route 
    pattern="/thething/create/" 
    isform 
    redirect="/"
    onmatch="<?php $id = 'new' ?>"
    >

<!-- 
    A button to create a new instance of the entity
    Just has to have ?id=new instead of ?id=someid
-->
<a href="?id=new"> <!-- With the 2nd route & the onmatch, we could instead route to /thething/create/ -->
    <button type="Submit" style="background-color:<?=$newColor?>;padding:0.4rem 1.0rem;border-radius:0.5rem;" >New Item</button>
</a>
<!-- 
    - action set based upon the route. 
    - Method sets to POST
    - enctype will be set for forms containing a file input
        - if the file input is included through a sub-view ($lia->view(...)), then you'll have to add the enctype yourself, as the sub-view is added at runtime. & enctype is set at compile time... I might be lying a little. Not sure.
-->
<form class="BlockForm" > 
    <!-- With a same-named View, the table will be auto-filled-->
    <!-- values will be auto-filled from the entity with id == $_GET['id'] -->
    <label>Title
        <input type="text" name="title" />
    </label>
    <label>Description <br>
        <textarea name="description"></textarea>
    </label>

    <!-- Add a delete button -->
    <!-- Hidden input because `submit` is a poorly handled feature in https://github.com/Taeluf/Fresh-Compo -->
    <input type="hidden" name="fresh_delete" />
    <!-- It's the the delete button. We use javascript to add the name because of the poorly implemented `submit` feature
         It's hacky as heck, and I don't like it. But it's where we are.
    -->
    <input 
        type="Submit" 
        value="Delete" 
        style="background-color:red;padding:0.4rem 1.0rem;border-radius:0.5rem;" 
        onclick="if (!confirm('Delete this?')){event.stopPropagation();event.preventDefault();} else {this.name='fresh_delete';}" 
        />

    <!-- 
        These will be auto-added:
        <input name="fresh_table" value="THE_TABLE_NAME" type="hidden">
        <input name="id" value="the $id variable, generally from $_GET" type="hidden">
    -->

    <input type="Submit" value="Submit">
</form>
```













# Yeah so,
I thought this is what I was gonna write.



## Quick Example
### Executing it

### The View

### The Form

## Package Structure
- public/
- view/
    - Page_HomeView.php
    - Page_HomeView/
        - ContentLayout.css
        - Colors.css
    - Page_HomeForm.php
    - Page_HomeForm.css
    - Page_HomeForm.js

## Create a View

### Routing the view

### Display the view manually

## Create a Form

### Route the form via associated view

### Route the form manually

### Display the form manually

### Submitting the Form

## Editing entities



## Other notes

