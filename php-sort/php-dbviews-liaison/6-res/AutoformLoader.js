
class AutoformLoader extends RB.Autowire {

    attach(){
        // console.log('attached');
        this.node.addEventListener('click',this.clicked.bind(this));
    }
    clicked(event){
        event.stopPropagation();
        event.preventDefault();
        window.location.href = this.node.getAttribute('tlf-clicktoedit-url')+'?id='+this.node.getAttribute('tlf-clicktoedit-id');
    }
}

AutoformLoader.autowire('[tlf-clicktoedit-url]');
