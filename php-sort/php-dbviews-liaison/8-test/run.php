<?php

require_once(dirname(dirname(dirname(dirname(dirname(__DIR__))))).'/dependency-manager.php');

class TestNewCompo extends \RBear\Tester {

    public function prepare(){
        require_once(__DIR__.'/sample/Note.php');

    }

    public function testDisplayView(){
        $this->clearCompiledFiles();
        $lia = new \Liaison();
        $lia->addPackage(['dir'=>__DIR__.'/sample/','name'=>'fresh']);
        $note = $lia->package('fresh');
        $note->setFinder(
            function($table,$lookup){
                return (object)[
                    (object)[
                    'id'=>null,
                    'body'=>'a basic, default note.',
                    'name'=>'a dumb note'
                ]];
            }
        ); 

        $note->onRequest_Setup(null,true);

        $view = $note->view(null,['pattern'=>'/']);
        ob_start();
        echo $view;
        $output = ob_get_clean();
        echo $output;
        if (strpos($output,'a basic, default note. ')!==false
            &&strpos($output,'<h1>Note: a dumb note</h1>')!==false
            &&strpos($output,'<lia-route')===false){
                return true;
            }
        return false;
    }
    public function testDeliverStaticRoute(){
        $this->clearCompiledFiles();
        $lia = new \Liaison();
        $compo = new \FreshCompo(__DIR__);
        $compo->setFinder(
            function($table,$lookup){
                return (object)[
                    (object)[
                    'id'=>null,
                    'body'=>'a basic, default note.',
                    'name'=>'a dumb note'
                ]];
            }
        );
        $lia->addComponent($compo);
        $lia->Event->AddFreshCompo(new \Li\Note(),true);

        ob_start();
        $lia->deliver('/');
        $output = ob_get_clean();
        echo $output;
        if (strpos($output,'a basic, default note. ')!==false
            &&strpos($output,'<h1>Note: a dumb note</h1>')!==false
            &&strpos($output,'<lia-route')===false){
                return true;
            }
        return false;
    }
    public function testDeliverDynamicRoute(){
        $this->clearCompiledFiles();
        $lia = new \Liaison();
        $compo = new \FreshCompo(__DIR__);
        $compo->setFinder(
            function($table,$lookup){
                return (object)[
                    (object)[
                    'id'=>$lookup,
                    'body'=>$lookup.'a basic, default note.',
                    'name'=>'a dumb note'
                ]];
            }
        );
        $lia->addComponent($compo);
        $lia->Event->AddFreshCompo(new \Li\Note(),true);

        ob_start();
        $lia->deliver('/note-3/');
        $output = ob_get_clean();
        echo $output;
        if (strpos($output,'a basic, default note. ')!==false
            &&strpos($output,'<h1>Note: a dumb note</h1>')!==false
            &&strpos($output,'<p>MD::::id:3a basic, default note.::::DM</p>')!==false
            &&strpos($output,'<p>MD::::id:*a basic, default note.::::DM</p>')!==false
            &&strpos($output,'<lia-route')===false){
                return true;
            }
        return false;
    }
    public function testDeliverFormEdit(){
        $this->clearCompiledFiles();
        $lia = new \Liaison();
        $compo = new \FreshCompo(__DIR__);
        $compo->setFinder(
            function($table,$lookup){
                return (object)[
                    (object)[
                    'id'=>$lookup,
                    'body'=>$lookup.'a basic, default note.',
                    'name'=>'a dumb note'
                ]];
            }
        );
        $lia->addComponent($compo);
        $lia->Event->AddFreshCompo(new \Li\Note(),true);

        $_GET['id'] = 6836736;
        ob_start();
        $lia->deliver('/edit/');
        $output = ob_get_clean();
        echo $output;
        if (strpos($output,'<input name="id" value="id:6836736;" type="hidden"><input name="fresh_table" value="note" type="hidden">')!==false
            &&strpos($output,'<form action="/submit/" method="POST">')!==false
            &&strpos($output,'<input type="text" name="body" value="id:6836736;a basic, default note.">')!==false
            &&strpos($output,'<lia-route')===false){
                return true;
            }
        return false;
    }
    public function testDeliverFormCreate(){
        $this->clearCompiledFiles();
        $lia = new \Liaison();
        $compo = new \FreshCompo(__DIR__);
        $compo->setFinder(
            function($table,$lookup){
                return (object)[
                    (object)[
                    'id'=>$lookup,
                    'body'=>'',
                    'name'=>''
                ]];
            }
        );
        $lia->addComponent($compo);
        $lia->Event->AddFreshCompo(new \Li\Note(),true);

        unset($_GET['id']);
        ob_start();
        $lia->deliver('/edit/');
        $output = ob_get_clean();
        echo $output;
        if (strpos($output,'<input name="id" value="id:new;" type="hidden"><input name="fresh_table" value="note" type="hidden">')!==false
            &&strpos($output,'<form action="/submit/" method="POST">')!==false
            &&strpos($output,'<input type="text" name="body" value="">')!==false
            &&strpos($output,'<lia-route')===false){
                return true;
            }
        return false;
    }
    public function testDeliverSubmitNew(){
        $this->clearCompiledFiles();
        $lia = new \Liaison();
        $compo = new \FreshCompo(__DIR__);
        $compo->setFinder(
            function($table,$lookup){
                return (object)[
                    (object)[
                    'id'=>$lookup,
                    'body'=>'',
                    'name'=>''
                ]];
            }
        );
        $success = false;
        $compo->setSubmitter(
            function($table,$data,$itemId,$passthru) use (&$success){
                
                if ($table=='note'
                    &&$data==[
                        'name'=>['A new note'],
                        'body'=>['A good, new paragraph, telling you all about whatever this note is about. It\'s new!'],
                        'id' => ['']
                    ]
                    &&$itemId==''
                    &&$passthru==[
                        'pattern'=>'@POST./submit/',
                        'redirect_url'=>'/'
                    ]
                ){
                    $success = true;
                }
            }
        );
        $lia->addComponent($compo);
        $lia->Event->AddFreshCompo(new \Li\Note(),true);

        $_SERVER['REQUEST_METHOD'] = 'POST';
        $_POST =
            [
                'id'=>'',
                'name'=>'A new note',
                'body'=> 'A good, new paragraph, telling you all about whatever this note is about. It\'s new!',
                'fresh_table'=>'note'
            ];
        ob_start();
        $lia->deliver('/submit/');
        $output = ob_get_clean();
        if (trim($output)!='')$success = false;
        echo "output: ";
        var_dump($output);

        return $success;
    }

    protected function clearCompiledFiles(){
        $dir = __DIR__.'/sample/compiled/';
        if (is_dir($dir))$this->rmdirRecursive($dir);
    }
    protected function rmdirRecursive($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->rmdirRecursive("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }
}

\TestNewCompo::runAll();