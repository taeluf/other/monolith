<?php
array (
  'Access.DisplayItem' => 
  array (
    'comment' => 'Set an access control to determine if individual items can be displayed.
```php
$liaison->set(\'Fresh.Item.Display\',
    function($item, $table, $id) use ($liaison){
        // do your custom code. Maybe look up a user. Maybe just return true for displaying.
        return false;
    }
);
```

@TODO Allow bool setting for Fresh.Item.Display, when a callable is not needed.
@TODO MAYBE pass `$lia` along to the Fresh.Item.Display handler',
    'key' => 'Access.DisplayItem',
    'exportline' => '@export(Access.DisplayItem)',
  ),
  'Edit.Auto' => 
  array (
    'comment' => 'Add \'tlf-clicktoedit\' to any entity node which you want to have editable.',
    'key' => 'Edit.Auto',
    'exportline' => '@export(Edit.Auto)',
  ),
  'Edit.Access' => 
  array (
    'comment' => 'Declare a \'Fresh.Item.Edit\' callback.
- Return true to allow, false to not allow
- accept paramaters `$item`, `$tableName`, & `$itemId`
- Will be called for every single item that is loaded via an entity node during a request.
```php
$liaison->set(\'Fresh.Item.Edit\',
    function($item, $tableName, $itemId){
        // or check that however you want...
        if (currently_logged_in_user()->hasAccessTo($item))return true;
        return false;
    }
);
```',
    'key' => 'Edit.Access',
    'exportline' => '@export(Edit.Access)',
  ),
  'Edit.HTML' => 
  array (
    'comment' => 'Call `$lia->send(\'Body_Extra_End_Print\')` at the end of your `<body>` tag, so the \'Edit Content\' button can be displayed.',
    'key' => 'Edit.HTML',
    'exportline' => '@export(Edit.HTML)',
  ),
)
?>