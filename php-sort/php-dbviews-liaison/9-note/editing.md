# Notes on Editing

Features
- ClickToEdit: Click an item to edit it
    - call enableClickToEdit()
    - or `$lia->set('Fresh.ClickToEdit', true)`
    - Click 'edit' button to launch ClickToEdit
        - `$lia->set('Fresh.ShowEditPrompt')`
        - call showClickToEditPrompt()
    - GET ?edit=true to launch ClickToEdit
        Click button from ClickToEditPrompt OR manually type
- Inline 'edit', 'add new', and 'delete' buttons
    - `$lia->view('Fresh/New', ['view'=>$this, 'item'=>$theItem])`
    - `$lia->view('Fresh/Delete', ...)`
    - `$lia->view('Fresh/Edit', ...)`
- GET /itemediturl/edit/?id=3 to actually edit
- GET /itemediturl/edit/?id=new to create a new item
- GET /itemediturl/create/ to create a new item
- POST /itemediturl/submit/ to submit edits, creation, and deletion


```php
// this is necessary
$liaison->set('Fresh.Edit.Allow', true);

//these override Fresh.Access.Checker.... Checker is the fallback
$liaison->set('Fresh.Access.View', true); //always allow viewing
$liaison->set('Fresh.Access.Edit', function(){});//conditionally allow editing
$liaison->set('Fresh.Access.Delete', function(){});//conditionally allow deletion
$liaison->set('Fresh.Access.Create', function(){});//conditionally allow creating


$liaison->set('Fresh.Access.Checker', 
    function($mode=['view','edit','create','delete'], $table='tablename', $id='id'){

    }
);
// optional
$liaison->set('Fresh.Edit.OnClick', true);
$liaison->set('Fresh.Edit.Prompt', true);
$liaison->set('Fresh.Create.Allow', true);
$liaison->set('Fresh.Delete.Allow', true);
```