# Dev Status

Scaffolded out a bit of an example in `test/src/BlogOrm.php`

I wrote a really simple `lilorm` executable to run it from the cli. Wrote a couple starter functions for creating tables. 

Right now, it's pretty far off from what I want


## LilOrm Goals
- integrate with LilDb project
- have a SIMPLE cli-runner to create tables and do migrations
- Have a SIMPLE interface for defining tables
- Have a SIMPLE interface for loading queries into objects


An object that would represent a database row. Features:
- `$id_column`, necessary for running `UPDATE`
- `save()` function
- callback-approach to saving relationships
- callback-approach to loading relationships (maybe)
- Can be used anonymously.
- magic `__get()` to handle loading complex things (such as lazy loading comments on a blog post).
- pre-save callback

To handle DB operations, the Db object needs:
- a lildb instance
- a table name
- an id_column
- a list of properties to store in db

## LilDbRow
We'll have a root object `LilDbRow` (or something) that represents the object as loaded from the database. You can use `LilDbRow` as-is OR you can override it if you need any normalization or easier use.

### Properties
- `protected $properties = [];` // these are db properties
- `protected $id_column = 'id';`
- `protected $table = 'tbl_name';`
- `protected $src_row = [];`: the row as it was loaded initially (often from the db)

### Utility Methods
- `__construct($lilDb, $table, $id_column,$row=[])`
- `set_lil_db($lilDb)` or `set_db($lildb)` 
- `set_row($row)` 
- `save()` will `UPDATE` or `INSERT` depending on null/not-null of `id` column
- `delete()` will delete the row

### Methods For Overriding
- `willSave()`: returns `$this->properties[]` but can (should) be overridden for custom functionality
    - maybe `willSave($properties)`
- `didLoad()` or `didLoad($row)`: Return an array of properties, which gets set to `$this->properties`
    - so if you don't like the DB's format for something, you can modify it in `didLoad` and `willSave`, or you can just override `getProp` and `setProp` to get it in the format you want

### Other Methods
- `__get('prop')` calls `getProp()` or `get_db_prop('prop')` (returning from the array)
    - Also use this to get non-db props. Like, if you have a `slug` db prop, you might declare `getUrl()` to build a full url from the slug + the host + the blog category or something.
- `__set('prop', $val)` calls `setProp($value)` or `set_db_prop('prop', $val)` (setting it to the properties array)
    - only set to properties array if the props array is empty OR the property is already defined in it

### Static Properties
- `static public $lilDb`: Used by static functions for easier instantiation
- `static public $table`: If this is null, I want to derive it from the class name ...
- `static public $id_column = 'id'`:

### Static Functions
- `static public function new()`: return `new ThisClass(static::$lilDb, static::$table ?? strtolower(static::class), self::$id_column)`
- `static public fromRow($row)`: return `::new()` but also set the row
    - MAYBE have `new()` accept an optional row, then `fromRow($row)` can modify the row if we wish
    - how are invalid properties handled?


## LilDbTable
We'll have a root object `LilDbTable` (or something) that is used to create a table & handle migrations. This could be part of the `LilDbRow`, but that sounds like clutter. It could be organized into traits, maybe. I like everything being together. But one piece is distinctly for working with rows in your codebase. The other is distinctly for setting up your database. I think these should be separate. I also don't like the different parts of the same thing being separated, so idk.

## Properties
- `static public $lilDb`: Used by static functions for easier instantiation
- `static public $table`: If this is null, I want to derive it from the class name ...
- `static public $id_column = 'id'`:

## Static Utility Functions
- `create($version)`: calls `create_version()`
- `delete($areYouSure=false)`
- `empty($areYouSure=false)`
- `migrate($fromVersion, $toVersion)` calls `migrate_v1_to_v2()`

## Other Notes
- I want a way to create/migrate all my tables, then call:
    - `all_tables_created()` and `all_tables_migrated()`
        - good time to add foreign key constraints, since all the table exist now!
- I want a way to execute sql queries on the table & return either array rows or LilDbRows.
    - MyTable::getBlogs();
- I want a really simple way to separate out `JOIN` rows and create an easy interface for integrating into the resulting object
- If i have an array of array-rows (by using LilDb directly), I want an easy way to convert that to an array of `LilDbRow`s.
- maybe an `objectify` callback that takes a row & returns an object (this is overlap with `LilDbRow`)
 







