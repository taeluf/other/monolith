# LilOrm
This project is a hobby project under development. I have no ETA. It is MIT license, so go crazy, but don't take the name `LilOrm`, please.

See [Status.md](/Status.md) for my up-to-date dev notes

You can use [LilDb](https://tluf.me/lildb) for simple db operations with very little boilerplate & its literally one class.
