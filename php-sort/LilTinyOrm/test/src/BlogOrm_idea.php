<?php

class BlogOrm extends \Tlf\LilOrm {

    /** if you didn't want the static functions right in here */
    use BlogOrm_Sql;

    static public function create_v1(){
        static::$ldb->create('blog',['id'=>'int PRIMARY KEY', 'title'=>'VARCHAR(200)','body'=>'VARCHAR(1500)']);
        // static::$ldb->exec('ADD FOREIGN KEY blog.id something something user_blogs.blog_id');
        // i like the return-a-string version better. bit cleaner
        //
        // plus you can do stuff prior to returning a string
        //
    }
    // public function create_v1(){
        // return <<<SQL
            // CREATE TABLE blog (id int PRIMARY KEY, title VARCHAR(200), body VARCHAR(1500))
        // SQL;
    // }
    static public function all_tables_ready(){
        return <<<SQL
            ADD FOREIGN KEY blog.id something something user_blogs.blog_id
        SQL;
        // or the ldb exec... no no
    }

    /**
     * do: `liltinyorm migrate v1 v2` in your terminal. Or maybe `vendor/bin/liltinyorm migrate v1 v2`
     */
    static public function migrate_v1_to_v2(){
        return <<<SQL
            ALTER TABLE blog ADD COLUMN tags VARCHAR(100) -- or something like that
        SQL;
    }

    static public function objectify($row){
        return new static($row['title'], $row['body'], $row['id']);
    }

    /** $pageId might better be a slug ... */
    static public function getBlogForPage($pageId){
        // define a query that maybe does a join? Idk. The objectifying will be relatively manual.
        //
        // The point here is to keep things well-organized and provide a minimal set of tools to remove some boilerplate
        //
        // So I don't know what I'd wanna do with a join. Maybe there's a nice way I can parse a join ... like provide a helper function for that
        //
        // Because if I'm getting a blog for a page, I want the user, the comments, replies ... everything.
        // I could do it in multiple queries, but more trips to the database is not what I want.  especially when its on a separate server
        // 
        // But that's dependent upon the performance of my particular application.
        // Example:
        //
        // and how does objectification work if different queries return different columns & stuff?
        $sql = 
        <<<SQL
            SELECT blog.*, page.*, author.* FROM blog 
                JOIN page ON 
                    page.blog_id = blog.id
                JOIN author ON
                    blog.author_id = author.id
            WHERE page.id = :page_id
        SQL;
        $stmt->bind(['page_id'=>$pageId]);
        $items = static::sort($stmt->exec()->fetchAll());
        $blog = static::objectify($items['blogs'][0]);

        // I could objectify these, too, if I want
        $blog->setUser($items['user']);
        $blog->setPage($items['page']);
    }

    public function __construct($title, $body, $id){
        $this->props = ['title'=>$title, 'body'=>$body, 'id'=>$id]; // idk... maybe maybe not
    }

    // then have a magic getter and setter
}
