<?php

namespace Tlf;

class LilOrm {

    static public \Tlf\LilDb $ldb;

    final static public function create($version="v1"){
        $function = "create_$version";
        $str = static::$function();
        if (is_string($str)){
            $this->ldb->exec($str);
        }
    }

    final static public function all_tables_ready($version="v1"){
        $function = "all_tables_ready_$version";
        $str = static::$function();
        if (is_string($str)){
            static::$ldb->exec($str);
        }
    }
    final static public function test($version="v1"){
        $function = "test_$version";
        $str = static::$function();
        if (is_string($str)){
            static::$ldb->exec($str);
        }
    }


}
