# Making it regex only


    ^ #start of line
        ([a-zA-Z\-\_0-9\.]+) # key regex
    \: # colon to separate from value
       # ([a-zA-Z]+:)? # Delimiter regex
        # value regex
    

- Correctly matches non-delimited keys & values
    - ([a-z]+):((?:(?:.|\n|\r)(?!^[a-z]+:))+)
- Correctly matches delimited keys & values
    - ([a-z]+):([A-Z]*:)((.|\r|\n)*)^:\2
- Matches everything correctly, BUT references are not right
    - (?:(?:([a-z]+):([A-Z]*:)((.|\r|\n)*)^:\2)|([a-z]+):((?:(?:.|\n|\r)(?!^[a-z]+:))+))
    - (?|(?:(?:([a-z]+):([A-Z]*:)((.|\r|\n)*)^:\2))|(([a-z]+):((?:(?:.|\n|\r)(?!^[a-z]+:))+)))
    
- Matches everything and gives me victory:

# Deconstruction
```regex
# non-delimiting regex
^([a-z]+) # key
 :
 ((?:
    (?:.|\n|\r) # characters we want
    (?!^[a-z]+:) # But NOT if those characters make up a key
 )+)

# Delimiting regex

^([a-z]+) # key
:
([A-Z]*:) # delimiter
((.|\r|\n)*) # characters we want
^:\2 # Ends with delimiter


# Caputure all (#2)
^([a-z]+): # key CP#1
    ((?:[A-Z]*:)? # delimiter, optional
        (?:\s*(\r?\n|$)) # whitespace, new line OR end of file (line?)
    ) # CP#2
    (    # value, CP#3
        (?:(?:
            (?:.|\n|\r) # characters we want
            (?!^[a-z]+:) # But NOT if those characters make up a key
        )+)          
    | # or
        
        ((.|\r|\n)*) # characters we want
        ^:\2 # Ends with delimiter

    )           # delimited value


# Caputre all (hopefully)
^([a-z]+): # key
    (    # Non-delimited value
        (?:(?:
            (?:.|\n|\r) # characters we want
            (?!^[a-z]+:) # But NOT if those characters make up a key
        )+)          
    | # or
        ([A-Z]*:) # delimiter
        ((.|\r|\n)*) # characters we want
        ^:\2 # Ends with delimiter

    )           # delimited value



# non-delimiting WITHOUT lookahead
 ^([a-z]+) # key
 :
 ((?:
    (?:.|\n|\r) # characters we want
 )+)
 (([a-z]+):|$)

 ```

 ^([a-z]+):((?:(?:.|\n|\r))+)(([a-z]+):|$)


Testing against this, to setup delimiting
```
key: value 1
nightmare:DELIM:
notakey:
obviously not a key
notakey:
:DELIM:
abc: value 2
new line
anotherkey: value 3
```



^([a-zA-Z\-\_0-9\.]+)\:((.|\n|\r)(?!^[a-zA-Z0-9]:))+



abc:value
abc2: value 
abc3:     value blah cat dog 3284790871345&^*(&^$_!@#$_*@#^_()*#@$!   
abc4:
one
two
three


abc.x-y_z.5:  x
   abc
fasdf
   blah

cat.dog:DELIM:
notakey1: value
notakey2: value
not.a.key: valuv vall
:DELIM: