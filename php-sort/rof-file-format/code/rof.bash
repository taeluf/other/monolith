#!/usr/bin/env bash

str="$(cat test.rof)"

declare -A ROF_CONF

state=""
stOpen="0"
stKey=1

curKey=""
curValue=""

function conf_load(){

    while read -r line; do
        key=$(echo "$line" | sed -E 's/^([a-zA-Z._-]+):.*/\1/')
        
        if [[ "$key" != "$line" ]];then
            if [[ -n "$curKey" ]];then
                # remove trailing whitespace from curValue

                # echo "#####${curValue:(-1)}"
                while [[ "${curValue:(-1)}" == " " || "${curValue:(-2)}" == "\n" ]];do
                    # echo "TRIM END"
                    if [[ "${curValue:(-1)}" == " " ]];then
                        curValue="${curValue:0:-1}"
                    else  
                        curValue="${curValue:0:-2}"
                    fi
                done
                
                # echo "#####${curValue:(-1)}"
                if [[ "${curValue:(-1)}" == "\\" ]];then
                    # echo "backslash!!!!!!"
                    curValue="${curValue:0:-1}"
                fi

                ROF_CONF["$curKey"]="$curValue"
                # echo "KEY:${curKey}"
            fi
            len="${#key}"
            len=$(( len + 1 ))
            value="${line:${len}}"

            while [[ "${value:0:1}" == " " ]];do
                value="${value:1}"
            done

            if [[ "${value:0:1}" == "\\" ]];then
                value=${value:1}
            fi
            # echo "##$key"
            # echo "--$value"
            curKey="$key"
            curValue="$value"
        else
            # echo "--$line--"
            if [[ "${line:0:1}" == "\\" ]];then
                line=${line:1}
            fi
            # echo "--$line--"
            # continue;
            if [[ "${line:-1}" == "\\" ]];then
                echo '################trim end'
                line="${line:0:-1}"
            fi
            curValue+="\n"
            curValue+=$line
        fi
    done <<< "$str"

    while [[ "${curValue:(-1)}" == " " || "${curValue:(-2)}" == "\n" ]];do
        # echo "TRIM END"
        if [[ "${curValue:(-1)}" == " " ]];then
            curValue="${curValue:0:-1}"
        else  
            curValue="${curValue:0:-2}"
        fi
    done
    # echo "#####${curValue:(-1)}"
    if [[ "${curValue:(-1)}" == "\\" ]];then
        # echo "backslash!!!!!!"
        curValue="${curValue:0:-1}"
    fi
    ROF_CONF["$curKey"]="$curValue"




}

function conf_get(){
    key="$1"
    # local -n OUT $2
    # echo "--------$key"
    OUT="${ROF_CONF["$key"]}"

    echo -e "$OUT"
}

function conf_set(){
    key="$1"
    val="$2"
    ROF_CONF["$key"]="$val"
}

## THIS IS THE ONLY PART THAT DOESNT WORK!!!!
function conf_save(){
    file="test-out.rof"
    : > $file
    out=""
    for key in "${!ROF_CONF[@]}"; do
        # echo "$key:::::"
        out="${key}: "
        # echo -e "$out" >> "$file"
        value="${ROF_CONF["$key"]}";
        if [[ "${value:0:1}" == " " ]];then
            value="\\${value}"
        fi
        if [[ "${value:(-1)}" == " " ]];then
            value="${value}\\"
        fi
        c=0
        value=$(echo -e "$value")
        while read -r line; do
            storeValue=""
            # echo "---$line---"
            # continue;
            if [[ $c -eq 0 ]];then
                c=1
                storeValue+="$line"
                echo -e "${key}: $line" >> $file
                continue;
            fi
            # if [[ "${line:0:3}" == "not" ]];then
                # line="\\$line"
            # fi
            if [[ "$line" =~ ^([a-zA-Z._-]+):.* ]];then
                line="\\$line"
            elif [[ "${line:0:1}" == "\\" ]];then
                line="\\$line"
            fi 
            # key=$(echo "$line" | sed -E 's/^([a-zA-Z._-]+):.*/\1/')
            storeValue+="$line"
            storeValue+=$(echo -e "\n")
            echo "$storeValue" >> $file
        done <<< "$value"
        echo "" >> $file
        # out+="$storeValue"


        # echo "$out" >> $file
        # echo -e "\n" > $file
    done
    # echo -e "$out" > $file
}

conf_load

conf_set a.one "I am a new value for a.one"

conf_save

echo "######################### CAT FILE ####################################"

cat test-out.rof

# echo "######################### ORIG FILE ####################################"

# cat test.rof
# echo "--a.one:$(conf_get a.one)"
# echo "--two-whoo:$(conf_get Another_two-whoo)"
# echo "--last.four:$(conf_get last.fourkey)"
# echo "--three:$(conf_get A.Key.three)"
