<?php

namespace ROF;

// require_once(__DIR__.'/../../../github/Tokenizer/class/Token.php');

class ROFToken extends \ROF\Token {


    public $properties = [];
    public $paramValue;
    public $delimiter;
    // protected $prop;


    protected function nextTokenFromRemainder($tokenName){
        return $this->nextToken($tokenName,'');
    }


    public function willEndBeforeParam(&$matches){
        if ((preg_match('/[\r\n]{1,}$/',$this->text)
                ||strlen($this->text)=='0'
            )
            &&
            preg_match('/^([a-zA-Z\.\-]{1,}\:)/',$this->remainderText)){
            return true;
        }
    }
    public function endBeforeParam($matches){
        $this->nextTokenFromRemainder('paramName');
    }

    public function willEndParamName(&$matches){
        if (preg_match('/^([a-zA-Z\.\-]{1,})$/',$this->text,$matches)
            &&preg_match('/^(\:{1,})[^\:]/',$this->remainderText)){
                return true;
        }
    }
    public function endParamName($matches){
        $paramName = $matches[1];
        $this->nextTokenFromRemainder('determineDelimiter');
    }
    public function willEndDetermineDelimiter(&$matches){
        if (preg_match('/^(\:{1,})$/',$this->text,$matches)
            &&preg_match('/^[^\:]/',$this->remainderText)){
                return true;
            }
    }
    public function endDetermineDelimiter($matches){
        $delimiter = $matches[1];
        $delimiter = substr($matches[1],1);
        if ($delimiter==''){
            $this->nextTokenFromRemainder('parseNormal');
        } else {
            $this->delimiter = $delimiter;
            $this->nextTokenFromRemainder('parseDelimited');
            // $newToken->delimiter = $delimiter;
            // $newToken->append($this->clearRemainderText());
        }
    }

    public function willEndParseNormal(&$matches){
        $meh = [];
        if (strlen($this->remainderText )===0
            ||$this->willEndBeforeParam($meh)
            ){
                return true;
        }
        // var_dump(substr($this->text,-10));
        // var_dump(substr($this->remainderText,0,10));
    }
    public function endParseNormal($matches){
        $propName = $this->prev->prev->text;
        $value = trim($this->text);
        // $prop = [$propName=>$value];
        $this->properties[$propName] = $value;
        // var_dump($prop);
        // var_dump($this->remainderText);
        // $this->remainder = ["\n"]+$this->remainder;
        $this->nextToken('beforeParam',substr($this->text,-1));
        // var_dump($matches);
        // var_dump($this->prev->text);
        // var_dump($this->prev->delimiter);
        // var_dump($this->prev->prev->text);
        // var_dump($this);
        // exit;
        //this is where I save the param name & the value, then create a new token which is the next param.
        // so it should start at 'willEndBeforeParam' with the remaining text

    }

    public function willEndParseDelimited(&$matches){
        $delimiter = $this->prev->delimiter;
        // var_dump($delimiter);
        if (preg_match('/^'.$delimiter.'[\r\n]/',$this->remainderText)){
            return true;
        }
    }
    public function endParseDelimited($matches){
        $propName = $this->prev->prev->text;
        $value = trim($this->text);
        // $prop = [$propName=>$value];
        // var_dump($prop);
        $this->properties[$propName] = $value;
        $this->nextTokenFromRemainder('beforeParam');
    }

    public function asArray(){
        $properties = $this->next ? $this->next->asArray() : [];
        $properties = array_merge($this->properties,$properties);
        
        foreach ($this->children as $child){
            $properties = array_merge($properties,$child->properties);
        }
        
        return $properties;
    }

    // public function willEndStart(&$matches){
    //     if (preg_match('/^([a-zA-Z\.]{1,})(\:{1,})([^\:])$/',$this->text,$matches)
    //         ||preg_match('/\n([a-zA-Z\.]{1,})(\:{1,})([^\:])$/',$this->text,$matches)){
    //         return true;
    //     }
    //     return false;
    // }
    // public function endStart($matches){
    //     $this->delimiter = substr($matches[2],1);
        
    //     // var_dump($matches);
    //     $valueToken = $this->nextToken('value',$matches[3]);

    //     $this->properties[$matches[1]] = $valueToken->paramValue;

    //     echo '//////';
    //     var_dump($this->properties);
    //     echo '---';
    //     $next = $this->nextToken('start','');

    //     var_dump('stop');


    //     //var_dump($valueToken);


    //     return;
    //     exit;


    //     $this->delimiter = substr($matches[2],1);
    //     var_dump($this->delimiter);
    //     // var_dump($matches);
    //     // exit;
    //     $key = $matches[1];
    //     $valueToken = $this->nextToken('value','');
    //     $this->properties[$key] = trim($valueToken->text);
    //     // var_dump($this->properties);
    //     // var_dump($this->remainder);
    //     // exit;
    //     // var_dump($this->text);
    //     // exit;
    // }
    // public function willEndValue(&$matches){
    //     if ($this->prev->delimiter==''
    //         &&preg_match('/([^\n]*\n)([a-zA-Z\.])(*\:{1,})([^\:])$/',$this->text,$matches)
    //         ||strlen($this->prev->delimiter)>0
    //         &&preg_match('/([^\n]*\n)'.$this->prev->delimiter.'$/',$this->text,$matches)){

    //             return true;
    //     }
    //     return false;
    //     if (preg_match('',$this->text,$matches)){
    //         return true;
    //     }
    // }

    // public function endValue($matches){
    //     $value = $matches[1];
    //     $this->paramValue = trim($value);
    //     $this->prev->remainder = $this->remainder;
    //     $this->remainder = [];
    //     // var_dump($matches);
    //     // $this->nextToken('nextThing','');
    //     // $this->remainder = [];
    //     // exit;
    //     // exit;
    // }



    // public function wilssslEndValue(&$matches){
    //     // var_dump($this->text);
    //     // exit;
    //     if (preg_match('/\n([a-zA-Z\.]*)'.$this->prev->delimiter.'[^:]$/',$this->text,$matches)){
    //         var_dump($this->prev->delimiter);
    //         var_dump($this->text);
    //         // exit;
    //         return true;
    //     }
    //     return false;
    // }
    // public function endzzzValue($matches){
    //     var_dump($matches);
    //     exit;
    //     $propertyToken = $this->nextToken('start','',':');
    //     // var_dump($this->text);
    //     // var_dump($matches);
    //     // exit;
    //     // $this->text = '';
    //     // $this->remainder = [];
    //     // $this->next('value',$this->text);
    // }


}