<?php

namespace ROF;

class Encoder {

    public function encode($array){
        $output = '';
        foreach ($array as $key=>$value){
            $output .= "{$key}: ".$value."\n";
        }
        return $output;
    }
}