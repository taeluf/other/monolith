<?php

/**
 * This is exported from a docblock
 *
 * @attr I think this tag is exported too
 * @export(Dumb.Docblock.ExampleExport)
 *
 * Stuff below the export tag is not exported.
 * another @ export will overwrite the one preceding it ... it's awkward
 */
function this_is_a_dumb_example(){
    echo "The Docblock Exports extension just scrapes for docblocks & has zero awareness of the code. It just strips docblocks";
}


// @export_start(Dumb.Codeblock.ExampleExport)
function you_can_export_a_whole_block_of_code(){
    echo "i could write real sample code, but why bother? lol";
}
// @export_end(Dumb.Codeblock.ExampleExport)
