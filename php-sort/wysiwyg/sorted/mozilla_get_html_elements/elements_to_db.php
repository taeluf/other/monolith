<?php

$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");


$dataFile = __DIR__.'/element_data.json';
$allData = json_decode(file_get_contents($dataFile),TRUE);



$statementString = "INSERT INTO html_elements(tag_name,description,group_name) VALUES ";

$valuesList = [];
$i = 0;
foreach ($allData as $groupName=>$groupData){
    foreach ($groupData['elements'] as $tagName=>$data){
        $i++;
        $description = $data['description'];
        $valuesList[':tag_name'.$i] = $tagName;
        $valuesList[':description'.$i] = $description;
        $valuesList[':group_name'.$i] = $groupName;
        $valuesInsertStrings[] = '(:tag_name'.$i.',:description'.$i.',:group_name'.$i.')';
    }
}
$statementString .= implode(",\n",$valuesInsertStrings);

$statement = $pdo->prepare($statementString);
$statement->execute($valuesList);
print_r($statement->errorInfo());
echo $statementString;

print_r($valuesList);