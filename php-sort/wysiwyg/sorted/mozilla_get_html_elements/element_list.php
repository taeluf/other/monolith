<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {
  box-sizing: border-box;
}

.container {
    display:flex;
    flex-wrap:wrap;
    flex-direction:row;
    justify-content:flex-start;
    align-items:stretch;
    max-width:1000px;
}

.left {order:1; background:red; flex-basis:100%; }
.middle {order:2; background: green;flex-basis:100%; }
.right {order:3; background:yellow;flex-basis:100%; }

@media screen and (min-width:600px) {
   .container {
       flex-wrap:nowrap;
   } 

    .left {
        flex-basis:350px;
        order:1;
    }
    .middle {
        flex-basis:1;
        order:2;
    }
    .right {
        flex-basis:300px;
        order:3;
    }
    #elementsBoxContainer {
        position:relative;
    }
    #elementsBox {
        position:fixed;
        top:20px;
    }
}


.element {
    border: 1px solid black; 
    margin:4px;
    padding:4px;
    display:inline-block;
}

.group {

    height:4em;
    width:49%;
    border: 1px solid black;
    display:inline-block;

}


</style>
</head>
<body>
<h1>HTML Elements</h1>

<?php
$data = json_decode(file_get_contents(__DIR__.'/element_data.json'),TRUE);
$groups = $data;
$groupNames = array_keys($groups);

$extraGroups = ['HTML Sections', 'Structured Content', 'Meta Data', 'Resources','Headers', 'Lists', 'Common Text Markup', 'Other, Common', 'Other, Rare'];
foreach ($extraGroups as $groupName){
    if (!in_array($groupName,$groupNames))$groupNames[] = $groupName;
}

?>

<div class="container">
<div class="left" style="background-color:#bbb;">
    <?php
  


  $groupHtml = array_map(function($groupName){
    return '<div onclick="showElementsOfGroup(event);" id="gr_'.$groupName
    .'" class="group" ondragover="event.preventDefault();" ondrop="dropElementIntoGroup(event);">'.$groupName.'</div>';
  },
  $groupNames);

  echo implode($groupHtml);
?>
</div>


<!--- LEFT COLUMN ENDS -->
    <div class="middle"></div>

    <!-- RIGHT COLUMN BEGINS -->
  <div class="right" style="background-color:#aaa;">
    <div id="elementsBoxContainer">
        <div id="elementsBox"></div>
    </div>
  </div>
 

<script>


function showElementDescription(event){
    var element = event.target;
    var description = element.getAttribute("data-description");
    alert('element '+element.tagName+': '+description);
}

function showElementsOfGroup(event){
    let groupName = event.target.id.substring(String('gr_').length);
    let request = new Request('/wyg/get_elements_from_group/?group='
        +encodeURIComponent(groupName)
        );
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.json();
            }
        })
        .then(json => {
            console.log(json);
            let elementsBox = document.getElementById("elementsBox");
            elementsBox.innerText = '';
            var key;
            for (key in json){
                let elementData = json[key];
                let elementName = key;
                let elementDescription = elementData.description;
                let element = document.createElement('div');
                element.setAttribute('draggable',true);
                element.addEventListener('dragstart',event => dragGroupedElement(event));
                element.addEventListener('click',event=>showElementDescription(event));
                element.innerText = elementName;
                element.setAttribute('id','el_'+elementName);
                element.setAttribute('class','element');
                element.setAttribute('data-description',elementDescription);
                elementsBox.appendChild(element);
            }
            
        });
}


function dragGroupedElement(event){
    let elementName = event.target.innerText;
    event.dataTransfer.setData("elementName", elementName);
    console.log(elementName);
}


function setGroupOfElement(elementName,groupName){

    let request = new Request('/wyg/set_group_on_element/?group='
        +encodeURIComponent(groupName)
        +'&element='
        +encodeURIComponent(elementName));
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.text();
            }
        })
        .then(blob => {
            console.log(blob+"");
        });
}


function dropElementIntoGroup(event){

    let groupDiv = event.target;
    let groupName = groupDiv.id.substring(String('gr_').length);
    let elementName = event.dataTransfer.getData("elementName");
    let originalNode = document.getElementById("el_"+elementName);
    
    originalNode.parentNode.removeChild(originalNode);
    let node = document.createElement('span');
    node.innerText = elementName+", ";
    groupDiv.appendChild(node);

    setGroupOfElement(elementName,groupName);
}

function startElementDrag(event,element){
    event.dataTransfer.setData("elementName", event.target.innerText);
    
}

</script>

</div>
</div>

</body>
</html>







