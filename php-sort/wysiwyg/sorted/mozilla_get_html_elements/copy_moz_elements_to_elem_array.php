<?php

$dataFile = __DIR__.'/element_data.json';
$data = json_decode(file_get_contents($dataFile),TRUE);

$backupFile = __DIR__.'/backups/element_data-backup-'.microtime(TRUE).'.json';
file_put_contents($backupFile,json_encode($data));

$mozData = json_decode(file_get_contents(__DIR__.'/mozilla_sorted_elements.json'),TRUE);

$newData = [];
$mozCopy = $mozData;
foreach ($mozCopy as $groupName=>$groupData){
    $newList = [];
    foreach ($groupData['elements'] as $index=>$elementData){
        if (is_string($index)){
            $newData[$index] = $elementData;
            continue;
        }
        $elementName = substr($elementData['elementName'],1,-1);
        if ($elementName=='h1>, <h2>, <h3>, <h4>, <h5>, <h6'){
            for ($i=1;$i<=6;$i++){
                $newList['h'.$i] = ['tagName'=>'h'.$i,'description'=>$elementData['description']];
            }
        } else {
            $newList[$elementName] = ['tagName'=>$elementName,'description'=>$elementData['description']];
        }
    }

    $newData[$groupName] = 
    [
        'groupName' => $groupName,
        'groupDescription' => $groupData['groupDescription'] ?? '',
        'elements'=>$newList,

    ];
}
print_r($newData);

file_put_contents($dataFile,json_encode($newData));