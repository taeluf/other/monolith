<div id="errthing_output">

</div>
<div id="errthing">
    <?=file_get_contents(__DIR__.'/mozilla_docs_elements_list.html');?>

</div>


<script type="text/javascript">

    let errthing = document.getElementById("errthing");
    let errthing_output = document.getElementById("errthing_output");
    h2Nodes = document.querySelectorAll("h2");
    

    var p;
    var allGroupData = {};
    for (node of h2Nodes){
        var data = {elements:[]};
        data.groupName = node.innerText.replace(/Section$/,'');
        
        p = node.nextSibling.nextSibling;
        if (p.tagName=='P'){
            data.groupDescription = p.innerText;
        }
        let table = p
        while (table.tagName!='TABLE'){
            table = table.nextSibling
        }
        let tds = table.querySelectorAll("td");
        let i = 0;
        let elementData = {};
        for (i<tds.length;element=tds[i],description=tds[i+1];i=i+2){
            elementData = {elementName:element.innerText,description:description.innerText};
            data.elements.push(elementData);
        }
        
        errthing_output.innerText += JSON.stringify(data);
        allGroupData[data.groupName] = data;
    }

    console.log(JSON.stringify(allGroupData));

</script>