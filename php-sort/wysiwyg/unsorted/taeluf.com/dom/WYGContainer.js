
class WYGContainer extends WYG.Wire {

    __construct(){
        this.objects = new WeakMap();
        this.keys = {};
    }
    __attach(){
        this.parser = WYG.Parser;
    }
    ondragover(event){
        event.preventDefault();
        event.stopPropagation();
    }
    ondrop(event){
        event.preventDefault();
        event.stopPropagation();
        let objKey;
        if (objKey = event.dataTransfer.getData('objKey')){
            //dragging a node that is already in the editor
            const obj = this.objFor(objKey);
            obj.node.parentNode.removeChild(obj.node);
            // console.log(obj.node);
            this.addNode(obj,this.deriveTarget(event));
            return;
        }
        //dragging from the elements panel / control panel
        const dt = event.dataTransfer;
        const target = this.deriveTarget(event);
        const newNode = this.parser.htmlAsNode(dt.getData('code'));
        const obj = WYG.Node.wireNode(newNode);
        const settings = JSON.parse(dt.getData('settings'));
        for (const key in settings) obj[key] = settings[key];

        this.addNode(obj,target);
    }

    onclick(event){
        event.preventDefault();
    }
    addCode(code,target=undefined,settings=undefined){
        const newNode = this.parser.htmlAsNode(code);
        const obj = WYG.Node.wireNode(newNode);
        if (settings !== undefined)for (const key in settings) obj[key] = settings[key];
        this.addNode(obj,target);
    }
    addNode(obj,target=undefined){
        if (target==undefined)target = this;
        const node = obj.node;
        if (target.node.classList.contains("WYGContainer")
            ||target.isContainer
                &&(!obj.isContainer||target.isSuperContainer)){
                    console.log(target);
            target.node.appendChild(obj.node);
        } else if (obj.isContainer
            &&target.isSuperContainer){
                target.node.appendChild(obj.node);
        } else {
            this.addNode(obj,RB.WireContext.fromNode(target.node.parentNode));
            // target.node.parentNode.appendChild(obj.node);
        }
    }

    deriveTarget(event){
        let node = event.target;
        const obj = RB.WireContext.fromNode(node);
        return obj;
        // const obj = RB.WireContext.fromNode(node);
        // console.log(obj);
        // console.log('derive target done');
        // return node;

        // if (node.classList.contains("WYGContainer")){
        //     console.log("container");
        //     return node;
        // } else if (node.classList.contains('wyg-wrapper')){
        //     return node.firstChild;
        // } else if (node.nodeName==='#text'){
        //     node = node.parentNode;
        // } else if (node.tagName=='INPUT'){
        //     node = node.parentNode;
        // } else if (node.parentNode.classList.contains('wyg-wrapper')) {
        //     return node;
        // }

        // // while (node!==undefined
        // //         &&node!==null
        // //         &&!this.editor.isContainer(node)){
        // //     node = node.parentNode;
        // // }

        // return node;
    }

    keyFor(obj){
        if (this.objects.has(obj))return this.objects.get(obj);
        else {
            const uniqueId = this.objects.length 
                        + '-'+window.performance.now() 
                        + '-'+Math.random();
            this.objects.set(obj,uniqueId);
            this.keys[uniqueId] = obj;
            return uniqueId;
        }
    }
    objFor(key){
        return this.keys[key] ?? undefined;
    }
}

WYGContainer.autowire();