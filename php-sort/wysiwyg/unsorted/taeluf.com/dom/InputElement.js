
class InputElement extends WYG.Wire {

    __attach(){
        this.context = "wyg";
        this.code = this.node.getAttribute('data-code');
        this.settings = this.node.getAttribute('data-settings');
        this.node.draggable = true;
    }
    _wyg(){
        return ['WYGContainer'];
    }

    ondblclick(event){
        // console.log(this.wyg);
        this.wyg.addCode(this.code,null,this.settings);
    }

    ondragstart(event){
        // console.log('dragstart');
        event.dataTransfer.setData('code',this.code);
        event.dataTransfer.setData('settings',this.settings);
    }
    
}

InputElement.autowire();