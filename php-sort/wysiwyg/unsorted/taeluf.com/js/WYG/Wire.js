
if (typeof WYG === 'undefined')var WYG = {};
WYG.Wire = class extends RB.WireContext {

    attach(){
        super.attach();
        // console.log(this);
        this.context = "wyg";
        // Object.defineProperty(this,'wyg',{
        //     get:function(){
        //         return this.get('WYGContainer');
        //     }
        // });
    }
    _wyg(){
        return ['WYGContainer'];
    }

    key(){
        const wyg = this.wyg;
        return wyg.keyFor(this);
    }
}