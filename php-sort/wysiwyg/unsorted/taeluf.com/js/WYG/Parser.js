
var WYG = WYG ?? {};

WYG.Parser = class {

    static fields(templateCode,valuesCode=undefined){
        const fieldReg = /\{\{[^\}\n]*\}\}/g;
        const fieldStrings = templateCode.match(fieldReg) || [];
        const fields = [];
        for (const fieldString of fieldStrings){
            const field = new Wyg.Field(fieldString);
            fields[field.key] = field;
        }

        if (typeof valuesCode==typeof ""){
            const valueFields = this.fields(valuesCode);
            for (const valueField of valueFields){
                const templateField = fields[valueField.key];
                templateField.setValue(valueField.value);
            }
        }
        return fields;
    }
    
    static htmlAsNode(html,alwaysWrap=false,wrapWith=undefined){
        const allowWrap = (typeof wrapWith === typeof "string"||alwaysWrap);
        const wrapper = document.createElement(wrapWith || 'span');
        wrapper.innerHTML = html;
        let count = 0;
        let node = wrapper;
        for (const node of wrapper.childNodes){
            if (node.outerHTML==null){
                let value = node.nodeValue.trim();
                if (value!==''){
                    count++;
                }
            } else {
                count++;
            }
        }
        if ((count==0||count>1)&&allowWrap
            ||alwaysWrap===true){
            return wrapper;
        } else if (count===0){
            throw "No content found in the html: \n"+html;
        } else if (count===1){
            return wrapper.children[0];
        } else {
            throw "Several top level nodes exist in the html. "
                    +"\nPass 'span', 'div', or another node tag name as second argument to htmlAsNode to get such a node with your html as its content."
                    +"\nSubmitted Html is: \n"
                    +html;
        }
    }

}