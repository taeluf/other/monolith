<?php

$this->addStyleUrl('/wyg/Layout.css');
ob_start();
require($_SERVER['DOCUMENT_ROOT'].'/../dev/internal/js-autowire/framework.js.php');
$this->addScriptCode(ob_get_clean());

$this->addScriptUrl("/wyg/js/WYG.js");
$this->addScriptUrl("/wyg/js/WYG/Wire.js");
$this->addScriptUrl("/wyg/dom/InputElement.js");
$this->addScriptUrl("/wyg/dom/WYGContainer.js");
$this->addScriptUrl("/wyg/js/WYG/Parser.js");
$this->addScriptUrl("/wyg/js/WYG/Node.js");

?>

<div id="WYG">

    <section id="input">
        <h1>Inputs</h1>
        <button class="InputElement" data-code="<h1>Header</h1>" data-settings='{"isContainer":false}'>Header</button>
        <button class="InputElement" data-code="<section style='min-height:20px;background:blue;'></section>" data-settings='{"isContainer":true,"isSuperContainer":false}'>Section</button>
        <button class="InputElement" data-code="<p>Lorem Ipsum Dolor</p>" data-settings='{"isContainer":true,"isSuperContainer":false}'>Paragraph</button>
        <button class="InputElement" data-code="<strong>Much Weight</strong>" data-settings='{"isContainer":false,"isSuperContainer":false}'>Strong</button>
        <button class="InputElement" data-code="<label>{{Input Name}}<br><input type='text' name='@Input Name'></label></strong>" data-settings='{"isContainer":false,"isSuperContainer":false}'>Text Input</button>
    </section>
    <section id="preview">
        <h1>How it looks</h1>
        <p>Clearly, this page is under development.</p>
        <div class="WYGContainer">
        </div>
    </section>
    <!-- <section id="output">
        <h1>Output</h1>
    </section> -->
</div>
