
Wyg.Editor = class {


    constructor(template){
        this.template = template;
        this.map = new Wyg.Map();
    }

    replaceAttributeCode(node){
        this.map.push('node',node);
        this.map.push('code',node.outerHTML,'node',node);

        for (const attribute of node.attributes){
            const name = attribute.localName;
            const code = attribute.value;
            
            if (!Wyg.Field.isCode(code))continue;

            
            const field = new Wyg.Field(attribute.value);
            this.map.push('field',field,'node',node);
            // const field = this.map.fieldFromCode(attribute.value);

            if (name=='wyg'){
                node.attributes.removeNamedItem(name);
                const str = field.value;
                const attrNode = Wyg.htmlAsNode('<span '+str+'></span>');

                for (const newAttribute of attrNode.attributes){
                    node.setAttribute(newAttribute.localName,newAttribute.value);
                }
            } else {
                node.setAttribute(name,field.value);
            }
        }

    }
    replaceTextNodes(node){
        const ref = this.map.refFromNode(node);
        for (const child of node.childNodes){
            if (child.nodeValue===undefined
                ||!(/\{\{[^\}\n]*\}\}/g.test(child.nodeValue))){
                    continue;
            }
            const template = new Wyg.Template(child.nodeValue);
            for (const key in template.fields){
                const valuesValue = this.template.valueFromKey(key);
                let currentValue = valuesValue;
                if (valuesValue=='default'){
                    currentValue = this.template.valueFromKey(key);
                }
                child.nodeValue = currentValue;
            }
        }    
    }
    replaceChildCode(node){
        for (const child of node.children){
            if (child.outerHTML===undefined)continue;
            this.replaceCode(child);
        }
    }
    replaceCode(node){
        this.replaceAttributeCode(node);
        this.replaceTextNodes(node);
        this.replaceChildCode(node);
    }

    createNode(){
        console.log('create node');
        const template = this.template.copy();
        const code = template.code;
        const node = Wyg.htmlAsNode(code,false,'span');
        this.map.reset();
        console.log('replace code');
        this.replaceCode(node);
        console.log('code replaced');
        console.log(this.map);


        console.log('end create node');
        return node
    }

    
    // createNodeList(node){

    //     let list = [];
    //     let copy = node.cloneNode();
    //     copy.innerText = '';
    //     copy.innerHTML = '';
    //     let newMatches = copy.outerHTML.match(/\{\{[^\}\n]*\}\}/g) || [];
    //     let keys = [];
    //     for (let match of newMatches){
    //         if (match==null||match.trim().length<1)continue;
    //         let field = new Wyg.Field(match);
    //         let key = field.key;
    //         keys.push(key);
    //     } 
    //     let ref = {};
    //     ref.node = node;
    //     ref.keys = keys;
    //     list.push(ref);
    //     for (let child of node.childNodes){
    //         let text = '';
    //         let matches = [];
    //         if (child.outerHTML!==undefined){
    //             list = list.concat(this.createNodeList(child));
    //         } else {
    //             let childText = child.nodeValue;
    //             let newMatches = childText.match(/\{\{[^\}\n]*\}\}/g);
    //             matches = matches.concat(newMatches);
                
    //         }
    //         let keys = [];
    //         for (let match of matches){
    //             if (match==null||match.trim().length<1)continue;
    //             let field = new Wyg.Field(match);
    //             let key = field.key;
    //             keys.push(key);
    //         }
    //         let ref = {};
    //         ref.node = node;
    //         ref.keys = keys;
    //         list.push(ref);
    //     }
    //     return list;
    // }
    // createMap(node){
    //     let list = this.createNodeList(node);
    //     console.log('list::');
    //     console.log(list);
    //     return;
    //     let map = [];
    //     for (let ref of list){
    //         let nodeRef = this.refFromNode(ref.node,map);
    //         nodeRef.keys = nodeRef.keys.concat(ref.keys);
    //     }
    //     return map;
    // }
};