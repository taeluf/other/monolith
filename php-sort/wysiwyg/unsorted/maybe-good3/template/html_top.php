<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    <?=file_get_contents(__DIR__.'/styles.css');?>
</style>
<script type="text/javascript">
    <?=file_get_contents(__DIR__.'/../js/Wyg.js');?>
    <?=file_get_contents(__DIR__.'/../js/HtmlParser.js');?>
    <?=file_get_contents(__DIR__.'/../js/Entry.js');?>
    <?=file_get_contents(__DIR__.'/../js/Template.js');?>
    <?=file_get_contents(__DIR__.'/../js/Editor.js');?>
    <?=file_get_contents(__DIR__.'/../js/other.js');?>

</script>
</head>
<body>