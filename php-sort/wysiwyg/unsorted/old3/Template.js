

Wyg.Template = class {

    declareClassParams(){
        this.entries = [];
        this.valueEntries = [];
        this.templateHtml = '';
        // this.reservedKeys = ['TemplateName'];
    }
    constructor(templateHtml){
        this.declareClassParams();
        this.initializeClass(templateHtml);

    }
    initializeClass(templateHtml){
        this.templateHtml = templateHtml;
        const variablesReg = /\{\{[^\}\n]*\}\}/g;
        let nearJsonList= templateHtml.match(variablesReg);
        let entryList = this.parseEntries(templateHtml);
        this.valueEntries = this.getValueEntries(entryList);
        this.entries = this.getVariableEntries(entryList);
    }

    getFinalHtml(){
        var html = this.templateHtml;
        for (let key in this.valueEntries){
            let valueEntry = this.valueEntries[key];
            html = html.replace(valueEntry.entryString,'');
        }
        html = html.trim();
        for (let key in this.entries){
            let variableEntry = this.entries[key];
            html = html.replace(variableEntry.entryString,variableEntry.value || '');
        }
        return html;
    }

    getEditorHtml(){
        var html = this.templateHtml;
        var node = Wyg.htmlInSpan(html);
        console.log(html);
        console.log(node);
        console.log('keys in node... getEditorHtml()');
        let editorHtml = '';
        for (let child of node.childNodes){
            let copy = child.cloneNode();
            copy.innerHTML = '';
            console.log(child);
            console.log(copy);
            let entries = this.parseEntries((copy.outerHTML || copy.nodeValue));
            for (let entry of entries){
                html = html.replace(entry.getEditableValue());
            }
        }

        console.log('html to come...');
        console.log(html);


        for (let key in this.valueEntries){
            let valueEntry = this.valueEntries[key];
            html = html.replace(valueEntry.entryString,valueEntry.getEditableValue());
        }
        html = html.trim();
        for (let key in this.entries){
            // console.log('replace a thing:'+key);
            let variableEntry = this.entries[key];
            html = html.replace(variableEntry.entryString,variableEntry.getEditableValue());
        }
        return html;
    }

    getControlsContainer(){
        let container = document.createElement('span');
        for (let key in this.entries){
            let entry = this.entries[key];
            container.appendChild(entry.getControlsNode());
        }
        return container;
    }

    getValueEntries(entriesList){
        let valueEntries = [];
        for (let entry of entriesList){
            if (entry.separator!='=')continue;
            valueEntries[entry.key] = entry;
        }
        return valueEntries;
    }
    getVariableEntries(entriesList){
        let values = [];
        let variables = [];
        let finalList = [];
        for (let key in entriesList){
            let entry = entriesList[key];
            if (entry.separator=='='){
                if (values[entry.key]!==undefined){
                    throw "Value for key '"+entry.key+"' has already been set. You can only set a value once for a key.";
                }
                values[entry.key] = entry;
            } else {
                if (variables[entry.key]!==undefined){
                    throw "Entry key '"+entry.key+"' has already been set. You must pick a new name for '"+entry.entryString+"'";
                }
                variables[entry.key] = entry;
            }
        }
        for (let key in variables){
            let variable = variables[key];
            key = variable.key;
            let finalEntry = variable;
            if (values[key]===undefined){}
            else finalEntry.value = values[key].value;
            finalList[key] = finalEntry;
        }
        return finalList;
    }
    
    parseEntries(templateHtml){
        console.log('parseEntries');
        console.log(templateHtml);
        let entriesReg = /\{\{[^\}\n]*\}\}/g;
        let entryStrings = templateHtml.match(entriesReg);
        let entries = [];
        for (let entryString of (entryStrings || [])){
            let entry = new Wyg.Entry(entryString);
            entries.push(entry);
        }
        return entries;
    }

    get(key){
        return (this.entries[key] || this.valueEntries[key]).value;
    }


};