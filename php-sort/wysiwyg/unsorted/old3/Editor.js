

Wyg.Editor = class {

    static enable(node,template,controlPanel){
        this.editorNode = node;
        this.template = template;
        this.controlPanel = controlPanel;

        node.innerHTML = template.getEditorHtml();
        node.addEventListener('click', Wyg.Editor.nodeClickedEvent.bind(Wyg.Editor));
    }

    static nodeClickedEvent(event){
        console.log('node clicked');
        console.log(event.target);
        console.log('-----');
        const clickedNode = event.target;
        let editableNode = this.getEditableNode(clickedNode);
        let template = new Wyg.Template(editableNode.outerHTML);
        let editControlsContainer = template.getControlsContainer();
        this.controlPanel.innerHTML = '';
        this.controlPanel.appendChild(editControlsContainer);
    }

    static getEditableNode(clickedNode){
        return clickedNode;
        let parentNode = clickedNode.parentNode;
        if (parentNode===this.editorNode){
            console.log('clicked');
            return clickedNode;
        }
        let nodeTemplate = new Wyg.Template(clickedNode.outerHTML);
        if (nodeTemplate.valueEntries.length<=3
            &&clickedNode.children.length<=1){
                console.log('parent');
            return parentNode;
        }
        console.log('clickedNode');
        console.log(nodeTemplate);
        console.log(clickedNode.children);
        return clickedNode;
    }
};