

Wyg.Entry = class {


    constructor(entryString){
        let parts = this.getParts(entryString);
        for (let key in parts){
            this[key] = parts[key];
        }
        
    }

    getParts(entryString){
        const splitKeyReg = /\{\{([^\=\}\:\n]*)([\=\:]?)([^\}]*)\}\}/;
        const parts = entryString.match(splitKeyReg);
        let key = parts[1];
        let separator = parts[2];
        let entryData = parts[3];
        let value = null;
        let type = null;
        
        if (key=="placeholder"){
            value = "abcdefg";
            type = "text";
        } else if (separator=='='){
            type = 'text';
            value = entryData;
        } else if (separator==':'){
            const splitTypeReg = /([^\=\n]*)(\=)?([^\n]*)$/;
            const typeValueParts = entryData.match(splitTypeReg);
            type = typeValueParts[1];
            value = typeValueParts[3];
        } else if (separator==''){
            type = 'text';
            value = null;
        }

        return {
            "key":key,
            "separator":separator,
            "type":type,
            "value":value,
            "entryString":entryString
        };
    }

    getControlsNode(){
        let node = document.createElement('input');
        node.setAttribute('type','text');
        node.setAttribute('placeholder',this.key);
        return node;
    }

    getEditableValue(){
        if (this.separator=='='){
            return '';
        } else if (this.type.trim()=='children'){
            return '<span class="wyg_container_node">{{'+this.key+'}}</span>';
        } else if (this.value==''||this.value==null){
            return '{{'+this.key+'}}';
        } else {
            return this.value;
        }
    }


    parseValuesFromHtml(templateHtml){
        const valuesReg = /\{\{[^\=\}]*\=[^}]*\}\}/g;
        let valuesList = templateHtml.match(valuesReg);
        let cleanValuesList = {};
        for (let jsonIsh of valuesList){
            let splitLoc = jsonIsh.indexOf('=');
            let key = jsonIsh.substr(2,splitLoc-2);
            let value = jsonIsh.substr(splitLoc+1,jsonIsh.length-splitLoc-3);
            cleanValuesList[key] = value;
        }
        console.log(cleanValuesList);
        return cleanValuesList;
    }

    parseNearJsonArray(nearJsonArray){
        const variables = {};
        for (let nearJson of nearJsonArray){
            let json = this.parseNearJson(nearJson);
            for (let origKey in json){
                let key = origKey;
                let counter = 2;
                if (variables[key]!==undefined){
                    throw "key '"+key+"' has already been set in the template and cannot be set again.";
                }
                // while (variables[key]!==undefined){
                //     key = origKey+""+counter;
                //     counter++;
                // }
                let variable = this.fillOutVariable({'type':json[origKey], 'nearJson':nearJson, 'key':key});
                variables[key] = variable;
                
            }
        }
        // console.log('variables:');
        // console.log(variables);
        return variables;
    }
    parseNearJson(nearJson){
        const keyTypeReg = /^\{\{([^\:]*)\:([^\}]*)\}\}$/;
        const keyOnlyReg = /\{\{([^\:]*)\}\}/;
        let jsonString = '';
        if (keyTypeReg.test(nearJson)){
            jsonString = nearJson.replace(keyTypeReg,'{"$1":"$2"}');
        } else if (keyOnlyReg.test(nearJson)){
            jsonString = nearJson.replace(keyOnlyReg, '{"$1":"text"}');
        }
        let json = JSON.parse(jsonString);
        return json;
    }
    fillOutVariable(variableData){
        if (this.values[variableData.key]!=undefined){
            variableData.value = this.values[variableData.key];
        } else if (variableData.type.substr(0,1)===':'){
            variableData.value = variableData.type.substr(1);
            variableData.type = 'text';
        }
        variableData.editorReplacement = this.parseEditorReplacement(variableData);
        return variableData;
    }
};