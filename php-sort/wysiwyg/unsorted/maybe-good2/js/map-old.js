
Wyg.Map = class {

    constructor(map = []){
        this.map = map;
        // const exampleMap = {
        //     "node":node,
        //     "key":"some_key",
        //     "field":"field",
        //     "code":"some_html",
        // };
    }
    
    getRef(mapKey,mapValue){

        for (const refObj of this.map){
            if (refObj.hasOwnProperty(mapKey)
                &&refObj[mapKey]===mapValue){
                    return refObj;
            }
        }

        const ref = {};
        ref[mapKey] = mapValue;
        this.map.push(ref);
        return ref;
    }

    setNode(node,siblingKey,siblingValue){
        this.set('node',node,siblingKey,siblingValue);
    }
    setKey(key,siblingKey,siblingValue){
        this.set('key',key,siblingKey,siblingValue);
    }
    setField(field,siblingKey,siblingValue){
        this.set('field',field,siblingKey,siblingValue);
    }    
    setCode(code,siblingKey,siblingValue){
        this.set('code',code,siblingKey,siblingValue);
    }
    set(insertKey,insertValue,refKey,refValue){
        let ref = this.getRef(refKey,refValue);
        //console.log(ref);
        ref[insertKey] = insertValue;
    }

    from(desiredKey,fromKey,fromValue){
        const ref = this.getRef(fromKey,fromValue);
        if (ref!=null&&ref.hasOwnProperty(desiredKey))return ref[desiredKey];
        
        if (desiredKey=='field'
            &&fromKey=='code'){
            const field = new Wyg.Field(fromValue);
            this.setField(field,fromKey,fromValue);
            return field;
        }
    }

    fieldsFromNode(node){

    }

    fromNode(lookingFor,fromValue){
        return this.from(lookingFor,'node',fromValue);
    }
    fromKey(lookingFor,fromValue){
        return this.from(key,'key',fromValue);
    }
    fromField(lookingFor,fromValue){
        return this.from(lookingFor,'field',fromValue);
    }
    fromCode(lookingFor,fromValue){
        return this.from(lookingFor,'code',fromValue);
    }

    
/* removed because dumb 
  //-----------------------  
    nodeFromNode(node){
        return this.from('node','node',node);
    }
  //-----------------------
*/
    nodeFromKey(key){
        return this.from('node','key',key);
    }
    nodeFromField(field){
        return this.from('node','field',field);
    }
    nodeFromCode(code){
        return this.from('node','code',code);
    }

    keyFromNode(node){
        return this.from('key','node',node);
    }
    
/* removed because dumb 
  //-----------------------  
    keyFromKey(key){
        return this.from('key','key',key);
    }
  //-----------------------
*/
    keyFromField(field){
        return this.from('key','field',field);
    }
    keyFromCode(code){
        return this.from('key','code',code);
    }

    fieldFromNode(node){
        return this.from('field','node',node);
    }
    fieldFromKey(key){
        return this.from('field','key',key);
    }
    
/* removed because dumb 
  //-----------------------  
    fieldFromField(field){
        return this.from('field','field',field);
    }
  //-----------------------
*/
    fieldFromCode(code){
        return this.from('field','code',code);
    }

    codeFromNode(node){
        return this.from('code','node',node);
    }
    codeFromKey(key){
        return this.from('code','key',key);
    }
    codeFromField(field){
        return this.from('code','field',field);
    }
    
/* removed because dumb 
  //-----------------------  
    codeFromCode(code){
        return this.from('code','code',code);
    }
  //-----------------------
*/
}

