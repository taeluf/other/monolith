

Wyg.Template = class {


    constructor(templateCode,valuesCode=null){
        this.code = templateCode;
        this.valuesCode = valuesCode;
        this.fields = this.codeToFields(templateCode);
        this.map = new Wyg.Map();
        for (const field of this.fields){
            this.map.setField(field,'key',field.key);
        }
        if (typeof valuesCode==typeof ""){
            const valueFields = this.codeToFields(valuesCode);
            for (const valueField of valueFields){
                const templateField = this.map.fromKey('field',valueField.key);
                templateField.setValue(valueField.value);
            }
        }
    }

    html(){
        const code = this.code;
        let html = code;
        for (const key in this.fields){
            const field = this.fields[key];
            html = field.codeToHtml(html);
        }
        return html;
    }
    updateValue(key,value){
        const field = this.fields[key];
        field.setValue(value);
    }

    copy(){
        return new Wyg.Template(this.code);
    }


    codeToFields(templateCode){
        const fieldReg = /\{\{[^\}\n]*\}\}/g;
        const fieldStrings = templateCode.match(fieldReg);
        const fields = [];
        for (const fieldString of fieldStrings){
            const field = new Wyg.Field(fieldString);
            fields[field.key] = field;
        }
        return fields;
    }

    // updateFieldValue(field, newValue){
    //     newValue = newValue || '';
    //     let length = field.value.length;
    //     this.editCode = this.editCode.substring(0,field.start)+newValue+this.editCode.substring(field.start+length);
    //     const diff = -1*(length - newValue.length);
    //     let start = field.start;// || this.editCode.indexOf(field.value);
    //     field.start = start;
    //     field.value = newValue;
    //     for (const field of this.fields){
    //         if (field.start>start){
    //             field.start += diff;
    //         }
    //     }

    // }

    buildEditor(){
        this.editCode = this.code;
        let i = 0;
        for (let field of this.fields){
            let value = field.value;
            field.value = field.code;
            field.start = this.editCode.indexOf(field.value);
            this.updateFieldValue(field,value);
        }
        
        return;
        let sixth = this.fields[6];
        let eigth = this.fields[8];
        let insert = '----insert 6'+sixth.key+'-----';
        this.editCode = this.editCode.substring(0,sixth.start)+insert+this.editCode.substring(sixth.start);
        eigth.start = eigth.start + insert.length;

        // let sixth = this.fields[6];
        // let eigth = this.fields[8];
        insert = '----insert 8'+eigth.key+'-----';
        this.editCode = this.editCode.substring(0,eigth.start)+insert+this.editCode.substring(eigth.start);
        //eigth.start = eigth.start + insert.length;
        console.log(this.editCode);


        const code = this.code;
        const start = code.indexOf(field.code);
        const length = field.value.length;
        this.editCode = code.replace(field.code,'');
        this.fixStartPositions(start,-length);
        const offset = this.getFieldStart(field,code);
        code = field.htmlWithNewValue(code,'');
    }

    fixStartPositions(fromOrigStart, plusNewLength){
        for (const field of this.fields){
            
        }
    }

    getFieldStart(field){
        for (const field of this.fields){

        }
    }

    parseTemplateCode(templateCode){
        


        return fieldCodeList;
    }
    
    parseFieldCodeList(fieldCodeList,templateCode){
        const fields = [];
        for (let fieldCode of fieldCodeList){
            let field = new Wyg.Field(fieldCode,this);
            fields.push(field);
        }
        return fields;
    }

    all(){
        return this.fields;
    }
    fieldFromCode(code){
        for (const key in this.fields){
            const field = this.fields[key];
            if (field.code==code){
                return field;
            }
        }
        throw "Field could not be found for code: "+code;
    }
    valueFromKey(fieldKey){
        const field = this.fieldFromKey(fieldKey);
        if (field)return field.value;
        else return undefined;
        return field.value;
    }
    fieldFromKey(fieldKey){
        return this.fields[fieldKey];
        const list = [];
        for (let field of this.fields){
            if (field.key===fieldKey){
                list.push(field);
            }
        }
        if (list.length>1){
            throw "There are multiple fields for key '"+fieldKey+"' which is not currently allowed.";
        } else if (list.length==0){
            return undefined;
        }
        return list[0];
    }

    editValuesHtml(){
        let html = '<div style="display:flex;">';
        for (const field of this.fields){
            html += '<span><h1>'+field.key+'</h1><input onkeyup="Wyg.Gross.updateValue(this.getAttribute(\'name\'),this.value);" type="text" placeholder="'+field.key+'" name="'
                                +field.key+'" value="'+Wyg.Gross.valueFromKey(field.key)+'">'
                        +'</span>';
        }
        html += '</div>';
        return html;
    }
};