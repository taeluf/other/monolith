

Wyg.Field = class {

    static isCode(str){
        let field = null;
        try {
            field = new Wyg.Field(str);
        } catch {
            return false;
        }
        return true;
    }

    constructor(fieldCode){
        let parts = this.getParts(fieldCode);
        this.key = parts.key;
        this.value = parts.value;
        this.defaultValue = parts.value;
        this.type = parts.type;
        this.code = parts.code;
        this.isTypeDeclared = parts.isTypeDeclared;
        this.isValueDeclared = parts.isValueDeclared;
        this.target = undefined;// 9-22-2019: Attribute or #text node

        if (this.type==''||this.type==undefined||this.type==null){
            this.type = this.getDefaultType();
        }


        this.target = undefined;
        this.templateTarget = undefined;
        this.targetNode = undefined;
        // this.rootEditNode = undefined;
    }

    setValue(value){
        const oldValue = this.value;
        if (value==='default'){
            this.value = this.defaultValue;
        } else {
            this.value = value;
        }

        if (this.target===undefined
            ||!(this.target instanceof Node)){
                return;
            }
        if (this.target!==undefined
            &&this.target instanceof Attr
            &&this.target instanceof Node){
                if (this.target.localName=='wyg'){

                    try {
                        const oldSpanCode = '<span '+oldValue+'></span>';
                        const oldSpan = Wyg.Parser.htmlAsNode(oldSpanCode);
                        for (const oldAttr of oldSpan.attributes){
                                this.targetNode.removeAttribute(oldAttr.localName);
                        }
                    } catch (error){
                        console.log(error);
                    }
                    const newCode = '<span '+this.value+'></span>';
                    const newSpan = Wyg.Parser.htmlAsNode(newCode);
                    for (const newAttr of newSpan.attributes){
                        this.targetNode.setAttribute(newAttr.localName,newAttr.value);
                    }
                } else {
                    const replacement =  this.templateTarget.value.replace(this.code,this.value);
                    this.targetNode.setAttribute(this.target.name,replacement);
                }
        } else if (this.target!==undefined
            &&this.target instanceof Text
            &&this.target instanceof Node){
                this.target.nodeValue = this.templateTarget.nodeValue.replace(this.code,this.value);
            }
    }

    setTargets(attribute,templateAttribute,targetNode){
        this.target = attribute;
        this.templateTarget = templateAttribute;
        this.targetNode = targetNode;
        // this.rootEditNode = this.getRootEditNode();
    }
    // getRootEditNode(){
    //     if (false&&this.targetNode.tagName=='INPUT'){
    //         return this.targetNode.parentNode;
    //     } else if (this.targetNode.nodeName=='#text'){
    //         return this.targetNode.parentNode;
    //     }
    //     return this.targetNode;
    // }


    codeToHtml(code){
        let html = code.replace(this.code,this.value);

        return html;
    }

    getDefaultType(){
        return 'text';
    }

    getParts(fieldCode){
        const partsReg = /\{\{([^\:\=\}\n]*)((\:)([^\=\}\n]*))?((=)([^\}\n]*))?\}\}/;
        const matches = fieldCode.match(partsReg);
        const parts = {};
        parts.code = matches[0];
        parts.key = matches[1];
        parts.isTypeDeclared = (matches[3]===':');
        parts.type = matches[4];
        parts.isValueDeclared = (matches[6]==='=');
        parts.value = matches[7];

        return parts;
    }



//match regex: \{\{([^\:\=\}]*)(\:([^\=\}]*))?(=([^\}]*))?\}\}
//test strings--------
// {{Key:type=value}}
// {{Key:=value}}
// {{Key:type}}
// {{Key=value}}
// {{Key:=}}
// {{Key:}}
// {{Key=}}
// {{Key}}

// {{:type=value}}
// {{:type=}}
// {{:type}}
// {{:=value}}
// {{:=}}
// {{:}}

// {{=value}}
// {{=}}
// {{}}


};