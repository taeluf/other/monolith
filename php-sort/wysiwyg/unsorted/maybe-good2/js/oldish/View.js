

Wyg.View = class {


    constructor(templateCode,valuesCode){
    
        this.template = new Wyg.Template(templateCode);

        return;





        this.values = new Wyg.ValuesList(valuesCode);





        this.fields = new Wyg.Fields(this.templateCode);
        this.valueFields = new Wyg.Fields(this.valuesCode);



        let  valueFieldsToDelete = ""
        let newValuesCode = this.valuesCode;
        for (const field of this.fields.all()){
            const valueField = this.valueFields.fromKey(field.key);
            if (valueField===undefined){
                const newFieldString = '{{'+field.key+'=default}}';
                newValuesCode += "\n"+newFieldString;
            }
        }
        for (const field of this.valueFields.all()){
            const templateField = this.fields.fromKey(field.key);
            if (templateField===undefined){
                newValuesCode = newValuesCode.replace(field.code,'');
                valueFieldsToDelete += "\n"+field.code;
            }
        }
        const defaultsReg = /\{\{[^\}]*default\}\}/g;
        const defaults = newValuesCode.match(defaultsReg);
        newValuesCode = newValuesCode.replace(defaultsReg,'');
        newValuesCode = newValuesCode.replace(/\n{1,}/g,"\n");
        newValuesCode += "\n\n"+defaults.join("\n");
        console.log('------------------');
        newValuesCode = newValuesCode + "\n\n\n\n" + valueFieldsToDelete;
        newValuesCode = newValuesCode.replace(/\n{4,}/g, "\n\n\n\n");
        console.log(newValuesCode);
        this.valueFields = new Wyg.Fields(newValuesCode);
        this.valuesCode = newValuesCode;

        console.log(this.valueFields);

    }

    valuesFromCode(){
        
    }

    fieldsNotSpecified(){
        const unspecified = [];
        for (let field of this.fields.all()){
            let valueField = this.valueFields.fromKey(field.key);
            if (valueField===undefined){
                unspecified.push(field);
            }
        }
        return unspecified;
    }

    finalReplacements(){
        let replacements = [];
        for (let templateField of this.fields.all()){
            const valueField = this.valueFields.fromKey(templateField.key);
            let value = templateField.value;
            if (valueField instanceof Wyg.Field
                &&valueField.value!='default'){
                value = valueField.value;
            }
            replacements.push({'code':templateField.code,'value':value});
        }
        return replacements;
    }

    editorOutput(){
        let replacements = this.fields.all();
        let html = this.templateCode;
        for (let replacement of replacements){
            html = html.replace(replacement.code,'{{'+replacement.key+'}}');
        }
        return html;
    }
    finalOutput(){
        let replacements = this.finalReplacements();
        let html = this.templateCode;
        for (let replacement of replacements){
            html = html.replace(replacement.code,replacement.value);
        }
        return html;
    }

};