


Wyg.Gross = class {


    static initialize(){

        let page = new Wyg.Page();
return;
    }

    static updateValue(key,newValue){
        const field = new Wyg.Field('{{'+key+'='+newValue+'}}');
        const regStr = '\{\{' + key+ '[^\}]*\}\}';
            console.log('regStr');
            console.log(regStr);
            const newValuesCode = this.valuesNode.value.replace(new RegExp(regStr,'g'), field.code);
            console.log(newValuesCode);
            console.log('----------');
        this.valuesNode.value = newValuesCode;
        this.updateOutput();
    }

    static valueFromKey(key){
        const template = new Wyg.Template(this.codeNode.value,this.valuesNode.value);
        let value = template.valueFields.fromKey(key).value || undefined;
        if (value===undefined
            ||value==='default'){
            value = template.fields.fromKey(key).value;
        }
        return value;
    }

    static setupCodeNode(){
        this.codeNode.addEventListener('keyup',event=>{this.updateOutput.call(this);this.updateValuesNode.call(this);});
    }
    static setupValuesNode(){
        this.valuesNode.addEventListener('keyup',this.updateOutput.bind(this));
    }
    static setupEditorNode(){
        this.editorNode.addEventListener('click',this.editorClicked.bind(this));
        this.editorNode.addEventListener('mouseover',this.editorHovered.bind(this));
        this.editorNode.addEventListener('mouseout',this.editorUnHovered.bind(this));
        // this.view = new Wyg.View(this.codeNode.value);
        // this.editorNode.innerText = '';
        // this.editorNode.innerHTML = '';
        let editorNode = this.makeEditorNode();
        this.editorNode.appendChild(editorNode);
        console.log(this.nodeRef);
        let refList = [];
        for (let ref of this.nodeRef){
            let groupRef = this.refFromNode(ref.node,refList);
            groupRef.keys = groupRef.keys.concat(ref.keys);
        }
        console.log('---zzaaazz---');
        console.log(refList);
    }

    static refFromNode(node,refList){
        for (let ref of refList){
            if (ref.node===node){
                return ref;
            }
        }
        let ref = {};
        ref.node = node;
        ref.keys = [];
        refList.push(ref);
        return ref;
    }

    static makeNodeList(node){
        let list = [];
        let copy = node.cloneNode();
        copy.innerText = '';
        copy.innerHTML = '';
        let newMatches = copy.outerHTML.match(/\{\{[^\}\n]*\}\}/g) || [];
        let keys = [];
        for (let match of newMatches){
            if (match==null||match.trim().length<1)continue;
            let field = new Wyg.Field(match);
            let key = field.key;
            keys.push(key);
        } 
        let ref = {};
        ref.node = node;
        ref.keys = keys;
        list.push(ref);
        for (let child of node.childNodes){
            let text = '';
            let matches = [];
            if (child.outerHTML!==undefined){
                list = list.concat(this.makeNodeList(child));
                // let copy = child.cloneNode();
                // copy.innerText = '';
                // copy.innerHTML = '';
                // let newMatches = copy.outerHTML.match(/\{\{[^\}\n]*\}\}/g);
                // matches = matches.concat(newMatches);
                //get matches from outerHTML;
            } else {
                let childText = child.nodeValue;
                let newMatches = childText.match(/\{\{[^\}\n]*\}\}/g);
                matches = matches.concat(newMatches);
                
            }
            let keys = [];
            for (let match of matches){
                if (match==null||match.trim().length<1)continue;
                let field = new Wyg.Field(match);
                let key = field.key;
                keys.push(key);
            }
            let ref = {};
            ref.node = node;
            ref.keys = keys;
            //reference.
            list.push(ref);
        }
        return list;
    }
    static makeEditorNode(){
        let node = Wyg.htmlInSpan(this.codeNode.value);
        let copy = null;
        let list = this.makeNodeList(node);
        this.nodeRef = list;
        return node;
    }

    static editorClicked(event){
        event.stopPropagation();
        event.preventDefault();
        const clickedNode = event.target;
        let editHtml = this.view.template.editCode;
        let searching = true;
        let index = 0;
        let found = 0;
        clickedNode.classList.remove('wygHover');
        if (clickedNode.className=='')clickedNode.removeAttribute('class');
        while(true){
            let lastIndex = index;
            index = editHtml.indexOf(clickedNode.outerHTML,index+1);
            if (index>=0){
                found++;
            }
            if (found>1
                ||index===-1){
                    index = lastIndex;
                break;
            }
        }


        console.log(editHtml.substr(index,clickedNode.outerHTML.length));

        console.log('found '+found);
        if (found==1){
            console.log('found node at index: '+index);
            console.log(index);
        }
        console.log(event.target.outerHTML);
        console.log('failed?');
        console.log(editHtml);

    }
    static editorHovered(event){
        event.target.classList.add('wygHover');
    }
    static editorUnHovered(event){
        event.target.classList.remove('wygHover');
    }

    static updateEditorNode(){
        const template = new Wyg.Template(this.codeNode.value,this.valuesNode.value);
        this.editorNode.innerHTML = template.editorOutput();
    }

    static updateValuesNode(){
        let valuesCode = this.valuesNode.value;
        const template = new Wyg.Template(this.codeNode.value,valuesCode);
        valuesCode = template.valuesCode;
        console.log(valuesCode)
        this.valuesNode.value = valuesCode;
    }

    static updateOutput(){
        const template = new Wyg.Template(this.codeNode.value,this.valuesNode.value);
        this.outputNode.innerHTML = template.finalOutput();
    }

};