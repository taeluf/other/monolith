

Wyg.ValuesList = class {


    constructor(templateCode){
        const fieldList = this.parseTemplateCode(templateCode);
        this.fields = this.stringsToFields(fieldList);
    }

    parseTemplateCode(templateCode){
        const fieldReg = /\{\{[^\}\n]*\}\}/g;
        const fieldCodeList = templateCode.match(fieldReg);


        return fieldCodeList;
    }
    
    parseFieldCodeList(fieldCodeList,templateCode){
        const fields = [];
        for (let fieldCode of fieldCodeList){
            let field = new Wyg.Field(fieldCode,this);
            fields.push(field);
        }
        return fields;
    }

    all(){
        return this.fields;
    }
    fromKey(fieldKey){
        const list = [];
        for (let field of this.fields){
            if (field.key===fieldKey){
                list.push(field);
            }
        }
        if (list.length>1){
            throw "There are multiple fields for key '"+fieldKey+"' which is not currently allowed.";
        } else if (list.length==0){
            return undefined;
        }
        return list[0];
    }

    editValuesHtml(){
        let html = '<div style="display:flex;">';
        for (const field of this.fields){
            html += '<span><h1>'+field.key+'</h1><input onkeyup="Wyg.Gross.updateValue(this.getAttribute(\'name\'),this.value);" type="text" placeholder="'+field.key+'" name="'
                                +field.key+'" value="'+Wyg.Gross.valueFromKey(field.key)+'">'
                        +'</span>';
        }
        html += '</div>';
        return html;
    }
};