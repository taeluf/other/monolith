<?php

class Wyg {

    public function pdo(){
        $dsn = 'mysql:dbname=wyg;host=localhost';
        $pdo = $this->pdo ?? new PDO($dsn,"user","pass_word");
        $this->pdo = $pdo;
        return $pdo;
    }

    public function templates(){
        
        $pdo = $this->pdo();
        // $templateId = $_GET['templateId'];

        $tagDataQuery = $pdo->prepare("SELECT * FROM template");
        $tagDataQuery->execute();

        return $tagDataQuery->fetchAll();
        while ($row = $tagDataQuery->fetch(PDO::FETCH_ASSOC)){

        }

    }
    public function saveTemplate($data){
        $object = (object)$data;

        $table = 'template';
        $pdo = $this->pdo();
        $query = '';
        if (empty($object->id)||!is_numeric($object->id)){
            $object->id=NULL;
            $query = "INSERT INTO {$table}(code, css, `group`, id, js, name, site, `values` ) VALUES(:code,:css,:group,:id,:js,:name,:site,:values)";
        } else {
            $query = "UPDATE {$table} SET code=:code,css=:css,`group`=:group,js=:js,name=:name,site=:site,`values`=:values WHERE id=:id";
        }
        $statement = $pdo->prepare($query);
        $statement->execute([
            ':code' => $object->code,
            ':css' => $object->css,
            ':group' => $object->group,
            ':js' => $object->js,
            ':name' => $object->name,
            ':site' => $object->site,
            ':values' => $object->values,
            ':id' => $object->id,
    
        ]);
        $object->id = $object->id ?? $pdo->lastInsertId();

        $data = ['error'=>var_export($statement->errorInfo(),true),
                'id'=>$object->id];
        echo json_encode($data);
        exit;
        return $object->id;
    }
}

?>