<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php 
    echo '<style type="text/css">';
    echo file_get_contents(__DIR__.'/../css/style.css');
    
    echo '</style>';
?>
<script type="text/javascript">

<?php

    echo file_get_contents(__DIR__.'/../js/Wyg.js');
    echo file_get_contents(__DIR__.'/../js/Page.js');
    echo file_get_contents(__DIR__.'/../js/Parser.js');
    echo file_get_contents(__DIR__.'/../js/Insertables.js');
    echo file_get_contents(__DIR__.'/../js/Editor.js');
    echo file_get_contents(__DIR__.'/../js/Field.js');
    echo file_get_contents(__DIR__.'/../js/Map.js');

?>
</script>
</head>
<body>