<?php

include_once(__DIR__.'/../php/Wyg.php');

$wyg = new Wyg();
$templates = $wyg->templates();
    // ob_get_clean();
$sites = [];
foreach ($templates as $index=>$templateRow){
    $site = $templateRow['site'];
    $sites[$site] = $sites[$site] ?? [];
    $group = $templateRow['group'];
    $sites[$site][$group] = $sites[$site][$group] ?? [];
    
    $templateCode = $templateRow['code'];
    $templateName = $templateRow['name'];
    $templateValues = $templateRow['values'];
    $sites[$site][$group][] = $templateRow;
}

// ob_start();
foreach ($sites as $siteName=>$site){
    echo '<div class="wyg-site"><h2>'.$siteName.'</h2>'."\n";
    foreach($site as $groupName=>$group){
        echo '    <div class="wyg-group"><h3>'.$groupName."</h3>\n";
        foreach ($group as $index=>$template){
            echo '        <div id="wyg-template_'.uniqid().'" class="wyg-template">'.$template['name'].'<textarea class="wyg-template-code">'
            // .json_encode(['code'=>$template['code'],'id'=>$template['id'],'name'=>$template['name'],
            //             'site'=>$template['site'],'group'=>$template['group']])
            .json_encode($template)
            .'</textarea></div>';
        }
        echo '    </div>';
    }
    echo '</div>';
}
// echo ob_get_clean();

// exit;
?>




<div style="display:none;">
<div class="wyg-site"><h2>HTML Elements</h2>
    <div class="wyg-group"><h3>Form Elements</h3>
        <div class="wyg-template"><!-- <input type="{{type}}" placeholder="{{placeholder}}"> --></div>
    </div>
</div>
<div class="wyg-site"><h2>Pre-made</h2>
    <div class="wyg-group"><h3>Site Structure</h3>
        <div class="wyg-template"><!-- wyg html to show a 3 column, full page layout --></div>
        <div class="wyg-template"><!-- wyg html to show a responsive center-column layout --></div>
        <div class="wyg-template"><!-- wyg html to show a responsive layout with a left-column menu on desktop, and a drop-down menu for mobile--></div>
    </div>
    <div class="wyg-group"><h3>Forms</h3>
        <div class="wyg-template"><!-- blank form template code. --></div>
        <div class="wyg-template"><!-- contact form template code. --></div>
        <div class="wyg-template"><!-- user login form... perhaps other sorting? --></div>
        <div class="wyg-template"><!-- user registration form. perhaps further sorting? --></div>
    </div>
<div class="wyg-site"><h2>Decatur Thrift</h2>
    <div class="wyg-group"><h3>Thrift Store</h3>
        <div class="wyg-template"><!-- a template for creating the thrift store listing --></div>
        <div class="wyg-template"><!-- a template for deleting the thrift store listing --></div>
        <div class="wyg-template"><!-- a template for showing the thrift store --></div>
    </div>
    <div class="wyg-group"><h3>Single-use</h3>
        <div class="wyg-template"><!-- a template for the nav menu on desktop --></div>
        <div class="wyg-template"><!-- a template for the nav menu on mobile --></div>
        <div class="wyg-template"><!-- the customized contact form --></div>
    </div>
    <div class="wyg-group"><h3>Reusable</h3>
        <div class="wyg-template"><!-- custom comment form --></div>
        <div class="wyg-template"><!-- store map --></div>
    </div>
</div>
</div>




So what actually goes inside wyg-template?
data-templateId is set. Or there's an internal map. or both. the Template Name is listed in the node.
