

<div id="wyg">
    <div id="tabBar">
        <span class="tab selected">Editor</span>
        <span class="tab">Output</span>
        <span class="tab">Code</span>
        <span class="tab">Styles</span>
        <span class="tab">Scripts</span>
    </div>

    <div id="panels">
        <section class="panel selected" data-tab="Editor">

            <div id="breadcrumbs"></div>
            <div id="controls"></div>
            <div id="save">
                <label>Template ID
                    <input type="text" value="" id="templateId">
                </label>
                <label>Template Name
                    <input type="text" value="" id="templateName">
                </label>
                <label>Group Name
                    <input type="text" value="" id="groupName">
                </label>
                <label>Site Name
                    <input type="text" value="" id="siteName">
                </label>
                <button onclick="Wyg.Page.instance.saveTemplate();">Save Template</button>
            </div>
            <h1>Editor</h1>
                <div id="fields"></div>
                <label for="highlight_fix">Highligh "fix"</label>
                <input type="checkbox" name="highlight_fix" checked>
                <div id="editor"></div>
            <h1>Insertables</h1>
                <div style="background:lightblue;" id="insertables"><?php include(__DIR__.'/insertables.php');?></div>
        </section>

        <section class="panel" data-tab="Output">
            <h1>Final Output</h1>
            <textarea rows="20" cols="100" id="output"></textarea>
            
        </section>
        
        <section class="panel" data-tab="Code">
            <h1>Template Code</h1>
            <button onclick="Wyg.Page.initialize();">Re-load template</button><br>
            <textarea rows="10" cols="120" id="templateCode"></textarea>
            <h1>View values</h1>
            <textarea rows="10" cols="120" id="valuesCode"></textarea>
        </section>
        <section class="panel" data-tab="Styles">
            <h1>CSS Code</h1>
            <textarea rows="10" cols="120" id="styleCode"></textarea>
        </section>
        <section class="panel" data-tab="Scripts">
            <h1>Javascript Code</h1>
            <textarea rows="10" cols="120" id="scriptCode"></textarea>
        </section>
    </div>
<!-- 
    <span class="tab">Editor</span>
    <span class="tab">Output</span>
    <span class="tab">Code</span>
    <span class="tab">Style</span>
    <span class="tab">Scripts</span> -->

</div>

<script type="text/javascript">
    Wyg.Page.initialize();
</script>



<!--

<form method="{{Submit Method:[POST,GET]=POST}}" action="{{Submit URL:url}}" wyg="{{Attributes:text=cat='some_cat' dog='some_dog'}}">
    <fieldset>
        <legend>{{Form Name:text=Contact Us}}</legend>
        <label>{{Name Label:text=Name}}<br>
            <input type="text" name="{{Name Key:text=name}}" placeholder="{{Name Placeholder:text=Alex Doe}}">
        </label><br>
        <label>{{Phone Number Label:text=Phone Number}}<br>
            <input type="{{Phone Input Type:text=tel}}" name="{{Phone Key:text=phone}}" placeholder="{{Phone Placeholder:text=+1-888-555-4433}}">
        </label><br>
        <label>{{Message Label:text=Your Message}}<br>
            <textarea name="{{Message Key:text=message}}" placeholder="{{Message Placeholder:text=Hi, I need help with my ...}}"></textarea>
        </label>
        <br>
        <input type="submit" value="{{Submit Button Text:text=Send Message}}">
    </fieldset>
</form>








{{Submit URL=/customer_service/send_message/}}
{{Form Name=Contact Customer Service}}
{{Name Label=First and Last Name}}
{{Message Placeholder=I need help with phone model XXX-#####}}

{{Submit Method=default}}
{{Name Key=default}}
{{Name Placeholder=default}}
{{Phone Number Label=default}}
{{Phone Input Type=default}}
{{Phone Key=default}}
{{Phone Placeholder=default}}
{{Message Label=default}}
{{Message Key=default}}
{{Submit Button Text=default}}







-->