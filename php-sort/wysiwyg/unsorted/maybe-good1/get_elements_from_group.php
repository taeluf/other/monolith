<?php


$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");

$elLookup = $pdo->prepare("SELECT * FROM html_elements WHERE groupName LIKE :groupName");
$elLookup->execute([':groupName'=>$_GET['group']]);

$elements = [];
foreach ($elLookup->fetchAll() as $index => $elRow){
    $elRow['isContainer'] = ($elRow['isContainer']==TRUE);
    $elements[$elRow['tagName']] = $elRow;
}

echo json_encode($elements);