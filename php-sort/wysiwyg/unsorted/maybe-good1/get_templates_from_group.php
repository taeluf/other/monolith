<?php


$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");

$elLookup = $pdo->prepare("SELECT * FROM template WHERE groupName LIKE :groupName");
$elLookup->execute([':groupName'=>$_GET['group']]);

$elements = [];
foreach ($elLookup->fetchAll(PDO::FETCH_ASSOC) as $index => $elRow){
    $elements[] = $elRow;
}

echo json_encode($elements);