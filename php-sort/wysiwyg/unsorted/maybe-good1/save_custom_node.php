<?php

$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");

$update = $pdo->prepare(
    "INSERT INTO html_elements(tagName,friendlyName,description,groupName,isContainer,templateHtml) "
        ."VALUES(:tagName,:friendlyName,:description,:groupName,:isContainer,:templateHtml)");



$_POST['isContainer'] = FALSE;
$_POST['groupName'] = 'Custom';
$_POST['description'] = 'custom desscription';
$_POST['friendlyName'] = '';
$_POST['tagName'] = 'custom_tag';
function fromPost($array){
    $ret = [];
    foreach ($array as $bind=>$postKey){
        $ret[$bind] = $_POST[$postKey];
    }
    return $ret;
}

print_r($_POST);

$bind = fromPost(
    [':friendlyName'=>'friendlyName'
    ,':groupName'=>'groupName'
    ,':description'=>'description'
    ,':tagName'=>'tagName'
    ,':isContainer' => 'isContainer'
    ,':templateHtml'=>'templateHtml']);
$bind[':isContainer'] = $bind[':isContainer']? 1 : 0;
print_r($bind);

$update->execute($bind);

print_r($update->errorInfo());

return;
header("Location: /wyg/builder/");
exit;