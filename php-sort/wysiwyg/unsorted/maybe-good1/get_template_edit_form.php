<?php


$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");

$update = $pdo->prepare(
    "SELECT * FROM html_elements WHERE templateId = :templateId");
$update->execute([':templateId'=>$_GET['templateId']]);

$data = $update->fetch(PDO::FETCH_ASSOC);

echo json_encode($data);