
Element.prototype.getTemplateData 
= function(key){
    if (this.hasAttribute('data-templateData')){
        const templateData = JSON.parse(this.getAttribute('data-templateData'));
        if (key===null)return templateData;
        else return templateData[key];
    } else {
        console.log('data-templateData is not set on');
        console.log(this);
    }
};

DataTransfer.prototype.getTemplateData 
= function(key){
    const templateData = JSON.parse(this.getData('templateData'));
    return templateData[key];
};

function generateUUIDISH() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = (performance && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'axxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxxz'.replace(/\-/g,'_').replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}
function toggleEditorMode(button){
    const editor = document.getElementById("wyg");
    if (button.innerText=='Show HTML'){
        editor.tagName = 'textarea';
        button.innerText = 'Hide HTML';
        editor.classList.toggle('show_html');
        // editor.setAttribute('data-innerHTML',editor.innerHTML);
        // editor.innerText = editor.innerHTML;
    } else {
        editor.tagName = 'div';
        button.innerText = 'Show HTML';
        editor.classList.toggle('show_html');
        // editor.innerHTML = editor.innerText;
    }
}
function showElementForm(event){
    var element = event.target;

    let request = new Request('/wyg/get_element_form/?templateId='+encodeURIComponent(element.getTemplateData('id')));
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.text();
            }
        })
        .then(formHtml => {
            document.getElementById("settings").innerHTML = formHtml;
        });
}

function showGroupSettings(event){
    let groupName = event.target.getAttribute('data-groupName');
    let request = new Request('/wyg/get_group_settings/?group='
        +encodeURIComponent(groupName)
        );
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.json();
            }
        })
        .then(json => {
            let settingsBox = document.getElementById("settings");
            settingsBox.innerText = '';
            settings_html = json.settings_html;
            settingsBox.innerHTML = settings_html;
        });
}
function showElementsOfGroup(event){
    showGroupSettings(event);
    let groupName = event.target.getAttribute('data-groupName');
    let request = new Request('/wyg/get_elements_from_group/?group='+encodeURIComponent(groupName));
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.json();
            }
        })
        .then(json => {
            let elementsBox = document.getElementById("elementsBox");
            elementsBox.innerText = '';
            var key;
            for (key in json){
                let elementData = json[key];
                let templateId = elementData.id;
                let elementDescription = elementData.description;
                let friendlyName = elementData.friendlyName || elementData.tagName;
                let templateHtml = elementData.templateHtml;
                let isContainer = elementData.isContainer;
                
                let element = document.createElement('div');
                element.innerText = friendlyName;
                element.setAttribute('draggable',true);
                element.addEventListener('dragstart',event => dragGroupedElement(event));
                element.addEventListener('click',event=>showElementForm(event));
                element.setAttribute('id','el_'+templateId);
                element.setAttribute('class','element');
                element.setAttribute('data-templateData',JSON.stringify(elementData));
                // element.setAttribute('data-description',elementDescription);
                // element.setAttribute('data-templateId', templateId);
                // element.setAttribute('data-templateHtml',templateHtml);
                // element.setAttribute('data-isContainer',isContainer);
                elementsBox.appendChild(element);
            }
            
        });
}

function showTemplatesOfGroup(event){
    showGroupSettings(event);
    let groupName = event.target.getAttribute('data-groupName');
    let request = new Request('/wyg/get_templates_from_group/?group='+encodeURIComponent(groupName));
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.json();
            }
        })
        .then(json => {
            let elementsBox = document.getElementById("elementsBox");
            elementsBox.innerText = '';
            var key;
            for (key in json){
                let elementData = json[key];
                let templateId = elementData.id;
                let elementDescription = elementData.description;
                let friendlyName = elementData.friendlyName || elementData.tagName;
                let templateHtml = elementData.templateHtml;
                let isContainer = elementData.isContainer;
                
                let element = document.createElement('div');
                element.innerText = friendlyName;
                element.setAttribute('draggable',true);
                element.addEventListener('dragstart',event => dragGroupedElement(event));
                element.addEventListener('click',event=>showElementForm(event));
                element.setAttribute('id','el_'+templateId);
                element.setAttribute('class','element');
                element.setAttribute('data-templateData',JSON.stringify(elementData));
                // element.setAttribute('data-description',elementDescription);
                // element.setAttribute('data-templateId', templateId);
                // element.setAttribute('data-templateHtml',templateHtml);
                // element.setAttribute('data-isContainer',isContainer);
                elementsBox.appendChild(element);
            }
            
        });
}


function dragGroupedElement(event){
    let node = event.target;
    event.dataTransfer.setData("templateData", 
        JSON.stringify(node.getTemplateData(null))
        );
}


function setGroupOfElement(templateId,groupName){

    let request = new Request('/wyg/set_group_on_element/?group='
        +encodeURIComponent(groupName)
        +'&element='
        +encodeURIComponent(templateId));
    fetch(request)
        .then(response => {
            if (response.status === 200) {
                return response.text();
            }
        })
        .then(text => {
            console.log("element group was changed. text response:"+ text+"");
        });
}


function dropElementIntoGroup(event){

    let groupDiv = event.target;
    let groupName = groupDiv.id.substring(String('gr_').length);
    let templateId = JSON.parse(event.dataTransfer.getData("templateData"));
    let originalNode = document.getElementById("el_"+templateId);
    
    originalNode.parentNode.removeChild(originalNode);
    let node = document.createElement('span');
    node.innerText = templateId+", ";
    groupDiv.appendChild(node);

    setGroupOfElement(templateId,groupName);
}

function htmlAsNode(html){
    const wrapper = document.createElement('div');
    wrapper.innerHTML = html;
    const node = wrapper.firstChild;
    return node;
}