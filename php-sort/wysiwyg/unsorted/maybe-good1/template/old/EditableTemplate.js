
class EditableTemplate {


    constructor(templateHtml,templateId,contentNode,templateData){
        this.templateHtml = templateHtml;
        this.templateId = templateId;
        this.contentNode = contentNode;
        this.templateData = templateData;
        this.replacements = [];
        this.setUpEditor();
    }

    setUpEditor(){
        EditableTemplate.setActiveTemplate(this);
        const contentEditor = document.getElementById("contentEditor");
        contentEditor.innerHTML = '';
        const fields = this.templateHtml.match(/\{\{[^(\}\})]*\}\}/g);
        contentEditor.appendChild(Wyg.htmlAsNode('<b>'+this.templateData.friendlyName+':</b>'));
        var thisArg = this;
        let input = null;
        let field;
        for (field of fields){
            input = document.createElement('input');
            input.setAttribute('type','text');
            input.setAttribute('placeholder',field);
            input.addEventListener('keyup',event=>{thisArg.updateContent(event);});
            
            contentEditor.appendChild(input);
        }

    }

    
    getContentHtml(placeholder,value){
        let contentHtml = this.templateHtml;
        this.replacements[placeholder] = value;
        let replacement = null;
        for (placeholder in this.replacements){
            replacement = this.replacements[placeholder];
            if (replacement==null||replacement.trim()==null||replacement.trim()=='')continue;
            contentHtml = contentHtml.replace(placeholder,replacement)
        }
        return contentHtml;
    }
    updateContent(event){
        // console.log('update content');
        // console.log(this.contentNode);
        const input = event.target;
        const value = input.value;
        const placeholder = input.getAttribute('placeholder');
        const replacedHtml = this.getContentHtml(placeholder,value);
        const newNode = Wyg.htmlAsNode(replacedHtml);

        this.contentNode.parentNode.replaceChild(newNode,this.contentNode);
        this.contentNode = newNode;
    }
}

EditableTemplate.getActiveTemplate = 
    function(){
        return this.activeTemplate || null;
    };
EditableTemplate.setActiveTemplate = 
    function(editableTemplate){
        const templateMoveButtons = document.getElementById("templateMoveButtons");
        if (editableTemplate==null){
            templateMoveButtons.setAttribute('disabled',true);
        } else {
            templateMoveButtons.removeAttribute('disabled');
        }
        this.activeTemplate = editableTemplate;
    };