<?php
$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");
?>

<h1>HTML Elements</h1>
<div id="editorSettings">
  <b>Editor</b>
  <label>Template Name:<input id="templateName" type="text" name="templateName"></label>
  <button id="save_current_template" onclick="Wyg.Editor.saveCurrentTemplate();">Save Template</button>
  <button onclick="Wyg.Editor.copyHtmlToClipboard();">Copy HTML</button>
  <select onchange="Wyg.showSelectedType(this.value);">
    <option value="editing">Editing</option>
    <option value="final">Final Output</option>
    <option value="finalHtml">Final Output HTML</option>
    <option value="templateHtml">Template HTML</option>
  </select>
</div>
<div id="controlPanel">
  <div>
    <span id="staticControls">
      <fieldset id="templateMoveButtons" disabled>
          <button onclick="Wyg.Editor.moveItemUp();">Up</button>
          <button onclick="Wyg.Editor.moveItemLeft();">&lt;</button>
          <button onclick="Wyg.Editor.moveItemRight();">&gt;</button>
          <button onclick="Wyg.Editor.moveItemDown();">Down</button>
      </fieldset>
    </span>
    <span id="contentEditor"></span>
  </div>
</div>
<?php

$groupsLookup = $pdo->prepare("SELECT DISTINCT groupName FROM element;");
$groupsLookup->execute();
$groupNames = array_map(function($row){return $row['groupName'];},
                        $groupsLookup->fetchAll());

//$extraGroupsAlreadySaved = ['HTML Sections', 'Structured Content', 'Meta Data', 'Resources','Headers', 'Lists', 'Common Text Markup', 'Other, Common', 'Other, Rare'];
$extraGroups = [];
foreach ($extraGroups as $groupName){
    if (!in_array($groupName,$groupNames))$groupNames[] = $groupName;
}

//$dsn = 'mysql:dbname=wyg;host=localhost';
//$pdo = new PDO($dsn,"user","pass_word");
  $elementGroupHtml = array_map(function($groupName){
    return '<div onclick="Wyg.Fun.showElementsOfGroup(event);" id="gr_'.$groupName
    .'" class="group" data-groupName="'.$groupName.'" ondragover="event.preventDefault();" ondrop="Wyg.Fun.dropElementIntoGroup(event);">'.$groupName.'</div>';
  },
  $groupNames);

  //
  //
  //
  //
  //
  $groupsLookup = $pdo->prepare("SELECT DISTINCT groupName FROM template");
  $groupsLookup->execute();
  $groupNames = array_map(function($row){return $row['groupName'];},
                          $groupsLookup->fetchAll());
  
  //$extraGroupsAlreadySaved = ['HTML Sections', 'Structured Content', 'Meta Data', 'Resources','Headers', 'Lists', 'Common Text Markup', 'Other, Common', 'Other, Rare'];
  $extraGroups = [];
  foreach ($extraGroups as $groupName){
      if (!in_array($groupName,$groupNames))$groupNames[] = $groupName;
  }
  
  //$dsn = 'mysql:dbname=wyg;host=localhost';
  //$pdo = new PDO($dsn,"user","pass_word");
    $templateGroupHtml = array_map(function($groupName){
      return '<div onclick="Wyg.Fun.showTemplatesOfGroup(event);" id="gr_'.$groupName
      .'" class="group" data-groupName="'.$groupName.'" ondragover="event.preventDefault();" ondrop="Wyg.Fun.dropElementIntoGroup(event);">'.$groupName.'</div>';
    },
    $groupNames);
?>

<div contenteditable="false" id="wyg" ondragover="event.preventDefault();" ondrop="Wyg.dropElement(event);">

</div>

<div id="elementList">
    <div id="elementsBoxContainer">
        <div id="elementsBox"></div>
    </div>
  </div>

<div id="groupList">
  <section>
    <h4>HTML Templates</h4>
      <?php
        echo implode($templateGroupHtml);
      ?>  
  </section>
  <section>
    <h4>HTML Elements</h4>
      <?php
        echo implode($elementGroupHtml);
      ?>
  </section>
</div>  
 
  <div id="settings">
  </div>



