
Wyg.Editor = class {

    static saveCurrentTemplate(){
        var editor = document.getElementById("wyg");
        const copy = editor.cloneNode(true);
        const children = copy.children;
        let finalNode = null;
        let output = [];
        for (child of children){
            // if (isValidNode(child)){
                // output.push(cleanNode(child));
            // }
            output.push(child);
        }
        if (output.length>1){
            finalNode = makeNodeFromArray(output);
        } else if (output.length==1){
            finalNode = output[0];
        } else {
            alert('nothing');
            return;
        }
        sendNodeToServer(finalNode);
    }
    static sendNodeToServer(node){
        const finalHTML = node.outerHTML;
        const formData = new FormData();
        formData.append('templateHtml',finalHTML);
        formData.append('templateName',document.getElementById("templateName").value);
        const request = new Request('/wyg/save_custom_template/',
            {
                "method":"POST",
                "body":formData
            }
        );
        fetch(request)
            .then(response=>{return response.text();})
            .then(text=>{console.log('response: '+text);});
    }
    static makeNodeFromArray(output){
        const container = document.createElement('span');
        for (node of output){
            container.appendChild(node);
        }
        return container;
    }
    static isValidNode(node){
        return (node.outerHTML.trim()!='');
    }

    static cleanNode(node){
        const classListCopy = node.classList;
        for (className of classListCopy){
            if (className.match(/^elem\_uniq\_[a-zA-Z\-0-9\_]+$/)){
                node.classList.remove(className);
            }
        }
        if (node.classList.length==0){
            node.removeAttribute('class');
        }
        return node;
    }

    static moveItemLeft(){
        const activeTemplate = Wyg.getActiveTemplate();
        const node = activeTemplate.node;
        const sibling = node.previousSibling;
        if (sibling==null){
            alert('selected element is at the beginning of its container');
            return;
        }
        node.parentNode.removeChild(node);
        sibling.parentNode.insertBefore(node,sibling);
    }
    static moveItemRight(){
        const activeTemplate =Wyg.getActiveTemplate();
        const node = activeTemplate.node;
        const sibling = node.nextSibling;
        if (sibling==null){
            alert('selected element is at the end of its container');
            return;
        }
        node.parentNode.removeChild(node);
        sibling.parentNode.insertBefore(node,sibling.nextSibling);
    }

    static moveItemUp(){
        const editor = document.getElementById("wyg");
        const activeTemplate =Wyg.getActiveTemplate();
        const node = activeTemplate.node;
        const parent = node.parentNode;
        if (parent==editor){
            alert('selected element is not inside a container.');
            return;
        }
        parent.removeChild(node);
        parent.parentNode.insertBefore(node,parent);
    }

    static moveItemDown(){
        const editor = document.getElementById("wyg");
        const activeTemplate =Wyg.getActiveTemplate();
        const node = activeTemplate.node;
        const sibling = node.nextSibling;
        const siblingTemplate = Wyg.templateFromNode(sibling);
        if (siblingTemplate==null||!siblingTemplate.isContainer){
            alert('the selected element\'s next sibling is NOT a container');
            return;
        }
        node.parentNode.removeChild(node);
        sibling.insertBefore(node,sibling.firstChild);
    }

    static copyHtmlToClipboard(){
        const editor = document.getElementById("wyg");
        const htmlBox = Wyg.htmlAsNode('<textarea>'+editor.innerHTML+'</textarea>');
        editor.appendChild(htmlBox);
        htmlBox.select();
        htmlBox.setSelectionRange(0, 9999999); /*For mobile devices*/
        document.execCommand("copy");
        editor.removeChild(htmlBox);

        alert("Copied the text: " + htmlBox.value);
    }

}