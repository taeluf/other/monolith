
class EditableField {

    constructor(fieldString,template){
        this.fieldString = fieldString;
        this.fieldString = fieldString;
        this.template = template;
        // let jsonString = fieldString.substring(1,fieldString.length-1).trim();
        const noQuoteJsonReg = /^\{\{([^\:]*)\:([^\}]*)\}\}$/;
        const nameOnlyReg = /\{\{([^\:]*)\}\}/;
        let jsonString = '';
        if (noQuoteJsonReg.test(fieldString)){
            jsonString = fieldString.replace(noQuoteJsonReg,'{"$1":"$2"}');
        } else if (nameOnlyReg.test(fieldString)){
            jsonString = fieldString.replace(nameOnlyReg, '{"text":"$1"}');
        }
        let fieldJson = JSON.parse(jsonString);
        for (let key in fieldJson){
            var fieldName = fieldJson[key];
            var fieldKey = key;
            break;
        }
        this.fieldName = fieldName;
        this.type = fieldKey;
        this.value = fieldName;
        this.input = this.createInput();
    }

    createInput(){  
        switch (this.type){
            case "nodeList":
                console.log('create input nodeList needs to open up to placing new nodes');
                this.value = '<span class="wyg_node_container">{{Place Items Here}}</span>';
                return document.createElement('span');
            case "url":
                return this.createTypedInput('url');
            case "text":
            default:
                return this.createTypedInput('text');
        }
    }

    createTypedInput(type){
        let template = this.template;
        let field = this;
        let input = document.createElement('input');

        
        input.setAttribute('type',type);
        input.setAttribute('placeholder',this.fieldName);
        input.addEventListener('keyup',event=>
        {
            field.value = event.target.value || field.fieldName;
            template.updateContent();
            template.selected();
        });
        return input;
    }


}