
class Template {

    constructor(templateData){
        console.log(templateData);
        for (let key in templateData){
            this[key] = templateData[key];
        }
        this.config = templateData;

        this.createEditor();
        this.makeUpdatedNode();
        this.updateContent();
    }

    setupNode(){
        const node = this.node;
        const template = this;
        var thisArg = this;
        node.addEventListener('click',event => {
            event.stopPropagation();
            event.preventDefault();
            Wyg.selectTemplate(template);
        });

        if (this.isContainer){
            node.addEventListener('dragover',
                function(event){
                    this.classList.add('wyg-highlighted');
                }
            );
            node.addEventListener('dragleave',
                function(event){
                    this.classList.remove('wyg-highlighted');
                }
            );

            node.addEventListener('mouseenter',
                function(event){
                    this.classList.add('wyg-highlighted');
                }
            );
            node.addEventListener('mouseleave',
                function(event){
                    this.classList.remove('wyg-highlighted');
                }
            );
            // node.addEventListener('mouseout',
            //     function(event){
            //         this.classList.remove('wyg-highlighted');
            //     }
            // );
        }
    }
    deselected(){
        this.node.classList.remove('wyg-selected');
    }
    selected(){
        this.node.classList.add('wyg-selected');
    }

    createEditor(){
        if (this.editorNode!=null)return;
        const outerHtml = this.templateHtml;
        const fields = this.templateHtml.match(/\{\{[^(\}\})]*\}\}/g);
        this.editableFields = [];
        var editor = document.createElement('span');

        for (let field of fields){
            let editableField = new EditableField(field,this);
            let input = editableField.input;
            editor.appendChild(input);
            this.editableFields.push(editableField);
        }
        this.editorNode = editor
    }
    makeUpdatedNode(){
        let html = this.config.templateHtml;
        for (let editableField of this.editableFields){
            //console.log(editableField);
            html = html.replace(editableField.fieldString, editableField.value);
        }
        this.node = Wyg.htmlAsNode(html);
        this.setupNode();
    }
    updateContent(){
        if (this.node==null){
            this.makeUpdatedNode();
            return;
        }
        let parentNode = this.node.parentNode;
        let nextSibling = this.node.nextSibling;
        parentNode.removeChild(this.node);
        this.makeUpdatedNode();
        parentNode.insertBefore(this.node,nextSibling);
    }
}