
class Wyg {
    static htmlAsNode(html){
        const wrapper = document.createElement('div');
        wrapper.innerHTML = html;
        const node = wrapper.firstChild;
        return node;
    }
    
    static templateFromNode(node){
        for (let template of this.templates){
            if (template.node===node){
                return template;
            }
        }
        for (let elementTemplate of this.elementTemplates){
            if (elementTemplate.node.tagName==node.tagName){
                return elementTemplate;
            }
        }
        if (node.classList.contains('wyg_node_container')){
            templateData = {"isContainer":true,
                            "templateHtml":node.outerHTML,
                            "":""};

        }
        return null;
    }
    static placeTemplateNode(template,nextSiblingNode){

        let parentNode = nextSiblingNode.parentNode;

        this.templates = this.templates || [];
        this.templates.push(template);

        let siblingTemplate = this.templateFromNode(nextSiblingNode);

        if (nextSiblingNode!=null
            &&nextSiblingNode.id=="wyg"
            ||siblingTemplate!=null
            &&siblingTemplate.node!=null
            &&siblingTemplate.isContainer
            
            ){
            parentNode = nextSiblingNode;
            nextSiblingNode = null;
        }

        parentNode.insertBefore(template.node,nextSiblingNode);
    }
    static dropElement(event){
        event.preventDefault();
        event.stopPropagation();
        let template = new Template(JSON.parse(event.dataTransfer.getData('templateData')));
        this.placeTemplateNode(template,event.target);
    }

    static clearSelection(){
        const editor = document.getElementById("contentEditor");
        editor.innerHTML = '';
        if (this.selectedTemplate!=null){
            this.selectedTemplate.deselected();
        }
    }

    static selectTemplate(template){
        this.clearSelection();
        const editor = document.getElementById("contentEditor");
        this.selectedTemplate = template;
        editor.appendChild(template.editorNode);
        template.selected();
        const moveButtons = document.getElementById("templateMoveButtons");
        if (template==null)moveButtons.setAttribute('disabled','disabled');
        else moveButtons.removeAttribute('disabled');
        this.activeTemplate = template;
    }

    static showEditingView(){
        
    }

    static showSelectedType(type){
        switch (type){
            case "editing":
                Wyg.showEditingView();
            case "final":
            case "finalHtml":
            case "templateHtml":
        }
    }
    static getActiveTemplate(){
        return this.activeTemplate || null;
    }

    static assignElementTemplates(){
        var request = new Request("/wyg/get_all_elements_data/");
        fetch(request)
        .then(response=>{
            if (response.status === 200) {
                return response.json();
            }
        })
        .then(json=>{
            // console.log(json);

            Wyg.elementTemplates = [];
            for (let key in json){
                let elementData = json[key];
                Wyg.elementTemplates.push(new Template(elementData));
            }
        })
    }
}

Wyg.assignElementTemplates();