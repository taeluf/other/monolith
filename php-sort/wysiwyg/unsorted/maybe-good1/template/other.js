Element.prototype.getTemplateData 
= function(key){
    if (this.hasAttribute('data-templateData')){
        const templateData = JSON.parse(this.getAttribute('data-templateData'));
        if (key===null)return templateData;
        else return templateData[key];
    } else {
        console.log('data-templateData is not set on');
        console.log(this);
    }
};

DataTransfer.prototype.getTemplateData 
= function(key){
    const templateData = JSON.parse(this.getData('templateData'));
    return templateData[key];
};

