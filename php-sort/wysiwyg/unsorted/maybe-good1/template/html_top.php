<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    <?=file_get_contents(__DIR__.'/styles.css');?>
</style>
<script type="text/javascript">
    <?=file_get_contents(__DIR__.'/Wyg.js');?>
    <?=file_get_contents(__DIR__.'/Template.js');?>
    <?=file_get_contents(__DIR__.'/Wyg.Fun.js');?>
    <?=file_get_contents(__DIR__.'/other.js');?>
    <?=file_get_contents(__DIR__.'/Editor.js');?>
    <?=file_get_contents(__DIR__.'/EditableField.js');?>


</script>
</head>
<body>