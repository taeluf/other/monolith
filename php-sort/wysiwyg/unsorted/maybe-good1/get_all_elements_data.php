<?php


$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"user","pass_word");

$elLookup = $pdo->prepare("SELECT * FROM html_elements");
$elLookup->execute();

$elements = [];
foreach ($elLookup->fetchAll(PDO::FETCH_ASSOC) as $index => $elRow){
    $elRow['isContainer'] = ($elRow['isContainer']==TRUE);
    $elements[$elRow['tagName']] = $elRow;
}

echo json_encode($elements);