<?php

class WYG extends \Lia\Compo {


    public function onRouteWillExecute($event,$url,$matches){
        $user = $event->Get_User();
        if ($user->is('moderator')
            &&($_GET['edit']??null)=='true'){
            $event->View('depend/JSAutowire');
            $event->View('theme/BlockForm');
            $event->View('JSToPHP/ServerScript');
            $event->View('schema/Editable');
            $event->View('schema/EditDialog');
        }
    }
    public function onRequestCompleted($event,$url,$output,$goodRoute){
        $editMode = TRUE; //would be derived from user = moderator & $_GET edit = true
        if (!$editMode)return;
        if (strlen(trim($output))==0)return;
        $headers = apache_response_headers();
        // print_r($headers);
        if (substr($url,-1)!='/'
            ||$headers!=[]){
            echo $output;
            return;
        }
        // exit;
        // \DM::require('FormTools')
        // echo $output;
        // // echo $url;
        // exit;
        $user = $event->Get_User();
        if ($user->is('moderator')
            &&($_GET['edit']??null)=='true'){
            $ft = new \FormTools($output,true);
            $dialog = $event->View('schema/EditDialog');
            $ft->SchemaPrepareEdit($dialog);
            echo $ft;
        } else {
            $ft = new \FormTools($output,true);

            $ft->SchemaPrepareDisplay();
            echo $ft;
        }
    }

        // public function onSetup_Launch($event){
            // echo "launch";
            // exit;
        // // require($this->dir('vendor').'/redbean/rb-mysql.php');
    // }
}