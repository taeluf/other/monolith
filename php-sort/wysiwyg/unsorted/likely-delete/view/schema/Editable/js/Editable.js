
class SchemaEditable extends FancyServer {

    _dialog(){return 'SchemaEditDialog';}
    __attach(){
        
    }
    async onclick(){
        this.oneDialog.show();
        const editForm = await this.fetch('getForm','home/edit/'+this.node.getAttribute('typeof'),this.node.getAttribute('lookup'));
        this.oneDialog.setForm(editForm);
    }
}
SchemaEditable.autowire();