
class PageEditor extends WygBase {

    _editor(){return 'EditableCSSCode';}
    _nodeHTML(){return 'NodeHTML';}
    __attach(){
        // this.context='WygStyle';
        document.addEventListener('click',this.documentClicked.bind(this));
    }

    onclick(event){
        event.stopPropagation();
        event.preventDefault();
    }
    documentClicked(event){
        // console.log(event.target);
        event.preventDefault();
        this.target = event.target;
        const editor = this.editor;
        editor.handle(event.target);
        this.nodeHTML.handle(event.target);
    }
}

PageEditor.autowire();