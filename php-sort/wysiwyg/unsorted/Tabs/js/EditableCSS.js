class EditableCSSCode extends WygBase {


    __attach(){
        // this.context='WygStyle';
        this.stylesheet = document.createElement('style');
        document.head.appendChild(this.stylesheet);
    }
    
    onkeyup(event){
        if (this.target==null)alert('You must click something');
        this.stylesheet.innerHTML = this.node.value;
    }
    handle(node){
        this.target = node;
        const tagName = node.tagName;
        const classList = node.classList;
        const id = node.id;
        let selector = '';
        if (id.trim().length>1)selector = '#'+id;
        else if (classList.length>0)selector = tagName+'.'+node.className.split(' ').join('.');
        else selector = tagName;
        this.node.value = selector+ ' { '+"\n\n}";
    }
}
EditableCSSCode.autowire();