<?php
$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"reed","pass_word");
?>

<h1>HTML Elements</h1>
<button id="toggle_editor_mode" onclick="toggleEditorMode(this)" >Show HTML</button>
<?php

$groupsLookup = $pdo->prepare("SELECT DISTINCT groupName FROM html_elements;");
$groupsLookup->execute();
$groupNames = array_map(function($row){return $row['groupName'];},
                        $groupsLookup->fetchAll());

//$extraGroupsAlreadySaved = ['HTML Sections', 'Structured Content', 'Meta Data', 'Resources','Headers', 'Lists', 'Common Text Markup', 'Other, Common', 'Other, Rare'];
$extraGroups = [];
foreach ($extraGroups as $groupName){
    if (!in_array($groupName,$groupNames))$groupNames[] = $groupName;
}

$dsn = 'mysql:dbname=wyg;host=localhost';
$pdo = new PDO($dsn,"reed","pass_word");
  $groupHtml = array_map(function($groupName){
    return '<div onclick="showElementsOfGroup(event);" id="gr_'.$groupName
    .'" class="group" data-groupName="'.$groupName.'" ondragover="event.preventDefault();" ondrop="dropElementIntoGroup(event);">'.$groupName.'</div>';
  },
  $groupNames);

?>

<div contenteditable="true" id="wyg" ondragover="event.preventDefault();" ondrop="(new Wyg).dropElement(this,event);">

</div>

<div id="elementList">
    <div id="elementsBoxContainer">
        <div id="elementsBox"></div>
    </div>
  </div>

<div id="groupList">
    <?php
      echo implode($groupHtml);
    ?>
</div>  
 
  <div id="settings">
  </div>



