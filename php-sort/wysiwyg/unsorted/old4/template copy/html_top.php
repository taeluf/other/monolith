<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
    <?=file_get_contents(__DIR__.'/styles.css');?>
</style>
<script type="text/javascript">
    <?=file_get_contents(__DIR__.'/scripts.js');?>
</script>
</head>
<body>