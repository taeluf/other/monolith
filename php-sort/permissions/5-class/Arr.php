<?php

namespace Taeluf\Permissions;

class Arr implements IPerm {

    protected $file = null;

    public function __construct($srcFile){
        $this->file = $srcFile;
        $this->data = $this->loadData();
    }

    public function can($performerUUID, $action, $consenterUUID){

        // We get consent first, because that's what matters most
        $consenterActions = $this->data[$consenterUUID];
        if (($consenterActions[$action]['consent']??null)!==true){
            return false;
        }

        // Now we check if performer is capable of said action
        // which is LESS important than consent (but still very important as far as code goes)
        $perfActions = $this->data[$performerUUID];
        if (($perfActions[$action]['perform']??null)!==true){
            return false;
        }

        return true;
    }

    public function getPermissions($uuid, $type=null){
        $perms = $this->data[$uuid];
        //figure out the format we want & return it
    }


    public function addPerform($uuid, $action){
        $this->data[$uuid][$action]['perform'] = true;
    }

    public function addConsent($uuid, $action){
        $this->data[$uuid][$action]['consent'] = true;
    }

    public function removePerform($uuid, $action){
        unset($this->data[$uuid][$action]['perform']);
    }

    public function removeConsent($uuid, $action){
        unset($this->data[$uuid][$action]['consent']);
    }

    public function storeChanges(){
        $file = $this->file;
        $ext = pathinfo($file,PATHINFO_EXTENSION);
        if ($ext=='php'){
            $data = var_export($this->data,true);
            $put = file_put_contents($file,$data);
            return $put;
        } else if ($ext=='json'){
            $data = json_encode($this->data);
            $put = file_put_contents($file,$data);
            return $put;
        }
    }


    protected function loadData(){
        $file = $this->file;
        $ext = pathinfo($file,PATHINFO_EXTENSION);
        if ($ext=='php'){
            $data = include($file);
            if (!is_array($data)){
                throw new BaseException("Your PHP file '{$file}' must return an array.");
            }
            return $data;
        } else if ($ext=='json'){
            $content = file_get_contents($file);
            $data = json_decode($content,true);
            return $data;
        }

        throw new BaseException("File must be 'php' or 'json', but '{$ext}' was given");
    }
}