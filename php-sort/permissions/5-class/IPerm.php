<?php

namespace IPerm\Permissions;

interface IPerm {

    public function can($perfUUID, $action, $consenterUUID);
    public function getPermissions($uuid, $type=null);
    public function addConsent($uuid, $action);
    public function addPerform($uuid, $action);
    public function removeConsent($uuid, $action);
    public function removePerform($uuid, $action);

    public function storeChanges();
}