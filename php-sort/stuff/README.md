# PHP-Tools
Some one-off tools for PHP programming. Intention is for you to copy files & put them in your project, not so much for you to use this as a dependency / library.

## Status: Unmaintaned
This repo might get occasional updates. I mostly have it here for myself. But... then again, I don't keep the files up to date or use them in most my projects so...

## Current Tools
- Mime delivery: deliver PHP files based upon mimetype
- PDO: a simple wrapper for PDO. (It's kinda bad)
- Arrays: I don't even know any more
- Strings: Again, not really sure any more
- htaccess: redirects all requests to deliver.php & forces https when not on localhost
- ClassGetter: use `token_get_all()` to get classes & interfaces from files (& directories)
- File: A convenience class for working with files
