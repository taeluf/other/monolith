<?php

namespace RBear;

class Strings {



    static public function replaceFirst($input,$lookFor,$replaceWith=''){
        if (substr($input,0,strlen($lookFor))==$lookFor){
            return substr_replace($input,$replaceWith,0,strlen($lookFor));
        }
        return $input;
    }
}