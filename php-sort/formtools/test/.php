<?php

    $src = new stdClass;
    $src->name = 'Reedy Bear';
    $src->bio = 'A silly goof who likes to write code and stuffuffuffuffufffufufffuff';
    $src->image = '/the_image.jpg';

    $formPlus = new \FormPlus($src);
    $formPlus->start();
?>
<form action="/submit" method="post" enctype="multipart/form-data">

<label>Name<br>
    <input type="text" name="name">
</label>
<br>
<label>Bio<br>
    <textarea rows="10" cols="40" name="bio"></textarea>
</label>
<br>
<img style="width:300px;" alt="Current Image" src="" data-inputname="image" />
<br>
<label>Change Image<br>
    <input type="file" name="image">
</label>
<br>
<input type="submit">

</form>

<?php
    $formPlus->display();
?>