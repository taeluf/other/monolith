# Autoload Project Status


## Next
- Update my testing lib to include the vendor/autoload.php file in the `getcwd()`
- Generate an autoload file for the current directory (so a library can autoload itself with no shipped dependencies)
- Document

## Latest
- Fixed variable naming conflicts caused by lack of scope management
- Fixed whitespace problems on output autoload.php files
- add `pwd` to be scanned for global classmap
    - exclude `pwd`/vendor
- write a vendor/autoload.php file (to use in place of, or in addition to composer's own. Maybe move composer's)
    - check (and later write) vendor/autoload.hash
    - if hash matches, then overwrite existing vendor/autoload
        - else move to composer-autoload.php
    - own autoload will include composer-autoload.php
    - write vendor/autoload.php

