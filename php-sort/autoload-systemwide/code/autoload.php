<?php

(function(){
    $dir = dirname(__DIR__);

    if (file_exists($globalComposer = $dir.'/depend/vendor/autoload.php')){
        require($globalComposer);
    }


    $classmap = $dir.'/.cache/classmap.php';
    $build = $dir.'/run.php';

    if (!file_exists($classmap)){
        require($build);
    }

    $classes = require($classmap);

    spl_autoload_register(
        function($fqcn) use ($classes){
            $fqcn = '\\'.$fqcn;
            if (!isset($classes[$fqcn]))return;
            
            if (count($classes[$fqcn])==1){
                include($classes[$fqcn][0]);
                return;
            }
            $slimmed = [];
            foreach ($classes[$fqcn] as $file){
                if (strpos($file,'old')===false)$slimmed[] = $file;
            }
            if (count($slimmed)==1){
                include($slimmed[0]);
            } else {
                trigger_error("Could not include class '{$fqcn}' because there were multiple files for the same classname.");
            }
        },false,true
    );

})();
