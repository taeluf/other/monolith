# System Wide Autoloader

## Usage
1. Go to a project directory
2. execute the run script
3. You will be prompted & a new classmap will be generated
4. `require()` the generated `vendor/autoload.php` file

You can also write a `composer.json` at `depend/vendor/composer.json` (inside this autoload repo) & run composer & those will be globally loaded, too.

## Notes
- See `.cache/classmap.php` and `.cache/global-autoload-dirs.php` for cached paths.
    - For new system setups, just delete contents of both these files & start fresh 
