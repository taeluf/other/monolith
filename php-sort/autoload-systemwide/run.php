<?php


$dir = getcwd();


if (substr($dir,0,strlen(__DIR__))==__DIR__){
    echo "You can't add this dir to the global autoloader";
    return;
}


$dirsFile = __DIR__.'/.cache/global-autoload-dirs.php';
$dirs = is_file($dirsFile) ? require($dirsFile) : [];

$phpReturn = '<?php return ';

echo "Add $dir to global autoloading? ";
$answer = trim(fgets(STDIN));
$addToAutoloading = true;
if ($answer=='y' || $answer == 'Y'){
    if (!is_array($dirs))$dirs = ['autoload'=>[], 'exclude'=>[]];
    $dirs['autoload'][$dir] = 1;
    $dirs['exclude'][$dir.'/vendor/'] = 1; 
    
    file_put_contents($dirsFile, trim($phpReturn.var_export($dirs,true).';'));
    echo "\n\nUpdated global autoload files!\n\n";
}



echo "Setup vendor/autoload.php? ";
$answer = trim(fgets(STDIN));

if ($answer=='y' || $answer == 'Y'){
    if (is_file($dir.'/vendor/autoload.php')){
        $sha1 = sha1_file($dir.'/vendor/autoload.php');
        if ($sha1==trim(file_get_contents($dir.'/vendor/autoload.hash'))){
            unlink($dir.'/vendor/autoload.php');
        } else {
            if (is_file($dir.'/vendor/composer-autoload.php'))unlink($dir.'/vendor/composer-autoload.php');
            rename($dir.'/vendor/autoload.php', $dir.'/vendor/composer-autoload.php');
        }
    }

    $autoloadFile = var_export(__DIR__.'/code/autoload.php',true);
    
    $phpCode = 
    <<<PHP
        <?php
        if (file_exists(__DIR__.'/composer-autoload.php'))require_once(__DIR__.'/composer-autoload.php');
        if (file_exists($autoloadFile)){
            require_once($autoloadFile);
        }
    PHP;

    if (!is_dir($dir.'/vendor/'))mkdir($dir.'/vendor/');
    file_put_contents($dir.'/vendor/autoload.php', trim($phpCode));
    file_put_contents($dir.'/vendor/autoload.hash', sha1_file($dir.'/vendor/autoload.php'));

    echo "\n\nMade an autoloader!\n\n";
}


echo "\nWriting a new classmap!\n";

// build the global autoload file
require_once(__DIR__.'/code/DependencyBuilder.php');
$db = new DependencyBuilder();
// $dirs = require($dirsFile);
foreach ($dirs['autoload'] as $d=>$one){
    $db->addScanDir($d);
}
foreach ($dirs['exclude'] as $d=>$one){
    $db->addExcludeDir($d);
}
$db->build();

echo "Done!\n";
