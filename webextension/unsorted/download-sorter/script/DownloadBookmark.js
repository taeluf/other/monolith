class DownloadBookmark extends RB.WireContext {

    __attach(){
        console.log('attached');
        console.log(this.node);
    }
    onclick(event){
        console.log('did click');
        
        browser.runtime.sendMessage({
            url: this.node.getAttribute('data-url'),
            filename: this.node.getAttribute('data-dest')
        });
        
    }
}