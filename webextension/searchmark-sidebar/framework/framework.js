

function loadStyle(href){
    // avoid duplicates
    for(var i = 0; i < document.styleSheets.length; i++){
        if(document.styleSheets[i].href == href){
            return;
        }
    }
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    head.appendChild(link);
}

function loadScript(src){
    const url = src;
    const viewUrl = browser.runtime.getURL(url);
    const request = new RB.Request(viewUrl);
    request.handleText(function(text){
            try {
                console.log('handle text');
                const script = document.createElement('script');
                script.innerHTML = text.trim();
                document.body.appendChild(script);
            } catch (e) {
                console.log(e);
            }
    });
    console.log('as html');
}

function loadView(url){
    const viewUrl = browser.runtime.getURL(url);
    const request = new RB.Request(viewUrl);
    request.handleText(function(text){
            try {
                const div = document.createElement('div');
                div.innerHTML = text.trim();
                document.body.appendChild(div);
            } catch (e) {
                console.log(e);
            }
    });
}

