
console.log('init.js');

function doSetup(){
        const cssUrl = browser.runtime.getURL('/style/panel.css');
        loadStyle(cssUrl);
        console.log('css');
        loadView('/view/panel.html');
        console.log('view');
        loadScript('/script/main.js');
        console.log('js');
} 
document.body.addEventListener("load",doSetup);
doSetup();

