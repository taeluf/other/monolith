

var RB = RB ?? {};
RB.Request = class {


    constructor(url, method){
        this.params = {};
        this.url = url;
        this.method = method ?? 'POST';
    }
    put(key,value){
        if (key in this.params){
            this.params[key] = (typeof this.params[key]==typeof []) ? this.params[key] : [this.params[key]];
            this.params[key].push(value);
        } else {
            this.params[key] = value;
        }
    }
    // send(){
    //     const request = new Request();
    //     console.log('sendysendysend');
    //     return this;
    // }
    handleJson(func){
        var formData = new FormData();
        //very simply, doesn't handle complete objects
        // console.log(this.params);
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            // cache: "no-cache",
            // credentials: "same-origin",
            // redirect: "follow",
            // referrerPolicy: "no-referrer",
            // headers: {
            //     'Content-Type': 'application/json'
            //     // 'Content-Type': 'application/x-www-form-urlencoded',
            // },
            body: formData
        }).then(res => {
            return res.json();
            // console.log(res.json());
            // document.getElementById("cards").innerHTML = res.json().cards;
            // console.log("Request complete! response:", res);
        }).then(json => {
            // document.getElementById("cards").innerHTML = json.cards;
            func(json);
        });
    }
    handleText(func){
        var formData = new FormData();
        //very simply, doesn't handle complete objects
        // console.log(this.params);
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            // cache: "no-cache",
            // credentials: "same-origin",
            // redirect: "follow",
            // referrerPolicy: "no-referrer",
            // headers: {
            //     'Content-Type': 'application/json'
            //     // 'Content-Type': 'application/x-www-form-urlencoded',
            // },
            body: formData
        }).then(res => {
            return res.text();
            // console.log(res.json());
            // document.getElementById("cards").innerHTML = res.json().cards;
            // console.log("Request complete! response:", res);
        }).then(text => {
            // document.getElementById("cards").innerHTML = json.cards;
            func(text);
        });
    }
}
