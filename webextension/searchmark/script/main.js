

document.querySelector('.tlf-tagmark-bubble')
    .addEventListener('click',
        function(event){
            document.querySelector('.tlf-tagmark-bubble').style.display = 'none';
            document.querySelector('.tlf-tagmark').style.display = 'flex';
        }
    )

document.querySelector('.tlf-tagmark .cancel')
.addEventListener('click',
    function(event){
        document.querySelector('.tlf-tagmark').style.display = 'none';
        document.querySelector('.tlf-tagmark-bubble').style.display = 'block';
    }
)

document.querySelector('.tlf-tagmark .mark')
.addEventListener('click',
    function(event){
        let item = localStorage.getItem('tagmark');
        // let item = null;
        if (item==null)item = "{}";
        item = JSON.parse(item);
        const tags = document.querySelector('.tlf-tagmark [name="tags"]').value;
        const notes = document.querySelector('.tlf-tagmark [name="notes"]').value;
        const url = window.location;
        item[url] = {
            "url": url,
            "tags": tags,
            "notes": notes
        };

        console.log(item);
        localStorage.setItem('tagmark', JSON.stringify(item));
    }
)