<?php

/**
 *
 * @param $nums array<int index, int number>
 * @return array<int index, int diff_number>  
 */
function find_differences(array $nums): array{

    $diffs = [];
    for ($i = 0; $i < count($nums) - 1; $i+=1) {
        $left = $nums[$i];
        $right = $nums[$i+1];
        $diff = $right - $left;

        $diffs[] = $diff;
    }
    return $diffs;
}

function all_zero(array $nums): bool {
    foreach ($nums as $num){
        if ($num!=0)return false;
    }
    return true;
}

function get_zeroed_stack(array $nums): array{
    if (all_zero($nums))return [];
    //$stack = [];
    $diffs = find_differences($nums);
    echo "\n\n\nNUMS\n";
    print_r($nums);
    echo "\n\n\nDiffs\n";
    print_r($diffs);
    ////exit;
    //$stack[] = $diffs;
    if (all_zero($diffs))return [$diffs];

    return array_merge([$diffs], get_zeroed_stack($diffs));
}

function extend_zeroed_stack(array $num_stack): array {
    global $ops;
    $stack_index = count($num_stack) - 1;

    $num_stack[$stack_index][] = 0;
    $last_num_added = 0;

    $last_num_index = count($num_stack[$stack_index])-1;

    for (--$stack_index;$stack_index>=0;$stack_index--){
        echo "\nStack Index: $stack_index";
        echo "\nNums This Row: ".implode(" ",$num_stack[$stack_index]);
        echo "\nLastnum_added: ".$last_num_added;
        $last_num_added = 
            $last_num_added
            + $num_stack[$stack_index][$last_num_index++];

        $ops++;
        $num_stack[$stack_index][] = $last_num_added;
    }
    return $num_stack;
}

function extend_stack_left(array $num_stack): array {
    $stack_index = count($num_stack) - 1;

    $num_stack[$stack_index][] = 0;
    $last_num_added = 0;

    $last_num_index = 0;

    for (--$stack_index;$stack_index>=0;$stack_index--){
        echo "\nStack Index: $stack_index";
        echo "\nNums This Row: ".implode(" ",$num_stack[$stack_index]);
        echo "\nLastnum_added: ".$last_num_added;
        $last_num_added = 
            $num_stack[$stack_index][0]
            - $last_num_added
            ;

        array_unshift($num_stack[$stack_index],$last_num_added);
        //$num_stack[$stack_index][] = $last_num_added;
    }
    return $num_stack;
}
