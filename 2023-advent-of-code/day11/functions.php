<?php

global $chars;
$chars = [
    '|'=>[[0,-1],[0,1]], // north & south
    '-'=>[[1,0],[-1,0]], // east & west
    'L'=>[[0,-1],[1,0]], // north & east
    'J'=>[[0,-1],[-1,0]], // north & west
    '7'=>[[0,1],[-1,0]], // south & west
    'F'=>[[0,1],[1,0]], // south & east

    //. is ground; there is no pipe in this tile.
    //S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.

];


function get_char($x,$y,$line_width,$text): string{
    $pos = (($line_width+1) * $y) + $x;
    $char = substr($text,$pos,1);
    return $char;
}

function is_connecting($pos1,$charpos,$char): bool {
    global $chars;

    
    $pos_adjustments = $chars[$char] ?? null;
    if ($pos_adjustments === null) return false;

    $target_one = 
        ['x'=>$charpos[0] + $pos_adjustments[0][0],
         'y'=>$charpos[1] + $pos_adjustments[0][1],
        ];
    if ($target_one==(array)$pos1)return true;

    $target_two = 
        ['x'=>$charpos[0] + $pos_adjustments[1][0],
         'y'=>$charpos[1] + $pos_adjustments[1][1]
        ];
    if ($target_two==(array)$pos1)return true;

    return false;
}

/**
 * 
 * @return object coordinates
 */
function get_next_step(object $prev_cord, object $from_cord, int $line_width, string $input_text): object{
    $connecting_pipes = find_connecting_pipes($from_cord,$line_width, $input_text);

    echo "\nConnecting Pipes from cord: ".$from_cord->x.','.$from_cord->y."\n";
    print_r($connecting_pipes);

    $step1 = $connecting_pipes[0];
    $step2 = $connecting_pipes[1] ?? null;
    $next_step = null;
    if ($step1==$prev_cord)$next_step = $step2;
    else $next_step = $step1;

    echo "\nNext Step: ".$next_step->x.','.$next_step->y;

    return $next_step;
}

/**
 * Return array of coordinantes, should be 2
 * Coord is an object w/ $obj->x & $obj->y as integer coordinates.
 * @param $c object coordinate
 * @return array<int index, object cords> array of coordinate objects.
 */
function find_connecting_pipes(object $c, int $line_width, string $text): array{
    global $chars;
    $search_all = [
        [$c->x - 1, $c->y], // west 
        [$c->x, $c->y-1], // north
        [$c->x+1, $c->y], // east
        [$c->x, $c->y+1], // south 
    ];

    $start_char = get_char($c->x, $c->y, $line_width, $text);
    if ($start_char == 'S')$search = $search_all;
    else if ($start_char== '.')$search = [];
    else {
        $search_diffs = $chars[$start_char];
        $search = [];
        foreach ($search_diffs as $diff){
            $dx = $diff[0];
            $dy = $diff[1];
            $search[] = [$c->x + $dx, $c->y + $dy];
        }
    }

    $connecting = [];
    //echo "\n\n\nCur_pos: ".$c->x.','.$c->y;
    foreach ($search as $s){
        $char = get_char($s[0],$s[1],$line_width,$text);
       //echo "\nTest Char $char at: ".$s[0].','.$s[1];

        $cp = $s[0].','.$s[1];
        $p1 = $c->x.','.$c->y;
        echo "\n Does $char($cp) connect to $p1?";
        if (is_connecting($c, $s, $char)){
            echo " yes";
            $connecting[] = (object)['x'=>$s[0], 'y'=>$s[1]];
        }
    }
    //echo "\n";

    return $connecting;
}

function get_char_type(int $x, int $y, &$xycords, $x_max, $y_max){
    $PIPE = 7;
    $INSIDE = 12;
    $OUTSIDE = 15;
    $UNKNOWN = -1;
    $SEARCHING = 56;


    if (isset($xycords["$x,$y"]) 
        &&$xycords["$x,$y"] != $UNKNOWN)return $xycords["$x,$y"];
        //$xycords["$x,$y"] != $UNKNOWN)return $xycords["$x,$y"];
    $xycords["$x,$y"] = $SEARCHING;

    $cardinal_positions = [
        [$x - 1, $y], // west 
        [$x, $y-1], // north
        [$x+1, $y], // east
        [$x, $y+1], // south 
    ];

    foreach ($cardinal_positions as $p){
        $cx = $p[0];
        $cy = $p[1];
        $n = "$cx,$cy";
        $known_value = $xycords[$n] ?? '';
        echo "\nKnown Value for $n:".$known_value;
        if ($known_value==$SEARCHING)continue;
        if ($cx < 0
            || $cy < 0
            || $cx > $x_max
            || $cy > $y_max
            || $known_value == $OUTSIDE
        ) return $OUTSIDE;
        

        //$sub_type = get_char_type($cx, $cy, $xycords);
        //if ($sub_type == $SEARCHING)continue;

        //if ($sub_type == $OUTSIDE)return $OUTSIDE;
        //if ($sub_type == $INSIDE) return $INSIDE;
        //if ($sub_type == $UNKNOWN) continue;
        //if ($sub_type == $PIPE) continue;
        
    }
    return $UNKNOWN;
}

