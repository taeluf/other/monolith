## Install 
<!-- If using Code Scrawl, copy+paste this content into .docsrc/README.src.md -->
<!-- If NOT using Code Scrawl, replace `@git_url()` with your package's https git url -->
We'll `git clone` this package and run the install script, which will download our single dependency & setup `~/.bashrc` to point to this library.
1. `cd ~/; mkdir vendor; cd vendor;` or `cd` into another directory for downloaded packages
2. `git clone @git(https_url)`
3. `./install`
4. Either `source ~/.bashrc` or re-launch your terminal
