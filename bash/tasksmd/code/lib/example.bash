#!/usr/bin/env bash

is_git_dir(){
    local dir
    dir="$(project_dir)"
    if [[ "$dir" != "" ]];then
        return 0;
    fi
    echo ""
    msg_mistake "$(pwd) is not a project directory. Try again. "
    echo ""
    return 1;
}

function git_dir(){
    echo "$(git rev-parse --show-toplevel 2>/dev/null)";
}


