#!/usr/bin/env bash

##
# Optionally import other command files from this group. Only core.bash is loaded by default for `core` group
# import code/core-extra

## 
# @arg variable name in which to store the aliases output
# An array in form of `(alias:"group command", alias2:"another command")`
#
function core_aliases(){
    declare -n al="$1"
    al=()
    

    # running `core abd` will call `core add bin dir
    al+=(abd:"core add bin dir")
    # running `core h` will call the primary `help` function & show a menu
    al+=(h:"help")
}

##
# `core` is the group & every `core` group function must begin with `core_`.
# When running your library, `core` group is assumed if a group is not given, so `yourcommand core` and `yourcommand` are the same thing.
#
function core(){
    local dir
    dir="$(pwd)"
    task rc.data.location="$dir/.task/data" rc:"$dir/.config/.taskrc" $@
}

function core_debug(){
    run core rc.debug=1 rc.debug.hooks=1 $@
}

## 
# Add an `export PATH` call to your `~/.bashrc` file 
#
# @tip Add a `bin` directory
# @arg directory - a full directory path
#
function core_add_bin_dir(){
    if prompt_yes_or_no "Add '$dir' to PATH via ~/.bashrc?";then
        echo "export PATH=\"$dir:\$PATH\"" >> ~/.bashrc
        msg_status "~/.bashrc updated!"
    fi

    msg_instruct "Run 'source ~/.bashrc' or re-launch your terminal"
}

