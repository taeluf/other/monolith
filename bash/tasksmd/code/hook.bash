#!/usr/bin/env php
<?php

$line=readline();


$data = json_decode($line, true);
// $data['description'] = 'not '.$data['description'] .' again';
//
$data['name'] = $data['name'] ?? '';



$dataLocation = '';
foreach ($argv as $index=>$value){
    $parts = explode('rc.data.location=',$value);
    if (count($parts)==1)continue;
    $parts = explode(' rc:',$parts[1]);
    $dataLocation = $parts[0];
}

$name = $data['name'] ?? null;
$description = $data['description'];
$attr = $data;
unset($attr['name']);
unset($attr['description']);
$uuid = $attr['uuid'];
unset($attr['uuid']);
ksort($attr);

foreach ($attr as $key=>$value){
    $attributeList .= "- ${key}: ${value}\n";
}
$attributeList = substr($attributeList,0,-1);



$md = <<<MD
# $name
- UUID:$uuid
$attributeList
$description
MD;

$project = $data['project'] ?? 'default';

if ($name == null)$name = $uuid;

$file = dirname($dataLocation).'/'.$project.'/'.$name.'.md';
if (!is_dir(dirname($file))){
    mkdir(dirname($file), 0755, true);
}
file_put_contents($file, $md);

$data['description'] = $file;

echo json_encode($data);


// echo "$line";
# echo "{\"description\":\"$line\"}"
# echo "$abc"
#
#
#


return;
?>
# TaskA (task name)
- UUID: whatever (the UUID is really important, because it allows for renaming)
- Status: D
- Assigned: Reed (or whatever)
- Tags: abc, def, ghi, jkl, mno, pqr, stu, vwx, yz
A description of the task. It can be multiple lines. This paragraph is what's previewed in the GUI.

Additional task information can come below, like here. When you click on the card in the GUI, this stuff will display too. Maybe its a whole page of its own.
