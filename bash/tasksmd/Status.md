# Status
This was intended to store tasks as markdown files. It's a proof of concept I'm abandoning



There is a proof of concept. You can run `./bin/taskmd add some task` and a task will be corretly output to the task directory

There is no way to give a name for the task, as far as I could find in the taskwarrior documentation. I think I'd need to add that myself. 


I don't intend to continue development of this. Its a fun idea, but its probably not worth my time to put into it, especially since I don't think I even really want that workflow.

