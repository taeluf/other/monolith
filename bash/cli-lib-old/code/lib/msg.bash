#!/usr/bin/env bash

function color_li(){
    local color;
    if [[ $(($1 % 2)) == 0 ]];then
        color="${cLiEven}"
    else
        color="${cLiOdd}"
    fi
    echo "$color";
}

function header(){
    msg "${cHeader}# ${1}${cOff}" 
}
function big_header(){
    msg "${cHeader}${1}${cOff}"
}

function line_break(){
    msg "${cBreak}---------${cOff}"
}

function msg(){
    # See os-setup.sh
    os_msg "$1" "$2" "$3"
}


function msg_status(){
    msg "${cStatus}${1}...${cOff}"
}
function msg_instruct(){
    msg "${cInstruct}${1}${cOff}"
}
function msg_mistake(){
    msg "${cMistake}Whoops!${cOff} ${1}${cMistake}${2}${cOff}"
}
function msg_notice(){
    msg "${cMistake}NOTICE:${cOff} ${1}${cMistake}${2}${cOff}"
}
function msg_success(){
    msg "${cSuccess}WooHoo!${cOff} ${1}"
}
function msg_ulist(){
    c=0;
    for li in "$@"; do
        c=$((c+1))
        msg "  $(color_li $c)-${cOff} ${li}"
    done
}
function msg_olist(){
    c=0;
    for li in "$@"; do
        c=$((c+1))
        msg "  $(color_li $c)${c}.${cOff} ${li}"
    done
}

function msg_header(){
    msg "${cHeader}# ${1}${cOff}" 
}









