#!/usr/bin/env bash

if [ "$(uname)" == "Darwin" ]; then
    OS="MAC"
    # If necessary, call for macOS 
    # specific initialization code

elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
    OS="LINUX"
else
    OS="OTHER"
fi

os_msg(){
    # @TODO maybe turn color off at the end of every echo
    if [[ $OS == "MAC" ]]; then
        echo "$1" "$2" "$3"
    else 
        echo -e "$1" "$2" "$3"
    fi
}
