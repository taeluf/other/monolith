#!/usr/bin/env bash

cli_tips(){
    echo ""
    echo "Some Tips";
    echo "  1. For any prompts, press [enter] to confirm your answer."
    echo "  2. You may see things like (y-yes/n-no/[blank]-use default). "
    echo "     This means: "
    echo "     a. Type \"y\" for yes"
    echo "     b. Type \"n\" for no"
    echo "     c. Leave blank & press [enter] to use the default";
    echo "  3. Press [ctrl+c] any time to cancel this script."
    echo "  4. [ctrl+click] to open links in your browser"
    echo ""
    
    prompt "[enter] to continue"
}

