#!/usr/bin/env bash


function core(){
    run help
}

function core_install_framework(){

    # see internal/core.bash
    determine_project_root project_dir
    [ -z "$project_dir" ] && return
    
    # determine "src" folder name (code, src, app, etc)
    determine_src_folder "$project_dir" srcPath 
    [ -z "$srcPath" ] && return
    
    if [[ ! -d "$srcPath" ]];then
        msg_mistake "Something went wrong with creating '$srcPath'"
        return
    fi 

    # determine cli lib's command name
    prompt_quit "What is your library's command name?" cmdName
    
    runDest="$project_dir/$cmdName"
    cliDest="$srcPath/cli.bash"
    libDest="$srcPath/lib/"
    sampleDest="$srcPath/core/"
    internalDest="$srcPath/internal/"
    msg_header "Changes Staged"
    msg_ulist \
        "Copy 'run' script to '$runDest'" \
        "chmod ug+x 'run' script" \
        "Copy 'cli' script to '$cliDest'" \
        "Copy library code to '$libDest'" \
        "Copy sample code to '$sampleDest'" \
        "Copy sample code to '$internalDest'" \

    prompt_yes_or_quit "Continue?" \
        && return

    runSrc="$runDir/tlf_lib_run"
    pdirLen="${#project_dir}"
    srcDirName="${srcPath:$pdirLen}"
    # cp "$runSrc" "$runDest"
    echo -e \
"#!/usr/bin/env bash

dir=\"\$(cd \"\$(dirname \"\${BASH_SOURCE[0]}\" )\" >/dev/null 2>&1 && pwd )\"
runDir=\"\$dir\"
. \$dir/${srcDirName}/cli.bash
" \
> "$runDest"
    chmod ug+x "$runDest"

    frameworkSrc="$runDir/code"
    cp -R "$frameworkSrc"/* "$srcPath"


    msg_instruct "Src files at '$runDir'"
    prompt_yes_or_quit "Remove framework source files?" remSrc \
        && return

    rm -rf "$runDir"
}
