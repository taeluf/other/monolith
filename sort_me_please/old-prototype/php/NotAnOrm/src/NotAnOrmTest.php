<?php

namespace Tlf;

require_once(__DIR__.'/NotAnOrm.php');
// require_once(__DIR__.'/.php');

echo "--\n";


function runTest(){

    $nao = NotAnOrm::new('reed', 'BadPass12', 'temp');

    $nao->createTable('recipe');

    $nao->create('recipe', ['name'=> 'Tofu Stirfry']);
    $nao->create('recipe', ['name'=> 'Veg Springroll']);
    $nao->create('recipe', ['name'=> 'Candied Baby Carrots']);
    $nao->create('recipe', ['name'=> 'Lasagna']);
    $nao->create('recipe', ['name'=> 'Sleep Potion']);
    $nao->create('recipe', ['name'=> 'Happy Potion']);
    $nao->create('recipe', ['name'=> 'Uh oh! poison']);
    $nao->create('recipe', ['name'=> 'Lo Mein']);


    $tofuStirfy = $nao->selectOne('recipe', ['name' => 'Tofu Stirfry']);
    $recipe = $nao->selectOne('recipe', ['name'=>'Sleep Potion']);
    
    $potions = $nao->sql('recipe', 'getPotions');

    var_dump($potions);

}

runTest();





echo "\n\nTest Done\n\n";
