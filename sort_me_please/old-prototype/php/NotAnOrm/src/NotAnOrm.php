<?php

namespace Tlf;

class NotAnOrm {

    protected $pdo;

    public function __construct($pdo){
        $this->pdo = $pdo;
    }

    public function createTable($table){
        $class = ucfirst($table);
        $file = $class.'.php';
        require_once(__DIR__.'/'.$file);
        $class = "\\Tlf\\$class";
        $this->execute($class::createTable());
    }

    public function execute($sql, ?array $binds=null){
        return $this->query($sql, $binds);
    }
    public function query($sql, ?array $binds=null){
        $pdo = $this->pdo;
        $stmt = $pdo->prepare($sql);
        $stmt->execute($binds);
        $rows = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $rows;
    }

    public function sql($tableName, $queryName){
        $class = ucfirst($tableName);
        require_once(__DIR__.'/'.$class.'.php');
        $class = '\\Tlf\\'.$class;
        $sql = $class::getSql($queryName);
        $items = $this->execute($sql);
        return $items;
    }


    public function selectOne($tableName, $requiredValues=[]){
        return $this->select($tableName, $requiredValues)[0];
    }
    public function select($tableName, $requiredValues=[]){
        $sql = "SELECT * FROM `${tableName}` ";
        if (is_string($requiredValues)){
            echo "dont care yet";
            // $stmt .= $requiredValues;
            // $rows = $this->query($stmt);
            // return;
        }
        else {
            $binds = [];
            $params = [];
            foreach ($requiredValues as $key=>$value){
                $binds[":$key"] = $value;
                $joiner = '=';
                if (is_string($value)){
                    $joiner = ' LIKE ';
                }
                $keySql = "`$key` $joiner :$key";
                $params[] = $keySql;
            }
            $whereStr = "WHERE ".implode(' AND ', $params);
            $sql .= $whereStr;

            print_r($sql);
            print_r($binds);

            $rows = $this->query($sql, $binds);
            $items = $this->rowsToObjects($tableName, $rows);
            return $items;
        }
    }

    public function rowsToObjects($table, $rows){
        $class = ucfirst($table);
        $class = "\\Tlf\\${class}";
        $items = [];
        foreach ($rows as $row){
            $items[] = new $class($row);
        }

        return $items;
    }

    public function create($tableName, $starterValues){

        $cols = [];
        $binds = [];
        foreach ($starterValues as $key=>$value){
            $cols[] = $key;
            $binds[":{$key}"] = $value;
        }
        $colsStr = '`'.implode('`, `',$cols).'`';
        $bindsStr = implode(', ', array_keys($binds));
        $stmt = "INSERT INTO `${tableName}`(${colsStr}) 
                VALUES (${bindsStr})
            ";

        $this->execute($stmt, $binds);
    }


    static public function new($user, $password, $db, $host='localhost'){
        $pdo = new \PDO("mysql:dbname=${db};host=${host}", $user, $password);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $nao = new static($pdo);
        return $nao;
    }
}
