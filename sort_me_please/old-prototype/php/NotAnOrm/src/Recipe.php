<?php

namespace Tlf;

class Recipe {

    static protected $create = 
        <<<MYSQL
            DROP TABLE IF EXISTS recipe;
            CREATE TABLE recipe
                (id INT AUTO_INCREMENT PRIMARY KEY,
                    name VARCHAR(256) 
                )
            ;
        MYSQL
    ;


    static protected $queries = [
        'getPotions' => 
        <<<MYSQL
            SELECT * FROM recipe WHERE name LIKE '%Potion%'
        MYSQL,
    ];


    public function __construct($row){
        foreach ($row as $key=>$value){
            $this->$key = $value;
        }
    }



    static public function getSql($name){
        return static::$queries[$name];
    }

    static public function createTable(){
        return static::$create;
    }
}
