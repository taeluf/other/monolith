<?php

class RouterBenchCompare extends \Taeluf\Tester {

    public function doARoute(){

    }

    public function paths(){
        return [

            '/blog/black-lives-matter/',
            '/blog/toxic-pollution',
            '/about/me/',
            '/blog/global-warming/',
        ];
    }

    public function routes(){
        return [
            '/blog/{article}/',
            // '/blog/{article}',
            '/about/{page}/',

            '/black-lives-matter/',
            '/save/the/environment/',
            '/{any}.{person}/deserves/respect/',

            '/abc/{dyn}/something/',
            '/abc/def/{dynam}.{two}/',
            '/abc/def/{dynam}-{two}',
            '/abc/def/{dynam}{two}.{abc}',
            '/@POST.greed/kills/@GET.people/',
            '/flarg/harp/@POST.@GET.{dyn}/',
            '@POST./regulations/on/{megacorp}/',
        ];
    }

    public function testLiaison(){
        $lia = new \Liaison();
        // $package = new \Lia\Package($lia, __DIR__);
        // $router = new \Lia\Compo\Router($package);
        $router = $lia->compo('Router');

        foreach ($this->routes() as $route){
            echo "\n$route\n";
            $router->addRoute($route, [$this,'doARoute']);
        }

        $i = 0;
        while ($i++ < 1000){

            ob_start();
            foreach ($this->paths() as $path){
                $request = new \Lia\Obj\Request($path, 'GET');
                $route = $router->route($request)[0] ?? null;
                if ($route!=null)
                    echo $route->url()."\n";
            }
            $ob = ob_get_clean();
        }

        echo $ob;
        echo "did the benching!";
    }
}


