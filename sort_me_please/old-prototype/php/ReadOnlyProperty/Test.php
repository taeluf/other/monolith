<?php
require_once(__DIR__.'/ReadOnlyTrait.php');

class Person {
    use Readonly;

    protected $name;
    //simply declaring this means "the 'name' property can be read by anyone"
    private $r_name;

    protected $phoneNumber;

    public function __construct($name){
        $this->name = $name;
        $this->phoneNumber = '123-555-1234';
    }
}


$person = new \Person('george');

echo "Name: ".$person->name."\n";
echo "\nNow an error:\n";

try {
    $person->name = "Bellina";
} catch (\Error $e){
    echo "Success!! Could not set person's readonly name.\n";
}

try {
    $pn = $person->phoneNumber;
} catch (\Error $e){
    echo "Success!! could not get protected phone number.\n";
}
