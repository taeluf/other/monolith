# rof File Format

There is a working example at `test/RofTest.php`. I used pretty simple operations, so I think most languages will be able to implement it pretty easily. The worst cross-language thing will be `trim` probably. Maybe I shouldn't trim?? Idk.

# STATUS: REFRESHED
I'm working on a new version with this format:

```rof
# rof : [a-zA-Z\.]+
A.key: and this
is a long value
# I am a comment
A.nother.key: This is a single line value
```

The first line defines your file:
- comment symbol
- separator between key:value
- keyRegex
- keyExcapeChar

So you can have
```rof
// rof = [a-z\.]+
a.key=
this.ISNOT.akey= because the key regex does not match
This is still a value
```

We're going to use a while-loop to ready & to write values.
