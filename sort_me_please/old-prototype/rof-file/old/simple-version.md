# Simple version that matches key/value pairs, removing whitespace, delimiters are not allowed
Therefore if your line starts with something that looks like a key, then it is a key... There is no escaping...

## Sample

key.abc-funny_blah: value
value line 2
value line 3

key2.76yUKLnm_-: value 2 single line
simple.key: yes, good simple value
simple.key2: very good


## Starting Regex
- This is working, but fails to remove trailing whitespace from a value
- shortened: ^([a-zA-Z0-9_\-\.]+):(?:\s|\r|\n)*((?:(?:.|\n|\r)(?!^[a-zA-Z0-9_\-\.]+:))+)
- Requires multi-line & global modifiers

^([a-zA-Z0-9_\-\.]+) # key
 :
(?:\s|\r|\n)*
 ((?:
    (?:.|\n|\r) # characters we want
    (?!^[a-zA-Z0-9_\-\.]+:) # But NOT if those characters make up a key
 )+)


