<?php

class ROF {

    static public function decode($filePath){
        $decoder = new \ROF\Decoder();
        $data = $decoder->parseFile($filePath);
        $data = $decoder->flatten($data);
        return $data;
    }
}