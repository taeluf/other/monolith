#!/usr/bin/env bash

str="$(cat test.rof)"

declare -A conf


state=""
stOpen="0"
stKey=1

curKey=""
curValue=""

echo "$IFS";

while read -r line; do
    # echo "--${line}--"
    # continue;
    key=$(echo "$line" | sed -E 's/^([a-zA-Z._-]+):.*/\1/')
    
    if [[ "$key" != "$line" ]];then
        if [[ -n "$curKey" ]];then
            # remove trailing whitespace from curValue

            # echo "#####${curValue:(-1)}"
            while [[ "${curValue:(-1)}" == " " || "${curValue:(-2)}" == "\n" ]];do
                # echo "TRIM END"
                if [[ "${curValue:(-1)}" == " " ]];then
                    curValue="${curValue:0:-1}"
                else  
                    curValue="${curValue:0:-2}"
                fi
            done
            
            echo "#####${curValue:(-1)}"
            if [[ "${curValue:(-1)}" == "\\" ]];then
                echo "backslash!!!!!!"
                curValue="${curValue:0:-1}"
            fi

            conf["$curKey"]="$curValue"
            # echo "KEY:${curKey}"
        fi
        len="${#key}"
        len=$(( len + 1 ))
        value="${line:${len}}"

        while [[ "${value:0:1}" == " " ]];do
            value="${value:1}"
        done

        if [[ "${value:0:1}" == "\\" ]];then
            value=${value:1}
        fi
        # echo "##$key"
        # echo "--$value"
        curKey="$key"
        curValue="$value"
    else
        # echo "--$line--"
        if [[ "${line:0:1}" == "\\" ]];then
            line=${line:1}
        fi
        # echo "--$line--"
        # continue;
        if [[ "${line:-1}" == "\\" ]];then
            echo '################trim end'
            line="${line:0:-1}"
        fi
        curValue+="\n"
        curValue+=$line
    fi
done <<< "$str"

# remove trailing whitespace from curValue

# echo "#####${curValue:(-1)}"
while [[ "${curValue:(-1)}" == " " || "${curValue:(-2)}" == "\n" ]];do
    # echo "TRIM END"
    if [[ "${curValue:(-1)}" == " " ]];then
        curValue="${curValue:0:-1}"
    else  
        curValue="${curValue:0:-2}"
    fi
done

echo "#####${curValue:(-1)}"
if [[ "${curValue:(-1)}" == "\\" ]];then
    echo "backslash!!!!!!"
    curValue="${curValue:0:-1}"
fi

conf["$curKey"]="$curValue"
# echo "KEY:${curKey}"

echo 
echo "--TEST--"
echo "${!conf[@]}"
echo -e "--${conf['a.one']}--"
echo -e "--${conf['Another_two-whoo']}--"
echo -e "--${conf['last.fourkey']}--"
echo -e "--${conf['A.Key.three']}--"
