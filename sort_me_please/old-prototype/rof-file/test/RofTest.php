<?php

function decode_rof_line($rofLine){
    $remainder = $rofLine;
    $pos = strpos($remainder,' ');
    $rof_comment = substr($remainder,0,$pos);
    $remainder = substr($remainder, $pos+4); // because 'rof' is always there
    $configSeparator = substr($remainder,0,1);
    $remainder = substr($remainder,1);
    $pos = strpos($remainder, $configSeparator);
    $rof_separator = substr($remainder, 0, $pos);
    $remainder = substr($remainder, $pos);
    $rof_keyReg = trim($remainder);

    return [
        'rof_comment'=>$rof_comment,
        'rof_separator'=>$rof_separator,
        'rof_keyReg'=>$rof_keyReg,
    ];
}

function arrayFromRof($filePath){
    $fh = fopen($filePath, 'r');

    $firstLine = fgets($fh);
    $settings = decode_rof_line($firstLine);
    extract($settings);

    $buffer = '';
    $lastKey = null;
    $out = [];
    $line = null;
    $lastLine = '';
    $commLen = strlen($rof_comment);
    do {
        $buffer .= $lastLine;
        $line = fgets($fh);

        if (substr($line,0,$commLen)==$rof_comment)continue;

        $pos = strpos($line,$rof_separator);
        if ($pos===false){
            $lastLine = $line;
            continue;
        }
        $key = substr($line, 0, $pos);
        if (preg_match('/^'.$rof_keyReg.'$/',$key)!==1){
            $lastLine = $line;
            continue;
        }
        $lastLine = substr($line, $pos+1);

        if ($lastKey!==null)$out[$lastKey] = trim($buffer);
        $buffer = "";
        $lastKey = $key;
    }
    while ($line !== false);

    $buffer .= $lastLine;
    if ($lastKey!==null)$out[$lastKey] = trim($buffer);

    return $out;
}

// $content = file_get_contents(__DIR__.'/sample-custom.rof');
$rof = arrayFromRof(__DIR__.'/sample-custom.rof');
print_r($rof);


echo "\nbench comparison disabled. Comment out return line to turn back on\n";
return;
bench_compare('sample-custom',200000);
echo "\n";

function bench_compare($name,$iters=50000){
    $json = __DIR__.'/'.$name.'.json';
    $rof = __DIR__.'/'.$name.'.rof';
    $start=microtime(true);
    for ($i=0;$i<$iters;$i++){
        $result = arrayFromRof($rof);
    }
    $end = microtime(true);
    $rofLen = $end - $start;

    $start = microtime(true);
    for ($i=0;$i<$iters;$i++){
        $result = json_decode(file_get_contents($json),true);
    }
    $end = microtime(true);
    $jsonLen = $end - $start;

    echo "rof: $rofLen";
    echo "\njson: $jsonLen";
}

