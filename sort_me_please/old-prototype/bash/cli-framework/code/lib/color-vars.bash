#!/usr/bin/env bash

cHeader="${BIPurple}"
cOff="$Color_Off"
cBreak="${BIBlue}"
cCommand="${BIGreen}"
cStatus="${BICyan}"
cInstruct="${BIYellow}"
cMistake="${BIRed}"
cSuccess="${BIGreen}"
cMark="${BIGreen}"

cLiOdd="${BIGreen}"
cLiEven="${BIBlue}"
