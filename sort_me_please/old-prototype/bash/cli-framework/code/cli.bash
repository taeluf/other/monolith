#!/usr/bin/env bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
codeDir=$dir
IMPORTS=()

# source all files in your "src/lib" directory & "src/internal" directory
function source_files(){

    libDir="$codeDir/lib"
    if [[ -d "$libDir" ]]; then
        for file in "$libDir"/*.bash; do
            source $file
        done
    fi

    
    internalDir="$codeDir/internal"
    if [[ -d "$internalDir" ]];then
        for file in "$internalDir"/*.bash; do
            source $file
        done
    fi
}

# import a file from your "src/core" directory
function import(){
    # @TODO Only source files if they haven't been sourced yet
    imp="${1/\//-}"
    # echo "##IMP:${imp}"
    existing=${IMPORTS["$imp"]}
    # echo "EXISTING:${existing}"
    if [[ "$existing" != "$imp" ]];then
        # echo "---------SOURCING $1"
        source "${codeDir}/${1}.bash"
        # source "${codeDir}/${1}.bash"
        IMPORTS["$imp"]="$imp"
    fi

    # echo "${IMPORTS[@]}"
}

# is_function function_name returns true if function_name is a function. false otherwise
function is_function(){
    func="$1";
    TYPE=`LC_ALL=C type -t "${func}"`;
    if [[ -n $TYPE && $TYPE == 'function' ]]; then
        return 0;
    fi

    return 1;
}

# runs a command. run cmd_grp cmd. cmd_grp is the name of the file in "src/core". and prefix for cmd. Default cmd_grp is 'core'.
# Ex: `run install` redirects to `run core install`, which will import `src/core/core.bash` and call `core_install`
function run(){
    # Determine the command group
    # msg_header "Try"
    # echo ":::${@}"
    cmd_group="$1";
    cmd_group_file="${codeDir}/core/${cmd_group}.bash"
    input=("${@:2}")
    
    # Check if command group's file exists, else set to core
    if [[ ! -f "${cmd_group_file}" ]]; then
        input=("${@:1}")
        cmd_group="core"
        # cmd_group_file="${codeDir}/core/${cmd_group}.bash"
    fi
    import "core/${cmd_group}"
    # echo "FILE: ${cmd_group_file}"

    ## Preparing the command
    cmd="${cmd_group}"
    tryCmd="$cmd"
    argsToRemove=0;
    maxTries=4;
    tries=0;
    ### Checking cmd_group_arg1 is a function. Then cmd_group_arg1_arg2 etc
    for arg in "${input[@]}"; do
        newCmd="${tryCmd}_${arg}"
        # echo "##${newCmd}"
        tries=$(($tries + 1))
        if is_function $newCmd;then
            # echo "#### IS FUNCTION"
            cmd="$newCmd"
            hits=$tries
        fi
        tryCmd="$newCmd"
        if [[ $tries -ge $maxTries ]]; then
            break 2;
        fi
    done
    args=("${input[@]:${hits}}")

    # echo "GROUP:${cmd_group}"
    # echo "COMMAND:${cmd}"
    # echo "ARGS:${args[@]}"
    # echo "ACTUAL:${@}"

    # Fallback to command group if a sub-command wasn't found
    if [[ "${cmd_group}" == "${cmd}" && "${#args[@]}" -ge 1 ]];then
        attempt="${args[@]:0:1}"
        cgStr="${cmd_group} "
        if [[ "$cmd_group" == "core" ]];then
            cgStr=""
        fi;
        msg_mistake "[bent ${cgStr}${attempt}] is not a function"
        msg_instruct "will execute [bent ${cmd_group}]"
        # @TODO consider adding a prompt here with [enter] to continue
        if ! prompt_enter;then
            return;
        fi
    fi

    $cmd $args
}


source_files

# Provide clear starting point for bent output
line_break;

# run core upload
run $@

msg ""
