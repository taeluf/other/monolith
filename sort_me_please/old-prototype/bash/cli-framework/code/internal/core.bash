#!/usr/bin/env bash
# All files in internal/ are included automatically by cli.bash


function determine_project_root(){

    local -n output="$1"

    # determine project root directory
    dir="$(pwd)";
    msg_instruct "Using project direcotry '${dir}'"

    prompt_yes_or_no "Is this okay?" usePwd
    

    if [[ ! $usePwd ]]; then
        prompt_quit "Enter path relative to '${dir}'" relPath \
            && return;
    fi

    path="$dir/$relPath"
    if [[ ! -d "$path" ]]; then
        msg_mistake "'$path' is not a directory"
        return;
    fi

    path="$(realpath "$path")"
    if [[ ! $usePwd ]];then
        msg
        msg_instruct "Using '$path'"
    fi

    output="$path"
}

function determine_src_folder(){
    local -n output="$2"
    srcDirList=('src' 'code' 'app')

    srcDir=""
    # echo "${srcDirList[@]}"
    for subDir in "${srcDirList[@]}"; do
        if [[ -d "$project_dir/$subDir" ]];then
            srcDir="$subDir"
            break 2
        fi
    done
    
    useSrcDir=$(false)
    if [[ "$srcDir" != "" ]]; then
        prompt_yes_or_no "Use '${srcDir}' for source directory?" useSrcDir
    fi;

    if [[ ! $useSrcDir ]]; then
        prompt_quit "Enter src directory name" srcDir \
            && return
    fi
    srcPath="$project_dir/$srcDir"

    if [[ ! -d "$srcPath" ]];then 
        prompt_enter "Creating directory ${srcPath}" \
            || return
        mkdir -p "$srcPath"
    fi

    output="$srcPath"
}
