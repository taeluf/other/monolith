#!/usr/bin/env bash


function help(){
    msg_header "Tlf Cli Framework"
    msg_instruct "cmd_name [command_group] [command]"
    msg "Any commands in the 'core' group can be called without specifying the group"

    prompt_choose_function \
        "# cmd_name [command] OR cmd_name core [command]" \
            "run core install_framework" "install" "Install the framework for your project" \
            \
            \
        "# cmd_name [command_group]" \
            "'\`cmd_name [command_group]\` to see sub-commands" \
            "'  then \`cmd_name [command_group] [command]\`" \
            "run help" "help" "Get help / view the commands available in this library" \
    ;
}
