# Bash Cli Library
A little library for building bash-powered cli apps.

## Set up your new cli library
1. Create a directory for your project
2. Run these two commands:
    ```bash
    git clone https://gitlab.com/taeluf/bash/cli-lib temp-tlf-cli-lib/
    temp-tlf-cli-lib/tlf_lib_run install_framework
    ```
3. Review bash files in `src_dir/core` and `src_dir/internal`. 
4. Repeat any time you want a new version of the framework (I think that should work)

## Learn how to use the framework
1. Look at the files in this repo
2. Look at the files in your new repo after doing the "install" (they're the same)
3. Follow said example

OR
Look at my lib [Git Bent](https://tluf.me/git-bent), which is going to be the best example of this lib in use.
