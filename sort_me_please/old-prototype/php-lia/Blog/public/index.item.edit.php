<?php


    $this->addScriptUrl("/script/editor.js");

    $article = \ROF\Article::fromSlug($this->slug);
    if ($article==false){
        $article = new \ROF\Article();
        if ($this->allows($article->locks('create'))){
            $article = new \ROF\Article();
        } else {
            echo "You do not have permission to create an article.";
            $this->showAtUrl($article->url);
            return;
        }
    } else if (!$this->allows(...$article->locks('edit'))){
        echo "You do not have permission to edit this article.";
        $this->showAtUrl($article->url);
        $this->setType('redirect:no-base');
        return;
    }
    $this->addScriptUrl('/script/simple-mde.js');
    $this->addStyleUrl('/style/simple-mde.css');
    $articleSlug = $this->slug ? $this->slug : ($_GET['url'] ?? '');
  ?>
<form action="<?=$this->url('','submit');?>" method="post">
    <div class="flex-container">
        <section style="min-width:65%;padding:0;">
            <fieldset>
                <legend>Create Article</legend>
                <label for="title">Title</label>
                <br>
                <input style="width:80%;" type="text" placeholder="title" name="title" value="<?=$article->title;?>">
                <br><br>
                <label for="category">Category</label>
                <br>
                <select id="category_selector" name="category" onchange="category_changed(this);">
                    <option value="">None</option>

                    <?php $categories = \ROF\Article::getCategories(); 
            
                        foreach($categories as $category) { 
                            if ($category==NULL)continue;
                            ?>
                          <?php  $selected = $article->category==$category ? 'selected' : ''; ?> 
					   		<option value="<?=$category?>" <?=$selected?>><?=$category?></option>
					   	
                    <?php } 
                    ?>
						<option value="_new_">New Category</option>
                </select>
                <div id="new_category_box" style="display:none;">
                    <br>
                    <b>Category Name</b>: 
                    <input type="text" id="new_category_input" placeholder="category name" /> <input type="button" value="Use this Category" onclick="category_added();"/>
                </div>
                <br>
                <label for="url">Customize the URL</label>
                <br>
                <p>You can use these characters: <b>-</b> <b>_</b> <b>/</b> along with lowercase text and numbers.</p>
                <br>
                <b id="article_category_url">/<?=$article->category?>/</b><input type="text" name="slug" value="<?=$articleSlug?>">
                <br>
                <br><br>
                <label for="blurb">Brief Description</label>
                <br>
                <textarea style="width:50%;" rows="8" name="blurb" placeholder="(optional)Blurb for search results & preview"><?=$article->blurb?></textarea>
                <br><br>
                <div>
                    <label for="article-body">Body</label>
                    <div id="text-area-container">
                        <textarea id="rof_article_editor" style="width:500px;" rows="20" name="article-body" placeholder="Write your article here"><?=$article->markdownBody?></textarea>
                    </div>
                </div>
            </fieldset>
        </section>
    </div>
    <br><br>
    <input type="submit" value="Save Article">
    <input type="hidden" name="id" value="<?=$article->id;?>">
    <br>
    <script>
    var simplemde = new SimpleMDE({ element: document.getElementById("rof_article_editor") });
    </script>
</form>

<?php
