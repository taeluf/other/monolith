# Hot Code
Just some code blocks that I find extremely helpful, but are dumb to make into a real dependency


## PHP

### PHP, static vs self
in a class, `static` refers to the child-most class. `self` refers to the class in which the code is written.
For inheritance use `static`. 

### PHP, split string by line

#### str_replace & explode
Testing, awhile ago, showed this to be much faster than the regex option.
```php
$arrayOfLines = explode("\n",
                    str_replace(["\r\n","\n\r","\r"],"\n",$str)
            );
```

### PHP, get files from dir

####
Tags: recursive, function, basic
```php
function allFilesFromDir($dir){
        if (!is_dir($dir))return null;
        $dh = opendir($dir);
        $allFiles = [];
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            if (is_dir($dir.'/'.$file)){
                $subFiles = $this->allFilesFromDir($dir.'/'.$file);
                $allFiles = array_merge($allFiles,$subFiles);
                continue;
            }
            $allFiles[] = str_replace('//','/',$dir.'/'.$file);
        }
        return $allFiles;
    }
```

####
Tags: recursive, class, static
```php
class Files {
        static public function allFilesFromDir($dir,$recurse=true){
            if (!is_dir($dir))return null;
            $dh = opendir($dir);
            $allFiles = [];
            while ($file=readdir($dh)){
                if ($file=='.'||$file=='..')continue;
                if ($recurse
                    &&is_dir($dir.'/'.$file)){
                    $subFiles = $this->allFilesFromDir($dir.'/'.$file);
                    $allFiles = array_merge($allFiles,$subFiles);
                    continue;
                }
                $allFiles[] = str_replace('//','/',$dir.'/'.$file);
            }
            return $allFiles;
        }
}
```


### PHP Parsedown, add target=\_blank to links
```php
if (!class_exists('MyParsedown')){
    class MyParsedown extends Parsedown{
        protected function inlineLink($excerpt){
            $link = parent::inlineLink($excerpt);
            if (!isset($link))return null;
            $link['element']['attributes']['target'] = '_blank';

            // var_dump($link);
            // throw new \Exception('love cats');
            return $link;
        }
    }
}
```

### PHP Redbean, Custom model & Custom Properties (non-saving)
Note that I use `RDB` instead of `R`. This was a quick change to the redbean source code. `R` caused conflicts in my project.
1) Write a `CustomProps` class to modify built-in behavior
2) Write a model that uses the custom property feature
3) Enable your Custom Props class & Your model namespace
4) Use it!
```php
// STEP 1 - Extend Redbean
namespace RDBBean;
class CustomProps extends \RedBeanPHP\OODBBean {
    //With how redbean works, the `&` for return-by-reference is required (I think)
    public function &__get($prop){
        $model = $this->box();
        if (method_exists($model,$method='get'.ucfirst($prop)))return $model->$method();
        return parent::__get($prop);
    }
}
//
//
// STEP 2 - Create a Model
namespace RDBModel;
class Person extends \Redbean_SimpleModel {

    public function &getFullName(){
        $name = $this->bean->firstName.' '.$this->bean->lastName;
        return $name;
    }
}
//
//
// STEP 3 - Tell Redbean what you've done!
define('REDBEAN_MODEL_PREFIX', '\\RDBModel\\');
define('REDBEAN_OODBBEAN_CLASS', '\\RDBBean\\CustomProps');
//
// Step 4 - Utilize!
$person = \RDB::findOne('person','id=?',[1]);
$fullName = $person->fullName;
echo $fullname;
```

### PHP, Header send 500 server error
Ref: https://stackoverflow.com/questions/4162223/how-to-send-500-internal-server-error-error-from-a-php-script
`header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error",true,500);`

## GIT
### GIT, Delete a branch
Note the lowercase `-d` in `git branch -d branch_name`. This means the branch will only be deleted if it is fully merged upstream. If you use uppercase `-D`, it's like `-d --force`, which will delete regardless of merge status
```bash
# pretend I know how to set variables in bash
BRANCH_NAME=your-branch-name
# Optional archiving
git checkout -b archive-$BRANCH_NAME;
git push -u origin archive-$BRANCH_NAME;
git push origin --delete $BRANCH_NAME; # Delete Remote
git branch -d $BRANCH_NAME; # Delete Local
```

### GIT: Add, Commit, And push all at once
We'll make a bash script that will do `git add -A; git commit -m "..."; git push`, exiting if you use an empty commit message
Create a file, give it execute permission, and open it for editing. You don't have to use the `bin` directory. But for now, we are.
```bash
cd ~/;
mkdir bin;
cd bin;
touch gitamp; # The file name IS the command name. you can make it whatever you like.
chmod +x gitamp;
nano gitamp;
```

Now, copy+paste the following bash script into the `gitamp` file.
```bash
echo "git add, commit, push from: "; pwd;
read -p "Enter Commit Message:" msg; 
case $msg in
    "" ) echo "Empty commit message. Exiting";exit;;
esac
git add -A; 
git commit -m "$msg"; 
git push;
```
Now, you can `~/bin/gitamp` OR add it to your path, like so:  
`sudo nano ~/.bashrc`, and on the last line write `export PATH=$PATH:~/bin`

Then restart your terminal or enter `source ~/.bashrc`

## Web Layout/Design, HTML / CSS
### HTML/CSS, Wrap text around an image with float  
Tags: css, html, css-float, responsive  

```html
<style>
    .floatContainer {}
    .floatContainer img {
        float:left;
        width:100%;
        max-width:450px;
    }
    .floatContainer p {}
    .floatContainer p:before {
        content: ' ';
        display:table;
        width:15em;
    }
</style>
<div class="floatContainer">
    <img src="/image/cat_cover_image.jpg" alt="A Big Cat" />
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

        Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst.
    </p>
</div>
```

### HTML/CSS, Wrap text around an image
Tags: html, deprecated
Note that `html` `align` attribute is deprecated and unsported. It's for tables anyway. But I found it in a blog & checked that it [cannot be used](https://caniuse.com/#search=html%20align) any more, anyway.
```html
<div>
    <img src="IMAGE URL" align="left" /><!-- `align` is deprecated -->
    <p>Your text goes here.</p>
</div>
```

## Mysql

### Create a database
### Import a database
You must first create the database  
- `mysql -u username -p new_database < data-dump.sql`

### export a database
- `mysqldump -u username -p database_name > data-dump.sql`


## VSCode

### Use single-line comments for HTML & CSS

1. install the multi-command extension 
    - F1 -> type `?ext` -> search `multi-command` -> install package by ryuta46
2. Add the below code to your settings.json 
    - F1 ->  type `>Preferences: Open Settings (JSON)`
3. Add the below code to your keybindings.json 
    - F1 -> type `>Preferences: Open Keyboard Shortcuts (JSON)`

#### settings.json code
```json
"multiCommand.commands": [
    {
        "command": "multiCommand.LineByLineComment",
        "sequence": [
            "editor.action.insertCursorAtEndOfEachLineSelected",
            "editor.action.addCommentLine",
        ]
    }
]
```
#### keybindings.json code
```json
{
    "key": "ctrl+/",
    "command": "extension.multiCommand.execute",
    "args": {
        "command": "multiCommand.LineByLineComment"
    },
    "when": "editorTextFocus && editorHasSelection && resourceExtname =~ /\\.(html|css|scss|php)/"
    // "when": "editorTextFocus && editorHasSelection && resourceExtname =~ /\\.html/"
}
```
