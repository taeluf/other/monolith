# Get Files recursive in PHP

Here's a one-liner that declares nothing in the global scope, thus will not conflict with any functions or classes you have.

```php
$files = (new class{
            function getFiles($path,$ext='*'){
                $dh = opendir($path);
                $files = [];
                while ($file = readdir($dh)){
                    if ($file=='.'||$file=='..')continue;
                    $fPath = str_replace('//','/',$path.'/'.$file);
                    if (is_dir($fPath)){
                        $subFiles = $this->getFiles($fPath);
                        $files = array_merge($files,$subFiles);
                    } else if ($ext=='*'
                        ||pathinfo($fPath,PATHINFO_EXTENSION)==$ext){
                        $files[] = $fPath;
                    }
                    $files[] = $fPath;
                }
                return $files;
            }
        })->getFiles($dir);
```