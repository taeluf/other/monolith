# Manage Git Branches

## Add a branch, work, then merge changes
0. Currently on existing_branch
1. git checkout -b new_branch_name
2. do work on new_branch_name & commit & push
3. git checkout existing_branch
4. git merge new_branch_name

## delete a branch
1. Be ABSOLUTELY sure that you don't need the branch any more
2. consider doing `git checkout -b archive-branch_name; git push -u origin archive-branch_name;`
3. Remote: `git push origin --delete branch_name`
4. Locally: `git branch -d branch_name` 
    - Note the lowercase `-d`. This means the branch will only be deleted if it is fully merged upstream.
    - If you use uppercase `-D`, it's like `-d --force`, which will delete regardless of merge status










#### Old Notes
good stuff to remember

-delete a branch on the remote repository
git push origin --delete branch_name

-delete a tag on the remote repository
git push --delete origin tagname

-not really sure what this is
git remote update
git fetch --all --prune 

-create a branch
git checkout -b branch_name
-- edit files
git add -A
git commit -m "message"
git push origin branch_name


-create a tag
git tag -a tag_name -m "this version rocks"
git push origin tag_name


-get local copy of remote repository
git clone https://domain.com/repository-uri



---creating a new release version
-create release branch if not exists
-switch to release branch
-make necessary changes to composer.json & any other files
git add -A
git commit -m "message"
git push origin release
-create tag if not exists
git push origin tag_name

---create a new release version with existing release branch
git checkout release
git merge master
// make any changes to files, if needed
git add -A
git commit -m "release version x.y.z"
git push origin release
git tag -a x.y.z -m "release version"
git push origion x.y.z


---delete a commit, but keep the files
git log
git reset --soft ID_NUMBER_FROM_LOG
# then make your new commits & pushes as needed

--list branches
