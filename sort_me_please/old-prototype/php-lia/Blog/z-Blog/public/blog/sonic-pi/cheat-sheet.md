# Sonic-Pi Cheatsheet

## synths
:dull_bell
:pretty_bell
:beep
:sine
:saw
:pulse
:subpulse
:square
:tri
:dsaw
:dpulse
:dtri
:fm
:mod_fm
:mod_saw
:mod_dsaw
:mod_sine
:mod_beep
:mod_tri
:mod_pulse
:tb303
:supersaw
:hoover
:prophet
:zawa
:dark_ambience
:growl
:hollow
:mono_player
:stereo_player
:blade
:piano
:pluck
:sound_in
:noise
:pnoise
:bnoise
:gnoise
:cnoise
:basic_mono_player
:basic_stereo_player
:basic_mixer
:main_mixer

## Samples
### Drums
sample :drum_heavy_kick
sleep 1
sample :drum_tom_mid_soft
sleep 1
sample :drum_tom_mid_hard
sleep 1
sample :drum_tom_lo_soft
sleep 1
sample :drum_tom_lo_hard
sleep 1
sample :drum_tom_hi_soft
sleep 1
sample :drum_tom_hi_hard
sleep 1
sample :drum_splash_soft
sleep 1
sample :drum_splash_hard
sleep 1
sample :drum_snare_soft
sleep 1
sample :drum_snare_hard
sleep 1
sample :drum_cymbal_soft
sleep 1
sample :drum_cymbal_hard
sleep 1
sample :drum_cymbal_open
sleep 1
sample :drum_cymbal_closed
sleep 1
sample :drum_cymbal_pedal
sleep 1
sample :drum_bass_soft
sleep 1
sample :drum_bass_hard
sleep 1
sample :drum_cowbell
sleep 1
sample :drum_roll
sleep 1

### Electric sounds
sample :elec_triangle
sleep 1
sample :elec_snare
sleep 1
sample :elec_lo_snare
sleep 1
sample :elec_hi_snare
sleep 1
sample :elec_mid_snare
sleep 1
sample :elec_cymbal
sleep 1
sample :elec_soft_kick
sleep 1
sample :elec_filt_snare
sleep 1
sample :elec_fuzz_tom
sleep 1
sample :elec_chime
sleep 1
sample :elec_bong
sleep 1
sample :elec_twang
sleep 1
sample :elec_wood
sleep 1
sample :elec_pop
sleep 1
sample :elec_beep
sleep 1
sample :elec_blip
sleep 1
sample :elec_blip2
sleep 1
sample :elec_ping
sleep 1
sample :elec_bell
sleep 1
sample :elec_flip
sleep 1
sample :elec_tick
sleep 1
sample :elec_hollow_kick
sleep 1
sample :elec_twip
sleep 1
sample :elec_plip
sleep 1
sample :elec_blup
sleep 1

### Guitar
sample :guit_harmonics
sleep 1
sample :guit_e_fifths
sleep 1
sample :guit_e_slide
sleep 1
sample :guit_em9
sleep 1
### Misc
sample :misc_burp
sleep 1
sample :misc_crow
sleep 1
sample :misc_cineboom
sleep 1



### Percussion
sample :perc_bell
sleep 1
sample :perc_bell_2
sleep 1
sample :perc_snap
sleep 1
sample :perc_snap2
sleep 1
sample :perc_swash
sleep 1
sample :perc_till
sleep 1
sample :perc_door
sleep 1
sample :perc_impact_1
sleep 1
sample :perc_impact_2
sleep 1
sample :perc_swoosh
sleep 1

### Ambient
sample :ambi_soft_buzz
sleep 1
sample :ambi_swoosh
sleep 1
sample :ambi_drone
sleep 1
sample :ambi_glass_hum
sleep 1
sample :ambi_glass_rub
sleep 1
sample :ambi_haunted_hum
sleep 1
sample :ambi_piano
sleep 1
sample :ambi_lunar_land
sleep 1
sample :ambi_dark_woosh
sleep 1
sample :ambi_choir
sleep 1
sample :ambi_sauna
sleep 1
### Bass
sample :bass_hit_c
sleep 1
sample :bass_hard_c
sleep 1
sample :bass_thick_c
sleep 1
sample :bass_trance_c
sleep 1
sample :bass_drop_c
sleep 1
sample :bass_woodsy_c
sleep 1
sample :bass_voxy_c
sleep 1
sample :bass_voxy_hit_c
sleep 1
sample :bass_dnb_f
sleep 1
### Bass Drums
sample :bd_pure
sleep 1
sample :bd_808
sleep 1
sample :bd_zum
sleep 1
sample :bd_gas
sleep 1
sample :bd_sone
sleep 1
sample :bd_haus
sleep 1
sample :bd_zome
sleep 1
sample :bd_boom
sleep 1
sample :bd_klub
sleep 1
sample :bd_fat
sleep 1
sample :bd_tek
sleep 1
sample :bd_ada
sleep 1
sample :bd_mehackit
sleep 1


### Snares
sample :sn_dub
sleep 1
sample :sn_dolf
sleep 1
sample :sn_zome
sleep 1
sample :sn_generic
sleep 1

### Loops
sample :loop_industrial
sleep 1
sample :loop_compus
sleep 1
sample :loop_amen
sleep 1
sample :loop_amen_full
sleep 1
sample :loop_garzul
sleep 1
sample :loop_mika
sleep 1
sample :loop_breakbeat
sleep 1
sample :loop_safari
sleep 1
sample :loop_tabla
sleep 1
sample :loop_3dprinter
sleep 1
sample :loop_drone_g_97
sleep 1
sample :loop_electric
sleep 1
sample :loop_mehackit_1
sleep 1
sample :loop_mehackit_2
sleep 1
sample :loop_perc_1
sleep 1
sample :loop_perc_2
sleep 1
sample :loop_weirdo
sleep 1

### Tabla
sample :tabla_tas1
sleep 1
sample :tabla_tas2
sleep 1
sample :tabla_tas3
sleep 1
sample :tabla_ke1
sleep 1
sample :tabla_ke2
sleep 1
sample :tabla_ke3
sleep 1
sample :tabla_na
sleep 1
sample :tabla_na_o
sleep 1
sample :tabla_tun1
sleep 1
sample :tabla_tun2
sleep 1
sample :tabla_tun3
sleep 1
sample :tabla_te1
sleep 1
sample :tabla_te2
sleep 1
sample :tabla_te_ne
sleep 1
sample :tabla_te_m
sleep 1
sample :tabla_ghe1
sleep 1
sample :tabla_ghe2
sleep 1
sample :tabla_ghe3
sleep 1
sample :tabla_ghe4
sleep 1
sample :tabla_ghe5
sleep 1
sample :tabla_ghe6
sleep 1
sample :tabla_ghe7
sleep 1
sample :tabla_ghe8
sleep 1
sample :tabla_dhec
sleep 1
sample :tabla_na_s
sleep 1
sample :tabla_re
sleep 1


### Vinyl
sample :vinyl_backspin
sleep 1
sample :vinyl_rewind
sleep 1
sample :vinyl_scratch
sleep 1
sample :vinyl_hiss
sleep 1

### Glitchy sounds
sample :glitch_bass_g
sleep 1
sample :glitch_perc_1
sleep 1
sample :glitch_perc_2
sleep 1
sample :glitch_perc_3
sleep 1
sample :glitch_perc_4
sleep 1
sample :glitch_perc_5
sleep 1
sample :glitch_robot_1
sleep 1
sample :glitch_robot_2
sleep 1

### Mehackit sounds
sample :mehackit_phone_1
sleep 1
sample :mehackit_phone_2
sleep 1
sample :mehackit_phone_3
sleep 1
sample :mehackit_phone_4
sleep 1
sample :mehackit_robot_1
sleep 1
sample :mehackit_robot_2
sleep 1
sample :mehackit_robot_3
sleep 1
sample :mehackit_robot_4
sleep 1
sample :mehackit_robot_5
sleep 1
sample :mehackit_robot_6
sleep 1
sample :mehackit_robot_7
sleep 1
