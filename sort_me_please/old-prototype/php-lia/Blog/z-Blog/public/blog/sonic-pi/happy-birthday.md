# Happy Birthday in Sonic Pi
This is a very simplistic rendition of the Happy Birthday song, played with [Sonic Pi](https://sonic-pi.net/). Thanks to Musescore for [the sheet music](https://musescore.com/user/173585/scores/166951)

I wrote it in code from the sheet music. `play 60` is like playing middle-c on a piano.
To play notes procedurally, it was necessary to mix sustain & sleep like so:
```python
play 60, sustain: 1
sleep 1
play 61 ...
```  

Of note was the need to set the beats-per-minute with `use_bpm 90`. As the sheet music is in `3/4 time`, I THINK this is correct. I think that's how it works, but I'm very novice at music & even more novice at sonic pi (though much more experienced coding in general).

## Happy Birthday Code
I have not gone through it again to ensure I got the notes correct
```python
# Welcome to Sonic Pi v2.10
#60 = Middle C
#67
use_bpm 90
use_synth piano
# 1 measure = 3 beats (3/4 time). 
# if 120-bpm, then 3 beats per measure means 40 measures per minute
#    this yields 60sec/40measures = 1.33 (or 3/2) seconds per measure
# But I want one measure to = 1 second
# So, I switched to 90 bpm

# first measure
sleep 2
play 64, sustain: 0.5
sleep 0.5
play 64, sustain: 0.5
sleep 0.5
play 65, sustain: 1
play 57, sustain: 2
play 55, sustain: 2
play 53, sustain: 2
sleep 1
play 64, sustain: 1
sleep 1

# 2nd measure
play 67, sustain: 1
sleep 1
play 66, sustain: 2
play 57, sustain: 2
play 56, sustain: 2
play 54, sustain: 2
sleep 2

# 3rd measure
play 64, sustain: 0.5
sleep 0.5
play 64, sustain: 0.5
sleep 0.5
play 65, sustain: 1
play 57, sustain: 2
play 56, sustain: 2
play 52, sustain: 2
sleep 1
play 64, sustain: 1
sleep 1

# 4th measure
play 68, sustain: 1
sleep 1
play 67, sustain: 2
play 57, sustain: 2
play 56, sustain: 2
play 54, sustain: 2
sleep 2

# 5th measure
play 64, sustain: 0.5
sleep 0.5
play 64, sustain: 0.5
sleep 0.5
play 71, sustain: 1
play 60, sustain: 2
play 58.5, sustain: 2
play 55, sustain: 2
sleep 1
play 69, sustain: 1
sleep 1

#6th Measure
play 67, sustain: 1
sleep 1
play 66, sustain: 1
play 60, sustain: 2
play 58, sustain: 2
play 56, sustain: 2
sleep 1
play 65, sustain: 1
sleep 1

# 6th Measure
play 70, sustain: 0.5
sleep 0.5
play 70, sustain: 0.5
sleep 0.5
play 69, sustain: 1
play 60, sustain: 2
play 57, sustain: 2
play 55, sustain: 2
sleep 1
play 67, sustain: 1
sleep 1

# 7th measure, final
play 68, sustain: 1
play 57, sustain: 1
play 56, sustain: 1
play 54, sustain: 1
sleep 1
play 64, sustain: 2

play 57, sustain: 2
play 55, sustain: 2
play 53, sustain: 2
sleep 2
```
