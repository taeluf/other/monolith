
# Apache Won't load after PHP Upgrade
I upgraded ubuntu from 19 to 20. In the process PHP 7.3 was removed & 7.4 was added

Then apache wouldn't start
`sudo apachectl restart` gave an error pointing at `/etc/apache2/apache2.conf`, and subsequently pointing at `/etc/apache2/mods-enabled/php-7.3.load`,  so I opened it & saw it was pointing to `/usr/lib/apache2/modules/libphp7.3.so`... which turned out not to exist any more.

My `/etc/apache2/apache2.conf` has wildcard loading of enabled mods:
```apacheconf
IncludeOptional mods-enabled/*.load
IncludeOptional mods-enabled/*.conf
```

So I `cd /etc/apache2/mods-enabled; ls;` & confirmed the php-7.3 .load & .conf are there. I move them with `sudo mv php-7.3.conf ~/Documents/temp-backup/php-7.3.conf;sudo mv php-7.3.load ~/Documents/temp-backup/php-7.3.load;`, which isn't really necessary because these are, I believe, the basic php 7.3 install files. I never modified them, anyway.

The php-7.3 module files are gone now (because I used `mv` instead of `cp`).

## Reinstall PHP
For good measure, I remove 7.3 before installing PHP 7.4
```bash
sudo apt-get remove php7.3
sudo apt autoremove # Once upon a time, my system broke because of this. Good thing I had backed up the list of packages
sudo apt-get install php7.4
sudo apt-get install php # Not sure if I needed this, but I did it anyway
sudo apt-get update # for good measure
sudo apt-get upgrade
```

Okay, so `sudo apachectl start`
**Woohoo!!!** Success.
Go to `dev.localhost` in the browser AND!!!
... I see PHP code.  
    :(
    
So I do a [quick search](https://www.ecosia.org/search?q=apache+not+running+php&addon=firefox&addonversion=4.0.4), ending up on [techrepublic](https://www.techrepublic.com/article/how-to-fix-apache-2-not-executing-php-files/). 
They suggest adding a `<FilesMatch \.php$>\nSetHander application/x-httpd-php\n</FilesMatch>`  (note the two \n for new lines) to `/etc/apache2/apache2.conf`, which I try... running `sudo apachectl restart` & refreshing my `dev.localhost`. 
Nothing.
They suggest running `a2dismod mpm_event` & `a2enmod mpm_prefork`, which I ignore. Though I DO use `sudo a2enmod php7.4`
Then `sudo apachectl restart`
And everything is working! 

## TL;DR
You may want to change the php versions from 7.3 & 7.4 & you may not need the removal step.
```bash
mkdir ~/Documents/temp-backup/;
sudo mv /etc/apache2/mods-enabled/php-7.3.conf ~/Documents/temp-backup/php-7.3.conf;
sudo mv /etc/apache2/mods-enabled/php-7.3.load ~/Documents/temp-backup/php-7.3.load;
sudo apt-get remove php7.3
sudo apt autoremove # Once upon a time, my system broke because of this. Good thing I had backed up the list of packages
sudo apt-get install php7.4
sudo apt-get install php # Not sure if I needed this, but I did it anyway
sudo apt-get update # for good measure
sudo apt-get upgrade
sudo a2enmod php7.4
sudo apachectl start
```
You also might just skip that `mv` / backup step. If you want to keep the old PHP version, you might need to remove it & reinstall it
