# Load Internal File from Javascript | Firefox Extension & Addon

Scouring through the [MDN Extensions Documentation](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions), I had an incredibly hard time finding out how to include an html file's content in my javascript content-script. I found that one must use [web_accessible_resources](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/web_accessible_resources) to declare which resources are available, then make a "simple" web request to get the file's content.

The example today will be adding a simple (& ugly) control panel to a website (specifically [StardewValleyWiki](https://stardewvalleywiki.com/)). It's sole function will be to show a control panel on top of the page, style it with a CSS file, and move the `#searchform` into the popup control panel. Note that this approach does not use any of the documented [user interface methods](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/user_interface).

**Disclaimer** I'm writing this tutorial after creating my first FF extension & I may not be using best (or even good) practices. It did work, though.

## How to load an internal extension file into a javascript script
1. Add the file to [`web_accessible_resources`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/web_accessible_resources) of your [**manifest.json**](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json) like so:  
    ```json
        {
            //name, description, etc
            "content_scripts": ["/script/init.js"],
            "web_accessible_resources": ["/dir/the-file.ext"]
        }
    ```
    **NOTE:** Any page & any extension can access the `web_accessible_recources` of your extension, as long as they can get your extension's url prefix (& I believe they can, but I don't know how that works).  
    **NOTE 2:** Following code could go in one of your [`content_scripts`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/content_scripts), listed in your [manifest.json](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json).
2. **Get the internal url** of the file:  
    ```js
    const resourceUrl = browser.runtime.getURL("/dir/the-file.ext");
    ```
3. Use the `resourceUrl` to load a file in the manner you need:  
  a. **Raw File Content** - If you wish to do work with the string representation of the file, you send a web request to the url for the resource.
     ```js
        const resourceUrl = browser.runtime.getURL("/dir/the-file.ext");
        fetch(resourceUrl)
        .then(response => {
            // return response.json(); // if you want a JSON object. The file has to contain valid JSON.
            return response.text();
        }).then(rawFileContent => {
            //doSomething with it
            console.log(rawFileContent);
        });
     ```  
     Check out [my Request class](https://github.com/ReedyBear/JS-Request/) to save some boilerplate code & make web-requests easier (especially with POST requests).  

  b. **Add HTML** to the main document `<body>`.
    Declare this function, which will add the raw file string to the document:   
    ```js
    function loadHtmlString(html,parentNode = 'document.body'){
        if (parentNode==='document.body')parentNode = document.body;
        var wrapper  = document.createElement('div'); //you could use a span here, if you want inline-styling, or customize styles through js. 
        wrapper.innerHTML = html;
        parentNode.appendChild(wrapper);
    }
    ```
    Then use the **Raw File Content** approach to get the HTML String & call upon `loadHtmlString`
    ```js
    const resourceUrl = browser.runtime.getURL("/dir/the-file.ext");
    fetch(resourceUrl).then(response => {return response.text();}).then(
        function(rawFileContent) {
            loadHtmlString(rawFileContent); //adds to document.body
            //or loadHtmlString(rawFileContent, nodeOfYourChoice);
        });
    ```
          
  c. **Add stylsheet** to the main document `<head>`  (call `loadStyle(resourceUrl)`)
     ```js
    function loadStyle(href){
        // avoid duplicates
        for(var i = 0; i < document.styleSheets.length; i++){
            if(document.styleSheets[i].href == href){
                return;
            }
        }
        var head  = document.getElementsByTagName('head')[0];
        var link  = document.createElement('link');
        link.rel  = 'stylesheet';
        link.type = 'text/css';
        link.href = href;
        head.appendChild(link);
    }
     ```
  d. **Add script** to the main document `<head>`  (call `loadScript(resourceUrl)`)
     ```js
    function loadScript(src){
        // avoid duplicates
        for(var i = 0; i < document.scripts.length; i++){
            if(document.scripts[i].src == src){
                return;
            }
        }
        var head  = document.getElementsByTagName('head')[0];
        var script  = document.createElement('script');
        script.type = 'text/javascript';
        script.src = src;
        head.appendChild(script);
    }
     ```
  e. **Add image** to the main document... Basically the same as *c.* & *d.*, except you `document.createElement('img')` & you add it somewhere in the `<body>`. Don't forget to **add an [`alt attribute`](https://www.w3schools.com/TAGS/att_img_alt.asp), for visually impaired users**   


## Sample application loading internal resource files. 
Check out the [Hello World extension](/firefox-extension/hello-world/) if this is your first time making a Firefox Extension.  

We'll build a simple control panel overlay for [Stardew Valley Wiki](https://stardewvalleywiki.com). The search box on the page will be added to a `position:fixed` div & that search box will be focused everytime a wiki page is loaded or focused. (alt+tab from one app back to the browser)

### 1. Create the skeleton  
1. Create a directory named `rbear-stardew`. Perhaps in `Documents/Firefox Extensions` or inside your usual code workspace, or any other directory of your choice.
2. Create files as follows (content to follow):
   ```
    rbear-stardew
        - manifest.json  //extension settings
        | view
            - panel.html  //html for the control panel
        | script
            - init-panel.js //to set everything up
            - RB-Request.js //to help with sending requests
            - functions.js  //helper functions for adding files to the page
        | style
            - panel.css  //style the control panel
   ```

### 2. Add code to all the files

#### manifest.json
The [`manifest.json`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json) contains all the settings for the extension.   
Pay special attention to [`content_scripts`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/content_scripts) and [`web_accessible_resources`](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/web_accessible_resources).
```json
{
    "manifest_version": 2,
    "name": "StardewValleyWiki Panel",
    "version": "1.0",
    "description": "Adds a (bad) control panel to StardewValleyWiki.com",
    "content_scripts": [
        {
            "matches": [
                "*://stardewvalleywiki.com/*"
            ],
            "js": [
                "/script/RB-Request.js",
                "/script/functions.js",
                "/script/init-panel.js"
            ]
        }
    ],
    "web_accessible_resources": ["style/panel.css","view/panel.html"]
}
```  
The order of the scripts in `content_scripts` is very important. Those scripts are loaded in the order they're listed & `init-panel.js` needs the prior two scripts to be loaded already.

#### view/panel.html
This is a very simple panel which will float on top of the page, thanks to `style/panel.css`.  
```html
<div id="rbear_wiki_panel">
    <h6>Control Panel</h6>
</div>
```  

#### style/panel.css
This makes your HTML panel float. & it's super ugly.
```css
#rbear_wiki_panel {
    background:rgba(0,0,0,0.4);
    position:fixed;
    right:0;
    top:0;
    width:250px;
    height:200px;
}
```  

#### script/init-panel.js  
This will set everything up. Note that it depends upon `RB-Request.js` and `functions.js`  
```js
    function focusSearch(){
        const searchInput = document.getElementById("searchInput"); //pre-existing component on the StardewWiki
        searchInput.focus();
    };
    function doSetup(){
            const cssUrl = browser.runtime.getURL('/style/panel.css');
            loadStyle(cssUrl);

            const viewUrl = browser.runtime.getURL('/view/panel.html');
            const request = new RB.Request(viewUrl);
            request.handleText(function(rawHtml){
                loadHtmlString(rawHtml);
                const form = document.getElementById("searchform"); //pre-existing component on the StardewWiki
                    form.parentNode.removeChild(form);
                const wikiPanel = document.getElementById("rbear_wiki_panel"); //prefix, to avoid collisions with resources on the site.
                    wikiPanel.appendChild(form); //moved the searchform from the page as normal over to the floating panel
                focusSearch();
            });
    }
    window.addEventListener("focus",focusSearch); //so the search bar gets focus every time the page is focused
    document.body.addEventListener("load",doSetup); //in case the body isn't loaded yet... 
    doSetup(); //the body probably is already loaded, so 'load' won't be called. This is a bad approach.
```  
You can see we call `loadStyle(cssUrl)` above. This is because, if we embed a `<link>` inside `/view/panel.html`, it will load `stardewvalleywiki.com/view/panel.html`, since the html is being added to the main document's body. There are probably better ways to handle this.  

#### script/RB-Request.js
Get an up-to-date [`Request class`](https://github.com/ReedyBear/JS-Request/) on github.  
```js
if (typeof RB === 'undefined')var RB = {};
RB.Request = class {
    constructor(url, method){
        this.params = {};
        this.url = url;
        this.method = method ?? 'POST';
    }
    put(key,value){
        if (key in this.params){
            this.params[key] = (typeof this.params[key]==typeof []) ? this.params[key] : [this.params[key]];
            this.params[key].push(value);
        } else {
            this.params[key] = value;
        }
    }

    handleJson(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            body: formData
        }).then(res => {
            return res.json();
        }).then(json => {
            func(json);
        });
    }
    handleText(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            body: formData
        }).then(res => {
            return res.text();
        }).then(text => {
            func(text);
        });
    }
}
```

#### script/functions.js
```js
function loadStyle(href){
    // avoid duplicates
    for(var i = 0; i < document.styleSheets.length; i++){
        if(document.styleSheets[i].href == href){
            return;
        }
    }
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    head.appendChild(link);
}
function loadHtmlString(html,parentNode = 'document.body'){
    if (parentNode==='document.body')parentNode = document.body;
    var wrapper  = document.createElement('div'); //you could use a span here, if you want inline-styling, or customize styles through js. 
    wrapper.innerHTML = html;
    parentNode.appendChild(wrapper);
}
```

### 3. Run the extension

1. Open [https://stardewvalleywiki.com](https://stardewvalleywiki.com) in Firefox & look at the search bar in the top-right
2. Go to `about:debugging` in a new tab (type that into the url bar)
3. click 'This Firefox'
4. click 'Load Temporary Addon'
5. Choose the 'manifest.json' of your extension
6. Refresh the Stardew wiki page.
7. Click on the page (so search bar not focused). 
8. Alt+tab to a different program, then alt+tab back & type "tractor" (or whatever)