# Godot Hot Code
Code I want to keep track of and maybe share

## Get Screen Size
Tags: gdscript
```gdscript
OS.get_screen_size() # The full computer screen size
OS.get_window_size() # Just gets the game window
get_viewport().size # .x / .y for width/height. Just the game window
```

## Get Mouse Position
Tags: gdscript
```
var pos = get_viewport().mouse_position()
```
