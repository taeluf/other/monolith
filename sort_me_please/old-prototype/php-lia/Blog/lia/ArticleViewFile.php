<?php

namespace ROF\Article;

interface ArticleViewInterface {}

class ArticleViewFile extends \Lia\ViewFile implements ArticleViewInterface{


    protected $article;
    

    protected function initiateParams($params=[]){
        // parent::initiateParams($params);
        $this->params['article'] = $params[0];
        $this->article = $params[0];
    }

    protected function articleUrl(){
        return $this->cleanUrl($this->article->url);
    }

}