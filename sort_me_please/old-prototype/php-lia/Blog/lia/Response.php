<?php

namespace ROF\Article;

class Response extends \Lia\Response {


    public function __construct(...$params){
        parent::__construct(...$params);

    }

    protected function buildSpecialResponse(){
        
        $pdo = $this->get('pdo');
        $url = $this->url;
        $url = parse_url($url,PHP_URL_PATH);
        $statement = $pdo->prepare("SELECT * FROM rof_articles WHERE url LIKE :url");
        $statement->execute(
            array(
                ":url" => $url
                )
            );
        $rows = $statement->fetchAll();
        if (count($rows)>1){
            $this->type='error';
            $this->errorContent = "There was more than one article for url '{$url}'. That's not allowed.";
            return;
        } else if (count($rows)<1){
            $this->type = 'error';
            $this->errorContent = "<p>There were no articles for the url '{$url}'.</p>";
            $this->errorContent .="<p><a href=\"".$this->absoluteUrl('/')."\">Return to Home Page</a></p>";

            if ($this->allows('rof-article-create')){
                $this->errorContent .="<p><a href=\"".$this->urlWithBase('/create/').'?url='.urlencode($this->url)."\">Create Article</a></p>";
            }
            // $this->isSuccessful=false;
            return;
        }
        // else {
        //     $this->type = 'page';
        //     $this->content = 'blahblah';
        //     $this->isSuccessful = true;
        //     return;
        // } 
        // else {
        //     // $this->type = 'page';
        //     // $this->errorFile = $this->dir('error','/create-article.php');
        //     // $this->isSuccessful=true;
        //     // $this->content = 'this should be overridden by '.$this->errorFile;
        //     // return;
        //     var_dump('article bla');exit;
        // }

        $article = \ROF\Article::fromRow($rows[0]);

        $this->type = 'page';
        $this->isSuccessful = TRUE;
        $this->content = $this->view('Article',$article);
    }

}