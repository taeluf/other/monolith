

class Blogger extends RB.Autowire {

    actions = [];

    keysup = {};
    keysdown = {};


    map = {
        "Control": "ctrl"
    }

    attach(){
        this.node.setAttribute('contentEditable','');
        Blogger.instance = this;
        this.selected = new BloggerSelected(this.node);
        this.node.addEventListener('keyup',this.onkeyup.bind(this));
        this.node.addEventListener('keydown',this.onkeydown.bind(this));
    }

    onkeyup(event){
        // event.preventDefault();

        const key = this.map[event.key] ?? event.key;
        // console.log('up:'+key);
        this.keysup[key] = key;
        delete this.keysdown[key];
    }
    onkeydown(event){
        // event.preventDefault();

        const key = this.map[event.key] ?? event.key;
        // console.log('down:'+key);
        this.keysdown[key] = (this.keysdown[key] ?? 0) + 1;
        // console.log(this.keysdown[key]);
        const combo = Object.keys(this.keysdown).join('+');
        let firstClick = true;
        for (const index in this.keysdown){
            const c = this.keysdown[index];
            if (c>1)firstClick=false;
        }
        for (const action of this.actions){
            if (action.key==combo){
                event.preventDefault();
                if (firstClick){
                    action.action(this);
                }
            }
        }

        console.log(combo);
    }


    addAction(action,key,name){
        this.actions.push({"action":action,"key":key,"name":name});
    }
}

Blogger.autowire();


RB.Tools.onPageLoad(function(){

const blogger = Blogger.instance;
blogger.addAction(
    function(content, sValue){
        document.execCommand('bold', false, sValue); content.node.focus();


        // console.log(content);
        // content.selected.wrap('<b>','</b>');
    },
    "ctrl+b",
    "bold"
);
})


class BloggerSelected {

    constructor(node){
        this.node = node;
    }

    wrap(open,close){
        console.log(open,close);

        var range = document.createRange();
        range.selectNodeContents(this.node);
        var sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
        console.log(sel.toString());
    }

}