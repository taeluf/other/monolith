<?php

namespace ROF;

class Article {


    public $id;
    public $blurb;
    public $markdownBody;
    public $htmlBody;
    public $title;
    public $slug;
    public $url;
    public $category;
    public $authorId;
    public $status;


    static protected $pdo = null;

    static public function setPdo($pdo){
        self::$pdo = $pdo;
    }

    static public function all($order){
        $pdo = self::$pdo;
        $order = "ORDER BY title";
        $statement = $pdo->prepare("SELECT * FROM rof_articles {$order}");
        $statement->execute();
        
        $articles = [];
        foreach ($statement->fetchAll() as $row){
            $articles[] = self::fromRow($row);
        }
        return $articles;
    }

    static public function fromQueryString($queryString,$params=[]){
        $pdo = self::$pdo;
        $statement = $pdo->prepare($queryString);
        if (count($params)>0)$statement->execute($params);
        else $statement->execute();
        $articles = [];
        foreach ($statement->fetchAll() as $row){
            $articles[] = self::fromRow($row);
        }
        return $articles;
    }

    static public function fromCategory($categoryName, $order){
        $categoryName = strtolower($categoryName);
        $pdo = self::$pdo;
        $order = "ORDER BY title";
        $statement = $pdo->prepare("SELECT * FROM rof_articles WHERE category LIKE :category {$order}");
        $statement->execute(
            array(
                ":category" => $categoryName,
            )
        );
        $articles = [];
        foreach ($statement->fetchAll() as $row){
            $articles[] = self::fromRow($row);
        }
        return $articles;
    }   

    static public function fromUrl($url){
        $pdo = self::$pdo;
        $statement = $pdo->prepare("SELECT * FROM rof_articles WHERE url LIKE :url");
        
        $success = $statement->execute(
            [':url' => $url]
        );
        $rows = $statement->fetchAll();
        $article = ($rows[0] ?? false) ? \ROF\Article::fromRow($rows[0]) : false;
        return $article;
    }

    static public function cleanSlug($slug){
        while(substr($slug,0,1)=='/')$slug = substr($slug,1);
        while(substr($slug,-1)=='/')$slug = substr($slug,0,-1);
        return $slug;
    }

    static public function fromSlug($slug){
        $slug = self::cleanSlug($slug);
        $pdo = self::$pdo;
        $statement = $pdo->prepare("SELECT * FROM rof_articles WHERE slug LIKE :slug");
        $success = $statement->execute(
            [':slug' => $slug]
        );
        $rows = $statement->fetchAll();
        if (count($rows)==0)return false;
        $article = \ROF\Article::fromRow($rows[0]);
        return $article;
    }
    
    static public function fromRow($row){
        static $parsedown;
        if ($parsedown == NULL)$parsedown = new \Parsedown();
        $article = new \ROF\Article();
        $article->id = $row['id'];
        $article->blurb = $row['blurb'];
        $article->markdownBody = $row['body'];
        $article->htmlBody = $parsedown->text($row['body']);
        $article->title = $row['title'];
        $article->slug = $row['slug'];
        $article->url = $row['url'];
        $article->category = $row['category'];
        $article->authorId = $row['author_id'];
        $article->status = $row['status'];
        
        return $article;
    }

    static public function getCategories($includeDefault = TRUE){
        $categories = [];
        $categories = array_combine($categories,$categories);
        $pdo = self::$pdo;
        $query = $pdo->prepare("SELECT DISTINCT category FROM rof_articles ORDER BY category ASC");
        $query->execute();
        
        foreach ($query->fetchAll() as $row){
            $categories[$row['category']] = $row['category'];
        }

        return $categories;
    }
    

    public function locks($action){
        switch ($action){
            case 'edit': return ['rof-article-edit:any','rof-article-edit:id-'.$this->id];
            case 'create': return ['rof-article-create'];
            default: throw new \Exception("There are no locks for given action '{$action}'");
        }
    }
    
}
