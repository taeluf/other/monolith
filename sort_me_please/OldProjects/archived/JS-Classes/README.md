# JS-Classes (under construction)
Object Oriented classy feel! Automatically attach to HTML Elements. Extend Parent Classes!

# ARCHIVED Oct 14, 2019
This is not necessary. My first version was years ago before Javascript had classes. It has classes now (and there are improvements to make to them!) so I'm going to use those. I might make a separate library which serves as my JS Framework. Why don't I use react or vue or one of the many pre-existing ones? Because where's the fun in that?

## Features
* Declare classes in an object oriented fashion
* Extend classes (currently has some bugs)
* auto-attach classes to html elements

## Installation
Include `jsclasses.js` prior to any class declarations

## Usage
**Declare a class**   

    JSClass("MyClass", closure);
**Extend a class**

    JSClass("MyChild extends MyClass", closure);
**Assign a class to an HTML Element**

    <input type="button" class="jsclass jsclass-MyClass" value="Click Me!">
**Make a closure**
A closure is simply a function, represented by `function(){}`. Here's an example of a class declaration with a closure and a call to a function:

    JSClass("Alerter",
        function(){
            this.alertUser = function(message){
                alert(message);
            }
         }
    );
    var alerter = new Alerter();
    alerter.alertUser("This is a stupid class with no meaningful function");
    
**With a constructor**
You can add a constructor to your closure, which serves as the class body. Here, you'll also declare an instance variable.

    JSClass("BetterAlerter",
        function(){
            this.message = null;
            this.construct = function(message){
                this.message = message;
            }
            this.alertUser = function(){
                alert(this.message);
            }
        }
    );
    var betterAlerter = new BetterAlerter("I mean, that's cool. But this is still a useless class");
    betterAlerter.alertUser();
    
**Extend a class**
You can even extend a class! So we're going to make something else useless. It's not currently working in the object oriented way I intended.

    JSClass("AlerterLogger extends BetterAlerter",
        function(){
            this.alertUser = function(){
                console.log("this will be alerted: \n"+message);
                this.parent.alertUser();
            }
        }
    );
    var logger = new AlerterLogger("Well, now we're doing a little more. Sorry for all the alerts.");
    logger.alertUser();
    
**Attach to an HTML Element**

    <div class="jsclass jsclass-ColorChanger">Some small content</div>
Then the javsacript:

    JSClass("ColorChanger",
        function(){
            this.colors = ["red","orange","yellow","green","blue","indigo","violet"];
            this.counter = 0;
            this.attach = function(dom){
                //here, this refers to the instantiated ColorChanger
                dom.onclick = function(){
                    //here, this refers to the html element
                    var index = this.jsclass.counter%7;
                    this.style.background = this.jsclass.colors[index];
                    this.jsclass.counter++;
                    
                }
            }
        }
    );

## Problems
* Extension does not work for static functions and properties
* Extension copies all parent methods & variables into child class, and overwrites all parent methods. So, the child gets the parent methods if the child has not declared them. the child.parent object has its methods overwritten, though, which is incorrect. I should be able to call child.parent.overridenMethod() and get the parent's implementation of that method.
* there may be a constructor issue as well, but if so, it's related to extending classes.
