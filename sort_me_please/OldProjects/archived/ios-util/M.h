//
//  M.h
//  Free. No warranty. Do what you want with it.
// Created by Jakar in 2014
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface M : NSObject

+(void)logError:(NSError *)error withInformation:(NSString *)info, ...;
+(void)logInformation:(NSString *)info, ...;


+ (NSURL *)applicationDocumentsDirectory;

BOOL isPad();
NSString *boolString(BOOL b);


@end
