//
//  KeyboardStateListener.h
//  JakarUtilities
//
//  Created by Reed Sutman on 3/22/14.
//  Copyright (c) 2014 Jakar Co. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface KeyboardStateListener : NSObject{
    BOOL _isVisible;
    CGSize _keyboardSize;
}


+ (KeyboardStateListener *)sharedInstance;

@property (nonatomic, readonly) BOOL visible;
@property (nonatomic, readonly) CGSize keyboardSize;

@end
