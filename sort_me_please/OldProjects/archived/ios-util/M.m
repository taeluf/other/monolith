//
//  M.m
//  Free. No warranty. Do what you want with it.
// Created by Jakar in 2014
//

#import "M.h"


BOOL isPad(){
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
}
NSString *boolString(BOOL b){
    return b ? @"YES" : @"NO";
}

@implementation M //M for main. It makes it super easy to call in code

+(void)logError:(NSError *)error withInformation:(NSString *)info, ...{
//    return;
    NSString *string = [NSString string];
    if (info!=nil){
        va_list ap;
        va_start(ap, info);
        string = [[NSString alloc] initWithFormat:info arguments:ap];
    }
    
    if (error!=nil){
        NSLog(@"\n%@\nError:ld %@, lfr %@, lro %@, lrs %@",string, error.localizedDescription,error.localizedFailureReason,error.localizedRecoveryOptions,error.localizedRecoverySuggestion);
    } else {
        NSLog(@"\nPassed Info: %@ \nNSError was nil",string);
    }
    
}
+(void)logInformation:(NSString *)info, ...{
    //return;
    if (info==nil)return;
    va_list ap;
    va_start(ap, info);
    NSString *string = [[NSString alloc] initWithFormat:info arguments:ap];
    NSLog(@"%@",string);
    
}

+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


//+(NSE)


@end
