<?php

namespace Tiny;

class Response implements \Tiny\IFace\Response{


    protected $content;
    protected $redirectUrl;
    protected $baseUrl = '';

    protected $mimeHeader;
    protected $type;

    public function __construct($content=''){
        $this->content = $content;
        $this->type = 'html';
    }

    public function send(){
        $target = parse_url($this->redirectUrl,PHP_URL_PATH).'?'.parse_url($this->redirectUrl,PHP_URL_QUERY);
        $requested = parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH).'?'.parse_url($_SERVER['REQUEST_URI'],PHP_URL_QUERY);
        $destination = NULL;
        if ($this->type=='redirect:no-base'
            &&$target!==$requested){
                $destination = '/'.$target.'/';
        } else if ($this->type=='redirect'
            &&$target!==$requested){
                $destination = '/'.$this->baseUrl.'/'.$target.'/';
        } else {
            $mimeHeader = \ROF\Mime::getHeaderForExt($this->type);
            header($mimeHeader);
            echo $this->content;
            return;
        }
        $destination = str_replace(['////','///','//'],'/',$destination);
        
        if (strlen($this->content)>0){
            $destination = $this->getMessageDestination($destination,$this->content);
        }
        header("Location: ".$destination);
        if ($destination!=$_SERVER['REQUEST_URI']){
            
            return;
        }
        
    }

    public function output(){
        return $this->content;
    }

    public function setContent($content){
        $this->content = $content;
    }

    public function setType($type){
        if ($type==null)return;
        $this->type = $type;
    }
    public function getType(){
        return $this->type;
    }
    public function setRedirectUrl($newUrl){
        $this->redirectUrl = $newUrl;
    }

    public function setBaseUrl($baseUrl){
        $this->baseUrl = $baseUrl;
    }
    public function showAt($fileKey,$action='',$slug=''){
        $this->setType('redirect');
        $url = '/'.$fileKey.'/'.$slug.'/'.$action.'/';
        $url = str_replace(['////','///','//'],'/',$url);

        $this->setRedirectUrl($url);
    }

    protected function getMessageDestination($destinationUrl,$content){
        $query = parse_url($destinationUrl, PHP_URL_QUERY);
        $messageId = 'msg-'.uniqid();
        if (session_status()===PHP_SESSION_NONE)session_start();
        $_SESSION[$messageId] = $content;
        if ($query) {
            $destinationUrl .= '&msgId='.$messageId;
        } else {
            $destinationUrl .= '?msgId='.$messageId;
        }
        return $destinationUrl;
    }
        
    public function message(){
        if (session_status()===PHP_SESSION_NONE)session_start();
        $msgId = $_GET['msgId'] ?? false;
        $content = $_SESSION[$msgId] ?? false;
        unset($_SESSION[$msgId]);
        $message = $content ? (object)['id'=>$msgId,'content'=>$content] : false;
        return $message;
    }
}