<?php

namespace Tiny;

class PubFile implements \Tiny\IFace\PubFile {


    protected $content;
    protected $type;
    protected $methods = [];
    protected $params = [];
    protected $file;

    public function __construct($filePath){
        $this->file = $filePath;
        $this->type = pathinfo($filePath,PATHINFO_EXTENSION);
        if ($this->type=='php')$this->type = 'html';
        // $this->cargo = $cargo;
        // $this->envoy = $cargo->envoy;
        // $this->path = $cargo->filePath;
        // $this->slug = $cargo->slug;
        // $this->viewName = $cargo->viewName;
        // $this->action = $cargo->action;
        // $this->user = $cargo->user ?? null;
        // $this->lock = $cargo->lock ?? null;
        // $this->route = $cargo->route;
        // $this->apps = $cargo->apps;

        // $this->frameType = 'frame';
        // $this->contentType = $this->getContentType();
        // $this->content = $this->getContent();
    }

    protected function pushMethod($name,$callable,$override,$throw){
        if (isset($this->methods[$name])){
            if ($override){
                $this->methods[$name] = $callable;
                return true;
            } else if ($throw) {
                throw new \Exception("A method with name '{$name}' has already been exposed. "
                ."Error for object with class '".get_class($object)."'\n");
            }
            return false;
        }
        $this->methods[$name] = $callable;
        return true;
    }
    protected function pushParam($name,$value,$override,$throw){
        if (isset($this->params[$name])){
            if ($override){
                $this->params[$name] = $value;
                return true;
            } else if ($throw) {
                throw new \Exception("A paramater with name '{$name}' already exists.");
            }
            return false;
        }
        $this->params[$name] = $value;
        return true;
    }
    public function exposeMethod($name,$callable){
        $this->pushMethod($name,$callable,false,true);
    }
    public function exposeObjectMethods($object,$methods){
        foreach ($methods as $methodName){
            $this->pushMethod($methodName,[$object,$methodName],false,true);
        }
    }
    public function exposeObjectParams($object,$params){
        foreach ($params as $param){
            $this->pushParam($param,$object->$param,false,true);
        }
    }
    public function exposeParam($paramName,$value){
        $this->pushParam($paramName,$value,false,true);
    }

    public function content(){
        if ($this->content!==null)return $this->content;

        $content = '';
        ob_start();
        include($this->file);
        $content = ob_get_clean();

        $this->content = $content;
        return $content;
    }

    // protected function showResponseAt($fileKey,$action='',$slug=''){
    //     $this->type = 'redirect-display';
    //     $this->
    // }

    public function __call($methodName,$params){
        if (isset($this->methods[$methodName])){
            $callable = $this->methods[$methodName];
            if (!is_callable($callable)){
                throw new \BadMethodCallException("Exposed method '{$methodName}' does not exist for callable: '".print_r($callable,true)."'");
            }
            return call_user_func_array($callable,$params);
        }
        throw new \BadMethodCallException("Method '{$methodName}' does not exist on ".get_class($this)." & has not been exposed to it through the expose-methods.\n");
    }

    public function __get($param){
        if (isset($this->params[$param])) return $this->params[$param];
        else if ($this->has($param)){
            return $this->get($param);
        }
        throw new \OutOfRangeException("Paramater '{$param}' does not exist on ".get_class($this)." & has not been exposed to it through the expose-methods.\n");

    }

    public function getType(){
        return $this->type;
    }













    // public function getContentType(){
    //     if ($this->contentType!=null)return $this->contentType;
    //     $ext = pathinfo($this->path, PATHINFO_EXTENSION);
    //     // if ($ext==null)
    //     return $ext;
        
    // }

    // public function getContent(){
    //     // echo "content";
    //     // var_dump(get_class($this));
    //     // var_dump($this->cargo);
    //     // if ($this->contentType!==null)return $this->contentType;
    //     if (!is_file(realpath($this->path))){
    //         // var_dump($this->apps);
           
    //         foreach ($this->apps as $app){
    //             // echo 'app ap app app app ap app';
    //             if ($app->isRequested()){
    //                 $response = $app->getResponse();
    //                 ob_start();
    //                 $response->send();
    //                 $this->contentType = $response->type;
    //                 return ob_get_clean();
    //             }
    //         }
    //         // echo '::'.__DIR__.'::'."\n\n<br><br>";
    //         // echo new \Exception('');
    //         // echo "\n\n<br><br>";
    //         // echo get_class($this)."\n\n<br><br>";
    //         // echo '[--no sub apps connected at this url --]';
    //         // return;
    //     }
    //     if (!is_file($this->path)){
    //         return NULL;
    //     }
    //     ob_start();
    //     include($this->path);
    //     $content = ob_get_clean();
    //     $content = $this->header.$content.$this->footer;
    //     return $content;
    // }

    // public function setPath($path){
    //     $this->path = $path;
    // }

    // protected function get($key){
    //     return $this->envoy->get($key);
    // }

    // public function url(...$params){
    //     $urlFunc = [$this->route,'url'];
    //     $url = call_user_func_array($urlFunc,$params);
    //     return $url;
    // }

    // public function showResponseAt(...$params){
    //     $urlFunc = [$this->route,'url'];
    //     $url = call_user_func_array($urlFunc,$params);
    //     $this->cargo->redirectUrl = $url;
    //     $this->contentType = "redirect";
    // }
    // // public function showErrorAt(...$params){

    // // }
    // public function startWith($html){
    //     $this->header = $html;
    // }
    // public function endWith($html){
    //     $this->footer = $html;
    // }

    // public function message(){
    //     if (session_status()===PHP_SESSION_NONE)session_start();
    //     $msgId = $_GET['messageId'] ?? false;
    //     $content = $_SESSION[$msgId] ?? false;
    //     unset($_SESSION[$msgId]);
    //     $message = $content ? (object)['id'=>$msgId,'content'=>$content] : false;
    //     return $message;
    // }

    // protected function view($viewName,$params){

    //     $file = $this->cargo->viewDir.'/../view/'.$viewName.'.php';
    //     foreach ($params as $varName=>$var){
    //         $$varName = $var;
    //     }
    //     ob_start();
    //     include($file);
    //     return ob_get_clean();
    // }

    // protected function allows(...$keys){
    //     if ($this->user==null)return false;
    //     return call_user_func_array([$this->user,'unlocksAny'],$keys);
    // }
}