<?php

namespace Tiny\IFace;

interface App {

    public function __construct($appDir,$baseUrl=NULL);

    public function getResponse($url=NULL);
    public function deliver($url=NULL);
    public function isRequested($url=NULL);
    
    public function addApp($app);
    public function loadApp($appDir,$baseUrl=NULL);
    
    public function setUser(\Tiny\IFace\User $user);
    // public function setDBPdo($pdoObject,$key='db-pdo');
    // public function setDBCredentials($user,$password,$extra=[]);
    // public function set($key,$value);
    // public function get($key);
    // public function send($msgKey,...$param);
    // public function receive($msgKey,$function,$priority=0);




}