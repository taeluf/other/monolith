<?php

namespace Tiny;

class Frame implements \Tiny\IFace\Frame {

    protected $output;

    protected $dir;
    protected $content;
    protected $defaultFrame;
    protected $scripts = [];

    public function __construct($frameDir,$content='',$defaultFrame='top-bottom'){
        if (!is_dir($frameDir)){
            $frameDir = NULL;
            $defaultFrame = NULL;
            // throw new \Exception("Directory path '{$frameDir}' does not point to an existing directory");
        }
        $this->dir = $frameDir;
        $this->content = $content;
        $this->defaultFrame = $defaultFrame;
    }

    protected function preContent(){
        $file = $this->dir.'/top.php';
        // echo "\n$file\n";
        if (file_exists($file)){
            include($file);
        }
    }
    protected function postContent(){
        $file = $this->dir.'/bottom.php';
        if (file_exists($file)){
            include($file);
        }
    }

    public function addScript($scriptName){
        // var_dump('add script');
        $this->scripts[$scriptName] = $scriptName;
        // var_dump($scriptName);
        // var_dump($this->scripts);

    }

    protected function headHtml(){
        // var_dump($this->scripts);
        // exit;
        $html = '';
        foreach ($this->scripts as $scriptName){
            $html .= '        <script type="text/javascript" src="/script/'.$scriptName.'.js">'."\n";
        }
        return $html;
    }
    
    public function setContent($content){
        $this->content = $content;
    }

    public function display($frame=null){
        $frame = $frame ?? $this->defaultFrame;
        if ($frame=='top-bottom'){
            $this->preContent();
            echo $this->content;
            $this->postContent();
        } else if ($frame==null||$frame==false){
            echo $this->content;
        } else {
            throw new \Exception("Frame '{$frame}' does not exist, or the functionality for including it is not set up.");
        }
    }
    
    public function output($frame=null){
        ob_start();
        $this->display($frame);
        return ob_get_clean();
    }


}