<?php


class Tiny {
    use \Tiny\Getter;

    static public function appFromDir($dir,$baseUrl=NULL){
        $app = '';
        $rails = self::getRails($dir);
        
        if ($app==null){
            $appRail = $rails['Tiny\\IFace\\App'] ?? '\\Tiny\\App';
            $app = ($baseUrl===NULL) ? new $appRail($dir) : new $appRail($dir,$baseUrl);
        }

        return $app;
    }


    

}


?>