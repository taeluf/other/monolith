<?php


$base = '/base/';
$url = '/base/0/?get_stuffs';
// $url = $_SERVER['REQUEST_URI'];

$url = parse_url($url,PHP_URL_PATH);
// $url = substr($url,0,strlen($base));
// $url = $url ?? '/';

$router = new \Tiny\Rail\Router(__DIR__.'/../../tiny-sample-app/public/',$base);

$paths = [
    '/',
    '/slug',
    '/about',
    '/about/slug',
    '/about/more/',
    '/about/slug/ask',
    '/list/',
    '/metadata',
    '/the_user/',
    '/am-style.css',

];

echo "<h1>Test Router</h1>";
echo "<pre>";
foreach ($paths as $path){
    $path = $base.'/'.$path;
    echo "\n{$path} \n    :".$router->filePath($path);
}
echo "</pre>";
