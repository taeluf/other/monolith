<?php

try {
    $site = 'www.maconzero.com';
//BEFORE HANDLING
//    enable autoloaders
require "vendor/autoload.php";
\ROF\Autoloader::enable(__DIR__.'/vendor/rof/'.$site.'/Class/');
//    OTHER pre-configurations
$pdo = \ROF\FN::getPdo("mysql:host=my.host.com;dbname=DB_NAME","DB_USER",'PASSWORD');
\ROF\Article\Config::getInstance()->initialize(["pdo"=>$pdo,"url"=>'/article']);
\ROF\User\Config::getInstance()->initialize(["pdo"=>$pdo,"url"=>'/user']);
$handler = \ROF\Handler::getInstance();
$handler->config = ["directory_index" => 'index.php', "site_dir" => __DIR__.'/vendor/rof/'.$site,
                    "public_dir" => 'Public', "sytles_dir" => 'Style', "script_dir" => 'Script'];
///$handler = \ROF\Handler::getInstance();
// $handler->setRoutes(
//     ['.css' => __DIR__.'/Style/',
//      '.js' => __DIR__.'/Script/',
//      '.php' => __DIR__.'/Public/',
//      'image' => __DIR__.'/Images/',
//     ]
//     );
    
// request routers
$routers = [
    "resource" => array($handler,'handleResource'),
    "article" => array(\ROF\Article\Handler::getInstance(),'routeRequest'),
    "user" => array(\ROF\User\Handler::getInstance(),'routeRequest'),
    "default" => array($handler,'handleRoute'),
    ];
    
    ob_start();
    foreach ($routers as $name=>$router){
        if (!is_callable($router))throw new \Exception("Router '{$name}' is not callable.");
        $response = call_user_func($router);
        if ($response===TRUE)break;
    }
$content = ob_get_clean();
  
$processors = [
     'tiny-mvc' => function($content) {
         $view = new \ROF\View\Controller(__DIR__.'/vendor/rof/www.maconzero.com');
         $view->load($content);
         $view->display();
         return TRUE;
     },
     'default' => array('\ROF\Handler','showContent'),
        ];


foreach ($processors as $name=>$processor){
    ob_start();
    $response = call_user_func($processor,$content);
    $content = ob_get_clean();
    if ($response===TRUE)break;
}
echo $content;
    
} catch (\Exception $e){
        echo "<pre>";
        print_r($e);
    echo "\n".$e->getTraceAsString();
        echo "</pre>";
    }


return;

?>
