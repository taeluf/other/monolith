<?php

require('vendor/autoload.php');

$handler = \ROF\Handler::getInstance();
$handler->setMultiConfig(
        ["devMode" => TRUE,
         "serverSource" => __DIR__,
         "siteSource" => __DIR__.'/vendor/rof/testing.testing/',
         ]
    );
$handler->go();


?>