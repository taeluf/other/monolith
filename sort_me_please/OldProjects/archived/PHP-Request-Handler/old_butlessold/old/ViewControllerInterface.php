<?php 

namespace ROF\Request;


/**
 * Defines most basic requirements for a view controller.
 * 
 * @author reedbear
 *
 */
interface ViewControllerInterface {
    
    /**
     * echos content
     * @param string $content
     */
    public function display();
    public function load($content);
    
    
}


?>
