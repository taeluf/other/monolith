# PHP Request Handler (under re-factor, not currently useable)
A request handler for PHP-powered websites. Routes URLs to DOCUMENT_ROOT/Public/url-path.extension

# Install
1. Copy the package files to your server (see below)
2. Copy `handle_request.php` and `.htaccess` from SITE_ROOT to the root of your website
3. Review `handle_request.php` and choose your document root (including leaving it as-is)
4. Create folders `Public` and `Class` in your document root
5. **Optional**, install a view controller, such as [github.com/ReedOverflow/PHP-View-Controller](https://github.com/ReedOverflow/PHP-View-Controller), or create your own
   * You must edit `handle_request.php`
6. Create scripts in `Public` directory, which will be delivered upon an HTTP request

### Composer Install
    "require": { "rof/request-handler": "1.*" }
Then follow the **Install**, step 2, above

### Manual Install
1. Create a directory at the root of your site called `Class`  
2. Copy the `Request` directory into the `Class` directory
3. Install the [Autoloader](https://github.com/ReedOverflow/PHP-Autoloader) (copy+paste Autoloader.php into `Class/ROF/`)
4. Follow **Install**, step 2, above.  
**Note:** `handle_request.php` will auto-include the autoloader if you put it in the right place.

# Usage
* Files that you want delivered must be created in the `Public` directory or a subdirectory of `Public`
* Any classes you want autoloaded should be created in the `Class` directory. See the [Autoloader](https://github.com/ReedOverflow/PHP-Autoloader) for more info.
* Create a custom `ViewController` by implementing \ROF\Request\ViewControllerInterface (see issues)
* See `handle_request.php` for additional usage information and configuration options

# URL Handling
* URL pointing to directory redirects internally to directory.'home.php'\
* URL pointing to directory but ending without a trailing slash will have a trailing slash added
* URL pointing to a PHP file with '.php' extension will be redirected to URL without '.php', with no trailing slash. '.php' will be added internally.
* URL pointing to a PHP file, with no '.php', with a trailing slash will be redirected to that URL without the trailing slash. '.php' will be added internally.
* If `/directory/` exists and is a directory AND `/directory.php` exists and is a PHP file, then a request for either `/directory` or `/directory/` will have end with the trailing slash and include the file at `/directory/home.php`. Thus, a PHP file cannot have the same name as the directory and be delivered.

# Future Plans
* Simplify installation process (not sure I can!?)
* Keep it simple. Framework-style functionality should never be required.
* Create a plethora of libraries that are independent of but mesh well with this one.
* Remove the autoloader from the library. Putting in a manual auto-loader to be used on non-composer installs
* Add a pre-load script option, for running PHP code prior to page delivery (currently, it must go in handle\_request.php)
* Add 'file-not-found' handling, regex pattern matching of urls, too

# Issues
* The ViewController implementation is not currently used. You don't actually have to `implement` as long as you have the two function `load($content)` which prepares content, then `display`, which will display the content. The RequestHandler will prepare the content and pass it to `load(..)`
* When a file is not found, there is an uncaught exception. This should be handled by the request handler and given a specific error message
* When a file is found but there is no content, an uncaught exception is thrown. Instead there should be an appropriate error message.
* There should be a list of recommended libraries to accompany this one, for enhanced features.
* Autoloading should be removed from the request handler as it is a separate library

# Changelog
* **1-25-2019**: fixed POST problem caused by URL rewriting
