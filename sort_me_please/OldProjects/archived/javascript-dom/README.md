# Archived
This has been replaced by https://github.com/ReedOverflow/JS-Classes, a much better library, and this one will receive no more updates.

It has more recently been replaced by Autowire, which i'm not gonna bother to link

javascript-dom
==============

a simple way to map css classes to javsacript classes. I might improve the docs and code with time, but I made this for myself, so I probably won't worry too much about it.

See <a href="http://stackoverflow.com/questions/26820050/javascript-library-to-manage-dom-actions-and-properties/26822707#26822707">my StackOverflow post</a> for an example of it actually working.

Call `JDOMProcessor.pageLoaded();` after your `jdom` functions have been loaded.

You may do whatever you want with the source code. Distribute, modify, include in your own project and sell, or whatever.

Usage is very simple. Write your HTML with css classes (which you should be doing anyway) and add the 'jdom' css class when you want it mapped to a javscript function.

i.e.:
```
  <body>
    <div class="jdom div"> <!-- I wouldn't name a class 'div', but i'm just showing an example -->
      <a class="jdom legal trademark">
          some interesting title
      </a>
    </div>
  </body>
  
```
And to define how they function, define one function for each (I'll go over the (weirdish) naming conventions in a second)
i.e.:
```
//for the outer div, class = "jdom div"
function jdomDiv_div(parent,element){ //you can rename parent and element to, say, body and div respectively
  alert(element.innerHTML);
}
//for the inner span, class = "jdom legal trademark". I'll rename my params here
function jdomDivSpan_cool_span(div,a){
  a.href = '/legal/trademark.html';
  a.target = '_blank';
  //assign functions, too. That's the main reason I made jdom.
  a.onclick = function(){
    return confirm('do you want to read some legaleez?');
  }
}

```
Now, for some stupid reason, you will alert the inner html of <b>element</b> on page load.
AND you'll set the href and target for a link. 
AND now if you click on the a tag, user will have to confirm before leaving.

<h2>Naming Conventions:</h2>
There are three parts,
<ul><li>'jdom' - this always prepends functions using this jdom</li>
<li>Tags - (optional) includes Parent tag and Element tag</li>
<li>classes - the css classes of the html element</li>
</ul>
BUT, you don't have to include the Tags or all of the css classes (though, they do have to be in order)
Starting with this html:
`<form><input class="jdom checkbox autoSubmit coolClass">`  
the function name is as follow:
```
part 1: jdom
part 2: FormInput
part 3: _checkbox_autoSubmit_coolClass
```
Always prepend part 3 with an underscore

It will first check for for a function including all class names, first removing the parent tag, then the child tag, then puts the tags back and removes the last defined class and repeats. The above would check the following functions until it found one that works, at which time it would stop:
```
jdom FormInput _checkbox_autoSubmit_coolClass
jdom     Input _checkbox_autoSubmit_coolClass
jdom           _checkbox_autoSubmit_coolClass

jdom FormInput _checkbox_autoSubmit
jdom     Input _checbkox_autoSubmit
jdom           _checkbox_autoSubmit

jdom FormInput _checkbox
jdom     Input _checkbox
jdom           _checkbox
```
The next check WOULD be `jdom FormInput _` but there are no more classes, so it stops.

<b>Note:</b> that there are no spaces in the function names (obviously). I only put spaces so you can see the difference.

If you don't want something to be called (say jdomInput_checkbox_autoSubmit), then don't define a function for it.
There is no inheritance. So, if you defined `jdom Input _checkbox` AND `jdom FormInput _checkbox_autoSubmit`, only the latter will be called for the example html above.

If you have questions, figure it out, or email me at support@jakar.co and I'll try to help. I plan to add quite a bit to this, but this is for myself so I'm not gonna promise updates or anything.

