
JSClass("left-arrow",
    function(){
        
        this.attach = function(dom){
            dom.onclick = function(){
              var imageWrappers = this.parentNode.getElementsByClassName("wrapper");
              var currentPosition = null;
              var type = typeof document.createElement("span");
              var outputWrappers = [];
              for (var index in imageWrappers){
                  var wrapper = imageWrappers[index];
                  if (typeof wrapper!==type)continue;
                  if (wrapper.classList.contains("display")){
                    currentPosition = wrapper.getAttribute("data-position");
                  }
                  outputWrappers[wrapper.getAttribute("data-position")] = wrapper;
              }
              var newPosition;
              if (currentPosition==0)newPosition = imageWrappers.length-1;
              else newPosition = currentPosition - 1;
              console.log(newPosition);
              outputWrappers[currentPosition].classList.remove("display");
              outputWrappers[currentPosition].classList.add("hidden");
              outputWrappers[newPosition].classList.remove("hidden");
              outputWrappers[newPosition].classList.add("display");
            }
        }
    }
);


JSClass("right-arrow",
    function(){
        
        this.attach = function(dom){
            dom.onclick = function(){
              var imageWrappers = this.parentNode.getElementsByClassName("wrapper");
              var currentPosition = null;
              var type = typeof document.createElement("span");
              var outputWrappers = [];
              for (var index in imageWrappers){
                  var wrapper = imageWrappers[index];
                  if (typeof wrapper!==type)continue;
                  if (wrapper.classList.contains("display")){
                    currentPosition = wrapper.getAttribute("data-position");
                  }
                  outputWrappers[wrapper.getAttribute("data-position")] = wrapper;
              }
              var newPosition;
              if (currentPosition==(imageWrappers.length-1))newPosition = 0;
              else newPosition = Number(currentPosition) + 1;
              console.log("current:"+currentPosition+"\nnew:"+newPosition);
              outputWrappers[currentPosition].classList.remove("display");
              outputWrappers[currentPosition].classList.add("hidden");
              outputWrappers[newPosition].classList.remove("hidden");
              outputWrappers[newPosition].classList.add("display");
            }
        }
    }
);