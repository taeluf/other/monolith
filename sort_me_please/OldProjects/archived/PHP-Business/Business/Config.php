<?php

namespace ROF\Business;

class Config extends \ROF\MVC\Config {
    
    static public function getRootDirectory(){
        return realpath(__DIR__.'/..');
    }
    static public function getNamespace(){
        return '\ROF\Business\\';
    }
    static public function getLoader(){
        return new \ROF\Business\Loader();
    }
    static public function getBusiness(){
        return new \ROF\Business();
    }
    static public function getTag(){
        return new \ROF\Tag();
    }
    
}

?>