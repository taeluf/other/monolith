# PHP-Messsage-Form (under construction)
a convenient way to display messages to users. 

# ARCHIVE Oct 14, 2019
I liked this when I made it. I like it in theory. But I'm not interested right now. This functionality will likely be integrated into a website and might later be abstracted out into its own library.

--

You create a new message form with a title, message, and array of named links. Then you call `display()` to show it on the same page or `redirect()` to show it on a different page.

It theoretically should work, but it has not been tested yet.

# Composer Installation

    "require": { "rof/message-form": "dev-master" }

# Manual Installation
Copy MessageForm.php to wherever you store your classes and set it up to be autoloaded OR `include` it manually.


# Initial Setup
Create a file that contains:  

    \ROF\MessageForm::displaySession();

The default file path is `yoursite.com/message.php` You can choose a different location, but you will have to pass the full URL to the redirect() function call;

`displaySession` will display what's stored in the current session variable and clear it, so the message only displays once. It will show an error if there is no session data.

# Usage

    $messageForm = new \ROF\MessageForm("Subject", "MessageText", array("Home Page" => $homePageurl, "other Url" => $otherUrl));
    $messageForm->redirect($filePath); //default is yoursite.com/message.php or you can provide the full url (protocol & domain)
    //Pass nothing or NULL or FALSE to use the default
    //or
    $messageForm->display(); //this will show it on the current page. You may want to exit or return after to prevent further execution
    

That's it! 

# Changelog
Jan 3 2019
 - removed the `getUrl` call which was invalid and part of a previous project
 - made the array of links an optional argument in the constructor
