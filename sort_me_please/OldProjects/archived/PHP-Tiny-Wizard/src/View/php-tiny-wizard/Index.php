<h1>PHP Tiny Wizard</h1>
<p>This is a setup wizard for <a href="https://github.com/ReedOverflow/PHP-Tiny/">PHP Tiny</a></p>. This page is intendended for the website developer. If you are not the developer, please contact the website owner and let them know this page is still up. A lot of websites have a link somewhere at the bottom or in a menu at the top. </p>
<hr>
<p>Please enter the following code onto the web server (and remove the <b>\</b> from <b><\?php</b>), then click Next</p>
<code>
<pre>
        $randomCode = 'RANDOM_CODE'; //you can (should) put something a little more random in here
        $config = \ROF\Tiny\Wizard\Config::getInstance(["code"=>$randomCode]);
        $config->router->deliverRequest();

</pre>
</code>