<?php


spl_autoload_register(
    function($className){
        $namespace = "ROF\\";
        $path = str_replace($namespace,'',$className);
        $path = str_replace('\\','/',$path);
        if ( file_exists( ($absolute = __DIR__.'/'.$path.'.php') ) ){
            include($absolute);
        }
    }
);


?>