<?php

namespace ROF\Tiny\Wizard;

/** Used to instantiate views for this library.
 *  You probably don't need to write any code in here... but you might! 
 * 
 */
class View extends \ROF\Tiny\View {


    public function __construct($viewName,$obj=NULL){
        $loader = \ROF\Tiny\Wizard\Loader::getInstance();
        parent::__construct($viewName,$loader->getObjectFromSlug(NULL));
    }
    public function startForm(){
        $page = @$_GET['page'];
        if ($page==NULL||!is_numeric($page))$page = 0;
        $next = $page+1;
        echo '<form action="?page='.$next.'" method="POST"><input type="hidden" name="submit" value="TRUE"/>';
    }

    public function showNav(){
        echo new static('View/php-tiny-wizard/Nav.php',$this->obj);
    }
}

?>