<?php

namespace ROF\Tiny\Wizard;

/** Routes web requests. You should put any code in here which is relevant to URLs and request routing
 *  You don't necessarily need any code in here. The parent-class functions will take care of the basic functionality
 */
class Router extends \ROF\Tiny\Router {


    // public function deliverRequest($willDeliver=NULL){
        
    //     if ($this->isRequested()){
            
    //     }
    // }

    public $wizard;
    public $view;

    public function __construct(){
        parent::__construct();
        $this->wizard = $this->config->loader->getObjectFromSlug();
        $this->view = $this->getView();
    }

    public function getViewName(){
        $page = isset($_GET['page']) ? $_GET['page'] : 0;
        if (isset($this->config->pages[$page])){
            return $this->config->pages[$page];
        } else {
            return 'index';
        }
    }

    public function deliverRequest($willDeliver=NULL){
        if (session_status()!=PHP_SESSION_ACTIVE)session_start();
        $this->wizard->updateSession();
        $this->view->startForm();
        parent::deliverRequest();
        $this->view->showNav();
        echo "\n</form>\n";

        include(realpath(__DIR__.'/../../../test/Tester.php'));
        $tester = new \ROF\Tiny\Wizard\Tester();
        $tester->runAll();

    }

}







?>