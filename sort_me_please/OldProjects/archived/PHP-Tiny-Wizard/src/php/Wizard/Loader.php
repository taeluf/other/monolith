<?php

namespace ROF\Tiny\Wizard;

/** Loads data. This data can come from a database, the file system, a json file, or anything. 
 * There may be functions to return instantiated objects based upon this loaded data, such that the user of this library does not deal with raw data
 * 
 */
class Loader extends \ROF\Tiny\Loader {

    /** Get an object based upon a url-slug. 
     *  This method MUST be implemented, or the request routing will not work.
     * 
     * @return object an object of any class or NULL
     */
    public function getObjectFromSlug($slug=NULL){
        $sessionKey = isset($this->config->sessionKey) ? $this->config->sessionKey : NULL;
        $wizard = new \ROF\Tiny\Wizard($sessionKey);
        return $wizard;
    }

}




?>