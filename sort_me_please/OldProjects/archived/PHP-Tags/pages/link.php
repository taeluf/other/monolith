<view type="parent" name="Page" scripts="tag-link.js">
<?php

$biz = \ROF\Business::getBusinessByName(@$_GET['biz']);
$pdo = \ROF\FN::getPdo();

if ($biz==NULL){
  echo "No business exists with the short-name '".@$_GET['biz']."'";
  return;
}
?>
  <form action="/tag/submit-tag-link.php" method="POST">
    <h1>Edit Tags, <?=$biz->getName()?></h1>
      <fieldset id="existing_tags">
        <legend>Tags already assigned</legend>
        <?php 
              $assignedSearch = $pdo->prepare("SELECT Tags.* FROM Tags JOIN tags_biz ON tags_biz.tag_id = Tags.id WHERE tags_biz.biz_id = :businessId ");
              $assignedSearch->execute(array(":businessId" => $biz->getId()));
              while ($row = $assignedSearch->fetch()){?>
                <input type="button" class="jsclass jsclass-assigned_tag" value="<?=$row['name']?>" data-id="<?=$row['id']?>">
              <? }

        ?>
        
      </fieldset>
    <br><br>
    <fieldset id="selected_tags">
      <legend>Tags pending assignment</legend>


    </fieldset>
    <br><br>
    <fieldset id="removed_tags">
      <legend>Tags pending removal</legend>

    </fieldset>
    <br><br>
    <fieldset id="unselected_tags">
      <legend>Unselected Tags</legend>
      <?php
        $unselectedSearch = $pdo->prepare("SELECT Tags.* FROM Tags "
                                             ."WHERE NOT EXISTS " 
                                                ."(SELECT * FROM tags_biz "
                                                    ."WHERE Tags.id = tags_biz.tag_id "
                                                    ."AND tags_biz.biz_id=:businessId "
                                                .")");
        $unselectedSearch->execute(array(
          ":businessId" => $biz->getId()
        ));
        while ($row = $unselectedSearch->fetch()){
          ?>
          <input class="jsclass jsclass-unselected_tag" value="<?=$row['name']?>" type="button" data-id="<?=$row['id']?>">
        <?}
      ?>
    </fieldset>
    
    <br>
    <input type="submit" value="Submit" >
    <input type="hidden" name="business_id" value="<?=$biz->getId()?>">
    <br>
    <br>
  </form>
  

  
</view>