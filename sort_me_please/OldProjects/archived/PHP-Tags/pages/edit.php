<view type="parent" name="Page">
<?php

$tag = \ROF\Tag::getTagByName(@$_GET['name']);
if ($tag==NULL){
  $tag = new \ROF\Tag();
}

?>

<form action="/tag/tag-submit.php" method="POST">

  <fieldset>
    <legend>
      <?= $tag->id==NULL ? "Create Tag" : "Edit Tag" ?>
    </legend>
    <label for="name">Tag Name:</label><br>
    <input type="text" name="name" value="<?=$tag->name?>">
    <br><br>

    <label for="description">Description:</label><br>
    <textarea name="description" placeholder="Tell me more about this tag!"><?=$tag->description?></textarea>
    <br><br>


  </fieldset>
  <br>
  <input type="submit" value="Submit">
  <br>
  <br>
  
  <input type="hidden" name="id" value="<?=$tag->id?>">
  
</form>

  
  </view>