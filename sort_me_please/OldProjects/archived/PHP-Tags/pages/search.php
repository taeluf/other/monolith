<view type="parent" name="Page" stylesheets="TagSearch.css" scripts="TagSearch.js">
<?php

$tag = Tag::getTagByName(@$_GET['tag']);
if ($tag==NULL){
  $tag = new Tag();
}
$tagList = new TagList($tag->name);

?>
  <h1>Search for [<?=$tag->name?>]:</h1>
  <fieldset>
    <legend>Searching For</legend>
    <span class="tag jsclass jsclass-SearchTag"><?=$tag->name?></span>
  </fieldset>
  
  <fieldset>
    <legend>Filter Results</legend>
    <span class="tag jsclass jsclass-SearchTag"><?=$tag->name?></span>
  </fieldset>
  
  <h1>Businesses</h1>
    <?php
      foreach($tagList->getBusinesses() as $name=>$business){
        ?>
        <fieldset>
          <legend><?=$name?></legend>
          <p><?=$business->getBlurb()?></p>
        </fieldset>
        <?php
      }
    ?>
  
  
  
  
  
</view>