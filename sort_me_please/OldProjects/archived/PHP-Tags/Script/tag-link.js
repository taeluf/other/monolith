var staticCount = 0;
JSClass("assigned_tag",
    function(){
  
      this.attach = function(dom){
          dom.jsclass = this;
          dom.onclick = function(){
              var fieldset = document.getElementById("removed_tags");
              fieldset.appendChild(this);
              this.className = "jsclass jsclass-removed_tag";
              JSClass.processor.attachClassToElement(this);
              var input = document.createElement("input");
              input.type = "hidden";
              input.value = this.getAttribute('data-id');
              input.name = "removed_tags[]";
              this.input = input;
              fieldset.appendChild(input);
          }
      }
      
    }
);

JSClass("removed_tag",
    function(){
        this.attach = function(dom){
            dom.onclick = function(){
                var fieldset;
                this.input.parentNode.removeChild(this.input);
                this.input = null;
                fieldset = document.getElementById("existing_tags");
                this.className = "jsclass jsclass-assigned_tag";
                fieldset.appendChild(this);
                JSClass.processor.attachClassToElement(this);
            }
        }
    }
)

JSClass("unselected_tag",
    function(){
        this.attach = function(dom){
            dom.onclick = function(){
                var fieldset = document.getElementById("selected_tags");
                this.className = "jsclass jsclass-selected_tag"
                fieldset.appendChild(this);
                var input = document.createElement("input");
                input.name = "selected_tags[]";
                input.value = this.getAttribute('data-id');
                input.type = "hidden";
                fieldset.appendChild(input);
                this.input = input;
                JSClass.processor.attachClassToElement(this);
            }
          
        }
        
    }
);

JSClass("selected_tag",
    function(){
        this.attach = function(dom){
          dom.onclick = function(){
              this.input.parentNode.removeChild(this.input);
              var fieldset = document.getElementById("unselected_tags");
              this.className = "jsclass jsclass-unselected_tag";
              this.removeAttribute("name");
              fieldset.appendChild(this);
              JSClass.processor.attachClassToElement(this);
          }
          
        }
        
    }
);