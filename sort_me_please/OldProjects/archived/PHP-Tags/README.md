# PHP-Tags (under construction)
A tag system for PHP. Used for categorizing, extended item descriptions, and searching

# ARCHIVE OCT 14, 2019
This is still something I want to build, but I'm not going to start with the abstraction. I may, eventually, return to this library. I think it would be great to have a drop-in tag system, but I'm not ready to make the abstraction. One day.

This lib is currently proprietary and for personal use only. I intend to make it a robust library, but I am not there yet.
