<?php

namespace ROF;

class Tag {
  
  
  public $name = "";
  public $description = "";
  public $id = NULL;
  
  static public $pdo;
  
  public function __construct(){
    
  }
  
  
  static public function updateTag($id,$newName,$newDescription){
    $newName = self::getCleanName($newName);
    $pdo = self::$pdo;
    $update = $pdo->prepare("UPDATE Tags SET name=:newName, description=:newDescription WHERE id=:id");
    $didUpdate = $update->execute(array(
        ":newName" => $newName,
        ":newDescription" => $newDescription,
        ":id" => $id
    ));
    return $didUpdate;
  }
  
  static public function createTag($name,$description){
    $name = self::getCleanName($name);
    $pdo = self::$pdo;
    $insert = $pdo->prepare("INSERT INTO Tags(name,description) VALUES(:name,:description)");
    $didInsert = $insert->execute(array(
        ":name" => $name,
        ":description" => $description
    ));
    return $didInsert;
  }
  
  static public function getCleanName($name){
    return strtolower(preg_replace('/[^A-Za-z0-9\-]/',"-",$name));
  }
  
  static public function getTagByName($name){
    $name = self::getCleanName($name);
    $pdo = self::$pdo;
    $search = $pdo->prepare("SELECT * FROM Tags WHERE name LIKE :name");
    $search->execute(array(
      ":name" => $name
    ));
    $rows = $search->fetchAll();
    if (count($rows)==0||count($rows)>1){
      return NULL;
    } else {
      $info = $rows[0];
      $tag = new self();
      $tag->name = $info['name'];
      $tag->description = $info['description'];
      $tag->id = $info['id'];
      return $tag;
    }
  }
  public function getUrl(){
    return "/tag/search.php?tag=".$this->name;
  }
  
}


Tag::$pdo = \ROF\FN::getPdo();