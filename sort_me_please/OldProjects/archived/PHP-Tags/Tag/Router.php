<?php

namespace ROF\Tag;

class Router {
  
    static public function handleRequest(){
        $page = $_GET['f'];

        $dir = realpath(__DIR__.'/../pages/');
        $path = str_replace('//','/',$dir.'/'.$page.'.php');

        if (file_exists($path)&&realpath($path)===$path){
          include($path);
        } else {
          echo "Failed for path: {$path}";
        }
    }
}

?>