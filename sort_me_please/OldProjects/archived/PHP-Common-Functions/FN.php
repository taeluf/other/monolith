<?php

namespace ROF;

class FN {



  static public function getPdo($initString=NULL, $user = NULL, $password = NULL){
      static $staticInitString;
      static $staticUser;
      static $staticPassword;
      static $pdo;
      $pdoChanges = FALSE;
      if ($initString!=NULL){
          $staticInitString = $initString;
          $pdoChanges = TRUE;
      }
      if ($user!=NULL){
        $staticUser = $user;
      }
      if ($password!=NULL){
          $staticPassword = $password;
      }
      if ($staticInitString==NULL){
          throw new \Exception("getPdo was not given an initialization string. the string is something"
              ."like mysql:host=localhost;dbname=test");
      }
      if ($pdo==NULL||$pdoChanges){
          try {
              $pdo = new \PDO($initString,$user,$password);
          } catch (\Exception $e){
              print "Error!: " . $e->getMessage() . "<br/>";
              die();
          }
      }
      return $pdo;
  }

  /** returns TRUE or FALSE
  */
  static public function strStartsWith($string,$start){
    $length = strlen($string);
    $startsWith= (substr($string, 0, $length) == $string);
    return $startsWith;
  }

  static public function getProtocol(){
      $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
    return $protocol;
  }
  
  static public function getDomain(){
    return $domain_name = $_SERVER['HTTP_HOST'];;
  }
  
  static public function getSiteAddress(){
    return self::getProtocol()."://".self::getDomain();
  }



}



?>
