# Branch & tag versioning
Really just me rambling, thinking things throug, trying to figure out how I want to do versioning.



I've struggled for awhile to figure out the best way to manage branch naming on my many projects. The conventional approach of having a single "main" branch hasn't worked for me. As most of my software relies upon other software I'm building, things get out of date really fast. So I may have one site running an old version of Liaison & another site using the latest. I haven't created tags for either, because neither is stable enough & I need the continued/open dev process. So I depend upon branches, rather than tags.

Putting all my code in a "main" branch means OldSite breaks if I update Liaison to a new major version. 

So, now I name my branches as version numbers. I tried using `0.0.0` as the very first version & incrementing from there. But the purely-numeric approach conflicts with tags when I also make a tag named `0.0.0`. So now I'm trying `v0.0.0` to avoid that conflict. The version-number approach also means: `composer require taeluf/theproject v0.0.0.x-dev` (`.x-dev` at end instead of `dev-` at the beginning).

But then I'm wondering, what does each of those numbers mean? They need to have particular meaning if I want to be consistent in any way. I still conflict on this, because I'm not always sure I want to bump up the 2nd place, especially if the project is pretty new & unstable. 

What I'm proposing, I think:
Branches: `vMajorNumber.MinorNumber`
Tags: `MajorNumber.MinorNumber.releaseNumber` 

So a branch `v0.1` would yield tags like `v0.1.0`, `v0.1.1`, ... `v0.1.33`, etc.

But what does it mean for a tag when the 3rd number is incremented? Is it a new feature? A bug fix? Is a breaking change allowed?

So what are you allowed to do on the `v0.1` branch?
- Initial development (in very early stages, every commit could be a breaking change. But there comes a point that it should be stable. & after stability is found, then breaking changes should be kept to a minimum)
- bug fixes
- Add non-breaking features
- Change internal architecture
- Very minor breaking changes <- Things that SHOULDN'T affect most people very significantly.

But what do I do when I realize that the current version I'm on is... well, bad & just needs to be thrown away? Do I increment the version number then? I think I should, yes.


What would require you to go from `v0.1` to `v0.2`?
- 


Should I do initial development of a new version on a different branch than `v0.2`? 
    Like if I'm working on a new version, but I haven't figured it out yet, isn't it possible I'll throw that version away? And then its not really part of the forward/upward progression of the project. Its an aside that didn't stick. So do I wait until a branch is becoming stable before I create a versioned branch for it?

If I did this the flow would be:
1. On branch `v0.1`
2. new branch `proposal_brief_descript` <- Maybe come up with a name for it, based upon the changes that are being proposed. For example, this newest Liaison could have used `new branch v-no_api_prefix`. The `v-` indicates that this is a version proposal, rather than a feature branch
3. develop on `v-no_api_prefix` for a while, until things are stable
4. Decide to move forward with this new version
5. new branch `v0.2` (from `v-no_api_prefix`)
6. Now I'm locked in to my versioning scheme <- No more significant breaking changes without incrementing the version number.

SHOULD I allow minor breaking changes on a version branch?


When do I increment the first number? The MajorVersion? From `v0.#` to `v1.0`?
Is it based upon the internals? The public API? That significant breaking changes are introduced? That entirely new documentation needs to be written?

I think its save to say that `v0.#` is... unstable, not super mature, maybe there aren't tests or documentation is lacking. A `v1.0` should be somewhat mature. Tests present. Documentation present. 

So the change from 0.# to 1.0 involves stability, maturity, and thoroughness. What would take from `v1.#` to `v2.#`?
This isn't really something I have to answer right now. I don't really know. I like the "Entire new documentation" and "significant breaking changes"

But I guess its more of a question of: When does an existing version reach its end of life? When `v1.#` is end of life, then `v2.#` needs to be made for the next life-force of the project



