#!/usr/bin/env bash

##
#
# if you add this file to a directory in your path,
# and maybe modify the ~/dev starting directory at the bottom
#
# then you can `fuzzy-find-dirs.bash dir1fuzzy dir2fuzzy`
# and get a single dir back
#

function smartFuzzy(){
    local all_smart_files
    simpleFuzzy all_smart_files ${@}

    # TODO Loop through the searches & return the one with the greatest exactness
    echo "${all_smart_files[0]}"
}

function simpleFuzzy(){
    shopt -s nocaseglob
    local all_dumb_files
    declare -n all_dumb_files="$1"

    findStr="$2"
    for name in "${@:3}"; do
        findStr+=/*"$name"*
    done

    files=()
    while IFS= read -r -d '' file; do
        all_dumb_files+=("$file")
    done < <(find $findStr -maxdepth 0 -type d -print0) 
}

smartFuzzy ~/dev $@
