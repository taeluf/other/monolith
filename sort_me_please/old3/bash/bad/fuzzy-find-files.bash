#!/usr/bin/env bash

shopt -s nocasematch
startDir=~/dev


function getFuzzyChildren(){
    search="$1"
    parent="$2"
    declare -n fuzzy="$3"
    declare -n exact="$4"
    declare -n all="$5"


    dirs="$(find ~/dev/ -maxdepth 1 -type d -name "*$search*")"

    fuzzy=()
    exact=()
    for dir in "$parent"/*; do
        if [[ ! -d "$dir" ]];then
            continue;
        fi;
        name="$(basename "$dir")"
        all+=("$(basename "$dir")")
        # echo "Name: $name"
        if [[ "$name" == *"$search"* ]];then
            fuzzy+=("$dir")
            if [[ "$name" == "$search" ]];then
                exact=("$dir")
            fi
        fi
    done

}


lvl2All=()

if [[ -n "$1" ]];then
    
    score=0

    getFuzzyChildren "$1" "$startDir" lvl1Fuzzy lvl1Exact lvl1All


    keepGoing=true
    if [[ ${#lvl1Exact[@]} -eq 1 ]];then
        startDir="${lvl1Exact[0]}"
        # echo  "k: $startDir"
        score=0
    elif [[ ${#lvl1Fuzzy[@]} -ge 1 ]];then
        startDir="${lvl1Fuzzy[0]}"
        score=0
    fi

    if [[ -n "$2" && ${#lvl1Fuzzy[@]} -ge 1 ]]; then
        for searchDir in "${lvl1Fuzzy[@]}";do
            if [[ ! -d "$searchDir" ]];then
                continue;
            fi;
            getFuzzyChildren "$2" "$searchDir" lvl2Fuzzy lvl2Exact lvl2SearchAll

            lvl2All+=("${lvl2SearchAll[@]}")
            if [[ "$searchDir" == "${lvl1Exact[0]}" ]];then
                if [[ "$2" == "$(basename "${lvl2Exact[0]}")" ]];then
                    # echo "allexact"
                    startDir="${lvl2Exact[0]}"    
                    score=4
                    break;
                elif [[ "$score" -lt 3 && ${#lvl2Fuzzy[@]} -ge 1 ]];then
                    # echo "lvl1Exact"
                    score=3
                    startDir="${lvl2Fuzzy[0]}"
                fi 
            elif [[ "$score" -lt 2 ]];then
                if [[ "$2" == "$(basename "${lvl2Exact[0]}")" ]];then
                    # echo "lvl2exact"
                    startDir="${lvl2Exact[0]}"
                    score=2
                elif [[ "$score" -eq 0 && ${#lvl2Fuzzy[@]} -ge 1 ]];then
                    # echo "lvl2fuzzy"
                    startDir="${lvl2Fuzzy[0]}"
                    score=1
                fi
            fi
        done
    else
        echo -n "lvl1: "
        for lvl1 in "${lvl1Fuzzy[@]}";do
            echo -n "$(basename $lvl1), "
        done
        echo
        getFuzzyChildren "DoNotMatchAnyFiles" "$startDir" lvl2Fuzzy lvl2Exact lvl2SearchAll
        echo -n "lvl2: "
        for lvl2 in "${lvl2SearchAll[@]}";do
            echo -n "$(basename $lvl2), "
        done
        echo
    fi
else 
    
    getFuzzyChildren "DoNotMatchAnyFiles" "$startDir" lvl1Fuzzy lvl1Exact lvl1All
    echo "lvl1: ${lvl1All[@]}"
    exit
fi

echo "Using $startDir"
read -p "Continue? (enter) "


vimx -geometry 1920X1080 -u ~/.SpaceVim/vimrc --cmd "cd $startDir" -c ":source ~/.vimrc" 
