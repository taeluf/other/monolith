<?php
/*
 *
 * This comes from testing.reedoverflow.com
 *
 */

//$this->register(event, callable or file, priority?);
\ROF\Autoloader::enable(realpath(__DIR__.'/../Class'));

$pdo = \ROF\FN::getPdo("mysql:host=gator4271.hostgator.com;dbname=maconzer_testing","maconzer_tester",'~#xjL;5spoJn');
\ROF\Article\Config::getInstance()->initialize([
           "pdo"=>$pdo,
            "url"=>'/article', 
           "single_script" => $_SERVER['DOCUMENT_ROOT'].'/vendor/rof/testing.testing/tiny-mvc/article.php', 
           "permissions" => new \TestPermissions()                
           ]);
           
\ROF\User\Config::getInstance()->initialize([
           "pdo"=>$pdo,
            "url"=>'/user', 
           "single_script" => $_SERVER['DOCUMENT_ROOT'].'/vendor/rof/testing.testing/tiny-mvc/user.php', 
           "site_name" => 'TEST SITE for ReedOverflow.com',
           "contact_name" => 'Reed',
           "contact_email" => 'reed@reedoverflow.com',
           "support_url" => '/contact/'
           ]);


$this->registerUrlHandler(
    function($url){
        $conf = \ROF\Article\Config::getInstance();
        if (strpos($url,$conf->url)===(int)0){
            return TRUE;
        } else {
            return FALSE;
        }
    },
    function($url){
        $handler = \ROF\Article\Handler::getInstance();
        $handler->deliverRequest();
    },
    "article"
);

$this->registerUrlHandler(
    function($url){
        $conf = \ROF\User\Config::getInstance();
        if (strpos($url,$conf->url)===(int)0){
            return TRUE;
        } else {
            return FALSE;
        }
    },
    function($url){
        $handler = \ROF\User\Handler::getInstance();
        $handler->deliverRequest();
    },
    "user"
);





$this->registerContentHandler(
	function($content){
   		return TRUE;
   },
   function($content){
  	$documentRoot = $_SERVER['DOCUMENT_ROOT'].'/vendor/rof/testing.testing';
	$controller = new \ROF\View\Controller($documentRoot);
	$controller->load($content);
	$controller->display();
  	
  },
  'default'




);



?>
