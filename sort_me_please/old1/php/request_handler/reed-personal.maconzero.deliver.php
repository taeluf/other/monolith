<?php

require "vendor/autoload.php";

//create a new handler and configure it.
//The Handler is the only required part.

$configuration = new \JakarCo\Request\Configuration();
$configuration->configure();

$handler = new JakarCo\Request\Handler();

$controller = new \JakarCo\View\Controller();
$configuration->setViewController($controller);
$handler->setConfiguration($configuration);

//$configuration->setViewController($controller);
//You may pass a string or an array of strings to directory. The source folder is $_SERVER['DOCUMENT_ROOT'];
//$configuration->setAutoloadDirectory($directory);
//Defaults to /Public. Will accept with or without trailing slash and sets path relative to document root.
//$configuration->setPublicDirectory('Public');


//This is just a comment for testing's sake
$handler->handle();


?>
