<?php

use \RO\Permissions\Permissions;

var_dump(Permissions::hasAccess(0,Permissions::ACCESS_NO_USER)); // true
var_dump(Permissions::hasAccess(FALSE,Permissions::ACCESS_NO_USER)); // true
var_dump(Permissions::hasAccess(NULL)); // false
var_dump(Permissions::hasAccess($someUserId,Permissions::ACCESS_USER)); //true
  


?>
