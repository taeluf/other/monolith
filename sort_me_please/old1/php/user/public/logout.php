<?php

  $user = \RO\User\User::getLoggedInUser();
  if ($user===FALSE){
    (new \RO\User\MessageForm("Not Logged In",
                             "You tried to log out, but there was no user logged in. ",
                             array(
                             "Home Page" => "home",
                             "Log In" => "login",
                             "Help" => "help"
                             )
                             )
    
    )->redirect();
  }

  $user->logout();
  


?>