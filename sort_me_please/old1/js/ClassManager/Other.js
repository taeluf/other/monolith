/**
 * These look to just be other classes that used the class manager
 */

jsclassmanager.Class("editor",
    function(){
      
        this.onkeyup = function(){
            var callback = this.updateResult;
            var markdownText = this.value;
            var params = "view=Markup&view_type=none&method=plaintext&markdown_text="+encodeURIComponent(markdownText);
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
              if (this.readyState == 4 && this.status == 200) {
                callback(this);
              }
            };
            xhttp.open("POST", this.form.getAttribute('data-markup_url'), true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            xhttp.send(params);

        }

        this.updateResult = function(response){
            var resultDom = document.getElementById("article-result");
            resultDom.innerHTML = response.responseText;
        }   
        
    },
    function(){
  
    }
);

// JSClass("AddLink",
//         function(){
            
//             this.attach = function(dom){
//                 var link_callback = this.updateResult;
//                 dom.onclick = function(){
//                     var urlDom = document.getElementById("add-source");
//                     var url = urlDom.value;
//                     urlDom.value = "";
//                     var params = "url="+encodeURIComponent(url);
//                     var xhttp = new XMLHttpRequest();
//                     xhttp.onreadystatechange = function() {
//                       if (this.readyState == 4 && this.status == 200) {
//                         link_callback(this);
//                       }
//                     };
//                     xhttp.open("POST", "/article/?view=add_link", true);
//                     xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//                     xhttp.send(params);
//                 }
//             }
            
//             this.updateResult = function(response){
//                 var data = JSON.parse(response.responseText);
//                 var div = document.createElement("div");
//                 div.innerHTML = '<a href="'+data.Url+'">'+data.Title+'</a><b class="jsclass jsclass-RemoveLink">X</b>'
//                                 +'<input type="hidden" name="sources[]" value='+data.Url+'>';
//                 var resultDom = document.getElementById("sources_box");
//                 resultDom.appendChild(div);
//                 JSClass.processor.attachClassesToDom();
//             }
//         }
// );

// JSClass("RemoveLink",
//         function(){
    
//             this.attach = function(dom){
//                 dom.onclick = function(){
//                     this.parentNode.parentNode.removeChild(this.parentNode);
//                 }
                
//             }
//         }
// );

