-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 13, 2020 at 11:24 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `maconzer_reedoverflow`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `blurb` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `title`, `blurb`, `body`) VALUES
(1, 'Testing! 2', 'An article purely for testing the website\'s article-writing service this is the first test, technically.', '# Big Test  \r\nYou know we gotta make things work. So that\'s why we do these test things before we actually get to production. I think it\'s nice!!!  \r\n\r\n\r\n**And this is bold <3**\r\n\r\n* And this is a list\r\n* and more listing\r\n* And MORE!!!\r\n\r\n### Actually the first\r\n\r\nBut now I\'m updating it. AGAIN\r\n\r\n#### THIRD UPDATE. OR 4? whatever'),
(2, 'Testing!', 'An article purely for testing the website\'s article-writing service', '# Big Test  \r\nYou know we gotta make things work. So that\'s why we do these test things before we actually get to production. I think it\'s nice!!!  \r\n\r\n\r\n**And this is bold <3**\r\n\r\n* And this is a list\r\n* and more listing\r\n* And MORE!!!'),
(3, 'PHP PDO MySQL Overview', 'Learn how to perform all the SQL queries with PHP\'s PDO class', 'The documentation for PDO is available at [php.net/pdo](http://php.net/pdo) .  \r\n\r\n# Create a PDO object\r\nYou may want to use a `try/catch` in case PDO fails to open.  \r\n\r\n    $pdo = new PDO(\'mysql:dbname=testdb;host=127.0.0.1\', \'dbuser\', \'password\');(\r\n\r\nYou should be able to find the dbname, host, username, and password all in your host\'s server manager page, often cPanel. \r\n\r\nWe\'ll use `$pdo` for the rest of our examples.\r\n\r\n# Query for one item  \r\n\r\n    $userSearch = $pdo->prepare(\"SELECT * FROM users WHERE user_id =LIKE :user_id AND password_hash LIKE :password_hash\"); \r\n    $success = $userSearch->execute(array(\r\n        \":user_name\" => $user_name, //assuming you have a user_name variable\r\n        \":password_hash\" => $password_hash //also assuming you\'ve prepared this\r\n    ));\r\n    $userData = $userSearch->fetch();\r\n    $userSearch->closeCursor();\r\n\r\nNext, we will cover checking your results for what you want.\r\n\r\n# Validating your results\r\nSee `$userData = $userSearch->fetch();` under **Query...* above.\r\n\r\nWe\'ll edit that line to\r\n    if ($success===FALSE){\r\n        //print an error, or throw an Exception\r\n        return;\r\n    } else if ($userSearch->rowCount()===0) {\r\n        //print error for no results, throw exception, or alternate\r\n        return;\r\n    } else if ($userSearch->rowCount()>1) {\r\n        //throw/print a different error, as there should only be one matching user row\r\n        return;\r\n    }\r\n    $userData = $userSearch->fetch();    \r\n\r\nYou can have an else block for that last line, instead of using those returns. However, I like to do it this way so my success code block is not contained within a giant `if`, which I find ugly and distracting.\r\n \r\n');

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expiresAt` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `user_id`, `name`, `data`, `expiresAt`) VALUES
(3, 2, 'cookie', '%242y%2410%24gLIheA2x5D7cQJRkNY7J%2FexhRPKQzGmMfCAs1WIWtETL5OBH.Dh5G', 1549844705),
(5, 2, 'cookie', '%242y%2410%24W.NJIjfhq9mGmW0TCGVM0.pc1OaBssT6NZjDoNq3hF29I0DgY6kBS', 1549844724),
(6, 2, 'cookie', '%242y%2410%24CfGeDzrVNAmR4ud0Ysry7eSp4RvzV8sqr4CCLUvcI1ORBrG3BQPRq', 1549845006);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `email`) VALUES
(2, 'Reed Sutman', 'rsutman@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `access_level`) VALUES
(1, 2, 20001);

-- --------------------------------------------------------

--
-- Table structure for table `rof_articles`
--

CREATE TABLE `rof_articles` (
  `id` int(11) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci,
  `blurb` text COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rof_articles`
--

INSERT INTO `rof_articles` (`id`, `title`, `url`, `blurb`, `body`) VALUES
(1, 'Fixing PHP Recoverable Fatal Error: Object of class Closure could not be converted to string in ...', 'closure-string-error', 'See how to fix Recoverable the Fatal Error: Object of class Closure could not be converted to string in ... You\'re converting to a string, when you need to invoke the function. ', 'Let\'s suppose you have a `closure` (which is an inline `function`) assigned to a variable, such as:\r\n\r\n```php\r\n<?php\r\n       //Enter your code here, enjoy!\r\n\r\n//this is pseudo-random and NOT cryptographically secure\r\n$randomChars = function($length)\r\n{\r\n    for (\r\n        $chars = array(\'0123456789\',\'abcdefghijklmnopqrstuvwxyz\',\'ABCDEFGHIJKLMNOPQRSTUVWXYZ\',\'!@#$%^&*()_+-=\'),\r\n        $randomString=\"\",\r\n        $i=0;\r\n        $i<$length;$i++){\r\n        $randomString .= \r\n            substr($chars[$i%4], \r\n                random_int(0, strlen($chars[$i%4])), 1);\r\n        }\r\n    return $randomString;\r\n};\r\n\r\n?>\r\n\r\n```\r\nThis is a fairly simple random string generator. \r\n\r\nNow, let\'s suppose you\'re getting the error **Recoverable Fatal Error: Object of class Closure could not be converted to string in** and you\'re stuck.\r\n\r\nYou probably have a bit of code like:\r\n```php\r\necho \"Here are my random characters: \".$randomChars;\r\n```\r\nWell, this generates an error, because `$randomChars` is not a string. It is a `function` which *returns* a string. \r\n\r\n## The Fix\r\nInstead of using `$randomChars` as a string, use it as a function:\r\n```php\r\n$theChars = $randomChars(15); //get a random string 15 characters long\r\necho \"I have this random string: \".$theChars;\r\n```\r\nNow everything works, because you\'re getting the return value of the function call, rather than trying to print out the function itself.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `rof_articles`
--
ALTER TABLE `rof_articles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url` (`url`(254));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rof_articles`
--
ALTER TABLE `rof_articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

