# Major Projects
The goal of this file is to determine which major project(s) I should be focusing on. 

I will need to answer the following questions for each project of consideration:
	1) Who is it going to help?
	2)  a.) What moral positives might be associated? 
		b.) and negatives?
	3) What action will this bring?
	4) How will it help me?
	5) What does it teach?
	6) Community...
		a.) How does it build community?
		b.) What is that community supposed to accomplish? (actions)
	7) How will I ensure ease-of-use?
	8) How will I monetize?
	//9)  a.) How long until a prototype? A beta verion? A first release version?
	//	b.) How long after first-release will it continue to be my primary work-project?


- Liaison - YES, is a web-app & website development system
	1) Helps me, other php developers. Very long term, helps business owners / bloggers / non-coders
	2) openness, sharing, community
	3) Creation of useful software including websites & web-apps (hopefully some open-source)
	4) Builds the core of my software company. Provides tools for the future of my software development. Eventual monetizations of apps & themes through self-hosted app-store
	5) Teaches how to organize an application, how to collaborate & integrate multiple software projects, and how to extend functionality of existing projects
	6)  a.) Digitally, through github, the liaison-website, possibly FB group / slack channel / reddit / stackoverflow tags, etc
		b.) Creating a suite of PHP tools that make PHP-programming playful.
	7) Good documentation that is community-editable. By designing APIs with intuitive design as a central goal. By ensuring future backwards-compatibility through intentional design (after Beta phase). By constantly improving extensibility features
	8) Accept contribution... the themes & lia-apps app store, paid integrations (like a wordpress-integrater for $1.00 or something)
- Software Dev App Store - NO/Meb, an store for developers to find software they need for their projects
	1) Software Developers, me
	2)  a.) Oppenness, sharing, community
		b.) Strict requirements may exclude well-meaning software
	3) Collaboration upon a software-goal rather than a single software library, better software integrations, community-driven collections of like-goaled software
	4) Money. Sorting of software that I'm interested in.
	5) Teaches how to present a software project & how to write and package it in an intuitive way
	6)  a.) Digitally, through the app-store's website, github, online channels (fb, slack, reddit, SO)
		b.) Organization & clear presentation of tools for developers, such that any developer can find the best version of a software for their particular needs.
	7) By writing out kinks for current software-searching systems (github). Enforcing strict standards for app/software documentation. Enforce strict standards for grouping / tagging / categorizing software. Community-driven moderation system (for improving app documentation & categorization). A feedback system both for the app-store and for software-packages themselves
	8) Selling apps & taking a cut. Advertising (no). Accepting contributions. "Free" apps are actually penny-apps & you get 50 pennies per year for free
- Decatur Cares (Decatur Moves / MoveDecatur) - MEB/Yes, A site for organizing & publishing local causes
	1) Helps local NFPs, activists, organizers, which subsequently helps their bases, thus the Decatur residents at-large (so I hope)
	2) community building, openness, trasnparency, individual empowerment
	3) Organizing with a larger base, more volunteering, formation of coalitions, sharing important information with a larger base
	4) A hook for selling web-design services. A hook for selling NFP Team Hub. Builds my brand. Increases my reach. Not sure how to monetize directly. Post my own causes. Share about Macon Zero.
	5) Teaches how to get a movement off the ground, how to get involved in a movement, what movements are available locally, how to organize online
	6)  a.) Online through the site. In-person with events/meetups (hosted on the site, possibly push to FB/EventBrite/etc). FB Groups & other online outlets would serve as marketing opportunities, more than community-building.
		b.) Act on local issues in a collaborative fashion
	7) By testing it thoroughly, collecting feedback from Movers, collecting feedback from site users, sketching out a sitemap & designs, studying other websites for design ideas, watching some design-education videos, keeping the code-base easy-to-manage, keeping the interface simple, a tag-based system for organizing movements, groups, & events; (maybe) tracking site-usage like time spent on each page
	8) Selling NFP Team Hub, sell merchandise (shirts, hats...), accept donations for orgs(take 2%), allow online sales (take 3%), Patrons (donations to me), finding sponsors to invest in the project, crowd-funding, advertising, pay-to-unrestrict features (ex: Only allow 2 events posted per month for free Organizer accounts),
- NFP Team Hub - NO, a drop-in utility for a small non-profit's own website, for organizing in that NFP
	1) Folks in small non-profits, folks on small cause-oriented teams
	2)  a.) Openness, community
		b.) Transparency could be foregone if the team hub is used poorly. This is also easily a money-centric project
	3) Information is better-shared among a team of people, cause-groups are more efficient and can focus more on the work, better organization of documents & accounting. 
	4) Money. Use the hub for Macon Zero
	5) Teaches how to organize a non-profit, how to do some basic accounting, & how to file paperwork for an org
	6)  a.) Digitally through the org's private team page, & with email lists
		b.) The specific mission of the group which is organizing on the team hub
	7) Collecting feedback from teams who use it. Sitemap sketch... design sketches... organizational needs chart, using it for Macon Zero
	8) By selling it. Incorporating web-design services (for sale), incorporating a donation feature (public-facing) & take a small %, other custom software solutions which would be sold separate from, but included in (&marketed through) NFP Team Hub
- LoveDecatur.com - NO, a website for hosting information about Decatur & helping citizens engage effectively (mixin with DecaturCares?)
	1) local activists, concerned citizens, newcomers to town, journalists
	2) Openness, transparency, individual empowerment
	3) More local government engagement, speaking at city council, citizen-driven local gov' transparency
	4) Not sure how to monetize or if I want to. Increases my reach. Proof & practice of Liaison's abilities.
	5) Teaches how to engage with local gov, teaches local laws, teaches about what's going on locally, teaches about local figures & organizations
	6)  a.) On LoveDecatur.com w/ comments, local journalism, data sharing. Through Online portals like FB page & FB Groups
		b.) Take collaborative action on local (politics-related) issues & keep local government in check
	7) Keep content organized (tags & general design). Subdomains for specific interfaces (like FOIA stuff?), keeping it minimal and specific to local government, free from popup-ads, accessibility features
	- FOIA education - NO, a set of pages that teach folks how to send FOIA requests & provides information about local FOIA offices
	- FOIA upload & search - NO, a utility for managing FOIA requests & sharing them publicly
	- Decatur Law - NO, a tool for viewing & searching Decatur's laws
	- Council Meetings - NO, pages to share information about council meetings & host public discussion
	8) Patrons(donations), selling merchandise (shirts, wristbands, FOIA Kit, idk), advertising (probably not), selling a mobile app (probably not), contracting with the city for specific projects (bus routes,etc; unlikely), crowd-funding new features for the website (like bus routes, copwatch), Pay-to-post content (such as an extensive bio for a political candidate)
- Shop-Decatur - MEB, To provide online-shopping to local businesses
	1) Local business owners, local consumers, morally-interested consumers (eventually), and the world-at-large (eventually w/ ethical scorecard)
	2) Builds community. Money is the primary motivator... it supports consumerism, and as presented, does not bring in the ethical scorecard until a later time. Starting with ethical scorecard as a central feature could make this a morally passable project
	3) More local shopping, more local sales, more ethical business practices (to get a stunning ethical scorecard)
	4) Money. Increased reach. Helps me shop locally.
	5) Teaches consumers how to be ethical shoppers, teaches businesses what they need to do to be ethical
	6)  a.) Through ShopDecatur site w/ comments, product reviews, business reviews, discussion groups about businesses & through facebook (but FB more as a marketing tool than a community-building tool), & customer-support features
		b.) Increase local economic success & drive businesses to have more ethical practices & communicate with businesses about what customers want
	7) Put product search first & business-search second. Use tags for searching. tracking viewed items (for the user's own benefit). Focus on brick-and-mortar before anything else, enforce strict standards for product displaying, thoroughly vet the shopping API, & the usual of sitemapping it, sketching interfaces, and placing intuitive design as a central goal
	8) By taking a portion of every sale, offering software-development services for businesses (websites, tools), paid memberships (for businesses) with additional features, advertising (probably not), paid-members-only features for consumers, sell service to list a business (for internet-afraid biz owners)
	




- How Ethical Is - NO/Meb, provides ethical information on companies, non-profits, individuals, gov` entities (start as a branch of my web-notes/research-notes/human-curated-content stuffs...)
	1) Morally-interested consumers, the world at-large
	2) Builds online community, creates transparency, literally the whole thing is about being morally awesome, individual empowerment
	3) More ethical business practices, consumer engagement with companies, consumer boycott of companies
	4) Not sure how to monetize
- The Data Website - MEB, to host & nicely present a broad range of data (would likely start as a component of LoveDecatur)
	1) Helps activists, concerned citizens, & folks doing research
	2) openness, individual empowerment, societal engagement 
	3) Citizen engagement with public bodies
	4) Improved reach. Outlet for data-related research I do. Not sure how to monetize
- Human Curated Search - NO/Meb, Provides a human-oriented & human-organized search engine (likely start as a personal project)
	1) Technical searchers, researchers, and other folks who need more specificity & quality than is brought from keyword-searches
	2) 	a.) Openness, community, transparency, removes machine-caused bias, empowers individuals
		b.) Risk of personal bias both from community members & admins. Huge risk of missed information.
	3) Less time spent finding the right information, less personal data collection (getting away from google), more energy/time to spend acting upon information, more useful debates (better voting), less biased decision making
	4) Increased reach. Not sure how to monetize. Builds upon other long-term projects (tag-search, community-building, curation of sources)
- Fact-finding & building site - NO/Meb, Provides a web-interface for collaboratively reviewing & compiling sources to present facts w/ a "reliable" trust-rating. (likely start as a personal project)
	1) Journalists, the general public, researchers, essay-writers, the world-at-large
	2)  a.) Openness, sharing, community transparency, individual empowerment
		b.) Partisan mis-use, bias (community & admins/moderators)
	3) (See Human Curated Search #3)
	4) Increased reach. Tool for me to sort my own research.
- IL Law site - NO, a site for viewing & searching Illinois's laws
	1) Illinois Residents, me
	2) Transparency, clarity of information, accessible information, individual empowerment
	3) Better outcomes for individuals interacting with the criminal system (courts/police)
	4) Not sure how to monetize. Increased reach