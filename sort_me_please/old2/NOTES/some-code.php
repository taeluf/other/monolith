<?php


function submissionToSpatieArray($data){
    $days = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];

    $set = [];
    $index = 0;

    
    while($index<count($data['hour_start'])){
        $hasMatch = FALSE;
        foreach ($days as $day){
            $key = $day.$index;
            if (!isset($data[$key]))continue;
            
            $hasMatch = TRUE;

            $minuteStart = $data['minute_start'][$index];
            if (strlen($index.$minuteStart)===2)$minuteStart = '0'.$minuteStart;
            $startTime = $data['hour_start'][$index].':'.$minuteStart.$data['ampm_start'][$index];
            $startDate = DateTime::createFromFormat('g:iA',$startTime);


            $minuteend = $data['minute_end'][$index];
            if (strlen($index.$minuteend)===2)$minuteend = '0'.$minuteend;
            $endTime = $data['hour_end'][$index].':'.$minuteend.$data['ampm_end'][$index];
            $endDate = DateTime::createFromFormat('g:iA',$endTime);

            $set[$day][] = $startDate->format('H:i').'-'.$endDate->format('H:i');

        }
        $index++;

    }

    return $set;
}

function hasPermission($user,$action,$pins=NULL){
    
    $lock = new Lock($action,$pins);
    $key = new Key($user);
    return $key->open($lock);
}

function getUser(){
    return ROF\User::loggedInUser();
}

class Lock {
    
    public $store;
    public $action;

    public function __construct($action,$store){
        $this->store = $store;
        $this->action = $action;
    }
}

class Key {
    
    public $user;

    public function __construct($user){
        $this->user = $user;
    }

    public function open($lock){
        $store = $lock->store;
        $action = $lock->action;
        $user = $this->user;

        switch ($action){
            case "create-store":
                if ($user->isInGroup('admin')){
                    return true;
                }
                return false;
            case "edit-store":
                if ($user->isInGroup('admin')
                    ||$store!==null
                    &&$store->isOwnedBy($user)){
                    return true;
                }
                return false;
            case "default":
                return false;
        }
    }
}