# App store for software developers
A site to search for software projects that a developer can use to improve their own project.


## Hours-Selector Example
1) I need an hours selector for my website. I know how use `<select>` & `<option>` & I can definitely do it, but it's just a pain in the butt. If I can avoid writing server-side parsing that would be a huge plus, but I can deal either way. 
2) I search for 'hours selector' & see a list of potential types of products. First on the list is `hours selector`. Next is `calendar`. Next is `date selector`. 
3) I choose `hours selector` and NOW I see a list of "apps" that provide an hours selector. Some of the apps listed are:
    - A simple block of html (no css) w/ the select/options w/ 5-minute granularity
    - Styled html (with css), but simple selection
    - A javascript lib that generates an hours selector based upon supplied constructor args (like granularity, 24-hour v. am/pm, etc)
    - A web-app that you go to (someone's own website), fill in settings, and click 'generate'. (online minifiers would be a good example of this)
    - a Windows-GUI hours selector that can be dropped into a windows app
    - an ios hours selector that can be dropped into an iOS appp
    - a PHP-HTML software lib that provides the HTML hours selector (generated or not) as well as server-side processing of the input time into a PHP datetime object
4) I refine my search for 'output=html' (it's a tag?). The windows & ios ones disappear.
5) I refine my search for 'output=css' because I want some styling as well
6) I refine my search for 'backend=php', I choose the PHP-HTML lib & get going!

## Package requirements
- Each package will need to be well-defined (tags, specific problem(s) it solves)
- Well-Documented: Onboarding has to be super easy. No hard thinking. 1) Download this 2) Initialize that 3) Call this. 4) Configure it more, if you want
- Packages should solve specific problems. A calendar lib may also have an hours-selector, but one app cannot solve both problems. The calendar app tells you exactly how to setup a calendar on your site. The hours-selector app (same EXACT code-base) will tell you exactly how to setup the hours-selector. (the calendar app may reference the hours-selector & visa versa)
- Open source? Hmmm.... Yes, I think so. 
- Licensing requirements? Idunno.