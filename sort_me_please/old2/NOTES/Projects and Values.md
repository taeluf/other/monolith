# Project Values - any major project I start should:
- Help someone
    - Teaches something, improves access to resources, helps someone organize for a cause, provides a tool for increasing sales or managing complex things, 
- Be inline with my moral values
    - Individual-empowerment, openness, sharing, societal engagement (rather than innocent bystander), do-no-harm, privacy, transparency (about intent & shortcomings), money is secondary to mission
- Bring about action (not idle consumption)
    - Action may be activism, selling products, going to events, congregating with friends, sending emails, protesting, creating crafts, creating software, or anything else that's essentially more than "Oh I'm mad about that news. Now let me get on with the rest of my life."
- Bring me long term benefit
    - This may be money, building up other projects, improving my overall reach, teaching me skills
- Teach something
    - whether it's the general public interacting with the site, the base (such as businesses who have listings), government officials, or whatever... somebody needs to learn something
- Build community
    - Whether online, locally, nationally, etc. Just as long as it involves people working together toward a common cause
- Be easy & fun to use
    - Calls for intuitive design, achievement/awards system, is not overwhelming, provides some non-serious benefit 


# Project Categories
Nearly every project has the potential to grow and fit into a higher-level category or to diminishs and fit into a lower-level category.
- Major Project - A flagship project that is public facing & incorporates every single value listed above. These will require a significant time investment (1-6 months?), have multiple releases, continued addition of new features, and require long term support. These projects will generally have a broad scope & help many people.
- Medium Project - A standalone project which may support another project. Moderate Time investment (5-20 work days), incorporates at least two of the above values (preferably >= half), and will likely require some ongoing support. Occasional feature-updates are expected after the minimum-viable-product, but very-long term growth is not required.
- Minor Project - A supporting project that incorporates at least two of the values above or is a necessary component of a major project. These will require 5 days or less to create a minimum viable product. Future updates & features will be optional, and minimal ongoing support (bug & security fixes) will be rquired.
- Tiny Project - A project that can be cranked out in 1-2 days and does not necessarily meet any of the values above (but probably meets 1 or 2).
- Hobby Project - A project I'm doing for fun. It may also serve as a supporting/tiny/minor/medium/or major project, but public release is not expected & it's not to be worked on during non-hobby work-hours
- Supporting Project - A project which explicitly supports another project. These may be standalone or integrated. They may require long-term support or not. Ongoing bug & security updates expected. May or may not incorporate project values.
- Other Project - A project that is not well-defined, lacks clear goals, does not fit in the above definitions, and/or does not meet the values listed above.




# All projects  
[- Project Name - PRIORITY, A brief description of the project]

## Major
- Liaison - YES, is a web-app & website development system
- Decatur Cares - MEB/Yes, A site for organizing & publishing local causesex
- The Data Website - MEB, to host & nicely present a broad range of data
- Shop-Decatur - MEB, To provide online-shopping to local businesses
- How Ethical Is - NO/Meb, provides ethical information on companies, non-profits, individuals, gov` entities
- LoveDecatur.com - NO, a website for hosting information about Decatur & helping citizens engage effectively
	- FOIA education - NO, a set of pages that teach folks how to send FOIA requests & provides information about local FOIA offices
	- FOIA upload & search - NO, a utility for managing FOIA requests & sharing them publicly
	- Decatur Law - NO, a tool for viewing & searching Decatur's laws
	- Council Meetings - NO, pages to share information about council meetings & host public discussion
- IL Law site - NO, a site for viewing & searching Illinois's laws
- Human Curated Search - NO/Meb, Provides a human-oriented & human-organized search engine
- Fact-finding & building site - NO/Meb, Provides a web-interface for collaboratively reviewing & compiling sources to present facts w/ a "reliable" trust-rating
- Software Dev App Store - NO/Meb, an store for developers to find software they need for their projects
- NFP Team Hub - NO, a drop-in utility for a small non-profit's own website, for organizing in that NFP


## Medium
- Liaison\User - YES, is a drop-in user-management system for websites
- JS-Autowire - YES, is a DOM-interaction Javascript framework
- Liaison\Article - MEB/YES, A drop-in content-manager for websites
- FormTools - MEB/Yes, a suite of tools for working with HTML forms
	- Anti-Spam - MEB/Yes, a php spam-controller for HTML Forms
	- Input-Sanitizer - No, a php form inputs filter / sanitizer
	- Auto-filler - MEB/Yes, 
	- Auto-saver  - MEB/Yes, a PHP form-processer that uses OOP to plug user-input into persistent storage
- ReedyBear.com - MEB, To host my own projects, articles, & tutorials
- Calendar Lib - NO/Meb, Provides an interactive drop-in calendar
- MaconZero.com - NO, The official Macon Zero website
	- eco.maconzero.com - NO, an environment-centric Macon Zero educational website
- Is This Recycleable? - NO, a local recycling-education website
- Tag Search System - NO, an extensible tool for performing searches on any data set 

## Minor
- PDO backed-by-file - MEB/Yes, a data-storage & backup mechanism to enhance PDO-powered websites
- PHP-View-Controller | Template-engine - MEB/No, an HTML-syntaxed template engine
- Tokenizer - NO, An opensource software project allowing PHP devs to more easily process text into hierarchical data
- Events Library - NO, A drop-in liaison app for events (but has no calendar?)
- Meta Package - NO, a suite of tools for managin meta-information on a website
	- Website menu builder - NO, a tool for managing a website's menu items
	- Page details editor - NO, a tool for managing meta-information for a page like title, description, tags, etc. For SEO & internal use
- Site Admin - NO, a tool for managing a a web-app and/or website and/or webpage
- Data-Tools - NO, a tool for processing & visualizing data sets
- PHP Documentor - NO, A liaison drop-in web-app for displaying software documentation & parsing source-code for documentation

## Tiny
- PHP-Tester - MEB/No, a UNIT Testing library with a PHP & Web interface
- Contact Form Lib - MEB/No, Provides a pretty simple drop-in contact form w/ some configurability
- Head minifier - MEB/No, Mush together & minify parts of the `<head>`, such as stylsheets & scripts, possibly images
- Resource R("some.value") - NO/Meb, Provides site-wide resource lookup
- PHP-Mime-Delivery - NO, An opensource software project helping folks deliver raw files on their websites
- Hours-Selector - NO, An opensource HTML & CSS project for providing an hours-picker UI. May include a collection of similar/competing projects
- PHP-Autoloader - NO, An opensource autoloader for PHP
- Query-args - NO, a tool for building MYSQL queries in PHP
- Head Merger - NO, a php-lib for combining `<head>` tags together into one `<head>` tag
- Meta-deriver - NO, a php-lib to derive title, image, description, etc from Schema Markup
- Call-Permissions - NO, a software lib for micro-managing access to particular pieces of "public" code

## Supporting
- Delegate - YES, Object Oriented cooftenllaboration of global paramaters
- Task Director - YES, Complex, ordered task management

## Hobby
- WYSIWYG - NO, a full-featured utility for building websites, templates, and content
- MTG Wiz - NO, a website for looking up MTG cards & possibly buying them locally
- Download-Sorter (directory bookmarks) - NO, a browser extension to ease-ify selection of download directory
- PHP Tools (PDO, Arrays, Strings) - NO, a suite of tools for PHP Web Development
- Web-notes - NO, a personal note-taking web-app w/ integrated tag search
- Multi-lingual PHP - NO, a software package to facilitate calling code of other languages through direct PHP object-oriented methods

## Other


## Not Interested
- Super Smash Move-Masher - NO, a website/tool for smash-ultimate players to create their own character move-set mashups & share them
- Color Manager - NO, A web-tool for managing & building a color pallette for your brand / website / whatever
- PHP-plus - NO, compile PHP source code into a more-object-oriented syntax (wrapping strings & arrays in objects)
- JS Trait - NO, provide trait-like functionality to a JS class