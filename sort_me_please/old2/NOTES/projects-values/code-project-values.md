What are my core values?

They are not:
- making money
- simply producing
- simply giving back to society

There's realms of code I write (or want to write). Often times the Fun realm is also useful & the useful realm is also fun
- For Fun, because I feel like it, because something seems cool
	- mtg, WYSIWYG,
- Something Useful - Something I want to use that I couldn't find (or I just want to roll my own for fun)
	- Liaison, User app, Article App, Notes WebApp, WYSIWYG
	- Driven by "I want it to exist. I think it would be a really cool tool to have. And I think others might like it too"
- To make money
- To make the world better

My Projects (& The motivation / values for creating them)


Thought: Add "#4 SHOULD it be a priority?" (& then unpack what "SHOULD" means)

For each project:
	1) Why did I start it?
	2) Is it a priority? (Yes, No, Maybe)
	3) What do I want to do with it?
	4) Why do (or would) I want to complete (or update) it?

What core value is represented by each 'why'?




# Yes
- Liaison
	1) To simplify web-site & web-app development for myself & others
	2) Yes
	3)  a) To develop full-gui applications that are easily integrated into sites
		b) To build application-driven (thus easily-maintained) websites
		c) To compete with Wordpress & improve the world of web-development
		d) Eventually to improve the world of website-building (for non-developers)
		e) To build a PHP App Store, developer app-store, theme-store
		f) To build cross-framework applications
		g) (distant future) To build cross-platform (including mobile, desktop, & server-side) applications
		i) (very distant future) To build a PHP-driven desktop & mobile operating system
	4)  a) OTHERS_SUCCESS To help others build websites (devs & non-devs)
		b) OTHERS_ENJOYMENT To make coding easier and more fun
		c) OTHERS_ENJOYMENT To make website better for the general public
		d) FUTURE_SUCCCESS To build my long-term software-company-career
		e) PERSONAL_FULFILLMENT To satisfy a vision that I've had for years, of building my own framework
		f) PERSONAL_FUN To build software (PHP desktop apps) that I've envisioned & want to build
- Liaison\User (does this package include user-based permissions?)
	1) To provide a 2-line integration of a user-login system on a website, including full GUI
	2) Yes
	3)  a) To provide user-login on my own websites
		b) To provide easy user-login setup for other folks' websites
		c) As a 'hook' to get people to find out about Liaison & maybe start using it for their own projects
	4)  a) COMMUNITY To make my sites interactive
		b) OTHERS_SUCCESS To make others' sites interactive
		c) OTHER_PROJECT To help Liaison grow
- JS-Autowire
	1) To make javascript development & Dom-interactions simpler
	2) Yes (but not much needs done other than documenting)
	3)  a) #1
		b) To make page-development easier for the public
		c) For fun
	4)  a) OTHERS_EASE
		b) OTHERS_EASE
		c) PERSONAL_FUN
- Delegate (to retrieve values)
    1)  a) For a global-configuration system & data transfer system for Liaison
    2) Yes
    3)  a) Global config & data transfer for Liaison
	4)  a) OTHER_PROJECT
- Task Director
    1) To schedule tasks for Liaison
    2) Yes
    3)  a) For Liaison
        b) Because I couldn't find anything comparable
        c) Possibly for other task/event driven applications... eventually
	4)  a) OTHER_PROJECT
		b) USEFUL it fills a gap in available software

# Maybe, Lean Yes
- Liaison\Article
	1) To provide a drop-in for basic content-creation for any PHP-powered website
	2) Maybe (lean Yes)
	3)  a) To provide article-writing & content-creation on my own websites
		b) To make it easy for other web-devs to do the same on their own
		c) As a 'hook' to get people into Liaison
		d) To possibly monetize with additional themes & extensions
		e) To provide a drop-in Gutenberg, or Gutenberg(+non-gute extensions), or a gutenberg-like eco-system
	4)  a) TEACH & SHARE To publicly share my ideas, teachings, & content
		b) OTHERS_SUCCESS To help others share content
		c) OTHER_PROJECT help liaison grow
		d) MONEY to monetize
		e) USEFUL & EASE To simplify use of content-creation software (gutenberg)
- Local NFP | Causes | Decatur Cares website
	1) To provide publicity to all the folks who are doing good locally
	2) Maybe (lean yes)
	3)  a) To help local organizers, activists, and NFPs (incorporated activists lol) to reach volunteers & activists
		b) To help O&A&NFPs reach the general public
		c) To help O&A&NFPs look professional by having an official public page
		d) To find customers for me to build them custom websites!
	3)  a) COMMUNITY, OTHERS_EASE
		b) COMMUNITY, OTHERS_EASE
		c) OTHERS_SUCCESS
		d) MONEY
- FormTools
	- Anti-Spam
        1) To make a non-google spam-controller & for fun
        2) Maybe (lean yes)
        3)  a) To protect user data (all on one-server. No Google)
            b) To simplify form development (for myself & others)
		4)  a) PRIVACY (getting away from google)
			b) OTHERS_EASE 
			c) PERSONAL_EASE
	- Input-Sanitizer
        1) To remove boilerplate code for form input. For fun
        2) No
        3) #1
	- Auto-filler
        1) To make object editing easier
        2) Maybe (lean yes)
        3)  a) To make my code-life easier
            b) To make life easier for other coders
		4)  a) PERSONAL_EASE
			b) OTHERS_EASE
	- Auto-saver
        1) To make DB-connected forms easier to create
        2) Maybe (lean yes)
        3)  a) To make dynamic websites easier to think about
            b) To help others have an easier time creating database-connected forms
		4)  a) PEACE_OF_MIND
			b) PERSONAL_EASE
			c) OTHERS_EASE
- PDO backed-by-file (for "static" site management through FTP)
    1) To simplify access to the database
    2) Maybe (lean yes)
    3)  a) So I can work on my dynamic sites in a static-feeling FTPish Fashion (So databases don't have to be dumped & uploaded manually & so DB content is backed up always. Note that server-side files will still need to be backed up)
        b) So Liaision is easier to use
        c) So it's easier for anyone to work on a website (by viewing & editing the static files instead of interacting with the DB)
	4)  a) PERSONAL_EASE
		b) OTHER_PROJECT
		c) OTHERS_EASE

# Maybe
- The Data Website (BearData.com?? BerrData DataBear) - To curate many areas of data into one place with visualizations & easy-to-understand summaries, along with links to (& downloads of) source-data. (Think IL Traffic-Stop Data)
	1) To help people see the reality of the world we live in
	2) Maybe
	3)  a) To clear up misconceptions & illuminate info about things like violent crime, property crime
		b) To pull many sources together into one-place
		c) To provide a public-resource for government-collected data
	4)  a) LEARNING To help folks be meaningfully informed about data
		b) OTHERS_EASE To simplify research for folks
		c) OTHERS_EASE Simplify public engagement with government
- ReedyBear.com (ByteSizeBear, BearBytes, BearBites)
	1) To have a personal website for presenting my projects & writing tutorials
	2) Maybe
	3)
	4)  a) LEARNING To share code-related educational articles
		b) USEFUL To host whatever-might-come-along
		c) USEFUL & SHARE To host tools that I find useful & might want to share with others
		d) SHARE To maybe blog
		e) MARKET-SELF & projects To have a one-stop-shop for all-things-ReedyBear
		f) MARKET-SELF Maybe as a business-site for marketing myself & my website-building skills
- Shop-Decatur / DecaturDirectory / BuyInDecatur / DecaturThrift
	1)  a) To make money
		b) To help local stores get more sales
		c) To help local residents find thrift (& other) stores easily
		d) To reduce waste (Thrift)
		e) To build a local-sales-network (think CraigsList or FB Marketplace)
	2) Maybe
	3)  a) Integrate an ethical scorecard to promote moral business practices
	4)  a) MONEY To make money
		b) OTHERS_SUCCESS To help businesses succeed in the novel world of Corona
		c) MORAL_CONSUMERISM


# Maybe, Lean No
- PHP-View-Controller | Template-engine
	1) To provide features and a syntax that are not found in TWIG
	2) Maybe (lean No)
	3)  a) To complement Liaison's Theme/Template & View system
		b) See #1
		c) To provide content-building tools to other developers
	4)  a) OTHER_PROJECT
		b) USEFUL
		c) OTHERS_SUCCESS
- PHP-Tester
	1) To provide a web-interfaced Unit Tester (so CLI is not required)
	2) Maybe (lean No)
	3)  a) To make non-CLI testing available to the public
		b) Make web-development & project management easier
	4)  a) OTHERS_EASE To help others ship successful code more easily
		b) OTHERS_EASE to help project management be easier
- PHP, HTML, Javascript contact form Lib
	1) Because I needed a contact form for Macon Zero
	2) Maybe (lean no)
	3)  a) To create a 1-line drop-in full GUI contact-us App
		b) To simplify this part of building a website
		c) To show off Liaison-Theme's abilities
		d) As a 'hook' to draw folks to using Liaison for their sites
	4)  a) OTHERS_EASE
		b) OTHER_PROJECT
		c) PERSONAL_EASE
- Head minifier (mush multiple scripts / stylesheets together. Meb suggestions for image-sprite-ing)
    1) To try a different approach for condensing requests
    2) Maybe (lean No)
    3)  a) To have something that can be implemented on any server running PHP
        b) To help others provide fast websites in an easy-way
        c) To play with file-caching and string-hashing
        d) To play with PHP's DOMDocument
	4)  a) OPENNESS
		b) PERSONAL_EASE 
		c) PERSONAL_FUN
		d) PERSONAL_LEARNING
		e) USER_EXPERIENCE
		f) OTHERS_EASE

# No, lean Maybe
- Resource - R("some.value")
	1) To simplify project configuration & resource lookups
	2) No (Lean maybe)
	3)  a) To make it easy for other devs to handle resources in their projects
	4)  a) OTHERS_EASE
- How Ethical Is (say AT&T vs. Verizon vs. T-Mobile vs. Sprint AND/OR standalone cards for individual companies)
    1) To make it easy to be an ethical consumer
    2) No (lean Maybe)
    3)  a) To make it easy to be an ethical consumer
        b) To influence companies to be more ethical
        c) To reduce the harm caused by consumerism
	4)  a) MORAL DECISION MAKING
		b) MORAL CONSUMERISM / human rights / environment (& what else, really, is there?)
		c) HEALTH & WELLBEING
		d) LIFE
- PHP, HTML, & Javascript Calendar Lib
	1) I needed a calendar for Macon Zero (I think?)
	2) No (lean Maybe)
	3)  a) For fun
		b) For use in many/all of my websites
		c) To integrate with an open-source events-system
		d) To empower web-developers to integrate things into their own back-end instead of relying on external (Google) servers
	4)  a) PERSONAL_FUN
		b) USEFUL
		c) OTHER_PROJECT
		d) PRIVACY (getting away from google)
- Human Curated (& tag-driven) Search Engine
	1) To improve the quality of the online research experience
	2) No (but eventually? It seems like a HUGE project that can never be completed)
	3)  a) To make it easier to find what you're looking for online
		b) To compete with Google & reduce their invasive ad-targeting footprint
		c) To get rich as fug
	4)  a) OTHERS_EASE
		b) PRIVACY
		c) MONEY
- Fact-finding & building site
	1) To improve public-education & ease-of-research by curating "claims" together w/ source reviews, so instead of searching for articles, you search for information & you get information (with sources attached)
	2) No (Seems like a huge undertaking that will take a long time to get off the ground)
	3)  a) To facilitate online-research (a private-version of the tool could curate content together for yourself)
		b) To help with the "fake-news" problem
		c) To group like-resources together & provide summaries of long-exhausting-to-read articles that are easily digestible
		d) To create a reliable system of "trust"
		e) Community-driven fact-building... like Wikipedia, but bite-size-facts
	4)  a) LEARNING
		b) HONESTY
		c) OTHERS_EASE
		d) TRUST
		e) COMMUNITY
- Software Dev App Store
	1) To make it easier for developers to find software they need
	2) No (Lean maybe)
	3)  a) 
	4)  a) OTHERS_EASE
		b) MONEY
		c) OTHER_PROJECT
		d) OTHERS_SUCCESS
		e) PERSONAL_EASE


# No
- MTG Wiz
	1) Because I was disappointed with existing MTG card-search tools
	2) No
	3)  
	4)  a) PERSONAL_FUN It's fun to work on
		b) OTHER_PROJECT Helps me develop my tag-based searching system
		c) LEARNING I get experience parsing large-amounts of complex data
		d) USEFUL I want to use it
		e) OTHERS_ENJOYMENT I think other people would enjoy it
		f) MONEY I could monetize it (ads, store partnerships)
		g) OTHERS_SUCCESS I could help local MTG stores increase sales through local-online-ordering
- WYSIWYG
	1) I couldn't find an existing open-source WYSIWYG
	2) No
	3)  A slimmed-down version to create content-pages (blog/article) would be incredible
	4)  a) PERSONAL_FUN It's fun to work on
		b) USEFUL I would LOVE to be able to use it for designing components, website / etc
		c) OTHER_PROJECT Eventually to work as a site-builder for anyone who's using Liaison (including non-developers)
		d) OTHER_PROJECT A slimmed-down version to create content-pages (blog/article) would be incredible
		e) MONEY Maybe monetize?
		f) OTHERS_SUCCESS Maybe help business-owners & developers build websites more easily
- MaconZero.com
	- www.maconzero.com
		1) To build a zero-waste educational site for Macon Zero, plus a business page
		2) No
		3)
		4)  a) HONESTY & CLARITY So the information about the org is accurate
			b) TEACH To publish educational information (articles)
	- eco.maconzero.com (thought - work with lawn-care companies to pay for educational marketing materials & list Lawn-Care companies who sponsor the effort AND have their own eco-friendly services that meet the recommendations found within)
		1) To build a zero-waste educational site for public consumption
		2) No
		3)
		4)  a) FOLLOW-THROUGH To fulfill our original mission
			b) FOLLOW-THROUGH To be doing SOMETHING good... even if it's just online content
- Is This Recycleable?
	1) Never started it
	2) No
	3)  a) To build a simple resource for checking if specific items are recylable locally (such as pringle cans, goldfish bags, ice-cream containers, paper milk cartons, etc)
		b) Possibly as a MaconZero project
	4)  a) LIFE To make it easier for people to be good environmental stewards
		b) OTHER_PROJECT To give Macon Zero more reach, allowing our mission to be more successful
- Download-Sorter (directory bookmarks)
	1) To make my life a little bit easier, to have fun, to learn a new area of development
	2) No
	3)  a) To make a tool that will simplify others' lives
		b) See #1
	4)  a) OTHERS_EASE 
		b) USEFUL
		c) PERSONAL_FUN
- Color Manager - To build color profiles, see how colors contrast against one another, and get output of your color-profiles in multiple different formats
	1) A design-video gave me the idea & it seemed cool & seemd like it would be fun to build
	2) No
	3) Not especially inspired or interested. Just might be fun, I guess. I suppose it could be a useful tool, but that's really not my concern
	4)  a) PERSONAL_FUN
		b) OTHERS_SUCCESS
		c) USEFUL
- PHP Tools (PDO, Arrays, Strings)
	1) To simplify some unnecessarily complex operations & reduce boilerplate code
	2) No
	3)  See #1
	4)  a) PERSONAL_EASE 
		b) PERSONAL_FUN Make web development less cumbersome
- Tokenizer
	1) To process City Ordinance PDFs into hierarchies w/ DB representation & HTML
	2) No
	3)  a) For fun
		b) To improve processing of my `.rof` file extension
		c) To start-again on the City Ordinance PDF processing
		d) To provide a really cool text-processing tool to the public
	4)  a) PERSONAL_FUN
		b) OTHER_PROJECT
		c) FOLLOW_THROUGH
		d) OTHERS_SUCCESS (text-processing tool for the public)
- PHP-Mime-Delivery
	1) To make it super easy to deliver raw files through PHP w/ caching included
	2) No
	3)  a) To simplify web-development for other developers
	4)  a) OTHERS_EASE To make web-development easier for others (It's already baked into Liaison... so)
- Hours-Selector
	1) To have a ready-made hours-selector that can be dropped into any form
	2) No
	3)  a) See #1
		b) Make it available for the public
	4)  a) PERSONAL EASE
		b) OTHERS_EASE
- PHP-Autoloader
	1) To provide PSR-autoloading for non-composer environments
	2) No
	3) To make it easier for developers to integrate open-source projects into their own
	4) OTHERS_EASE
- NFP Team Hub -  Provide accounting, document storage, team settings, email lists, and more
	1) To organize documents, tasks, & other NFP-related things for Macon Zero & Decatur Pride
	2) No
	3)  a) To provide a one-stop-shop for NFP stuff on the NFP's own website
		b) Make it easier to run a non-profit organization
		c) To make it easier for other NFPs to run themselves
	4)  a) OTHERS_SUCCESS
		b) OTHERS_EASE
		c) OTHERS_EASE
- PHP, HTML, & Javascript Events Lib
	1) Because I've needed it in the past & will need it again
	2) No
	3)  a) to make web-devs reliant only on their own backend
		b) For non-profit events posting (personal projects)
		c) To provide schema-ready events for websites
	4)  a) PRIVACY
		b) EASE
		c) OTHER_PROJECT
		d) OTHERS_SUCCESS
- Web-notes (w/ tag-based search)
	1) To help me take better notes & do better research
	2) No
	3)  a) #1
		b) To develop the tag-based searching system/library
		c) A first/early-version prototype of the Fact website & Web search engine
	4)  a) EASE
		b) OTHER_PROJECT tag-system
		c) OTHER_PROJECT fact website
- Query-args
	1) To produce boiler-plate code for building web-form submissions (to make shhh easier)
	2) No
	3)  a) To build a more robust set of code-related tools which includes this one
		b) To suggest my Forms Tools to folks (which doesn't really exist yet lol)
		c) To make some code-writing tasks simpler & quicker
	4)  a) GIVING BACK robust suite of tools
		b) OTHER PROJECT (Form Tools)
		c) EASE
- Tag Search System
    1) To standardize the search-setup that I want to use for... basically everything
    2) No
    3)  a) To fuel the MTG search site (but I'm working on the tag-stuff standalone there)
        b) To use for looking up data (part of the Data-Tools)
        c) To make it easy to make a site searchable & make that search relevant & easily curated
	4)  a) OTHER_PROJECT mtg wiz
		b) OTHER_PROJECT data-tools
		c) PERSONAL_EASE
		d) COMPETITIVE EDGE
- Meta Package
	- Website menu builder
        1)  a) For fun
            b) To simplify web-menu creation
        2) No
        3)  a) For fun
            b) To work with Liaison Themes
            c) As part of a larger "Site Admin" project
		4)  a) PERSONAL_FUN
			b) OTHER_PROJECT liaison themes
			c) OTHER_PROJECT site admin
	- Page details editor (title, description, canonical url, etc)
        1)  a) For fun
            b) To simplify website administration
		2) No
		3) ...
		4)  a) EASE
			b) OTHER_PROJECT site admin
- Site Admin - to manage content on a website
    1) Haven't started, but I'd like to compete with wordpress in the DIY website front
    2) No
    3)  a) To grow Liaison beyond the developer community
        b) To provide admin-features for Liaison-apps
        c) To provide an admin-interface for the Meta Package
	4)  a) OTHER PROJECT liaison for non-developers
		b) OTHER PROJECT liaison for developers
		c) OTHER PROJECT meta package
		d) EASE of website management
- Data-Tools - Tools for processing & visualizing data
    1) The Decatur-Traffic-Data needs features this package would offer & I've come across several other sets of data for which such a tool would be useful
    2) No
    3)  a) To make it easy for me to keep the public informed about things like traffic data, weather-history, police use-of-force, etc
	4)  a) HONESTY
		b) OPENNESS
		c) EASE
		d) LEARNING
- LoveDecatur.com
    1) To publicize information about Buffett's DUI grant
    2) No
    3)  a) To help Decatur citizens engage with local government
        b) To provide a one-stop-shop for news-sources for Decatur
        c) To promote causes that are good for people of Decatur
        d) To simplify the interface for interacting with the city (ordinances, multiple government entities, etc)
	- FOIA education
        1) To help citizens get the information they need & help them do information-based activism
        2) No
        3)  a) To make local FOIA-onboarding SO MUCH easier
            b) To help expose local corruption
	- FOIA upload & search (see "- FOIA education")
	- Decatur Law
        1)  a) For fun
            b) To make it easier to look up local laws
        2) No
        3)  a) To work on my Tokenizer & learn about text-processing
            b) To draw attention to LoveDecatur & make it more useful
            c) Part of the one-stop-Decatur-deal
	- Council Meetings
        1)  a) To get more people at council meetings
            b) To encourage folks to stand up to local government
        2) No
        3)  a) #1a 
            b) #1b
            c) Build a presence for LD & make it more useful (thus improving the over-all reach I have)
	4)  a) HONESTY
		b) ACTION
		c) LEARNING
		d) OPENNESS
		e) POWER reach
- IL Law site
    1-3) See "- LoveDecatur.com - Decatur Law..."
	4)  a) LEARNING
		b) EMPOWERMENT
		c) EASE
- PHP-JS & PHP-Python & other PHP-calls-other-programming-language... things
    1)  a) For fun
        b) To use Javascript's DOM features (instead of PHP's... Ugh)
    2) No
    3)  a) For fun
        b) The DOM features of JS
        c) To make multi-lingual server-side projects easy to implement
	4)  a) PERSONAL_FUN
		b) USEFUL
		c) FUTURE_SUCCESS
- PHP Documentor
	1) To provide web-based documentation for my own projects
	2) No
	3)  a) #1
		b) As a flagship project to show off the theming-abilities of Liaison-Theme
		c) To improve documentation for any/all public code-projects (I could even scrape github)
	4)  a) PERSONAL_FUN
		b) OTHER_PROJECT liaison & themes
		c) LEARNING
		d) EASE
- Call-Permissions (only allow calling from certain files || lines of code || classes, etc)
    1)  a) For fun
        b) To secure applications (Or more to prototype a method of doing it)
    2) No
    3)  a) Fun
        b) Secure code
	4)  a) PERSONAL_FUN
		b) SECURITY
		c) PRIVACY
- Super Smash Move-Masher
	1) Nathan & I talked about... something & it sounded like fun
	2) No
	3)  a) To provide a fun tool for the public to share smash-related content & ideas
		b) To test out my Javascript DOM Engine & Liaison
		c) For fun
	4)  a) PERSONAL_FUN
		b) OTHER_PROJECT (js-autowire)
		c) OTHERS_ENJOYMENT
- Head Merger (provide a <head>CODE</head> and merge CODE into the page's <head>)
    1) To try out a different way of adding code to the <head> of a Liaison Theme
    2) no
    3)  a) I'm not really
        b) To provide multiple options for adding code to pages through Liaison
	4)  a) OTHER_PROJECT liaison & themes
		b) EASE
- Meta-deriver (Derive title, image, description, etc from Schema Markup)
    1) To remove repetitive code
    2) No
    3)  a) So Schema-representation & <head> Meta-data can be in sync 
        b) To encourage web-page markup
	4)  a) CONSISTENCY
		b) EASE
		c) CLARITY
		d) OPENNESS
- PHP-plus (auto-wrap strings/arrays in objects to make them easier to work-with)
    1)  a) For fun
        b) To improve Dev Experience for PHP
    2) No
    3)  a) No real interest
        b) To improve DevX 
	4)  a) EASE
- JS Trait - provide trait-like functionality to a JS class
    1) To add extensibility to JS-Autowire
    2) No
    3) I'm not interested. 
        a) To make javascript more extensible and object oriented
	4)  a) COLLABORATION
		b) EASE