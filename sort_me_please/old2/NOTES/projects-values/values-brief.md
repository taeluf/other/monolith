

# ALL values, & their counts

- 28 OTHER_PROJECT
- 23 OTHERS_EASE
- 14 PERSONAL_FUN
- 12 EASE
- 11 OTHERS_SUCCESS
- 10 PERSONAL EASE
- 10 USEFUL
- 8 LEARNING
- 6 PRIVACY
- 6 MONEY
- 4 OPENNESS
- 4 OTHERS_ENJOYMENT
- 4 COMMUNITY
- 3 FOLLOW THROUGH
- 3 FUTURE SUCCESS
- 3 HONESTY
- 2 LIFE
- 2 MARKET-SELF
- 2 MORAL CONSUMERISM
- 1 ACTION
- 1 COLLABORATION
- 1 CONSISTENCY
- 1 CLARITY
- 1 COMPETITIVE EDGE
- 1 EMPOWERMENT
- 1 GIVING BACK
- 1 HEALTH & WELLBEING
- 1 MORAL DECISION MAKING
- 1 PEACE OF MIND
- 1 PERSONAL FULFILLMENT
- 1 PERSONAL LEARNING
- 1 POWER (having power / influence over the world)
- 1 SECURITY
- 1 TRUST
- 1 USER EXPERIENCE

# Actually a value

- quality of experience
    - EASE
    - FUN
    - ENJOYMENT
    - PEACE OF MIND
- working with others
    - GIVING BACK / CONTRIBUTING TO THE WORLD (but not necessarily the economy)
    - COMMUNITY
    - COLLABORATION
    - ACTION
    - HELPING OTHERS
- ethical
    - PRIVACY
    - LIFE
    - HONESTY
    - MORALITY
    - HEALTH & WELLBEING
    - TRUST
- material
    - FOLLOW THROUGH
    - MONEY
    - SECURITY
    - PERSONAL POWER
    - CONSISTENCY
    - CLARITY
- empowerment
    - DISTRIBUTED POWER
    - KNOWLEDGE 
    - SKILLS
    - OPENNESS
    - SUCCESS

Who is benefited?
- Myself
- Developers
- General Public
- Activists
- Government
- Businesses
- Economy



# Some rambling...?
Whatever I decide to really invest my time in should have a range of benefits, including:
- Personal Growth
- Material growth (money)
- Career Growth
- Helps someone else (giving back)
- Makes something easier 
- Empowers individuals
- Helps the environment
- Brings people together
- Respects: Personal Privacy, Public Transparency


There should be a personal benefit (money, career path, learning something), benefit for an individual (meet a specific need, make a task easier), a community benefit (brings people together, empowers citizens), and an at-large benefit (the environment, human rights)


Whatever project I decide to take on very seriously should:
- Personal
    - Teach me something code-related
    - Teach me something about the world
    - Bring in money
    - Benefit other projects of mine
    - Support a long-term vision I have
- Others 
    - Bring people together
    - Make someone's life easier
    - Empower people
    - Bring about action, NOT idle consumption
- Ethics
    - Respect privacy
    - Do no harm (environment, human rights, economy)
    - Free from biasoing 
    - Be transparent (in intent, shortcomings, ethical failings)


Values... By max priority
- Bring about action (not idle consumption)
    - Action may be activism, selling products, going to events, congregating with friends, sending emails, protesting, creating crafts, creating software, or anything else that's essentially more than "Oh I'm mad about that news. Now let me get on with the rest of my life."
- Brings me long term benefit
    - This may be money, building up other projects, improving my overall reach, teaching me skills
- Be inline with my moral values
    - Individual-empowerment, do-no-harm, privacy, transparency (about intent & shortcomings), money is secondary to mission
- Benefit someone else
    - Teaches something, improves access to resources, helps someone organize for a cause, provides a tool for increasing sales or managing complex things, 
- Teaches something
    - whether it's the general public interacting with the site, the base (such as businesses who have listings), government officials, or whatever... somebody needs to learn something
- Builds community
    - Whether online, locally, nationally, etc. Just as long as it involves people working together toward a common cause
- Is easy & fun to use
    - Calls for intuitive design, achievement/awards system, is not overwhelming, provides some non-serious benefit 