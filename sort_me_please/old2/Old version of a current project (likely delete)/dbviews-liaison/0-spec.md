# New Component Design

There will be two versions of the new component design. The first will be, basically, flat. At least it contains a view, and probably a Form. But it can have a lot of features. 

The flast structure is like so:
    
ComponentName
    - View.php
    - Form.php
    - Class.php (or ComponentName.php) (or Core.php ??)
    - event/
        - event.name.php
        - another.event.php
    - error.php 
    - OR error/
        - 404.php
        - 400.php
        - any.php or error.php or default.php



## The most basic first version
    1. Create a View
    2. Create a Form
    3. Create a public file which creates the component & displays the view
    4. Pub file, create component, display the form
    5. pub file, create component, submit the form


## Things it should do
- Specify route inside the view file, likely with markup
- Form is auto-routed
- Submission is automatically handled, including verifying post data against the form structure
- View & Form can be auto-loaded by specifying HTML Attributes
- The auto-loaded object is accessible by PHP anywhere in the view file
- Permissions & Roles can be specified with markup on the view or with a custom html tag
- Can add a custom Submit.php for completely custom handling
- specify a "format" attribute to customize output on view. Maybe call it "decode"?
- specify an "encode" attribute to customize encoding when received from form submission
- The component can expose functions to "encode" and "decode" in its views
- Minor-compilation pre-removes rb-attributes & queries inline, prior to the view being 'required'
- No use of DOMDoc in production
- Can set event handlers
- rb-var="cat" to expose entity as `$cat` in php
- Looping is available, so displaying a single view CAN show multiple copies of the view 
- Can seamlessly display after a query OR from explicit input
- Can write custom submission hooks to encode user submission, add properties, or whatever
- markup contains all necessary validation-information, is NOT delivered with the form and IS checked before submitting


## Eventually? 
- query language is dot sql (changing from id:<?=$id?>)
- Convert routing to pure regex & compile an .htaccess that handles all routes
- Can expose functions globally, to itself & to other components
- Works without Liaison ?? Better with it
- Query library can be changed with ease (currently RDB will be tightly integrated???)
- The markup-processing can be extended & features can be added
- Can use {{curly_brace_variables}} instead of PHP in places
- Pre-Compiles the component for performance as well as features... possibly into a single file!
- Can seamlessly integrate with a GRAPHQL API
- All "encode" and "decode" function calls to include paramaters. Currently only simple things like "md" will work
- Allow piping. Ex: "md | noscripts" to convert markdown to html, then pass that html to the "noscripts" function which will remove any <script/> tags and inline javascript
