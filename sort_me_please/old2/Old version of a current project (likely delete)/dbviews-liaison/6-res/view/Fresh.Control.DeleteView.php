<?php

if (!$fresh->callHandler('Fresh.Item.Delete', $rb_object, $table, $rb_object->id)
    ||($_GET['id']??null)=='new')return;

/**
 * To add a delete button to your form, use this code
 * ```php
 * <input type="hidden" name="fresh_delete" />
 * <?=$delButton = $lia->view('Fresh.Control.Delete',['rb_object'=>$rb_object,'table'=>$table,'id'=>$rb_object->id]);?>
 * ```
 * 
 * @export(Control.Delete)
 */
?>
<input type="Submit" 
    onclick="if (!confirm('Delete this?')){event.stopPropagation();event.preventDefault();} else {this.name='fresh_delete';}" 
    value="Delete" 
    style="background-color:red;padding:0.4rem 1.0rem;border-radius:0.5rem;" 
/>