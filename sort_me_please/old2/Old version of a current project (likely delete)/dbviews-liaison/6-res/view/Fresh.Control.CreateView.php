<?php

// echo 'view blah';
// exit;
if (!$fresh->callHandler('Fresh.Item.Create', $table)
    ||($_GET['id']??null)=='new')return;

/**
 * To add an edit button to your form, use this code
 * ```php
 * <?=$lia->view('Fresh.Control.Create',['table'=>$table, 'text'=>'New Item');?>
 * //table is required. text is optional.
 * ```
 * Inside your `<form>` entity tag, you will have access to `$table`. Outside it, you will have to write out the table.
 *
 * @export(Control.Create)
 */
?>

<button 
    onclick="event.preventDefault();event.stopPropagation();window.location.href='?id=new';"
    style="background-color:green;padding:0.4rem 1.0rem;border-radius:0.5rem;"
><?=$text??'New Item'?></button>
