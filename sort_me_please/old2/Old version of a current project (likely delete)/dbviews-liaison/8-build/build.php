<?php

require_once(__DIR__.'/../../../../../depend.php');

$libDir = dirname(__DIR__);
$docu = new \TLF\Docu($libDir, '0-doc', '8-build/docsource', '8-build/README.src.md');
$docu->document();

$unusedFile = $libDir.'/0-doc/exports/unused-exports.md';
$unusedExports = file_get_contents($unusedFile);
file_put_contents($libDir.'/0-doc/2-Everything-Else.md',$unusedExports);