
# @export(Class.Component)
# Create a component
`new \Fresh\Component($name,$dir);`
I'm not actually sure what the name is used for, if anything.  
If you `extends \Fresh\Component`, you can declare `$name` and `$dir` as protected properties then one does not need to be supplied via the constructor


# @export(Feature.getView)
# Load a view
The `$passthru` will be [`extract`](https://www.php.net/manual/en/function.extract.php)ed so the the view file can access values directly by their keys.  
`$passthru` is optional. 

```php
$compo = new \Fresh\Component($componentName, $componentDir);
$passthru = ['id'=>1];
$viewName = 'Note';
$view = $compo->view($viewName,$passthru);
echo $view;
```
This will load the file at `$componentDir/view/$viewNameView.php. To be clear, your view file must end in `View.php`


# @export(Feature.getForm)
# Load a form
The `$passthru` will be [`extract`](https://www.php.net/manual/en/function.extract.php)ed so the the form file can access values directly by their keys.  
`$passthru` is optional. 

```php
$compo = new \Fresh\Component($componentName, $componentDir);
$passthru = ['id'=>1, 'submit_url'=>'/submit/myform/'];
$viewName = 'Note';
$view = $compo->form($viewName,$passthru);
echo $view;
```
This will load the file at `$componentDir/view/$viewNameForm.php. To be clear, your form file must end in `Form.php`.


# @export(Feature.filterSubmission)
## Filter submitted data
I haven't tested this, and I don't trust it. But here's my best notes on it.

This uses a `filter` handler, which you set by:
```php
$compo->setFilterHandler(
    function($name, $value, $input){
        //modify the value, like normalizing a phone number
        return $modifiedValue; 
    }
)
```

I'm confused, and not 100% sure how this works in practice.  
Anyway, for a `$_POST['somekey'] = 'some value';`, there should be an `<input name="somekey" />` in the source form.  
Your `filter` handler will be called as `$filter('somekey','some value', $domInputNode)`.  
If there are multiple `filter` handlers... it might break.


# @export(Method.parseLookupStr)
## Handling `rb-find` parameter.
You can write `rb-find` in any way you want, but the built in idea is a simple list of `key1:value1;key2:value2;`  
call $compo->parseLookupString($find) to break those into an array. 

If your key or value contains a colon (:) or semi-colon (;), that'll probably break things.


# @export(Handler.filter)
## Submission Handler


# @export(Handler.Compile.Form_Entity_Loop_Start)
Call `$compo->addCompileHandler('Form.EntityLoop.Start', $callable)`  
`$callable` should:
- Accept NO paramaters
- Return a string of valid PHP code
   - DO NOT include open & close tags @ beginning and end of your code.
      - You MAY close & re-open amidst your code.

@TODO Maybe pass some paramaters to the callable???


# @export(Class.Handler)
Handler is the source for extensability. You can set a handler either on the component & it will propagate, or set it directly on a view.


# @export(Internals.Handler)
Doing $handlerObject->compile will get an array of handlers in the 'compmile' domain, via the `__get` magic method.  
Then: `return (object)$arrayOfHandlers`... SO you have, simply, a cast object. 

TODO Create a HandlerDomain object that has magic methods to make calling more dev-friendly  
For each handler, if the handler was set with setHandler()... you can `$handler->compile->$HandlerName()`... and get a return value  
If the handler was put with addHandler()... Then $handler->compile->$HandlerName returns an array of handlers, which can each then be called.


# @export(Contribute.readonly)
A cute little trait to allow for public-read-only properties on a class

Declare:
```php
use Fresh\Readonly;

protected $property; // Publicly, do `$theClass->property` to retrieve... though the public cannot set it.
private $r_property; // This instructs the trait.
```


# @export(Handler.Compile.View_Entity_Loop_Start)
Call `$compo->addCompileHandler('View.EntityLoop.Start', $callable)`  
`$callable` should:
- Accept NO paramaters
- Return a string of valid PHP code
   - DO NOT include open & close tags @ beginning and end of your code.
      - You MAY close & re-open amidst your code.

@TODO Maybe pass some paramaters to the callable???


# @export(Handler.Runtime.addResources)
If your view has any resources associated, you must register an addResources handler.
- `addResources`: `function($viewObj, $resourcesList){...use your framework or whatever...}
   - `$resourcesList` is in the format ['php'=>$arrayOfFiles,'js'=>$arrayOfFiles,'css'...] & so on for EVERY file that is associated with your view.
   - These files are found at `YourViewNameView/some_file.EXTENSION`
      - I think you can do YourViewNameView.css and YourViewNameView.js as well
      - It also works with YourViewNameForm/files....


# @export(Internals.executeCompileHandlers)
Calling $viewRoot->executeCompileHandlers($handlerName, ...$argsForHandler) will return an array of return values from the called handlers.

