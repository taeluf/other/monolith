
class SchemaEditable extends FancyServer {

    _dialog(){return 'SchemaEditDialog';}
    __attach(){
        
    }
    onclick(event){
        event.stopPropagation();
        event.preventDefault();
        this.showForm();
    }
    async showForm(){
        this.oneDialog.show();
        const formName = this.node.getAttribute('form');
        const type = this.node.getAttribute('table');

        if (formName==null){
            alert("You must specify form=FormName on the root html node.");
            return;
        }
        this.class = 'AutoSchema';
        const editForm = 
            await this.fetch(
                'getForm',
                type,
                formName,
                this.node.getAttribute('lookup'),
                this.node.getAttribute('data-init')??null,
                this.node.getAttribute('options')??['abc']
            );
        this.oneDialog.setForm(editForm);
    }
}
SchemaEditable.autowire();