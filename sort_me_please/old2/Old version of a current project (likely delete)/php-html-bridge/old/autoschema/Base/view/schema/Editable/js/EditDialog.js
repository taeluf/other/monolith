
class SchemaEditDialog extends RB.Autowire {

    __attach(){
        const cancel = this.node.querySelector('.CancelDialogButton');
        cancel.addEventListener('click',this.hide.bind(this));
    }
    show(){
        console.log('showing');
        this.node.style.display="flex";
    }
    hide(){
        this.node.style.display = "none";
    }
    setForm(formHTML){
        this.node.children[0].children[1].innerHTML = formHTML;
    }
}
SchemaEditDialog.autowire();