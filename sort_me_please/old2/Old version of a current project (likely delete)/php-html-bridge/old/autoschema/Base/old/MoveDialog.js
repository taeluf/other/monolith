

class MoveDialog extends FancyServer {

    __attach(){
        console.log(this.node);
    }
    clear(){
        this.moves = [];
        this.node.innerHTML = "";
    }
    addMove(move){
        this.moves.push(move);
    }
    show(){
        let html = '';
        console.log(this.moves);
        for (const move of this.moves){
            html += JSON.stringify(move) + "\n\n";
        }
        this.node.innerHTML = html;

    }
}

MoveDialog.autowire();



// 
// class CallApi {
// 
// class CallApi {
    // static public function specials($charName){
        // $specials = [
            // 'Bob'=>["cats","dogs","babies"],
            // "Susan"=>["Kangaroos","cats",'Elephants'],
            // "Karen"=>['nope'],
            // 'George'=>['unpredictable','sneak','party'],
            // 'Julia'=>['fly','hover','yes']
        // ];
        // return $specials[$charName];
    // }
    // static public function retrieveMoves($charName){
        // return [
            // "punch",
            // "kick",
            // "plunder",
            // "{$charName} SPECIAL"
        // ];
    // }
// }


