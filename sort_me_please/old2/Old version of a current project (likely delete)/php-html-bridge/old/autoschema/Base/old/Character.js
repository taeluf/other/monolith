class SmashCharacter extends FancyServer {


    _dialog(){
        return 'MoveDialog';
    }
    __attach(){
        this.name = this.node.innerText;
        this.moves = null;
    }

    async onclick(event){
        this.showSpinnerWheelDelayed(200/*ms*/); //so if we cancel within 200ms, the spinner wheel will never show. 50-100ms is probably more UX friendly
        
/*HERE*/const moves = this.moves || await this.fetch('specials',this.name);
        this.moves = moves; 
        const dialog = this.oneDialog;
        dialog.clear();
        for (const move of moves){ 
            dialog.addMove(move);
        }
        dialog.show();
        this.cancelSpinnerWheel();
    }
}

SmashCharacter.autowire("button");