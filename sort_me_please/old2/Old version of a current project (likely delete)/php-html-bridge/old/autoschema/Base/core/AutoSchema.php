<?php

class AutoSchema extends \Lia\Compo {


    public function onRouteWillExecute($event,$url,$matches){
        $user = $event->Get_User();
        if ($event->Get('is_edit_mode'))
        {
            $event->View('depend/JSAutowire');
            $event->View('theme/BlockForm');
            $event->View('JSToPHP/ServerScript');
            $event->View('schema/Editable');
            $event->View('schema/EditDialog');
        }
    }
    
    // public function onRequestCompleted($event,$url,$output,$goodRoute){
    public function onRequestCompleted($event,$url,$output,$goodRoute){
        $editMode = TRUE; //would be derived from user = moderator & $_GET edit = true
        if (!$editMode)return;
        if (strlen(trim($output))==0)return;
        // $headers = function_exists($f='apache_response_headers') ? $f() : headers_list();
        $headers = apache_response_headers();
        if (substr($url,-1)!='/'
            ||$headers!=[]){
            echo $output;
            return;
        }
        // exit;
        // \DM::require('FormTools')
        // echo $output;
        // // echo $url;
        // exit;
        $doc = null;
        if ($event->Get('is_edit_mode')){
            $doc = new \Doc($output);
            $dialog = $event->View('schema/EditDialog');
            $doc->autoFill(false);
            $doc->enableEditMode($dialog);
            $doc;
        } else {
            $doc = new \Doc($output);

            $doc->autoFill(true);
            // echo $doc;
        }
        $event->AutoSchemaDone($doc,$url,$goodRoute);
    }
    public function onEvents_Sort($event,$EventName,$args,$matchedEvents){
        if ($EventName=='AutoSchemaDone'){
            $as = $this;
            usort($matchedEvents,
                function($a,$b) use ($as){
                    if ($a[0]==$as)return 1;
                    if ($b[0]==$as)return -1;
                    return 0;
                }  
            );
            return $matchedEvents;
        }
        // return null;
    }
    public function onAutoSchemaDone($event,$doc){
        // echo "as first";
        // exit;
        echo $doc;
    }
        // public function onSetup_Launch($event){
            // echo "launch";
            // exit;
        // // require($this->dir('vendor').'/redbean/rb-mysql.php');
    // }

    static public function getForm($event,$type,$formName,$lookup,$initData=null,$options=[]){
            // $formHTML = $result;
            // // echo $formHTML;exit;
            // $formName = $this->args[1];
            // $doc = new \Doc($formHTML,['lookup'=>$this->lookup]);
            // // $doc->setEvent($event->Get('global_event_object'));
            // $doc->autoFill();
            // // $ft->SchemaPrepareForm($formName);
            // $result = $doc.'';
        $options = \Doc::decodeDataString($options);
        $form = $event->View($formName,['type'=>$type,'lookup'=>$lookup,'initData'=>$initData,'options'=>$options]);

        $doc = new \Doc($form.'',['lookup'=>$lookup,'initData'=>$initData,'formName'=>$formName]);
        // if ($lookup=='new'||$lookup=='new;'){
            
        // } else {
        // }
            $doc->autoFill(true);

        return ''.$doc;
    }
    static public function composDir(){
        return realpath(__DIR__.'/../../');
    }
    static public function submit($data,$event){
        echo '<pre>';
        print_r($data);
        // $viewName = $_POST['view_name'] ??null;
        // unset($_POST['view_name']);
        $formName = $data['autoschema_form'] ??null;
        // unset($_POST['form_name']);
        // if ($formName===null&&$viewName!=null)$view
        $type = $data['autoschema_type'] ??null;
        $lookup = $data['autoschema_lookup']??null;
        $initData = $data['autoschema_initData']??[];
        // unset($_POST['autoschema_type']);
        // unset($_POST['autoschema_lookup']);


        if ($formName==null){
            throw new \Exception("The <form> element must specify an attribute form='name/of/View'");
        }
        $form = $event->View($formName,
            [
                "type"=>$type,
                "lookup"=>$lookup,
            ]
        );

        $options = [];
        $prefix = 'autoschema_';
        foreach ($data as $key=>$value){
            if (substr($key,0,strlen($prefix))==$prefix){
                $options[substr($key,strlen($prefix))] = $value;
                unset($data[$key]);
            }
        }
        
        $doc = new Doc($form,$options);
        $doc->setImagePaths($event->Get('image_destination'),$event->Get('image_url_prefix'));
        $doc->submit($data);
    }
}