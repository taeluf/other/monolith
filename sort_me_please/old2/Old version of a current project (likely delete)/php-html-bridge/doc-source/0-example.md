# Final Output
Given the sample component below, this will be your final code

## Note View
```php
$note = new \Fresh\Component($componentDir);
$view = $note->view('Note',['id'=>1]);
echo $view;
```
yields
```html
<div>
    This is standalone &amp; nothing fancy happens to it.
</div>
<div>
    <h6>table: note</h6> <!--$table="note"-->
    <div style="background:#EEE">
        <h1>Note: first note</h1>
        <p>the first <b>paragraph</b> of a note is always the best paragraph.</p>
    </div>
    <hr>
</div>
```

## Note Form
```php
$note = new \Fresh\Component($componentDir);
$form = $note->form('Note',['id'=>1,'submit_url'=>'/note/submit-edit/']);
echo $form;
```
yields
```html
<form action="/note/submit-edit/" method="POST"> 
    <input type="text" name="name" value="first note"/>
    <input type="text" name="body" value="the first **paragraph** of a note is always the best paragraph.">

    <input type="submit" />
</form>
```


# Sample Component

**instantiate:** `$note = new \Fresh\Component($componentDir);`

**File Structure:**
- `Note` is the name of the view & the name of the form.
- **Tip:** Namespace your views with `Page_`, `Depend_`, and other prefixes to maintain sanity. Nesting in folders is not available yet.
- All your views & forms go in the `view` directory.
- Views end with `View.php` and forms with `Form.php`

```
- component 
    - view
        NoteView.php
        NoteView.css
        NoteForm.php
        NoteForm.js
        NoteView
            - Colors.css
            - Sizes.css
```



## NoteView.php
```php
<div>
    This is standalone &amp; nothing fancy happens to it.
</div>
<div rb-table="note" rb-find="id:<?php echo $id?>">
    <h6>table: <?=$table?></h6> <!--$table="note"-->
    <div style="background:#EEE">
        <h1>Note: <?=$note->name?></h1>
        <p rb-prop="body" rb-format="markdown">This text will be replaced with the body of the loaded note.</p>
    </div>
    <hr>
</div>
```

Display the view: (we still have to setup the `find` handler)
```php
$view = $note->view('Note',['id'=>1]); //to display note with id=1
echo $view;
$view = $note->view('Note',['id'=>'*']); //to display all notes (no limits or pagination yet)
echo $view;
```

## NoteForm.php
```html
<form> 
    <input type="text" name="name"/>
    <input type="text" name="body">

    <input type="submit" />
</form>
```
And to display the form:
```php
    $form = $note->form('Note',['id'=>1,'submit_url'=>'/note/submit-edit/']);
```

## Looking up the notes

You have to set a `find` handler. We'll use a simple array lookup for this example.

```php
$data = [
    '1'=>[
        'id'=>1,
        'name'=> 'first note',
        'body'=> 'the first **paragraph** of a note is always the best paragraph.'
    ],
    '2'=>[
        'id'=>2,
        'name'=> 'second note',
        'body'=> 'the second note with a paragraph is the worst note.'
    ]
];
$note->setRuntimeHandler('find',
    function($tableName, $lookupStr) use ($note, $data){
        //Breaks key:value;key2:value2 into a key=>value array
        $lookup = $note->parseLookupStr($lookupStr);
        $notes = $data;
        if ($data[$lookup->id]==null)
            return [];
        else if ($data[$lookup->id]=='*') //convert the array entries to objects
            return array_map(function($entry){return (object)$entry;},$data);
        else if (isset($data[$lookup->id]))
            return [(object)$data[$lookup->id]];
        else 
            return [];
    }
);
```

## Handling Resources
We also have to set an `addResources` handler to handle those `.css` and `.js` files 
```php
$compo->setHandler('addResources',
    function($resourceList){
        foreach ($resourcesList['css'] as $cssFilePath){
            // my_framework_object()->addCSSFile($cssFilePath);
        }
        foreach ($resourcesList['js'] as $jsFilePath){
            // my_framework_object()->addJSFile($cssFilePath);
        }
    }
);
```
