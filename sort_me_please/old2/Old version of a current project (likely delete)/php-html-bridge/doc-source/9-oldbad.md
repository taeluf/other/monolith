
### Old, bad notes
### Hey, ignore these
There are runtime handlers and compile time handlers.
There are also queries which are a type of compile time handler.

These need to be documented.

#### Handlers (these notes are bad and might be wrong)
- Syntax for getting a particular set of handlers is:
    - $this->handler->domain->key
    - Ex: Get the compile-time handlers for Entity Props:
        $key = 'entity-prop'; //gotta use a variable because of the '-' in the key-name
        $this->handler->compile->$key;
- Call a handler by $handler(args...); They should just be callable this way... I think.
- Add a handler:
    - Add a handler to a specific view: $view->handler->addHandler('domain','key',$callbleOrWhateverData);
    - Set a single handler that cannot be overridden and cannot have more than one handler: $view->handler->setHandler('domain','key',$callableOrWhatever);
        - Issue: 'addHandler' will overwrite 'setHandler'
    - Propagate a handler to all views: $component->handler->addHandler('domain','key',$callableOrWhateverData);
- Call a handler, return values:
    - Call $view->handlerName(); to call the 'runtime' handler identified by 'handlerName'.
        - If 'runtime::handlerName' was set by 'setHandler', then the handler one and only handler will be called & the return value will be passed back
        - If 'runtime::handlerName' was added by 'addHandler', then an array of return values will be passed back, since there may be multiple handlers
        - If there is no handler found, a BadMethodCallException will be thrown


#### Query Handlers
Call $compo->addViewQuery($xpath,$callback) or $view->addQuery
Call $compo->addFormQuery($xpath,$callback) or $form->addQuery

#### Runtime handlers
Call compo->addRuntimeHandler($handlerName, $callback) to propogate to all views & forms or $view->addRuntimeHandler(...)

#### Compile time handlers
Call compo->addCompileHandler($handlerName, $callback) to propogate to all views & forms or $view->addCompileHandler(...)

- pre-doc: $cleanSource, $dirtySource, $compiler, $viewObj
    - return $cleanSource (or your modification of it)
    - the new clean source will be used for the next pre-doc handler & for the compilation steps to come


#### Getting it working
- Define a `find` handler
    - `this->addRuntimeHandler('find',$callable);` where `$callable` is like: `$callable(string $tableName, string $lookupStr)` where `$lookupString` is documented below

#### Lookup string 
is the custom format we're using in this package like `id:3;`. It's simple & terrible, but it works for now. You can use multiple things like `slug:cat;category:pet`. The resulting SQL will use an `AND` for all key=>value pairs listed