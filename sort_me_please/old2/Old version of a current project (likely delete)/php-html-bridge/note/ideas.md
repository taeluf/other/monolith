# Forms

```html
<form
    perm="role:Edit.note.this;">

    name label:
    <input type="text" name="name">

    body label:
    <textarea name="body"></textarea>
</form>

<!--


    -->

<!-- <route url="/"> -->
<!-- <perm role="View.note"> -->
<info route="/note-$id/edit/" role="Edit.note.this" >

<form rb-table="note" rb-find="id:{$id}">
    name label:
    <input type="text" name="name">

    body label:
    <textarea name="body"></textarea>
</form>


<!--


    -->

<rb-find rb-table="note" route="/note-$id/edit/" role="Edit.note.this" find="id:$id">

    <form>
        name label:
        <input type="text" name="name">

        body label:
        <textarea name="body"></textarea>
    </form>

</rb-find>
```

# Views


```html
<!-- <route url="/"> -->
<!-- <perm role="View.note"> -->
<info route="/" role="View.note" >
<info route="/note-$id/" role="View.note" >

<rb-loop><!-- or an rb-loop attribute-->
    <div rb-table="note" rb-find="id:<?=$id??"*"?>">
        <h1>Note: <?=$note->name?></h1>
        <p rb-prop="body" rb-format="markdown"></p>
    </div>
    <hr>
</rb-loop>


<route url="/">
<route pattern="/note-$id/">
<route name="Note">
<perm role="View.note">
<!-- <info route="/" role="View.note" > -->
<!-- <info route="/note-$id/" role="View.note" > -->

<div rb-table="note" rb-find="id:<?=$id??"*"?>" loop>
    <h1>Note: <?=$note->name?></h1>
    <p rb-prop="body" rb-format="markdown"></p>
</div>
<hr>





<div 
    rb-route="
        url:/;
        pattern/note-$id/;
        name:Note;
        " 
    rb-perm="
        can:View.note;
        role:Admin;        
        "
    rb-table="note" 
    rb-find="id:<?=$id??"*"?>" 
    loop
>
    <h1>Note: <?=$note->name?></h1>
    <p rb-prop="body" rb-format="markdown"></p>
</div>
<hr rb-with="note">
```