<?php

require_once(dirname(__DIR__,3).'/autoload.php');

class CompoTester extends \Taeluf\Tester {


    protected $data = [
                    '1'=>[
                        'id'=>1,
                        'name'=> 'first note',
                        'body'=> 'the first paragraph of a note is always the best paragraph.'
                    ],
                    '2'=>[
                        'id'=>2,
                        'name'=> 'second note',
                        'body'=> 'the second note with a paragraph is the worst note.'
                    ]
                ];
    
    public function prepare(){
        require_once(__DIR__.'/Note/Note.php');
    }
    
    public function testFileExists(){
        $compo = new \Li\Note();
        $view = $compo->view();
        if (file_exists($view->file)){
            return true;
        } 
        echo "File '{$view->file}' does not exist.";
        return false;
    }
    public function testCompotoViewtoForm(){
        $compo = new \Li\Note();
        $view = $compo->view();
        $form = $view->form();
        $formFile = realpath($form->file);
        $testFile = realpath(__DIR__.'/Note/view/Form.php');
        if (is_file($testFile)&&$testFile===$formFile
            &&$form instanceof \Fresh\Form)return true;
        echo "File: '".$form->file."' does not match '{$testFile}'... or the file does not exist";
        return false;
    }
    public function testFormViewNamesMatch(){
        $compo = new \Li\Note();
        $view = $compo->view();
        $form = $view->form();
        if ($form->name==$view->name)return true;
        echo "Form '{$form->name}' should have same name as view '{$view->name}'";
        return false;
    }
    public function testDisplayView(){
        // throw new \Exception("I think I need to fix all of my handlers. I don't think they're being set correctly. The compiling, specifically, is borken.");
        $note = new \Li\Note();
        $view = $note->view(null,['id'=>1]);
        $view->setRuntimeHandler('find',
            function($tableName, $lookupStr) use ($note){
                $lookup = $note->parseLookupStr($lookupStr);
                // print_r($lookup);
                $notes = $this->data;
                if (isset($notes[$lookup->id??null])){
                    return [(object)$notes[$lookup->id]];
                }
                $notes = array_map(function($entry){return (object)$entry;},$notes);
                if ($lookup->id??null=='*')return $notes;
                return [];
            }
        );
        $output = $view.'';
        echo $output;
        if ($pos = strpos($output,'<h1>Note: first note</h1>')>0){
            if (strrpos($output,'<h1>Note: first note</h1>')!==$pos
                &&strpos($output,'<h1>Note: second note</h1>',$pos+3)>0){
                return true;
            }
        }
        return false;
    }
    public function testDisplayEditForm(){
        $note = new \Li\Note();
        $form = $note->form(null,['id'=>1,'submit_url'=>'#these-tests-dont-use-a-webserver']);
        $form->setRuntimeHandler('find',
            function($tableName, $lookupStr) use ($note){
                $lookup = $note->parseLookupStr($lookupStr);
                // print_r($lookup);
                $notes = $this->data;
                if (isset($notes[$lookup->id??null])){
                    return [(object)$notes[$lookup->id]];
                }
                $notes = array_map(function($entry){return (object)$entry;},$notes);
                if ($lookup->id??null=='*')return $notes;
                return [];
            }
        );
        $output = $form.'';
        echo $output;
        if (strpos($output,'<form action="#these-tests-dont-use-a-webserver" method="POST">')!==false
            &&strpos($output,'<input type="text" name="name" value="first note">')!==false
            &&strpos($output,'<input name="id" value="1" type="hidden"><input name="fresh_table" value="note" type="hidden">')!==false){
            return true;
        }
        return false;
    }
    public function testCompile(){
        throw new \Exception("Compile test is not yet written\n");
    }
    public function testPassthru(){
        throw new \Exception("passthru test not yet written");
    }
    public function testDisplayCreateForm(){
        /*
        * So... I think... I think I can modify my find function to accept a null 'id:new' as the lookup string, dispense an empty object, and display with that.
        * This is just a jank way to handle it, dispensing an empty object...
        */
        $note = new \Li\Note();
        $form = $note->form(null,['id'=>'new','submit_url'=>'#these-tests-dont-use-a-webserver']);
        $form->setRuntimeHandler('find',
            function($tableName, $lookupStr) use ($note){
                $lookup = $note->parseLookupStr($lookupStr);
                if ($lookup->id=='new'){
                    $obj = (object)['name'=>'','body'=>'','id'=>''];
                    return [$obj];
                }
                // print_r($lookup);
                $notes = $this->data;
                if (isset($notes[$lookup->id??null])){
                    return [(object)$notes[$lookup->id]];
                }
                $notes = array_map(function($entry){return (object)$entry;},$notes);
                if ($lookup->id??null=='*')return $notes;
                return [];
            }
        );
        $output = $form.'';
        echo $output;
        if (strpos($output,'<form action="#these-tests-dont-use-a-webserver" method="POST">')!==false
            &&strpos($output,'<input type="text" name="name" value="">')!==false
            &&strpos($output,'<input name="id" value="" type="hidden"><input name="fresh_table" value="note" type="hidden">')!==false){
            return true;
        }
        return false;
    }
    public function testSubmitNew(){
        $compo = new \Li\Note();
        $postData = [
            'name'=>'The third, submitted note',
            'body'=>'I am the body of the third note, and you know you love me!',
            "fresh_table"=>"note"
        ];
        $succeeded = false;
        $compo->setSubmitHandler(
            function (string $tableName, array $dataToSave, $itemId) use (&$succeeded,$postData) {
                $cleanData['name'] = [$postData['name']];
                $cleanData['body'] = [$postData['body']];
                if ($tableName==='note'
                    &&$dataToSave===$cleanData
                    &&$itemId===null){
                        $succeeded = true;
                    } else {
                        var_dump($tableName,$dataToSave,$itemId,$cleanData);
                    }
            }
        );
        $compo->submit(null,null,$postData);
        return $succeeded;
    }
    public function testAddRuntimeHandlerPropagation(){
        $compo = new \Li\Note();
        $compo->addRuntimeHandler('testPropagation',
            function($input){
                return '_'.$input;
            }
        );
        $compo->addRuntimeHandler('testPropagation',
            function($input){
                return '---'.$input;
            }
        );

        $view = $compo->view();
        $form = $compo->form();
        
        $vTest = $view->callHandler('testPropagation',"abc");
        if ($vTest[0]!=='_abc'
            ||$vTest[1]!=='---abc'){
            return false;
        }
        $fTest = $view->callHandler('testPropagation','bdc');
        if ($fTest[0]!=='_bdc'
            ||$fTest[1]!=='---bdc')return false;

        return true;

    }
    public function testSetRuntimeHandlerPropagation(){
        $compo = new \Li\Note();
        $compo->setRuntimeHandler('testPropagation',
            function($input){
                return '_'.$input;
            }
        );

        $view = $compo->view();
        $form = $compo->form();
        $vTest = $view->callHandler('testPropagation',"abc");
        if ($vTest!=='_abc'){
            return false;
        }
        $fTest = $view->callHandler('testPropagation','bdc');
        if ($fTest!=='_bdc')return false;

        return true;

    }
    public function testSubmitEdit(){
        $compo = new \Li\Note();
        $postData = [
            'id' => 1,
            'name'=>'An edit to note 1',
            'body'=>'This edit makes this note SO MUCh better.',
            "fresh_table"=>"note"
        ];
        $succeeded = false;
        $compo->setSubmitHandler(
            function (string $tableName, array $dataToSave, $itemId) use (&$succeeded,$postData) {
                $cleanData['name'] = [$postData['name']];
                $cleanData['body'] = [$postData['body']];
                $cleanData['id'] = [$itemId];
                if ($itemId!==$postData['id'])return;
                if ($tableName==='note'
                    &&$dataToSave===$cleanData
                    &&$itemId===$postData['id']){
                        $succeeded = true;
                    } else {
                        var_dump($tableName,$dataToSave,$itemId,$cleanData);
                    }
            }
        );
        $res = $compo->submit(null,null,$postData);

        return $succeeded;
    }
    public function testCompoDoesNotDuplicateViews(){
        $compo = new \Li\Note();
        $view1 = $compo->view();
        $v2 = $compo->view();
        $v2->addCompilehandler('the-function-name',function(){echo "cats are fun and cute. Dogs are cool too!";});
        if ($view1===$v2)return true;
        return false;
    }
    public function testDiffComposHaveDiffViews(){
        $compo1 = new \Li\Note();
        $view1 = $compo1->view();
        $compo2 = new \Li\Note();
        $v2 = $compo2->view();
        $v2->addCompilehandler('the-function-name',function(){echo "cats are fun and cute. Dogs are cool too!";});
        if ($view1===$v2)return false;
        return true;
    }
    public function testGetViewSource(){
        $note = new \Li\Note();
        $view = $note->view();
        $file = $view->file;
        $src = $view->src;
        $fileSrc = file_get_contents($file);
        if ($src===$fileSrc){
            return true;
        }
        return false;
    }
    public function testReadonlySrc(){
        $note = new \Li\Note();
        $view = $note->view();
        $file = $view->file;
        try {
            $view->src = "good";
        } catch (\Error $e){
            echo $e;
            return true;
        }
        return false;
    }

    public function testNothing(){
        return true;
    }
}

CompoTester::runAll();
