<?php

ini_set('display_errors',1);
error_log(E_ALL);

require(__DIR__.'/vendor/autoload.php'); // If you're using composer
// require(__DIR__.'/vendor/Liaison/autoload.php'); // If you're using git clone. This autoload is not yet functional (so use composer)

$liaison = new \Liaison();
$myApp = new \Lia\Package($liaison, __DIR__.'/MySite/');
$liaison->set('lia.resource.useCache',false);
$liaison->set('lia.cacheDir',$myApp->dir('cache'));

$liaison->deliver();