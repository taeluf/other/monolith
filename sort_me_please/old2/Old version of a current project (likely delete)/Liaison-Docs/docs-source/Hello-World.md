# Liaison Getting Started
We're going to build a simple Liaison application from scratch.

## 1. Install
Download Liaison one of these ways:
- `git clone https://gitlab.com/taeluf/Liaison.git`
	- Make a 'vendor' folder in your project directory, then run `git clone` inside `vendor`
- `composer require taeluf/liaison dev-valpha`
	- You MUST add `"minimum-stability": "dev"` to your `composer.json`
- Edit `composer.json` to include `"require":{"taeluf/liaison": "dev-valpha"}`, then run `composer install`

## 2. Setup your environment

### 2a. Setup Apache
Setup a [virtual host](Tips/virtual-host.md) so you can view this sample website in your browser.
Alternatively, you can execute `php deliver.php` on the command line.

### 2b. .htaccess file
You may already have a .htaccess file delivering to a single php script. If so, you can continue to 2b.
- Create a `.htaccess` file at the root of your site with the following:
```.htaccess
DirectoryIndex /deliver.php

RewriteEngine On

# Prevent child directories from being accessed directly
RewriteOptions InheritDownBefore

# Direct literally every request to /deliver.php
RewriteCond %{REQUEST_URI} !^/deliver.php$
RewriteRule (.+) /deliver.php [L,QSA]
```
See our [`.htaccess` tips](Tips/htaccess.md) for forced `www.` and forced `https`

### 2c. Scaffold your project
1. Create a dir `MySite`
2. Create a dir `MySite/cache`
3. Create a dir `MySite/public`

### 2d. Load Liason
We will autoload Liaison and add a Package (app), which we'll be setting up in the next steps.
- Create `deliver.php` file in the root of your site:
```php
<?php
//for debugging sake
ini_set('display_errors',1);
error_log(E_ALL);

require(__DIR__.'/vendor/autoload.php'); // If you're using composer
// require(__DIR__.'/vendor/Liaison/autoload.php'); // If you're using git clone. This autoload is not yet functional (so use composer)

$liaison = new \Liaison();
$myApp = new \Lia\Package($liaison, __DIR__.'/MySite/');
//Force resource files to be recreated on every request
$liaison->set('lia.resource.useCache',false);
//Set the cache dir to your own
$liaison->set('lia.cacheDir',$myApp->dir('cache'));

$liaison->deliver();
```
Right now, Liaison should give you an exception, saying there are no routes found. In step 3, we'll add content.

## 3. Add a little Content
For more, see [all about Routing](Routing.md)
### 3a. Make Files
In your `MySite/public` dir, create the following files:
- `index.php`: Just code in a `<h1>Index Page</h1>`
- `{cause}.php`: No code, yet. & Yes, you DO need the curly braces in the file name.
Now refresh the home page of your site.  OH WOW, it's a page. Whoop... We're getting to the cooler stuff!

### 3b. Write a little code
In `index.php`, after the `<h1>`, write:
```php
<?php

$keys = array_keys(get_defined_vars());
print_r($keys);
```
Now you can see all the variables available. (a nice trick to have in your belt)  
We'll do a little more later.

In `{cause}.php`, write:  
```php
<h1>Cause name: <?=$cause?></h1>
```
1. Visit `site.localhost/stop-global-warming/` You see the cause (or the slug, anyway)
2. Visit `/treatment-over-prison/`: You see the cause
3. Visit `/black-lives-matter/end-over-policing/`: You don't get a page, because the dynamic paramater does not allow slashes

## 4. Create a View
We're creating a simple view with a button that refreshes the page using javascript. For more, see [all about Views](Views.md).

### 4a. The Base View
Create the file `MySite/view/button/refresh.php`, and write:
```html
<button class="refresh">Refresh Page</button>
```

### 4b. Include the view on your pages
In both `index.php` AND `{cause}.php`, append the following:
```php
$view = $lia->view('button/refresh');
echo $view; //$view is an object, but can be echod
```
Refresh both pages, and you'll see the button, though it doesn't do anything yet.

### 4c. Style it
Create the file `MySite/view/button/refresh.css`, and write:
```css
button.refresh {
	padding:8px;
	background:orange;
	border-radius: 8px 50%;
	cursor:pointer;
}
```
Refresh & you'll see the styled button.

### 4d. Script it!
Create the file `MySite/view/button/refresh.js`, and write:
```js
//Since the script loads in before the html element, we use a timeout. Nasty, but should work.
setTimeout(
	function(){
		document.querySelector('button.refresh').addEventListener('click',
		    function(event){
		        location.reload();
		    }
		)		
	},250
);
```

### 4e. Pass variables to the view
1. Edit the `$lia->view('button/refresh')` call in `index.php` to: `$lia->view('button/refresh', ['name'=>'SomeName'])`
2. Edit `refresh.php` to use the provided name. Change `<button class="refresh">Refresh Page</button>` to `<button class="refresh">Refresh <?=$name?></button>`
Refresh your home page & the name should be displayed. Try passing the cause from `{cause}.php` to `button/refresh.php` in this same way.

## Additional Resources
- My [php environment package](http://tluf.me/php-environment/) helps with runtime changes between localhost, staging server, and production server
