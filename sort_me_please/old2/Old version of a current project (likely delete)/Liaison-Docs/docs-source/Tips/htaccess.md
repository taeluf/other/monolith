# .htaccess
For a full `.htaccess` file, try the following:
```
DirectoryIndex /deliver.php

RewriteEngine On

# Prevent child directories from being accessed directly (in case they have their own .htaccess files)
RewriteOptions InheritDownBefore

# Force HTTPS (unless on localhost)
RewriteCond %{HTTP_HOST} !=localhost
RewriteCond %{HTTP_HOST} !=127.0.0.1
RewriteCond %{REMOTE_ADDR} !=127.0.0.1
RewriteCond %{REMOTE_ADDR} !=::1
RewriteCond %{HTTPS} off
RewriteRule .* https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]


# Force www (unless on localhost)
RewriteCond %{HTTP_HOST} !=localhost
RewriteCond %{HTTP_HOST} !=127.0.0.1
RewriteCond %{REMOTE_ADDR} !=127.0.0.1
RewriteCond %{REMOTE_ADDR} !=::1
RewriteCond %{HTTP_HOST} !^www\. [NC]
RewriteRule .* https://www.%{HTTP_HOST}%{REQUEST_URI} [L,R=301]


# Direct literally every request to /deliver.php
RewriteCond %{REQUEST_URI} !^/deliver.php$
RewriteRule (.+) /deliver.php [L,QSA]
```