# Autoloading

## class Directory
Put class files in the `class` directory of your package in a [`PSR-4`](https://www.php-fig.org/psr/psr-4/) style. So `\Taeluf\FunnyStrings` should be at `class/Taeluf/FunnyStrings.php`

## Add directories to scan
Call `$lia->autoload($directory, $baseNamespaces=[])`. This adds the directory to be searched in a `PSR-4` style. If you want to do away with a directory, pass namespaces. Like: `$lia->autoload($dir, ['Taeluf'])`, then you could place class `Taeluf\FunnyStrings` directly  in `$dir/FunnyStrings.php` & it will be loaded.

If you're developing an app, use the api. Instead of `$lia->autoload(...)`, call `$lia->callApi('lia.autoload', $directory, $namespaces=[])`; If you're developing a website, `$lia->autoload()` is recommended.

## Add a custom autoloader (functional)
Call `$lia->addAutoloader($callback)` where `$callback` takes a single param `$fullyQualifiedClassName`. For consistent apis use `$lia->api('lia.autoload', 'add', $callback)` instead. Of course, you could just use `spl_autoload_register`.

## Add a custom autoloader (declarative)
In one of your components, prefix a function with `autoload` (or `autoload_`). Something like:
```php
class MyCompo extends \Lia\Compo {

    public function autoloadMyOwnWildClasses($className){ 
        //you should check if the file exists, as autoloaders aren't supposed to output errors
        $this->myWildMapToFiles[$className];
    }
}
```
See [prefixing](More/Prefixing.md) for additional info.

## TODO
We hope to add:
- caching of classes
- classmap-style autoloading, so you would just have to have the file in the given directory, without concern for it's namespace or directory structure
