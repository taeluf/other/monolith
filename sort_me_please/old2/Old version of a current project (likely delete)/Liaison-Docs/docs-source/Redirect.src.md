# Redirecting to a new url

## Standard PHP Fashion
You might do this in a `view` or `public` file, where `$package` is available
```php
$url = $package->url('/register/'); // pre-pends the url prefix for the package
header("Location: {$url}", $statusCode=302);/*302 temporary is the default*/ 
exit; // Not always necessary, but exiting stops silly errors from preventing the redirect
```

## Built-in Methods
Any one of these will perform a redirect.
- `$lia->goto($url, $statusCode=302);` - Standard redirect to the url
- `$lia->gotoWithMessage($url, "You don't have access to this page", $statusCode=302);`
	- Redirect to the url with the given message.
- `$lia->gotoWithContent($url, $statusCode=302);`
	- Redirect to the url with the content of the current request.

### gotoWithContent(...) example
For url `/user/logout/`, we're in `public/logout.php`:
```php
<?php
$url = $package->url('/'); // returns '/user/'
$lia->gotoWithContent($url); //Default 302 temporary is good. 301 permanent would cause the browser to do an immediate redirect in the future, instead of running the request to /user/logout/, and that would be bad.
// remove cookie from user's browser
// delete cookie entry in the database
if ($logoutFailed){
    echo "Something went wrong when logging you out. You may need to clear cookies in your browser if this continues.";
    return;
} 
echo "Logged out successfully! Please come back soon :)"
``` 

Which will land us on `public/index.php`:
```php
<div class="goto-message">
    <?=$lia->getGotoMessage()?>
</div>
<?php
//some php logic 
if ($userIsLoggedIn) echo $this->view("user/profile",['user'=>$user]);
else echo $this->view('user/login');
```
