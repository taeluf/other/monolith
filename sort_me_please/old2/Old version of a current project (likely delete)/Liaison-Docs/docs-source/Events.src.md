# Events

## Component based event handling
Declare methods in your components to handle events by prefixing the event name with `onEmit_` & using unserscores (`_`) instead of periods (`.`). Example:
```php
class RequestLogger extends \Lia\Compo {
	public functon onEmit_Server_responseSent($responseOutput){
	    file_put_contents(__DIR__.'/log.txt', 'Response sent at '.microtime(true), FILE_APPEND|LOCK_EX);
	}
}
```
This function would be called for `Server.responseSent` event.


## Create/Emit an event
Just call `$lia->emit('Namespace.nameOfEvent', $param1, $param2, ...$etc)`

## Functional event handling
Call `$lia->schedule('Namespace.nameOfEvent', $callable)`. Then `$callable` will be invoked whenever `Namespace.nameOfEvent` is emitted.

## All Available Events
This may not be an exhaustive list & may not be kept up to date. You can search the liaison code base for `->emit(` to find current event names. You can use `func_get_args()` to inspect the arguments passed to your event handler.

### Server Events
- `Server.willDisplayContent` sends `(\Lia\Obj\Response $response)`, which has two paramaters `string $response->content` and `bool $response->useTheme`. You can modify either of these
- `Server.responseSent` sends with no paramaters after all output has been sent to browser. Also see config `lia.Server.bufferResponse`