# Old notes
    /**
     * Cache files can be used for whatever you like and are stored by key without file extensions, unless you give one to them. Namespaces are recommended.
     * Two files are created when you cache a file: `file-ns.thekey` and `meta-ns.thekey`. The `meta-` file stores the expiry and may hold more information in the future. The `file-` stores the content.
     * @export(Internals.Cache)
     */



    /**
     * There is both a public and private cache dir. Both of which are in Liaison's directory by default.
     * - The cache is used by the Resources class to handle your compiled css and js files. 
     * 
     * To change the cachedir do:
     * ```php
     * $lia->set('lia.cacheDir', $yourPackage->dir('cache'));
     * ```
     * - You MAY use an explicit path instead of using your `$package->dir()` lookup
     * - `lia.cacheDir` may be anywhere you like.
     * 
     * @export(Usage.Cache.dir)
     * 
     */


    /** 
     * 
     * Call `$lia->cacheFile($key, $content, $maxAgeInSeconds)` to store a file in the cache.
     *    - You may leave off `$maxAgeInSeconds` to use the default, which is five days
     *    - Cache files cannot be loaded after they expire and will be automatically cleaned up at some point.  
     * 
     * Call `$lia->getCacheFile($key)` to get the contetns of a cached file
     *    - returns `false` if the cache file does not exist or has expired
     *    - Files are returned as a string. JSON is not decoded. PHP files are not processed.
     * 
     * @export(Usage.Cache)
     */
