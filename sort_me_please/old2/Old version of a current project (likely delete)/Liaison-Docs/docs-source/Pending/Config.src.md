# Config docs needing cleanup
/**
 * - `set($key, $val)` will override any previously set `$key`
 * - `default($key, $val)` will never override `set()` or previous `default()` calls
 * - `get($key)` will throw an exception if the `$key` has not been set
 * 
 * @export(Usage.Config.notes)
 */


## Todos
These were supposed to be TODOs... but I think they're better as guidance for writing comprehensive docs.
/**
 * - config file loading... set() should generally be able to override configs from the file
 * - Do config file contents automatically get loaded into Liaison? Maybe, if they're namespaced, at least?
 * - Should I limit access to set() & get(). NOT EARLY ON
 *     - maybe in the future? I'm concerned about performance, the time to implement, and how much it's actually needed (assuming you only run code you trust)
 * - What happens if a config value is overridden? Error? Let it happen? Fail silently? Log?
 * - add conflict resolution for duplicate config keys
 * - consider a non-exception approach to a not-set config key
 * 
 * @export(TODO.Config)
 */
