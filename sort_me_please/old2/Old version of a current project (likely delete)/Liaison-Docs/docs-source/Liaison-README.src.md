# Liaison
Build full GUI framework/cms-agnostic PHP applications. Many apps will require zero configuration and zero routing code. And build full websites.

## Status: Unstable, Nearing stability, Docs are bad
I don't... think I'll be breaking anything for awhile. But it could happen! The docs for Liaison are pretty bad right now. I'm working on [completely new documentation](https://gitlab.com/taeluf/Liaison-Docs) in a different repo. I don't think most the docs in this repo are accurate any more...


## Overview
A minimal project only needs a `public` dir. If it's a website (instead of an App), you'll need a `.htaccess` and a `deliver.php` to run Liaison.

Some of these details will change as development continues, but the overall idea will remain consistent.

### Sample Application
If this were truly a "blog" app, most of the public files would not be there, but I wanted to show the scope.
```
codeDir/
    - config.json - Usually not needed
    - class/ - General classes which get autoloaded
        - BlogPost.php 
    - core/ - Liaison components
        - Blog.php - Handles creating / editing of blog posts and sitemap generation. 
    - public/ - 
        - index.php - delivers at `/`
        - about.md - delivers at `/about/` (requires CommonMark)
        - image.png - delivers at `/image.png` (directories are cool, too)
        - contact-us.php - `/contact-us/`, has a <form>
        - @POST.contact-us.php - `/contact-us/` accepts the <form> submission
        - blog/
            - {slug}.php - `/blog/the-slug/` script to show your blog posts for the given slug 
    - view/
        - blog.php - `echo $lia->view('blog');`
        - blog.css - auto-included when 'blog' view is displayed
        - blog.js - same as the .css
        - blog/ - css & js files will also be included automatically
            - extra.css
            - extra.js
```

#### core/Blog.php
```
class Blog extends \Lia\Compo {

    // You can have multiple sitemapWhateverName(){} functions
    // static `public/` files will be auto-included in your sitemap
    public function sitemapBlogPosts(){
        //... Haven't figured out this feature yet
    }
    public function routePatternBlogSubmit(){
        // More likely, your blog posts would use a function like this and submission would use a public file.
        // so I'll update this later...
    }
}
```

#### public/blog/{slug.php}
```php
$blogPost = BlogPost::getPost($slug);
$lia->seoTitle($blogPost->title);
$lia->seoImage($blogPost->image);
echo $lia->view('blog',['post'=>$blogPost]);
```

#### view/blog.php
```php
    <h1><?=$post->title?></h1>
    <div><?=$post->bodyAsHtml()?></div>
```

### Executing Liaison on your site
```php
$lia = new \Liaison();
$package = new \Lia\Package($lia, __DIR__.'/code/'); //or wherever your Liaison project is
$lia->deliver();
```


