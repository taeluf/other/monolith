# Configurations


## Package Configurations
### Set package level configurations
- `$lia->set('packageName.cofig.key', 'value');`
- `$package->set('config.key', 'value');`
- `new \Lia\Package($liaison, $dir, $configs=['config.key'=>'value', 'another.key'=>'value2']);`
	- Or use sub-arrays instead of dot-notation: `$configs=['config'=>['key'=>'value']];`
- In the `config.json` file at the root of your package, containing `{"config.key":"value"}` OR nested keys: `{"config":{"key":"value"}}`
	- Do both nested keys & dot keys work? (pretty sure)

### Available package level configurations
- `dir.public.baseUrl` set to an absolute url path with trailing & leading slashes, like `/user/` or `/blog/`. Defaults to `/`. Your `public/index.php` file will be delivered at the base url.
- 


## Global Configurations
Set global configs with `$lia->set('the.setting.name', 'the value');`. 

### Recommended
- `lia.cacheDir` - absolute path to the directory to use as your cache. Recommended usage: `$lia->set('lia.cacheDir', $mainPackage->dir('cache'));`
- `lia.resource.useCache` - set true or false. `true` is default. Cache files are created regardless, but if set `false`, cache files will not be used after initial generation.

### Resources
- `lia.resource.forceRecompile` - true or false (default). `true` forces recompilation of resource files on every request. I don't remember the rules for recompilation otherwise.
- `lia.resource.useCache` - set true or false. `true` is default. Cache files are created regardless, but if set `false`, cache files will not be used after initial generation.


### Cache
- `lia.Cache.clearAfterXMinutes`: Maximum frequency that stale cache files should be checked for deletion. Default is `60*24*5` (5 days)
	- You can also set it to `0.5` to set the frequency to 30 seconds.
	- Stale cache files are only cleared at the end of a request, so if you set your cache to clear every 30 seconds, but you only have a request every 15 minutes, your cache files will only be checked for staleness every 15 minutes.
	- The maximum time allowed is 180 days
- `lia.cacheDir`: The directory

### Server
- `lia.Server.bufferResponse` true or false: If true (default), sends all output buffers before emitting the `Server.responseSent` event. This might not work without Apache configurations

