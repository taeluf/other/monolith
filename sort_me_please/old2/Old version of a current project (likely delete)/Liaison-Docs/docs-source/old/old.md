# About Views
I removed these docs from the `Object/View.php` file, cause that's... really not where they belong...
```
/**
 * Views are defined in your package and can follow one of two different structures
 * ```
 * public/ ... public files
 * view/
 *     - Blog.php
 *     - Blog.js
 *     - Blog.css
 *     - Theme.php
 *     - Theme/
 *         - main.js
 *         - main.css
 *         - extra/ ... contains a bunch more css & js files
 * ```
 * - Calling `$view = $lia->view('Theme', $args)` will encompass `Theme.php` and load every single `.js` and `.css` file in the `Theme` directory
 * - Calling `$view = $lia->view('Blog', $args)` will encompass `Blog.php` for the view and load `Blog.js` and `Blog.css`
 *     - I think I did this!!! @TODO implement these sibling-resource files 
 * - `$args` are `extract`ed so your `Theme.php` & `Blog.php` files receive the array keys as paramaters.
 * - Resource files are loaded when you call `$view->resources()`, `$content = $view->content()`, or `$content = "".$view;` (tostring)
 * - Place your view in a subdirectory like `view/Theme/Light.php` for a view named `Theme/Light`
 * 
 * @export(Usage.View.Packaged)
 */
```

# Fancy Closures
- I've also kept this documentation in the fancy closure class, just as it is here:

 * Fancy closures enable binding of paramaters to a callable and inspection of callables, whether as `['\\StaticClass','functionName']`, `[$object, 'methodName']`, or `$anonymousFunction = function(){}`.
 * Example:  
 * ```php
 * $cat = new \Funny\Cat();
 * $loudness = 7;
 * $closure = new \Lia\Utility\FancyClosure([$cat, 'purr'], [$loudness]);
 * $closure->funcName === 'purr';
 * $closure->object === $cat;
 * $closure->class === 'Funny\\Cat'; //it prints with a single slash, but backslashes are generally better off being escaped in my experience
 * $closure->isStatic === false; // would be true if using ['Funny\Cat', 'purr'] instead of [$cat, 'purr']
 * $closure->isAnonymous === false; // would be true for function(){echo 'Kindness can be anonymous too.';}
 * $closure->function === null; // would be the anonymous function, if you had passed a function instead of an `[$obj, 'funcName']` callable
 * 
 * $closure->origCallable === [$cat, 'purr']; //just the first arg you pass to the constructor
 * $closure->bound === [$loudness]; // and this is the 2nd arg
 * ```
 * 
 * @export(Utility.FancyClosure)
 */


# Liaison
- I've removed these docs from code/class/Liaison.php

/**
 * # Liaison
 * Liaison ties everything together. Packages & their components, view lookups, config values, routing, events, and more??
 * 
 * ## Basic Usage
 * ```php
 * $lia = new \Liaison();
 * $lia->addPackage(
 *     ['dir'=>__DIR__.'/my-package/',
 *      'name'=>'MyPackage'
 *      ]
 * );
 * $lia->deliver();
 * ```
 * This will load files from your package and route appropriately.
 * 
 * ### $lia, Methods to know
 * - deliver($url=null)
 *     - uses the requested url by default, but you can pass in a url, which may include a get string like `?cats=cute`
 * - addPackage($options): $options is an array
 *     - dir: The root directory of your package
 *     - name: The name to be used for retrieving your package. 
 *         -Each package must have a unique name
 * - package($name): retrieve a package by name
 * - packages(): A list of packages
 * - compo($name): For each package, get any components named `$name`. 
 *     - 0 components found, return null, 1 component found, return the component, 2+ components found, throw an exception
 * - set($key, $value) & get($key,$valueIfNotFound): Store & retrieve values across the $lia instance
 * - view($name, $passthru): Get a view (from any package) by $name, passing $passthru to the view.
 *     - See [Views](0-docs/Views.md) for more information
 * - register($eventName, $object, $method=null): Register the event to be executed
 * - send($eventName,$arg1, $arg2, $arg3, ....): Execute the given event, passing the list of args along.
 *     - See [Events](0-docs/Events.md)
 *      
 * @export(old.About.Liaison)
 */



# Package.php
- I've left these in the code/class/Package.php

/**
 * Files in the `public` dir of your package will be automatically routed to.
 * - `public/index.php` files will be delivered without the file name or extension
 * - `public/dir/file.php` files will be delivered at `/dir/file/`
 * - `public/resource.js` and other non `.php` files will be delivered at `/resource.ext`
 * 
 * There's much more to document and features to set up with routing, still. 
 * 
 * @export(Usage.Router.publicDir)
 */


# Autoloading

     * Use the built-in autoloader to load classes with the following structure:
     * ```
     * /hierarchy
     *     - Contact/
     *          - Submitter.php: namespace Contact, class Submitter
     *     - Customer/
     *          - Order.php, namespace Customer, class Order
     *          - Person.php, ns Customer, cls Person
     * /flat
     *     - Visa.php: ns Payment\Processor, cls Visa
     *     - Bitcoin.php: ns Payment\Processor, cls Bitcoin
     *     - Oauth.php: ns User\Auth, cls Oauth
     * ```
     * 
     * Load all of the above with the following:
     * ```php
     * $dir = __DIR__;
     * $lia->autoload($dir.'/hierarchy');
     * $lia->autoload($dir.'/flat',['Payment\\Processor', 'User\\Auth'])
     * ```
     * 


