# Prefixing
Prefixing allows you to add to your local `$liaison` api in declarative class-driven ways.

## Declaring Basic Prefixes 
We'll use [`autoload`](../Autoload.md) as the example prefix. The `autoload` prefix points to the `lia.autoload` api & its `prefix` handler.

Writing a prefix:
- Prefixes must be followed by an uppercase letter: `autoloadBlogClasses` works. `autoloadblogClasses` does NOT
- OR an underscore followed by a letter: `autoload_BlogClasses` AND `autoload_blogClasses` will BOTH work

About Prefixes:
- Every prefix MUST point to an api key + handler. 
- Prefixes are set globally on the `Liaison` object.
- New prefixes can be added or removed GLOBALLY with `$liaison->addApiPrefix('theprefix', 'api.key', $handler='prefix')`.
- A prefix CAN (but usually should not) point to a different handler
- A prefix handler receives `$cleanName, $callable` where `$cleanName` is everything after the prefix. This WILL include the underscore (`_`) if present.

Modifying Global Prefixes: 
- Remove: `$lia->removePrefix('autoload')` will stop the `autoload` prefix from working. The API will be unchanged.
- Add: `$lia->addApiPrefix('al', 'lia.autoload')`. 
- Use a named handler (instead of the default `prefix`): `$lia->addApiPrefix('al', 'lia.autoload', $handler='locations')`. Note there is no `locations` handler for the `lia.autoload` api. But one could theoretically be added with different functionality from the built-in autoload prefix handler.

Adding a Prefix Handler
- Quick & Dirty: `$lia->addPrefix('al', function($cleanName, $callable){})`. That function will process any methods using the `al` prefix. This is good for a single website, but not so good if you're creating a proper API for others to use.
- With proper Api: `$lia->addApi('lia.autoload', function($cleanName,$callable){}, 'prefix')`. Then create the prefix: `$lia->addPrefix('autoload', 'lia.autoload');`

###Declarative Component Approach: 
```php
namespace Lia\Compo;
class Autoloader extends \Lia\Compo
    public function apiPrefix_lia_autoload_autoload_addAutoloaderFromPrefix($cleanName, $callback) {
        return $this->lia->api('lia.autoload', 'add', $callback);
    }
}
```
Let's break down this wildly long function name: `apiPrefix_lia_autoload_autoload_addAutoloaderFromPrefix`
- `api` is the prefix of this function, which pipes it to the `prefix` handler of the api named `lia.api`.
- `Prefix` (from the left) informs the `prefix` handler of the api `lia.api` that we are creating a new prefix
- `addAutoloaderFromPrefix` is the global method name, so `$lia->addAutoloaderFromPrefix(...)` exists now
- `_autoload_` (farthest right) declares the global prefix `autoload`
- `lia_autoload` (the remaining parts) declares that the api key is `lia.autoload`

## Changing prefixes just for your component
Override `getApiPrefixes()` of the `Scanner` trait. Basically, call `$this->Scanner_getApiPrefixes()` to get the global prefixes, then modify them as you please.
```php
class CustomComponent extends \Lia\Compo {
    public function getApiPrefixes(){
        $globalPrefixes = $this->Scanner_getApiPrefixes();
        $autoload = $globalPrefixes['autoload'];
        // $autoload == ['lia.autoload', 'prefix']
        unset($globalPrefixes['autoload']);
        $globalPrefixes['al'] = $autoload;
        return $globalPrefixes;
    }
}
```
This example removes the `autoload` prefix and adds an `al` prefix, pointing to the same api+handler. You could leave `autoload` set & just ADD the `al` prefix. This also only affects the component in which `getApiPrefixes()` is declared. 

If you want to change prefixes for all components in your app, then create a base component extending from `Lia\Compo` that customizes prefixes. Then each of your components can extend from that base component. That base component should be in your `class` dir, unless it should do additional component-y things.


## Note for App & Extension developers
App & extension developers should always expose their api with a key, using the declarative component approach or the `$lia->addApi(...); $lia->addApiPrefix()` approach. Doing so means a website developer can customize their experience by changing what liaison methods ponit to which api. But other applications can still call your api by its unchanging api key & handler name.
