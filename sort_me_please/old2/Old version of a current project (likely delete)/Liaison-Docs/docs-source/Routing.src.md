# Routing

These "docs" come from the Router.php file. I have left his block there as well. Need to clean it up, of course.
    * ```
    * rules:
    *     .php will generally be removed & replaced with a trailing slash, but that is NOT part of parsePattern()
    *     That will be a pattern-normalization step that happens prior to parsePattern() and is extensible/configurable
    * 
    *     Methods: @POST, @GET, @PUT, @DELETE, @OPTIONS, @TRACE, @HEAD, @CONNECT
    *         - We do not currently check the name of the method, just @ABCDEF for length 3-7
    *         - These must appear after a `/` or after another '@METHOD.' or they will be taken literally
    *         - lower case is not valid
    *         - Each method MUST be followed by a period (.)
    *         - example: /@POST.dir/sub/@GET.file/ is valid for both POST /dir/sub/file/ and GET /dir/sub/file 
    * 
    *   Paramaters:
    *       - {under_scoreCamel} specifies a named, dynamic paramater
    *       - {param} must be surrounded by path delimiters (/) OR periods (.) which will be literal characters in the url
    *       - {param} MAY be at the end of a pattern with no trailing delimiter
    *       - {paramName:regex} would specify a dynamic portion of a url that MUST match the given regex. 
    *           - Not currently implemented
    *       - {?param} would specify a paramater that is optional
    *           - Not currently implemented
    *     examples: 
    *         /blog/{category}/{post} is valid for url /blog/black-lives/matter
    *         /blog/{category}.{post}/ is valid for url /blog/environment.zero-waste/
    *         /blog/{category}{post}/ is valid for url /blog/{category}{post}/ and has NO dynamic paramaters
    * ```
