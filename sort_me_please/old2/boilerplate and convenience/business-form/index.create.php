<?php


$user = getUser();

if (!hasPermission($user,'create-store')){
    echo 'no permission!!!';
    return;
}

?>

<form action="/store/submit/" method="POST" enctype="multipart/form-data">
<fieldset>
<legend>New Store</legend>
<label><strong>Store Name</strong><br>
    <input name="name" type="text" placeholder="Thriftalicious">
</label><br><label><label><strong>Owner / Manager Name</strong><br>
    <input name="manager" type="text" placeholder="Pat Smith">
</label><br><strong>Phone Number</strong><br>
    <input name="phone" type="tel" placeholder="217-877-####">
</label><br><label><strong>Email Address</strong><br>
    <input name="email" type="email" placeholder="my_email@email_provider.com">
</label><label><br><strong>Official Website</strong><br><input name="website" type="url" placeholder="https://my-thriftalicious-site.com"></label>
<br><label><strong>Cover Image</strong><br>
    <input name="image" type="file" placeholder="placeholder" accept="image/*">
</label><br><label><strong>Store Description</strong><br>
    <textarea name="description" rows="10" cols="70"></textarea>
</label>
</fieldset>

<fieldset>
<legend>Address</legend>
<label><strong>Street Address</strong><br>
    <input name="street" type="text" placeholder="187 Eldorado St">
</label><br>
<label><strong>City</strong><br>
    <input name="city" type="text" placeholder="Decatur" value="Decatur">
</label><br>
<label><strong>State</strong><br>
    <input name="state" type="text" placeholder="Illinois" value="Illinois" required="" disabled="">
</label><br>
<label><strong>Zip Code</strong><br>
    <input name="zipCode" type="text" placeholder="6252#">
</label>
</fieldset>

<?=file_get_contents(__DIR__.'/../../template/hours-selector.html');?>

<br><br>

<input type="submit" value="Submit">

<input type="hidden" name="id">
</form>


<?=file_get_contents(__DIR__.'/../../template/hours-selector-blank.html');?>