<?php


$store = getStore($this->getSlug());

$view = $this->getView('/store/create/');


$filled = simpleFormFill($view,$store);

$hoursPlaceholder = '<!--hours-placeholder-->';
$htmlNeedingHours = preg_replace('/\<fieldset\>\<div class\=\"hours-selector\"\>[.\s\S]*\<\/fieldset\>/',$hoursPlaceholder,$filled);

$hours = [];

foreach ($store->hours as $weekDay => $openHours){
    foreach ($openHours as $hourSpec){
        $hours[$hourSpec][$weekDay] = $weekDay;
    }
}


$template = file_get_contents(__DIR__.'/../../template/hours-selector.html');
$templateCopy = $template.'';
$hoursHtml = '';
$index = 0;
foreach ($hours as $hourSpec => $openDays){

    $hourParts = explode('-',$hourSpec);
    
    $timeStart = $hourParts[0];
    $startParts = explode(':',$timeStart);
    $hourStart = $startParts[0];
    $minuteStart = $startParts[1];
    $ampmStart = 'AM';

    $timeEnd = $hourParts[1];
    $endParts = explode(':',$timeEnd);
    $hourEnd = $endParts[0];
    $minuteEnd = $endParts[1];
    $ampmEnd = 'AM';

    if ($hourStart>12){
        $hourStart = $hourStart-12;
        $ampmStart = 'PM';
    }

    if ($hourEnd>12){
        $hourEnd = $hourEnd-12;
        $ampmEnd = 'PM';
    }
    if (strlen($hourStart)==1)$hourStart = '0'.$hourStart;
    if (strlen($hourEnd)==1)$hourEnd = '0'.$hourEnd;

    if (strlen($minuteStart)==1)$minuteStart = '0'.$minuteStart;
    if (strlen($minuteEnd)==1)$minuteEnd = '0'.$minuteEnd;
    

    $html = $templateCopy;
    $html = str_replace('selected','',$html);



    $reg = '/(name\=\"[a-z]{3,6}day)[0-9]\"/';
    $html = preg_replace($reg,'${1}'.$index.'"',$html);


    $daysReg = str_replace('day','','('. implode('|',$openDays) .')');
    $reg = '/(name\=\"'.$daysReg.'day)[0-9]\"/';

    $html = preg_replace($reg,'${1}'.$index.'" checked',$html);


    
    $posHourStart = strpos($html,'hour_start');
    $posHourStartOption = strpos($html,'>'.$hourStart,$posHourStart);
    $html = substr_replace($html,' selected',$posHourStartOption,0);

    $posMinuteStart = strpos($html,'minute_start');
    $posMinuteStartOption = strpos($html,'>'.$minuteStart,$posMinuteStart);
    $html = substr_replace($html,' selected',$posMinuteStartOption,0);

    $posAmpmStart = strpos($html,'ampm_start');
    $posAmpmStartOption = strpos($html,'>'.$ampmStart,$posAmpmStart);
    $html = substr_replace($html,' selected',$posAmpmStartOption,0);


    $posHourEnd = strpos($html,'hour_end');
    $posHourEndOption = strpos($html,'>'.$hourEnd,$posHourEnd);
    $html = substr_replace($html,' selected',$posHourEndOption,0);

    $posMinuteEnd = strpos($html,'minute_end');
    $posMinuteEndOption = strpos($html,'>'.$minuteEnd,$posMinuteEnd);
    $html = substr_replace($html,' selected',$posMinuteEndOption,0);

    $posAmpmEnd = strpos($html,'ampm_end');
    $posAmpmEndOption = strpos($html,'>'.$ampmEnd,$posAmpmEnd);
    $html = substr_replace($html,' selected',$posAmpmEndOption,0);


    $index = $index + 1;
    $hoursHtml .= $html."\n<hr>\n";

    $templateCopy = $template.'';
}
if ($hoursHtml===''){
    $hoursHtml = $templateCopy;
}



$filled = str_replace($hoursPlaceholder,$hoursHtml,$htmlNeedingHours);
echo $filled;



?>