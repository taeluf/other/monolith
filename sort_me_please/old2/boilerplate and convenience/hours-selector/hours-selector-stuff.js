
function showNewHoursSelector(){
    const oldList = document.querySelectorAll('.hours-selector');
    const blank = document.querySelector('.blank-hours-selector');
    const selector = blank.cloneNode(true);
    selector.setAttribute('class','hours-selector');
    selector.removeAttribute('style');
    const checks = selector.querySelectorAll('input[type=checkbox]');
    const index = oldList.length;
    for (const check of checks){
        check.setAttribute('name',check.value+""+index);
    }

    oldList[0].parentNode.appendChild(selector);
    oldList[0].parentNode.insertBefore(document.createElement('hr'),selector);
}

function deleteHours(deleteButton){
    if (confirm('delete this hours specification?')){
        const selector = deleteButton.parentNode.parentNode;
        days = selector.querySelectorAll('input[type=checkbox]');
        for (const dayInput of days){
            dayInput.checked = false;
        } 
        selector.style.display = 'none';
        // deleteButton.parentNode.parentNode.parentNode.removeChild(deleteButton.parentNode.parentNode);
    }
}