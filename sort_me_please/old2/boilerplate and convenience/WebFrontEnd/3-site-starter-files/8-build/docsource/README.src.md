# Fresh Component: A PHP View Library
A preamble... though this readme applies more to a library than a website.

## Example

## Install
`composer require vendor/whatever`
```json
{
    "name":"vendor/cool",
    "require":{
        "vendor/whatever": "dev-version_1"
    }
}
```

@import(ImportantNote1)

@import(ImportantNote2)