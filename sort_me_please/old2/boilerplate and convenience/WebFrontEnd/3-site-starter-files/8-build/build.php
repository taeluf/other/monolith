<?php

require_once(dirname(dirname(__DIR__)).'/depend.php');

$libDir = dirname(__DIR__);
$docu = new \TLF\Docu($libDir, '0-doc', '8-build/docsource', '8-build/docsource/README.src.md');
$docu->document();