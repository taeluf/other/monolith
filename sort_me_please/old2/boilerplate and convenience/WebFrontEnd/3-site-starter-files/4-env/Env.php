<?php

class Env {

    protected $localStaging;
    protected $liaDebug;
    protected $env;

    public function __construct($options){
        $this->localStaging = $options['local_staging'] ?? false;
        $this->liaDebug = $options['lia_debug'] ?? false;

        if ($_SERVER['HTTP_HOST']=='www.domain.com'
                        ||$_SERVER['HTTP_HOST']=='domain.com')$env = "public";
        else if ($_SERVER['HTTP_HOST']=='staging.domain.com')$env = "staging";
        else if ($_SERVER['HTTP_HOST']=='domain.localhost') $env = "local";

        $this->env = $env;
    }

    public function setup(){
        if ($this->env=='local'&&!$this->localStaging){
            ini_set('display_errors',true);
            error_reporting(E_ALL);
            require(__DIR__.'/../../depend.php');

        }
        
        if ($this->env=='remote'||$this->env=='staging'||$this->localStaging){
            require(__DIR__.'/../vendor/autoload.php');

            if ($this->env!='local'){
                ini_set('pcre.jit','0');
                ini_set('display_errors',false);
            }
        }
    }

    public function prepareLiaison($vars){
        extract($vars);
        if ($this->env=='local'){
            if (!$this->localStaging){
                $liaison->set('js.forceRecompile',true);
                $liaison->set('css.forceRecompile',true);
                $liaison->set('FreshPackage.forceRecompile',true);
                $liaison->set('Events.debug',true);
                $liaison->set('Events.debug_ignore',
                    [
                        // 'RouteWillExecute',
                        // 'Route_Filter',
                        'Request_Completed'
                    ]
                );
            }
        } 

        $liaison->set('debug', $this->liaDebug);

        $credentials = null;
        $credFile = __DIR__.'/credentials';

        if ($this->env=='staging')$credFile .='-stage.txt';
        else if ($this->env=='public') $credFile .= '-public.txt';
        else if ($this->env=='local') $credFile .='-local.txt';


        $credentials = file_get_contents($credFile);
        $p = explode(':',$credentials);
        $host = trim($p[0]);
        $db = trim($p[1]);
        $un = trim($p[2]);
        $pw = trim($p[3]);

        \RDB::setup('mysql:host='.$host.';dbname='.$db,
            $un, $pw);
    }

    public function prepareDeliver($vars){
        extract($vars);
        \SiteNew\Autowire::setAllowEdits(false);
        if ($this->env=='local'){
            if (!$this->localStaging){
                \SiteNew\Autowire::setAllowEdits(true);
                \SiteNew\Autowire::setupAutowire($liaison->package('Site'));
                $liaison->set('Fresh.allowEdit',true);
            }
        }
    }

    public function error($vars){
        extract($vars);

        if ($this->env!='local'||$this->localStaging){
            require(__DIR__.'/../0-error/error.html');

            $url = $_SERVER['REQUEST_URI'];
            $log = "\nNew Error at ".date("y-m-d H:i:s")." \n    for url '{$url}'\n";
            $str = $e.'';
            $pieces = explode("\n",$str);
            $str = "    ".implode("\n        ",$pieces);

            $log .= $str."\n";
            file_put_contents(__DIR__.'/../0-error/log',$log,FILE_APPEND);
        }

        if ($this->env=='local'){
            throw $e;
        }
    }
}