<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <?=$event->PrintHeadResources();?>
</head>
<body>
    <div class="PageWrap">
        <header>
            <div class="HeaderWrapper">
                <div class="HeaderContent">
                    <?=$event->View('site/Header');?>
                </div>
            </div>
        </header>
        <main>
            <div class="MainWrapper">
                <div class="MainContent">
                    <?=$content;?>
                </div>
            </div>
        </main>
        <footer>
            <div class="FooterWrapper">
                <div class="FooterContent">
                    <?=$event->View('site/Footer');?>
                </div>
            </div>
        </footer>
    </div>
</body>

</html>