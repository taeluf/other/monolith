const { app, BrowserWindow, screen } = require('electron');
const path = require('path');

const MAIN_HTML = path.join('file://', __dirname, 'index.html');
const CHILD_PADDING = 50;

const onAppReady = function () {
  const { width, height } = screen.getPrimaryDisplay().workAreaSize
  let right = new BrowserWindow({

    width: 300,
    height: height,
    transparent: true,
    frame: false,
    alwaysOnTop:true
  });

  right.once('close', () => {

    parent = null;
  });

  right.loadURL(MAIN_HTML);
  right.setPosition(width-300,0);
};

//~ app.on('ready', onAppReady);
app.on('ready', () => setTimeout(onAppReady, 500));
