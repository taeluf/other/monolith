<?php

namespace RBear\LiaExt;

class Meta {

    protected $request;
    
    protected function __construct($request){
        $this->request = $request;
    }
    public function meta($fields){
        $request = $this->request;
        foreach ($fields as $field=>$value){
            // echo $field."\n";
            if (substr($field,0,'5')=='meta.'){
                // var_dump('za');exit;
                $request->addHeadHtml('<meta name="'.substr($field,5).'" content="'.$value.'">');
                continue;
            } else if (substr($field,0,'3')=='og.'){
                // var_dump('za');exit;
                $request->addHeadHtml('<meta name="og:'.str_replace('.',':',substr($field,3)).'" content="'.$value.'">');
                continue;
            }
            switch ($field){
                case 'title':
                    $request->addHeadHtml('<title>'.$value.'</title>');
                    $request->addHeadHtml('<meta property="og:title" content="'.$value.'">');
                    break;
                case 'image':
                    $request->addHeadHtml('<meta property="og:image" content="'.$value.'">');
                    break;
                case 'url.style':
                    $request->addStyleUrl($value);
                    break;
                case 'url.script':
                    $request->addScriptUrl($value);
                    break;
                default:
                    $request->addHeadHtml("<!--could not set unrecognized field '{$field}'-->\n");
            }
            
        }
    }

    public function extractHtmlRdfa($string){
        $rdfaParser = new \Jkphl\RdfaLiteMicrodata\Ports\Parser\RdfaLite();

        // Parse an HTML file
        // $rdfaItems = $rdfaParser->parseHtmlFile('/path/to/file.html');

        // Parse an HTML string
        $items = [];
        $rdfaItems = $rdfaParser->parseHtml($string);
        // print_r($rdfaItems);
        // exit;
        foreach ($rdfaItems->items[0]->properties as $schemaProp=>$propArray){
            $propName = \RBear\Strings::replaceFirst($schemaProp,'http://schema.org/');
            $items[$propName] = $propArray[0];
        }
        // print_r($items);
        // exit;
        return $items;

        // // Parse a DOM document (here: created from an HTML string)
        // $rdfaDom = new \DOMDocument();
        // $rdfaDom->loadHTML($string);
        // $rdfaItems = $rdfaParser->parseDom($rdfaDom);

        // // Parse an XML file (e.g. SVG)
        // $rdfaItems = $rdfaParser->parseXmlFile('/path/to/file.svg');

        // // Parse an XML string (e.g. SVG)
        // $rdfaItems = $rdfaParser->parseXml('<svg viewBox="0 0 100 100" vocab="http://schema.org/">...</svg>');

        // return $rdfaItems;
        // // echo json_encode($rdfaItems, JSON_PRETTY_PRINT);
    }

    static public function addToApp($app,$key=NULL){
        $app->addHook('requestStarted',[static::class,'addToRequest'],$key);
    }
    static public function addToRequest($request,$key){
        $ext = new self($request);
        $key = $key ?? 'meta';
        $request->addExtension($ext,$key);
        return $ext;
    }
}