<?php

class SiteMeta {


    protected $pages = 
        [
            "menu" => [
                [
                    "url"=>"/index.php",
                    "title"=>"Home Page",
                    "children"=>[
                        [
                            "url"=>"/child.php",
                            "title"=>"Child Page"
                        ],
                        [
                            "url"=>"/other.php",
                            "title"=>"Other"
                        ]
                    ]
                ],
                [
                    "url"=>"/index.php",
                    "title"=>"Dummy Second Item",
                    "children"=>[
                        [
                            "url"=>"/child.php",
                            "title"=>"Dummy Child Page"
                        ],
                        [
                            "url"=>"/other.php",
                            "title"=>"Dummy Other"
                        ]
                    ]
                ]
            ]
        ];

    public function __construct(){

    }

    public function displayMenu($menuFile){
        $menu = $this->pages["menu"];
        require($menuFile);
    }

    static public function forDomain(){
        return new self();
    }
}