
<section style="background:lightgrey;">
<ul>
    <?php
        foreach ($menu as $index=>$item){
            $item = (object)$item;
            ?>
            <li><a href="<?=$item->url?>"><?=$item->title?></a>
                <?php
                    if (is_array($item->children)&&count($item->children)>0){
                        echo '<ul>';
                        foreach($item->children as $child){
                            $child = (object)$child;
                            ?>
                                <li><a href="<?=$child->url?>"><?=$child->title?></a>
                            <?php
                        }
                        echo '</ul>';
                    }
                ?>
            </li>
            <?php
        }
    ?>
    <!-- // <li>Item
    //     <ul>
    //         <li>Child</li>
    //         <li>Other</li>
    //     </ul>
    // </li> -->
</ul>
</section>