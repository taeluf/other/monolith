<view type="self" stylesheets="Calendar.css"></view>
<?php $calendar = \ROF\Calendar\Calendar::getActiveCalendar(); 
  $today = new \DateTime("now",new DateTimeZone("CDT"));
?>
  <div class="calendar">
    <div class="head">
      <h1><a href="<?=$calendar->getPreviousYearUrl();?>">&lt;&lt;</a>
        <?php echo $calendar->getNameOfMonth().' '.$calendar->getYear(); ?>
        <a href="<?=$calendar->getNextYearUrl();?>">&gt;&gt;</a>
      </h1>
      <div style="text-align:center;">
        <a class="button" href="<?=$_SERVER['REDIRECT_URL']?>"><?='Today '.$today->format("M d, Y");?></a>
      </div>
      <div class="controls">
        <a href="<?=$calendar->getPreviousMonthUrl();?>" class="previous">&lt;-<?=$calendar->getPreviousMonthName();?> </a>
        <a href="<?=\ROF\Calendar\Calendar::getActiveCalendar()->getNextMonthUrl() ?>" class="next"><?=$calendar->getNextMonthName();?>-&gt;</a>
      </div>
      <div class="dayNames">
        <span>Sunday</span>
        <span>Monday</span>
        <span>Tuesday</span>
        <span>Wednesday</span>
        <span>Thursday</span>
        <span>Friday</span>
        <span>Saturday</span>
      </div>
    </div>
    <div class="week">
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
    </div>
<div class="week">
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
    </div>
    <div class="week">
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
    </div>
    <div class="week">
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
    </div>
    <div class="week">
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
    </div>
 <?php if (\ROF\Calendar\Calendar::getActiveCalendar()->numWeeks()>=6) { ?>
    <div class="week">
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
      <view type="include" name="Day"></view>
    </div>
<?php } ?>
      
  </div>
  
