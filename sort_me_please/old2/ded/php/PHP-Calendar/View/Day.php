<?php $day = \ROF\Calendar\Calendar::getActiveCalendar()->getNextDay(); ?>
<span class="day">
  <div class="ofMonth">
    <?php echo $day->getDayOfMonth(); ?>
  </div>
  <div class="info">
    <?php echo $day->getDayInfo(); ?>
  </div>
</span>

