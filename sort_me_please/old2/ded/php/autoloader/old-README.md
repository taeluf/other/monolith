# PHP-Autoloader
Simple autoloader which maps namespaces to directories.  
`\My\NS\Class` loads from `BASE_DIR.'/My/NS/Class.php'`, for example.

# Composer Installation

    "require": { "rof/autoloader": "1.*" }

# Manual Installation
Copy Autoloader.php to wherever you want and `require_once` it in any script before you need it to function.   
Then see the usage (right below!)

# Usage

## Enable

### DEFAULT
    \ROF\Autoloader::enable();
    
### ALSO DEFAULT
    \ROF\AUtoloader::enable(NULL,TRUE);
    
### CUSTOM (the default uses these settings, but you can change them)
    $baseDirectory = $_SERVER['DOCUMENT_ROOT'].'/Class';
    $staticInitializer = TRUE; 
    \ROF\Autoloader::enable($baseDirectory, $staticInitializer);
    
## Where to put my classes
Put your class files in the base directory. By default, this is the document root with a subdirectory of 'Class'.  
For namespaced classes, there must be a directory for each namespace level.  
Examples:  
`MyClass` will `include $baseDirectory.'/MyClass.php'`  
`MyNamespace\MyClass` will `include $baseDirectory.'/MyNamespace/MyClass.php`  
`My\Ugly\Deep\Namespaced\Classy\Thingy` will `include $baseDirectory.'/My/Ugly/Deep/Namespaced/Classy/Thingy.php` 

## Static Initializer
You can declare a static initializer function with

    public static function __initialize(){ self::$my_static_var = complex_algorithm(); }
    
You can disable this by passing `FALSE` as the second variable of `enable(..)`. To use the default path but remove the static initializer, call `enable(NULL,FALSE)` as `NULL` will tell it to use the default base path.
    
 


    
