<?php

// use Sanitizer\Test\Sanitizer as TestClass;
// use Sanitizer\Sanitizer as Sanitizer;

namespace Sanitizer\Test;

class Sanitizer {

    public function __construct(){
        $this->sanitizer = new \Sanitizer\Sanitizer();
    }

    public function run(){
        echo '<pre>'."\n";
        $methods = get_class_methods($this);
        foreach ($methods as $method){
            if ($method=='run')continue;
            $mo = $method;
            while (strlen($mo)<15){
                $mo .='-';
            }
            echo "<b>{$mo}:</b> ";
            echo $this->$method() ? 'true' : 'false';
            echo "<br>\n";
        }
        echo "\n</pre>";
    }

    public function createSanitizer(){
        $sanitizer = new \Sanitizer\Sanitizer();
        return ($sanitizer instanceof \Sanitizer\Sanitizer);
    }
    public function sanitizeNumber(){
        $sanitizer = $this->sanitizer;
        $testInputs = [
            "1235439580234518",
            "12930418-347=--=-2345++-=23-510-908[]}{()",
            "abafds2384789u5@$#%@^676218b1ublh",
            "48432572834%$2Abdjea",
            "\n\s\t394abcd"
        ];
        foreach ($testInputs as $input){
            $result = $sanitizer->number($input);
            //assert
            if (!is_numeric($result)){
                return false;
            }
        }
        return true;
    }

    public function sanitizeSet(){
        $set = [
            "phone"=>"+1-217-413-9202",
            "email"=>"rsutman@gmail.com",
            "name" => "Reed Sutman",
            "password" => "KSDJF2314kl;7*&09245b^@#$"
        ];
        $types = [
            "phone" => "phone",
            "email" => "email",
            "name" => ["charcount"=>254],
            "password" => "string",
        ];
        $result = $this->sanitizer->set($set,$types);
        if (
            $result['phone'] == '12174139202'
            &&$result['email'] == 'rsutman@gmail.com'
            &&$result['name'] == 'Reed Sutman'
            &&$result['password'] == 'KSDJF2314kl;7*&09245b^@#$'
        ){
            return true;
        } else {
            return false;
        }
    }

}


?>