<?php

namespace RBear;

class Arrays extends \ArrayObject {

    public function __construct($array, 
        $flags=\ArrayObject::STD_PROP_LIST|\ArrayObject::ARRAY_AS_PROPS, 
        $iterator_class = "ArrayIterator"){
            parent::__construct($array,$flags,$iterator_class);

    }

    public function implode($glue=''){
        // print_r($this->getArrayCopy());
        // exit;
        return implode($glue,$this->getArrayCopy());
    }



    static public function hasKeys($array,...$keys){
        $true = TRUE;
        foreach($keys as $key){
            if (!isset($array[$key])){
                return false;
            }
        }
        return true;
    }

    static public function missingKeys($array,...$keys){
        $missing = [];
        foreach($keys as $key){
            if (!isset($array[$key])){
                $missing[] = $key;
            }
        }
        return new self($missing);
    }
}

