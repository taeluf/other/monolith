<?php

namespace RBear;

class PDO extends \PDO {

    protected $config;

    public function __construct($login=[],$giveHelp=TRUE){
        if (!is_array($login)
            ||!Arrays::hasKeys($login,'db','user','password')){
            throw new \Exception("You must pass an array, with at least: `['db'=>'my_db_name','user'=>'my_login','password'=>'my_password']`."
                ."\nYour array is missing: ".Arrays::missingKeys($login,'db','user','password')->implode(', '));
        }
        $defaults = [
            'type'=>'mysql',
            'host'=>'127.0.0.1',
            'fetch_style'=>\PDO::FETCH_ASSOC
        ];
        if ($giveHelp){
            $missing = Arrays::missingKeys($login,...array_keys($defaults));
            if (count($missing)>0){
                trigger_error("Defaults are being used for the keys(".$missing->implode(',')."). The full list of defaults is: ".var_export($defaults,TRUE)
                    .".\nPass FALSE as the second paramater to new RBear\PDO([],FALSE) to disable this log");
            }
        }
        $o = (object)array_merge($defaults,$login);
        
        $config = new \stdClass;
        $config->fetch_style = $o->fetch_style;
        $this->config = $config;
        parent::__construct($o->type.':dbname='.$o->db.';host='.$o->host,$o->user,$o->password);
    }

    public function fetchAll($query,$bind=[]){
        $statement = $this->prepare($query);
        if (count($bind)>0)$statement->execute($bind);
        else $statement->execute();
        $all = new Arrays($statement->fetchAll($this->config->fetch_style));
        $all->error = $statement->errorInfo();
        return $all;
    }
}