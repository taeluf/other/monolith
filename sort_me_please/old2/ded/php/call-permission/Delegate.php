<?php

class Delegate {

    protected $data = [];

    protected $origFile;

    protected $permits = [
        'file'=>[]
    ];

    public function __construct(){
        // $this->data = new \RBear\Arrays();
        $trace = debug_backtrace();
        $this->origFile = $trace[1]['file'];
    }

    public function addData($array){
        $this->data = array_merge_recursive($this->data,$array);
        // $this->data = $this->data->merge_recursive($array);
    }

    public function get($param){
        $parts = explode('.',$param);
        // print_r($parts);    
        $data = $this->data;
        // print_r($data);
        if (!$this->hasAccess($param)){
            throw new \Exception("Lookup key '{$param}' cannot be accessed by calling file/class/function(???)");
        }
        foreach ($parts as $index=>$key){
            if (isset($data[$key])){
                $data = $data[$key];
            } else {
                throw new \Exception("Key '{$key}' is not set within lookup string '{$param}'");
            }
        }
        return $data;
    }

    public function permitFile($fileName,$paramater){
        $trace = debug_backtrace();
        if ($this->origFile !== $trace[1]['file']){
            throw new \Exception("delegate->permitFile() can only be called from the instantiating file.");
        }
        $this->permits['file'][$paramater] = $this->permits['file'][$paramater] ?? [];
        $this->permits['file'][$paramater][] = $fileName;
    }
    protected function hasAccess($lookup){
        $trace = debug_backtrace();
        // print_r($trace);
        if ($trace[0]['file']!==__FILE__)return false;
        if (isset($this->permits['file'][$lookup])
            &&in_array($trace[1]['file'], $this->permits['file'][$lookup])){
            return true;
        }
        // print_r($trace);
        return false;   
    }

    public function call($param,...$args){
        
    }
}