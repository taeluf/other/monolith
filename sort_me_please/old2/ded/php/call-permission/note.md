# Accessors
I want to, by default, forbid access to any property. A property can then be permitted access by
- a specific class, optionally it's subclasses and/or parent classes as well
- a specific file
- a specific namespace
- a particular line of code
- to a call that includes a password

A paramater can be set to allow as long as just one method is met, or that several methods are met. A traditiona if &/|| situation