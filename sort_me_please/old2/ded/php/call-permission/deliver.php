<?php

require(__DIR__.'/Delegate.php');
require(__DIR__.'/../../github/tools/class/Arrays.php');

$delegate = new Delegate();
$delegate->addData(
    [
        'user'=>[
            'name'=>'Reed',
            'email'=>'rsutman@gmail.com',
            'password'=>'encryptedData',
            'passwordExpiry'=>'expires always',
            'profileUrl'=>'dev.localhost/fake-user-profile/'
        ],
        'site'=>[
            'name'=>'Dev Site',
            'pdo'=>new stdClass(),
            'log'=>function($message){
                echo "We are supposed to log the following message, but we're just printing it because we're baaaaad.";
            }
        ]
    ]
);

$delegate->permitFile(__FILE__,'user');
$delegate->permitFile(__FILE__,'user.name');
$delegate->permitFile(__FILE__,'site.pdo');

include(__DIR__.'/other.php');

$obj = new stdClass;
$obj->user = $delegate->get('user');
$obj->userName = $delegate->get('user.name');
$obj->fakePdo = $delegate->get('site.pdo');

$delegate->call('site.log','This awesome message should be logged');


var_dump($obj);