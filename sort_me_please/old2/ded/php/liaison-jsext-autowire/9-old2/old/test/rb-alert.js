




class AlertParent extends RB.Autowire {

    ontouch(event){
        console.log('whatever');
    }
}

class Alert extends RB.Autowire {


    onclick(event){
        alert("The alerter worked");
        console.log('onclick');
        console.log(arguments);
        console.log(this);
        console.log(event.target);
    }
}
Alert.className = 'rb-Alert';

Alert.autowire();
