# Install JS Autowire

- Download/copy `Autowire-min.js` & deliver it on your own server
- **TODO:** `<script type="text/javascript" href="#PATH-TO-CDN"></script>` (Because who doesn't host their js package on a CDN?)
- **TODO:** `composer require rbear/js-autowire` (a PHP universe thing...)
    - I still need to sideload to packagist
- **TODO:** `npm something something` (A world I'm new to)


## Install with Composer
I have NOT made a packagist package or created a release.  
So, in your `composer.json`:
```json
"require": {
        "taeluf/jsautowire": "dev-master"
    },
"minimum-stability": "dev",
"repositories": [
    {
        "type": "vcs",
        "url":  "git@github.com:ReedOverflow/JS-Autowire.git"
    }
]
``` 

Then do one of:
    - Get the `Autowire-full.js` or `Autowire-min.js` from the downloaded repository.
    - `$jsCode = file_get_contents( \JSAutowire::jsFullPath() )` for non-minified
    - `$jsCode = file_get_contents( \JSAutowire::jsMinPath() )` for minified code
    - Use Liaison
    
### For Liaison
If you use `Liaison`, do the following:
```php
$liaison = new \Liaison(); //the instance you're adding all your components to for a site
$liaison->addComponentDir(\JSAutowire::compoDir());
```
