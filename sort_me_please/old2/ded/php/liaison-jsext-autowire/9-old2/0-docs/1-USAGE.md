# Js Autowire: USAGE

Review the `OVERVIEW` doc file for new features & additional documentation which needs to be better sorted.

## Basics
- `YourClass extends RB.Autowire`
- `YourClass.autowire()` or `YourClass.autowire('#querySelector')`
- declare `onclick(event){/*your code*/}` inside `YourClass` to automatically set it up on the node
    - Use any `onEVENT_NAME` function to do the same
- override `__attach()` to do initialization once the node is attached.
- `this.node` is the auto-wired node
- `this.context = "contextName"` in `__attach()` to specify scope for auto-properties. 
    - skip this to use the default/global context
- override `__ready()` to do initialization after all autowiring is complete, thus sibling objects can be referenced
- Declare `_propName(){return 'OtherClass';}`
    - `this.propName` returns an array of `OtherClass` objects in your context
    - `this.onePropName` returns one instance of `OtherClass` in your context

## Wiring Options
- Pass a query-selector to autowire like: `Increment.autowire('.rb-Increment');`
- Specify the query-slector on the class: `Increment.querySelector = '.rb-Increment';`
    - Any query selector works. Such as `Increment.querySelector = 'div#TheIncrementor';` will attach `<div id="TheIncrementor">` to `Increment`
- Direct Wiring (static): `Increment.wireNode(someNodeObject);`
- Direct Wiring (instance): `new \Increment(someNodeObject);`
- Specify the css class name: `Increment.className = "rb-Increment";`
- Use the declared class name: `class BigDiv extends RB.Autowire` maps to `<anytag class="BigDiv">`

## Initialization Methods
Every class can implement the initialization methods `__construct()`, `__attach()`, and `__ready`
- `__construct()` is called before a node is attached to the JS Object...
- `__attach()` is called post-attach. 
    - The associated node can be referenced with `this.node` from inside `__attach()`
    - **Warning:** Do not use auto properties at this point. Wait until `__ready()`
- `__ready()`:  to do initialization after all autowiring is complete, thus sibling objects can be referenced

## Context & AutoProps
- Declare `this.context='ContextName'` in `__attach()`. All nodes in the same `'ContextName'` will be able to reference each other with `_autoProps...` Or leave this off & the default context will be used
- Specify `_propName(){return 'ClassName';}` to allow nodes attached to `'ClassName'` to be referenced with `this.propName;`
- Declare `this.context=['ContextName','OtherContext'];` to have multiple contexts
    - Specify `_propName(){return [null,'ClassName'];}` to auto-prop from your `'OtherContext'`