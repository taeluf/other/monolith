<?php
require(__DIR__.'/build.php');
echo '<script>'.file_get_contents(__DIR__.'/../Autowire-min.js').'</script>';
echo '<script>'.file_get_contents(__DIR__.'/../test/rb-alert.js').'</script>';
echo '<script>'.file_get_contents(__DIR__.'/../test/rb-test-Request.js').'</script>';
?>
<section>
    <h1>Alerter</h1>
    <p>Click this button to show an alert (and some console logs happen too)</p>
    <?=file_get_contents(__DIR__.'/../test/rb-alert.html');?>
</section>
<section>
    <h1>Count Incrementer</h1>
    <p>Each time you click 'Add 1', the number just below it will be increased by one. This demonstrates the context-getting</p>

    <script type="text/javascript">
        class Increment extends RB.Autowire {
            _count(){ return 'RB_Count'; } // this.count will return the .RB_Count Object (not the node directly). 
            __attach(){ this.context = "RB"; }

            onclick(event){
                this.count.add(1);//.node.innerText = count;
            }
        }
        // Increment.querySelector = ".rb-Increment";
        Increment.className = "rb-Increment";
        Increment.autowire();

        
        class RB_Count extends RB.Autowire {

            __attach(){ 
                this.context = "RB"; 
                this.count = parseInt(this.node.innerText.trim());
            }
            add(amount){
                this.node.innerText = (this.count=this.count+amount);
            }
        }
        RB_Count.autowire();
    </script>


    <button class="rb-Increment">Add 1</button>
    <div class="RB_Count">0</div>
</section>
<section>
    <button class="rb-test-Request">Make Request</button>
    <div class="rb-test-Result"></div>
</section>
