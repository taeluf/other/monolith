# JS Autowire: Overview
This file contains:
    - Features List
    - Recent Changes 
    - New documentation
    - Condensed instruction of functionality

You should look at the USAGE & EXAMPLE doc files, probably. For code.

**Docs:** (eventually) View the docs on [ReedyBear.com/docs/js/autowire/](https://www.reedybear.com/docs/js/autowire/)

## Features (incomplete)
- Automatically attach a Javascript class/object to a DOM Node
- Reference other AutoWire objects (& their attached nodes) with 1-line autoproperty methods.
- Create multiple contexts in which AutoWire objects can relate
- Declare event listeners (like `onclick`) in your class to add the listeners to the nodes.


## New Features: (not documented elsewhere)
- Requests can be sent and responded to more easily. In an Autowire class, call `this.send(url, data, this.responseMethod, requestMethod="POST");`. `responseMethod` will be auto-bound with the same `this` which calls `this.send`. `this.responseMethod` accepts one paramater, which is a JSON Object. If the request does not return a JSON object, it will not work.
- (no tests) Set `this.context = this.nodeContext(node.parentNode,contextName='default')` to set the context based upon a node. A unique id is generated for the node passed to nodeContext(...). `contextName` is optional & usually not necessary
- (no tests) autoprops always return an array now. Previous it returned null if not found, the object if only one found, and an array if > 1 found
- (no tests) `this.oneProp` will load a single object for `_prop(){ return 'ClassNameOfTarget' }`. If one item, returns it. If multiple items, returns the first (no intentional ordering). If no items, returns null
- (no integrated tests) `__ready()` is called after ALL `autowire()` calls are resolved, thus allowing you to reference siblings during `__ready()`, which you could not reliably do during `__attach()`


## Notes
- You can ignore the `php` directory. I am using it for build purposes because I'm predominantly a PHP dev & not set up for a js-dev workflow
- The `liaison` directory is for integration with my home-brewed PHP framework
- the `old` directory contains code I want to hold onto and maybe use at a later time.
- The `test` directory has tests, but `php/run-test.php` puts those tests together (& builds & includes the minified framework)