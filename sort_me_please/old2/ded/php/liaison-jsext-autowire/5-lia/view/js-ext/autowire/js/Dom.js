

Autowire.Dom = class {

    static cycle(node,attribute,value=null){
        if (value==null
            &&node.hasAttribute(attribute)){
                node.removeAttribute(attribute);
        } else if(value==null){
            node.setAttribute(attribute,'');
        } else {
            // const index = value.indexOf(node[attribute]);
            // index++;
            // index = index%value.length;
            node[attribute] = value[ (1+value.indexOf(node[attribute]))%value.length ];
        }
    }

    static is(tagName,node){
        if (node.tagName.toLowerCase()==tagName.toLowerCase())return true;
        return false;
    }
}