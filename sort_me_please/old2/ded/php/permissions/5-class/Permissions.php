<?php

namespace Taeluf;

class Permissions implements Permissions\IPerm {

    public function __construct(Permissions\IPerm $dataProvider){
        $this->provider = $provider;
    }

    // public function can($performerUUID, $action, $consenterUUID){
    //     $can = $this->provider->can($performerUUID, $action, $consenterUUID);
    //     return $can;
    // }
    // public function getPermissions($uuid, $type=null){
    //     return $this->provider->getPermissions($uuid, $type);
    // }

    public function __call($method, $args){
        return $this->provider->$method(...$args);
    }


}