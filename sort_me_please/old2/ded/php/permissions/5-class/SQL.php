<?php

namespace Taeluf\Permissions;

class SQL implements IPerm {

    protected $pdo;

    public function __construct($pdoObject){
        $this->pdo = $pdoObject;
        //verify it's a valid pdo object
    }

    public function can($performerUUID, $action, $consenterUUID){
        $sql = 
        <<<SQL
            SELECT COUNT(*) 
                FROM 
                    test_permissions AS consenter,
                    test_permissions AS performer

                WHERE 
                    (
                        consenter.uuid LIKE ':consenter_uuid'
                        AND consenter.type LIKE 'consent'
                        AND consenter.action LIKE ':action'
                    )
                AND
                    (
                        performer.uuid LIKE ':performer_uuid'
                        AND performer.type LIKE 'perform'
                        AND performer.action LIKE ':action'
                    )
                ;
        SQL;
        // perform sql query
        // if count >= 1, then return true
        // else return false
    }

    public function getPermissions($uuid, $type=null){
        $sqlType = "";
        if ($type==\Taeluf\Permissions::PERF){
            $sqlType = "AND type LIKE 'perform'";
        } else if ($type==\Taeluf\Permissions::CONSENT){
            $sqlType = "AND type LIKE 'consent'";
        } else if ($type!=null){
            throw new \Exception("type must be one of perf or consent");
        }
        $sql = 
        <<<SQL
            SELECT action, type
                FROM test_permissions AS perms
                WHERE
                    perms.uuid LIKE :uuid
                    {$sqlType}
        SQL;
        //perform query
        //return results
    }


}