<?php

namespace Head;

class Merger {

    static protected $merger = null;

    static public function instance(){
        return self::$merger ?? (self::$merger = new self());
    }

    protected $html = '';

    public function mergeStart(){
        ob_start();
    }
    public function mergeEnd(){
        $this->html .= "\n".ob_get_clean();
    }
    public function getHtml(){
        return $this->html;
    }
}