<?php

\Head\Merger::instance()->mergeStart();
?>
<title>New Title</title>
<link rel="stylesheet" type="text/css" href="/some/sheet.css">
<script type="text/javascript" src="/some/script.js"></script>
<?php
\Head\Merger::instance()->mergeEnd();
?>