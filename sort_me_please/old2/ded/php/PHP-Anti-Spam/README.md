
# Under Construction, 12-27-2018
This library is probably not ready to be used! I haven't looked at it in a month or two and am not sure of it's current standing.

# HTML-Anti-Spam-Form
PHP Anti-spam library. Currently in the planning stages. Current code will likely be deleted.

Forgive everything on this page. I'm writing it for myself at this stage.

# Why not just use google's recaptcha?
You probably should. It's probably better. And probably easier to implement, too. This library will provide server-side operation, which probably provides some kind of benefit. Personally, I just like to do things in PHP. And I don't like adding js code to my `<head>` either. Mainly, I just want to make this little library.

However, this does not require any API calls from your website to Google, so that's something!

Perhaps I should add Google's recaptch to this, making it a simple wrapper for recaptcha, so PHP developer's lives are just a lil bit easier.

No third-party tracking. This is all your server! Google tracks the bejeeze outta everything, seems like

# Project under construction

## thoughts and stuff
It's been awhile since I studied fighting spam. But there are a few key components that I'm aware of that help prevent spam submissions, which include:


  - Honeypot field
    - This is a blank, hidden field, which hopefully any bot will fill. If it's filled, then you know it's not a valid submission.
  - Auto-fill field
    - This will be filled by javascript code. Hopefully the bot would not execute the javascript
  - Already-filled field
    - This is a field that will have data already in it. If the data is not there, or is not correct, then you know that it's not filled by a user who requested the web page
  - Time field
    - This indicates when the page was loaded. If the form is submitted too quickly or too slowly, chances are it's a bot
  - Data field
    - this will contain an encrypted version of all the data that's talked about above. This field will be the only one with a predictable name. The rest of the data will be verified based upon this field and have names that are obfuscated
    

I would also like to provide an option to obfuscate the names of any field that you put into your form. The obfuscations can be random, therefore eliminating any possibility that a bot could be successful, as it wouldn't know what to fill in. Even if it kept the encrypted data field alive, it would eventually die, as the time would expire. All of the fields mentioned above should have encrypted versions of their data, so they are not vulnerable. Plus the encrypted time data in the data field should match the time value in the time field. Since a bot would not know which field is which without requesting the page, it would not know what to fill in. 

So, the next step, I guess, is to start designing classes. What classes should exist and what functions should exist in the classes? AND what variables and class constants should exist?


(here's a question. Should I remove the AntiSpam namespace and make it simply RO\AntiSpam? I could do this for my other libs as well, at least for the base class that shares a name with the namespace...)

RO\AntiSpam\AntiSpam:

**Object Variables**
- $names
  - this will contain key=>value pairs where the key is an obfuscated name and the value is the readable name (or vice versa?). The obfuscated name will be present at `<input name="OBFUSCATED_NAME">`
- $values
  - key=>value pair where the key is the readable name of a spam-control field and the value is the expected value.

**Object Methods**
- associative array  getSubmittedData($isPOST=TRUE)
  - returns all submitted data as an associative array with readable names, leaving out all the spam control fields.
- HTML string getSpamControls()
  - Returns the full HTML for all the fields described above
- bool isSpam()
  - Checks the incoming data. Reads the "data" field and decrypts it, then validates all the data
- string getEncryptedData()
  - returns a string which represents all of the data in the object's $names variable and $values variable. 
- associative array getDecryptedData()
  - returns an array of arrays. First level contains key="names" & value= array of obfuscatedName=>readable name pairs. First level also contains key="value" & value= array of readableName=>spamControlValue
- html string getDataField()
  - returns an `<input>` which has name = "data" and value="ENCRYPTED STRING"
- string getReadableName($obfuscatedName)
  - this is called upon submission. returns the readable name from the obfuscated name. It uses the 'data' field to know what the readable name is
- string getObfuscatedName($readableName)
  - obfuscates the name. Adds the readable name and obfuscated name to an array. That array is later encrypted with all the data and saved to the form in the data field. This is called upon form delivery, NOT submission


# thoughts
It may be a good option to, instead of storing all the data to the 'data' field, I could store it to the `$_SESSION` variable. The problem with this is the reliance upon sessions. It would reduce the page load time, as the encrypted string of data MAYYY be a bit long. However, I doubt it's more than 500 characters in the encrypted form. It's not like it really has to be secure.
It could be reasonable to store the data in the form itself AAAND in a session variable, then I compare the two as well as compare the "data" value to the 

It could be reasonable to receive the data on submission, redirect to another page, then redirect to the actual result page. Sounds complex and unnecessary and frustrating for a user if anything goes wrong.

