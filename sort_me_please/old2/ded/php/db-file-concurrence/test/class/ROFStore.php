<?php

namespace RBear;

class ROFStore {

    protected $pdo;
    protected $tableName;
    protected $primaryKey;

    public function __construct($dir, $primaryKey){
        $this->dir = $dir;
        $this->primaryKey = $primaryKey;
    }
    public function query($forId){
        $dir = $this->dir;
        $dh = opendir($dir);
        $fileData = [];
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            $ext = pathinfo($file,PATHINFO_EXTENSION);
            if ($ext!=='rof')continue;
            $parts = explode('--',$file);
            if (count($parts)!==2)continue;
            $id = $parts[1];
            if ($id==$forId)return $this->rof_parse($dir.'/'.$file);
        }

        return null;
    }
    protected function rof_parse($filePath){
        return (new \ROF\Resource\Decoder())->parseFile($filePath);
    }
    protected function rof_encode($array){
        return (new \ROF\Resource\Encoder())->encode($array);
    }
    protected function fileName($row){
        $name = $row['number'].$row['suit'];
        if (isset($row[$this->primaryKey]))$name .= '--'.$row[$this->primaryKey];
        return $name;
    }
    protected function uuid(){
        /** This function used from https://github.com/kengoldfarb/underscore_libs/blob/master/src/_Libs/_UUID.php
         *  under the MIT license
         * Copied on May 5, 2020 @ 11:33am central-standard-time
         */
        $format = '%04x%04x-%04x-%04x-%04x-%04x%04x%04x';
        return sprintf($format,
                        // 32 bits for "time_low"
                        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                        // 16 bits for "time_mid"
                        mt_rand(0, 0xffff),
                        // 16 bits for "time_hi_and_version",
                        // four most significant bits holds version number 4
                        mt_rand(0, 0x0fff) | 0x4000,
                        // 16 bits, 8 bits for "clk_seq_hi_res",
                        // 8 bits for "clk_seq_low",
                        // two most significant bits holds zero and one for variant DCE1.1
                        mt_rand(0, 0x3fff) | 0x8000,
                        // 48 bits for "node"
                        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
    public function getAll(){
        $dir = $this->dir;
        $dh = opendir($dir);
        $fileData = [];
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            $ext = pathinfo($file,PATHINFO_EXTENSION);
            if ($ext!=='rof')continue;
            $key = pathinfo($file,PATHINFO_FILENAME);
            $fileData[$key] = $this->rof_parse($dir.'/'.$file);
            $fileData[$key]['--file_path--'] = $dir.'/'.$file;
        }
        return $fileData;
    }
    public function put(&$dataRows){
        $pk = $this->primaryKey;
        $changedRows = [];
        $copy = $dataRows;
        foreach ($copy as $index=>$row){
            if (isset($row[$pk])){
                $existing = $this->query($row[$pk]);
                if ($row===$existing)continue;
                $changedRows[] = $row;
                continue;
            } 
            $row[$pk] = $this->uuid();
            $changedRows[] = $row;
            $dataRows[$index] = $row;
        }
        
        foreach ($changedRows as $row){
            $existingFile = $row['--file_path--'] ?? null;
            $newFile = $this->dir.'/'.$this->fileName($row).'.rof';
            if ($existingFile!==null
                &&$existingFile!=$newFile)rename($existingFile,$newFile);
            $copy = $row;
            unset($copy['--file_path--']);
            file_put_contents($newFile,$this->rof_encode($copy));
        }
        return true;
    }
    public function putOne($row){
        return $this->put([$row]);
    }
    protected function getPut($row,$count){
        $pk = $this->primaryKey;
        if (isset($row[$pk])){
            ksort($row);
            $keys = '`'.implode('`,`',array_keys($row)).'`';
            $bindRows = [];
            $updateStrs = [];
            $pkBinder = '';
            foreach ($row as $key=>$value){
                $bindKey = ':'.preg_replace('/[^a-zA-Z0-9]/','_',$count.'_'.$key);
                $bindRows[$bindKey] = $value;
                if ($key==$pk){
                    $pkBinder = $bindKey;
                    continue;
                }
                $updateStrs[] = "`{$key}`={$bindKey}";
            }
            $updateStr = implode(',',$updateStrs);
            $sql = "UPDATE {$this->tableName} SET {$updateStr} WHERE `{$pk}`={$pkBinder};";
            $sql .= "\nSELECT NULL AS id, 'true' AS record;";
            return [
                'sql'=>$sql,
                'binds'=>$bindRows,
            ];
        } else {
            ksort($row);
            $keys = '`'.implode('`,`',array_keys($row)).'`';
            $bindRows = [];
            foreach ($row as $key=>$value){
                $bindRows[':'.preg_replace('/[^a-zA-Z0-9]/','_',$count.'_'.$key)] = $value;
            }
            $bindStr = implode(',',array_keys($bindRows));
            $sql = "SET @uuid=uuid();";
            $sql .= "\nINSERT INTO {$this->tableName} (`{$pk}`,{$keys}) VALUES (@uuid,{$bindStr});";
            $sql .= "\nSELECT @uuid AS id, 'true' AS record;";
            return [
                'sql'=>$sql,
                'binds'=>$bindRows,
            ];
        }
    }
}