<?php

namespace RBear;

class DataComparator {

    public function synchronize($a,$b){
        $aData = $a->allData();
        $bData = $b->allData();
        $a->pushChanges($bData);
        $b->pushChanges($aData);
        return;
        $aCopy = $aData;
        foreach ($aCopy as $aKey=>$aRow){
            if (!isset($bData[$aKey])){
                $bData[$aKey] = $aRow;
                continue;
            }
            $bRow = $bData[$aKey];
            if ($bData[$aKey]==$aRow){
                    unset($bData[$aKey]);
                    unset($aData[$aKey]);
                    continue;
                }
            if ($aRow['mtime']>$bRow['mtime']){
                $bData[$aKey] = $aRow;
                unset($aData[$aKey]);
            } else if ($bRow['mtime']>$aRow['mtime']){
                $aData[$aKey] = $bRow;
                unset($bData[$aKey]);
            }
        }
        var_dump($aData);
        var_dump($bData);
        $a->pushChanges($aData);
        $b->pushChanges($bData);
    }
}