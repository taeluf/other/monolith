<?php

namespace RBear;

class ROFDataSource {

    protected $dir;

    public function __construct($dir){
        $this->dir = $dir;
    }

    public function allData(){

        $dir = $this->dir;
        $dh = opendir($dir);
        $fileData = [];
        while ($file=readdir($dh)){
            if ($file=='.'||$file=='..')continue;
            $key = pathinfo($file,PATHINFO_FILENAME);
            $fileData[$key] = $this->rof_parse($dir.'/'.$file);
        }

        return $fileData;
    }
    public function pushChanges($data){
        
    }

    protected function rof_parse($filePath){
        return (new \ROF\Resource\Decoder())->parseFile($filePath);
    }
}