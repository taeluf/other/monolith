<?php

namespace RBear;

class PDODataSource {

    protected $pdo;
    protected $table;

    public function __construct($pdo,$table){
        $this->pdo = $pdo;
        $this->table = $table;
    }
    public function allData(){
        $dbData = $this->pdo->fetchAll("SELECT * FROM card");
        unset($dbData['error']);
        unset($dbData['count']);
        $retData = [];
        foreach ($dbData as $index=>$row){
            $retData[$row['number'].$row['suit']] = $row;
        }
        // var_dump($retData);
        // exit;
        return $retData;
    }
}