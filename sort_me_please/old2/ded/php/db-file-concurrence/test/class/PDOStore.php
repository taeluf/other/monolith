<?php

namespace RBear;

class PDOStore {

    protected $pdo;
    protected $tableName;
    protected $primaryKey;

    public function __construct($pdoObj, $tableName, $primaryKey){
        $this->pdo = $pdoObj;
        $this->tableName = $tableName;
        $this->primaryKey = $primaryKey;
    }
    public function query($query,$bind=[],$fetchMode=PDO::FETCH_ASSOC){
        $statement = $this->pdo->prepare($query);
        $result = count($bind)>0 ? $statement->execute($bind) : $statement->execute();
        if (!$result)return trigger_error(print_r($statement->errorInfo(),true)) && FALSE;
        return $statement->fetchAll($fetchMode);
    }
    public function getAll(){
        $statement = $this->pdo->prepare("SELECT * FROM {$this->tableName};");
        $result = $statement->execute();
        return ($statement->fetchAll(PDO::FETCH_ASSOC));
    }
    public function put(&$dataRows){
        $pk = $this->primaryKey;
        $bulkStatement = [];
        $bulkBind = [];
        $c=0;
        foreach ($dataRows as $row){
            $put = $this->getPut($row,$c++);
            $bulkStatement[] = $put['sql'];
            $bulkBind = array_merge($bulkBind,$put['binds']);
        }
        $bulkStatement = array_map(function($sql){return $sql;},$bulkStatement);
        $bulkSQL = implode("\n",$bulkStatement);
        $statement = $this->pdo->prepare($bulkSQL);
        $result = $statement->execute($bulkBind);
        $insertIds = [];
        do {
            // if ($statement->rowCount()==0)continue;
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
            if (count($rows)==0)continue;
            $row = $rows[0];
            if ($row['record']!=='true')continue;
            $id = $row['id'];
            $insertIds[] = $id;
        } while($statement->nextRowset());
        // $insertIds = $statement->fetchAll();
        // var_dump($bulkSQL);
        // var_dump($bulkBind);
        // // exit;
        // echo "insert ids:\n";
        // var_dump($insertIds);
        // echo "\ndone with insert ids;";
        // var_dump($statement->errorInfo());
        // exit;
        var_dump($insertIds);
        // var_dump($dataRows);
        // var_dump(array_slice($dataRows,2,1));
        $copy = $dataRows;
        $index=0;
        foreach ($copy as $key=>$row){
            $row[$pk] = $insertIds[$index] ?? $row[$pk];
            $index++;
            $dataRows[$key] = $row;
        }
        // foreach ($insertIds as $index=>$insertId){
        //     if ($insertId===null)continue;
        //     $singleRow = array_slice($dataRows,$index,1);
        //     $row = array_pop($singleRow);
        //     var_dump($row);
        //     $entries = array_slice($dataRows,$index,1);
        //     // var_dump($entries);
        //     // $entry = array_pop($entries);
        //     // $entry[$pk] = $insertId;
        //     // array_splice($dataRows,$index,1,$entry);
        // }
        var_dump($dataRows);exit;
        return $result;
    }
    public function putOne($row){
        return $this->put([$row]);
    }
    protected function getPut($row,$count){
        $pk = $this->primaryKey;
        if (isset($row[$pk])){
            ksort($row);
            $keys = '`'.implode('`,`',array_keys($row)).'`';
            $bindRows = [];
            $updateStrs = [];
            $pkBinder = '';
            foreach ($row as $key=>$value){
                $bindKey = ':'.preg_replace('/[^a-zA-Z0-9]/','_',$count.'_'.$key);
                $bindRows[$bindKey] = $value;
                if ($key==$pk){
                    $pkBinder = $bindKey;
                    continue;
                }
                $updateStrs[] = "`{$key}`={$bindKey}";
            }
            $updateStr = implode(',',$updateStrs);
            $sql = "UPDATE {$this->tableName} SET {$updateStr} WHERE `{$pk}`={$pkBinder};";
            $sql .= "\nSELECT NULL AS id, 'true' AS record;";
            return [
                'sql'=>$sql,
                'binds'=>$bindRows,
            ];
        } else {
            ksort($row);
            $keys = '`'.implode('`,`',array_keys($row)).'`';
            $bindRows = [];
            foreach ($row as $key=>$value){
                $bindRows[':'.preg_replace('/[^a-zA-Z0-9]/','_',$count.'_'.$key)] = $value;
            }
            $bindStr = implode(',',array_keys($bindRows));
            $sql = "SET @uuid=uuid();";
            $sql .= "\nINSERT INTO {$this->tableName} (`{$pk}`,{$keys}) VALUES (@uuid,{$bindStr});";
            $sql .= "\nSELECT @uuid AS id, 'true' AS record;";
            return [
                'sql'=>$sql,
                'binds'=>$bindRows,
            ];
        }
    }
}