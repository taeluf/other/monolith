# Product & Tech Spec: A package to ensure consistency between a database and file-based representation of that db
In a small site, I have some data that is stored on disk. I don't want to create web forms for it, not at this stage. I simply want to plug the data in myself & have it shown on the website. I'd like it to exist in a database, but I want to edit it as files. So this would require something to ensure consistency between the files and the database.

The database is to be queried.
The database may be updated
The files are NOT to be queried
When a database entry is updated, the corresponding file is updated
When a file is modified, the corresponding row of the database will be updated with the changes
If the database and the file have the same modification time, then a .diff file will be generated


When a query is performed, a marker file's mod-time is checked. If the marker-file has been modified (since last query), then all modified files are checked over and inserted / updated into the database. If ids are generated from inserts, the files are updated as well. Now that everything is up-to-date, the query is performed.

The most crucial functionality is comparing the file's info to the db's info. To check all rows, an array of all files for the table is built. Just the names. Then the database is queried for all rows (but looped cursor-like, not array-like). For each row, the corresponding file is loaded. Mod-time is compared & column-values are compared & the db or file is updated if needed. File changes are done as each file is accessed. The database is updated every 50 rows or at the end, thus a stack of updates is made. As a DB row is processed, the associated file is removed from the array of all files. After all DB rows are processed, the array of files is checked for any straggling files. Each of these files is then inserted into the table.

A future version could:
- Auto-create the table from the data-files
- Output the schema to a file
    - Allow the schema file to be edited & have changes auto-pushed into the database
- Provide querying functionality & their mod-times


## Tech Spec (hopefully?)
There are two data sources which must be compared & synchronized. It gets complicated when there is a back and forth between data sources. 

### INSERT from File to DB & return id + new mod-time
1) FileSource has 10 new rows. The following steps are applied to each row.
2) PDO source does NOT have this row. An INSERT is performed.
3) An ID is generated from the database. This id-generation updates the mod-time on the data
4) FileSource receives the new data (id & new mod-time) from PDO source. 
5) FileSource pushes the new row to the existing file, adding an id & updating the mod-time
6) Repeat 2-5 until all rows are processed

### INSERT from File to DB, but IDs already are set
FileSource has 10 rows. PDOSource was corrupted & all rows are deleted. Each row in FileSource already has an ID
1) Each row of FileSource is pushed into PDOSource. No new IDs are generated. Mod-times do not change. This is all.

### INSERT from File to DB, but IDs have conflicts with non-equal rows
FileSource does NOT define IDs. It only stores them. Therefore these rows SHOULD be inserted & given new ideas. Options include:
- Update the DB where the IDs conflict with the data from the file (integer-ids make this really bad. UUIDs would prevent this)
- INSERT the rows from FILE, generate new IDs, (thus new mod-time) & update FILE with the new IDs & Mod-times
- do nothing (How would I ever know that two rows with the same ids are not the same row with non-equal updates?)

It should be update the existing rows where an id-conflict exists. The assumption is that an id in one data source is exactly equal to the id in the other data source. The id is the one thing that ties them together. EVERYTHING ELSE changes & cannot reliably be used to ensure consistency.
NOTE: A DataSource may have a 'findRowEqualTo()'... Example: `$aRow = $a->findRowEqualTo($bRow); if ($aRow['mtime']>$bRow['mtime']) update $bRow`

### File has correct data except: No ID, mod-time is wrong. Database has correct data WITH ID
Should this be resolveable? Possibly.
- `FILE->findRowEqualTo(DBRow)`... If FILE returns a row, then the FILE will be updated with the ID & latest mod-time
- `FILE->findRowEqualTo(DBRow)`... returns null. The DB row is Pushed to FILE as a new row

### Data is consistent, then FILE is updated
1) Most recent mod-time is loaded from DB
2) All FILEs with mod-time more recent are loaded
3) For each FILE row, `DB->findRowEqualTo(FILERow)` is executed
4) Each DBRow is updated (array) with the latest data from FILERow
5) Each DBRow is UPDATEd (mysql) with the latest data & mod-time.

### Data is consistent, then both FILE & DB (same row) are updated
- This breaks my most-recent mod-time solution... 'Least I think it does
1) Most recent mod-time is loaded from DB
2) All files with more recent mod-times are loaded.
3) DB Rows are updated from each file
    - Therefore, if FILE & DB are updated at different times, the more recent update wins

1) most recent mod-time is loaded from FILE
2) All DBRows with more recent mod-times are loaded
3) `FILE->findRowEqualTo(DBRow)` is called for each DBRow
4) Each FILERow is updated & pushed to disk

I think a modification log needs to be kept. Each modification has:
- a rowId
- a modTime
- a modId
- a modSource (specifying DB or FILE, basically)
- the modData (an array (json) of key=>value pairs, containing the full mod, not just the rows that changed)

Each Source must specify:
- a rowId
- a modId

We assume:
- Some datasources are extremely inefficient & cannot be queried
- We have access to a modification log

To ensure consistency:

Idunno.

There are:
- Data Source A
- Data Source B
- Modifications
- Stale Data
