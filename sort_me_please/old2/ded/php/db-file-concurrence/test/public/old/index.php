<?php

$pdo = $this->get('db.pdo');

// $cards = new CardFile();
// $db = new CardDB();

// require(__DIR__.'/../../../rof-file-format/class/Decoder.php');
// require(__DIR__.'/../../../rof-file-format/class/ROFToken.php');

function rof_parse($filePath){
    // $content = file_get_contents($filePath);
    return (new \ROF\Resource\Decoder())->parseFile($filePath);
}

$dir = __DIR__.'/../db/card/';
$dh = opendir($dir);
$fileData = [];
while ($file=readdir($dh)){
    if ($file=='.'||$file=='..')continue;
    $fileData[$file] = rof_parse($dir.'/'.$file);
}

echo var_export($fileData);
exit;

$dbData = $pdo->fetchAll("SELECT * FROM card");
unset($dbData['error']);

foreach ($dbData as $key=>$card){
    $num = $card['number'];
    $suit = $card['suit'];
    $fileKey=$num.$suit.'.rof';
    // var_dump($fileKey);
    if (isset($fileData[$fileKey])){
        unset($fileData[$fileKey]);
    }
}

print_r($fileData);
// return;
if (count($fileData)>0){
    $keyList = array_keys($fileData[array_keys($fileData)[0]]);
    sort($keyList);
    $keyList = implode(',',$keyList);
    $fileData = array_map(function($row){ksort($row);return $row;},$fileData);
    $bindArray = [];
    $valuesList = [];
    foreach ($fileData as $index=>$row){
        $subRow = [];
        ksort($row);
        foreach ($row as $key=>$value){
            $realKey = ':'.preg_replace('/[^a-zA-Z0-9]/','_',$key.$index);
            $subRow[$realKey] = $value;
            $bindArray[$realKey] = $value;
        }
        $valuesList[] = '('
                .implode(',',array_keys($subRow))
            .')';
    }

    $sql = "INSERT INTO card ({$keyList}) \n     VALUES ".implode(",\n",$valuesList);
    var_dump($sql);
    var_dump($bindArray);
    // return;
    $result = $pdo->query($sql,$bindArray);
    var_dump($result);
}