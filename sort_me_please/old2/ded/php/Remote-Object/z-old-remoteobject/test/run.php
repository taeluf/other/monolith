<?php


class MTGTester extends \RBear\Tester{

    public function prepare(){
        \RDB::wipe('tag');
        $tAbility = \RDB::dispense('tag');
        $tAbility->name = 'triggered-ability';
        $tAbility->short='trigger';
        // $tag->level=0;
        $tAbility->ownChildrenList = [];
        $tAbility->parent = '';

        $taWhenever = \RDB::dispense('tag');
        $taWhenever->name = 'triggered-ability-whenever';
        $taWhenever->short='whenever';
        // $tag->level=1;
        $taWhenever->ownChildrenList = [];
        $taWhenever->parent = 'triggered-ability';
    $tAbility->ownChildrenList[] = $taWhenever;


        \RDB::store($tAbility);
        \RDB::store($taWhenever);
    }

    public function testGetChildTags(){
        $mtg = new \MTG();
        $tags = $mtg->tags('triggered-ability');
        if ($tags[2]->name=='triggered-ability-whenever')return true;
    }
    public function testGetTags(){
        $mtg = new \MTG();
        $tags = $mtg->tags();
        if ($tags[1]->name=='triggered-ability')return true;
    }


}

MTGTester::runAll();