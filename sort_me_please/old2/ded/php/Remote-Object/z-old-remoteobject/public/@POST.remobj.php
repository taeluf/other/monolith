<?php


ob_end_clean();
ob_end_clean();
// ob_end_clean();
// ob_end_clean();
// ob_end_clean();
// ob_end_clean();
// ob_end_clean();

class Mathy {

    public function add($a, $b){
        return $a + $b;
    }
}

class RemoteObject {

    protected $name;
    protected $args;
    public $obj;

    public function __construct($name,$args){
        $this->name = $name;
        $this->args = $args;

        $this->obj = new $name(...$args);
    }

    public function getMethods(){
        $ref = new \ReflectionClass($this->name);
        $methods = $ref->getMethods();
        $names = [];
        foreach ($methods as $method){  
            $names[] = $method->getName();
        }
        return $names;
    }

    public function getParams(){
        $ref = new \ReflectionClass($this->name);
        $properties = $ref->getProperties();
        $names = [];
        foreach ($properties as $property){  
            $names[] = $property->getName();
        }
        return $names;
    }

}

$data = json_decode($_POST['calls'],true);
$i = 0;
$c = count($data);
foreach ($data as $call){
    $i++;
    $isLast = ($c==$i);
    $action = $call['action'] ?? null;
    if ($action==='construct'){
        $name = $call['name'];
        $args = json_decode($call['args'],true);
        $rem = new RemoteObject($name,$args);
        $methods = $rem->getMethods();
        $params = $rem->getParams();
        $ret = [
            'action'=>$call['action'],
            'methods'=>$methods,
            'props'=>$params
        ];
        if ($isLast){
            echo json_encode($ret);
        }
    } else if ($action==='call'){

        $method = $call['method'];
        $args = json_decode($call['args'],true);
        $response = $rem->obj->$method(...$args);
        echo json_encode(
            [
                'action'=>$call['action'],
                'method'=>$method,
                'ret'=>$response
            ]
        );

    }

}












exit;