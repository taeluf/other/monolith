<!-- <script type="text/javascript" src="/RemoteObject.js"></script> -->

<script type="text/javascript">

async function doRemoteObject(){

    var mathy = await Remote.new('Mathy');
    const four = await mathy.add(2, 2);
    console.log("We just added 2 & 2!");
    console.log("2+2 = "+four);
    console.log("PHP did it!");
}


class Remote {


    constructor(){
        this.log = [];
    }
    async prepare(name,...args){
        this.name = name;

        const req = new RB.Request('/remobj/');
        // req.put('action','construct');
        // req.put('name',name);
        // req.put('args',args);
        const reqone = {
            "action": "construct",
            "name": name,
            "args": JSON.stringify(args)
        };
        this.log.push(reqone);
        req.put('calls',JSON.stringify(this.log));

        await req.handleJson(this.handleResponse.bind(this));
    }
    async send(method,...args){
        const req = new RB.Request('/remobj/');
        // req.put('action','construct');
        // req.put('name',name);
        // req.put('args',args);

        const req2 = {
            "action": "call",
            "method": method,
            "args": JSON.stringify(args)
        };
        this.log.push(req2);
        req.put('calls',JSON.stringify(this.log));

        await req.handleJson(this.handleResponse.bind(this));
        return this.ret;
    }
    async handleResponse(json){
        console.log('response');
        if (json.action=='construct'){
            for (const method of json.methods){
                this[method] = this.send.bind(this,method);
                console.log(method);
            }
            this.ret = this;
        } else if (json.action=='call'){
            console.log(json);
            // return json
            this.ret = json.ret;
        }

        return;
        // this.ret = json;
        // return json;
    }

    static async new(name,args){
        var remoteObj = new Remote();
        await remoteObj.prepare(name,args);
        return remoteObj;
    }
}

</script>

<button onclick="doRemoteObject();">Click me</button>







<script type="text/javascript">
if (typeof RB === 'undefined')var RB = {};
RB.Request = class {
    constructor(url, method){
        this.params = {};
        this.url = url;
        this.method = method || 'POST';
    }
    put(key,value){
        if (key in this.params){
            this.params[key] = (typeof this.params[key]==typeof []) ? this.params[key] : [this.params[key]];
            this.params[key].push(value);
        } else {
            this.params[key] = value;
        }
    }

    async handleJson(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        const submitData = {method: this.method, mode: "cors"};
        if (this.method=='POST')submitData['body'] = formData;
        await fetch(this.url, submitData).then(res => {
            return res.json();
        }).then(json => {
            func(json);
        });
    }
    handleText(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            body: formData
        }).then(res => {
            return res.text();
        }).then(text => {
            func(text);
        });
    }
}
</script>
<?php

exit;


?>
