<?php

class Python
{

    protected $file = null;

    protected $shell = null;
    protected $pipes = null;
    // protected $outputFile = null;
    protected $tempDir;
    protected $srcDir;

    protected $write = null;
    protected $read = null;
    public function __construct($tempDir,$srcDir)
    {
        $this->tempDir = $tempDir;
        $this->srcDir = $srcDir;
        posix_mkfifo('pypipe',0777);
        // $stream = fopen($this->tempDir.'/output.txt','w+');
        // $write = fopen($this->tempDir.'/input.txt','r');
        // $stream = fopen('php://stdin','r');

        $descriptorspec = array(
            // 0 => $stream,
            0 => array("pipe", "r"), // stdin is a pipe that the child will read from
            // 0 => $write,
            // 1 => $stream,
            // 1 => array("pipe",'w'),
            1 => STDIN,
            // 1 => array("file",$this->tempDir.'/output.txt','w+'),
            2 => array("file",$this->tempDir.'/error.txt','w+')
        );
        $cwd = $this->tempDir;
        $this->shell = proc_open("bash\n", $descriptorspec, $pipes, $cwd);
        $write = $pipes[0];
        $this->write = $write;
        // $stream = $pipes[1];
        // $this->read = $stream;
        stream_set_blocking($this->write,false);
        $this->read = $this->tempDir.'/output.txt';
        // stream_set_blocking($this->write,false);
        fwrite($this->write,"python -;\n");
        $this->outputFile = $this->tempDir.'/output.txt';
        $this->pipes = $pipes;
        if (!is_resource($this->shell)){
            throw new \Exception("Could not instantiate the python shell.");
        }

    }
    public function includeFile($filePath){
        fwrite($this->write, 'exec(open(\''.$this->srcDir.$filePath.'\').read())'."\r\n");
    }
    public function exec($command){
        fwrite($this->write, $command."\n");
    }
    public function call($pyRefId,$method,$args){
        $argString = '\''.implode('\',\'',$args).'\'';
        if (count($args)==0)$argString = '';
        // ob_start();
        // echo stream_get_contents($this->read);
        $file = $this->read;
        $command = "open('{$file}','a').write({$pyRefId}.{$method}({$argString}));\r\n";
        // echo $command."\n
        fwrite($this->write, $command);
        // $output = ob_get_clean();
        
        sleep(1);
        // echo stream_get_contents($this->read);
        echo file_get_contents($this->read);
    }

    public function new($className,...$args){
        $pyRefId = \Rbear\Strings::randomAlpha(12).'a';
        $argString = '\''.implode('\',\'',$args).'\'';
        if (count($args)==0)$argString = '';
        fwrite($this->write, "{$pyRefId} = {$className}({$argString})\r\n");
        return $pyRefId;
    }
    public function close(){
        fclose($this->pipes[0]);
        // fclose($this->pipes[1]);
        // fclose($this->pipes[2]);
        proc_close($this->shell);
    }

    public function enableAutoloader(){
        spl_autoload_register([$this,'__autoload']);
    }


    public function __autoload($class){
        $file = $this->srcDir.'/'.str_replace(['\\'],'/',$class).'.py';
        if (file_exists($file)){
            $tempClassFile = $this->tempDir.'/pyclass/'.uniqid().'.php';
            \PythonObject::$classes[$tempClassFile] = $this;
            $tempClassContent = 
            <<<PHPPYCLASS
            <?php
                class {$class} extends \PythonObject {
                    public function __construct(...\$args){
                        \$this->shell = static::\$classes['{$tempClassFile}'];
                        \$this->class = '{$class}'; 
                        parent::__construct(...\$args);
                    }
                }

            PHPPYCLASS;
            file_put_contents($tempClassFile,$tempClassContent);
            require($tempClassFile);

            $pyClassFile = str_replace('\\','/',$class).'.py';
            $this->includeFile($pyClassFile);
        }
    }

    protected function __desctruct(){
        $this->close();
    }
}
