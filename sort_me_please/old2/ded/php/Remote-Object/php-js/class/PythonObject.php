<?php

class PythonObject {

    static public $classes = [];

    protected $class;
    protected $shell;
    protected $pyRefId;

    public function __construct(...$args){
        $this->pyRefId = $this->shell->new($this->class,...$args);
    }

    public function __call($methodName,$args){
        // fwrite($this->pipes[0], $objName.'.displayEmployee()'."\n");
        // $this->shell->exec($this->pyRefId.'.'.$methodName.'()',$args);
        return $this->shell->call($this->pyRefId,$methodName,$args);
    }
}