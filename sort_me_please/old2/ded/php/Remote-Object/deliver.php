<?php

try {

    require(__DIR__.'/4-env/Env.php');
    $env = new \Env([
        'local_staging'=>false,
        'lia_debug'=>false
    ]);

    $env->setup();

    $liaison = new \Liaison();
    
    $env->prepareLiaison(get_defined_vars());

    $liaison->addPackage(['dir'=>__DIR__.'/6-FreshPackage/','name'=>'Site']);
    $liaison->addPackage(['dir'=>__DIR__.'/6-OldPackage/', 'name'=>'OldSite']);
    $liaison->addPackage(['dir'=>\JSAutowire::compoDir(),'name'=>'js-autowire']);
    $liaison->addPackage(['dir'=>\LiaViews::compoDir(), 'name'=>'lia-views']);

    $liaison->set('Resource.tempDir',__DIR__.'/6-FreshPackage/temp/');
    $liaison->set('Resource.pubDir',__DIR__.'/6-OldPackage/public/');

    $liaison->set('Site.imgDir',__DIR__.'/6-OldPackage/public/img-upload/');
    $liaison->set('Site.imgUrl','/img-upload/');
    
    $env->prepareDeliver(get_defined_vars());

    $liaison->deliver();


}
catch (\Exception $e){
    $env->error(get_defined_vars());
    exit;
} catch (\Error $e){
    $env->error(get_defined_vars());
    exit;
} catch (\Throwable $e){
    echo "There was an error that our website couldn't handle.";
    exit;
}