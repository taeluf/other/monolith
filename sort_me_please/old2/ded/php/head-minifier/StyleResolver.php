<?php

namespace Resolver;

class Style extends \Resolver\Basic{

    
    public function minifiedOutput(){
        $output = '';

        $sheetNodes = [];
        foreach ($this->nodes as $node){
            if ($node->getAttribute('rel')=='stylesheet'){
                $sheetNodes[] = $node;
            } else {
                $output.=$this->outerHtml($node);
            }
        }
        
        $fileContent = '';
        $urlMapper = new \Resolver\UrlToFile(__DIR__.'/public/');
        foreach ($sheetNodes as $index=>$node){
            // $storageDir = __DIR__.'/sheet-store/';
            $filePath = $urlMapper->getFilePath($node->getAttribute('href'));
            if (file_exists($filePath)){
                $fileContent .= '/* start stylesheet for url '.$node->getAttribute('href').' *\\'."\n\n";
                $fileContent .= file_get_contents($filePath)."\n\n";
            } else {
                $fileContent .= '/* NOT FOUND... stylesheet for url '.$node->getAttribute('href').' NOT FOUND *\\'."\n\n";
            }
        }
        $outputUrl = $urlMapper->createUrlForString('compiled.css',$fileContent);
        $output .= '<link rel="stylesheet" type="text/css" href="'.$outputUrl.'">'."\n";
        return $output;
    }
}