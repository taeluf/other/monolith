<?php

namespace Resolver;

class UrlToFile {

    protected $base;

    public function __construct($baseDir){
        $this->base = $baseDir;
    }

    public function getFilePath($forUrl){
        $path = $this->base.'/'.$forUrl;
        $path = str_replace(['////','///','//'],'/',$path);
        return $path;
    }

    public function createUrlForString($fileName,$string){
        file_put_contents($this->base.'/'.$fileName,$string);
        return '/'.$fileName;
    }
}