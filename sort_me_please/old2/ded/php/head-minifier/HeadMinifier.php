<?php

namespace Head;

class Minify {

    static protected $minifier = null;

    static public function instance(){
        return self::$minifier ?? (self::$minifier = new self());
    }

    protected $html = '';
    // protected $minifyHtml = '';

    public function minifyStart(){
        ob_start();
    }
    public function minifyEnd(){
        $this->html .= "\n".ob_get_clean();
    }
    public function getHtml(){
        $minified = $this->doMinification();
        return $minified;
    }

    protected function doMinification(){
        $tagResolvers = [
            'title'=>new \Resolver\Title(),
            'script'=>new \Resolver\Script(),
            'link' =>new \Resolver\Style(),
        ];
        $basicResolver = new \Resolver\Basic();
        $doc = new \DomDocument();
        $doc->loadHtml($this->html);
        $nodes = [];
        foreach ($doc->childNodes[1]->childNodes[0]->childNodes as $index => $node){
            $nodes[$node->nodeName] = $nodes[$node->nodeName] ?? [];
            $nodes[$node->nodeName][] = $node;
        }
        $html = '';
        foreach ($nodes as $tagName=>$nodeArray){
            $resolver = $tagResolvers[$tagName] ?? null;
            if ($resolver==null)$resolver = $basicResolver;
            $resolver->loadNodeArray($nodeArray);
            $html .= $resolver->minifiedOutput();
            $resolver->clear();
            // echo 'one loop'."\n";
        }
        return $html;
        // print_r($nodes);
        // var_dump($doc->childNodes[0]);
        // exit;
    }
}