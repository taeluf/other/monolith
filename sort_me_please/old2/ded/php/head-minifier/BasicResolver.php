<?php

namespace Resolver;

class Basic {

    protected $nodes = [];
    
    public function __construct(){

    }

    public function loadNodeArray($nodeArray){
        $this->nodes = array_merge_recursive($this->nodes, $nodeArray);

    }

    public function clear(){
        $this->nodes = [];
    }

    public function minifiedOutput(){
        $output = '';
        foreach ($this->nodes as $index=>$node){
            $output .= $this->outerHtml($node)."\n";
        }
        return $output;
    }
    public function outerHtml($node){
        return $node->ownerDocument->saveHtml($node);
    }
}