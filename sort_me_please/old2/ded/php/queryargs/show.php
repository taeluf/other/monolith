<?php


$columns = explode(',',$_POST['columns']);
$columns = array_map(function($value){return trim($value);},$columns);

$insertColumnList = '('.implode(', ',$columns).')';
$insertValuesList = 'VALUES(:'.implode(',:',$columns).')';

$updateAppend = '';
$addIdBack = FALSE;
if (in_array('id',$columns)){
    unset($columns[array_keys($columns,'id')[0]]);
    $updateAppend = ' WHERE id=:id';
    $addIdBack = TRUE;
}

$updateColumnList = array_map(function($column){return $column.'=:'.$column;},$columns);
$updateColumns = "SET ".implode(',',$updateColumnList).$updateAppend;

if ($addIdBack)$columns[] = 'id';

$colPointers = array_map(function($column){return "        ':{$column}' => \$object->{$column},\n";},$columns);
$executeArray = "[\n".implode('',$colPointers)."\n    ]";
// $executeArray = "[\n        ':".implode("' => \$,\n        ':",$columns). "' => \$,\n    ]"; 

$phpCode = 
<<<PHP
public function (\$object){
    \$object = ;
    \$table = ;
    \$pdo = \$this->config->pdo;
    \$query = '';
    if (\$object->id===NULL){
        \$query = "INSERT INTO {\$table}{$insertColumnList} {$insertValuesList}";
    } else {
        \$query = "UPDATE {\$table} {$updateColumns}";
    }
    \$statement = \$pdo->prepare(\$query);
    \$statement->execute({$executeArray});
    \$object->id = \$pdo->lastInsertId();
}
PHP;


echo '<pre>';
echo $insertColumnList;
echo "\n";
echo $insertValuesList;
echo "\n";
echo $updateColumns;
echo "\n";
echo $executeArray;
echo "\n\n";
echo $phpCode;



echo '<pre>';

?>