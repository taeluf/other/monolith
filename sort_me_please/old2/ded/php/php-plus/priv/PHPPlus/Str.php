<?php

namespace PHPPlus;

class Str {

    protected $str;

    public function __construct($string){
        $this->str = $string;
    }

    public function replace($searchFor, $replace){
        $this->str = str_replace($searchFor, $replace, $this);
        return $this;
    }


    public function __toString(){
        return $this->str;
    }
}