<?php



class CallApi {

    static public function specials($charName){
        $specials = [
            'Bob'=>["cats","dogs","babies"],
            "Susan"=>["Kangaroos","cats",'Elephants'],
            "Karen"=>['nope'],
            'George'=>['unpredictable','sneak','party'],
            'Julia'=>['fly','hover','yes']
        ];
        return $specials[$charName];
    }
    static public function retrieveMoves($charName){
        return [
            "punch",
            "kick",
            "plunder",
            "{$charName} SPECIAL"
        ];
    }
}









$data = $_POST;
$method = $data['method'];
unset($data['method']);
$class = $data['class'];
unset($data['class']);
$args = [];
foreach ($data as $key=>$arg){
    if (!is_numeric($key))throw new \Exception("Only numeric keys");
    $args[$key] = $arg;
}
ksort($args);

$apiCaller = new \APICaller($class,$method,...$args);
$result = $apiCaller->resolve($event);
// $result = call_user_func(['CallApi',$method],...$args);
if ($method=='getForm'){
    $ft = new \FormTools($result);
    $ft->SchemaPrepareForm();
    $result = $ft->innerBodyHTML();
}

$ret = [
    'methods'=>[
        $method=>$result
    ]
];
echo json_encode($ret);


exit;