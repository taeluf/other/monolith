<?php

class APICaller {

    protected $args;
    protected $class;
    protected $function;

    public function __construct($class,$function,...$args){
        $this->class = $class;
        $this->function = $function;
        $this->args = $args;
    }
    public function resolve($event){

        $class = $this->class;
        $func = $this->function;
        $args = $this->args;
        $args[] = $event;
        $result = $class::$func(...$args);
        return $result;
    }
}