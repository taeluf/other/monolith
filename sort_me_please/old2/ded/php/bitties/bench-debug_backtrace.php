<?php

class Caller {


    public function tryIt($theId){
        return $theId.'-'.uniqid();
    }
    public function tryItTrace($theId){
        if (\Permissions::canAccess()){
            return $theId.'-'.uniqid();
        }
    }
}

class Runner {

    public function goControl(){
        $i=0;
        $stack = [];
        while ($i++<1000000){
            $caller = new Caller();
            $stack[] = $caller->tryIt(uniqid());
        }
        return $stack;
    }

    public function goTrace(){
        $i=0;
        $stack = [];
        while ($i++<1000000){
            $caller = new Caller();
            $stack[] = $caller->tryItTrace(uniqid());
        }
        return $stack;
    }
}

class Permissions {

    static public function canAlwaysAccess(){
        return true;
    }
    static public function canAccess(){
        $backtrace = debug_backtrace();

        // var_dump($backtrace);
        // exit;
        if (strlen($backtrace[1]['args'][0])>3
            &&$backtrace[0]['file']=='/var/www/html/dev/dumb/bench-debug_backtrace.php'
            &&$backtrace[1]['class']=='Caller'){
            return true;
        }
        throw new \Exception("We failed");
    }
}



$trace = new stdClass;
$trace->start = microtime(true);


$runner = new Runner();
$stack1 = $runner->goTrace();
$stack1 = array_slice($stack1,random_int(0,40),random_int(50,100));



$trace->end = microtime(true);

$trace->diff = floatval($trace->end - $trace->start);


///
///
///

$control = new stdClass;
$control->start = microtime(true);

$runner = new Runner();
$stack2 = $runner->goControl();
$stack2 = array_slice($stack2,random_int(0,40),random_int(50,100));


$control->end = microtime(true);

$control->diff = $control->end - $control->start;





echo "Control:\n";
var_dump($control);
echo "\n\nTrace:\n";
var_dump($trace);

print_r($stack1);
print_r($stack2);