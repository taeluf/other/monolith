
/**
 * This class SHOULD extend DOM.WireContext & be used as part of the view... or be renamed
 * init.js is dependent upon this class, so it's listed in the background scripts...
 * The class init.js needs is not actually the download picker... i don't think.
 */
class DownloadPicker {
    constructor(downloadItem){
        this.downloadItem = downloadItem;
    }
    show(){
        const request = new RB.Request('popup/DownloadPicker.html');
        request.handleText(function(text){
            console.log(document);
            const frame = document.createElement('div');
            frame.innerHTML = text;
            document.body.appendChild(frame);
            const sheets = document.styleSheets;
            let sheet;
            for (sheet of sheets){
                sheet.ownerNode.href = sheet.href;
            }
            const scripts = document.scripts;
            // console.log(scripts);
            let script;
            for (script of scripts){
                script.setAttribute('src',script.src);//.src = script.src;
            }
            // console.log(frame.innerHTML);
            document.body.removeChild(frame);
            const tab = browser.tabs.query({active:true})
                .then(function(tabs){
                    console.log(tabs);
                    const tab = tabs[0];
                    console.log(tab);
                    browser.tabs.sendMessage(tab.id,{'addHtml':frame.innerHTML,'downloadItem':this.downloadItem})
                        .then(function(tabs){
                            console.log('message sent?');
                        });
                    
                }.bind(this));
            console.log(tab);
            // const outputHtml = frame.parentNode.innerHTML;
            // // document.body.removeChild(frame);
            // console.log(outputHtml);
            // console.log(document);
        }.bind(this));
        // console.log(window);
        // console.log(document);
        // const page = browser.runtime.getBackgroundPage()
        //     .then(function(page){
        //         console.log(page);
        //         console.log(windo)
        //         page.foo();
        //         // const request = new RB.Request(page);
        //         // request.handleText(text=>console.log(text));
        //     });
        
        // console.log(page);
        // browser.tabs.query({active:true})
        // .then(this.showPopup.bind(this));
        // this.showPopup(browser.tabs.getCurrent());
        // const popup = browser.browserAction.getPopup({});
        // popup.then(this.showPopup.bind(this));
        // console.log(popup);
        // const div = document.createElement('div');
        // div.innerText = "AM A DIV SHOULD SHOW SOMEWHERE";
        // document.body.innerHTML = '';
        // document.body.appendChild(div);
    }
    showPopup(tabs){
        // console.log(tabs);
        // browser.pageAction.show(tabs[0].id);
        // browser.browserAction.openPopup();
        // console.log('setupopup');
        // browser.browserAction.setPopup({
        //     popup:popupUrl,
        //     windowId:window.id
        // });
    }
}