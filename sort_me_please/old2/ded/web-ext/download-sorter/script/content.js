browser.runtime.onMessage.addListener(request => {
  try {
    console.log('received message');
    console.log(request.addHtml);
    const existing = document.getElementById("downloadBookmarks");
    if (existing)existing.parentNode.removeChild(existing);
    const wrapper = document.createElement('div');
    wrapper.id = "downloadBookmarks";
    wrapper.innerHTML = request.addHtml;
    document.body.appendChild(wrapper);
    console.log(wrapper);
    console.log(window.readyState);
    console.log(window);
    const bmarks = document.querySelectorAll('.DownloadBookmark');
    for (const bmark of bmarks){
      bmark.setAttribute('data-url',request.downloadItem.url);
      bmark.setAttribute('data-filename',request.downloadItem.filename);
    }
    DownloadBookmark.autowire();
    // console.log(bmarks[0]);
    // DownloadBookmark.wireNode(bmarks[0]);

  } catch (e){console.log(e);}
    return Promise.resolve({html:request.addHtml})
  });

  console.log('did content');