# Downloads Sorter
Goal: To make download directory selection a 1-click job.
Features:
- When a file is downloaded, show a view with a quick selector
- Quick selector should show a few (6) bookmarked directories
- Quick selector may show recently used directories as well
- After quick-selection is made & download completed, another view shows asking: Open directory, Open File, Cancel. This view fades after 10 seconds
- An "add bookmark" button right on the popup quick selector
- Right click on a bookmark to remove it
- There is no separate settings page
- Bookmarks can be viewed by clicking browser toolbar icon (even if no downloads pending)
- A 'recent downloads' button to view recent files downloaded

Crucial feature:
- A quick-selector to show 6 bookmarked directories
- An 'add bookmark' button
- right click (or drag to trashcan) to remove bookmark

How:
1) Figure out how to respond to the download action
2) Interrupt the default behavior
3) load the HTML view (using the Popup extension feature)
    a) Load bookmarks from local storage
    b) Load each of them into the view
5) Register an onclick for each bookmark 
    a) Only respond to right-click
    b) Pop up simple confirmation "Delete bookmark?"
4) register an onclick for the 'Add Bookmark' button which will show the system's file selector
    a) Store the new bookmark in local storage & refresh the popup view