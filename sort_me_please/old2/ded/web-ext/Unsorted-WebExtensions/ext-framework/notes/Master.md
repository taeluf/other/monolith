# Browser Extension Framework
Vision: Browser extension are fun for new, inexperienced extension-developers, to engineer
Mission: To provide a structure and methods that simplify the extension-building process
Goals:
    1) Define a directory structure
    2) Provide a mechanism for loading any file type into an existing document (browser tab)
    3) Provide a UI-loading mechanism, so view files can be loaded into a document & shown (likely as a popup)
    4) Make dependency-loading available for scripts and html files
    5) Build example extensions that utilize the framework (like the Stardew & Downloader Bookmarks)
    6) Publish the framework
    7) Continue developing features for it
        a) cross-browser pollyfills
        b) Allow content-scripts to access background-scripts or other out-of-context scripts, withOUT using the messaging api (how?)
        c) Expose background scripts to content-scripts (case-by-case?)
        d) Integrate my Auto-wire framework with it


# Contexts
There are several contexts when working with a WebExtension. This functionality is made much more challenging by the multiple contexts. 

The contexts include:
- background_scripts - they all have access to previously listed bg scripts (from the manifest)
- content_scripts - has access to previously listed content_scripts which run on this same page
- the-page - Scripts loaded into a page have access to other scripts found within the page.

Pain point:
- content_scripts can NOT access variables declared in the page. My current strategy for `require`ing a script is to added a `<script>` tag to the page. I'm wondering... do content_scripts have a backgroundPage? Looks like no
