# Design Spec for a WebExtensions framework  
**Gist:** This WebExt framework will provide a set of features for more easily working with documents, views, and dependencies.  

**Contexts:** Three contexts will be considered here: `background_scripts`, `content_scripts`, & `web_page` (original document on which content_scripts run). Scripts already existing on or inserted into web_page are in a different context than code inside content_scripts. Other contexts, such as sidepanels and popups will not be considered in the initial spec, as the primary goal is to work in the context of a web page.  

**Features:**  
    - a `require(relFilePath)` function will load `.js` or `.css` files into the web_page context if called from content_scripts, or into the background_page if called from background_scripts  
    - a `show(viewPath, tabId?)` function will load a `.html` view into the associated `web_page` context if called from content_scripts. If called from background_scripts, the html view will be added to either: the active tab of the active window, or the tab specified by tabId  
    .... - The `ViewName.html` file (specified w/ viewPath) will often specify resources like scripts, stylesheets, or images with regular html syntax. The view will additionally be wrapped in a `div` with css `class="rbear-view-wrapper"` before being appended to `document.body`  
    .... - The relative url paths listed in the `.html` view will be re-written with the extension prefix before being added to a web_page  
    - `require_once` and `show_once` functions will do the same, except only the first time they're called for the given paramaters  

**Notes:**  
    - All loadable dependencies MUST be listed as `web_accessible_resources`. `/dir/*` is a convenient way to list multiple dependencies.  
    - a `content_script` must specify it's dependencies in the `manifest.json`, and MUST include the dependency upon the framework  
    - a `background_script` SHOULD specify it's dependencies in the `manifest.json`, and MUST include the dependency upon the framework  
    - a `web_page` can only define dependencies through a `view` or it's associated `content_script` (calling `require` from c_s)  
    - By design, the framework is not available in the web_page context. One can call `require` from `content_scripts` to make the framework available in the web_page. This functionality MIGHT work anyway, but it will not be tested or expected to work   
    - To call `view` from `background_script`, The `activeTab` permission MUST be added the `manifest.json`  
    .... - Research Needed: An additional permission MAY be required when working with a specific `tabId`, instead of the `activeTab`

**Potential Eventual Features:**  
    - A custom events system  
    - call `insert` to add a string to the document body  
    - call `insertHtml` to add html to the body  
    - Allow other file types to be included in a document with `require`  
    - a `getFile` function which returns the text-content of a file. Given the async nature of things... This will probably have to be done with a callback  
    - `background_script` can insert dependencies into any `web_page`  
    - Remove dependency upon `activeTab` permission by adding a message handler to `content_scripts` to handle `background_script` calls to `view`  
    - add a `require_on_web_page` function, which can be called from the `background_script` to add dependencies to any `content_script` or any tab.  
    - Extend functionality and testing to additional contexts, such as sidepanels and popups

**Unlikely, but considered features:**  
    - call `run` from a `content_script` to execute a function in it's `web_page`. This is a very much desireable feature, but it may not be possible.  
    - dependency loading into the `content_script` scope.  
    .... - I don't think I can programmitically add a script to the content_scripts scope. I have tried `eval`, messaging to use browser.tabs.executeScript() on a background_script (which also requires an addiitonal permission). When I add the `<script>` to the `document`, it is available in the `web_page` context, not the `content_script` context.  
