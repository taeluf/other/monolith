

function focusSearch(){
        const searchInput = document.getElementById("searchInput");
        const st = window.scrollTop;
        const sl = window.scrollLeft;
        searchInput.focus();
        window.scroll(st,sl);
};
function doSetup(){
        const cssUrl = chrome.runtime.getURL('/style/panel.css');
        loadStyle(cssUrl);

        const viewUrl = chrome.runtime.getURL('/view/panel.html');
        const request = new RB.Request(viewUrl);
        request.handleText(function(text){
                try {
                        const div = document.createElement('div');
                        div.innerHTML = text.trim();
                        document.body.appendChild(div);
                        let form = document.getElementById("searchform");
                        // form = form.cloneNode(true);
                        if (form.parentNode!=null)form.parentNode.removeChild(form);
                        const wikiPanel = document.getElementById("taeluf_wiki_search");
                        wikiPanel.appendChild(form);
                        focusSearch();
                } catch (e) {
                        console.log(e);
                }
        });
}
window.addEventListener("focus",focusSearch);
document.body.addEventListener("load",doSetup);
doSetup();


























// console.log(panel);
// console.log('---');
// console.log(browser.extension.getViews());
// console.log(browser.extension.getBackgroundPage('/view/panel.html'));
// console.log(browser.runtime.getBackageDirectoryEntry().then(d=>{return d;}));