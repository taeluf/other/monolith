function focusSearch(){
    const searchInput = document.getElementById("searchInput"); //pre-existing component on the StardewWiki
    searchInput.focus();
};
function doSetup(){
        const cssUrl = browser.runtime.getURL('/style/panel.css');
        loadStyle(cssUrl);

        const viewUrl = browser.runtime.getURL('/view/panel.html');
        const request = new RB.Request(viewUrl);
        request.handleText(function(rawHtml){
            loadHtmlString(rawHtml);
            const form = document.getElementById("searchform"); //pre-existing component on the StardewWiki
                form.parentNode.removeChild(form);
            const wikiPanel = document.getElementById("rbear_wiki_panel"); //prefix, to avoid collisions with resources on the site.
                wikiPanel.appendChild(form); //moved the searchform from the page as normal over to the floating panel
            focusSearch();
        });
}
window.addEventListener("focus",focusSearch); //so the search bar gets focus every time the page is focused
document.body.addEventListener("load",doSetup); //in case the body isn't loaded yet... 
doSetup(); //the body probably is already loaded, so 'load' won't be called. This is a bad approach.