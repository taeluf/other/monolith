function loadStyle(href){
    // avoid duplicates
    for(var i = 0; i < document.styleSheets.length; i++){
        if(document.styleSheets[i].href == href){
            return;
        }
    }
    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = href;
    head.appendChild(link);
}
function loadHtmlString(html,parentNode = 'document.body'){
    if (parentNode==='document.body')parentNode = document.body;
    var wrapper  = document.createElement('div'); //you could use a span here, if you want inline-styling, or customize styles through js. 
    wrapper.innerHTML = html;
    parentNode.appendChild(wrapper);
}