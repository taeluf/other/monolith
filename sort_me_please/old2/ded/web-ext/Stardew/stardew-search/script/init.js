

function focusSearch(){
        const searchInput = document.getElementById("searchInput");
        searchInput.focus();
};
function doSetup(){
        const cssUrl = browser.runtime.getURL('/style/panel.css');
        loadStyle(cssUrl);

        const viewUrl = browser.runtime.getURL('/view/panel.html');
        const request = new RB.Request(viewUrl);
        request.handleText(function(text){
                try {
                        const div = document.createElement('div');
                        div.innerHTML = text.trim();
                        document.body.appendChild(div);
                        const form = document.getElementById("searchform");
                        form.parentNode.removeChild(form);
                        const wikiPanel = document.getElementById("wiki_panel");
                        wikiPanel.appendChild(form);
                        focusSearch();
                } catch (e){console.log(e);}
        });
}
window.addEventListener("focus",focusSearch);
document.body.addEventListener("load",doSetup);
doSetup();


























// console.log(panel);
// console.log('---');
// console.log(browser.extension.getViews());
// console.log(browser.extension.getBackgroundPage('/view/panel.html'));
// console.log(browser.runtime.getBackageDirectoryEntry().then(d=>{return d;}));