<?php

ini_set('display_errors',true);
error_reporting(E_ALL);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

$url = $_SERVER['REQUEST_URI'];
$url = parse_url($url, PHP_URL_PATH);

if ($url=='/save/'){
    
    echo $_POST['marks'];
    $json = $_POST['marks'];
    $new = json_decode($json,true);
    $existing = file_get_contents(__DIR__.'/marks.json');
    $old = json_decode($existing,true);
    $final = array_replace_recursive($old,$new);
    $count = (count(scandir(__DIR__.'/old/'))-2);
    if ($count>10)$count = 0;
    rename(__DIR__.'/marks.json', __DIR__."/old/{$count}-marks.json");
    file_put_contents(__DIR__.'/marks.json',json_encode($final,JSON_PRETTY_PRINT));
} else if ($url=='/load/'){
    $json = file_get_contents(__DIR__.'/marks.json');
    echo $json;
}