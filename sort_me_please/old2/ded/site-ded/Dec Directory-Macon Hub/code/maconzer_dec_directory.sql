-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 13, 2020 at 11:21 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

--
-- Database: `maconzer_decatur_directory`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth`
--

CREATE TABLE `auth` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expiresAt` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth`
--

INSERT INTO `auth` (`id`, `user_id`, `name`, `data`, `expiresAt`) VALUES
(4, 1, 'password', '$2y$10$1OenN/4JrbMbqoC5QQz4Ce8HA1sRZnCubl4zfFoY5Wx1/RwfuJfgW', 1578085017),
(2, 1, 'cookie', '%242y%2410%24YV%2FNdYbOpVS2oXFoGlRtdu4.5NH%2FLJ7yP37rZyENzA7b..YLZchy2', 0),
(3, 1, 'cookie', '%242y%2410%24.vdmOIHCi4mRaWrxQjjzhebkaFmXPCtpqL4eFjcY.3DHZkC8Mhj7.', 0),
(5, 1, 'cookie', '%242y%2410%24Rnul4eb7oAN2hnmG3YBsheBIL.prLg%2F4G%2FlQQWC3UZ9LSUMSeAila', 0),
(9, 1, 'cookie', '%242y%2410%24ivud8I9xO0kgHp7exMwFuu%2Fiml%2FYlGAZPG%2FLig9FT2DdW5XqgDJAy', 1549143148),
(10, 1, 'cookie', '%242y%2410%245jPbxCIJGMAxi76DMmumgeE.pwzSDKlNMglj57pzU8J3MEGSORkum', 1549143246),
(11, 1, 'cookie', '%242y%2410%24BRmUALEQ8PsdoJdAFykFgum8Q7CRYLl1NFDpkgSWP%2FOppLJLjNxkG', 1549143562);

-- --------------------------------------------------------

--
-- Table structure for table `Breadcrumbs`
--

CREATE TABLE `Breadcrumbs` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `shortName` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bread_biz`
--

CREATE TABLE `bread_biz` (
  `breadcrumb_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Business`
--

CREATE TABLE `Business` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `shortUniqueName` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `schemaType` text COLLATE utf8_unicode_ci NOT NULL,
  `primaryImageUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `blurb` text COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `streetAddress` text COLLATE utf8_unicode_ci NOT NULL,
  `city` text COLLATE utf8_unicode_ci NOT NULL,
  `state` text COLLATE utf8_unicode_ci NOT NULL,
  `zipCode` int(11) NOT NULL,
  `companyName` text COLLATE utf8_unicode_ci NOT NULL,
  `companyId` int(11) DEFAULT NULL,
  `phoneNumber` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `contactUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `officialUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `facebookUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `menuUrl` text COLLATE utf8_unicode_ci NOT NULL,
  `yelpUrl` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Business`
--

INSERT INTO `Business` (`id`, `name`, `shortUniqueName`, `schemaType`, `primaryImageUrl`, `blurb`, `description`, `streetAddress`, `city`, `state`, `zipCode`, `companyName`, `companyId`, `phoneNumber`, `email`, `contactUrl`, `officialUrl`, `facebookUrl`, `menuUrl`, `yelpUrl`) VALUES
(1, 'Frank\'s Chicago Grill', 'FranksChicagoGrill', 'https://schema.org/LocalBusiness', 'https://scontent-ort2-2.xx.fbcdn.net/v/t1.0-9/48416028_1635169306789771_4536382790618316800_n.jpg?_nc_cat=107&_nc_ht=scontent-ort2-2.xx&oh=864f0d628c222f68ab5ab6c0e00ddddc&oe=5CCB31BB', 'It\'s a Franks! EDIT 2', 'Frank\'s Chicago Grill is home to franky foods EDIT 2... EDIT 3, now!!!', '824 W Eldorado St', 'Decatur', 'IL', 62522, '', NULL, '2174226060', '', '', '', 'https://www.facebook.com/pages/Frank-Chicago-Grill-in/263183607777665', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `hours`
--

CREATE TABLE `hours` (
  `id` int(11) NOT NULL,
  `datetime` text COLLATE utf8_unicode_ci NOT NULL,
  `display` text COLLATE utf8_unicode_ci NOT NULL,
  `biz_id` int(11) NOT NULL,
  `weight` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `Image`
--

CREATE TABLE `Image` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `biz_id` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `email`) VALUES
(1, 'Reed Sutman', 'rsutman@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `access_level`) VALUES
(1, 1, 20001);

-- --------------------------------------------------------

--
-- Table structure for table `Tags`
--

CREATE TABLE `Tags` (
  `id` int(11) NOT NULL,
  `name` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `Tags`
--

INSERT INTO `Tags` (`id`, `name`, `description`) VALUES
(1, 'vegan', 'A business with this tag is likely a restaurant, but may be a grocery store. This means they serve or sell vegan food. It does not indicate that they are exclusively vegan. If I added events, a vegan-potluck would get it too!\r\n\r\nTechnically almost every restaurant has vegan options, but a place like Diamonds, which literally has one vegan choice on the menu, would not get the vegan tag, as it really does not fit their primary focus. Head West or Donnie\'s may get the vegan tag, as they have a reasonably good selection of vegan food.'),
(2, 'vegetarian', 'Any establishment that has a good selection of or specializes in vegetarian food and products. This is likely a restaurant, but may be a grocery store. If you want to stretch it, it could apply to things like beauty stores, but cruelty-free would be a better tag for that.'),
(3, 'cruelty-free', 'This represents a store that has a good selection of cruelty-free items. For example, a make-up store that carries a large selection of products that have not been tested on animals would get this tag.'),
(4, 'breakfast', 'for restaurants that have a good selection of breakfast food'),
(5, 'lunch', 'for restaurants with a good selection of lunch foods'),
(6, 'dinner', 'for restaurants with a good selection of dinner food.'),
(7, 'health', 'For any establishment that is strongly focused on health, such as a health-food store, a vitamins store (regardless of controversy, that is what they market), a gym, etc'),
(8, 'parties', 'an establishment that hosts parties, such as the DISC, or Daugerties'),
(9, 'local', 'represents a locally owned establishment, such as Board Knight or Wildflower'),
(10, 'corporate', 'represents a business that is owned by a large corporate entity, such as Walmart, Target, Kroger'),
(11, 'regional', 'represents a company that is regional, such as Kroger, Krekels, or Steak \'N Shake (if memory serves)'),
(12, 'chicago-style', 'for chicago-style food serving establishments'),
(13, 'mexican', 'For mexican cuisine and specialty stores'),
(14, 'oriental', 'for oriental food, such as thai, indian, chinese '),
(15, 'chinese', 'for chinese food'),
(16, 'american', 'for american food'),
(17, 'thai', 'for thai food');

-- --------------------------------------------------------

--
-- Table structure for table `tags_biz`
--

CREATE TABLE `tags_biz` (
  `id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `biz_id` int(11) NOT NULL,
  `weight` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags_biz`
--

INSERT INTO `tags_biz` (`id`, `tag_id`, `biz_id`, `weight`) VALUES
(18, 5, 1, NULL),
(19, 6, 1, NULL),
(20, 9, 1, NULL),
(32, 12, 1, NULL),
(34, 2, 1, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth`
--
ALTER TABLE `auth`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Breadcrumbs`
--
ALTER TABLE `Breadcrumbs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shortName` (`shortName`);

--
-- Indexes for table `Business`
--
ALTER TABLE `Business`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shortUniqueName` (`shortUniqueName`);

--
-- Indexes for table `hours`
--
ALTER TABLE `hours`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Image`
--
ALTER TABLE `Image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `Tags`
--
ALTER TABLE `Tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tags_biz`
--
ALTER TABLE `tags_biz`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag_id` (`tag_id`,`biz_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth`
--
ALTER TABLE `auth`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `Breadcrumbs`
--
ALTER TABLE `Breadcrumbs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Business`
--
ALTER TABLE `Business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `hours`
--
ALTER TABLE `hours`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Image`
--
ALTER TABLE `Image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Tags`
--
ALTER TABLE `Tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tags_biz`
--
ALTER TABLE `tags_biz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;


















