# rof/change-decatur
Tell me a little bit about your package. What does it do? What needs does it fill? This bit should help your package show up in search results and catch the attention of developers... or your target user base.

## Notes
- This library is built with [PHP Tiny](https://github.com/ReedOverflow/PHP-Tiny-MVC/)
- Placeholder for something else important

## Features
- Feature One - DESCRIPTION
    - sub-feature - DESCRIPTION
        - EXTRA DETAILS (if needed)
    - sub-feature2 - DESCRIPTION
- Feature Two - DESCRIPTION
    - sub-feature - DESCRIPTION
        - EXTRA DETAILS (if needed)
    - sub-feature2 - DESCRIPTION

## Install
In your composer.json, put:
```json
{
    "require": {
        "rof/change-decatur": "PACKAGE VERSION"
    }
}
```
Then you can call `composer install` or `composer update` from your CLI (command line interface).    
---  
Alternatively, from your CLI, run:
```bash
composer require rof/change-decatur:version
```  
---  
**without composer** - `autoload.php` may need to be edited to control for complex namespace => directory mappings.
1. copy all the files from this repository into a SOURCE_DIRECTORY of your choice. 
2. call `include(SOURCE_DIRECTORY.'/src/php/autoload.php);`
3. Follow the *install without composer* instructions for [PHP Tiny](https://github.com/ReedOverflow/PHP-Tiny-MVC/).
4. Look at composer.json for this library and install all of the `require`d packages manually.
5. Get frustrated and start using composer because this process is terrible.

## Quick Start
For default functionality, simply call the following from a script you've set up to route requests:
```php
/** You only pass a configuration array the first time you call CD\Tiny\Config::getInstance()
 * 
 */
CD\Tiny\Config::getInstance(
    [   "pdo" => new \PDO(...),
        "fun-statement" => "cats are the best",
        //these are placeholder settings and should be changed
    ]
);
$router = CD\Tiny\Router::getInstance();
if ($router->isRequested())$router->enforceCleanUrl();
/** you should have some kind of template engine, which this gets printed into.
 */
echo $router->getView();
```
Now you can visit `/category-name/` on your web server to view the index page.

## License
This package is licensed under **proprietary**.

## Versions
- Version 2.0.0 - major new version
    - breaking changes
    - added features
    - probably the best things ever
- Version 1.0.1 - bug fix
    - Fixed the Flarm-bull-goof bug
- Version 1.0.0 - release
    - a real version, ready for release. Things work!
- Version 0.1.0 - alpha
    - everything was broken, bad, and unusable