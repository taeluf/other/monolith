<?php 

    $need = $this->obj; 
    $router = $this->config->router;
    $ideaUrl = $router->getPageUrl('needs',$router->getSlug(),'AttachIdeas');
    $printIdeas = 
        function($slug){
            $idea = \CD\Idea::fromSlug($slug);
            echo new \CD\Tiny\View('View/ideas/ListItem.php',$idea);
        };
?>
<a href="<?=$this->config->router->getPageUrl('sectors',$need->sector);?>"><?=$need->sector?></a><br>
<h1><?=$need->title?></h1>
<p><?=$need->description?></h1>

<h2>Ideas</h2>
<?php array_map($printIdeas,$need->ideas);?>
<br>
<a href="<?=$ideaUrl?>"><button>Add Ideas</button></a>

<!-- 

<h2>Claims</h2>
<p>here goes a list of claims related to this need</p>
<a href="<?=$router->getPageUrl('claims',NULL,'create')?>?need=<?=$need->slug?>"><button>Add Claim</button></a>

<h2>Solutions</h2>
<p>Here goes a list of possible solutions for this need</p>
<a href=""><button>Add Solution</button></a>

<h2>Problems</h2>
<p>Here goes a list of problems that are driving this need</p>
<a href=""><button>Add Problem</button></a>



<hr><hr>
<h1>Some of this may be useful still....</h1>
<h2>Need</h2>
<p>(What need is this idea related to?)</p>
<p><a href="/needs/create/">Create new need</a></p>


<h2>Solutions</h2>
<p>(Solutions should come from the Need, not the ideas)</p>

<h2>Supporting Data</h2>
<p>(What data supports this idea?)</p>
<p>(What about Supporting data for similar ideas?)</p>


<h2>Related ideas</h2>
<p>(A list of ideas that are the same, or very similar to this one)</p> -->