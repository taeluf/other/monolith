<?php

echo 'project addThread needs to be built. The function is declared in Loader, but it does not actually add a thread.';
echo "\nI need to create the project_threads table and subsequently add the insert statement/query to the save(...) function.";
echo "\nProject->addThread(...) is actually written. I guess it is the save function that needs to be worked on.";

$loader = $this->config->loader;
$project = $loader->projectFromId($_POST['projectId']);
$thread = $loader->threadFromId($_POST['threadId']);
$project->addThread($thread);
$thread->setProject($project);
$project->save();
$thread->save();


?>