<h1>Let's Change Decatur Together</h1>

<h2>What's your idea?</h2>
<?=new \CD\Tiny\View('View/ideas/Create.php');?>

<h2>Join the conversation</h2>
<?php
    $loader = \CD\Tiny\Loader::getInstance();
    $needs = $loader->getIdeas(10);
    $needsPrinter = function($need){
        $view = new \CD\Tiny\View('View/ideas/ListItem.php',$need);
        $view->obj = $need;
        echo $view;
        echo '<hr>';
    };
    array_map($needsPrinter,$needs);
?>



<h3>What I want</h3>
<pre>
<h1>Things I want on this page include</h1>
<fieldset>
    the ability to post a new thread
        - ideas / questions / complaints / discussions
        - Some examples:
            I want to ask "r decatur schools integrated?" // question, discussion
            I want to post "decatur schools need non-binary bathrooms" //suggestion, idea, declaration, need
            I want to post "we need to integrate decatur schools" //suggestion, idea, declaration, call to action ("we need")
            ... "let's get a marijuana dispensary" //suggestion, idea, declaration, call to action ("let's get")
            What if somebody posted "We need to segregate our buses" //suggestion, idea, declaration, call to action ("we need to"). Discriminator & gets deleted/closed
            "I think segregation is a bad thing" //thought, discussion
            "I called the police about a man with a gun and they took 2 hours to get to me" //complaint, discussion, seeking justice/retribution/solution (otherwise a complaint is useless).
        - Send notifications to users
        - Add to the general feed
        - Add to the user's profile
        

    Feeds
        - Recommended (personalized)
        - Popular (based upon activity, votes??)
        - New (recently created or not-yet active)
        - Sorted (popular and/or new ideas sorted by need)
    Sectors
        - a summary of each (or some) sector(s)
        - links to a sector page
    Articles
        - a feed of recent articles, all written about Decatur. (Ex: Posting a summary of Decatur's city council meeting)
    Events
        - a feed of upcoming events, likely ordered by how soon they happen
</fieldset>


Then I have questions. Some which may not need to be answered right now.
    Ideas are posts & can be commented on & appear in the post feed.
        Are Needs also posts? 
                It's an aggregate. A single need contains many ideas. Each idea contains many comments. The ideas serve as communication points. The needs cannot be commented on directly. There may be some kind of... administrative thing. Like, the ability to suggest an improvement to a need. But if you want to comment on a need, you'll have to create a discussion about the need. Is an idea a discussion about a need? Not necessarily. But yeah, it could be. So, then, I may need to rethink the idea of 'Idea'.
            Can they be commented on directly? 
                No. Not really. Maybe the ability to suggest an improvement, but perhaps that will serve as a different 'post' type. I think... whether I launch with multiple post types or not, I need to include forward-thinking capabilities for different post types. 
            Do they appear in the feed?
                Probably not. It depends on how I aggregate things. In the feed, am I showing individual ideas? Do I want to show different conversations amongst a need? 

        Are Sectors also posts? No. They're semi-static categories
            Can they be commented on directly?  Yes?? Maybe??
            Do they appear in the feed? No.


On ideas / questions / discussions (posts):
Each of these three will be... basically the same thing. I like that StackOverflow model of Question with Answers and then each question/answer can be commented upon for further discussion. I'm trying to think if that is a good model for this site. 

Are Decatur Schools segregated? (some rambling & explanation)
    Comments
        - you can look at illinoisreportcard.com
        - reach out to xyz organization
    Answers
        Decatur schools ARE segregated. There are 27% more white kids than black kids in the successful public schools. There are 80% white kids in private schools. Here is my data
            Comments
                -It's actually 87% white in 2018. See [link](http://)
        Decatur schools are NOT segregated. There isn't an exact match of white and black kids, but most of the schools are pretty integrated. The main problem is with the lower performance among black students.
            Comments
                - The 27% disparity mentioned by @abcAnswerer looks like segragation to me
                - disagree with you
                - but yeah, that kinda makes sense

            
This style gives the opportunity for complex answers to complex question. It also allows for threading. It makes comment-threading a little bit complicated though. What if you want to branch off amongst a comment. The Facebook model for commenting is pretty darn good. Anyone can comment on a post. Then to engage on a particular comment, you reply to it. Further "reply-tos" are not threaded, rather there are comments and sub-comments, and nothing deeper. I'm suggesting Questions with comments and answers with comments. It's kind of threaded, but not the same as Facebook. It might be easier...
I'm thinking about there being threads. Basically, an Answer is a thread. Each thread has attached to it some data (like a description, title, creator, editors??). Comments are on threads. a question is a thread which can have many sub-threads (answers). An answer is a thread which will not have sub-threads, but will have comments.


The complex thing is... I want to create this social component. I want discussion to be the lifeblood of the site. From discussions come actions. Ideally. 



"I called the police about a man with a gun and they took 2 hours to get to me" //complaint, discussion, seeking justice/retribution/solution (otherwise a complaint is useless).
    for this complaint there a responsible party, The Decatur Police Department
        A proposed solution would be to improve the resposne time of the police to gun calls
        A proposed action would be to write up the 911 operator who didn't mark it with the correct priority

So, here's another time where having the threads (question / answer) would be useful. The police department, for example, could leave an answer saying "We made a mistake. we're sorry & xyz action has been taken." then people can comment on the police's response. But then should comments be threaded as well? I don't really think so. Threaded comments lead to in-fighting. In-fighting could be good for the site. More traffic. More visitors. More activity. But it could also be bad for the site - more distraction, more arguing, less action-focused stuff. I want the site to be action focused. The whole purpos of be/change decatur is: to actually get things changed in decatur. I don't think the threaded comments will facilitate that well. And I can always add it later if I want to. So hek. Why bother doing it now, if it could be a feature add later on?



So, where am I right now?
Home page... This page.

Post a new Thread
    - customizations (maybe could make option to add these after posting? "Now you've posted... categorize it!")
        - choose a type??? maybe
        - choose a need it attaches to??? Not necessary
        - choose a sector??? not necessary
    - Title, content, tags. 
        (am I even using tags? That's a complex thing to add which may bring much difficulty. Surely something i WILL want. It brings us to the issue of categorizing. Maybe one of the tags IS a sector. Maybe one of the tags IS a need. Then the rest of the tags are general tags. Just, basic query-able tags)
    

</pre>
<!--[CONFIRM_URL_TEST_/]-->