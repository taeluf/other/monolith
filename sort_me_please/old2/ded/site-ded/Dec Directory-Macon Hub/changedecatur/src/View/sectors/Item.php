<?php
    $sector = $this->obj;
    $loader = $this->config->loader;
    $needs = $loader->getNeedsBySector($sector->slug);
    $printNeed = function($need){
        echo new \CD\Tiny\View('View/needs/ListItem.php',$need);
        echo '<hr>';
    };
?>
<h1><?=$sector->name?> Sector</h1>
<pre><?=$sector->description?></pre>

<h2><?=$sector->name?> Needs</h2>
<?php

array_map($printNeed,$needs);

?>