<?php


function showIdeaForAttachment($idea){
    echo new \CD\Tiny\View('View/needs/IdeaForAttachment.php',$idea);
}

$ideas = \CD\Idea::getAll();
$need = $this->obj;
$router = $this->config->router;
?>
<h1>Attach ideas to <b><?=$need->title?></b></h1>

<form method="POST" action="<?=$router->getPageUrl('submit',NULL,'AttachIdeasToNeed');?>">
<fieldset><legend>Ideas</legend>

<?php array_map('showIdeaForAttachment',$ideas);?>

<br>
<br>
<input type="submit" value="Submit" />

<input type="hidden" name="need" value="<?=$need->slug?>" />
</form>