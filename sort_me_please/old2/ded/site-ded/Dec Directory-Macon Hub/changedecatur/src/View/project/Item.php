<?php

$project = $this->obj;

?>
<h1><?=$project->title?></h1>
<p><?=$project->description?></p>
<h2>Desired Results</h2>
<p><?=$project->result?></p>
<h2>Potential Actions</h2>
<p><?=$project->description?></p>

<h1>Threads</h1>
<?php foreach($project->threads() as $thread){ ?>
<?=new \CD\Tiny\View('View/project/Thread.php',$thread);?>
<?php } ?>