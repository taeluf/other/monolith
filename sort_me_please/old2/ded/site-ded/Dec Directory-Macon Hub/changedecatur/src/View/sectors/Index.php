<?php

$sectors = \CD\Sector::getAll();
$showSector = 
    function($sector){
        echo '<h2>'.$sector->name.'</h2>'.'<p>'.$sector->description.'</p>';
    };
?>
<h1>Sectors</h1>
<?php
    array_map($showSector,$sectors);

?>