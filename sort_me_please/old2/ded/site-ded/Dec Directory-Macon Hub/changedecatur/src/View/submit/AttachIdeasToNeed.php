<?php


$need = \CD\Need::fromSlug($_POST['need']);
$need->ideas = $_POST['ideas'];
$need->save();

header("Location: ".$this->config->router->getPageUrl('needs',$need->slug));
exit;
?>
