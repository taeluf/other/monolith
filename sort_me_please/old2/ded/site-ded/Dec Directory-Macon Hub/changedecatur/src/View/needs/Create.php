<h1>Create Need</h1>
<p>Needs drive our community. Anyone can make a claim on the site, but it is only by organizing these claims into needs that we are able to start defining a clear process and community around the claim.</p>
<p>On the next screen, you can start proposing solutions and/or record actions taken</p>
<form method="POST" action="/submit/need/">
<fieldset><legend>New Need</legend>
<label for="title">Decatur needs...</label><br>
<input type="text" name="title" placeholder="Ex: less litter on the street"/><br>
<label for="description">Description</label><br>
<p>The description should only elaborate on the problem and the specific result desired. There should be nothing about solutions to the problem nor plans to achieve the result. This comes later</p>
<textarea name="description" cols=70 rows=10 placeholder="Describe the need in detail."></textarea><br>
<label for="sector">Sector</label>
<p>What is the <b>cause</b> of the problem or need? For example, the need "Schools need to be more diverse", relates to race &amp; education. But the problem is not with education itself. It's in how race interacts with the education system, so you would choose "Race".</p>
<p>With the need "Kids need more sex education", the sector would be "Education".</p>
<select name="sector_id">
    <option value="IDK">I don't know</option>
    <option value="LGBTQ">LGBTQ</option>
    <option value="race">Race</option>
    <option value="education">Education</option>
    <option value="mental_health">Mental Health</option>
    <option value="27">Environment</option>
    <option value="12">Poverty</option>
</select><br>

<br><input type="submit" value="Submit" />

</fieldset>
</form>