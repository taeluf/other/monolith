<?php
    $thread = $this->obj;
    $threadId = $thread->id;
    $projects = \CD\Tiny\Loader::getInstance()->getAllProjects();
    $projectOptionsList = array_map(function($project){return '<option value="'.$project->id.'">'.$project->title.'</option>';},$projects);
    $projectOptions = implode("\n",$projectOptionsList);
?>

<form method="POST" action="/submit/projecttothread/">
    <fieldset><legend>
    <p>
        <select name="projectId">
            <option disabled selected>Select One</option>
            <?=$projectOptions?>
        </select>

    </p>
    <p>
        <input type="submit" value="Submit">
    </p>

    <input type="hidden" name="threadId" value="<?=$threadId?>">
</form>
add an existing project OR
<p>
<a href="<?=\CD\Tiny\Router::getInstance()->getPageUrl('thread',$thread->slug,'addproject');?>">create a new project</a>