<?php

$need = \CD\Need::fromSlug($_POST['need']);
$sector = \CD\Sector::fromSlug($_POST['sector']);

$need->sector = $sector->slug;
$need->save();


header("Location: ".$this->config->router->getPageUrl("needs",$need->slug));
?>