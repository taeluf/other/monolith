<?php
    $idea = $this->obj;
    if ($idea==NULL){
        $summary = '';
        $description = '';
        $slug = '';
    } else {
        $summary = $idea->summary;
        $description = $idea->description;
        $slug = $idea->slug;
    }
?>
<form action="/submit/idea/" method="post">
    <fieldset><legend>Decatur needs...</legend>
    <p>Share your idea about what Decatur needs. Your ideas can be:</p>
    <p>1. discussed in the community</p>
    <p>2. presented to local leaders</p>
    <p>3. <b>enacted</b></p>
    <p>But don't worry. Just tell us about it. You don't have to have every detail figured out, or necessarily any detail. </p>
    <label for="description">Describe your idea in detail</label>
    <br>
    <textarea 
        rows="10"
        cols="70"
        name="description"
        placeholder="Tell me all about your idea!"
    ><?=$description?></textarea>
    <br>
    <br>
    <label for="summary">Summarize</label>
    <p>&nbsp;&nbsp;In 10 words or less, finish the sentence: </p>
    <p>&nbsp;&nbsp;&nbsp;&nbsp;"Decatur needs..."</p>
    <input type="text" name="summary" placeholder="less litter in gm square neighborhood" value="<?=$summary?>"/>
    <br>

    <input type="hidden" name="slug" value="<?=$slug?>" />
    <br>
    <input type="submit" value="Submit">
    </fieldset>

</form>