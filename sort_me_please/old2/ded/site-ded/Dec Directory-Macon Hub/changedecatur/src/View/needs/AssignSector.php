<?php

$need = $this->obj;
$router = $this->config->router;
$sectors = \CD\Sector::getAll();
$printSector = function($sector){
    echo new \CD\Tiny\View('View/needs/SectorForAttachment.php',$sector);
}
?>
<h1>Assign sector to <?=$need->title?></h1>
<form method="POST" action="<?=$router->getPageUrl('submit',NULL,'AssignSectorToNeed')?>">

<?php array_map($printSector,$sectors); ?>

<br>
<br>
<input type="submit" value="Submit">

<input type="hidden" name="need" value="<?=$need->slug?>">
</form>