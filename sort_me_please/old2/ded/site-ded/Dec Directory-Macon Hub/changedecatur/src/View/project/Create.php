<?php


?>
<form action="/submit/createproject/" method="POST">
    <fieldset><legend>Create a Project</legend>
        <p>
            Projects are specific, actionable things. For example, <b>Zero Waste</b> is a cause (or movement) because it is sufficiently broad. A <b>Zero Waste Project</b> could be <i>Ban Plastic Bags</i> or <i>Consumer Waste Education</i>.
        </p>
        <p>Don't worry, though. If you post a project which should be a cause (or visa versa), our moderators will help classify everything correctly.</p>
        <p>
        <label>Title<br>
            <input type="text" name="title" placeholoder="Title">
        </label>
        </p>
        <p>
            <label>Ramble about your project<br>
                <textarea name="description" placeholder="Tell me about your project" cols="70" rows="10"></textarea>
            </label>
        </p>
        <p>
            <label>What specific result(s) do you wish to see from your project?<br>
                <textarea name="result" placeholder="Tell me about your project" cols="70" rows="10"></textarea>
            </label>
        </p>
        <p>
            <label>What specific actions need to be performed?<br>
                <textarea name="action" placeholder="Tell me about your project" cols="70" rows="10"></textarea>
            </label>
        </p>
        <p>
            <input type="submit" value="Create Project">
        </p>
    </fieldset>
</form>
