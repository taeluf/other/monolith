<?php

namespace CD;

class Tester extends \ROF\Tester {

    public function testDeliverPages(){
        $urls = [
            '/',
            '/help/',
    
        ];
        $pass = TRUE;
        foreach ($urls as $url){
            $router = new \CD\Tiny\Router($url);
            $content = "".$router->getView();
            if (strpos($content,'<!--[CONFIRM_URL_TEST_'.$url.']-->')===FALSE){
                $pass = FALSE;
                break;
            }
        }
        return $pass;
    }

    public function testSaveClaim(){
        $loader = \CD\Tiny\Loader::getInstance();
        $claim = new \CD\Claim();
        $claim->title = "More diverse schools. ".time();
        $claim->content = "the schools in decatur are pretty diverse, but there are some HUGE gaps that need to be filled, especially in our private schools, which are predominantly white.";
        return $loader->saveClaim($claim);
    }

    public function testGetSolutions(){
        $solutions = (\CD\Tiny\Loader::getInstance())->getSolutions('1562699727_97034');
        if (is_array($solutions)){
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function testSomething(){
        return FALSE;
    }
    

}



?>