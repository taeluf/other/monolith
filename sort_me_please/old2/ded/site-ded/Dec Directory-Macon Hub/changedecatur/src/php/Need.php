<?php

namespace CD;

class Need {

    public $id;
    public $title;
    public $slug;
    public $description;
    public $sector;
    public $ideas = [];


    public function save(){
        $loader = \CD\Tiny\Loader::getInstance();
        $loader->saveNeed($this);
    }
    static public function fromSlug($slug){
        $loader = \CD\Tiny\Loader::getInstance();
        return $loader->needFromSlug($slug);
    }
    static public function getAll(){
        $loader = \CD\Tiny\Loader::getInstance();
        return $loader->getNeeds(999999);
    }
    static public function fromPOST(){
        $data = $_POST;
        if (!isset($data['id']))$data['id'] = NULL;
        $data['slug'] = \CD\Tiny\Router::getInstance()->slugify($data['title']);
        return static::fromRow($data);
    }
    static public function fromRow($row){
        $need = new \CD\Need();
        $need->id = $row['id'];
        $need->title = $row['title'];
        $need->description = $row['description'];
        $need->sectorId = $row['sector_id'];
        $need->slug = $row['slug'];
        //$need->ideas = isset($row['ideas']) ? $row['ideas'] : [];
        return $need;
    }
}