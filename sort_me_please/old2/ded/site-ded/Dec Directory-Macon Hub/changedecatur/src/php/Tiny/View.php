<?php

namespace CD\Tiny;

/** Used to instantiate views for this library.
 *  You probably don't need to write any code in here... but you might! 
 * 
 */
class View extends \ROF\Tiny\View {


    public function getTemplateTop(){
        return '';
        return '<!DOCTYPE html><html><head></head><body><main>';
    }
    public function getTemplateBottom(){
        return '';
        return '</main></body></html>';
    }

    public function __toString(){
        return $this->getTemplateTop().parent::__toString().$this->getTemplateBottom();

    }

    static public function showNeed(
        $need)
    { 
        echo new static('View/needs/ListItem.php',$need);
        echo '<hr>';
    }

}