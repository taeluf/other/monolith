<?php

namespace CD\Tiny;

class Config extends \ROF\Tiny\Config {




/* These are default configurations. 
* You don't need to change any of them, but you can. You may want to view the documentation if you're editing these.
* 
* For performance's sake, you should set up the categories array and set autoCategorize to false
* To get the default category configuration array, call `echo (CD\Tiny\Config::getInstance())->exportCategories(TRUE);` 
        to run the auto-categorization and get its var declaration
*    then copy that into this class and delete your existing `$categories` and `$autoCategorize` settings (since they're in the export)
*
    public $defaultCategory = '*'; //this may be ignored. if the default category doesn't exist, then it will use the first category in the configured categories array
    public $autoCategorize = TRUE; //maps your view folder to a set of categories. Not very performant
    public $categories = [];
    public $requiredConfigs = [];
*/

/* if you want to implement your own permissions class, this is where you do it
*  or if you wish to add any default configurations which require logic or instantiation
*/
    public function __construct(){
        parent::__construct();
        $this->categories['*']['url'] = '/';
        $this->categories['*']['views']['help']['slug'] = FALSE;
        $this->categories['*']['views']['test']['slug'] = FALSE;
        $this->categories['submit']['views']['idea']['slug'] = FALSE;
        $this->categories['submit']['views']['need']['slug'] = FALSE;
        $this->categories['submit']['views']['sector']['slug'] = FALSE;
        $this->categories['submit']['views']['attachideastoneed']['slug'] = FALSE;
        $this->categories['submit']['views']['assignsectortoneed']['slug'] = FALSE;
        $this->categories['submit']['views']['thread']['slug'] = FALSE;
        $this->categories['submit']['views']['comment']['slug'] = FALSE;
        $this->categories['submit']['views']['createproject']['slug'] = FALSE;
        $this->categories['submit']['views']['projecttothread']['slug'] = FALSE;

        $this->categories['needs']['views']['create']['slug'] = FALSE;
        $this->categories['ideas']['views']['create']['slug'] = NULL;
        $this->categories['sectors']['views']['create']['slug'] = FALSE;
        //$this->categories['thread']['views']['addproject']['slug'] = FALSE;
        $this->categories['project']['views']['create']['slug'] = FALSE;


    }
/*
*/

}

?>