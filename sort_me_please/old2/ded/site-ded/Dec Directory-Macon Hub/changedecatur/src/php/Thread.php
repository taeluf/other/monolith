<?php

namespace CD;

class Thread {

    public $id;
    public $title;
    public $body;
    public $tagString;
    public $slug;
    protected $comments;
    protected $project;
    protected $projectId;

    protected function __construct(){
        
    }

    public function comments(){
        if ($this->comments!==NULL)return $this->comments;
        $this->comments = \CD\Tiny\Loader::getInstance()->commentsForThread($this->id);
        return $this->comments;
    }
    public function project(){
        $project = $this->project ?? \CD\Tiny\Loader::getInstance()->projectFromId($this->projectId);
        $this->project = $project;
        $this->projectId = $project->id;
        return $project;
    }

    public function save(){
        $loader = \CD\Tiny\Loader::getInstance();
        $loader->save($this);
    }

    public function setProject($project){
        $this->project = $project;
        $this->projectId = $project->id;
    }

    static public function fromArray($data){
        $thread = new static();
        $thread->id = $data['id'];
        $thread->title = $data['title'];
        $thread->body = $data['body'];
        $thread->tagString = $data['tags'];
        $thread->slug = $data['slug'];
        $thread->projectId = $data['projectId'] ?? NULL;
        return $thread;
    }
}


?>