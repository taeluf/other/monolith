<?php

namespace CD;

class Project {


    public $id;
    public $title;
    public $slug;
    public $description;
    public $result;
    public $action;
    protected $threads;

    protected function __construct(){
    
    }

    public function save(){
        return \CD\Tiny\Loader::getInstance()->save($this);
    }

    public function threads(){
        if ($this->threads!=NULL)return $this->threads;
        $threads = \CD\Tiny\Loader::getInstance()->threadsFromProject($this->id);
        $this->threads = $threads;
        return $threads;
    }

    static public function fromArray($data){
        $project = new static();
        $project->id = $data['id'] ?? NULL;
        $project->title = $data['title'];
        $project->description = $data['description'];
        $project->result = $data['result'];
        $project->action = $data['action'];
        $project->slug = isset($data['slug']) ? $data['slug'] : \CD\Tiny\Router::getInstance()->slugify($data['title']);
        return $project;

    }

    public function addThread($thread){
        if (!in_array($thread,$this->threads())){
            $this->threads[] = $thread;
        }
    }
}


?>