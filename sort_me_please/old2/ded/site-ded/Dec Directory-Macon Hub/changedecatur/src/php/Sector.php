<?php

namespace CD;

class Sector {

    public $id = NULL;
    public $name;
    public $description;
    public $slug;


    public $needs;
    public $followers;
    public $ideas;



    static public function fromPOST(){
        $data = $_POST;
        if (!isset($data['id']))$data['id'] = NULL;
        $data['slug'] = \CD\Tiny\Router::getInstance()->slugify($data['name']);
        $sector = static::fromRow($data);
        return $sector;
    }
    static public function fromRow($row){
        $sector = new static();
        $sector->id = $row['id'];
        $sector->name = $row['name'];
        $sector->description = $row['description'];
        $sector->slug = $row['slug'];
        return $sector;
    }
    static public function getAll(){
        $loader = \CD\Tiny\Loader::getInstance();
        return $loader->sectors(99999);
    }
    static public function fromSlug($slug){
        $loader = \CD\Tiny\Loader::getInstance();
        return $loader->sectorFromSlug($slug);
    }
}