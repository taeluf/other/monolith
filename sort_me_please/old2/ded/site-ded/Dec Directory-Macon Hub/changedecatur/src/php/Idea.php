<?php

namespace CD;


class Idea {

    public $id;
    public $summary;
    public $description;
    public $need;
    public $slug;

    public function __construct(){

    }

    public function save(){
        $loader = \CD\Tiny\Loader::getInstance();
        $loader->saveIdea($this);
    }

    static public function fromRow(
        $row)
    {
        $idea = new static();
        $idea->id = $row['id'];
        $idea->summary = $row['summary'];
        $idea->description = $row['description'];
        $idea->need = NULL;
        $idea->slug = $row['slug'];
        return $idea;
    }

    static public function fromPOST()
    {
        $data = $_POST;
        $data['id'] = isset($_POST['id']) ? $_POST['id'] : NULL;
        $data['slug'] = (\CD\Tiny\Router::getInstance())->slugify($data['summary']);
        
        return static::fromRow($data);
    }
    static public function fromSlug($slug){
        $loader = \CD\Tiny\Loader::getInstance();
        return $loader->ideaFromSlug($slug  );
    }
    static public function getAll(){
        $loader = \CD\Tiny\Loader::getInstance();
        return $loader->getIdeas(99999999);
    }

    
    
}



?>