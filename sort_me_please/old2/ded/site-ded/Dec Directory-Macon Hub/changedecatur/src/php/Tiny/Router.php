<?php

namespace CD\Tiny;

/** Routes web requests. You should put any code in here which is relevant to URLs and request routing
 *  You don't necessarily need any code in here. The parent-class functions will take care of the basic functionality
 */
class Router extends \ROF\Tiny\Router {


    /** returns a slug-friendly version of the string
     * 
     */
    public function slugify($string){
        return strtolower(
            str_replace([' '],['-'],$string)
        );
    }
}







?>