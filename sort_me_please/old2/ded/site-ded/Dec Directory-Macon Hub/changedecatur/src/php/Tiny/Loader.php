<?php

namespace CD\Tiny;

/** Loads data. This data can come from a database, the file system, a json file, or anything. 
 * There may be functions to return instantiated objects based upon this loaded data, such that the user of this library does not deal with raw data
 * 
 */
class Loader extends \ROF\Tiny\Loader {

    /** Get an object based upon a url-slug. 
     *  This method MUST be implemented, or the request routing will not work.
     * 
     * @return object an object of any class or NULL
     */
    public function getObjectFromSlug($slug){
        $category = $this->config->router->getCategory();
        if ($category=='ideas'){
            return $this->ideaFromSlug($slug);
        } else if ($category=='needs'){
            return $this->needFromSlug($slug);
        } else if ($category=='sectors'){
            return $this->sectorFromSlug($slug);
        } else if ($category=='thread'){
            return $this->threadFromSlug($slug);
        } else if ($category=='project'){
            return $this->projectFromSlug($slug);
        }
        
        throw new \Exception("You must declare 'getObjectFromSlug()' in the Loader class.");
        /* this code below is just as an example. You may want to query a database or load something from disk, based upon the slug.
        * Remove the exception once you have your own implementation
        */
        $object = new MyObject($slug);
        return $object;
    }

    public function saveIdea(\CD\Idea $idea){
        if ($idea->id===NULL){
            $query = "INSERT INTO idea(id,summary,description,need_id,slug) VALUES(:id,:summary,:description,:need_id,:slug)";
        } else {
            $query = "UPDATE idea SET summary=:summary, description=:description, need_id=:need_id, slug=:slug WHERE id=:id";
        }
        $statement = $this->config->pdo->prepare($query);
        $rersult = $statement->execute(
            [
                ":id"=>$idea->id,
                ":summary" => $idea->summary,
                ":description" => $idea->description,
                ":need_id" => is_object($idea->need) ? $idea->need->id : NULL,
                ":slug" => $idea->slug
            ]
        );
        return $result;
    }

    public function getIdeas($count=10){
        $statement = $this->config->pdo->prepare("SELECT * FROM idea");
        $statement->execute();
        $ret = [];
        while ($row=$statement->fetch()){
            $ret[] = $this->ideaFromRow($row);
        }

        return $ret;
    }
    public function ideaFromSlug($slug){
        $statement = $this->config->pdo->prepare("SELECT * FROM idea WHERE slug LIKE :slug");
        $statement->execute([":slug"=>$slug]);
        return $this->ideaFromRow($statement->fetch());
    }

    public function ideaFromRow($row){
        return \CD\Idea::fromRow($row);
    }

    public function needFromSlug($slug){
        $statement = $this->config->pdo->prepare(
            "SELECT need.*, sector.* FROM need "
                ."JOIN sector "
                ."ON sector.id = need.sector_id "
            
            ."WHERE need.slug LIKE :slug"
        );
        $result = $statement->execute([':slug'=>$slug]);
        $row = $statement->fetch();
        $result = $this->resultsByTable($row,$statement);
        $need = $this->needFromRow($result['need']);
        $need->sector = $this->sectorFromRow($result['sector']);
        return $this->needFromRow($row);
    }
    public function saveNeed($need){
        if ($need->id===NULL){
            $query = "INSERT INTO need(id,title,slug,description,sector_id) VALUES(:id,:title,:slug,:description,:sector_id)";
        } else {
            $query = "UPDATE need SET title=:title, slug=:slug, description=:description, sector_id=:sector_id WHERE id=:id";
        }
        $statement = $this->config->pdo->prepare($query);
        $result = $statement->execute(
            [':id'=>$need->id,':title'=>$need->title,':slug'=>$need->slug,
':description'=>$need->description,':sector_id'=>$need->sectorId]);

        return $result;
    }
    public function saveNeedFromPost(){
        $need = \CD\Need::fromPost();
        return $this->saveNeed($need);
    }
    public function needFromRow($row){
        return \CD\Need::fromRow($row);
    }

    public function getNeedsBySector($sectorSlug){
        $statement = $this->config->pdo->prepare(
            "SELECT * FROM need "
                ."JOIN sector "
                    ."ON need.sector_id = sector.id "
        );
        $result = $statement->execute();
        $ret = [];
        while($result&&$row = $statement->fetch()){
            $ret[] = $this->needFromRow($row);
        }
        return $ret;
    }
    public function needs($count=NULL){
        $statement = $this->config->pdo->prepare("SELECT * FROM need");
        $result = $statement->execute();
        $ret = [];
        while($result&&$row = $statement->fetch()){
            $ret[] = $this->needFromRow($row);
        }
        return $ret;
    }

    public function sectors($count=NULL){
        $pdo = $this->config->pdo;
        $statement = $pdo->prepare("SELECT * FROM sector;");
        $result = $statement->execute();
        $ret = [];
        while($result&&$row = $statement->fetch()){
            $ret[] = $this->sectorFromRow($row);
        }
        return $ret;
    }
    public function sectorFromSlug($slug){
        $pdo = $this->config->pdo;
        $statement = $pdo->prepare("SELECT * FROM sector WHERE slug LIKE :slug");
        $statement->execute([':slug'=>$slug]);
        return $this->sectorFromRow($statement->fetch());
    }
    public function sectorFromRow($row){
        return \CD\Sector::fromRow($row);
    }
    public function saveSectorFromPOST(){
        $sector = \CD\Sector::fromPOST();
        $this->saveSector($sector);
        return $sector;
    }
    public function saveSector($sector){
        $pdo = $this->config->pdo;
        if ($sector->id===NULL){
            $query = "INSERT INTO sector(id,name,description,slug)"
                    ."VALUES(:id,:name,:description,:slug)";
        } else {
            $query = "UPDATE sector SET name=:name, description=:description, slug=:slug WHERE id=:id";
        }
        $statement = $pdo->prepare($query);
        $result = $statement->execute(
            [":id" => $sector->id,
            ":name" => $sector->name,
            ":description" => $sector->description,
            ":slug" => $sector->slug

            ]
        );
        if ($result){
            //echo 'saved';
           // exit;
        } else {
            echo 'failed';
            exit;
        }
    }

    protected function resultsByTable($row,$statement){
        $results = [];
        $numberedRow = array_filter($row, function($key){return is_numeric($key);},ARRAY_FILTER_USE_KEY);
        foreach($numberedRow as $index=>$value){
            $meta = $statement->getColumnMeta($index);
            $results[$meta['table']][$meta['name']] = $value;
        }
        return $results;
    }
    
    public function saveThread($thread){
        $object = $thread;
        $table = 'thread';
        $pdo = $this->config->pdo;
        $query = '';
        if ($object->id===NULL){
            $query = "INSERT INTO {$table}(id, title, body, tags, slug, projectId) VALUES(:id,:title,:body,:tags,:slug,:projectId)";
        } else {
            $query = "UPDATE {$table} SET title=:title,body=:body,tags=:tags,slug=:slug,projectId=:projectId WHERE id=:id";
        }
        $statement = $pdo->prepare($query);
        $statement->execute([
            ':title' => $object->title,
            ':body' => $object->body,
            ':tags' => $object->tagString,
            ':slug' => $object->slug,
            ':projectId' => $object->project()->id,
            ':id' => $object->id,
    
        ]);
        $object->id = $object->id ?? $pdo->lastInsertId();
    }

    public function threadFromSlug($slug){
        $query = "SELECT * FROM thread WHERE slug=:slug";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute([':slug'=>$slug]);
        return \CD\Thread::fromArray($statement->fetch());
    }
    public function threadFromId($id){
        $query = "SELECT * FROM thread WHERE id=:id";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute([':id'=>$id]);
        return \CD\Thread::fromArray($statement->fetch());
    }

    public function commentsForThread($threadId){
        $query = "SELECT * FROM comment WHERE threadId=:threadId ORDER BY createdAt ASC";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute([':threadId'=>$threadId]);
        $comments = [];
        while ($row = $statement->fetch()){
            $comments[] = \CD\Comment::fromArray($row);
        }
        return $comments;
    }

    public function saveComment($comment){
        $pdo = $this->config->pdo;
        $query = '';
        if ($comment->id==NULL){
            $query = "INSERT INTO comment(id,comment,createdAt,threadId) VALUES(:id,:comment, :createdAt,:threadId)";
        } else {
            $query = "UPDATE comment SET comment=:comment, createdAt=:createdAt, threadId=:threadId WHERE id=:id";
        }
        $statement = $pdo->prepare($query);
        $statement->execute([
            ':comment'=>$comment->body,
            ':id'=>$comment->id,
            ':createdAt'=>$comment->createdAt,
            ':threadId' => $comment->thread()->id,
        ]);
        if ($comment->id===NULL)$comment->id=$pdo->lastInsertId();
    }

    public function projectFromId($id){
        $query = "SELECT * FROM project WHERE id=:id";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute([':id'=>$id]);
        $row = $statement->fetch();
        // var_dump($row);
        // echo "\n\n\n\n---ROW PRINTED---\n\n\n\n";
        $project = \CD\Project::fromArray($row);
        // var_dump($project);
        // echo "\n\n\n\n---PROJECT PRINTED---\n\n\n\n";
        return $project;
    }
    
    public function saveProject($project){
        $object = $project;
        $table = 'project';
        $pdo = $this->config->pdo;
        $query = '';
        if ($object->id===NULL){
            $query = "INSERT INTO {$table}(id, title, description, result, action, slug) VALUES(:id,:title,:description,:result,:action,:slug)";
        } else {
            $query = "UPDATE {$table} SET title=:title,description=:description,result=:result,action=:action,slug=:slug WHERE id=:id";
        }
        $statement = $pdo->prepare($query);
        $statement->execute([
            ':title' => $object->title,
            ':description' => $object->description,
            ':result' => $object->result,
            ':action' => $object->action,
            ':slug' => $object->slug,
            ':id' => $object->id,
    
        ]);
        //$object->id = $object->id ?? $pdo->lastInsertId();
    }
    public function projectFromSlug($slug){
        $query = "SELECT * FROM project WHERE slug=:slug";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute([':slug'=>$slug]);
        return \CD\Project::fromArray($statement->fetch());
    }
    public function threadsFromProject($projectId){
        if ($projectId===NULL)return [];
        $query = "SELECT * FROM thread WHERE projectId=:projectId";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute([':projectId'=>$projectId]);
        $threads = [];
        while($thread=$statement->fetch()){
            $threads[] = \CD\Thread::fromArray($thread);
        }
        return $threads;
    }

    public function getAllProjects(){
        $query = "SELECT * FROM project";
        $statement = $this->config->pdo->prepare($query);
        $statement->execute();
        $projects = [];
        while ($project = $statement->fetch()){
            $projects[] = \CD\Project::fromArray($project);
        }
        return $projects;
    }

    public function save($item){
        if ($item instanceof \CD\Thread){
            return $this->saveThread($item);
        } else if ($item instanceof \CD\Comment){
            return $this->saveComment($item);
        } else if ($item instanceof \CD\Project){
            return $this->saveProject($item);
        } else {
            return false;
        }
    }
}




?>