<?php

namespace CD;

class Comment {

    public $body;
    public $id;
    public $createdAt;
    protected $thread;
    protected $threadId;




    public function save(){
        $loader = \CD\Tiny\Loader::getInstance();
        $loader->save($this);
    }
    static public function fromArray($data){
        $comment = new Comment();
        $comment->body = $data['comment'];
        $comment->id = isset($data['id']) ? $data['id'] : NULL;
        $comment->createdAt = isset($data['createdAt']) ? $data['createdAt'] : time();
        $comment->threadId = $data['threadId'];
        return $comment;
    }
    public function thread(){
        if ($this->thread!==NULL)return $this->thread;
        $this->thread = (\CD\Tiny\Loader::getInstance())->threadFromId($this->threadId);
        return $this->thread;
    }   




}

?>