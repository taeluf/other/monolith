<?php

$dsn = 'mysql:dbname=test_cd;host=localhost';
$pdo = new PDO($dsn,"reed","64Fuck64");

$orgsLookup = $pdo->prepare("SELECT * FROM organization WHERE id=:id");
$orgsLookup->execute([':id'=>$_GET['id']]);

$elements = [];
foreach ($orgsLookup->fetchAll(PDO::FETCH_ASSOC) as $index => $orgRow){
    ob_start();
?>
    <h1><?=$orgRow['name'];?></h1>
    <section><?=$orgRow['description'];?></section>
    <br><a href="<?=$orgRow['website']?>">Official Website</a>
    <br><a href="/cd/view_org/?id=<?=$orgRow['id']?>">Org Page</a>
    <p><?=$orgRow['description']?></p>
    <h2>Teams</h2>
    <section class="teamSection">
        <?php
            $projects = $pdo->prepare("SELECT * FROM team WHERE orgId=:orgId");
            $projects->execute([':orgId'=>$orgRow['id']]);
            foreach ($projects->fetchAll(PDO::FETCH_ASSOC) as $projectRow){
                ?>
                <h4><?=$projectRow['name']?></h4>
                <p><?=substr($projectRow['description'],0,250);?></p>

                <?php
            }
    
        ?>
    </section>
    <h2>Projects</h2>
    <section class="projectSection">
        <?php
            $projects = $pdo->prepare("SELECT * FROM project WHERE orgId=:orgId");
            $projects->execute([':orgId'=>$orgRow['id']]);
            foreach ($projects->fetchAll(PDO::FETCH_ASSOC) as $projectRow){
                ?>
                <h4><?=$projectRow['title']?></h4>
                <p><?=substr($projectRow['description'],0,250);?></p>

                <?php
            }
    
        ?>
    </section>
    <br>
    <br>

<?php
    echo ob_get_clean();
}

