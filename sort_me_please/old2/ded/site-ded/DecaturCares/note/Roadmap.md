
# Decatur Cares
This is probably going to be my flagship project.

# Roadmap
- Conceptualization: DONE
- Goal Setting: 1/2 day, Tues. 14th
- Market Research: 1/2 day, Tues. 14th (unless I decide to ask folks for input on the idea, then 3 days)
- High Level Design: 1 day, Thurs. 16th
- Code Plan/Outline: 2 days, Fri & Sat 17th & 18th
- Marketing plan: 1-2 days
- Detailed Design: 2-5 days (also where I get into free/paid features)
- Code Writing: Start Tues 21st. 10 work days, roughly
- Testing & Data input: May 5th. 5 days.
- Publishing beta: May 12th
- Publishing first major release: June 1

# Pain Points
- Search
- Overall design
- Online community features (forum / group blog / group chat / comments)
- Customer support system
- Volunteer opportunities
- Events + calendar
- Accepting donations
- Allowing online sales
- Defining free & pay-to-unrestrict features
- Onboarding local organizations (setting up their basic pages, getting them to create accounts & manage their pages,)
- Blogging/Content Creation (for individual groups/orgs)