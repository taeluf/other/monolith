# Decatur Cares: Technical Specification
**Author:** Reed Sutman, rsutman@gmail.com
**Date Started:** Apr 15, 2020

**Goals:** 
    - To build a beta-version of the product by May 12th and a first-release by June 1
    - Create a sitemap
    - Create a list of necessary software

## Software Required
- User login system with role management
- Content Creation software
- File manager, file uploads, possibly image editing
- Image file manager
- Routing system
- A site theme / layout, mobile menu
- Calendar & events webapp
- 

## Modules?/Components?/Views?
- Search (or should we call this "feed"?)
    - ListView
    - Pageination
    - Load dynamic content (multiple different EntityTypes can be searched for at once)
- Feed... An auto-updating version of the search?
- Group
    - View THE group page
    - Group Create / Edit View
    - SearchResult view
    - 

## Sitemap
- .com/{resource}/{ResId}/edit|create: Probably, edit&create pages will be merged, as it's just... more streamlined. They might use their own urls, though. They will NOT be in the sitemap.xml & will not be indexed by search engines
- .com/ Home Page: A feed with tag/button-based searching/filtering at the top. The default feed is TBD, but there will be a feed. 
    - When feed is refined, update address bar to .com/feed/?{TheSearch}
- .com/group/{GroupName} Group Pages:  A group page is the official "business" listing for a particular group or organization
    - .com/group/{GroupName}/{GroupId} for a permaurl
    - .com/group/{GroupName}/volunteer
- .com/cause/{CauseName}
    - .com/cause/{CauseId} for a perma-url
- .com/volunteer/
    - .com/volunteer/{OpportunityName}/
    - .com/volunteer/{GroupName}/
    - .com/volunteer/{CauseName}/
- Conflict resolution:
    - .com/group/{GroupName} Resolves to two groups. Both are shown in a feed, and either one can be selected
- Universal Pages:
- For `edit` and `get`, which only functionally use the `id`, everything else is aesthetic
    - .com/feed/?{EntityTypes}&{ResultsFormat}&{Order-Direction}&{SearchTerm}
        : What about FeedTypes? What if I want a feed of things that DoveInc has posted? Same url, more paramaters?
        - .com/feed/ (no params) has canonical=".com/"
    - .com/create/{?EntityType} - Has a selector to choose what type of entity to create
    - .com/edit/{?EntityType}/{?EntityName}/id:{EntityId}
    - .com/save/ IF there is a dynamic url for save, it's purely aesthetic, as functional info is POSTed
        - Saves item & redirects to the item's page
    - .com/get/{?EntityType}/{?EntityName}/id:{EntityId}
        - What if I want to get a paramater OF an entity, like Entity.posts??

- Group Pages
    - .com/{GroupName}
    - .com/group/{GroupName}
    - .com/feed/{GroupName}
    - .com/edit/{GroupId}
    - .com/save/
    - .com/get/{GroupId}
    - .com/create/Group
    : Pages belonging to the group:
        - .com/volunteer/{GroupName} - volunteer opportunities offered by the group
        - .com/{GroupName}/volunteer/
        - .com/{GroupName}/volunteer/{OpportunityName}
        - .com/cause/{GroupName} - causes the group supports / is involved in
        - .com/

## Website Schemas/entities
- Causes Relationship (v1: for groups, v2: more dynamic, v3 totally dynamic)
    - v1: (relationship between a cause and a group)
        - group_id
        - cause_id
        - relationship_type ('owned_by_group', 'group_subscribed_to', 'group_active_in')
        : ex: cause 'Native Plants' is 'owned_by_group' SONA. CILDSA would be 'group_active_in', and CEC would be 'group_subscribed_to'
    - v2: (relationship between a cause and another entity)
        - entity_type
        - entity_id
        - cause_id
        - relationship_type
        - ?permission_level - I think this goes somewhere else
    - v3: (relationship between two entities)
        - entity_type
        - entity_id
        - relationship ('owns', 'subscribed_to', 'active_in') - this types list... likely must be dynamic
        - target_type 
        - target_id
        : Ex: ET-Group, EID-###(SONA), 'owns', TT-Cause, TID-###(Native Plants)
        : Ex: ET-User, EID-###(Reed), 'owns', TT-Discussion, TID-###(DUI Grant), Group DSA is subscribed
        : Ex: SONA belongs_to Environmental category
        : Ex(NO): Environmental category owns SONA


# Blah blah
- Relationships are going to be the broadest and possibly most challenging thing to refine. It seems like relationships will be the core & backbone of this entire website. There will be some generalities that exist like "All users can subscribe_to [list of types]". Then there will have to be control mechanisms for assigning ownership of an item, changing ownership of an item... Searching will also have to consider permissions, so the wrong people don't see things they're not allowed/supposed to. For example, I don't think users themselves should be able to subscribe to requests_for_help. I think groups should be able to subscribe to / look at particular requests for help. The individual request can also limit who has access to it, so that request is going to need quite a set of permissions to make it functional & safe. Because if the wrong group looks at somebody's request for help, they could possibly take advantage. So the permissions system will be very integral to the overall trust-level of the website. Where groups may also belong in different "trust-level" categories. users might also exist in different "trust-level" categories. Where an Org like Dove probably has very high trust, and a group like DSA or Progressives might have a lower level of trust, as things are a bit more casual. 
    I'm not really sure where to go with this whole ramble about relationships. I know it's something I need to figure out, but I'm not really sure how to go about figuring it out. I don't know if I need a better understanding of how the website will look and function, or if I need to understand my database schemas better. It's just that there are going to be SO MANY relationships & I just don't really know how to manage them || represent them. I'm also getting overwhelmed. So when I see "Group owns event", I'm like "Well can a user own that event?" And then I'm like "How can I represent that in code?" But I don't even know what relationships there are. I think if I map out more of the relationships, then I can start to undertsand how to make it more robust & dynamic.
    So the personal problem I'm having is: I want the answers now. I want to spin in my head about how to make these relationships work. But what I think I need is to do the very boring task of writing out the relationships. If I write out these relationships, then I'll just... start to get it.

    But then... do I also jot down things that are specific to an entity? Like that group has a name & description? Do I need to be thinking about the Page Layout of a group? For me to conceptualize what data a group needs, I need to visualize what the group page is going to look like & visualize how it's going to be interacted with, so that's making me think that I can't really finish my schemas until I work on designs, but I'm also kinda like... I can't do design work without knowing what it is I'm designing. So I think these will play into each other.

    It's also a big question of how interactive things are going to be. The deeper I get into this, the more it seems this site will be a fantastic suite of tools, including client-management. Because that whole 'request-for-help' idea means that you get somebody to help & now they're attached to you digitally, and in real-life, and you use the digital tools to keep track of all the people your helping (aka, your cases). Furthhermore, this software will allow you to assign things (the request for help) to a particular user... where that user must be a team member with the "manage_requests_for_help" permission.... Or something

    So I seem to have quite this vision of how things are going to be, but I still don't really understand how they're going to be.

    So I think my next step is to go through the entities, sort some to the end that I'm not confident about, and then detail the relationships for the ones I AM confident about. Once I have relationships detailed... I think I'll have a better understanding of all that the site will do. I currently have so many grey clouds in my head. Where I kinda know... but it's just too foggy. 

Questions:
- What can users own?
- What can groups own?
- What can be subscribed to?
- What can be engaged with? (Group is_active_in cause)
- What is the difference between a cause & a Local Issue
    - A cause is a semi-formal effort to organize for the purpose of resolving a particular local issue
    - A local issue is... basically a complaint. One can say "There's not enough cookie dough in my ice cream." That's an issue. The cause is "To get more cookie in the ice cream."
- What types of relationships can there be?
    - owner, creator, moderator, subscriber,
- What IRL things exist?
    - Group (incorporated or not)
    - User - An individual. Individuals can belong to groups, teams, causes, actions... well no. It's a bit more specific than that
        - Group: in_group | subscribed_to_group
        - Cause: involved_in | subscribed_to
        - Team: member_of | NOT (maybe different membership levels?)
        - Action: has_performed | subscribed_to | assigned_to_action
        - Event: subscribed_to | is_organizing | did_attend | will_attend 
        - Request_for_Help: submitted_by | assigned_to
        - VolunteerOpportunity: signed_up_for | subscribed_to | applied_to | has_performed
        - JobOpportunity: signed_up_for (may be contacted) | applied_to (submitted a complete application) | subscribed_to (digital notifications) | was_hired
        - LocalIssue: shared_by | subscribed_to | is_acting_on (People who are doing something about this issue)
        :
        - Category: subscribed_to | created_by (but not owned by)
        - 
    - Cause (more of an idea)... A kind of... general mission. A cause generally SHOULD have actions available that either a team member or a member of the general public can do. Perhaps there's suggestions for elected officials as well.
    - Action - a specific thing one can do to support a cause, group, or event. An action can be exposed with different scopes, like... An action can be something a team member can do & only team members will see it. It can be something a group can do & groups can see it (but not the general public) or an action can be something anybody can do (like... write a letter to your Governor saying to "do this thing"...)
    - Event - a one-time thing that happens in the physical world
    - Team - A group of users who are assigned to a particular thing. The thing (cause/event...) has a team. That team consists of indidividuals.
    - VolunteerOpportunity - Belongs to a group. A team does NOT offer volunteer opportunities. Volunteer opportunities are things being "advertised" to the general public. 
    - LocalIssue - Belongs to the public. To the community. 

- What actors exist? That is, entities who have privelege to create content on the website
    - Site Admin
    - Site Moderator
    - Group
    - User
    - PublicFigure - Will every user have a 'PublicFigure'?
    - Team
- 1) Who can create/edit it?  2) Who can see it? 3) Who owns it?
    : Site Admin & Site Moderators can Create/Edit/View anything. (or just about)
    : The owner can always see their own things
    - Actors
        - Site Admin: 1) Web Developer. 2) Web Developer, yes. Public, maybe
        - Site Moderator: 1) Site Admin. 2) Site Admin, Web Developer, Public
        - Group: 1) Site Admin, Site Moderator, User. 2) The public. Everybody
        - User: 1) An individual. 2) Site Admin. Site Moderator. The individual
        - PublicFigure: 1) A User creates their own. The community can create one. Site Admin & Site Moderators 2) the general public
            - Internal review process will ensure that only folks who are ACTUALLY Public figures can be posted by the community
        - Team: 1) A group. A user?? 2.) The Group. Members of the team

    - Digital Representation of Real world thing:
        - LocalIssue: 1) Anybody. 2) Everybody. 3) The Community
        - JobOpportunity: 1) Group. 2) Everybody. 3) The Group
        - VolunteerOpportunity: 1) Group. 2) Everybody. 3) The Group
        - Action: 1) User. Group. Team. 2) Public || Group-members || Team-members. 3) The Creator
        - RequestForHelp/PersonalIssue: 1) A User. 2) Groups approved by the User. 3) The User
        - Event: 1) User. Group. Team. 2) Public 2) The Creator
    - Digital
        - Category 
        - BlogPost
        - Article
        - Meme
        - Image
        - Comment
        - Discussion
        - FeedItem
        - MiniBlogPost (such as TheGoodImDoing)
        - Review
        - Link (to external resource)

- Can be: 1) Posted to? 2) Attached To?
    - LocalIssue: 1) The public. 2) Cause, Event, Group, PublicFigure??, Action, VolunteerOpp, JobOpp, Action
    - Action: 1) A Cause. 2) Cause, Event, Group, User, PublicFigure, VolunteerOpp, JobOpp
    - Event: 1) A group, A cause, 


This posted to vs. attached to is very confusing. Attached to... is more general. Something can be attached by several means: Subscription, involvement in, opposition to, etc. Posting to means "This is the place where it belongs. This Entity owns the THING."


- Group
    - Specific to a particular group
        - Name
        - Description
        - contact information
        - team members
        - address(es)
        - hours (json? Very gross. Preferably a relationship)
        - photos
        - 
    - Relationships:
        - Group owns volunteer_opportunities (limit 3)
        - Group owns|subscribes_to|is_active_in local_issues (limit 5)
        - Group owns|is_active_in|subscribes_to requests_for_help (limit 15?)
        - Group belongs_to categories (limit 3?)
        - Group owns|is_active_in|subscribes_to|supports|opposes causes 
        - Group owns|co_organizes|subscribes_to| events (many-to-many)
        - Group owns|subscribes_to blog_posts
        - Group owns|subscribes_to articles
        - Group owns|subscribes_to discussions
- Event
    - Event is_owned_by Group
    - Event is_subscribed_to_by Users | Groups
- Team 
    - Team belongs to group | cause | event | local_issue | 

- Actions (is this a sub-type of event? Is Event a sub-type of Actions?)
- Cause
- VolunteerOpportunity
- JobOpportunity
- LocalIssue
- RequestForHelp
- Category  (race, environment, health, wildlife, & more) - there will also be sub-categories
- User
- Review
- Discussion
- Link (to an external resource / sameAs to another site)
- CommunityArticle
- TheGoodImDoing
- Comment
- BlogPost
- Meme
- Image
- FeedItem (Every other entity, which wishes to be in a feed, has a FeedItem). A FeedItem is the primary interactive source of the website. Every FeedItem can be commented on. A Group has a FeedItem. On the Group Page, comments are loaded from Group->FeedItem->comments. So the Group itself has one feed item. If an update is made to the group (name change, image change, etc), this FeedItem & comments remain the same.  Additionally, when the update is made, an additional BlogPost is created. The BlogPost has a FeedItem as well. The BlogPost says that "The Group was updated. [here's the updated FeedItem view]". Then the group will have Group->BlogPosts->FeedItems (to get all feed items from blogposts)





##### JUST WORKING IT THROUGH

## Notes, rambles
- The tech spec will take all the features from the product-spec &... well, make them more technical.
- There will need to be a tag-manager, category manager



## Features:
    - Groups (incorporated or not) can create professional pages that represent their brand
    - The general public can contact groups through an integrated customer-service-system
    - Cause-oriented events can be posted
        - Groups can share cause-oriented events (including fundraisers?)
        - Individuals can share cause-oriented events
        - Event information can be extracted from other online-event sharing platforms (such as Facebook & EventBrite)
    - Volunteer opportunities can be posted
        - Individuals can share volunteer opportunities for events they post
        - Groups can share volunteer opportunities for individual events and for the group at-large
    - Job opportunities can be posted
        - Must be on-topic
        - Only allowed for incorporated groups
        - Paid feature?
    - Causes can be posted by groups and/or individuals
    - Categories can be created by individuals or groups. Causes, groups, volunteer opportunities, and events will specify their categories
    - Community moderation features will be present. 
    - Content-Creators/Moderators will be able to post causes, groups, volunteer opportunities, and more... to help with visibility & reach
    - Discussions can be hosted on the site (forum style)... including discussions ABOUT groups/causes/events.
    - List links to the same-item (group/cause/event) on different sites (be a centralized hub!)
    - Reviews of groups, causes, events.
    - A meta sub-site to help the community contribute to what the site should be
    - An intuitive search & filter system with pleasing aesthetics
    - Hosting of articles written by community members (kind of like medium.com or H&R's letters-to-the-editor)
    - Allow anyone to post good that they're doing (photos required?)
    - Post local issues
    - Post requests for assistance
    - Teams software: Sharing resources amongst your a team of people

## Deferred Features
    - All communications can be handled ON the site. In v1, meaningful engagement will take place on platforms where groups are already active
    - Volunteer & job applications hosted on the site
    - Accounting software for organizations
    - Volunteer management software for orgs
    - Project management software for orgs
    - Google Docs alternative for orgs, & maybe something more structured for organizationally-required docs
    - Legal status software to ensure all paperwork is filed with the right entities on-time
    - Accept donations on the website (2.2% + $0.30 per transaction through paypal) See https://www.givelively.org/fees-and-disbursement 
    - Discussion email lists & announcement email lists for orgs

## Potential Paid Features
    - Email discussion groups & email announcement lists (but really, am I trying to compete with Mailchimp?)
    - Listing volunteer opportunities is free, but volunteer application processing could be paid. Orgs could sponser processing of volunteer applications (so volunteers don't have to pay to volunteer)
    - Limit # of volunteer opportunities to 5 for non-pro groups
    - Limit # of monthly events to 2 for non-pro groups
    - Limit # of online images to 10 for non-pro groups
    - Limit # of causes to 3 for non-pro groups
    - Limit # of associated links to 6 for non-pro groups

## Monetization ideas
I'd personally like to keep the bulk of monetization online. I'm a programmer & I don't want to step away from code & feature-updates to do cold-calling, event organizing, or other direct-involvement with the monetization process. I'd like to hire somebody to work on those efforts, though. But... gotta make money first to do that.
    - Sell integrated customer service software
    - Sell integrated accounting software
    - Host job listings
    - Provide cloud-storage of organizational documents
        - Charge more for additional security / encryption features
    - Start a coalition with a membership fee. 
    - Accept donations on the website (but this requires me to take donated-money away from NFPs!)
    - Advertise on the website (but this doesn't support the mission of the org, and many ad-networks use invasive tracking practices)
        - Pay to remove ads / Ads are disabled after we hit our monthly goal
        - Restrict ads to particular areas, like... pay-to-be-listed in the newsleter or have a reference to our sponsors & list them on a Sponsors' page.
    - Free-first, Pay-for-more: Have a suite of features that can be purchased by local non-profits to give them more tools, improve their reach with folks (?? advertising...)... This should be the primary monetization strategy, and I need to figure out what features might be paid for
    - Web-development services: 
        1) An org, for a small fee, can have their own domain to host the same content that is found on the site
        2) For a larger fee, this same software can run with a custom theme on their own domain
        3) For another fee, there can be content-differences between DecaturCares & their own domain. By default, the content would be the same (though DecaturCares would rel="canonical" over to the hosted domain???)
        4) For the largest fee, an entire custom website can be created. 
    - Patron / Sponsor program - Basically people can give us money to support our mission... even though this will be a for-profit business. Sponsorship can also serve as a form of advertising...
    - Associated event organizing - volunteer fairs, local-action meetups, trainings on how-to-run a non-profit, government-paperwork workshops
    - A weekly / monthly newsletter of some sort
    - A VIP version where you get information before non-VIPs
    - Organize professional fundraising events for groups
    - Provide additional marketing services 
        - push to Yelp, Facebook, Google Places, VolunteerMatch, EventBrite, H&R Community Calendar, etc... Create events, business pages, and more to increase visibility.
        - Design marketing materials for events, like flyers
    - Partner-ship fundraisers: Work with a local business to say... 10% of proceeds on a particular day will go to DecaturCares
    - By starting a fund-account for which money will go to NFPs &... 3% of all donations go to DecaturCares
    - An ethical-certification program that rates businesses on their ethical performance & lists ethical businesses on the website (but this is probably off-topic)
    - A sibling website for local businesses
    - Allow community members to sponsor groups by purchasing on-site-services for a group