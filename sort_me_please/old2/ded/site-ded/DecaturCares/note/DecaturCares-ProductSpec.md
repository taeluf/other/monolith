# Decatur Cares: Product Specification
**Author:** Reed Sutman, rsutman@gmail.com
**Date Started:** Apr 15, 2020


## Gist  
This spec will detail the vision & mission, high-level goals, major features for the website, important notes, deffered/delayed features, monetization strategies, the target audience, Out-Of-Scope ideas, Open Questions, Market Research, and future plans. Another, more detailed technical specification will be written, which explains how the product will be implemented.

**Vision:** Decatur's communities will be strengthened by online organizing software  
**Mission:** To create software that will benefit local activists, non-profits, and other cause-oriented organizers.
**Goals:**  
    - To provide all of the tools an organizer needs to make their cause effective
    - To improve visibility of local cause-oriented groups, events, and causes
    - To increase local volunteering
    - To improve the quality of life in Decatur, IL
    - Increase local citizen (& non-citizen) engagement with local causes / local actions

## Requirements
    - Operate with a sustainable for-profit business model

## Next Steps
    1) Review the product specification & revise.
    2) Write a technical specification which details: SiteMap, Database Schemas, relationships, and features
        - The first version will only detail the pre-monetization version.
    3) Review Tech Spec & Product Spec. Figure out what monetization strategies will fit in & update the product spec
    4) Update the tech spech to match the confirmed monetization strategies from the updated product spec
    5) Do market research - reach out to local leaders, activists, non-profits to see what they think
    6) Integrate feedback into the final specs
    7) Create a reasonable timeline, consider my life-situation and whether I can afford to take this on.
    7) Get coding, if it's viable.

## Target Audience
- Citizen Activists - individuals who lead local causes
- Potential Volunteers - individuals who would like to get involved in local causes
- Potential Donors - individuals who may want to give money (or other resources) to local orgs / causes
- News organizations - News organizations interested in stories about local causes & good-things going on
- Non-profit organizations - Incorporated orgs who are cause-oriented (as NFPs should be)
- Religious organizations ??? Religious groups often have causes. I'm not sure we want to support the faith-aspect, but definitely do want to support the cause-related efforts of their organization. ... Maybe Faith-orgs can post their litter pickups, but not their Sunday Mass??
- Political Candidates ??? It's debateable whether a political campaign counts as a cause... or not. Since the goal of the site is to increase engagement with local action && a political campaign IS... technically an action. This may fit in the scope, but it is not the core purpose.
- Political Groups - Political GROUPS (aside from campaigns) often have cause-oriented efforts. The local DSA chapter, for example, has many causes on which they perform direct action (protests, litter cleanups, FOIA requests, etc). So their direct-action is definitely in the scope... but some of their efforts (Raise money for candidate L) may not be in the scope of the site.
- Keyboard Warriors ??? while sometimes very valuable parts of cultural change & capable of generating action... are not necessarily in the scope of this project. This site is intended to create in-person action, not digital-action. The reason for this is my personal interest in building local community and creating human connection. Keyboard Warriors could definitely be a valuable part of the website, though. They could help keep information up to date, expose actions that need to be taken and such... Keyboard warriors, I think, are less of a target audience, and moreso... people I'd want to bring on-board to help grow this online-effort...

## Open Questions
- Will there be limits on what causes will allowed on the website? 
    - Right-to-choose (abortion) is a very hot-topic. Personally, I believe very strongly in the woman's right to choose & do not want to allow anti-choice/pro-life movements on the website. However, this would be discriminatory based upon my own beliefs and would aid in reducing the democratic-value of the site. Furthermore, allowing a pro-life movement could both: Increase participation in the anti-choice effort AND increase opposition to the pro-life effort. This is more democratic than saying "You can't do that here."
    - Proud Boys, National Socialist Movement, and other largely race-based groups/causes are... not okay. We have a right to civilized protest. Folks can organize waiving Nazi flags, and they can organize waiving American flags (or whatever). I want this site to be inline with the ideals of freedom, but I also want it to be inline with my ideals about hate, equality, fairness, transparency, etc...
    - Partisan causes are... not necessarily something I want on the site. (but it might be in scope?)
    - There's a lot to think about here. One approach could be to detail things that are specifically NOT allowed (discriminatory / hate-based groups, causes that seek to remove personal-freedoms, and other values/ideals). This way we're not saying "You can't have a pro-life rally & host it here." We're saying "If you have a pro-life rally, it can't involve hateful speech like 'You'll go to hell'". This is a tough one.
- Will it be all business? Could there be a memes section? Or something to that effect? It might increase engagement with the site and enjoyment of it, but it's definitely not ON-mission. If the overall result is increased engagement, then that's a good thing... but it could also bring the site off-topic, which would be a problem.
- How much can we spend (on advertising) to get one member of the public to open our website? To sign up?

## Market Research: Is this service needed?
1) My local experience - I've been involved, on and off, in the local community for about 2 years now. Before starting my own non-profit, I looked for volunteer opportunities & couldn't find them. I looked for cause-oriented groups & jobs & had no luck finding them. After being involved for awhile, i figured out what search terms to use & how to find NFPs on Facebook, and I made a lot of personal connections that informed me as to what's going on locally. It does seem the local media is decent about covering local causes, but these are not well-organized and cannot be meaningfully engaged with & are not a professional presentation of the organization.
2) Online review of
    - Activism social networks: I found one, in Germany, that looks really cool. It has some great features, but there's a login-wall before you can look at anything. I found a couple others that had similar efforts, but they were to function more like a social network than a public resource. I first want to provide a public resource, and secondly want to provide networking. And the networking is for serious purposes only (see 'Open Questions')
    - GalaxyDigital: A volunteer-organizing software. United Way uses this locally and there are quite a few organizations on it. There's some things it does really well. If you're on the site looking for a volunteer opportunity, it's pretty darn easy to find it. But it does not come up in search results
    - VolunteerMatch & GreatNonProfits: They look like they have a good amount of data about volunteer opportunities, but it's not a great one-stop-shop, it's not well-organized... It's an ugly list of search-results. It's too... data. It's too generic. And there's no local engagement with orgs around here, so it doesn't seem that organizations are well setup on this site. And I didn't know about them aside from doing online searches.
    - GreatNonProfits: Actually looks like an incredible site for non-profits. But it's missing something. It's a site you go to to find information, not to get involved in a community. It's there to look at non-profits, not causes themselves. It's too busy (poor design). It feels too much like a web-search... like "We scraped this data from the internet and categorized it for you so you can search a little better." I'm not trying to make a search engine for non-profits. I'm trying to create a community-building tool for causes, groups, and individuals. I have a wider scope. The existence of groups like GreatNonProfits is an indication that there is such a need in the world. They would be a competitor & I can be a very good competitor.
3) Presenting the idea to local leaderops & activists for feedback - Once I have made more progress with the spec & am clearer about what this project is supposed to accomplish, I can prepare some... slightly more marketable information than this giant product spec. Then I can reach out to local folks and see what they think & if they'd use something like this.

## Out-of-scope

## Features:
    - Groups (incorporated or not) can create professional pages that represent their brand
    - The general public can contact groups through an integrated customer-service-system
    - Cause-oriented events can be posted
        - Groups can share cause-oriented events (including fundraisers?)
        - Individuals can share cause-oriented events
        - Event information can be extracted from other online-event sharing platforms (such as Facebook & EventBrite)
    - Volunteer opportunities can be posted
        - Individuals can share volunteer opportunities for events they post
        - Groups can share volunteer opportunities for individual events and for the group at-large
    - Job opportunities can be posted
        - Must be on-topic
        - Only allowed for incorporated groups
        - Paid feature?
    - Causes can be posted by groups and/or individuals
    - Categories can be created by individuals or groups. Causes, groups, volunteer opportunities, and events will specify their categories
    - Community moderation features will be present. 
    - Content-Creators/Moderators will be able to post causes, groups, volunteer opportunities, and more... to help with visibility & reach
    - Discussions can be hosted on the site (forum style)... including discussions ABOUT groups/causes/events.
    - List links to the same-item (group/cause/event) on different sites (be a centralized hub!)
    - Reviews of groups, causes, events.
    - A meta sub-site to help the community contribute to what the site should be
    - An intuitive search & filter system with pleasing aesthetics
    - Hosting of articles written by community members (kind of like medium.com or H&R's letters-to-the-editor)
    - Allow anyone to post good that they're doing (photos required?)
    - Post local issues
    - Post requests for assistance

## Deferred Features
    - All communications can be handled ON the site. In v1, meaningful engagement will take place on platforms where groups are already active
    - Volunteer & job applications hosted on the site
    - Accounting software for organizations
    - Volunteer management software for orgs
    - Project management software for orgs
    - Google Docs alternative for orgs, & maybe something more structured for organizationally-required docs
    - Legal status software to ensure all paperwork is filed with the right entities on-time
    - Accept donations on the website (2.2% + $0.30 per transaction through paypal) See https://www.givelively.org/fees-and-disbursement 
    - Discussion email lists & announcement email lists for orgs

## Potential Paid Features
    - Email discussion groups & email announcement lists (but really, am I trying to compete with Mailchimp?)
    - Listing volunteer opportunities is free, but volunteer application processing could be paid. Orgs could sponser processing of volunteer applications (so volunteers don't have to pay to volunteer)
    - Limit # of volunteer opportunities to 5 for non-pro groups
    - Limit # of monthly events to 2 for non-pro groups
    - Limit # of online images to 10 for non-pro groups
    - Limit # of causes to 3 for non-pro groups
    - Limit # of associated links to 6 for non-pro groups

## Monetization ideas
I'd personally like to keep the bulk of monetization online. I'm a programmer & I don't want to step away from code & feature-updates to do cold-calling, event organizing, or other direct-involvement with the monetization process. I'd like to hire somebody to work on those efforts, though. But... gotta make money first to do that.
    - Sell integrated customer service software
    - Sell integrated accounting software
    - Host job listings
    - Provide cloud-storage of organizational documents
        - Charge more for additional security / encryption features
    - Start a coalition with a membership fee. 
    - Accept donations on the website (but this requires me to take donated-money away from NFPs!)
    - Advertise on the website (but this doesn't support the mission of the org, and many ad-networks use invasive tracking practices)
        - Pay to remove ads / Ads are disabled after we hit our monthly goal
        - Restrict ads to particular areas, like... pay-to-be-listed in the newsleter or have a reference to our sponsors & list them on a Sponsors' page.
    - Free-first, Pay-for-more: Have a suite of features that can be purchased by local non-profits to give them more tools, improve their reach with folks (?? advertising...)... This should be the primary monetization strategy, and I need to figure out what features might be paid for
    - Web-development services: 
        1) An org, for a small fee, can have their own domain to host the same content that is found on the site
        2) For a larger fee, this same software can run with a custom theme on their own domain
        3) For another fee, there can be content-differences between DecaturCares & their own domain. By default, the content would be the same (though DecaturCares would rel="canonical" over to the hosted domain???)
        4) For the largest fee, an entire custom website can be created. 
    - Patron / Sponsor program - Basically people can give us money to support our mission... even though this will be a for-profit business. Sponsorship can also serve as a form of advertising...
    - Associated event organizing - volunteer fairs, local-action meetups, trainings on how-to-run a non-profit, government-paperwork workshops
    - A weekly / monthly newsletter of some sort
    - A VIP version where you get information before non-VIPs
    - Organize professional fundraising events for groups
    - Provide additional marketing services 
        - push to Yelp, Facebook, Google Places, VolunteerMatch, EventBrite, H&R Community Calendar, etc... Create events, business pages, and more to increase visibility.
        - Design marketing materials for events, like flyers
    - Partner-ship fundraisers: Work with a local business to say... 10% of proceeds on a particular day will go to DecaturCares
    - By starting a fund-account for which money will go to NFPs &... 3% of all donations go to DecaturCares
    - An ethical-certification program that rates businesses on their ethical performance & lists ethical businesses on the website (but this is probably off-topic)
    - A sibling website for local businesses
    - Allow community members to sponsor groups by purchasing on-site-services for a group

## Future plans
- Expand nationally.
    1) Build and test in Decatur.
    2) Update & improve
    3) Adapt, publish, and test in Bloomington-Normal
    4) Repeat in one-other community
    5) Start hiring local-leaders to launch in other communities
    6) Publish a national-website which hosts all the communities (and generally directs to a subdomain of the national site, as we want to keep things local to an area)        
        
## Notes
- Causes are extremely specific, such as "Improve the drinking water quality in Decatur" or "Build the Recycle center" or "
- Community moderation will prevent abuse of cause-creation, category-creation, event-creation, group-creation. The privelege-earning model that StackOverflow uses will likely be adapted for use here.
- https://activist.network/explore/ provides some of the real-world infrastructure, but does NOT provide a social-organizing website...
- https://human-connection.org/en/  is doing some of what I want, but... it's missing something
- activist.local (or local.activist?) Something... was a sad sad presentation of an ~"activists social network"
- Discussions must be on-topic... 
- https://volunteermidillinois.galaxydigital.com/ is another competitor
        
## Considerations
- Can causes be explicitly political? "Remove the mayor from office" or "Elect THIS CANDIDATE" or "Pass this ordinance"
- Can any random user post a group? Do they need to earn priveleges first?
- Can we forbid certain causes from being posted? (such as NSM, anti-abortion rallies, and others that go against MY core values?)
- Can individuals communicate with one another?
- Are there going to be "contacts/friends" lists? Can people be @Mentioned? Are #tags allowed/useful? Does this function, at all, like a social network? 
- What is "on-topic" for the site?



## External Resources
- A guide for writing Tech Specs: https://codeburst.io/on-writing-tech-specs-6404c9791159 