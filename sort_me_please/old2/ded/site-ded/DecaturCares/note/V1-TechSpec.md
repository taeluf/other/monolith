# Sitemap

This sitemap is incomplete. It still needs:
    - payment pages
    - edit & create pages
    - save pages
    - A review for completeness & general goodness.
    - A review of /event/EventName/ (should it redirect to the host-group's event url?)
- /user/ - all the user-login pages
    - /user/profile/ - Very basic page. Lists your Group (if you have one)
- /GroupName/ - The primary page for a particular group.
    - /GroupName/volunteer/
        - /GroupName/volunteer/opp-slug/
    - /GroupName/causes/ 
        : individual causes redirect to /cause/CauseName/
    - /GroupName/events/
        - /GroupName/events/event-slug/ (or should it go to /event/EventName/?)
    - /GroupName/blog/
        - /GroupName/blog/entry-slug/
    ?? - /GroupName/info/ - Contact info... address... that kinda stuff? (might just all go on the main group page)
- /group/ - the feed of groups
    - /group/GroupId/ redirects to /GroupName/
- /event/ - all the events
    - /event/EventName/ - Information about an event
    - /event/EventName/groups/ - (or .../hosts/) the host & co-hosts of the event (in a more long-form view than on the event page itself)
    - /event/EventName/volunteer/ 
    - /event/EventName/blog/
- /cause/
    - /cause/CauseName/
    - /cause/CauseName/groups/ - groups interested in the cause
- /volunteer/ - all volunteer opps
    - /volunteer/GroupName/ - redirects to /GroupName/volunteer
    - /volunteer/volunteer-opp-id/ - redirects to /GroupName/volunteer/opp-slug/
- /blog/ - all blog posts from all orgs
    - /blog/GroupName/ - redirects to /GroupName/blog/
    - /blog/GroupName/BlogEntry - redirects to /GroupName/blog/BlogEntry

