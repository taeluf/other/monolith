
# Shop Decatur vs. Decatur cares
    Shop Decatur seems to have a simpler business plan. It's easy to understand. It's just so straightforward. There could be good things about it... (the ethical scorecard), but it ultimately supports a system that I have great disdain for. I think it's something that should exist (could make it easy to find vegan restaurants locally, for example), but working on it myself will be a huge time-sink & I don't think it's where I want to go right now.
    
    Decatur cares... can be a bit sloppy. It does not have to start with online sales, so there's fewer MinVP requirements. ShopDecatur requires online sales (does it? Can I outsource that?), thanks to the COVID situation.

    I think financially, ShopDecatur is a safer & better idea. But it will likely take longer to publish version 1, the standards will likely be higher, as it's a sales-oriented area. It's not the complexity that I have a problem with. Helping people buy stuff does NOT excite me. The ethical scorecard does! But... the ethical scorecard... Well. If I re-center the project around ethical-shopping (in Decatur), instead of decatur-shopping (while ethical), I think I'll be better off. If I really center on the ethical-idea, it might be viable (for my passions)


# Major (pain) Points of Development
- Shop Decatur
    - Figuring out what the ethical scorecard entails.
    - Shopping Cart & online sales
    - Getting a critical-mass of businesses, thus making it a viable product
    - The legal agreements for sellers & buyers
    - Search
    - Overall design
    - Customer support system
- Decatur Cares
    - Search
    - Overall design
    - Online community features (forum / group blog / group chat / comments)
    - Customer support system
    - Volunteer opportunities
    - Events + calendar
    - Accepting donations
    - Allowing online sales
    - Defining free & pay-to-unrestrict features
    - Onboarding local organizations (setting up their basic pages, getting them to create accounts & manage their pages,)
    - Blogging/Content Creation (for individual groups/orgs)


# Major steps
1.) Decide which project to take on (tentative decision is Decatur Cares/Team Decatur/Move Decatur)
2.) Define goals for the project (High-level goals. What are we hoping to accomplish, and why? Think about the real-world benefits)
3.) Do market research (will it be used? Is this needed? What competition is there?)
4.) Design a feature-map (detail each feature the site offers & the gist of how the code is organized)
5.) Design a sitemap (What web-pages will be required to implement all of the features & keep things easy-to-use?)
6.) High-level code outline (what db tables are required, how feature a talks to feature b, How the site interacts with feature c)
7.) Detailed code outline (db table schemas, list(s) of classes & what they do & how they are interacted with externally)
8.) Design the UI (each individual view / page)... atleast wireframes
9.) Design the internals of the outlined code
10.) Create the UI w/ calls to features (which have not yet been created)
11.) Write the internal code. (Do #9 & 10 for one feature, then repeat #9&10 for the next feature & so on)
12.) Plug in data

# Notes
- Throughout planning, keep track of ideas & note whether they're intended for first-release, eventual, or some specific goal. 
- Take good notes & keep them organized. Extract substantive pieces from journal-style notes. 
- Frequently re-evaluate the necessity of certain features, the designs & whatnot
- Keep a roadmap... or anticipated timeline for the project
- Set realistic goals (due dates) & keep to them
- Have patience

# Roadmap, major steps
- Conceptualization: What is the general idea?
- Goal Setting: What specifically will it do for the world?
- Market Research
- Design: How will things look, be organized, and be interacted with?
    - high-level design (site structure & features to include) 
    - detailed-design (appearance, specific UIs)
- Code Plan: An outline/spec of the API for using the code that drives the features
- Code Writing: This will be the longest step, most likely
- Testing (& filling in data)
- Publishing beta
- Publishing first major release
