# What is the minimum viable product?

# What is the minimum profitable product?


How basic am I okay with version-1 being?
    - Is a static directory site okay? Just a directory of organizations? No... I don't think that's enough. 
        Depends what the directory lists. If one is viewing a group, I want them to be able to see volunteer opportunities, causes this group is involved in, contact information. Maybe events?
    - Is a directory of causes necessary for the first version?
    - Does the first version need to be online organizing software? Or can it be, simply, a directory?
        - A directory... exists. There are directories already & I'm trying to turn a directory into an interactive online organizing tool.
    - I think the first version has to have some kind of monetization... For there to be monetization, it needs to be rich with features... or have advertising. If I do a simple directory, I can monetize through ads, but I have no tools to work with.
    - The gist of the site is:
        1) Here's a directory of: cause-oriented Groups, Causes, Volunteer Opportunities, Local Issues, Local cause-related events
        2) Here are tools for a Group to update their own: Group Info Page, Volunteer Opportunities, Events
        3) Here are content organization mechanisms (all managed by Group Owners):
            - Groups can list causes they care about. A cause will list groups who care about it
            // - Groups can list local issues they care about. A local issue will list groups who care about it.
            // - A cause can address a local issue (the cause lists the issue & the issue lists the cause)
            - An event can support a cause and/or a local issue
            - A Group has events they're organizing. 
            - A volunteer opportunity can support: A cause //, and/or a local issue
            - A group can list a volunteer opp on an event
            - An event has a Host (the group who made it) & a list of co-hosts as well
        4) here are ways to view the content:
            - A feed of causes (title, photo, description)
            - Feed of groups (title, photo, description)
            - Feed of volunteer opportunites (title, description, group, maybe photo)
            - feed of events (title, photo, description, group)
            // - feed of local issues
            - A mixed feed w/ custom filtering
            - Individual content pages for each of the above
            ??? - Sign up for our newsletter
        5) Here are ways you can interact with things listed on the site:
            - Contact the Person In Charge for the given group, volunteer opportunity, or event (via their website, phone, email, etc)
            - Contact one of the groups interested in a cause (by navigating to the group & contacting their PIC)
            - Share on other platforms (via share buttons)
        6) Here are ways you can contribute to the site:
            - Create a group. Everything else requires you be a CONFIRMED PIC of a group
            - Create a cause.
            - Create a volunteer opportunity
            - Create an event
            - Blog posts
        7) Here are ways I can make money:
            - Ask for contributions to the site
            - Ask for sponsors & list those sponsors
            - Charge $10 monthly fee if you want to: 
                - List more than: 
                    - 2 events in a month
                    - 2 volunteer opps at a time
                    - 8 photos for a group, 4 photos for an event, 3 photos for a volunteer opportunity
                    - 3 causes that your group "cares about" (You can create any # of causes though as causes are public)
                    - 2 causes that your event "cares about"
                    - 2 causes that your volunteer opportunity "cares about"
                    - For a group, 2 external web links (website+facebook prob), 1 email address, and 1 phone number
                    - 1 external link, 1 email, 1 phone for any event
                    - 1 external link, 1 email, 1 phone for any volunteer opportunity
                    - 4 blog posts per month
                - Have more than 1 content manager (that is, more than 1 site-login for a group)
            - Charge 1-time service fees
                - ?? $10 to verify an organization (& their admin)
                - $20 per Group listing for: Google, Yelp, Facebook & charges for long-term-support too
                - $20 per Place listing for
                - $10 per event listing on FB
                // - $500-$1000 for a custom website //not with version 1. Time needs to be spent on the central site, not the orgs' sites
                    // - unless I partner with a local web-design agency & take a referral fee
                - $10 to get into this month's site-wide newsletter (email, limit 10 listings per month)
            - Advertisements
                - Show them from an ad network
                - pay to hide ads for myself
                - Pay to hide ads for a group's page
                - Reach the monthly revenue goal ($5,000) to disable ads for the rest of the month for everybody

What's required for the first version:
    - A directory of groups (whether incorporated or not) who are cause-driven


Are causes and local issues different?
Let's take local issue out of the first version.

Volunteer Opportunities & events are both owned by their group
