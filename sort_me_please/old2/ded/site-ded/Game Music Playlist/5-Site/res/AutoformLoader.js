
class AutoformLoader extends RB.Autowire {

    attach(){
        // console.log('attached');
        this.node.addEventListener('click',this.clicked.bind(this));
    }
    clicked(event){
        event.stopPropagation();
        event.preventDefault();
        window.location.href = this.node.getAttribute('dfj-autoform-url')+'?id='+this.node.getAttribute('dfj-autoform-id');
    }
}

AutoformLoader.autowire('[dfj-autoform-url]');
