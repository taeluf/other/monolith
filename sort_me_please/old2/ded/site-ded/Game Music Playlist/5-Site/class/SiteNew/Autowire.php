<?php

namespace SiteNew;

class Autowire {


    static protected $allowEdits = false;

    static public function setAllowEdits(bool $allow){
        static::$allowEdits = $allow;
    }

    static public function setupAutowire($freshPackage){
        $freshCompo = $freshPackage->freshCompo;
        $freshCompo->setRuntimeHandler('getAutoformAttributes',[static::class,'getAutoformAttributes']);
        $freshCompo->addViewQuery('//*[@dfj-autoform]',[static::class,'compileInAutoform']);
        // $freshCompo->setRuntimeHandler('isEditAllowed',[static::class,'isEditAllowed']);
    }
    static public function compileInAutoform($doc,$view,$compiler,$node){
        $ph = $compiler->placeholderFor("<?php echo \$this->callHandler('getAutoformAttributes',\$lia,\$this,\$table,\$find,\$rb_object); ?>");
        $node->setAttributeNode(new \DOMAttr($ph));
        $node->removeAttribute('dfj-autoform');
        // $node->setAttribute('dfj-autoform','edited');
        // throw new \Exception("\n\nI need to set up the auto-form functionality. See ".__FILE__." on line 15\n\n");
    }

    static public function getAutoformAttributes($lia,$view,$table,$find,$object){
        $edit = static::isEditEnabled();
        if ($edit===true){
            if ($object!=null){
                $id = $object->id;
            } else {
                $list = $view->callHandler('find',$table,$find);
                if (count($list)>1)$id = "there-was-a-problem-finding-the-object-to-edit";
                else if (count($list)==0)$id = 'new';
                else {
                    $obj = reset($list);
                    $id = $obj->id;
                }
            }
            $lia->view('depend/JSAutowire');
            $lia->send('Res_AddScriptFile',dirname(dirname(__DIR__)).'/res/AutoformLoader.js');
            $form = $view->form();

            $url = $lia->send('Fresh_GetUrl',$form);

            return 'dfj-autoform-id="'.$id.'" dfj-autoform-url="'.$url.'"';
        }
        return '';
    }

    static public function isEditAllowed(){
        if (static::$allowEdits!==true)return false;
        return true;
    }

    static protected function isEditEnabled(){
        if (static::$allowEdits!==true)return false;
        if ($_GET['edit']??false=='true'){

            if ($_SERVER['HTTP_HOST']=='www.decaturforjustice.com'
                ||$_SERVER['HTTP_HOST']=='decaturforjustice.com')return false;


            if ($_SERVER['HTTP_HOST']=='staging.decaturforjustice.com'
                ||$_SERVER['HTTP_HOST']=='dfj.localhost') return true;
                
            return false;
        }
        return false;
    }
}