<?php $this->callHandler('compileAndAddResources');?>
<div id="footer">
    <div id="footer_container" class="BlogSection">
        <div>
            <h4>
                Links
            </h4>
            <ul>
                <li><a href="/coalition-terms/">Terms &amp; Conditions</a></li>
                <li><a href="/contact-us/">Contact Us</a></li>
                <li>Website Icon made by
                    <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a>
                    from
                    <a href="https://www.flaticon.com/free-icon/fist_2913198?term=fist&amp;page=1&amp;position=20" title="Flaticon">
                        www.flaticon.com
                    </a> and modified by DFJ</li>
                <!-- <li><a href="/documents/">Org Documents</a></li> -->
                <li><a href="/thank-you/">Other Attributions</a></li>
            </ul>
        </div>
        <!-- <div> -->
        <!-- <h4> -->
        <!-- Other -->
        <!-- </h4> -->
        <!-- <ul> -->
        <!-- <li><a href="/contact/">Contact Us</a></li> -->
        <!-- <li><a href="/contribute/">Contribute</a></li> -->
        <!-- <li><a target="_new" href="https://facebook.com/MaconZero/">Facebook</a></li> -->
        <!-- </ul> -->
        <!-- </div> -->
    </div>
</div>