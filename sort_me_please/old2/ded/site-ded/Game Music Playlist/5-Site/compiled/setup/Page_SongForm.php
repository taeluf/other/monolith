<?php
        $pattern = '/song-edit/';
        $this->callHandler('addPattern',$this,$pattern);
        $rtr = $lia->compo('Router');
        if ($this->callHandler('isEditAllowed',$this,$pattern))
        {
            $rtr->addCallbackRoute($pattern, $package,
                function($event,$route,$passthru) {
                    extract($passthru);
                    $passthru['pattern'] = '/song-edit/';
                    $passthru['submit_url'] = "/song-edit/submit/";
                    $passthru['id'] = $_GET['id'] ?? 'new';
                    $passthru['event'] = $event;
                    $passthru['lia'] = $event->lia;
                    $passthru['route'] = $route;
                    // print_r($passthru['id']);
                    // exit;
                    foreach ($route->extractables as $key=>$value){
                        $passthru[$key] = $value;
                    }
                    // var_dump($passthru);
                    // exit;   
                    $view = $this;
                    $view->setPassthru($passthru);
                    echo $view;
                }
            );
        }
    ?><?php
        $pattern = '@POST./song-edit/submit/';
        $this->callHandler('addPattern',$this,$pattern);
        $rtr = $lia->compo('Router');
        if ($this->callHandler('isEditAllowed',$this,$pattern))
        {
            $rtr->addCallbackRoute($pattern, $package,
                function($event,$route,$passthru) {
                    // var_dump(get_class($this));
                    $compo = $this->component;

                    $passthru['pattern'] = '@POST./song-edit/submit/';
                    $passthru['redirect_url'] = "/";
                    $passthru['event'] = $event;
                    $passthru['lia'] = $event->lia;
                    $passthru['route'] = $route;
                    foreach ($route->extractables as $key=>$value){
                        $passthru[$key] = $value;
                    }
                    $compo->submit('Page_Song', $passthru, $_POST);
                }
            );
        }
    ?>