
/* begin file Autowire-full.js */



if (typeof RB == typeof undefined)var RB = {};
/** This is an old version of the notes. You should view the docs instead.
 * 
 * This allows you to declare a class in Javascript, list events on an HTML node, and have functionality from the JS class automatically wired to the node.
 * 
 * 
 * 
 * The basic functionality is:
 *  - Extend RB.DomWire with your JS class
 *  - To auto-wire call YourClassName.autowire();
 *      - AutoWiring is done when the page is finished loading, NOT at the time you call autowire()
 *      - By Default looks for rb-YourNamespacedClassName on DOM Elements
 *      - To customize, you may: 
 *          - call YourClassName.autowire('DomeClass');
 *          - declare YourClassName.querySelector = 'DomClass';
 *          - declare YourClassName.getquerySelector = function(){return 'DomClass';};
 *      - NOTE: Precedence is given to autoWire('DomClass'), then YourClassName.querySelector, then YourClassName.getquerySelector()
 *  - Set Event methods in YourClass corresponding to JS Event Listeners.
 *      - Inside your class, set methods like `onclick(event)`, `mouseover`, `mouseout`, etc & they will be auto-wired to Dom Nodes
 *      - Ex: class YourClass extends RB.DomWire {  onclick(event){console.log(this);    console.log(event);   console.log(this.node)};
 *          - `this` refers to the instance of YourClass
 *          - `this.node` refers to the node which the listener was set on
 *          - `event` refers to the javascript event that was triggered
 *              - `event.target` may be different from `this.node` due to event propagation
 *  - Methods are assigned using theNode.addEventListener. 
 *      - You may declare this.eventMode = 'static' in your __construct() method to use theNode.onclick = YourClass.onclick;
 *  - You are encouraged to override the methods __construct() && __attach() in your subclass. The super method does not do anything
 *      - __construct() is called before attach()
 *      - __attach() is called AFTER attach()
 *      - You may override constructor() && attach() but it is NOT recommended & super method will need to be called
 *  - Set any additional methods and paramaters that you like, 
 *      - except for
 *          params: node, eventNode
 *          methods: getChildMethods, attachMethod (as well as the methods mentioned above)
 * 
 */
RB.Autowire = class {

    constructor(node){
        this.context = 'default';
        RB.Autowire.list.push({'node':node,'obj':this});
        this.name = this.name || this.constructor.name;
        this.eventMode = 'addEventListener';
        this.node = node;
        this.__construct();
        this.attach();
        this.__attach();
        this.didAttach();
    }
    __construct(){}
    __attach(){}
    attach(){
        const childMethods = this.getChildMethods();
        for (const methodName of childMethods){
            this.attachMethod(methodName);
        }
    }
    didAttach(){}   
    getChildMethods(){
        const methodNames = RB.Tools.getObjectMethodNames(this);
        const rootMethods = RB.Tools.getObjectMethodNames(RB.Autowire.prototype);
        const childMethods = methodNames.filter(method => rootMethods.indexOf(method)<0);
        return childMethods;
    }
    attachMethod(methodName){
        if (methodName.substring(0,2)!=='on')return false;
        const method = this[methodName].bind(this);
        if (this.eventMode=='addEventListener'){
            const eventName = methodName.substring(2);
            this.node.addEventListener(eventName,method);
            return true;
        } else if (this.eventMode=='static'){
            this.node[methodName] = method;
            return true;
        } else {
            throw "event mode must be 'addEventListener' or 'static'";
        }
    }
    nodeContext(node,name='default'){
        return name+"-"+RB.Tools.objectId(this.node.parentNode); 
    }
    get(objectName,contextName=null){
        if (contextName==null)contextName = this.context.slice(0,1);
        const map = RB.Autowire.contextMap[contextName];
        const values = map[objectName];
        if (values==null||values.length==0)return [];
        else return values;
    }
    didAttach(){
        const map = RB.Autowire.contextMap || {};
        if (typeof [] != typeof this.context)this.context = [this.context];
        for (const name of this.context){
            map[name] = map[name] || {};
            map[name][this.name] = map[name][this.name] || [];
            map[name][this.name].push(this);
        }
        RB.Autowire.contextMap = map;
        const proto = this.constructor.prototype;
        let keys = [];
        let obj = this.constructor.prototype;
        
        do keys = keys.concat(Object.getOwnPropertyNames(obj));
            while ((obj = Object.getPrototypeOf(obj))!=Object.prototype);
        const props = keys.filter(function(value,index,self){return self.indexOf(value)===index;});
        
        for (const prop of props){
            if (prop.charAt(0)=='_'&&prop.charAt(1)!='_'){
                const name = prop.slice(1);
                const ret = this[prop]() || name;
                Object.defineProperty( this, name, {
                            get : function(property){
                                if (typeof [] == typeof property){
                                    let i = -1;
                                    let output = '';
                                    for (const val of property){
                                        i++;
                                        if (val==null)continue;
                                        output = val;
                                        break;
                                    }
                                    const contextEntry = this.context[i];
                                    return this.get(output,contextEntry);
                                } else {
                                    return this.get(property);
                                }
                                
                            }.bind(this,ret)
                        } );
                const oneName = 'one'+name.charAt(0).toUpperCase() + name.slice(1);
                Object.defineProperty( this, oneName, {
                    get : function(property){
                        if (typeof [] == typeof property){
                            let i = -1;
                            let output = '';
                            for (const val of property){
                                i++;
                                if (val==null)continue;
                                output = val;
                                break;
                            }
                            const contextEntry = this.context[i];
                            const array = this.get(output,contextEntry);
                            if (array!=null&&array.length>0)return array[0];
                            else return null;
                        } else {
                            const array = this.get(property);
                            if (array!=null&&array.length>0)return array[0];
                            else return null;
                        }
                        
                    }.bind(this,ret)
                } );
            }
        }
    }
    send(url,data,responseMethod,method="POST"){
        var req = new RB.Request(url,method);
        req.handleJson(responseMethod.bind(this));
    }

    static autowire(querySelector=null) {
        RB.Autowire.readyCount++;
        RB.Autowire.ready = false;
        if (querySelector!=null)this.querySelector = querySelector;
        RB.Tools.onPageLoad(this.wire, this);
    }
    static wire(){
        const nodes = this.getNodes();
        const pending = [];
        for (const node of nodes){
            const obj = this.wireNode(node);
            pending.push(obj);
        }
        RB.Autowire.readyCount--;
        RB.Autowire.ready = RB.Autowire.readyCount===0;
        if (RB.Autowire.ready){
                for (const obj of pending){
                    if (typeof obj.__ready !== typeof function(){})continue;
                    obj.__ready();
                }
                for (const obj of RB.Autowire.readyPending){
                    if (typeof obj.__ready !== typeof function(){})continue;
                    obj.__ready();
                }
                RB.Autowire.readyPending = [];
        }
    }

    static fromNode(nodeObj){
        for (const row of this.list){
            if (row.node===nodeObj)return row.obj;
        }
    }
    static getQuerySelector(){
        return this.querySelector || '.'+(this.className || this.name);
    }
    
    static getNodes(){
        const querySelector = this.getQuerySelector(); 
        let nodes = [];
        try {
            nodes = document.querySelectorAll(querySelector);
        } catch (e){
            nodes = [];
        }
        return nodes;
    }
    
    static wireNode(node){
        const obj = new this(node);
        if (RB.Autowire.ready
            &&(typeof obj.__ready === typeof function(){})){
            obj.__ready();
        } else if (!RB.Autowire.ready){
            // should the readycount be incremented here???
            // I don't think so
            RB.Autowire.readyPending.push(obj);
        }
        return obj;
    }
}
RB.Autowire.readyCount = 0;
RB.Autowire.ready = true;
RB.Autowire.readyPending = [];

RB.Autowire.list = [];






RB.Tools = class {

};
RB.Tools.onPageLoad = function (func, thisArg, ...args) {
    if (window.readyState == "complete") {
        func.apply(thisArg, args);
    } else if (window.addEventListener != null) {
        document.addEventListener("DOMContentLoaded", function () {
            if (document.readyState == "interactive") {
                func.apply(thisArg, args);
            }
        });
    } else {
        window.onload = function () {
            func.apply(func, args);

        };
    }
}
RB.Tools.getObjectMethodNames = function(object){
    const properties = new Set();
    let currentObj = object;
    do {
        Object.getOwnPropertyNames(currentObj).map(item => properties.add(item))
    } while ((currentObj = Object.getPrototypeOf(currentObj)))
    return [...properties.keys()].filter(item => typeof object[item] === 'function')
}

RB.Tools.objectId = function(object){
    this.objectMap = this.objectMap || new WeakMap();
    if (this.objectMap.has(object)){
        return this.objectMap.get(object);
    }
    
    this.idMap = this.idMap || {};
    this.counter = this.counter || 0;
    const id = object.constructor.name+":"+(new Date()).getMilliseconds() + "-" + Object.keys(this.objectMap).length + "-" + this.counter++ + "-" + Math.random();
    this.idMap[id] = object;

    this.objectMap.set(object,id);
    
    return id;
}.bind(RB.Tools);

RB.Tools.objectFor = function(id){
    return this.idMap[id];
}.bind(RB.Tools);





if (RB===undefined)var RB = {};

RB.Dom = class {

    static cycle(node,attribute,value=null){
        if (value==null
            &&node.hasAttribute(attribute)){
                node.removeAttribute(attribute);
        } else if(value==null){
            node.setAttribute(attribute,'');
        } else {
            // const index = value.indexOf(node[attribute]);
            // index++;
            // index = index%value.length;
            node[attribute] = value[ (1+value.indexOf(node[attribute]))%value.length ];
        }
    }

    static is(tagName,node){
        if (node.tagName.toLowerCase()==tagName.toLowerCase())return true;
        return false;
    }
}

if (typeof RB === 'undefined')var RB = {};
RB.Request = class {
    constructor(url, method){
        this.params = {};
        this.url = url;
        this.method = method || 'POST';
    }
    put(key,value){
        if (key in this.params){
            this.params[key] = (typeof this.params[key]==typeof []) ? this.params[key] : [this.params[key]];
            this.params[key].push(value);
        } else {
            this.params[key] = value;
        }
    }

    handleJson(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        const submitData = {method: this.method, mode: "cors"};
        if (this.method=='POST')submitData['body'] = formData;
        fetch(this.url, submitData).then(res => {
            return res.json();
        }).then(json => {
            func(json);
        });
    }
    handleText(func){
        var formData = new FormData();
        for(var key in this.params){
            const param = this.params[key];
            if (typeof param == typeof []){
                for(const val of param){
                    formData.append(key,val);
                }
            } else formData.append(key,this.params[key]);
        }
        fetch(this.url, {
            method: this.method, 
            mode: "cors",
            body: formData
        }).then(res => {
            return res.text();
        }).then(text => {
            func(text);
        });
    }
}
/* end file Autowire-full.js */
