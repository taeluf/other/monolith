# Releasing a new version of the website

## Code-level changes:
- define('LOCAL_STAGING',true);
    - disable edit mode in Site/task/setup.auth/user_login.php
    - disable debug in /Site/task/setup.launch/configure.php
    - change to composer dependency in deliver.php, comment out dependency manager
    - disable error reporting in deliver.php
    - Make sure `ini_set('pcre.jit','0');` is written at top of deliver.php
- run composer update
- delete the sitemap.xml in Site/public
- test the site
- verify there is a new sitemap.xml
- delete 0-error/log

## Database backup:
- backup the database
    - cd /var/www/html/www.decaturforjustice.com/1-files/db; backupdb; 
        - dbname: decfj
        - user: decfj
        - password: badpassword
- replace non-standard collation settings
    ```bash
        latestfile=decfj_20-07-08.sql; #CHANGE THE FILE NAME!!!!!
        cd /var/www/html/www.decaturforjustice.com/1-files/db/
        sed -i 's/utf8mb4_0900_ai_ci/utf8mb4_unicode_ci/g' $latestfile
        sed -i 's/utf8mb4_unicode_520_ci/utf8mb4_unicode_ci/g' $latestfile
    ```

## GIT BACKUP
gitamp, enter commit message

## Push to server
```bash
ssh USER_NAME@decaturforjustice.com
cd ~/pub;
rm -rf staging.decaturforjustice.com
exit; #back to local command line

cd /var/www/html/www.decaturforjustice.com
rsync -avn --exclude "/1-files/img-upload-uncompressed" --exclude=".git" --exclude=".vscode" --exclude="z-old*/" --exclude="old/" --exclude=".github" --exclude="docs/" --exclude="doc/" --exclude="tests/" --exclude="test/" --exclude="testing/" ./ USERNAME@decaturforjustice.com:~/pub/staging.decaturforjustice.com

exit; # Check the dry run before running wet rsync command
rsync -av --exclude "/1-files/img-upload-uncompressed" --exclude=".git" --exclude=".vscode" --exclude="z-old*/" --exclude="old/" --exclude=".github" --exclude="docs/" --exclude="doc/" --exclude="tests/" --exclude="test/" --exclude="testing/" ./ dh_sf4amw@decaturforjustice.com:~/pub/staging.decaturforjustice.com

# Get back in the server
ssh USER_NAME@decaturforjustice.com
# Update database credentials
cd ~/pub/staging.decaturforjustice.com/env/;
# Set credentials to correct server credentials in format HOST_NAME:DB_NAME:USER_NAME:PASSWORD
# HOST_NAME is mysql.decaturforjustice.com
# DB_NAME is decfj_stage for the staging site or decfj for the primary site
# USER_NAME and PASSWORD are not stored digitally. Only on the server
nano credentials-stage.txt
nano credentials-public.txt # These two should be identical, save for the database name


# Update the database from backup file
cd ~/pub/staging.decaturforjustice.com/1-files/db/;
mysql -u USER_NAME -h mysql.decaturforjustice.com -p decfj_stage < NEWEST_BACKUP.sql

# Stay logged in, actually...     # exit; #back to local command line

```

## Test the staging site
    - Is there a sitemap.xml?
    - are SEO verification files present?
    - Do all the pages work?
    - Does a non-existent url show a nice error page?

## Moving to production  

```bash
ssh USER_NAME@decaturforjustice.com
cd ~/pub/staging.decaturforjustice.com/1-files/db/;
mysql -u USER_NAME -h mysql.decaturforjustice.com -p decfj < decfj_20-06-18.sql
cd ~/pub/
rm -rf www.decaturforjustice.com
mv staging.decaturforjustice.com www.decaturforjustice.com

```



rm -rf www.decaturforjustice.com
mv staging.decaturforjustice.com www.decaturforjustice.com


# Log into the database
 mysql -u USER_NAME -p -h mysql.decaturforjustice.com decfj 
