

FAVICON, attribution REQUIRED
    - <div>Website Icon made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/free-icon/fist_2913198?term=fist&page=1&position=20" title="Flaticon">www.flaticon.com</a> and modified by DFJ</div>
    - https://www.flaticon.com/free-icon/fist_2913198?term=fist&page=1&position=20
    - https://support.flaticon.com/hc/en-us/articles/207248209-How-I-must-insert-the-attribution-
    - must be in the footer

Take Action Cover Photo
    - protest photo
        - https://www.pexels.com/photo/crowd-of-protesters-holding-signs-4508668/
        - Photo by Josh Hild from Pexels
    - letter photo
        - https://www.pexels.com/photo/abstract-black-and-white-blur-book-261763/
        - by Pixabay, I think
    - 4th of july
        - https://www.pexels.com/photo/photo-of-person-holding-sparkler-1234389/
        - Photo by Malte Luk from Pexels
    - 4th of july 2 (the one i used)
        - https://www.pexels.com/photo/man-with-fireworks-769525/
        - Photo by Rakicevic Nenad from Pexels
    - Vote
        - https://www.pexels.com/photo/person-dropping-paper-on-box-1550337/
        - Photo by Element5 Digital from Pexels

What else photo
    - https://www.pexels.com/photo/ask-blackboard-chalk-board-chalkboard-356079/
    - By Pixabay

Monopoly go to jail
    - https://www.pexels.com/photo/hotrod-die-cast-model-on-board-1422673/
    - Photo by Suzy Hazelwood from Pexels