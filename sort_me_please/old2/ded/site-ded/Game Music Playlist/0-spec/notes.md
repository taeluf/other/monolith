# Resources for information about restorative justice
- Mother Jones, *What a World Without Cops Would Look Like*: https://www.motherjones.com/crime-justice/2020/06/police-abolition-george-floyd/?campaign_id=9&emc=edit_nn_20200605&instance_id=19111&nl=the-morning&regi_id=80389371&segment_id=30155&te=1&user_id=81e
- Police Violence: 
    - https://mappingpoliceviolence.org/
    - https://www.endingpoliceviolence.com/



# Our Demands, broadly
    - End Police Violence
        - Chicago did these:
            - Ban on chokeholds and strangleholds
            - Duty to Intervene
            - Require de-escalation
            - Require warning before shooting
            - Exhaust All Alternatives Before Shooting
            - Ban shooting at moving vehicles
            - Use-of-force continuum
            - Comprehensive Force Reporting
        
    - Defund the police
    - End racially disparate policing
    - Keep Officers Accountable

# ALL the things we want
    - 