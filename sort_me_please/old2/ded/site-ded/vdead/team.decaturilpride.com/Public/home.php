<view type="parent" name="BigHeaderPage">
  <h1>
   Team website for Decatur IL Pride collaboration
  </h1>
  <p>
    The intention of this site is to keep all pride-related things in one location. Many things will link off to other sites, especially for now, as the rest of the functionality is still under construction.
  </p>
  <h2>
    Important Info
  </h2>
  <p>
    Meeting on Tuesday September 18th at 6pm at Millikin in Shilling hall room 407
    <br>
  </p>
  <h2>
    Contact Info
  </h2>
  <p>
    Reed Sutman <br>
    cell - 217 413 9202<br>
    email - <a href="mailto:rsutman@gmail.com">rsutman@gmail.com</a><br>
    What I'm here for - to answer any questions you may have regarding Pride or our organization or basically anything. If I can't help, I'll try to direct you to the right place. 
  </p>
  <h2>
    Important Links
  </h2>
  <p>
    <a href="https://www.facebook.com/DecaturILPride/">Decatur Pride Facebook</a>
    <br><a href="https://drive.google.com/open?id=1XzDtA69ukgg9Z64WQzEKl645wUNhNWD7">Pride Google Drive Folder</a>
    <br><a href="https://drive.google.com/drive/folders/17H5XzIckMdAi0uCcfV2jYm9BJp06chcp?usp=sharing">Meeting Minutes</a>
    <br><a href="https://drive.google.com/open?id=1CH3rLRP_avfNfcd0WIp7N0u1lu54NqWSSikqEkynUTc">Purpose Statement</a>
    <br><a href="https://drive.google.com/open?id=1j-fCg2qbzYWEn_33y2SSzOmc2BQNb_sT">Projects</a>
    <br><a href="https://drive.google.com/open?id=1bm-zCNZg9GvJbaBxBsCpUCCPLkaNIuK-aUMuiEhNuXs">Fundraiser Ideas</a>
    <br><a href="https://drive.google.com/open?id=1Uekg-YgqTa-9lVvxZSZxJJcM9sbX-fBe">Legal Documents</a>
  </p>
  <h2>
    Google Drive
  </h2>
  <p>
    Google Drive is an online tool we can use to work on documents together, such as Meeting Minutes, our purpose statement, vendor lists, and so on.
  </p>
  <p>
    If you cannot access the files on Google Drive, please send Reed an email at <a href="mailto:rsutman@gmail.com">rsutman@gmail.com</a> and ask him to share the Google Drive account with you.
  </p>
  <p>
    If you have any questions, you can also reach Reed by phone at the number above.
  </p>
  <p>
    Go to <a href="https://drive.google.com/open?id=1_OhNbA-HGG8MLfGDaCe8BqCZ-lCl8naG">https://drive.google.com/open?id=1_OhNbA-HGG8MLfGDaCe8BqCZ-lCl8naG</a> to see the Google Drive folder. 
    <br>
    Go to <a href="https://support.google.com/drive/answer/2424384?hl=en&ref_topic=14940">Google Drive Getting Started guide</a> or the <a href="https://www.youtube.com/watch?v=cCZj5ojxRAA">beginner's tutorial</a> on youtube to get familiarized.
    <br> Or don't do that, and get ahold of Reed, and he can sit down and teach you how to use it.
  </p>
</view>