<?php

class Day {
  
  
  private $timestamp;
  private $dayInfo;
  private $date;
  
  public function __construct($timestamp,$dayInfo=array()){
    $this->timestamp = $timestamp+0;
    $this->dayInfo = $dayInfo;
    $today = new DateTime('now');
    $today->setTimeZone(Calendar::getTimeZone());
    $this->date = DateTime::createFromFormat('U',$timestamp);
    $this->date->setTimeZone(Calendar::getTimeZone());
    if ($today->format('Y-m-d')===$this->date->format('Y-m-d')){
      array_unshift($this->dayInfo,'<span class="today">Today</span>');
    }
   
  }
  public function getDayOfMonth(){
    return $this->date->format('j');
  }
  
  public function getDayInfo(){
    
    return "<div class=\"dayInfo\">".implode("</div><div class=\"dayInfo\">",$this->dayInfo).'</div>';

  }
  
  
  
}




?>