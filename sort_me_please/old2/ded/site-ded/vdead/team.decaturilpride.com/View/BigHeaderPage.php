<view type="self" stylesheets="BigHeaderPage.css" scripts="jdom.js">
<html>
  <head>
    <view type="output" name="Head"></view>
  </head>
  <body>
    <div id="bodyContent">
      <div id="headerContent">
         <view type="include" name="BigHeader"></view>
      </div>

      <div id="content">
        <view type="child"></view>
      </div>
      <div id="footerContent">
        <view type="include" name="Footer"></view>
      </div>
    </div>
  </body>
</html>
  </view>