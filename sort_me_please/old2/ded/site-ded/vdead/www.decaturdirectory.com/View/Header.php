<view type="self" stylesheets="Header.css">
  <header id="header">
    <div id="headerTitleContainer">
      <a id="header_title">Decatur Directory</a><span id="header_quote"></span>
    </div>
    <nav id="navigation">
      <ul id="nav-list">
        <li><a href="/">Home</a></li>
        <li><a href="/businesses.php">Businesses</a>
          <ul id= "nav-sublist">
            <li><a href="/category.php?c=services">Services</a></li>
            <li><a href="/category.php?c=restaurants">Restaurants</a></li>
            <li><a href="/category.php?c=government">Government</a></li>
            <li><a href="/category.php?c=groceries">Groceries</a></li>
            <li><a href="/category.php?c=other">Other</a></li>
          </ul>
        </li>
        <li><a href="/tag-search">Search</a></li>
        <li><a href="/for-business/">For Businesses</a></li>
      </ul>
    </nav>
    <div>
    </div>
  </header>
</view>