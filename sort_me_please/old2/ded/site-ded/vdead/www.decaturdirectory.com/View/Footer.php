<view type="self" stylesheets="Footer.css">
  <footer id="footer">
    <div id="footer_container">
      <div>
        <h4>
        Contact
      </h4>
        <ul>
          <li>- <a href="/help/contact.php">Contact Us</a></li>
          <li>- <a href="/help/report.php">Report a Problem</a></li>
          <li>- <a href="/help/new-listing.php">Submit a business listing</a></li>
          <li>- <a href="/help/correction.php">Correct a listing</a></li>
        </ul>
      </div>
      <div>
        <h4>
          Legal
        </h4>
        <ul>
          <li>- <a href="/legal/privacy.php">Privacy Policy</a></li>
          <li>- <a href="/legal/terms.php">Terms &amp; Conditions</a></li>
        </ul>
      </div>
      <div>
        <h4>
          Other
        </h4>
        <ul>
          <li>- <a href="/about.php">About</a></li>
        </ul>
      </div>
    </div>
    <div>
      Site created by <a href="http://reedoverflow.com/">ReedOverflow.com</a>
    </div>
  
  </footer>

</view>