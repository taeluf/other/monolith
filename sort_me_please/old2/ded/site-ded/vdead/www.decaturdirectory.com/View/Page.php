<!DOCTYPE html>
<view type="self" stylesheets="Page.css,business-css.php" scripts="jsclass.js,business-js.php">
<html>
  <head>
    <view type="include" name="Head"></view>
    <view type="output" name="Head"></view>
  </head>
  <body>
      <view type="include" name="Header"></view>
      <view type="include" name="Content"></view>
      <view type="include" name="Footer"></view>
  </body>
</html>
  </view>