# Under Construction, DecaturDirectory.com
Here's stuff about what the site is

# Jan 11, 2019
* resolved layout issues on business page.

## TODO
* build image rolodex
* refine tag searching appearance AND function
* I don't know. Gotta keep working.

# Jan 5, 2019
* integrated jdom
* created business tag editing page
* created tag editing & creation page
* discovered how to make prettier forms, using fieldset and legends and additional line breaks

## TODO
Just keep at it, basically. Some ideas I had from last night, which are not necessarily relevant include:
* a php class scraper that inputs documentation into database, has a GUI, and can be edited through it
* using tag-types to provide additional capabilities to the tag system
* permissions to use a GUI rather than code to set per-page permissions
* create permissions helper functions like "isAdmin()"
* Something else that I'm forgetting
* journaling-related... permissions or categories or markdown editor?
* reedoverflow.com
* decaturilpride.com
* www.maconzero.com
* hub.maconzero.com
* eco.maconzero.com

# Jan 3, 2019

## Accomplished:
- changed footer link styles
- set min-width to 50% on content area
- Set up editing of businesses
- Set up short unique name business lookup
- fixed user library problems
- added permissions to edit-business and submit-edit business pages
- renamed submit-new-business.php to submit-business-edit.php

## TODO:
Setup config for other business properties, like images, hours, and any others (if they exist. check the db schema)
get the business page working!
Build the Business "Manager" page
 - This would be used to edit a business, including adding new images, adding new tags, adding hours, and adding breadcrumbs
   * Create a breadcrumbs creation form 
   * Create a breadcrumbs lookup & submit-to-business form
   * Create a tags creation form
   * Create a tags lookup & submit-to-business form
   * Create an hours form, which auto-adds it to the business
   * Create an images creation form, which will auto-add the image to the business

## Notes:
this shit is garbage. I'm making a simple, yet effective submission system right now. It's ugly. It's not well controlled in code. But it technically works, as long as it's only admin submission happening.


# Dec 29, 2018

Accomplished:
 * Wrote a basic INSERT statement for the Business  
 * Built an ugly new-business form
 * Wrote a getter method for db-properties on the business
 
TODO:
Business Form & submission
   * make it prettier
   * secure the form and submission pages
   * validate the data before inserting
   * Create an update statement 
   * add business-id as a hidden field for editing businesses
   * Derive a short unique name from the submitted business name. May have to combine with the inserted-id or pseudo-random or the address!
   * Change submission to submit-business.php instead of submit-new-business.php, as it's also for editing
Other
   * Complete the Anti-Spam library, tidy it up, & write instructions
   * Setup proper config files for permissions & user libs
Enable user authentication on the new\_business.php page  
   * Tidy up the User library & instructions
      * create a "login" or empty user.
      * Update namespace for user public files
   * Tidy up the Permissions library & instructions
   * consider setting manual root directories for config files, or other, better configuration methods, or just other OPTIONS, at least
Build the Business class
   * Build the query to load business information by the business's short name
Build the Business "Manager" page
 - This would be used to edit a business, including adding new images, adding new tags, adding hours, and adding breadcrumbs
   * Create a breadcrumbs creation form 
   * Create a breadcrumbs lookup & submit-to-business form
   * Create a tags creation form
   * Create a tags lookup & submit-to-business form
   * Create an hours form, which auto-adds it to the business
   * Create an images creation form, which will auto-add the image to the business
Create a new release of Request Handler
 - There was an error with the autoloader root-directory. So I fixed it.
 
# Dec 28, 2018
Accomplished:
Designed business db schema and related schemas
wrote html for data

TODO:
create the new\_business.php form
This will also be used to edit the fields for an existing business...?
   * Complete the Anti-Spam library, tidy it up, & write instructions
   * Build the form!
   * Build the submission page which will receive the input-data 
   * Build the sql query which will save that data 
Enable user authentication on the new\_business.php page  
   * Tidy up the User library & instructions
   * Tidy up the Permissions library & instructions
Build the Business class
   * Build the query to load business information by the business's short name
   * Write the methods that return that information to the script
Build the Business "Manager" page
 - This would be used to edit a business, including adding new images, adding new tags, adding hours, and adding breadcrumbs
   * Create a breadcrumbs creation form 
   * Create a breadcrumbs lookup & submit-to-business form
   * Create a tags creation form
   * Create a tags lookup & submit-to-business form
   * Create an hours form, which auto-adds it to the business
   * Create an images creation form, which will auto-add the image to the business
Create a new release of Request Handler
 - There was an error with the autoloader root-directory. So I fixed it.

# Dec 27, 2018
It's been a long day. I have created versions of some of my packages and somewhat comprehensive instructions. I am excited to work on ReedOverflow.com, which is where I will end up putting the most comprehensive instructions for all of my libraries. At this time, all of my libs are kept on github and that's where the instructions live as well. I want it all to go on my own site, for the sake of branding and ad revenue, as I'm making the packages available for free.

I fixed some of the minor details in my packages, but the majority of my time today was spent with packagist and composer as well as writing READMEs.

There's a lot of HTML/CSS design to do for individual pages, but I have those designs mostly drawn out on paper.
I need to work on the individual business content page. It currently has dead functions. Once I finish that page, I will start working on the class that it's calling dead functions of. Then I will create the database.

The User Library still has message form integrated within it. I should separate that out as a separate library. It does make installation an extra step, but I think that's not SO bad...

My major goal still is: get decaturdirectory.com launched ASAP
Another somewhat major goal is to develop my libraries further and create release versions and comprehensive instructions. This makes them easier for me to use. It will make my code more consistent.
Then my secondary goals are: 
* Develop my libraries into a publishable state.
* Develop ReedOverflow.com as a front page for my development business, a personal blogging site, and a resource site for my libraries
* Develop MaconHub.com 
* Develop decaturilpride.com
* Develop MaconZero.com
* Develop newsletter software
* Develop accounting software
* Develop an ad-network with local businesses
* Develop various data-management systems
* Develop Calendar library to a publishable & usable state
* Develop or discover a locations library that's simple to use
* Develop inventory management systems

The underlying drive with all these is to keep each library simple and to make programming as a whole easier.
Every single thing I should create should make the rest of the things I create easier to work on. In part that will happen as a simple process of learning. 
In another way, that will happen because I'll keep creating libraries that all build into one-another.
The "Why not use a framework?" question still eats at me, though. I'm not sure, really, what the answer is. It's not necessarily that I think I can do a better job. It's not that I think this approach is more efficient. It's largely that I WANT to do it this way. I want the customizability. I want the complete freedom. I want the independence. 
Creating a framework of my own has been somewhat a dream of mine for a very long time. It's disappointing that all of the components already exist elsewhere, such as ORMs, login-systems, view-controllers, and so-on. What I do think I can provide that stands out from the frameworks is framework-agnostic components. That is, components which function well on their own or attached to the framework. The idea is to create each piece as an isolated, independently functioning piece, with minimal dependencies.
I understand that some things should have a few dependencies. Like my request handler... it depends upon an autoloader and does not technically function stand-alone. But the autoloader is a very minor dependence. I have them separated because I want to have an autoloader package and I do not want to maintain two sets of code. An alternate way that I can operate this is to have an autoloader that specifically loads all of the files for the request handler package. Then provide configuration capabilities in the request handler for installing other packages... but then why isn't the user just using composer!
I guess the autoloading is not a feature of the request handler, so those should be de-coupled. A nice thing the request handler can do is have a pre-process file (or handle_request.php) which allows you to do manual includes prior to the rest of the script operating, or what have you. The autoloader should absolutely be de-coupled from the request handler. I want these things to be independent of one another, but capable of working together.
The Calendar library relies upon the view library. I'm not fond of that, but I don't see a great way to mitigate it. The view library is somewhat advanced in its capabilities and I'm not positive that the calendar library requires all off those functions.
A possible way to configure my repitoire would be to have parent-packages and sub-packages. For example, View would be a parent package and Calendar would be a sub-package. View doesn't rely upon anything else, but Calendar DOES rely upon view. I don't love that setup, though, as I would like for Calendar to still be a standalone package.
I also want all of my packages to be easy to install WITH or WITHOUT composer. When one package relies upon another, it makes installation really stressful without composer. I want my libraries to be useful to any and all php developers. I'd eventually like to expand beyond PHP and code in other languages, though too. I fear this will make many of my PHP efforts seem somewhat... meaningless. 
However, as much as I'm concerned that my work will end up going to waste, I am also aware that it is a process of learning, and if I were in school, much of that work would go to waste as well. This is work I'm doing with the hope of real-world results and money for myself. 

I think I'm pretty good at programming. I feel like I'm getting better. It is common, however, that I get stuck on... not necessarily the most productive paths. There's a certain amount of organizing that I often feel like I need to get done before I can continue to do more work. I did a lot of that organizing work today. I suppose if I were programming day after day instead of taking several (3+) days off at a time, then I might not need things to be organized. But because I do take a lot of time off and my code-schedule is kind of sparse and random-seeming, it's hard to get back into what it was I'm working on.

I feel like I might have wasted my whole day today doing things that don't really matter. I did things that are nice. I got some libraries into a shape where I feel confident in them. I feel like I can trust them. I feel like I can reference them. And that is all super beneficial. But, I also see myself coding as if I'm going to be a professional programmer. I am a profressional programmer, but I'm doing it primarily for the sake of being an activist. I focus more on the programming-side than the activism-side though. It's where my desires seem to pull me, and it seems beneficial to follow my desires to a ponit... but some times it does get in the way of the mission that I think I'd rather be focusing on.

I feel like I could be spiraling out of control. Approaching a burnout. approaching a bottom. I want to just keep coding and coding and coding. it's addictive for me. It's very satisfying. It's very fullfilling. It's very useful. I have drive this week. I'm not inspired to sit around and relax. I want to do things. I want to enjoy my life. I don't want to just consume netflix. It is so boring and pointless to endlessly consume netflix. It wastes my life force, and there are better things that I could be doing.

So I code. And I want to keep coding and coding and coding and coding. But I want to practice guitar and piano and I want to read. But now I don't want to do those things. I feel depleted of my energies, but also as if I could continue coding for the rest of the night. When I finish my... journalling, I guess, I am done coding for the night. I need a break even if I feel like I could go forever. I need relaxation and easy enjoyment. I need rest. But it is not what I want. I want to focus on this as long as I have the energy. 

But now I've been on the computer since 10am today. It is now 9pm. That's an 11 hour work day, with a 30 minute break, an hour-ish long lunch, and a few other shorter breaks. It's been a very productive day. I've gotten a lot done. But I feel like I haven't gotten anything ACTUALLY productive done. Tomorrow, I want to focus on DecaturDirectory.com and really make some headway. I feel like I can get a lot done now. I feel like I'm ready to do some real coding. Today was not real coding. It was minor fixes and instruction writing. I think I improved my understanding of the libraries I've written, and that will be helpful. My time was not spent in the best possible way. My time was spent supporting my programmer image rather than developing my programming business. But the stuff I did does play into the business. It was not the most effective and efficient line of work, but it did some good. I'm happy with what I got done. I'm disappointed with what i did NOT get done. 

I'm excited to work on the "right" things tomorrow. I'm going to buckle-down on decaturdirectory.com and I am going to get a lot done with it. Once it's getting close to launch, I will ask people for their opinions. Those people include (or are mostly) Nathan. It will be an exciting day when I'm able to launch it.

One daunting factor of launching this site is all the work that will go into filling it with data. There are SO many businesses in Decatur, and I will have to get information for every single one of them in order for my site to be comprehensive. It's going to take a lot of work, a lot of my time, and a lot of my energy. I may not be able to get it all done in time to not have to get a job. I'm probably going to have to get a job. Damn. But, this will continue to develop over years. It will be a long term project. it will be a starting point for a real programming business. I might be able to hire employees in the next year or two. I don't want to run a programming business full time. I want to do base-building full time. I want to do additional activism on the side. Programming definitely plays into the base building. So I worry that my time is poorly spent working on the programming for profit, but I feel confident that it will play into my other missions.

DecaturDirectory.com really is not my passion. It's close to my passion. It's something I kind of want to make. I'd like to make a comprehensive scheduling system. I'd like to include all the features that businesses need. Inventory management. I'm excited by the idea of providing a comprehensive service to local businesses. I'm excited about helping local business. I'm excited about using the same code base and the same skills to support my non-profit missions. I'm excited to be ready to use the internet for activism. 

I'm excited to earn money doing something I enjoy. Programming is not my passion. Helping people, solving problems, helping the world, activism... These are my passions. But I haven't figured out how to make money doing these things, so I'm doing it with programming, something I'm good at that I enjoy. Something that may be able to be a temporary thing. I mean, I will continue to code, but I hope to spend less of my coding efforts on profit-ventures in the future. If that is the case, then... I will not create all the wonderful services that I envision. Hopefully I will get better and faster at programming so that I will be able to get more of the packages done that I want to.