<view type="parent" name="Page" stylesheets="business-css.php" scripts="business-js.php">
<?php

\ROF\Business\Config::setConfig([
        "PDO" => \ROF\FN::getPdo()
]);
\ROF\Business\Handler::handleRequest();

?>
</view>