<view type="parent" name="Page"s>
    <h1>Decatur, Illinois Businesses</h1>
    <p>Decatur has a lot to offer. We have a large variety of restaurants including mexican, Thai, Chinese, Indian, and more! There's a whole array of services available to care for your lawn, decorate your home, control pests, and just about anything else you may need! Look around to see what all we have!</p>
    <section>
        <form action="/tag-search" method="GET">
            <fieldset>
                <legend>Search</legend>
                <input type="text" name="search_text" hint="Search..."><br>
                tag1 tag2 tag3 tag4 <br>
                <input type="submit" value="Search">
            </fieldset>
        </form>
    </section>
    <section>
        <h1>Restaurants</h1>
        <p>Here are some featured (random) restaurants that you can find in Decatur. <a href="/business?cat=restaurant">Browse All Restaurants</a></p>
        <div class="photo_rolodex">
            <span class="jsclass jsclass-left-arrow">&lt;</span>
            <? $i=0; foreach (["url"=>'alt',"url2"=>'alt'] as $url => $alt){  ?>
            <span data-position="<?=$i?>" class="wrapper <?= $i===0?'display':'hidden'?>">
              <div class="title">
                <h3><?=$alt?></h3>
              </div>
              <img src="<?=$url?>" alt="<?=$alt?>" />
            </span>
            <? $i++; } ?>
            <span class="jsclass jsclass-right-arrow">&gt;</span>
        </div>
    </section>
    <section>
        <h1>Retail</h1>
        <p>Here are some featured (random) retail stores that you can find in Decatur. <a href="/business?cat=retail">Browse All Retail</a></p>
        <div class="photo_rolodex">
            <span class="jsclass jsclass-left-arrow">&lt;</span>
            <? $i=0; foreach (["url"=>'alt',"url2"=>'alt'] as $url => $alt){  ?>
            <span data-position="<?=$i?>" class="wrapper <?= $i===0?'display':'hidden'?>">
              <div class="title">
                <h3><?=$alt?></h3>
              </div>
              <img src="<?=$url?>" alt="<?=$alt?>" />
            </span>
            <? $i++; } ?>
            <span class="jsclass jsclass-right-arrow">&gt;</span>
        </div>
    </section>
    <section>
        <h1>Service</h1>
        <p>Here are some featured (random) Service businesses that you can find in Decatur. <a href="/business?cat=service">Browse All Service</a></p>
        <div class="photo_rolodex">
            <span class="jsclass jsclass-left-arrow">&lt;</span>
            <? $i=0; foreach (["url"=>'alt',"url2"=>'alt'] as $url => $alt){  ?>
            <span data-position="<?=$i?>" class="wrapper <?= $i===0?'display':'hidden'?>">
              <div class="title">
                <h3><?=$alt?></h3>
              </div>
              <img src="<?=$url?>" alt="<?=$alt?>" />
            </span>
            <? $i++; } ?>
            <span class="jsclass jsclass-right-arrow">&gt;</span>
        </div>
    </section>
    <section>
        <h2>Other Categories</h2>
        <p>Or check out businesses from any of these categories</p>
        <a href="/all-categories">All Categories</a>, <a href="blah">Blah</a>, <a href="blah">Blah</a>, <a href="blah">Blah</a>, <a href="blah">Blah</a>, <a href="blah">Blah</a>
        
    </section>
  
</view>