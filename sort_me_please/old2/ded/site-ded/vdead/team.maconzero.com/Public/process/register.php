<?php


// print_r($_POST);


// throw new Exception ("This page is currently under maintenance");


if (trim($_POST['email'])!=trim($_POST['confirmEmail'])){
  throw new Exception("Email addresses do not match.");
}
if (!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)){
  throw new Exception("The email you entered does not appear to be a proper email address.");
}

$pdo = MyPDO::get();

$exists = $pdo->prepare("SELECT * FROM maconzer_user.logins WHERE email=:email");
$exists->execute(array(":email"=>$_POST['email']));
$rows = $exists->fetchAll();
if (count($rows)>0){
  throw new Exception("A login already exists with this email address. ");
}
$exists->closeCursor();

$newUser = $pdo->prepare("INSERT INTO maconzer_user.info(cookie_id,name,email) VALUES(:name,:email)");
$newUser->execute(array(
  ":name" => $_POST['name'],
  ":email" => trim($_POST['email'])
));
$userId = $pdo->lastInsertId();
$newUser->closeCursor();

$insert = $pdo->prepare("INSERT INTO maconzer_user.logins(user_id,source,verified,email) VALUES(:user_id,:source,:verified,:email)");
$insert->execute(array(
  ":user_id" => $userId,
  ":source" => "email",
  ":verified" => 0,
  ":email" => trim($_POST['email'])
));
$loginId = $pdo->lastInsertId();
$insert->closeCursor();

$auth = $pdo->prepare("INSERT INTO maconzer_user.auth(user_id,entered_hash,cookie_hash,expiresAt) VALUES(:user_id,:entered_hash,:cookie_hash,:expiresAt)");
$factory = new RandomLib\Factory();
$generator = $factory->getLowStrengthGenerator();
$tempPassword = $generator->generate(32);
$verification_string = password_hash($tempPassword,PASSWORD_DEFAULT);
$cookieHash = "";
$auth->execute(array(
  ":user_id" => $userId,
  ":verification_string" => $verification_string,
  ":cookie_hash" => $cookieHash,
  ":expiresAt" => time()+60*60*24*3
));
$authId = $pdo->lastInsertId();
$auth->closeCursor();

if (!$loginId||!$userId){
  throw new Exception("Something went wrong when creating your account. Please try again.");
}

$mailSent = mail($_POST['email'],'Macon Zero Registration',
    "Thank you for signing up!<br><br>"
     ."<a href=\"http://team.maconzero.com/user/login.php\">Log In</a><br><br>"
     ."Your temporary password is <b>{$tempPassword}</b> It expires in 3 days.",
     array(
        "MIME-Version: 1.0",
        "Content-type: text/html; charset=iso-8859-1",
        "To: <".$_POST['email'].">",
        
     )
    );

if (!$authId||!$mailSent){
  throw new Exception("There was a problem setting up your account. Please go to the 'Forgot Password' page. to re-send your verification email");
}

?>

<view type="parent" name="BigHeaderPage">
Thank you for signing up! A temporary password has been sent to your email. Use it to <a href="/user/login.php">Log In</a>.

</view>




