
<?php

throw new Exception("This page is currently unavailable.");

echo str_replace("    ", "<br>",print_r($_POST,TRUE));

$authorizedAgents = array('rs378f');

if (!in_array($_POST['authorizedAgent'],$authorizedAgents)){
  throw new Exception("A new transaction can only be entered by an authorized agent.");
}

$pdo = MyPDO::get();

$statement = $pdo->prepare("INSERT INTO maconzer_team.Transactions(authorizedAgent,shortDescription,type,fund,account,cost,client,comments) "
                              ."VALUES(:authorizedAgent, :shortDescription, :type, :fund, :account, :cost, :client, :comments)");

$statement->execute(array(
  ":authorizedAgent" => $_POST['authorizedAgent'],
  ":shortDescription" => $_POST['shortDescription'],
  ":type" => $_POST['type'],
  ":fund" => $_POST['fund'],
  ":account" => $_POST['account'],
  ":cost" => $_POST['cost'],
  ":client" => $_POST['client'],
  ":comments" => $_POST['comments'],
  
));


?>