<view type="parent" name="BigHeaderPage">
  
  <h1>
    Finances
  </h1>
  
  <h2>
    New Transaction
  </h2>
  <form action="/form/submit-transaction.php" method="post">
    <fieldset>
      <legend>
        New Transaction
      </legend>
      
      <label for="authorizedAgent">Authorized Macon Zero Agent</label><br>
        <input type="text" name="authorizedAgent" />
      <br><br>
      
      <label for="shortDescription">Short Description</label> <br>
        <input type="text" name="shortDescription"/>
      <br><br>
      
      <label for="type">Transaction Type</label><br>
        <select name="type">
          <option value="select" selected disabled>Select One</option>
          <option value="purchase">Purchase</option>
          <option value="donation">Donation</option>
          <option value="sale">Sale</option>
          <option value="loan">Loan</option>
          <option value="loanRepayment">Loan Repayment</option>
          <option value="transfer">Transfer</option>
          <option value="reimbursement">Reimbursement</option>
          <option value="deposit">Deposit</option>
          <option value="Withdrawal">Withdrawal</option>
          <option value="other">Other (leave comments)</option>
      </select>
      <br><br>
      
      <label for="fund">Fund</label><br>
      <select name="fund">
        <option value="primary">Macon Zero General Fund</option>
        <option value="pride">Pride Fund</option>
      </select>
      <br><br>
      
      <label for="account">Account</label><br>
      <select name="account">
        <option value="checking">Staley Checking Debit</option>
        <option value="savings">Staley Savings</option>
        <option value="cash">Cash</option>
      </select>
      <br><br>
      
      <label for="cost">Amount</label> <br>
      $<input type="text" name="cost"/>
      <br><br>
      
      <label for="client">Client</label><br>
      <input type="text" name="client" />
      <br><br>
      
      <label for="comments">Comments</label> <br>
        <textarea name="comments" rows="4" cols="30"></textarea>
      <br><br>
      
      <view type="include" name="AntiSpam"></view>
      <input type="submit">
    </fieldset>
     
  </form>
 
</view>