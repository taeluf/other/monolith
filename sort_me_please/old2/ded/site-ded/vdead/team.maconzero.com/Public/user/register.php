<view type="parent" name="BigHeaderPage">

    <form action="/process/register.php" method="post">
      <fieldset>
        <legend>
          Register
        </legend>
        
        <label for="name">Name:</label><br>
        <input type="text" name="name">
        <br><br>
        
        <label for="email">Email:</label><br>
        <input type="email" name="email">
        <br><br>
        
        <label for="confirmEmail">Confirm Email:</label><br>
        <input type="email" name="confirmEmail" autocomplete="off">
        <br><br>
        
      </fieldset>
      <br>
      <view type="include" name="AntiSpam"></view>
      <input type="submit" value="Create Account">
  
  </form>
  
</view>