<?php



class View {
  
  
  private $content = "";
  
  private $originalContent = [];
  private $scripts = [];
  private $stylesheets = [];
  private $title;
  private $keywords;
  private $description;
  private $author;
  
  
  public function __construct($content){
    $this->content = $content;
    
    $dom = new simple_html_dom($this->content);
    $c=0;
    while ($c++<30&&count($dom->find('view'))>0){
      $this->singlePass();
      $dom = new simple_html_dom($this->content);
    }
    
  }
  
  private function insertHeadHtml(){
    // Collects the stylesheets and compiles them into a single, minified file
    // Collects the scripts and compiles them into a single, minified file.
    // Returns SEO information 
    // Returns viewport meta tag
    
    $outputHtml = "";
    
    $stylesheet = "";
    $stylesName = "css-";
    $mostRecentEdit = 0;
    foreach ($this->stylesheets as $fileName){
      $stylesName .= $fileName;
      $fileName = parse_url($fileName,PHP_URL_PATH);
      $file = $_SERVER['DOCUMENT_ROOT'].'/Style/'.$fileName;
      if (file_exists($file)){
        $editTime = filemtime($file);
        if ($editTime>$mostRecentEdit)$mostRecentEdit = $editTime;
        $contents = file_get_contents($file);
        $stylesheet .= $contents;
      } else {
        
        $stylesheet = "/* Stylesheet {$file} was not found */\n".$stylesheet;
      }
    }
    $stylesName = str_replace(array('.css','.','?','#',' '),array('-','_','v','h',''),$stylesName);
    $stylesName = substr($stylesName,0,251).'.css';
    $stylesPath = $_SERVER['DOCUMENT_ROOT'].'/Public/Compiled/'.$stylesName;
    if (!file_exists($stylesPath)||filemtime($stylesPath)<$mostRecentEdit){
      file_put_contents($stylesPath,$stylesheet);
    }
    $outputHtml .= '<link rel="stylesheet"mtype="test/css" href="/Compiled/'.$stylesName.'">'."\n";
    
    $scripts = "";
    $scriptsName = "js-";
    $mostRecentEdit = 0;
    foreach ($this->scripts as $fileName){
      $scriptsName .= $fileName;
      $fileName = parse_url($fileName,PHP_URL_PATH);
      $file = $_SERVER['DOCUMENT_ROOT'].'/Script/'.$fileName;
      if (file_exists($file)){
        $editTime = filemtime($file);
        if ($editTime>$mostRecentEdit)$mostRecentEdit = $editTime;
        $contents = file_get_contents($file);
        $scripts .= $contents;
      } else {
        
        $scripts = "/* Script {$file} was not found */\n".$scripts;
      }
    }
    $scriptsName = str_replace(array('.js','.','?','#',' '),array('-','_','v','h',''),$scriptsName);
    $scriptsName = substr($scriptsName,0,252).'.js';
    $scriptsPath = $_SERVER['DOCUMENT_ROOT'].'/Public/Compiled/'.$scriptsName;
    if (!file_exists($scriptsPath)||filemtime($scriptsPath)<$mostRecentEdit){
      echo "FILE MADE";
      file_put_contents($scriptsPath,$scripts);
    }
    $outputHtml .= '<script type="text/javascript" src="'.'/Compiled/'.$scriptsName.'"></script>'."\n";
    
    if ($this->title!=NULL){
      $outputHtml .= '<title>'.$this->title.'</title>'."\n";
    }
    if ($this->description!=NULL){
      $outputHtml .= '<meta name="description" content="'.$this->description.'">';
    }
     if ($this->keywords!=NULL){
      $outputHtml .= '<meta name="keywords" content="'.$this->keywords.'">';
    }
    
    $dom = new simple_html_dom($this->content);
    $outputTags = $dom->find("view[type=output], view[name=Head]");
    $c =0;
    foreach ($outputTags as $output){
      if ($c++>0)Errors::handle("Error preparing the page.", "There were multiple head output views. Only one is allowed. Error reported on line "
                                .__LINE__." of file ".__FILE__);
      $output->outertext = $outputHtml;
    }
    $this->content = $dom.'';
    //output tags are for inserting the head data, so that PHP doesn't have to be written in the html <head> tag. 
    //Instead a <view type="output"> is put into the head tag, then it is filled in by this script
  }
  private function discoverInfo($dom){
    $this->stylesheets = array_unique(
        array_merge($this->stylesheets, $dom->stylesheets!=NULL ? explode(',',$dom->stylesheets) : array())
      );
    $this->scripts = array_unique(
        array_merge($this->scripts, $dom->scripts!=NULL ? explode(',',$dom->scripts) : array())
      );
    if ($this->title==NULL&&$dom->title!=NULL){
      $this->title = $dom->title;
    }
    if ($this->description==NULL&&$dom->description!=NULL){
      $this->description = $dom->description;
    }
    if ($this->keywords==NULL&&$dom->keywords!=NULL){
      $this->keywords = $dom->keywords;
    }
  }
  
  private function singlePass(){
    
    $dom = new simple_html_dom($this->content);
    
    $parentTags = $dom->find('view[type=parent]');
    $includeTags = $dom->find('view[type=include]');
    $childTags = $dom->find('view[type=child]');
    $selfTags = $dom->find('view[type=self]');
    //self tags can contain information about stylesheets, scripts, and meta data.
    //self tags provide no visual information. They are data only and the tag must be removed.
    
    foreach ($selfTags as $self){
      //echo $self;exit;
      $this->discoverInfo($self);
      $self->outertext = $self->innertext;
    }
    static $offset = 0;
    foreach ($childTags as $count=>$child){
      $this->discoverInfo($child);
      $child->outertext = $this->originalContent[$count+$offset];
    }
   // $this->originalContent = array();
    foreach ($parentTags as $parent){
      $this->discoverInfo($parent);
      $fileContent = $this->getViewFile($parent->name);
      $this->originalContent[] = $parent->innertext;
      $parent->outertext = $fileContent;
    }
    foreach ($includeTags as $included){
      $this->discoverInfo($included);
      $fileContent = $this->getViewFile($included->name);
      $included->outertext = $fileContent;
    }
    
    $this->content = $dom->outertext;
  }
  
  private function getViewFile($viewName){
    $filePath = $_SERVER['DOCUMENT_ROOT'].'/View/'.$viewName.'.php';
    if (!file_exists($filePath)){
      Errors::handle("There has been an error creating the page.", "View '{$viewName}' could not be found on the server at path '{$filePath}'. "
                      ."Error reported on line ".__LINE__." in file ".__FILE__);
    }
      ob_start();
      include($filePath);
    $fileContent = ob_get_clean();
    return $fileContent;
  }
  
  public function display(){
    $this->insertHeadHtml();
    echo $this->content;
  }
  
  
}



?>