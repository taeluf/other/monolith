<?php

class View {
  
  private $content;
  private $viewName = NULL;
  private $viewContent = NULL;
  
  
  
  
  
  public function __construct($content=NULL,$viewName=NULL){
    echo "content size: ".strlen($content)." viewName:".$viewName."\n\n";
    $dom = NULL;
    $childTags = [];
    $parentTags = [];
    $includeTags = [];
    
    if ($viewName==NULL&&$content!=NULL){
      $dom = new \voku\helper\HtmlDomParser($content);
    } 
    if ($viewName!=NULL){
      $dom = new \voku\helper\HtmlDomParser($this->getViewFile($viewName));
    }
    $childTags = $dom->find("view[type=child]");
    $parentTags = $dom->find("view[type=parent]");
    $includeTags = $dom->find("view[type=include]");
    
    foreach($childTags as $childTag){
       $childTag->outertext=$content;
    }
    
    //install parent views over this one. The content of this view will become the content of these parent views.
    foreach($parentTags as $key=>$parentTag){
//       $parentView = new View('000'.$parentTag->innertext.'000',$parentTag->name);
//       $parentTag->outertext = $parentView.'';
      $parentTags[$key]->outertext = $this->getViewFile($parentTag->name);
     // $parentTag->outertext = 
    }
    
    $this->content = "".$dom->outertext;
    
    return;
    if ($viewName==NULL&&$content!=NULL){
      $dom = new \voku\helper\HtmlDomParser($content);
      $dom = $this->insertParents($dom);
      //$this->includeViews($dom);
      
      $this->content = ''.$dom;
    }
  }
  private function insertParents($dom){
    $views = $dom->find('view[type=parent]');
     foreach($views as $key=>$viewTag){
        $parentView = new \voku\helper\HtmlDomParser($this->getViewFile($viewTag->name));
        
        $this->insertContent($parentView,$viewTag->innerText);
      
        $dom = $this->insertParents($parentView);
      }
    
    return $dom;
  }
  private function includeViews($dom){
    foreach($dom->find('view[type=include]') as $viewTag){
        $includeView = new \voku\helper\HtmlDomParser($this->getViewFile($viewTag->name).'');
        $this->includeViews($includeView);
        $viewTag->outertext = ''.$includeView->outertext.'';
      }
  }
  
  private function insertContent($dom,$content){
    $c=0;
    foreach ($dom->find('view[type=child]') as $viewTag){
      if ($c++>0)Errors::handle("Multiple child views not allowed. There was an error.");
      $viewTag->outertext = ''.$content.'';
    }
  }
  private function getViewFile($fileName){
    $filePath = $_SERVER['DOCUMENT_ROOT'].'/View/'.$fileName.'.php';
    if (file_exists($filePath)){
      ob_start();
      include($filePath);
      $fileContent = ob_get_clean();
      return $fileContent;
    } else {
      Errors::handle("get view file {$filePath} failed");
    }
  }
  public function __toString(){
    
     // $this->loadParentViews();
      //$this->includeView();
    return $this->content."";
  }
  public function display(){

    echo $this."";
  }
  
  
  
  /** With $content only, create new dom document and parse for parent and include views.
  *   With $viewName only, create new dom document and parse for parent and include views. 
  *   with $viewName and $content, create new dom of $viewName then inser $content into $viewName's child view
  *
  */
  public function zz__construct($content=NULL,$viewName=NULL){
    echo "CONSTRUCT {$viewName}";
    if ($content==NULL&&$viewName==NULL){
      Error::shandle("Error creating a view","content and view name are null in ".__FILE__." on line ".__LINE__);
    }
    if ($viewName!=NULL){
     // echo "VIEW-{$viewName}"
      $file = $_SERVER['DOCUMENT_ROOT'].'/View/'.$viewName.'.php';
      if (!file_exists($file)){
        Errors::handle("Error finding View file.","View {$viewName} could not be careted because file {$file} does not exist");
      }
      ob_start();
      include($file);
      $fileContent = ob_get_clean();
      $dom = new \voku\helper\HtmlDomParser($fileContent);
      foreach ($dom->find("view[type=parent]") as $parentViewDom){
        $view = new View(NULL,$parentViewDom->name);
        /**
        I STOPPED WHILE WORKING ON THE __CONSTRUCT function
         
        **/
      }
      foreach ($dom->find("view[type=include]") as $includedViewDom){
        $view = new View(NULL,$includedViewDom->name);
        $includedViewDom->outertext = $view."";
      }
      if ($content!=NULL){
        
        foreach ($dom->find("view[type=child]") as $childViewDom){
          
          $childViewDom->outertext=$content;
        }
      }
      $content = $dom."";
    }
    if ($viewName==NULL){
      //this means it is a content view. It will have a parent, possibly include views, but no child views
      $dom = new \voku\helper\HtmlDomParser($content);
      foreach ($dom->find("view") as $view){
        if (strtolower($view->type)=="parent"){
          $parentView = new View($dom->innerText,$view->name);
          $dom->outertext = $parentView.'';
          $content = $dom.''; 
//           $filePath = $_SERVER['DOCUMENT_ROOT'].'/View/'.$view->name.'.php';
//           if (!is_file($filePath))Error::handle("Could not load the page.", "File {$file} not found in file ".__FILE__." on line ".__LINE__);
//           ob_start();
//           include($filePath);
//           $fileContent = ob_get_clean();
//           $parentView = new View($fileContent);
        } else if (strtolower($view->type)=="include"){
          $includeView = new View(NULL,$view->name);
          $view->outertext = $includeView.'';
//           $filePath = $_SERVER['DOCUMENT_ROOT'].'/View/'.$view->name.'.php';
//           if (is_file($filePath)){
//             ob_start();
//             include($filePath);
//             $fileContent = ob_get_clean();
//           } else {
//             Error::handle("There was a problem loading the page.", "file {$filePath} not found in ".__FILE__." on line ".__LINE__);
//           }
          $content = $dom.'';
        } else if (strtolower($view->type)=="child"){
          
        }
      }
      
    }
    $this->content = $content;
    $this->viewName = $viewName;
  }
  
  

  
  
  
  
  /*  new view (content)
    scan view for <view type="parent"> and <view type="include">
    for each parent view, create a new view with content from the parent file
    then in each parent view, find all <view type="child"> and replace the outer text with the content supplied.
       So, you create a new view (content) that has parent View "Page", so new view (contentWithoutparentViewTag, Page) gets created
       Then in the Parent View HTML, there should be a decaled child view which contentWithoutParentViewTag goes into
    Then the entire view is scraped for "include" views. They are loaded as new View objects, then each include <view> tag has its 
    outertext replaced with the content of the View HTML file that was named
    
    Different types of views being created:
      Content view - contains a parent, contains content, may contain include views. DOES NOT contain child views. May contain multiple parents, thus having multiple content sections
      Parent View - must contain exactly 1 child view. may contain include views. may include parent view
      Include View - must not contain child view. may contain parent view, but must be careful to avoid recursion
      
      
      */
      
  
  private function loadParentViews(){
    
    $contentDom = new \voku\helper\HtmlDomParser($this->content);
    foreach ($contentDom->find("view[type=parent]") as $parentViewTag){
      
      $viewName = $parentViewTag->name;
      ob_start();
      include($_SERVER['DOCUMENT_ROOT'].'/View/'.$viewName.'.php');
      $parentViewContent = ob_get_clean();
      $parentDom = new \voku\helper\HtmlDomparser($parentViewContent);
      foreach ($parentDom->find("view[type=child]") as $childView){
        
        $childView->outerText = $contentDom->innerText;
      }
      
      echo $parentDom;
      
    }
    
  }
  private function includeView(){
    
//     echo "INCLUDE VIEW";
    if ($this->viewContent!=NULL){
      Errors::handle("There has been an error.","View content has already been set. Error reported on line ".__LINE__." of file ".__FILE__."--ViewContent:".$this->viewContent."--");
    }
    if ($this->viewName==NULL){
      $contentDom = new \voku\helper\HtmlDomParser($this->content);
     
      foreach($contentDom->find('view') as $view){
        if ($view->type=="parent"&&$view->name!=NULL){
         // $view->outertext = new View($this,)
        }
      }
    }
    $file=$_SERVER['DOCUMENT_ROOT'].'/View/'.$this->viewName;
    if ($this->viewName!=NULL&&file_exists($file)&&is_file($file)){
      ob_start();
      include($file);
      $viewContent = ob_get_clean($file);
      $this->viewContent = $viewContent;
    } else {
      $this->viewContent = $this->content;
    }
    if ($this->viewContent==NULL){
      Errors::handle("There was an error.","View content is null. Error reported on line ".__LINE__." of file ".__FILE__.". Content was coming from file:".$file."--");
    }
    $dom = new \voku\helper\HtmlDomParser($this->viewContent);
    foreach($dom->find('view') as $view){
      $view->outerText = $this->content;
    }
    $this->viewContent = $dom."";
  }
  

  
}




?>