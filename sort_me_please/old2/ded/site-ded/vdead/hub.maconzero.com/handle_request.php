<?php

require "vendor/autoload.php";

//create a new handler and configure it.
//The Handler is the only required part.
$documentRoot = $_SERVER['DOCUMENT_ROOT'].'/vendor/JakarCo/'.$_SERVER['SERVER_NAME'];

$configuration = new \JakarCo\Request\Configuration($documentRoot);
$controller = new \JakarCo\View\Controller($documentRoot);
$configuration->setViewController($controller);


$handler = new JakarCo\Request\Handler($configuration);


$handler->handle();


?>
