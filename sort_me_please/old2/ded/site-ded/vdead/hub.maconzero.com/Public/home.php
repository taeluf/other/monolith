<view type="parent" name="BigHeaderPage" stylesheets="home.css">
  <section class="home contentSection">
  <h1>
   Macon Zero's Community Hub
  </h1>
  <p>
    Macon Zero is a new organization, founded in May of 2018 and you can learn more about it at <a href="http://www.maconzero.com">www.maconzero.com</a>.
    <br><br>
  This site is Macon Zero's Hub. We're planning to launch the first version of the hub by October 1st 2018. This doesn't look like much, but a lot of progress has been made. 
    <br><br>
    This hub is intended to tie our community together. There are a lot of good things happening already, but chances are, you don't know about a lot of it. So this site is going to share information about organizations from all sectors, including health, minority, environmental, political, and others. We will give you access to volunteer opportunities with organizations you care about. Furthermore, we will be here to help turn YOU into a social activist. If you have an idea how things can be better in Decatur area, we can help you make it happen. 
    <br><br>
    
      <a href="https://facebook.com/MaconZero">Facebook.com/MaconZero</a>
    <br><b>-Last updated Saturday, September 1, 2018</b>
  </p>
</section>
  
</view>