# view AndroidManifest.xml #generated:48
-keep class com.jakar.findmydroid.ActivityChooseFriends { <init>(...); }

# view AndroidManifest.xml #generated:38
-keep class com.jakar.findmydroid.ActivityHTHistory { <init>(...); }

# view AndroidManifest.xml #generated:36
-keep class com.jakar.findmydroid.ActivityStopRing { <init>(...); }

# view AndroidManifest.xml #generated:46
-keep class com.jakar.findmydroid.ActivityTabGeneral { <init>(...); }

# view AndroidManifest.xml #generated:29
-keep class com.jakar.findmydroid.ActivityTabHost { <init>(...); }

# view AndroidManifest.xml #generated:40
-keep class com.jakar.findmydroid.ActivityTabLocate { <init>(...); }

# view AndroidManifest.xml #generated:44
-keep class com.jakar.findmydroid.ActivityTabLocator { <init>(...); }

# view AndroidManifest.xml #generated:42
-keep class com.jakar.findmydroid.ActivityTabSecure { <init>(...); }

# view AndroidManifest.xml #generated:75
-keep class com.jakar.findmydroid.AdminReceiver { <init>(...); }

# view AndroidManifest.xml #generated:52
-keep class com.jakar.findmydroid.BlockList { <init>(...); }

# view AndroidManifest.xml #generated:50
-keep class com.jakar.findmydroid.DialogManageData { <init>(...); }

# view AndroidManifest.xml #generated:54
-keep class com.jakar.findmydroid.EnterPassword { <init>(...); }

# view AndroidManifest.xml #generated:86
-keep class com.jakar.findmydroid.ReceiverOpenApp { <init>(...); }

# view AndroidManifest.xml #generated:64
-keep class com.jakar.findmydroid.ReceiverSms { <init>(...); }

# view AndroidManifest.xml #generated:59
-keep class com.jakar.findmydroid.ServiceLocation { <init>(...); }

# view AndroidManifest.xml #generated:60
-keep class com.jakar.findmydroid.ServiceRing { <init>(...); }

# view AndroidManifest.xml #generated:62
-keep class com.jakar.findmydroid.ServiceSendingSms { <init>(...); }

