package com.jakar.findmydroid;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.app.TabActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;


@SuppressWarnings("deprecation")
public class ActivityTabHost extends TabActivity {
	
	//Dialogs!
	final int RATE_APP = 1;
	final int T_C = 2;
	final int CHECK_SETTINGS = 3;
	
	Context context; 	
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	EditText myPass2;
	

	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.tabhost);
	    
	    KeyguardManager mKeyGuardManager = (KeyguardManager)getSystemService(KEYGUARD_SERVICE); 
	    KeyguardLock mLock = mKeyGuardManager.newKeyguardLock("Missing Droid");  
	    mLock.disableKeyguard();
	    
	    context = this;
	    
	    
	    
	    pref = PreferenceManager.getDefaultSharedPreferences(this);
	    editor = pref.edit();
	    

	    Resources res = getResources(); // Resource object to get Drawables
	    M.tabHost = getTabHost();  // The activity TabHost
	    TabHost.TabSpec spec;  // Resusable TabSpec for each tab
	    Intent intent;  // Reusable Intent for each tab

	    
	 // Do the same for the other tabs
	    intent = new Intent().setClass(this, ActivityTabGeneral.class);
	    spec = M.tabHost.newTabSpec("general").setIndicator("General",
	                      res.getDrawable(R.drawable.other))
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, ActivityTabLocate.class);
	    spec = M.tabHost.newTabSpec("locate").setIndicator("Locate",
	                      res.getDrawable(R.drawable.locate))
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, ActivityTabSecure.class);

	    // Initialize a TabSpec for each tab and add it to the M.tabHost
	    spec = M.tabHost.newTabSpec("secure").setIndicator("Secure",
	                      res.getDrawable(R.drawable.secure))
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, ActivityTabLocator.class);
	    // Initialize a TabSpec for each tab and add it to the M.tabHost
	    spec = M.tabHost.newTabSpec("locator").setIndicator("Locator",
	                      res.getDrawable(R.drawable.findabuddy))
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    
	    intent = new Intent().setClass(this, ActivityHTHistory.class);
	    // Initialize a TabSpec for each tab and add it to the M.tabHost
	    spec = M.tabHost.newTabSpec(null).setIndicator(null,
	                      null)
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    intent = new Intent().setClass(this, BlockList.class);
	    // Initialize a TabSpec for each tab and add it to the M.tabHost
	    spec = M.tabHost.newTabSpec(null).setIndicator(null,
	                      null)
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    intent = new Intent().setClass(this, ActivityChooseFriends.class);
	    // Initialize a TabSpec for each tab and add it to the M.tabHost
	    spec = M.tabHost.newTabSpec(null).setIndicator(null,
	                      null)
	                  .setContent(intent);
	    M.tabHost.addTab(spec);
	    M.tabHost.getTabWidget().getChildAt(4).setVisibility(View.GONE);
	    M.tabHost.getTabWidget().getChildAt(5).setVisibility(View.GONE);
	    M.tabHost.getTabWidget().getChildAt(6).setVisibility(View.GONE);
	    
	    
	    int timesUsed = pref.getInt("timesUsed", 0);
		timesUsed++;
		editor.putInt("timesUsed", timesUsed);
		editor.commit();
		
		if ((timesUsed >=2 && !Market.isPro(context))){
			editor.putInt("timesUsed", 0);
			editor.commit();
			String special_message = "Thank you for using Missing Droid!"
					+"\n\nPlease upgrade for more features and to remove this annoying popup.";
			Market.upgradeDialog(context, special_message);
		}
		
		android.location.LocationManager locMan = (android.location.LocationManager)getSystemService(Context.LOCATION_SERVICE);
		 
		 boolean checkSettings = pref.getBoolean("checkSettings", true);
		 boolean gpsOff = locMan.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
		 boolean networkOff = locMan.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER);
		
		 if (!pref.getBoolean("TCA", false)){
	        	showDialog(T_C);
	     } else if (checkSettings && (!gpsOff || !networkOff)){
				 showDialog(CHECK_SETTINGS);
	     }
	}
	@Override
	public void onStart(){
		super.onStart();
		
		 M.tabHost.setCurrentTab(pref.getInt("startTab", 0));
		
		
		 
		 if (pref.getBoolean("appLock", false)){
			 Intent intent = new Intent(context, EnterPassword.class);
			 startActivityForResult(intent, 0);
		    }
		
		 
	}
	
	@Override
	public void onPause(){
		super.onPause();
		int tab = M.tabHost.getCurrentTab();
		if (tab>3){
			tab=0;
		}
		editor.putInt("startTab", tab);
		editor.commit();
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	@Override
	public void onStop(){
		super.onStop();
		if (pref.getBoolean("hideApp", false)){
			ComponentName cn = new ComponentName(context, ActivityTabHost.class);
			context.getPackageManager().setComponentEnabledSetting(cn, 
					PackageManager.COMPONENT_ENABLED_STATE_DISABLED, 
					PackageManager.DONT_KILL_APP);
			Toast.makeText(context, "Missing Droid will be hidden. May require Reboot", Toast.LENGTH_LONG);
		}
		
	}
	@Override
	protected Dialog onCreateDialog(int id){
		
		switch (id){
			case RATE_APP:
				LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				View rateAppView = inflater.inflate(R.layout.rateapp, null);
				Button rate = (Button)rateAppView.findViewById(R.id.rate);
				
				rate.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						Market.rate(context);
					}
				});
				Button getPro = (Button)rateAppView.findViewById(R.id.getPro);
				getPro.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						if (Market.isPro(context)){
							Market.apps(context);
						} else {
							Market.donate(context);
						}
					}
				});
				if (Market.isPro(context)){
					getPro.setText("Apps");
					TextView rateText = (TextView)rateAppView.findViewById(R.id.rateText);
					rateText.setText("Thank you for using Missing Droid. If you like it, " +
							"please give me a rating and please check out my other apps.");
				}
				CheckBox doNotShow = (CheckBox)rateAppView.findViewById(R.id.doNotShow);
				doNotShow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean newValue) {
							editor.putBoolean("doNotShow", newValue);
						
						editor.commit();
					}
				});
				
				final AlertDialog rateDialog = new AlertDialog.Builder(context)
				.setView(rateAppView)
				.show();
				Button cancelRate = (Button)rateAppView.findViewById(R.id.cancelRate);
				cancelRate.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						rateDialog.dismiss();
					}
				});
				return rateDialog;	
				
			case T_C:	
				AlertDialog TCDialog = new AlertDialog.Builder(this)
	        	.setMessage("By using Missing Droid, you agree to the End User Agreement at "+M.EULA)
				.setTitle("End User Agreement")
	        	.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						editor.putBoolean("TCA", true);
						editor.commit();
						
						android.location.LocationManager locMan = (android.location.LocationManager)getSystemService(Context.LOCATION_SERVICE);
						boolean checkSettings = pref.getBoolean("checkSettings", true);
						boolean gpsOff = locMan.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
						boolean networkOff = locMan.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER);
						if (checkSettings && (!gpsOff || !networkOff)){
							 showDialog(CHECK_SETTINGS);
						}	
					}
				})
				.setNeutralButton("I Decline", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
						
					}
				})
				.setNegativeButton("View Agreement", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
			    		M.openWebsite(context, M.EULA);
			    		finish();
					}
				})
				.setCancelable(true)
				.setOnCancelListener(new DialogInterface.OnCancelListener() {
					
					@Override
					public void onCancel(DialogInterface arg0) {
						finish();
					}
				})
				.setOnKeyListener(new DialogInterface.OnKeyListener() {
					
					@Override
					public boolean onKey(DialogInterface arg0, int arg1, KeyEvent arg2) {
						if (arg1==KeyEvent.KEYCODE_BACK)
							finish();
						return true;
					}
				})
				.show();
				
				return TCDialog;
			case CHECK_SETTINGS:
				final CheckBox dontShow = new CheckBox(context);
				dontShow.setText("Do not show again");
				AlertDialog checkSettingsDia = new AlertDialog.Builder(context)
				.setMessage("GPS or Network location services are disabled. To get the full functionality of Missing Droid, both should be enabled.\n\n" +
						"Would you like to open the settings and enable both now?")
				.setView(dontShow)
				.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						if (dontShow.isChecked()){
							editor.putBoolean("checkSettings", false);
							editor.commit();
						}
						Intent myIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS );
					    startActivity(myIntent);
					}
				})
				.setNeutralButton("Not Now", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						if (dontShow.isChecked()){
							editor.putBoolean("checkSettings", false);
							editor.commit();
						}
					}
				})
				.show();
				
				return checkSettingsDia;
		}
		return null;
	}
	
	@Override
	 protected void onActivityResult(int requestCode, int resultCode,
             Intent data) {
         if (requestCode == 0) {
             if (resultCode == RESULT_OK) {
                 
             } else {
            	 finish();
             }
         }
     }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.hostmenu, menu);
	    return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    // Handle item selection
	    switch (item.getItemId()) {
	    case R.id.uninstall:
	    	if (Build.VERSION.SDK_INT>7){
				if (AdminConditionalClass.checkAdmin(context)){
					AdminConditionalClass.removeAdmin(context);
					//Toast.makeText(context, "Administration disabled.", Toast.LENGTH_LONG).show();
				} 
				Uri packageUri = Uri.parse("package:"+getPackageName());
	            Intent uninstallIntent =
	              new Intent(Intent.ACTION_DELETE, packageUri);
	            startActivity(uninstallIntent);
			}
	        return true;
	    case R.id.upgrade:
	    	Market.donate(context);
	    	
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}
	
	
}
