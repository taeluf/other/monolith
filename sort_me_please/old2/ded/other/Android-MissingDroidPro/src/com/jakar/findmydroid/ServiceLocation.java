package com.jakar.findmydroid;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.text.format.DateFormat;
import android.util.Log;

public class ServiceLocation extends Service {
	
	
	private static final String tag = "MISSING_DROID";
		
		Context context;
		
		boolean toggledGPS = false;
	
		LocationManager locMan;
		LocationListener locList;
		LocationListener locList2;
		Location location;
		String provider;
		int status;
		Bundle extras;
		String fromAddress;
		StringBuilder sb;
		StringBuilder ab;
		StringBuilder xb;
		String nL = System.getProperty("line.separator");
		Geocoder geoCod;
		Address address;
		Thread llThread;
		Thread llThread2;
		boolean Located = false;
		boolean completedAddress = false;
		List<Address> addresses;
		Handler timeOutHandler;
		Runnable timeOutRunnable;
		Runnable timeOutRunnable2;
		
		String stringSend;
		
		Thread geoCodThread;
		
		double Acc;
		double Lat;
		double Long;
		double Accuracy;
		double Bear;
		double Speed;
		double Time;
		
		double Acc2;
		double Lat2;
		double Long2;
		double Accuracy2;
		double Bear2;
		double Speed2;
		double Time2;
		
		double Latit;
		double Longit;
		double Accur;
		double Bearing;
		double Speedit;
		int i;
		int i2;
		boolean locChanged;
		boolean loc2Changed;
		boolean locAcc;
		boolean loc2Acc;
		boolean locBear;
		boolean loc2Bear;
		boolean locSpeed;
		boolean loc2Speed;
		boolean goodAcc;
		
		boolean doneLocating;
		
		final int f1 = 10;
		
		boolean running;
		int arrayInt;
		ArrayList<String> fromArrayList;
		
		ArrayList<String> sentSmsRecMsg;
		ArrayList<Integer> sendSmsRecMsgAttempts;
		
		ArrayList<String> sentSmsStolenMsg;
		ArrayList<Integer> sendSmsStolenMsgAttempts;
		
		ArrayList<String> sentSmsLocationFound;
		ArrayList<Integer> sentSmsLocationFoundAttempts;

		ArrayList<String> sentSmsMapLink;
		ArrayList<Integer> sentSmsMapLinkAttempts;

		ArrayList<String> sentSmsAddress;
		ArrayList<Integer> sentSmsAddressAttempts;
		
		
		Location locat;
		
		SmsManager smsMan;
		
		SharedPreferences pref;
		SharedPreferences.Editor editor;
		PowerManager pm; 
		PowerManager.WakeLock wl;
				
	@Override
	public void onCreate(){
		context = this;
		pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MD_WAKE_LOCK");
		wl.acquire();
		//Log.i(tag, "FMD_SERVICE HAS WAKELOCK: " + wl.isHeld());
		goodAcc = false;
		sb = new StringBuilder();
		ab = new StringBuilder();
		xb = new StringBuilder();
		arrayInt = 0;
		running = false;
		fromArrayList = new ArrayList<String>();
		smsMan = SmsManager.getDefault();
		
		sentSmsRecMsg = new ArrayList<String>();
		sendSmsRecMsgAttempts = new ArrayList<Integer>();
		
		sentSmsStolenMsg = new ArrayList<String>();
		sendSmsStolenMsgAttempts = new ArrayList<Integer>();
		
		sentSmsLocationFound = new ArrayList<String>();         
		sentSmsLocationFoundAttempts = new ArrayList<Integer>();
		                              
		sentSmsMapLink = new ArrayList<String>();               
		sentSmsMapLinkAttempts = new ArrayList<Integer>();      
		                              
		sentSmsAddress = new ArrayList<String>();               
		sentSmsAddressAttempts = new ArrayList<Integer>();      
		
		pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		editor = pref.edit();
			
		timeOutHandler = new Handler();
		timeOutRunnable = new Runnable(){
			public void run(){
					locMan.removeUpdates(locList);
					locMan.removeUpdates(locList2);
					getGps();
			}
		};
		timeOutRunnable2 = new Runnable(){
			public void run(){
				if (geoCodThread.isAlive()){
					try {geoCodThread.interrupt();}
				 catch (Exception e){
//					 e.printStackTrace(); Log.e(tag, e.getMessage());
					 }
					M.startSmsMessageService(context, xb.toString(), null, fromArrayList, 4, false);
				 //finishFinding();
			}
				timeOutHandler.postDelayed(new Runnable(){
					public void run(){
						stopSelf();
					}
				}, 1000*60);
			}
		};
		
		geoCod = new Geocoder(this, Locale.getDefault());
		locMan = (LocationManager)getSystemService(LOCATION_SERVICE);
		
		
		locList = new LocationListener(){
			// start location changed
			@Override
			public void onLocationChanged(Location loc) {
				locChanged = true;		
						if (Double.valueOf(loc.getSpeed()) > 2){
							Acc = loc.getAccuracy();
							Lat = loc.getLatitude();
							Long = loc.getLongitude();
							if (loc.hasBearing()){
								Bear = loc.getBearing(); 
								locBear = true; 
							}
							if (loc.hasSpeed()){
								Speed = loc.getSpeed(); 
								locSpeed = true; 
							}
							String tol = pref.getString("locTol", "100");
							if (tol.equals("") || tol==null){
								tol = "100";
							}
							double tolerance;
							try {
								tolerance = Double.valueOf(tol);
							} catch (Exception e){
								e.printStackTrace();
								tolerance = 100;
							}
							
//							Log.i(tag, "Tolerance Double: " +String.valueOf(tolerance));
//							Log.i(tag, "Tolerance String: " + tol);
							if (Acc < tolerance){
								goodAcc=true;
							}
							Time = loc.getTime();
						} else if (loc.hasAccuracy()){
							if (loc.getAccuracy() < Acc || !locAcc){
								Acc = loc.getAccuracy();
								Lat = loc.getLatitude();
								Long = loc.getLongitude();
								if (loc.hasBearing()){
									Bear = loc.getBearing(); 
									locBear = true; 
								}
								if (loc.hasSpeed()){
									Speed = loc.getSpeed(); 
									locSpeed = true; 
								}
								locAcc = true;
								String tol = pref.getString("locTol", "100");
								if (tol.equals("") || tol==null){
									tol = "100";
								}
								double tolerance = Double.valueOf(tol);
								//Log.i(tag, "Tolerance Double: " +String.valueOf(tolerance));
								//Log.i(tag, "Tolerance String: " + tol);
								if (Acc < tolerance)
								goodAcc= true;
							}
							String tol = pref.getString("locTol", "100");
							if (tol.equals("") || tol==null){
								tol = "100";
							}
							double tolerance;
							try {
								tolerance = Double.valueOf(tol);
							} catch (Exception e){
								e.printStackTrace();
								tolerance = 100;
							}
							
//							Log.i(tag, "Tolerance Double: " +String.valueOf(tolerance));
//							Log.i(tag, "Tolerance String: " + tol);
							if (Acc < tolerance){
								goodAcc=true;
							}
							Time = loc.getTime();
						} else if (!locAcc){
							Lat = loc.getLatitude();
							Long = loc.getLongitude();
							if (loc.hasBearing()){
								Bear = loc.getBearing(); 
								locBear = true; 
							}
							if (loc.hasSpeed()){
								Speed = loc.getSpeed(); 
								locSpeed = true; 
							}
							Time = loc.getTime();
						}
						++i;
						editor.putString("Lat", Double.toString(Lat));
						editor.putString("Long", Double.toString(Long));
						editor.putString("Bear", Double.toString(Bear));
						editor.putString("Speed", Double.toString(Speed));
						editor.putString("Acc", Double.toString(Acc));
						editor.putString("Time", Double.toString(Time));
						editor.commit(); 
					if (goodAcc){
						  locMan.removeUpdates(locList);
						  locMan.removeUpdates(locList2);
						  getGps();
					  }
					//Log.i(tag, "GPS LOCATION CHANGED");
					//Log.i(tag, "GPS LAT: " + loc.getLatitude());
					//Log.i(tag, "GPS LONG: " + loc.getLongitude());
					//Log.i(tag, "GPS ACC: " + loc.getAccuracy());
					//Log.i(tag, "GPS SPEED: " + loc.getSpeed());

			}
				
			@Override
			public void onProviderDisabled(String provider){
					//Log.i(tag, "GPS is DISABLED");
				
				boolean autoGPS = pref.getBoolean("autoGPS", false);
		    	if (autoGPS) {
			    	new Handler().postDelayed(new Runnable(){
			    		@Override
			    		public void run(){
			    			M.enableGPS(context);
							//Log.i(tag, "ATTEMPTING TO ENABLE GPS");
			    		}
			    	}, 50);
		    		toggledGPS = true;
		    	}
		    	else {
				sb.append("GPS is off(less accurate)" + nL);		
				i = 5;
				//Log.i(tag, "NOT ENABLING GPS");
		    	}				
			}
			@Override
			public void onProviderEnabled(String provider) {
				//Log.i(tag, "GPS IS ON");
			}
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras){
				switch(status) {
					case LocationProvider.OUT_OF_SERVICE:
					case LocationProvider.TEMPORARILY_UNAVAILABLE:
					case LocationProvider.AVAILABLE:
						break;
				}
			} 
		};		
		
		locList2 = new LocationListener(){
			// start location changed
			@Override
			public void onLocationChanged(Location loc2) {
					loc2Changed = true;
						if (Double.valueOf(loc2.getSpeed()) > 2){
							Acc2 = loc2.getAccuracy();
							Lat2 = loc2.getLatitude();
							Long2 = loc2.getLongitude();
						if (loc2.hasBearing()){
							Bear2 = loc2.getBearing(); 
							loc2Bear = true; 
						}
						if (loc2.hasSpeed()){
							Speed2 = loc2.getSpeed(); 
							loc2Speed = true; 
						};
						Time2 = loc2.getTime();
						String tol = pref.getString("locTol", "100");
						if (tol.equals("") || tol==null){
							tol = "100";
						}
						double tolerance;
						try {
							tolerance = Double.valueOf(tol);
						} catch (Exception e){
							e.printStackTrace();
							tolerance = 100;
						}
//						Log.i(tag, "Tolerance Double: " +String.valueOf(tolerance));
//						Log.i(tag, "Tolerance String: " + tol);
						if (Acc2 < tolerance){
							goodAcc = true;
						}
						} else if (loc2.hasAccuracy()){
							if (loc2.getAccuracy() < Acc2 || !loc2Acc){
								Acc2 = loc2.getAccuracy();
								Lat2 = loc2.getLatitude();
								Long2 = loc2.getLongitude();
							if (loc2.hasBearing()){ 
								Bear2 = loc2.getBearing(); 
								loc2Bear = true; 
							}
							if (loc2.hasSpeed()){
								Speed2 = loc2.getSpeed(); 
								loc2Speed = true; 
							};
							loc2Acc = true;
							}
							Time2 = loc2.getTime();
							
							String tol = pref.getString("locTol", "100");
							if (tol.equals("") || tol==null){
								tol = "100";
							}
							double tolerance;
							try {
								tolerance = Double.valueOf(tol);
							} catch (Exception e){
								e.printStackTrace();
								tolerance = 100;
							}
//							Log.i(tag, "Tolerance Double: " +String.valueOf(tolerance));
//							Log.i(tag, "Tolerance String: " + tol);
							if (Acc2 < tolerance){
								goodAcc = true;
							}
							
						} else if (!loc2Acc){
							Lat2 = loc2.getLatitude();
							Long2 = loc2.getLongitude();
							if (loc2.hasBearing()){ 
								Bear2 = loc2.getBearing(); 
								loc2Bear = true; 
							}
							if (loc2.hasSpeed()){
								Speed2 = loc2.getSpeed(); 
								loc2Speed = true; 
							};
							Time2 = loc2.getTime();
						}
						++i2;
						editor.putString("Lat2", Double.toString(Lat2));
						editor.putString("Long2", Double.toString(Long2));
						editor.putString("Bear2", Double.toString(Bear2));
						editor.putString("Speed2", Double.toString(Speed2));
						editor.putString("Acc2", Double.toString(Acc2));
						editor.putString("Time2", Double.toString(Time2));
						editor.commit();
					
					if (goodAcc){
						  locMan.removeUpdates(locList);
						  locMan.removeUpdates(locList2);
						  getGps();
					  }
					//Log.i(tag, "NETWORK LOCATION CHANGED");
					//Log.i(tag, "NETWORK LAT: " + loc2.getLatitude());
					//Log.i(tag, "NETWORK LONG: " + loc2.getLongitude());
					//Log.i(tag, "NETWORK ACC: " + loc2.getAccuracy());
					//Log.i(tag, "NETWORK SPEED: " + loc2.getSpeed());
			}
			
			@Override
			public void onProviderDisabled(String provider){			
			}
			@Override
			public void onProviderEnabled(String provider) {
				//Log.i(tag, "NETWORK_PROVIDER_ON");
			}
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras){
				switch(status) {
					case LocationProvider.OUT_OF_SERVICE:
					case LocationProvider.TEMPORARILY_UNAVAILABLE:
					case LocationProvider.AVAILABLE:
						break;
				}
			} 
		};
		
	}
	//boolean netProv;
	
	public void startLocating(){
		String timeOutString = pref.getString("locTimeOut", "3");
		Long timeOut = Integer.valueOf(timeOutString).longValue();
		
		if (locMan.isProviderEnabled(LocationManager.GPS_PROVIDER) || locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER)||pref.getBoolean("autoGPS", false)){
			timeOutHandler.postDelayed(timeOutRunnable, 1000 * 60 * timeOut);
			
			try {locMan.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000*15, 0, locList);}
				catch (Exception e){
					//Log.e(tag, e.getMessage());
					}
		
		
			try {locMan.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000*15, 0, locList2);}
				catch (Exception e){
					//Log.e(tag, e.getMessage());
					}
		} else {
			sb.append("Location services are off." + nL);
			timeOutHandler.postDelayed(timeOutRunnable, 1000*60 / 6);
			
		}
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
			try {intent.getExtras();} catch (Exception e){
				//Log.e(tag, e.getMessage());
				//e.printStackTrace();
			}
			try {if (intent.hasExtra("trustedFriends")){
				fromArrayList.addAll(intent.getStringArrayListExtra("trustedFriends"));
				//Log.i(tag, "NUMBERS DELIVERED:" + intent.getStringArrayListExtra("trustedFriends"));
				arrayInt=fromArrayList.size();
			}} catch (Exception e){
				//Log.e(tag, e.getMessage());
				//e.printStackTrace();
			}
			try {
			if (intent.hasExtra("fromAddress")){
				fromArrayList.add(intent.getStringExtra("fromAddress"));
				//Log.i(tag, "NUMBER DELIVERED:" + intent.getStringExtra("fromAddress"));
			}} catch (Exception e){
				//Log.e(tag, e.getMessage());
				//e.printStackTrace();
			}
			
			//Log.i(tag, "CURRENT ARRAY OF NUMBERS: " + fromArrayList.toString());
			++arrayInt;
			boolean checkSim = intent.getBooleanExtra("checkSim", false);
			if (checkSim){
				String oldNum = pref.getString("oldNum", "");
				String howLong = pref.getString("locTimeOut", "3");
				String phone_stolen_message = "Phone number/sim has changed. Old number was " + oldNum + ". Please allow up to " + howLong +
						" minutes for location.";
				M.startSmsMessageService(context, phone_stolen_message, null, fromArrayList, 3, false);
			} else {
				if (pref.getBoolean("msgLocConf", true)){
					String howLong = pref.getString("locTimeOut", "3");
					String sms_rec_message = "Your message has been received. Please allow up to " + howLong + " minutes for location";
					M.startSmsMessageService(context, sms_rec_message, null, fromArrayList, 3, false);
				}
			}
				
			if (locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
				WifiManager wifiMan = (WifiManager)getSystemService(Context.WIFI_SERVICE);
				if (pref.getBoolean("autoWifi", false)){
					if (!wifiMan.isWifiEnabled()){
						wifiMan.setWifiEnabled(true);
						//Log.i(tag, "WIFI_AUTO_ENABLED");
					}
				}
			}
		if (!running) {
			running = true;
			locChanged = false;
			loc2Changed = false;
			locAcc = false;
			loc2Acc = false;
			locBear = false;
			loc2Bear = false;
			locSpeed = false;
			loc2Speed = false;
			
			
			
			sb.setLength(0);
			i = 0;
			i2 = 0;
			
			getBattery();
			
	//		logger = new StringBuilder();
	//		logger2 = new StringBuilder();
	//		logger3 = new StringBuilder();
	//		
	//		logger.setLength(0);
	//		logger2.setLength(0);
	//		logger3.setLength(0);	    	
	    	startLocating();
		}
		return START_NOT_STICKY;
	}
		
	// start getting the GPS
	@SuppressWarnings("unused")
	public void getGps(){
		
		doneLocating = true;

		
			if (!locChanged && !loc2Changed){
				//Log.i(tag, "GETTING LAST KNOWN LOCATION");
				locAcc = true;
				loc2Acc = true;
				locBear = true;
				loc2Bear = true;
				if (locMan.isProviderEnabled(LocationManager.GPS_PROVIDER)){
					try {
						Location loc1 = locMan.getLastKnownLocation(LocationManager.GPS_PROVIDER);
						Lat = loc1.getLatitude();
						Long = loc1.getLongitude();
						Bear = loc1.getBearing();
						Speed = loc1.getSpeed();
						Time = loc1.getTime();
						Acc = loc1.getAccuracy();
						if (loc1.hasBearing())
							locBear = true;
						if (loc1.hasSpeed())
							locSpeed = true;
						if (loc1.hasAccuracy())
							locAcc = true;
						
							editor.putString("Time", Double.toString(Time));
							editor.putString("Lat", Double.toString(Lat));
							editor.putString("Long", Double.toString(Long));
							editor.putString("Bear", Double.toString(Bear));
							editor.putString("Speed", Double.toString(Speed));
							editor.putString("Acc", Double.toString(Acc));
							editor.commit();
					} catch (Exception e){
						e.printStackTrace();
					}
					} else {
//						Lat = Double.parseDouble(pref.getString("Lat", "0"));
//						Long = Double.parseDouble(pref.getString("Long", "0"));
//						Acc =   Double.parseDouble(pref.getString("Acc", "0"));
//						Bear =  Double.parseDouble(pref.getString("Bear", "0"));
//						Speed = Double.parseDouble(pref.getString("Speed", "0"));
//						Time = Double.parseDouble(pref.getString("Time", "0"));
//						locAcc = true;
//						locSpeed = true;
//						locBear = true;
					}
				if (locMan.isProviderEnabled(LocationManager.NETWORK_PROVIDER)){
					try {
					Location loc2 = locMan.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
					Lat2 = loc2.getLatitude();
					Long2 = loc2.getLongitude();
					Bear2 = loc2.getBearing();
					Speed2 = loc2.getSpeed();
					Time2 = loc2.getTime();
					Acc2 = loc2.getAccuracy();
					if (loc2.hasBearing())
						locBear = true;
					if (loc2.hasSpeed())
						locSpeed = true;
					if (loc2.hasAccuracy())
						locAcc = true;
						editor.putString("Time2", Double.toString(Time2));
					 	editor.putString("Lat2", Double.toString(Lat2));
						editor.putString("Long2", Double.toString(Long2));
						editor.putString("Bear2", Double.toString(Bear2));
						editor.putString("Speed2", Double.toString(Speed2));
						editor.putString("Acc2", Double.toString(Acc2));
						editor.commit();
					} catch (Exception e){
						e.printStackTrace();
					}
				} else {
//					Lat2 = Double.parseDouble(pref.getString("Lat2", "0"));
//					Long2 = Double.parseDouble(pref.getString("Long2", "0"));
//					Acc2 =   Double.parseDouble(pref.getString("Acc2", "0"));
//					Bear2 =  Double.parseDouble(pref.getString("Bear2", "0"));
//					Speed2 = Double.parseDouble(pref.getString("Speed2", "0"));
//					Time2 = Double.parseDouble(pref.getString("Time2", "0"));
//					loc2Acc = true;
//					loc2Speed = true;
//					loc2Bear = true;
				}
				if (Double.valueOf(Time2) -  Double.valueOf(Time) >= 1000 * 60 * 5 || Double.valueOf(Time)==0){
					Lat = Lat2;
					Long = Long2;
					Acc = Acc2;
					Bear = Bear2;
					Speed= Speed2;
				} else if (Double.valueOf(Time) -  Double.valueOf(Time2) >= 1000 * 60 * 5 || Double.valueOf(Time2)==0){
					Lat2 = Lat;
					Long2 = Long;
					Acc2 = Acc;
					Bear2 = Bear;
					Speed2 = Speed;
				}
				sb.append("Location not found" + nL);
				sb.append("Last known location:"+nL);
				
			}
		
		
		if (!locAcc && !loc2Acc) {
			if (!locChanged){
				Latit = Lat2;
				Longit = Long2;
				Bearing = Bear2;
				Speedit = Speed2;
			} else if (!loc2Changed){
				Latit = Lat;
				Longit = Long;
				Bearing = Bear;
				Speedit = Speed;
			} else {
				Latit = Lat;
				Longit = Long;
				Bearing = Bear;
				Speedit = Speed;
			}
			
		} 
		else if (!loc2Acc){
			Accur = Acc;
			Latit = Lat;
			Longit = Long;
			Bearing = Bear;
			Speedit = Speed;
		} else if (!locAcc){
			Accur = Acc2;
			Latit = Lat2;
			Longit = Long2;
			Bearing = Bear2;
			Speedit = Speed2;
		} else if (Double.valueOf(Acc2) >= Double.valueOf(Acc)){
			Accur = Acc;
			Latit = Lat;
			Longit = Long;
			Bearing = Bear;
			Speedit = Speed;
		} else if (Double.valueOf(Acc) >= Double.valueOf(Acc2)){
			Accur = Acc2;
			Latit = Lat2;
			Longit = Long2;
			Bearing = Bear2;
			Speedit = Speed2;
		} 
		
		sb.append("Coordinates: " + Latit + ", " + Longit + nL);	
		
		if (!locAcc && !loc2Acc){sb.append("Accuracy is unkown" + nL);
		} else {
			StringBuilder ASB = new StringBuilder();
			ASB.append(Accur);
			int point = ASB.indexOf(".");
			ASB.setLength(point);
			sb.append("Accuracy: " + ASB.toString()  + " meters"+ nL);
		}
		if (locBear || loc2Bear){		
			double N = 0;
			double NNE = 22.5;
			double NE = 45;
			double ENE = 67.5;
			double E = 90;
			double ESE = 112.5;
			double SE = 135;
			double SSE = 157.5;
			double S = 180;
			double SSW = 202.5;
			double SW = 225;
			double WSW = 247.5;
			double W = 270;
			double WNW = 292.5;
			double NW = 315;
			double NNW = 337.5;
			double degrees = Bearing;
			StringBuilder direction = new StringBuilder();
			if (degrees <= ENE || degrees == 360) {
					direction.append("North");
				if (degrees >= NNE && degrees <= ENE){
					direction.append("East");
				}
			} else if (degrees > ENE && degrees <= ESE){
				direction.append("East");
			} else if (degrees > ESE && degrees <= WSW){
				direction.append("South");
				if (degrees > ESE && degrees <= SSE){
					direction.append("East");
				} else if (degrees >= SSW && degrees <= WSW){
					direction.append("West");
				}
			} else if (degrees > WSW && degrees <= WNW){
				direction.append("West");
			} else if (degrees > WNW && degrees <= 360){
				direction.append("North");
				if (degrees > WNW && degrees <= NNW){
					direction.append("West");
				}
			}
			sb.append("Bearing: " + direction.toString() + nL);
		} else {
			sb.append("Bearing unkown" + nL);
		}
		
		if (!locSpeed && !loc2Speed){sb.append("Speed unknown");
		}else {
			double mySpeed = Speedit * 2.23693629;
			StringBuilder speedSB = new StringBuilder();
			speedSB.append(mySpeed);
			if (speedSB.toString().contains("."))speedSB.setLength(speedSB.toString().indexOf("."));			
			sb.append("Speed: " + speedSB.toString()  + " mph");
		}
		//Log.i(tag, sb.toString());
		running = false;
		if (pref.getBoolean("msgLocInfo", true)){
			M.startSmsMessageService(context, sb.toString(), null, fromArrayList, 4, false);
		}
		//Log.i(tag, ab.toString());
		if (pref.getBoolean("msgLocLink", true)){
			final String maps_link_message = "http://maps.google.com/maps?q=loc:" + Latit + "," + Longit;
			M.startSmsMessageService(context, maps_link_message , null, fromArrayList, 3, false);
		}
		
		if (pref.getBoolean("msgLocAddress", true)){
			timeOutHandler.postDelayed(timeOutRunnable2, 1000*60*3);
			getAddress(this, new GeocoderHandler());
			
		} else {
			if (!running){
				stopSelf();
			}
		}
	
	}
		
			 
				
		
			
		 // finished getting the GPS
		 
	// start getting address from GPS
		
		
		public void getAddress(final Context context, final Handler handler) {
//			logger3.append("getAddress started");
//			editor.putString("logger3", logger3.toString());
//			editor.commit();
		    (geoCodThread = new Thread() {
		        @Override public void run() {
		            Geocoder geocoder = new Geocoder(context, Locale.getDefault());   
		            String result = null;
		            try {
		                List<Address> list = geocoder.getFromLocation(
		                        Latit, Longit, 1);
		                if (list != null && list.size() > 0) {
		                    address = list.get(0);
		                    // sending back first address line and locality
		                    result = address.getAddressLine(0) + ", " + address.getLocality();
		                    
		                }
		            } catch (IOException e) {
		            	//Log.e(tag, e.getMessage());
		            } catch (Exception e ) {
		            	//Log.e(tag, e.getMessage());
		            	}
		            	finally {
		            		
		            	
		                Message msg = Message.obtain();
		                msg.setTarget(handler);
		            	
		                if (result != null) {
		                    msg.what = 1;
		                    Bundle bundle = new Bundle();
		                    bundle.putString("address", result);
		                    msg.setData(bundle);
		                    xb.setLength(0);
		                    xb.append(result);
		                    M.startSmsMessageService(context, xb.toString(), null, fromArrayList, 4, false);
		                    //Log.i(tag, "ADDRESS: "+ result);
		                } else {
			                msg.what = 0;
			                msg.sendToTarget();
			                xb.setLength(0);
			                xb.append("Unable to obtain address");
			                M.startSmsMessageService(context, xb.toString(), null, fromArrayList, 4, false);
	                    //Log.i(tag, "ADDRESS result is null");
		                }
		                if (!running){
		                	stopSelf();
		                }		                
	            	}
		        }
		    }).start();
		} // finished getting address from GPS
		
		private class GeocoderHandler extends Handler {
		    @Override
		    public void handleMessage(Message message) {
		     try {   String result;
		        switch (message.what) {
		        case 1:
		            Bundle bundle = message.getData();
		            result = bundle.getString("address");
		            break;
		        default:
		            result = null;
		        }
//		        logger3.append(result);
		        editor.commit();
		       
		         }
		        finally {if (geoCodThread.isAlive()) 
		        geoCodThread.interrupt();
		        //finishFinding();
		        }
		    }   
		}
	
		public static String getOnlyNumerics(String str) {
		    
		    if (str == null) {
		        return "";
		    }
		
		    StringBuffer strBuff = new StringBuffer();
		    char c;
		    
		    for (int i = 0; i < str.length() ; i++) {
		        c = str.charAt(i);
		        
		        if (Character.isDigit(c)) {
		            strBuff.append(c);
		        }
		    }
		    return strBuff.toString();
		}
	
		 private void getBattery() {
		        BroadcastReceiver batteryLevelReceiver = new BroadcastReceiver() {
		            public void onReceive(Context context, Intent intent) {
		                context.unregisterReceiver(this);
		                int rawlevel = intent.getIntExtra("level", -1);
		                int scale = intent.getIntExtra("scale", -1);
		                int level = -1;
		                if (rawlevel >= 0 && scale > 0) {
		                    level = (rawlevel * 100) / scale;
		                }
		                sb.append("Battery Remaining: " + level + "%" +nL);
		            }
		        };
		        IntentFilter batteryLevelFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
		        registerReceiver(batteryLevelReceiver, batteryLevelFilter);
		    }		    
		
	public class LocalBinder extends Binder {
		 ServiceLocation getService() {
	            return ServiceLocation.this;
	        }
	    }
	
	private final IBinder mBinder = new LocalBinder();
	
	@Override
  public IBinder onBind(Intent intent) {
		return mBinder;	
	}
	
	@Override
	public void onDestroy(){
		if (toggledGPS){
			M.enableGPS(context);
		}
		int hs = pref.getInt("historySize", 0);
		CharSequence time = DateFormat.format("MM/dd/yyyy h:mmaa", System.currentTimeMillis());
		editor.putString("historyTitle"+hs, "Location Found");
		editor.putString("historySubText"+hs, "Location was found at "+time);
		editor.putString("historyMessage"+hs, "Lat: "+Latit
												+"\nLong: "+Longit
												+"\nAddress: "+xb.toString()
												+"\nLocated By: "+fromArrayList.get(0));
		hs++;
		editor.putInt("historySize", hs);
		editor.commit();
		try {wl.release();}catch (Exception e){
			//e.printStackTrace(); Log.e(tag, e.getMessage());
			}
		try{
			
		timeOutHandler.removeCallbacks(timeOutRunnable);
		timeOutHandler.removeCallbacks(timeOutRunnable2);
		}
		catch (Exception e){
			//e.printStackTrace(); Log.e(tag, e.getMessage());
			}
		stopSelf();
		//Log.i(tag, "FMD_SERVICE HAS WAKELOCK: " +wl.isHeld());
		//Log.i(tag, "FMD_SERVICE DESTROYED");
	}
} 