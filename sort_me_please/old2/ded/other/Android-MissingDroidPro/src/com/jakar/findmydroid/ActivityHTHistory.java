package com.jakar.findmydroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ActivityHTHistory extends Activity{
	
	Context context;
	AlertDialog dia;
	LinearLayout historyLayout;
	
	@Override
	public void onCreate(Bundle bundle){
		super.onCreate(bundle);
		setContentView(R.layout.activityhthistory);
		context = this;				
	}
	@Override
	public void onResume(){
		super.onResume();
		historyLayout = (LinearLayout)findViewById(R.id.historyLayout);
		
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		int size = pref.getInt("historySize", 0);
		
		for (int i=size-1;i>=0;i--){
			String title = pref.getString("historyTitle"+i, "N/A");
			String subText = pref.getString("historySubText"+i, "There is no history yet.");
			String message = pref.getString("historyMessage"+i, "There is no history yet because Missing Droid has not been used.");
			addItem(title,subText,message);
		}
	}
	
	public void addItem(final String title, final String subText, final String message){
		
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.VERTICAL);
		
		TextView tv = new TextView(context);
		tv.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
		tv.setText(title);
		tv.setTextSize(20);
		ll.addView(tv);
		
		TextView tm = new TextView(context);
		tm.setLayoutParams(new LinearLayout.LayoutParams(-1,-2));
		tm.setText(subText);
		ll.addView(tm);
		
		
		
		ll.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dia = null;
				dia = new AlertDialog.Builder(context)
						.setTitle(title)
						.setMessage(message)
						.setPositiveButton("Okay", null)
						.show();			
			}
		});
		
		historyLayout.addView(ll, new LinearLayout.LayoutParams(-1, -2));
			
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if( keyCode == KeyEvent.KEYCODE_BACK) {
	    	M.tabHost.setCurrentTab(0);
	        return true;
	    }else{
	        return super.onKeyDown(keyCode, event);
	    }
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
	    if( keyCode == KeyEvent.KEYCODE_BACK) {
	    	M.tabHost.setCurrentTab(0);
	        return true;
	    }else{
	        return super.onKeyDown(keyCode, event);
	    }
	}

}
