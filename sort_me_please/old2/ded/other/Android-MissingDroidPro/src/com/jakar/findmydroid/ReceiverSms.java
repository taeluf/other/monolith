package com.jakar.findmydroid;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.PhoneNumberUtils;
import android.telephony.SmsMessage;
import android.text.format.DateFormat;
import android.text.format.Time;

public class ReceiverSms extends BroadcastReceiver {

		String secPassword;
		boolean isDataMsg;
		    
    @Override
    public void onReceive(final Context context, Intent intent){
    	SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
    	SharedPreferences.Editor editor = pref.edit();
    	
    	String keyword = pref.getString("keyword", "Android");
    	keyword = keyword.toLowerCase();
    	if (!pref.getBoolean("enableKeyword", true)){
    		keyword = "";
    	}
//    	Log.e(M.tag, intent.toString());
//    	Log.e(M.tag, intent.getExtras().toString());

    	//This is to tell whether Missing Droid handled the sms or not.
    	boolean keywordReceived = false;

    	Bundle bundle = intent.getExtras(); 
        String recMsgString = "";            
        String fromAddress = "";
        SmsMessage recMsg = null;
        byte[] data = null;
        if (bundle != null)
        {
            //---retrieve the SMS message received---
           Object[] pdus = (Object[]) bundle.get("pdus");
            for (int i=0; i<pdus.length; i++){
                recMsg = SmsMessage.createFromPdu((byte[])pdus[i]);
                
                if (intent.getAction()=="android.intent.action.DATA_SMS_RECEIVED"){
                	try {
                    	data = recMsg.getUserData();
                    } catch (Exception e){

                    }
                	if (data!=null){
    	                for(int index=0; index<data.length; ++index)
    	                {
    	                       recMsgString += Character.toString((char)data[index]);
    	                       keyword = pref.getString("locatorKeyword", "");
    	                       keyword = keyword.toLowerCase();
    	                }
                    } else {
                    	return;
                    }
                	isDataMsg = true;
                } else {
                	 recMsgString = recMsg.getMessageBody();
                	 isDataMsg = false;
                }

                fromAddress = recMsg.getOriginatingAddress(); 
            }
//Log.e(M.tag, recMsgString.toString());
				
            	String originalMessage = recMsgString.toLowerCase();
                
                recMsgString = recMsgString.toLowerCase();
                //Log.i(tag, "Message From: " + fromAddress);
                //Log.i(tag, "Message Body: " + recMsgString);
                recMsgString = recMsgString.trim();
            	
                if (!recMsgString.startsWith(keyword)){
                	//Log.i(tag, "Does not start with " + defKeyword);
                	return;
                }
              //Check if only friends are allowed to use this command
				boolean friendRequired = pref.getBoolean("restrictAccess", false);
				//Log.i(tag, String.valueOf(friendRequired));
				
				String friendNum = "";
				//by default, we assume the command is not allowed. We will change this, soon though.
				boolean commandAllowed = false;
				if (friendRequired){
					int numOfFriends = pref.getInt("numOfFriends", 0);
					for (int fr = 0; fr<numOfFriends; fr++){
						String checkNum = pref.getString("friend"+fr, "");
						if (PhoneNumberUtils.compare(checkNum, fromAddress)){
							friendNum = checkNum;
							commandAllowed=true;
						}
					}
				} else commandAllowed=true;
				
				
                int numOfEnemies = pref.getInt("numOfEnemies", 0);
            	if (numOfEnemies>0){
            		for (int en = 0; en <numOfEnemies; en++){
            			String checkNumEnemy = pref.getString("enemy"+en, "");
            			if (PhoneNumberUtils.compare(checkNumEnemy, fromAddress)){
            				//Log.i(tag, fromAddress + " is a blocked number");
            				commandAllowed=false;
            			}
            		}
            	}
            	if (!commandAllowed) {
            		if (pref.getBoolean("msgDeniedConf", true)){
            			String message = "Your phone number has been blocked.";
            			if (friendRequired){
            				message = "Your phone number has not been added to the allow list so your request was cancelled.";
            			}
            			M.startSmsMessageService(context, message, fromAddress, null, 3, false);
            		}
					//Log.i(tag, "Command Allowed: " + String.valueOf(commandAllowed));
					return;
				}
                if (keyword.length() > 0){
                recMsgString = recMsgString.replace(keyword, "");
                }
                recMsgString = recMsgString.trim();
                
              //preset command words
                String comLocate;
                String comRing;
                String comLock; 
                String comWipe; 
                String comForward;
                String comOpen; 
                if (pref.getBoolean("enableCustomCommands", false)&&!isDataMsg){
	            	comLocate = pref.getString("locateCommand", "gps");
	            	comRing = pref.getString("ringCommand", "ring");
	            	comLock = pref.getString("lockCommand", "lock");
	            	comWipe = pref.getString("wipeCommand", "wipe");
	            	comForward = pref.getString("callForwardCommand", "forward");
	            	comOpen = pref.getString("openCommand", "open");
            	} else {
            		comLocate = "gps";
            		comRing = "ring";
            		comLock = "lock";
            		comWipe = "wipe";
            		comForward = "forward";
            		comOpen = "open";
            	}
                comLocate.toLowerCase();
                comRing.toLowerCase();
                comLock.toLowerCase();
                comWipe.toLowerCase();
                comForward.toLowerCase();
                comOpen.toLowerCase();
            	String command;
            	
            	//Log.i(M.tag, "recMsgString contents: " + recMsgString);
            	
            	if (recMsgString.startsWith(comLocate))
            		command = "locate";
            	else if (recMsgString.startsWith(comRing))
            		command = "ring";
            	else if (recMsgString.startsWith(comLock))
            		command = "lock";
            	else if (recMsgString.startsWith(comWipe))
            		command = "wipe";
            	else if (recMsgString.startsWith(comForward)){
            		command = "forward";
            	} else if (recMsgString.startsWith(comOpen)){
            		command = "open";
            	} else return;
            	
            	
            	//Log.i(tag, "Command is " + command);
            	   	
            boolean hasAdmin;
            if (Build.VERSION.SDK_INT>7){
            	hasAdmin = AdminConditionalClass.checkAdmin(context);	
            } else {
            	hasAdmin = false;
            }
            Intent commandIntent = new Intent();    
            //Log.i(tag, "Has Admin: " + String.valueOf(hasAdmin));
            //Log.i(tag, "recMsgString contents: " + recMsgString);
            
            boolean lockAllowed = pref.getBoolean("secLock", false);
            boolean wipeAllowed = pref.getBoolean("secWipe", false);
            Intent outboundSms = new Intent(context, ServiceSendingSms.class);
        	outboundSms.putExtra("recipient", fromAddress);
            if (command.equals("locate")){
            	commandIntent.setClass(context, ServiceLocation.class);
                commandIntent.putExtra("fromAddress", fromAddress);
                commandIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startService(commandIntent);
                keywordReceived = true;
            } else if (command.equals("ring")){
            	commandIntent.setClass(context, ServiceRing.class);
                commandIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startService(commandIntent);
                keywordReceived = true;
                if (pref.getBoolean("msgRingConf", true)){
                	String message = "Your ring request was received. The phone will ring for "+pref.getString("ringTime", "30")+" seconds.";
            		outboundSms.putExtra("message", message);
                	context.startService(outboundSms);
            	}
            } else if (command.equals("lock")){
            	String message = "";
            	if (hasAdmin&&lockAllowed){
            		int defKeyLength = keyword.length();
                	StringBuilder sb = new StringBuilder();
                	sb.append(originalMessage);
                	sb.delete(0, defKeyLength);
                	recMsgString = sb.toString();
                	recMsgString = recMsgString.trim();
                	sb.setLength(0);
                	sb.append(recMsgString);
                	sb.delete(0, comLock.length());
                	recMsgString = sb.toString();
                	recMsgString = recMsgString.trim();
                	//Log.i(tag, "RECEIVED MESSAGE STRING: " + recMsgString);
                	
                	String password;
            		if (recMsgString.length()>0){
            			password = recMsgString;
            		} else {
            			password = pref.getString("secPassword", "password");
            		}
            		if (Build.VERSION.SDK_INT>7){
            			if (password.length()>8||password.length()<4){
            				message = "Your lock request was received, but password did not meet length requirements of 4-8 characters.";
            			} else if (AdminConditionalClass.lockPhone(context, password)){
            				message = "Your lock request was received. Phone was locked and the password is: "+password;
            			} else {
            				message = "Your lock request was received. Password was not changed - unknown error.";
            			}
                    	editor.putString("secPassword", password);
                    	editor.commit();
                    	
            		} else {
            			message = "Your lock request was received. Phone was not locked because the Android version is not compatible.";
            		}
            		
            	} else {
            		message = "Your lock request was received. Remote locking is not enabled, so phone has not been locked.";
            	}
            	if (pref.getBoolean("msgLockConf", true)){
            		outboundSms.putExtra("message", message);
                	context.startService(outboundSms);
            	}
            	keywordReceived = true;
            } else if (command.equals("wipe")){
            	if (hasAdmin && wipeAllowed&&Build.VERSION.SDK_INT>7){
            		AdminConditionalClass.wipePhone(context);
            	} else {}
            	keywordReceived = true;
            } else if (command.equals("forward")){
        		recMsgString = recMsgString.replace(comForward, "");
        		recMsgString = recMsgString.trim();
        		
        		String forwardNumber = recMsgString;
        		//Log.i(tag, forwardNumber);
        		M.setCallForwarding(context, forwardNumber);
            	keywordReceived = true;
            	//Log.i(tag, forwardNumber);
            	String message = "";
            	if (forwardNumber.length() < 4){
            		message = "Your message was received. Phone will attempt to turn off call forwarding. Call in 30 seconds to test.";
            	} else {
            		message = "Your message was received. Phone will attempt to forward calls to "+forwardNumber
        					+ " Call in 30 seconds to test.";
            	}
            	if (pref.getBoolean("msgCallForwardConf", true)){
            		outboundSms.putExtra("message", message);
                	context.startService(outboundSms);
            	}
            } else if (command.equals("open")){
            	PackageManager pm = context.getPackageManager();
    			ComponentName compName = new ComponentName(context, ActivityTabHost.class);
    			pm.setComponentEnabledSetting(compName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
    				PackageManager.DONT_KILL_APP);
    			context.startActivity(new Intent(context, ActivityTabHost.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
    			editor.putBoolean("hideApp", false);
    			editor.commit();
        		command = "friendsOpen";
        	    keywordReceived=true;
        	    if (pref.getBoolean("msgCallForwardConf", true)){
            		outboundSms.putExtra("message", "Your request has been received and Missing Droid has been opened on the phone.");
                	context.startService(outboundSms);
            	}
            }
            //Log.i(tag, "recMsgString contents: " + recMsgString);
 
            boolean hideSms = pref.getBoolean("hideSMS", false);
        	if (hideSms && keywordReceived){
        	abortBroadcast();
        	}
        	if (keywordReceived){
        		int hs = pref.getInt("historySize", 0);
        		CharSequence time = DateFormat.format("MM/dd/yyyy h:mmaa", System.currentTimeMillis());
        		editor.putString("historyTitle"+hs, "Sms Received");
        		editor.putString("historySubText"+hs, "Sms was received from "+fromAddress+" at "+time);
        		editor.putString("historyMessage"+hs, "From: "+fromAddress
        												+"\nContents: "+originalMessage
        												+"\nDate-Time: "+time);
        		hs++;
        		editor.putInt("historySize", hs);
        		int timesUsed = pref.getInt("timesUsed", 0);
        		timesUsed++;
        		editor.putInt("timesUsed", timesUsed);
        		editor.commit();
        	}
//        	
       } // end of if (bundle != null)
    } // end of onReceive
} // end of ReceiverSms.class

