package com.jakar.findmydroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.widget.Toast;

public class Market {
	
	public static String upgrade = "Thank you for using Missing Droid! \n\nTo use this feature, you must upgrade.";
	
	public static boolean upgradeDialog(final Context context, String special_message){
		if (Market.isPro(context)){
			return true;
		}
		String message = Market.upgrade;
		if (special_message!=null){
			message = special_message;
		}
		new AlertDialog.Builder(context)
			.setTitle("Upgrade!")
			.setMessage(message)
			.setNegativeButton("Upgrade", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Market.donate(context);
				}
			})
			.setPositiveButton("Not Now", null)
			.show();
		return false;
	}
	
	public static boolean isMarket(Context context){
		if (M.testing){
			return M.market;
		}
		boolean isMarketSig = false;
		int currentSig = 1;
		try {
			Signature[] sigs = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
			for (Signature sig : sigs)
			{
				currentSig = sig.hashCode();
//			    Log.i("MISSING_DROID", "Signature hashcode: " + sig.hashCode());
			}
			} catch (Exception e){
				e.printStackTrace();
			}
		if (currentSig==-1545485543 || M.market){
			isMarketSig = true;
		} else {
			isMarketSig = false;
		}
		
		return isMarketSig;
	}
	
	public static void rate(Context context){
		String url;
		if (isMarket(context)){
			url = "market://details?id="+context.getPackageName();
		} else {
			url = "http://www.amazon.com/gp/mas/dl/android?p="+context.getPackageName();
		}
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		context.startActivity(intent);
	}
	public static void donate(Context context){
		String url;
		if (isMarket(context)){
			url = "market://details?id=com.jakar.findmydroid";
		} else {
			url = "http://www.amazon.com/gp/mas/dl/android?p=com.jakar.findmydroid";
		}
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		context.startActivity(intent);
	}
	public static void apps(Context context){
		String url;
		if (isMarket(context)){
			url = "market://search?q=pub:\"Jakar\"";
		} else {
			String pn = context.getPackageName();
			url = "http://www.amazon.com/gp/mas/dl/android?p="+pn+"&showAll=1";
		}
		Uri uri = Uri.parse(url);
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		context.startActivity(intent);
	}
	
	public static boolean isPro(Context context){
		if (M.testing){
			return M.pro;
		}	
		
		boolean hasPro;
		
		String pn = context.getPackageName();
//	    Log.i(tag, pn);
	    if (pn.equals("com.jakar.findmydroid") || M.pro){
	    	hasPro = true;
	    } else {
	    	hasPro = false;
	    }
	    	
		return hasPro;
	}
	

}
