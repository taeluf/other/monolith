package com.jakar.findmydroid;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActivityStopRing extends Activity{
	
	Button stopRinging;
	Button keepRinging;
	Intent ringStop;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.stopring);
        
        Context context = this;
        
        
        stopRinging = (Button)findViewById(R.id.stopRinging);
        ringStop = new Intent();
        ringStop.setClass(this, ServiceRing.class);
        
        
        
	}
	@Override
	public void onResume(){
		super.onResume();
        final String pn = this.getPackageName();
		registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent intent) {
            	unregisterReceiver(this);
            	finish();
                }
            }
        , new IntentFilter(pn+".stopRing.class"));
		
		stopRinging.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				stopService(ringStop);
				Intent startMain = new Intent(Intent.ACTION_MAIN);
				startMain.addCategory(Intent.CATEGORY_HOME);
				startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(startMain);
				finish();
			}
		});
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
	}

}
