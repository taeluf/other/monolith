package com.jakar.findmydroid;

import android.annotation.SuppressLint;
import android.app.admin.*;
import android.content.*;
import android.preference.PreferenceManager;
import android.widget.*;

@SuppressLint("NewApi")
public class AdminReceiver extends DeviceAdminReceiver {

		final String TAG = "MISSING_DROID";
	
	    @Override
	    public void onEnabled(Context context, Intent intent) {
	    	Toast.makeText(context, "Administration Granted", Toast.LENGTH_LONG).show();
        	SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        	SharedPreferences.Editor editor = pref.edit();
        	editor.putBoolean("reqAdmin", true);
        	editor.commit();
        	//Log.i(TAG, "DEVICE ADMINISTRATION ENABLED");
	    }

	    @Override
	    public CharSequence onDisableRequested(final Context context, Intent intent) {
	    	SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
	    	String secPassword = pref.getString("secPassword", "password");
	    	boolean preventUninstall = pref.getBoolean("preventUninstall", false);
	    	boolean secLock = pref.getBoolean("secLock", false);
	    	if (preventUninstall && secLock){
	    	Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			context.startActivity(startMain);
			AdminConditionalClass.lockPhone(context, secPassword);
        	//Log.i(TAG, "DEVICE ADMINISTRATION DISABLE REQUESTED & LOCKED PHONE");
	    	} 
        	//Log.i(TAG, "DEVICE ADMINISTRATION DISABLE REQUESTED AND GRANTED");
    		return "If you deactivate, Missing Droid will lose some of it's protective capabilities, only click Ok if you are" +
    				" sure you want to remove your protection";
	    }

	    @Override
	    public void onDisabled(Context context, Intent intent) {
	    	SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        	SharedPreferences.Editor editor = pref.edit();
        	editor.putBoolean("preventUninstall", false);
        	editor.putBoolean("secLock", false);
        	editor.putBoolean("secWipe", false);
        	editor.putBoolean("reqAdmin", false);
        	editor.commit();
			Toast.makeText(context, "Administration Disabled", Toast.LENGTH_LONG).show();
        	//Log.i(TAG, "DEVICE ADMINISTRATION DISABLED");
	    }

	    @Override
	    public void onPasswordChanged(Context context, Intent intent) {
        	//Log.i(TAG, "DEVICE PASSWORD CHANGED");
	    }

	    void showToast(Context context, CharSequence msg) {
	    }
	
	}
