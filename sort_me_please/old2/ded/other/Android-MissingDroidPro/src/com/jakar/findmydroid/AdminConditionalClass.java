package com.jakar.findmydroid;

import android.app.admin.*;
import android.content.*;
import android.preference.PreferenceManager;

public abstract class AdminConditionalClass {
	
	
	
	final String tag = "MISSING_DROID";
	static ComponentName devAdminReceiver = null;
	static DevicePolicyManager dpm = null;
	static SharedPreferences pref = null;
	
	public static void prepare(Context context){
		devAdminReceiver = new ComponentName(context, AdminReceiver.class);
		dpm = (DevicePolicyManager)context.getSystemService(Context.DEVICE_POLICY_SERVICE);
		pref = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public static boolean checkAdmin(Context context){
		prepare(context);
		return dpm.isAdminActive(devAdminReceiver);
	}
	public static void removeAdmin(Context context){
		prepare(context);
		dpm.removeActiveAdmin(devAdminReceiver);
	}
	public static boolean lockPhone(Context context, String password){
		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(startMain);
		
		prepare(context);
		if (password==null){
			password = pref.getString("secPassword", "password");
		}
		boolean pwChange = dpm.resetPassword(password, 0);
		dpm.lockNow();
		return pwChange;
	}	
	public static void wipePhone(Context context){
		prepare(context);
		dpm.wipeData(0);
	}
	
}
         
	
