package com.jakar.findmydroid;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

public class EnterPassword extends Activity{
	
	Context context;
	
	@Override
	public void onCreate(Bundle s){
		super.onCreate(s);
		setResult(RESULT_CANCELED);
		setContentView(R.layout.enterpassword);
		
		getWindow().setLayout(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		context = this;
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		
		final String savedPassword = pref.getString("secPassword", "password");
		String passwordHint = pref.getString("passwordHint", "");
		
		final EditText enterPassword = (EditText)findViewById(R.id.initialpassword);
		Button done = (Button)findViewById(R.id.donePassword);
		Button cancel = (Button)findViewById(R.id.cancelPassword);
		TextView hint = (TextView)findViewById(R.id.enterPasswordHint);
		
		hint.setText(passwordHint);
		
		done.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String enteredPassword = enterPassword.getText().toString();
				if (enteredPassword.equals(savedPassword)){
					setResult(RESULT_OK);
					finish();
				} else {
					Toast.makeText(context, "Incorrect Password", Toast.LENGTH_SHORT).show();
				}
			}
		});
		cancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
			}
		});
		
		
	}

}
