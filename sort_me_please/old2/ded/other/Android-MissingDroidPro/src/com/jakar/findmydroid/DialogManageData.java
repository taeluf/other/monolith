package com.jakar.findmydroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DialogManageData extends Activity {
	
	final String tag = "MISSING_DROID";
	
	Context context;
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	
	
	Button deleteData;
	Button dontDeleteData;
	Button unhideApp;
	
	final int ENTER_PASSWORD = 0;
	final int UNHIDE_APP = 1;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.managedatadialog);
		
		context = this;
		pref = PreferenceManager.getDefaultSharedPreferences(context);
		editor = pref.edit();
		
		deleteData = (Button)findViewById(R.id.deleteData);
		deleteData.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				boolean protectData = pref.getBoolean("protectData", false);
				if (protectData){
					showDialog(ENTER_PASSWORD);
				} else {
				ComponentName cn = new ComponentName(context, ActivityTabHost.class);
    			context.getPackageManager().setComponentEnabledSetting(cn, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
    				PackageManager.DONT_KILL_APP);
				editor.clear();
				editor.commit();
				//Log.i(tag, pref.getAll().toString());
				Toast.makeText(context, "Data Cleared", Toast.LENGTH_LONG).show();
				finish();
				}
			}
		});
		dontDeleteData = (Button)findViewById(R.id.dontDeleteData);
		dontDeleteData.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		unhideApp = (Button)findViewById(R.id.unhideApp);
		unhideApp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				int state = context.getPackageManager().getComponentEnabledSetting(new ComponentName(context, ActivityTabHost.class));
				if (state==PackageManager.COMPONENT_ENABLED_STATE_DEFAULT||state==PackageManager.COMPONENT_ENABLED_STATE_ENABLED){
					new AlertDialog.Builder(context)
						.setTitle("Not Hidden!")
						.setMessage("Missing Droid is not currently set to be hidden. "
								+"If it is not appearing, you may need to reboot your device or check your launcher settings.")
						.setPositiveButton("Ok", null)
						.show();
				} else {
					showDialog(UNHIDE_APP);
				}				
			}
		});
	}
	
	@Override
	protected Dialog onCreateDialog(int dialog){
		
		switch(dialog){
		
		case (ENTER_PASSWORD):
			final EditText passText = new EditText(context);
			passText.setText("");
			
			AlertDialog entPassDialog = new AlertDialog.Builder(context)
			.setMessage("Enter your security password to clear data")
			.setView(passText)
			.setPositiveButton("Delete Data", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					String password = pref.getString("secPassword", "password");
					if (passText.getText().toString().equals(password)){
						editor.clear();
						editor.commit();
						//Log.i(tag, pref.getAll().toString());
						finish();
					} else {
						Toast.makeText(context, "Wrong Password", Toast.LENGTH_LONG).show();
					}
					finish();
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			})
			.show();
			passText.setHint("Enter Password");
			
			return entPassDialog;
			
		case (UNHIDE_APP):
			final EditText unhideText = new EditText(context);
				unhideText.setHint("Enter Password or Keyword");
				unhideText.setText("");
				
			AlertDialog unhideDialog = new AlertDialog.Builder(context)
			.setMessage("Enter your password or keyword for Missing Droid to unhide the application from your launcher")
			.setView(unhideText)
			.setPositiveButton("Unhide", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					String password = pref.getString("secPassword", "password");
					String keyword = pref.getString("keyword", "FindMyDroid");
					boolean passmatch = unhideText.getText().toString().equals(password);
					boolean keymatch = false;
					
					if (!pref.getBoolean("caseSensitive", false)){
						keymatch = unhideText.getText().toString().equalsIgnoreCase(keyword);
					} else {
						keymatch = unhideText.getText().toString().equals(keyword);
					}
					if (passmatch || keymatch){
	        			ComponentName cn = new ComponentName(context, ActivityTabHost.class);
	        			context.getPackageManager().setComponentEnabledSetting(cn, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
	        				PackageManager.DONT_KILL_APP);
	        			editor.putBoolean("hideApp", false);
	        			editor.commit();
	        			Toast.makeText(context, "Missing Droid is back in your app list. May require reboot.", Toast.LENGTH_LONG).show();
	        			startActivity(new Intent(context, ActivityTabHost.class));
					} else {
						Toast.makeText(context, "Wrong password or keyword", Toast.LENGTH_LONG).show();
					}
					unhideText.setText("");
					finish();
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					unhideText.setText("");
				}
			})
			.show();
				
				
			return unhideDialog;
		}
		
		
		return null;
	}

}
