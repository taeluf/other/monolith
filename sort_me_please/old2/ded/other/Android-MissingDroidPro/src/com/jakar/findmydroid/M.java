package com.jakar.findmydroid;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.TabHost;
import android.widget.Toast;

import com.stericson.RootTools.RootTools;

public class M {
	//
	static final String tag = "MISSING_DROID";
	static final String email = "support@jakar.co";
	
	static final boolean testing = false;
	static final boolean pro = false;
	static final boolean market = false;
	static final boolean cantToggleGps = false;
	
	
	static final String website = "http://forum.jakar.co/";
//	static final String EULA = "<a href=\""+website+"end-user-agreement.html\">"+website+"end-user-agreement.html</a>";
	static final String EULA = website+"jakar-apps/1365269098/";
	static final String HOW_TO = website+"how-to-use.html";
	static final String ROOTING = website+"android-hack/1341261291/";
	
	static public ProgressDialog pd;
	static public Handler handler;
	
	static public ArrayList<String> queued;
	
	static public TabHost tabHost;
	
	static public boolean sendingMessage;
	
	
	public static void startSmsMessageService(Context context, String message, String recipient,
		ArrayList<String> recipient_list, int attempts_allowed, final boolean dataMsg){
		Intent serviceIntent = new Intent(context, ServiceSendingSms.class);
		serviceIntent.putExtra("message", message);
		serviceIntent.putExtra("recipient", recipient);
		serviceIntent.putExtra("recipient_list", recipient_list);
		serviceIntent.putExtra("attempts_allowed", attempts_allowed);
		serviceIntent.putExtra("dataMsg", dataMsg);
		context.startService(serviceIntent);
	}
    
	public static void sendSmsMessage(Context context, String message, String recipient,
			ArrayList<String> recipient_list, int attempts_allowed, final boolean dataMsg){
		//attempts_allowed is the number of times I'll retry before giving up, decrement 1 with each attempt
		//type=0 is text sms, type=1 is data sms
		sendingMessage=true;
		final SmsManager smsMan = SmsManager.getDefault();
		queued = new ArrayList<String>();
		if (queued.size()==0){
			BroadcastReceiver br = new BroadcastReceiver(){
				@Override
				public void onReceive(Context context, Intent intent) {
					Bundle bundle = intent.getExtras();
					String recipient = bundle.getString("array_loc");
					String message = bundle.getString("message");
					int attempts_allowed = bundle.getInt("attempts", 0);
					String this_queued = bundle.getString("queued");
					String type = bundle.getString("type");
					switch (getResultCode()){
						case Activity.RESULT_OK:
							queued.remove(this_queued);
							sendingMessage=false;
							if (dataMsg){
								Toast.makeText(context, "Sending Successful", Toast.LENGTH_LONG).show();
								String pn = context.getPackageName();
								context.sendBroadcast(new Intent(pn+".closeDialog"));
//								Log.e(M.tag, "data Sending completed");
							}
//							Log.e(M.tag, "sms Sending completed, type= "+type);
							context.unregisterReceiver(this);
							break;
						default:
							attempts_allowed--;
							if (attempts_allowed==0){
								queued.remove(this_queued);
								if (queued.size()==0){
									context.unregisterReceiver(this);
									if (type=="data"){
										Toast.makeText(context, "Sending Failed", Toast.LENGTH_LONG).show();
										String pn = context.getPackageName();
										context.sendBroadcast(new Intent(pn+".closeDialog"));
//										Log.e(M.tag, "data Sending failed");
									}
//									Log.e(M.tag, "sms Sending failed");
								}
							} else {
								sendSmsMessage(context, message, recipient, null, attempts_allowed, dataMsg);
							}
							break;
					}
				}
			};
			context.registerReceiver(br, new IntentFilter("SmsSent"));
		}
		if (recipient_list!=null){
			for (int i=0;i<recipient_list.size();i++){
				queued.add("SmsSent"+i);
			}
			for (int i=0;i<recipient_list.size();i++){
				Intent intent = new Intent("SmsSent");
				recipient = recipient_list.get(i);
				intent.putExtra("recipient", recipient);
				intent.putExtra("message", message);
				intent.putExtra("queued", "SmsSent"+i);
				attempts_allowed--;
				intent.putExtra("attempts", attempts_allowed);
				PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
				smsMan.sendTextMessage(recipient, null, message, pendingIntent, null);
			}
		} else {
			queued.add("SmsSent0");
			Intent intent = new Intent("SmsSent");
			intent.putExtra("recipient", recipient);
			intent.putExtra("message", message);
			intent.putExtra("queued", "SmsSent0");
			attempts_allowed--;
			intent.putExtra("attempts", attempts_allowed);
			intent.putExtra("type", dataMsg);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);
			if (dataMsg){
				short port = 32767;
				smsMan.sendDataMessage(recipient, null, port, message.getBytes(), pendingIntent, null);
			} else {
				smsMan.sendTextMessage(recipient, null, message, pendingIntent, null);
			}
		}
		
	}
	
	public static void enableGPS(Context context){
//		Log.i(M.tag, "Attempting to enable GPS");
		if(M.canToggleGPS(context)){
			String provider = Settings.Secure.getString(context.getContentResolver(), 
			        Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

			    if(provider.contains("gps") == true) {
			        return; // the GPS is already in the requested state
			    }

			    final Intent poke = new Intent();
			    poke.setClassName("com.android.settings",
			        "com.android.settings.widget.SettingsAppWidgetProvider");
			    poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
			    poke.setData(Uri.parse("3"));
			    context.sendBroadcast(poke);
		} else {
			Intent intent = new Intent("com.jakar.changegpssetting.GPSRoot");
			context.sendBroadcast(intent);
		}
		 
	}
	public static void openWebsite(Context context, String string){
		Uri uri;
		if (string.contains(M.website)){
			uri = Uri.parse(string);
		} else {
			uri = Uri.parse(website+string);
		}
		Intent web = new Intent(Intent.ACTION_VIEW, uri);
		web.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(web);
	}
	
	public static boolean canToggleGPS(Context context) {
		
		if (cantToggleGps){
			return false;
		}
		
	    PackageManager pacman = context.getPackageManager();
	    PackageInfo pacInfo = null;

	    try {
	        pacInfo = pacman.getPackageInfo("com.android.settings", PackageManager.GET_RECEIVERS);
	    } catch (NameNotFoundException e) {
	        return false; //package not found
	    }

	    if(pacInfo != null){
	        for(ActivityInfo actInfo : pacInfo.receivers){
	            //test if recevier is exported. if so, we can toggle GPS.
	            if(actInfo.name.equals("com.android.settings.widget.SettingsAppWidgetProvider") && actInfo.exported){
	                return true;
	            }
	        }
	    }

	    return false; //default
	}
	
public static void moveToSystem(final Context context){
		
		
		pd = new ProgressDialog(context);
		pd = ProgressDialog.show(context, "Preparing", "I am making preparations so that I can toggle your GPS status", true, false);
		
		handler = new Handler();
		
		
				handler.postDelayed(new Runnable(){

					@Override
					public void run() {	
						new Thread(new Runnable(){

							@SuppressWarnings("deprecation")
							@Override
							public void run() {
						PackageManager pm = context.getPackageManager();
						
						if (!RootTools.isAccessGiven()){
							Toast.makeText(context, "Could not obtain root. Cannot continue.", Toast.LENGTH_LONG).show();
							return;
						}
						try {
							File sd = Environment.getExternalStorageDirectory();
							
							final File dir = new File(sd, "gpstogglewidget");
							dir.mkdirs();
							final File output = new File(dir, "GPS-Root-Toggle.apk");
							
							if (output.exists()){
								output.delete();
							}
							
							AssetManager am = context.getAssets();
							String fileName = "GPS-Root-Toggle.apk";

						    InputStream in = am.open(fileName);
						    OutputStream out = new FileOutputStream(output.getAbsolutePath());

						        byte[] buffer = new byte[1024];
						        int read;
						        while ((read = in.read(buffer)) != -1) {
						            out.write(buffer, 0, read);
						        }
						        in.close();
						        in = null;
						        out.flush();
						        out.close();
						        out = null;
						        //Log.i(M.tag, "File transfer done. File "+output+" exists: " + output.exists());
						        
						        RootTools.remount("/system/", "rw");
						        
						       RootTools.sendShell("cat " + output + " >> /system/app/GPS-Root-Toggle.apk", new RootTools.Result() {
									
									@Override
									public void processError(String arg0) throws Exception {
										// TODO Auto-generated method stub
										
									}
									
									@Override
									public void process(String arg0) throws Exception {
										// TODO Auto-generated method stub
										
									}
									
									@Override
									public void onFailure(Exception arg0) {
										handler.post(new Runnable(){

											@Override
											public void run() {
												output.delete();
												dir.delete();
												pd.dismiss();
												Toast.makeText(context, "Copy Failed", Toast.LENGTH_LONG).show();
											}
											
										});
										
									}
									
									@Override
									public void onComplete(int arg0) {
										handler.post(new Runnable(){

											@Override
											public void run() {
												try {
													RootTools.sendShell("chmod 0777 /system/app/GPS-Root-Toggle.apk", 30);
												} catch (Exception e){
													
												}
												
												pd.dismiss();
												if (RootTools.exists("/system/app/GPS-Root-Toggle.apk")){
													Toast.makeText(context, "Preparation Completed. You may need to reboot your phone."
															, Toast.LENGTH_LONG).show();
													try {
														ActivityTabLocate.autoGPS.setChecked(true);
													} catch (Exception e){
														e.printStackTrace();
													}
													output.delete();
													dir.delete();
												} else {
													Toast.makeText(context, "Failure - error unknown", Toast.LENGTH_LONG).show();
												}
												
											}
											
										});
									}
								}, 60);
							//Log.i(tag2, "Done - line 134");
						} catch (Exception e){
							e.printStackTrace();
						}
							}
							
						}).start();
					}
					
				}, 1000);
		
		
	}

	public static String getCallForwardCode(String type, Context context){
		TelephonyManager telMan = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		String returnValue = "";
		String startCode;
		String endCode;
		String disableCode;
		String operator = telMan.getNetworkOperator();
		//Log.e(M.tag, telMan.getPhoneType()+"    "+operator);//
		
		if (operator.equals("310410") || operator.equals("310026")){ // ATT or T-MOBILE respectively
			startCode = "**21*";
			endCode = "#";
			disableCode="##21#";
		} else if (operator.equals("310120") || operator.equals("311480") || operator.equals("310005")){ // VERIZON 
			startCode = "*72";
			endCode ="";
			disableCode = "*73";
		} else if (operator.equals("310012")) {// SPRINT
			startCode = "*72";
			endCode ="";
			disableCode="*720";
			
		} //else if (operator.equals(OTHER_CARRIER_HERE)) {
		//}
		 else {
			 int phoneType = telMan.getPhoneType();
			if (phoneType==TelephonyManager.PHONE_TYPE_GSM){
				startCode = "*21*";
				endCode = "#";
				disableCode = "##21#";
			} else {
				startCode="*72";
				endCode="";
				disableCode="*720";
			}
			
		 }		
		
		if (type=="startCode"){
			returnValue = startCode;
		} else if (type=="endCode"){
			returnValue = endCode;
		} else if (type=="disableCode"){
			returnValue = disableCode;
		}
		
		return returnValue;
	}
	public static void setCallForwarding(Context context, String cfNum){
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		
		String forwardNumber = cfNum;
		String callNum;
		boolean turnOn;
		//Log.i(tag, forwardNumber);
				
		forwardNumber = getOnlyNumerics(forwardNumber);
				
		if (forwardNumber.length() < 4){
			forwardNumber = "";
			turnOn = false;
		} else {
			turnOn = true;
		}
		String startCode = pref.getString("startCode", M.getCallForwardCode("startCode", context));
		String endCode = pref.getString("endCode", M.getCallForwardCode("endCode", context));
		String disableCode = pref.getString("disableCode", M.getCallForwardCode("disableCode", context));
		
		if (turnOn){
			callNum = startCode + forwardNumber + endCode;
		} else {
			callNum = disableCode;
		}
		//Log.i("MISSING_DROID", "MMI code / call forward number: " + callNum);
		Intent fI = new Intent(Intent.ACTION_CALL);
		fI.setData(Uri.parse(String.format("tel:%s", Uri.encode(callNum))));
		fI.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		try{context.startActivity(fI);}
		catch (Exception e){
			//Log.e(tag, e.getMessage());
			}
	}
	public static String getOnlyNumerics(String str) {
	    
	    if (str == null) {
	        return "";
	    }
	
	    StringBuffer strBuff = new StringBuffer();
	    char c;
	    
	    for (int i = 0; i < str.length() ; i++) {
	        c = str.charAt(i);
	        
	        if (Character.isDigit(c)) {
	            strBuff.append(c);
	        }
	    }
	    return strBuff.toString();
	}
	public String getLengthTen(String str){
		if (str == null) {
	        return null;
	    }
	
	    StringBuilder strBuff2 = new StringBuilder();
	    strBuff2.append(str);
	    char c;
	    
	    for (int i = 0; i < str.length() ; i++) {
	    	c = str.charAt(i);
	        
	        if (strBuff2.length() > 10) {
	           strBuff2.deleteCharAt(0);
	        }
	    }
	    return strBuff2.toString(); 
	}

}
