package com.jakar.findmydroid;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivityChooseFriends extends Activity {
	
	
	final String tag = "MISSING_DROID";
	
	Context context;
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	
	//Dialog Values
	final int ADD_FRIEND = 0;
	final int GET_PRO = 1;
	
	ImageButton addFriendButton;
	
	int numOfFriends;
	
	StringBuilder sb;
	
	LayoutInflater inflater;
	View enterfriend;
	EditText enterFriendNum;
//	ImageButton chooseContact;
	
	AlertDialog addFriendDia;
	
	LinearLayout linLayoutParent;
	
	ArrayList<RelativeLayout> relativeLayoutList;
	ArrayList<TextView> textViewList;
	ArrayList<ImageButton> removeFriendList;
	
	private String unlockContent;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choosefriends);
		
		context = this;
				
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		editor = pref.edit();
		
		addFriendButton = (ImageButton)findViewById(R.id.addFriendButton);
		
		sb = new StringBuilder();
		
		linLayoutParent = (LinearLayout)findViewById(R.id.linLayoutParent);
		LinearLayout linearLayout1 = (LinearLayout)findViewById(R.id.linearLayout1);
		
		relativeLayoutList = new ArrayList<RelativeLayout>();
		textViewList = new ArrayList<TextView>();
		removeFriendList = new ArrayList<ImageButton>();
		
		
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	enterfriend = inflater.inflate(R.layout.enterfriend, null);
    	enterFriendNum = (EditText)enterfriend.findViewById(R.id.enterFriendNum);
//    	chooseContact = (ImageButton)enterfriend.findViewById(R.id.chooseContact);
    	
		
		addFriendButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (pref.getInt("numOfFriends", 0) > 2){
					boolean hasPro = Market.isPro(context);
					boolean unlockFriendList = pref.getBoolean("unlockFriendList", false);
					if (hasPro||unlockFriendList){
						showDialog(ADD_FRIEND);
					} else {
						unlockContent="unlockFriendList";
						showDialog(GET_PRO);
						}
				} else {
					showDialog(ADD_FRIEND);
				}
			}
		});
		linearLayout1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (pref.getInt("numOfFriends", 0) > 2){
					boolean hasPro = Market.isPro(context);
					if (hasPro){
						showDialog(ADD_FRIEND);
					} else {
						showDialog(GET_PRO);
					}
				} else {
					showDialog(ADD_FRIEND);
				}
			}
		});
		
		numOfFriends = pref.getInt("numOfFriends", 0);
		if (numOfFriends > 0){
			for (int i = 0; i < numOfFriends; i++){
				sb.setLength(0);
				sb.append("friend"+i);
				String key = sb.toString();
				String friendNum = pref.getString(key, "");
				addNewFriend(i,friendNum);
				//add to viewgroup
				//Log.i(tag, "Friend's phone number " + friendNum + " with the key " + key + " was just printed to screen");
				
			}	
			
		}
		
	}
	
	public void addNewFriend(final int id, final String number){
//		new Thread(new Runnable(){
//				@Override
//				public void run(){
					//Log.i(tag, "ID trying to be added: " + id);
					relativeLayoutList.ensureCapacity(id + 1);
					relativeLayoutList.add(new RelativeLayout(context));
					relativeLayoutList.get(id).setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
					relativeLayoutList.get(id).setId(5000+id);
					
					
					removeFriendList.ensureCapacity(id+1);
					removeFriendList.add(new ImageButton(context));
					removeFriendList.get(id).setImageResource(R.xml.removefriendbuttonstates);
					removeFriendList.get(id).setBackgroundResource(R.drawable.transparent);
					removeFriendList.get(id).setId(8000+id);
					RelativeLayout.LayoutParams removeFriendBtnParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
							LayoutParams.WRAP_CONTENT);
					removeFriendBtnParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					removeFriendList.get(id).setLayoutParams(removeFriendBtnParams);
					
					
					textViewList.ensureCapacity(id + 1);
					textViewList.add(new TextView(context));
					textViewList.get(id).setText(number);
					textViewList.get(id).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22);
					textViewList.get(id).setId(6000+id);
					RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, 
							LayoutParams.WRAP_CONTENT);
					textViewParams.addRule(RelativeLayout.LEFT_OF, 8000+id);//layout_toLeftOf 7000+id
					textViewParams.addRule(RelativeLayout.CENTER_VERTICAL);
					textViewList.get(id).setLayoutParams(textViewParams);
					
					View line = inflater.inflate(R.layout.line, null);
					RelativeLayout.LayoutParams lineParams = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					lineParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					line.setLayoutParams(lineParams);
					
					relativeLayoutList.get(id).addView(removeFriendList.get(id));
					relativeLayoutList.get(id).addView(textViewList.get(id));
					relativeLayoutList.get(id).addView(line);
							
															
//				}
//			}).start();
				linLayoutParent.addView(relativeLayoutList.get(id));
				
				removeFriendList.get(id).setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						
//						Log.i(tag, "RelativeLayoutList.get(0) " + relativeLayoutList.get(0).getId());
//						Log.i(tag, "RelativeLayoutList.size()" + relativeLayoutList.size());
						int myID = arg0.getId() - 8000;
						//Log.i(tag, "Removed ID is: "+ myID);
						linLayoutParent.removeAllViews();
						relativeLayoutList.clear();
						removeFriendList.clear();
						textViewList.clear();
//						Log.i(tag, "RelativeLayoutList.get(0) " + relativeLayoutList.get(0).getId());
//						Log.i(tag, "RelativeLayoutList.size()" + relativeLayoutList.size());
						String friendsNumber = pref.getString("friend"+myID, "");
						editor.remove("allowed"+friendsNumber);
						editor.remove("friend"+myID);
						editor.commit();
						//Log.i(tag, "Friend's number is: " + friendsNumber);
						
						for (int x = 0; x <=pref.getInt("numOfFriends", 0); x++){
							if (x<myID){
								
							} else if (x<pref.getInt("numOfFriends", 0)){
								int thisKeyInt = x;
								int nextKeyInt = x+1;
								String thisKeyStr = "friend"+thisKeyInt;
								String nextKeyStr = "friend"+nextKeyInt;
								String nextValue = pref.getString(nextKeyStr, "");
								editor.putString(thisKeyStr, nextValue);
								editor.commit();							
								
							} else if (x>=pref.getInt("numOfFriends", 0)){
								int thisKeyInt2 = x;
								int removeThisKey = x-1;
								String thisKeyStr2 = "friend"+thisKeyInt2;
								editor.remove(thisKeyStr2);
								editor.putInt("numOfFriends", removeThisKey);
								editor.commit();
							}
							
						}
						
						numOfFriends = pref.getInt("numOfFriends", 0);
						if (numOfFriends > 0){
							for (int i = 0; i < numOfFriends; i++){
								sb.setLength(0);
								sb.append("friend"+i);
								String key = sb.toString();
								String friendNum = pref.getString(key, "");
								addNewFriend(i,friendNum);
								//add to viewgroup
								//Log.i(tag, "Friend's phone number " + friendNum + " with the key " + key + " was just printed to screen");
								
							}	
							
						}
					}
				});
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
		
	}
	@Override
    protected Dialog onCreateDialog(int id) {
    	
        switch (id) {    	
        
        case ADD_FRIEND:
        	
//        	chooseContact.setOnClickListener(new View.OnClickListener() {
//				
//				@Override
//				public void onClick(View arg0) {
//					Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);  
//					startActivityForResult(intent, 1);
//				}
//			});
        	
        	enterFriendNum.setInputType(android.text.InputType.TYPE_CLASS_NUMBER);
        	
        	addFriendDia = new AlertDialog.Builder(this)
        	.setMessage("Enter your friend's phone number")
        	.setView(enterfriend)
        	.setPositiveButton("Save", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					if (enterFriendNum.length()>0){
						int currentNumOfFriends = pref.getInt("numOfFriends", 0);
						String friendsNum = enterFriendNum.getText().toString();
						
						for (int i = 0; i <currentNumOfFriends; i++){
							String checkNum = pref.getString("friend"+i, "");
							if (checkNum.equals(friendsNum)){
								Toast.makeText(context, "Number already on list", Toast.LENGTH_LONG).show();
								enterFriendNum.setText("");
								return;
							}
						}

					sb.setLength(0);
					sb.append("friend"+currentNumOfFriends);
					String key = sb.toString();
					
					editor.putInt("numOfFriends", currentNumOfFriends+1);
					editor.putString(key, friendsNum);
					editor.putBoolean("allowed"+friendsNum, true);
					editor.commit();
					
					addNewFriend(currentNumOfFriends, friendsNum);
					
					//Log.i(tag, "Phone number " + friendsNum + " was saved to SharedPrefs with key " + key + ", and there are" +
//							" now " + (currentNumOfFriends+1) +" friends saved");
					enterFriendNum.setText("");
					} else {
						Toast.makeText(context, "Enter Number", Toast.LENGTH_SHORT).show();
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					enterFriendNum.setText("");					
				}
			})
        	.show();
        	
        	return addFriendDia;
        case(GET_PRO):
			AlertDialog getProDia = new AlertDialog.Builder(context)
			.setMessage(Market.upgrade)
			.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					Market.donate(context);
				}
			})
			.setNegativeButton("Not Now", null)
			.show();
			return getProDia;
        }
        
        return null;
	}
	
	@Override  
	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
	    super.onActivityResult(reqCode, resultCode, data);  
	    if (resultCode == Activity.RESULT_OK) {  
	        Uri contactData = data.getData();  
	        Cursor c =  managedQuery(contactData, null, null, null, null);   
	        c.moveToFirst();
	        String number = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				int currentNumOfFriends = pref.getInt("numOfFriends", 0);
				
				sb.setLength(0);
				sb.append("friend"+currentNumOfFriends);
				String key = sb.toString();
				
				editor.putInt("numOfFriends", currentNumOfFriends+1);
				editor.putString(key, number);
				editor.commit();
	        addNewFriend(currentNumOfFriends, number);
	        try {addFriendDia.cancel();}catch(Exception e){e.printStackTrace();}
	        
	    }  
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if( keyCode == KeyEvent.KEYCODE_BACK) {
	    	M.tabHost.setCurrentTab(2);
	        return true;
	    }else{
	        return super.onKeyDown(keyCode, event);
	    }
	}
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
	    if( keyCode == KeyEvent.KEYCODE_BACK) {
	    	M.tabHost.setCurrentTab(2);
	        return true;
	    }else{
	        return super.onKeyDown(keyCode, event);
	    }
	}

}
