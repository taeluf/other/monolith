package com.jakar.findmydroid;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.provider.Settings;

import com.stericson.RootTools.RootTools;

public class ActivityTabLocate extends PreferenceActivity {
	
	Context context;
	
	final int GET_PRO = 0;
	final int AUTO_GPS = 1;
	final int NO_ROOT = 2;
	
	static public CheckBoxPreference autoGPS;
	
	SharedPreferences pref;
	
	String unlockContent;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.activitytablocate);
		context = this;
		
		pref = PreferenceManager.getDefaultSharedPreferences(context);
		
		autoGPS = (CheckBoxPreference)findPreference("autoGPS");
		autoGPS.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object newValue) {
				if (Boolean.valueOf(String.valueOf(newValue))){
					
					if (!Market.upgradeDialog(context, "You must upgrade to use this feature\n\nImportant! Requires Android 2.2"
							+" or below or you must have a rooted phone.")){
						return false;
					} else {
			    		boolean canToggle = M.canToggleGPS(context);
			    		if (canToggle){
			    			return true;
			    		} else {
			    			showDialog(AUTO_GPS);
			    		}
		    		return false;
			    	}
				} else return true;
			}
		});
		final CheckBoxPreference autoWifi = (CheckBoxPreference)findPreference("autoWifi");
		autoWifi.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				return Market.upgradeDialog(context, null);
			}
		});
		final RingtonePreference selRing = (RingtonePreference)findPreference("selRing");
		Uri alarm = Settings.System.DEFAULT_RINGTONE_URI;
		selRing.setDefaultValue(alarm);
		
		CheckBoxPreference enableTracking = (CheckBoxPreference)findPreference("enableTracking");
		enableTracking.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				new AlertDialog.Builder(context).setTitle("Notice!")
					.setMessage("Sorry, this feature is currently under development and is planned for a future update.")
					.setPositiveButton("Ok", null)
					.show();
				return false;
			}
		});
	}
	
	@Override
	protected Dialog onCreateDialog(int id){
		
		switch(id){
			case(GET_PRO):
				AlertDialog getProDia = new AlertDialog.Builder(context)
				.setMessage(Market.upgrade)
				.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Market.donate(context);
					}
				})
				.setNegativeButton("Not Now", null)
				.show();
			return getProDia;
			case(AUTO_GPS):
				AlertDialog autoGps = new AlertDialog.Builder(context)
			    .setMessage("Unfortunately, I'm not able to automatically toggle the GPS on your phone due to Android limitations. If you'd like, " +
			    		"you could use the Root method. This is a beta feature.\nTo learn about rooting go to "+M.ROOTING+" on a computer.")
			    .setPositiveButton("Use Root", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (RootTools.isAccessGiven()){
							M.moveToSystem(context);
						} else {
							showDialog(NO_ROOT);
						}
						
					}
				})
				.setNeutralButton("Website", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						M.openWebsite(context, "android-hack/1341261291/");
					}
				})
			    .setNegativeButton("Cancel", null)
				.show();
				
			return autoGps;
			case (NO_ROOT):
				AlertDialog noRoot = new AlertDialog.Builder(context)
				.setMessage("I was either unable to obtain root access or your phone is not rooted.\n\nIf you would like to " +
						"learn about rooting, visit my website at "+M.ROOTING+" on a computer.")
				.setPositiveButton("OK", null)
			    .show();
			return noRoot;
		}
		
		return null;
	}
	
	
}
