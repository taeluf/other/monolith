package com.jakar.findmydroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.widget.EditText;
import android.widget.Toast;

public class ActivityTabLocator extends PreferenceActivity {
	
		Context context;
		ProgressDialog friendDialog;
		String callForwardNumber;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.activitytablocator);
		
		context = this;
		friendDialog = new ProgressDialog(context);
		
		Preference aboutLocator = (Preference)findPreference("aboutLocator");
		aboutLocator.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.openWebsite(context, "jakar-apps/1365556833/");
				return false;
			}
		});
		final EditTextPreference locatorNumber = (EditTextPreference)findPreference("locatorNumber");
		final EditTextPreference lrKeyword = (EditTextPreference)findPreference("locatorRequestKeyword");
		final Preference lrLocate = (Preference)findPreference("locatorRequestLocate");
		final Preference lrRing = (Preference)findPreference("locatorRequestRing");
		final Preference lrCallForward = (Preference)findPreference("locatorRequestCallForward");
		final Preference lrLock = (Preference)findPreference("locatorRequestLock");
		final Preference lrWipe = (Preference)findPreference("locatorRequestWipe");
		String number = locatorNumber.getText();
		try {
			if (number!=null&&number.length()>0){
				locatorNumber.setTitle("Change Number - "+number);
			} else {
				locatorNumber.setTitle("Enter Number");
			}
		}catch (Exception e){}
		String keyword = lrKeyword.getText();
		try {
			if (keyword!=null&&keyword.length()>0){
				lrKeyword.setTitle("Change Keyword - "+keyword);
			} else {
				lrKeyword.setTitle("Enter Keyword");
			}
		}catch (Exception e){}
		locatorNumber.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				String number = String.valueOf(newValue);
				try {
					if (number!=null&&number.length()>0){
						locatorNumber.setTitle("Change Number - "+number);
					} else {
						locatorNumber.setTitle("Enter Number");
					}
				}catch (Exception e){}
				return true;
			}
		});
		lrKeyword.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				String keyword = String.valueOf(newValue);
				try {
					if (keyword!=null&&keyword.length()>0){
						lrKeyword.setTitle("Change Keyword - "+keyword);
					} else {
						lrKeyword.setTitle("Enter Keyword");
					}
				}catch (Exception e){}
				return true;
			}
		});
		Preference.OnPreferenceClickListener listener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				String recipient = locatorNumber.getText();
				boolean enterNumber = false;
				try {
					if (recipient.length()<1||recipient==null){
						enterNumber = true;
					}
				}catch (Exception e){ enterNumber = true;}
				if (enterNumber){
					new AlertDialog.Builder(context)
						.setTitle("Enter Number!")
						.setMessage("You must enter a phone number in order to use Locator")
						.setPositiveButton("Ok", null)
						.show();
					return false;
				}
				String pn = context.getPackageName();
				friendDialog = ProgressDialog.show(ActivityTabLocator.this, "", 
						"Request Sending...", true, true);
				BroadcastReceiver myReceiver = new BroadcastReceiver(){
					@Override
					public void onReceive(Context context, Intent intent){
						if (friendDialog.isShowing()){try {friendDialog.cancel();}catch (Exception e){}}
					}
				};
				registerReceiver(myReceiver, new IntentFilter(pn+".closeDialog"));
				String message = lrKeyword.getText();
				try {
					if (message.length()<1||message==null){
						message = "";
					} else {
						message += " ";
					}
				}catch (Exception e){ message = "";}
				String command = ""; 
				
				if (preference==lrLocate){
					command="gps";
				} else if (preference==lrRing){
					command="ring";
				} else if (preference==lrLock){
					command="lock";
				}
				M.startSmsMessageService(context, message+command, recipient, null, 3, true);
				
				return false;
			}
		};
		
		lrLocate.setOnPreferenceClickListener(listener);
		lrRing.setOnPreferenceClickListener(listener);
		lrLock.setOnPreferenceClickListener(listener);
		
		lrCallForward.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				
				String recipient = locatorNumber.getText();
				boolean enterNumber = false;
				try {
					if (recipient.length()<1||recipient==null){
						enterNumber = true;
					}
				}catch (Exception e){ enterNumber = true;}
				if (enterNumber){
					new AlertDialog.Builder(context)
						.setTitle("Enter Number!")
						.setMessage("You must enter a phone number in order to use Locator")
						.setPositiveButton("Ok", null)
						.show();
					return false;
				}
				
				
				final EditText cfNum = new EditText(context);
				new AlertDialog.Builder(context)
					.setTitle("Call Forwarding")
					.setMessage("To enable recipient's call forwarding, "
								+"enter a phone number")
					.setView(cfNum)
					.setPositiveButton("Cancel", null)
					.setNegativeButton("Enable", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							String cfNumber = cfNum.getText().toString();
							boolean enterNumber = false;
							try {
								if (cfNumber.length()<1||cfNumber==null){
									enterNumber = true;
								}
							}catch (Exception e){ enterNumber = true;}
							if (enterNumber){
								new AlertDialog.Builder(context)
									.setTitle("Enter Number!")
									.setMessage("You must enter a phone number to forward calls to")
									.setPositiveButton("Ok", null)
									.show();
								return;
							}
							String message = lrKeyword.getText();
							try {
								if (message.length()<1||message==null){
									message = "";
								} else {
									message += " ";
								}
							}catch (Exception e){ message = "";}
							String command = "forward "+cfNumber;
							friendDialog = ProgressDialog.show(ActivityTabLocator.this, "", 
									"Request Sending...", true, true);
							BroadcastReceiver myReceiver = new BroadcastReceiver(){
								@Override
								public void onReceive(Context context, Intent intent){
									if (friendDialog.isShowing()){try {friendDialog.cancel();}catch (Exception e){}}
								}
							};
							String pn = context.getPackageName();
							registerReceiver(myReceiver, new IntentFilter(pn+".closeDialog"));	
							M.startSmsMessageService(context, message+command, locatorNumber.getText(), null, 3, true);
						}
					})
					.setNeutralButton("Disable", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							String message = lrKeyword.getText();
							try {
								if (message.length()<1||message==null){
									message = "";
								} else {
									message += " ";
								}
							}catch (Exception e){ message = "";}
							String command = "forward";
							friendDialog = ProgressDialog.show(ActivityTabLocator.this, "", 
									"Request Sending...", true, true);
							BroadcastReceiver myReceiver = new BroadcastReceiver(){
								@Override
								public void onReceive(Context context, Intent intent){
									if (friendDialog.isShowing()){try {friendDialog.cancel();}catch (Exception e){}}
								}
							};
							String pn = context.getPackageName();
							registerReceiver(myReceiver, new IntentFilter(pn+".closeDialog"));
							M.startSmsMessageService(context, message+command, locatorNumber.getText(), null, 3, true);
						}
					})
					.show();
				return false;
			}
		});
		lrWipe.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				String recipient = locatorNumber.getText();
				boolean enterNumber = false;
				try {
					if (recipient.length()<1||recipient==null){
						enterNumber = true;
					}
				}catch (Exception e){ enterNumber = true;}
				if (!enterNumber){
					new AlertDialog.Builder(context)
					.setTitle("Confirm Wipe!")
					.setMessage("Please confirm you would like to wipe recipient's phone: "+recipient)
					.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							String message = lrKeyword.getText();
							try {
								if (message.length()<1||message==null){
									message = "";
								} else {
									message += " ";
								}
							}catch (Exception e){ message = "";}
							String command = "wipe";
							friendDialog = ProgressDialog.show(ActivityTabLocator.this, "", 
									"Request Sending...", true, true);
							BroadcastReceiver myReceiver = new BroadcastReceiver(){
								@Override
								public void onReceive(Context context, Intent intent){
									if (friendDialog.isShowing()){try {friendDialog.cancel();}catch (Exception e){}}
								}
							};
							String pn = context.getPackageName();
							registerReceiver(myReceiver, new IntentFilter(pn+".closeDialog"));
							M.startSmsMessageService(context, message+command, locatorNumber.getText(), null, 3, true);
						}
					})
					.setNegativeButton("Cancel", null)
					.show();
					return false;
				} else {
					Toast.makeText(context, "Please enter a phone number to locate.", Toast.LENGTH_LONG).show();
				}
				
				return false;
			}
		});
		
	}

}
