package com.jakar.findmydroid;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ActivitySimReport extends Activity {
	
	
	final String tag = "MISSING_DROID";
	
	Context context;
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	
	//Dialog Values
	final int ADD_simReport = 0;
	final int GET_PRO = 1;
	
	ImageButton addsimReportButton;
	
	int numOfsimReport;
	
	StringBuilder sb;
	
	LayoutInflater inflater;
	View entersimReport;
	EditText entersimReportNum;
	ImageButton chooseContact;
	
	AlertDialog addsimReportDia;
	
	LinearLayout linLayoutParent;
	
	ArrayList<RelativeLayout> relativeLayoutList;
	ArrayList<TextView> textViewList;
	ArrayList<ImageButton> removesimReportList;
	
	private String unlockContent;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choosefriends);
		
		context = this;
				
		pref = PreferenceManager.getDefaultSharedPreferences(this);
		editor = pref.edit();
		
		addsimReportButton = (ImageButton)findViewById(R.id.addFriendButton);
		
		sb = new StringBuilder();
		
		linLayoutParent = (LinearLayout)findViewById(R.id.linLayoutParent);
		LinearLayout linearLayout1 = (LinearLayout)findViewById(R.id.linearLayout1);
		
		relativeLayoutList = new ArrayList<RelativeLayout>();
		textViewList = new ArrayList<TextView>();
		removesimReportList = new ArrayList<ImageButton>();
		
		
		inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	entersimReport = inflater.inflate(R.layout.enterfriend, null);
    	entersimReportNum = (EditText)entersimReport.findViewById(R.id.enterFriendNum);
    	//chooseContact = (ImageButton)entersimReport.findViewById(R.id.chooseContact);
    	
		
		addsimReportButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (pref.getInt("numOfsimReport", 0) > 2){
					boolean hasPro = Market.isPro(context);
					boolean unlockSimList = pref.getBoolean("unlockSimList", false);
					if (hasPro||unlockSimList){
						showDialog(ADD_simReport);
					} else {
						unlockContent="unlockSimList";
						showDialog(GET_PRO);
						}
				} else {
					showDialog(ADD_simReport);
				}
			}
		});
		linearLayout1.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (pref.getInt("numOfsimReport", 0) > 2){
					boolean hasPro = Market.isPro(context);
					if (hasPro){
						showDialog(ADD_simReport);
					} else {
						showDialog(GET_PRO);
					}
				} else {
					showDialog(ADD_simReport);
				}
			}
		});
		
		numOfsimReport = pref.getInt("numOfsimReport", 0);
		if (numOfsimReport > 0){
			for (int i = 0; i < numOfsimReport; i++){
				sb.setLength(0);
				sb.append("simReport"+i);
				String key = sb.toString();
				String simReportNum = pref.getString(key, "");
				addNewsimReport(i,simReportNum);
				//add to viewgroup
				//Log.i(tag, "simReport's phone number " + simReportNum + " with the key " + key + " was just printed to screen");
				
			}	
			
		}
		
	}
	
	public void addNewsimReport(final int id, final String number){
//		new Thread(new Runnable(){
//				@Override
//				public void run(){
					//Log.i(tag, "ID trying to be added: " + id);
					relativeLayoutList.ensureCapacity(id + 1);
					relativeLayoutList.add(new RelativeLayout(context));
					relativeLayoutList.get(id).setLayoutParams(new ViewGroup.LayoutParams(-1, -2));
					relativeLayoutList.get(id).setId(5000+id);
					
					
					removesimReportList.ensureCapacity(id+1);
					removesimReportList.add(new ImageButton(context));
					removesimReportList.get(id).setImageResource(R.xml.removefriendbuttonstates);
					removesimReportList.get(id).setBackgroundResource(R.drawable.transparent);
					removesimReportList.get(id).setId(8000+id);
					RelativeLayout.LayoutParams removesimReportBtnParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
							LayoutParams.WRAP_CONTENT);
					removesimReportBtnParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
					removesimReportList.get(id).setLayoutParams(removesimReportBtnParams);
					
					
					textViewList.ensureCapacity(id + 1);
					textViewList.add(new TextView(context));
					textViewList.get(id).setText(number);
					textViewList.get(id).setTextSize(TypedValue.COMPLEX_UNIT_DIP, 22);
					textViewList.get(id).setId(6000+id);
					RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, 
							LayoutParams.WRAP_CONTENT);
					textViewParams.addRule(RelativeLayout.LEFT_OF, 8000+id);//layout_toLeftOf 7000+id
					textViewParams.addRule(RelativeLayout.CENTER_VERTICAL);
					textViewList.get(id).setLayoutParams(textViewParams);
					
					View line = inflater.inflate(R.layout.line, null);
					RelativeLayout.LayoutParams lineParams = new RelativeLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
					lineParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
					line.setLayoutParams(lineParams);
					
					relativeLayoutList.get(id).addView(removesimReportList.get(id));
					relativeLayoutList.get(id).addView(textViewList.get(id));
					relativeLayoutList.get(id).addView(line);
							
															
//				}
//			}).start();
				linLayoutParent.addView(relativeLayoutList.get(id));
				
				removesimReportList.get(id).setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View arg0) {
						
//						Log.i(tag, "RelativeLayoutList.get(0) " + relativeLayoutList.get(0).getId());
//						Log.i(tag, "RelativeLayoutList.size()" + relativeLayoutList.size());
						int myID = arg0.getId() - 8000;
						//Log.i(tag, "Removed ID is: "+ myID);
						linLayoutParent.removeAllViews();
						relativeLayoutList.clear();
						removesimReportList.clear();
						textViewList.clear();
//						Log.i(tag, "RelativeLayoutList.get(0) " + relativeLayoutList.get(0).getId());
//						Log.i(tag, "RelativeLayoutList.size()" + relativeLayoutList.size());
						String simReportNumber = pref.getString("simReport"+myID, "");
						editor.remove("allowed"+simReportNumber);
						editor.remove("simReport"+myID);
						editor.commit();
						//Log.i(tag, "simReport's number is: " + simReportNumber);
						
						for (int x = 0; x <=pref.getInt("numOfsimReport", 0); x++){
							if (x<myID){
								
							} else if (x<pref.getInt("numOfsimReport", 0)){
								int thisKeyInt = x;
								int nextKeyInt = x+1;
								String thisKeyStr = "simReport"+thisKeyInt;
								String nextKeyStr = "simReport"+nextKeyInt;
								String nextValue = pref.getString(nextKeyStr, "");
								editor.putString(thisKeyStr, nextValue);
								editor.commit();							
								
							} else if (x>=pref.getInt("numOfsimReport", 0)){
								int thisKeyInt2 = x;
								int removeThisKey = x-1;
								String thisKeyStr2 = "simReport"+thisKeyInt2;
								editor.remove(thisKeyStr2);
								editor.putInt("numOfsimReport", removeThisKey);
								editor.commit();
							}
							
						}
						
						numOfsimReport = pref.getInt("numOfsimReport", 0);
						if (numOfsimReport > 0){
							for (int i = 0; i < numOfsimReport; i++){
								sb.setLength(0);
								sb.append("simReport"+i);
								String key = sb.toString();
								String simReportNum = pref.getString(key, "");
								addNewsimReport(i,simReportNum);
								//add to viewgroup
								//Log.i(tag, "simReport's phone number " + simReportNum + " with the key " + key + " was just printed to screen");
								
							}	
							
						}
					}
				});
	}
	
	@Override
	public void onStop(){
		super.onStop();
	}
	@Override
	public void onDestroy(){
		super.onDestroy();
	}
	
	@Override
    protected Dialog onCreateDialog(int id) {
    	
        switch (id) {    	
        
        case ADD_simReport:
        	
        	chooseContact.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);  
					startActivityForResult(intent, 1);
				}
			});
        	
        	entersimReportNum.setInputType(android.text.InputType.TYPE_CLASS_NUMBER);
        	
        	addsimReportDia = new AlertDialog.Builder(this)
        	.setMessage("Enter your simReport's phone number")
        	.setView(entersimReport)
        	.setPositiveButton("Save", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					if (entersimReportNum.length()>0){
						int currentNumOfsimReport = pref.getInt("numOfsimReport", 0);
						String simReportNum = entersimReportNum.getText().toString();
						
						for (int i = 0; i <currentNumOfsimReport; i++){
							String checkNum = pref.getString("simReport"+i, "");
							if (checkNum.equals(simReportNum)){
								Toast.makeText(context, "Number already on list", Toast.LENGTH_LONG).show();
								entersimReportNum.setText("");
								return;
							}
						}

					sb.setLength(0);
					sb.append("simReport"+currentNumOfsimReport);
					String key = sb.toString();
					
					editor.putInt("numOfsimReport", currentNumOfsimReport+1);
					editor.putString(key, simReportNum);
					editor.putBoolean("allowed"+simReportNum, true);
					editor.commit();
					
					addNewsimReport(currentNumOfsimReport, simReportNum);
					
					//Log.i(tag, "Phone number " + simReportNum + " was saved to SharedPrefs with key " + key + ", and there are" +
//							" now " + (currentNumOfsimReport+1) +" simReport saved");
					entersimReportNum.setText("");
					} else {
						Toast.makeText(context, "Enter Number", Toast.LENGTH_SHORT).show();
					}
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					entersimReportNum.setText("");					
				}
			})
        	.show();
        	
        	return addsimReportDia;
        case(GET_PRO):
			AlertDialog getProDia = new AlertDialog.Builder(context)
			.setMessage(Market.upgrade)
			.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					Market.donate(context);
				}
			})
			.setNegativeButton("Not Now", null)
			.show();
			return getProDia;
        }
        
        return null;
	}
	
	@Override  
	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
	    super.onActivityResult(reqCode, resultCode, data);  
	    if (resultCode == Activity.RESULT_OK) {  
	        Uri contactData = data.getData();  
	        Cursor c =  managedQuery(contactData, null, null, null, null);   
	        c.moveToFirst();
	        String number = c.getString(c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
				int currentNumOfsimReport = pref.getInt("numOfsimReport", 0);
				
				sb.setLength(0);
				sb.append("simReport"+currentNumOfsimReport);
				String key = sb.toString();
				
				editor.putInt("numOfsimReport", currentNumOfsimReport+1);
				editor.putString(key, number);
				editor.commit();
	        addNewsimReport(currentNumOfsimReport, number);
	        try {addsimReportDia.cancel();}catch(Exception e){e.printStackTrace();}
	        
	    }  
	}

}
