package com.jakar.findmydroid;

import android.app.AlarmManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.preference.PreferenceManager;

public class ServiceRing extends Service{
	
	AudioManager audMan;
	RingtoneManager ringMan;
	AlarmManager alarmMan;
	Ringtone ringtone;
	MediaPlayer medPlay;
	Runnable stopRing;
	Runnable stopRinging;
	Handler stopHandler;
	final long receiveTime = System.currentTimeMillis();
	long stopTime = receiveTime;
	 
	
	Uri selRingURI;
	boolean maxRing;
	boolean ring30Sec;
	boolean ring2Min;
	boolean ring5Min;
	String ringTime;
	boolean interruptableRing;
	
	Uri defaultRing = android.provider.Settings.System.DEFAULT_RINGTONE_URI;
	Thread stopRingThread;
	String selRing;
	
	Intent stopRingIntent;
	
	PowerManager pm;
	PowerManager.WakeLock wl;
		
	String TAG = "MISSING_DROID";
	
	Context context;
	
	public class LocalBinder extends Binder {
		 ServiceRing getService() {
	            return ServiceRing.this;
	        }
	    }
	
	private final IBinder mBinder = new LocalBinder();
	
	@Override
  public IBinder onBind(Intent intent) {
		return mBinder;	
	}
	
	@Override
	public void onCreate(){
		context = this;
		pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
		wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MD_WAKE_LOCK");
		wl.acquire();
		stopRingIntent = new Intent(this, ActivityStopRing.class);
//		stopRingIntent.setClass(this, stopRing.class);
		stopRingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		  SharedPreferences pref1 = PreferenceManager.getDefaultSharedPreferences(this);
		  selRing = pref1.getString("selRing", defaultRing.toString());
		  maxRing = pref1.getBoolean("maxRing", true);
		  interruptableRing = pref1.getBoolean("interruptableRing", true);
		  ringTime = pref1.getString("ringTime", "30");
		  //Log.i(TAG, "Ring Time equals: " + ringTime);
		  //Log.i(TAG, "Wake Lock Active: " + String.valueOf(wl.isHeld()));
		  
		defaultRing.toString();
		
		medPlay = new MediaPlayer();
		medPlay.setAudioStreamType(AudioManager.STREAM_RING);
		
		stopRing = new Runnable(){
			public void run(){
				stopSelf();
			}
		};
		stopRinging = new Runnable(){
			@Override
			public void run(){
				startActivity(stopRingIntent);
			}
		};
		stopHandler = new Handler();
		
		selRingURI = Uri.parse(selRing);
		
		 try {medPlay.setDataSource(this, selRingURI);
		 		medPlay.prepare();
			} catch (Exception e){
				//Log.e(TAG, e.toString());
				try{medPlay = MediaPlayer.create(this, R.raw.dubstep);}
				catch (Exception x){
					//Log.e(TAG, e.toString());
					}
			 } 
			
			
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
//		Toast.makeText(this, "started", Toast.LENGTH_SHORT).show();
				
				audMan = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
				
			
					if (maxRing){
						audMan.setStreamVolume(AudioManager.STREAM_RING, audMan.getStreamMaxVolume(AudioManager.STREAM_RING), 
							AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
							if (audMan.getStreamVolume(AudioManager.STREAM_RING) != 
								audMan.getStreamMaxVolume(AudioManager.STREAM_RING)) {
								int up;
								for (up = audMan.getStreamVolume(AudioManager.STREAM_RING);
									up < audMan.getStreamMaxVolume(AudioManager.STREAM_RING);
									up++){
									audMan.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE);
								}
							}	
				} 
					audMan.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
					audMan.setMode(AudioManager.MODE_NORMAL);
			
			if (!ringTime.equals("ringNot")){
				try {
				medPlay.start();
				
					if (interruptableRing){
						stopHandler.postDelayed(stopRinging, 1000*5);
					}
					
					  medPlay.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
						@Override
						public void onCompletion(MediaPlayer medPlay) {
						
								medPlay.start();
												}
					});
				}
			catch (Exception e){
				//Log.e(TAG, e.getMessage());
				}
			}
			  
			if (ringTime.equals("300")){
				stopHandler.postDelayed(stopRing, 1000 * 60 * 5);
				
			} else if (ringTime.equals("120")){
				stopHandler.postDelayed(stopRing, 1000 * 60 * 2);
			
			} else if (ringTime.equals("30")){
				stopHandler.postDelayed(stopRing, 1000 * 60 / 2);
			} 
    		return START_NOT_STICKY;
    	}
		
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	try {
    	medPlay.stop();
    	medPlay.release();
    	stopHandler.removeCallbacks(stopRing);
    	stopHandler.removeCallbacks(stopRinging);} catch (Exception e){
    		//Log.e(TAG, e.getMessage());
    		}
    	String pn = context.getPackageName();
    	context.sendBroadcast(new Intent(pn+".stopRing.class"));
		try {wl.release();}catch (Exception e){
			//Log.e(TAG, e.getMessage());
			}
		//Log.i(TAG, "WAKE LOCK ACTIVE: "+wl.isHeld());
    }
	
	}
