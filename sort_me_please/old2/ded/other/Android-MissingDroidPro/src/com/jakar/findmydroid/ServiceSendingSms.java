package com.jakar.findmydroid;

import java.util.ArrayList;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class ServiceSendingSms extends Service {

	SmsManager smsMan;
	String sendType;
	String sendString;
	String sendAddress;
	
//	int numOfStrings;
//	ArrayList<String> sendStringList;
	
	String TAG = "MISSING_DROID";
	
//	int sI;
	
	@Override
	public void onCreate(){
//		sI = 0;
//		numOfStrings = 0;
		//Log.i(TAG, "sendingSmsService created");
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId){
		
		
		String message = intent.getStringExtra("message");  
		String recipient = intent.getStringExtra("recipient");
		ArrayList<String> recipient_list = intent.getStringArrayListExtra("recipient_list");
		int attempts_allowed = intent.getIntExtra("attempts_allowed", 3);
		boolean dataMsg = intent.getBooleanExtra("dataMsg", false);
//		Log.e(M.tag, message);
		M.sendSmsMessage(this, message, recipient, recipient_list, attempts_allowed, dataMsg);
		
		return START_NOT_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
