package com.jakar.findmydroid;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ActivityTabSecure extends PreferenceActivity {
	
	final String tag = "MISSING_DROID";
	Context context;
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	
	CheckBoxPreference reqAdmin;
	final int REQUEST_ADMIN = 0;
	final int REMOVE_ADMIN = 1;
	CheckBoxPreference secLock;
	CheckBoxPreference secWipe;
	CheckBoxPreference preventUninstall;
//	CheckBoxPreference subChangeLock;
	final int SUB_CHANGE_LOCK = 2;
	
	final int GET_PRO = 3;
	
	final int PREVENT_UNINSTALL = 4;
	final int PREVENT_UNINSTALL_NO_LOCK = 5;
	
	private String unlockContent;
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.activitytabsecure);
		
		context = this;
		pref = PreferenceManager.getDefaultSharedPreferences(context);
		editor = pref.edit();
		
		reqAdmin = (CheckBoxPreference)findPreference("reqAdmin");
		secLock = (CheckBoxPreference)findPreference("secLock");
		secWipe = (CheckBoxPreference)findPreference("secWipe");
		preventUninstall = (CheckBoxPreference)findPreference("preventUninstall");
		//subChangeLock = (CheckBoxPreference)findPreference("subChangeLock");
		
		secWipe.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				if (Boolean.valueOf(String.valueOf(newValue))){
					new AlertDialog.Builder(context)
					.setTitle("Notice!")
					.setMessage("Missing Droid will now be able to factory reset your phone. You understand that "
							+"only you, the user, are responsible for any lost data.")
					.setPositiveButton("Agree", null)
					.setNegativeButton("Decline", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							secWipe.setChecked(false);							
						}
					})
					.show();
				}
				
					
				return true;
			}
		});
		
		reqAdmin.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object newValue) {
				if (Build.VERSION.SDK_INT>7){
					if (AdminConditionalClass.checkAdmin(context) && Boolean.valueOf(String.valueOf(newValue))){
						return true;
					} else if (!AdminConditionalClass.checkAdmin(context) && !Boolean.valueOf(String.valueOf(newValue))){
						return true;
					}
				}
				if (Boolean.valueOf(String.valueOf(newValue))){
					if (Build.VERSION.SDK_INT > 7){
						showDialog(REQUEST_ADMIN);
					} else {Toast.makeText(getBaseContext(), "Only available for Android 2.2 and up", Toast.LENGTH_LONG).show();
		        	//Log.i(tag, "SDK_VERSION TOO LOW, DEVICE ADMINISTRATION NOT REQUESTED");
					}
				} else {
					//Log.i(tag, "REQUESTING DEV ADMIN OFF (dialog)");
					showDialog(REMOVE_ADMIN);
				}
				return false;
			}
		});
		secLock.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				if (!Boolean.valueOf(String.valueOf(arg1))){
					preventUninstall.setChecked(false);
					//subChangeLock.setChecked(false);
				}
				return true;
			}
		});
//		subChangeLock.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//			
//			@Override
//			public boolean onPreferenceChange(Preference arg0, Object arg1) {
//				if (Boolean.valueOf(String.valueOf(arg1))){
//					if (!secLock.isChecked() || !pref.getBoolean("checkSim", false)){
//						showDialog(SUB_CHANGE_LOCK);
//						return false;
//					} else return false;
//					
//				} else return false;
//				
//			}
//		});
		preventUninstall.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				if (Boolean.valueOf(String.valueOf(arg1))){					
					
			    	if (!Market.upgradeDialog(context, null)){
			    		return false;
			    	} else if (secLock.isChecked()){
			    		showDialog(PREVENT_UNINSTALL);
		    		return false;
			    	} else {
			    		showDialog(PREVENT_UNINSTALL_NO_LOCK);
			    		return false;
			    	}
				} else return true;
			}
		});
		final CheckBoxPreference protectData = (CheckBoxPreference)findPreference("protectData");
		protectData.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {		
				return Market.upgradeDialog(context, null);
			}
		});
		Preference backupNotice = (Preference)findPreference("backupNotice");
		backupNotice.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				new AlertDialog.Builder(context).setTitle("Notice")
					.setMessage("This feature is currently under development and is planned for a future update.")
					.setPositiveButton("Ok", null)
					.show();
				return false;
			}
		});
		//Preference formatSD = (Preference)findPreference("formatSD");
//		formatSD.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
//			
//			@Override
//			public boolean onPreferenceChange(Preference preference, Object newValue) {
//				new AlertDialog.Builder(context).setTitle("Notice")
//				.setMessage("This feature is currently under development and is planned for the next update.")
//				.setPositiveButton("Ok", null)
//				.show();
//				return false;
//			}
//		});
		Preference allowList = (Preference)findPreference("allowList");
		allowList.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.tabHost.setCurrentTab(6);
				return false;
			}
		});
		Preference blockList = (Preference)findPreference("blockList");
		blockList.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.tabHost.setCurrentTab(5);
				return false;
			}
		});
		EditTextPreference secPassword = (EditTextPreference)findPreference("secPassword");
		secPassword.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				int l = String.valueOf(newValue).length();
				if (l<4||l>8){
					new AlertDialog.Builder(context).setTitle("Invalid Password!")
						.setMessage("Password must be 4-8 characters. New password was not saved.")
						.setPositiveButton("Ok", null)
						.show();
					return false;
				}else {
					return true;
				}
			}
		});
	}
	@Override
	public void onResume(){
		super.onResume();
		if (Build.VERSION.SDK_INT>7){
		reqAdmin.setChecked(AdminConditionalClass.checkAdmin(context));
		}
	}
	
	
	@Override
    protected Dialog onCreateDialog(int id) {
    	
        switch (id) {
        case (REQUEST_ADMIN):
			AlertDialog requestAdminDialog = new AlertDialog.Builder(this)
			.setTitle("Enable Administration")
			.setMessage("Enabling Missing Droid to have Device Administration will let it lock your phone and wipe it's contents " +
					"for added protection.")
			.setPositiveButton("Enable", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					 ComponentName devAdminReceiver = new ComponentName(context, AdminReceiver.class);
					 Intent reqAdmin = new Intent("android.app.action.ADD_DEVICE_ADMIN");
				     reqAdmin.putExtra("android.app.extra.DEVICE_ADMIN",
				        		devAdminReceiver);
				     startActivityForResult(reqAdmin, 0);
				}
			})
			.setNegativeButton("Cancel", null)
			.show();
        
        	return requestAdminDialog;
		case (REMOVE_ADMIN):
			AlertDialog removeAdminDialog = new AlertDialog.Builder(this)
			.setTitle("Disable Administration")
			.setMessage("If you disable administration for Missing Droid, your phone can no longer " +
					"be locked or remotely wiped, and anybody will be able to easily uninstall Missing Droid " +
					"from your device. Are you sure?")
			.setPositiveButton("Disable", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					if (Build.VERSION.SDK_INT>7){
					AdminConditionalClass.removeAdmin(context);
					}
					reqAdmin.setChecked(false);
				}
			})
			.setNegativeButton("Leave Enabled", null)
			.show();
		
			return removeAdminDialog;
		case (SUB_CHANGE_LOCK):
			
			AlertDialog subChangeLockDialog = new AlertDialog.Builder(this)
//	    	.setMessage("To lock on sim card or phone number change, you must allow Missing Droid to monitor sim and phone number and " +
//	    			"lock your phone. \nWould you like to enable those now?")
			.setMessage("Due to some technical issues, this feature is temporarily disabled "
						+"and is planned to be re-enabled in the next update.")
//	    	.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
//				
//				@Override
//				public void onClick(DialogInterface arg0, int arg1) {
//					if (!secLock.isChecked())
//					secLock.setChecked(true);
//					if (!pref.getBoolean("checkSim", false)){
//						
//						TelephonyManager telMan = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//    					String savedSim = telMan.getSimSerialNumber();
//    					String inUseSim = telMan.getSimSerialNumber();
//    					String savedNum = telMan.getLine1Number();
//    					String inUseNum = telMan.getLine1Number();
//    					editor.putBoolean("checkSim", true);
//    					editor.putString("savedSim", savedSim);
//    					editor.putString("inUseSim", inUseSim);
//    					editor.putString("savedNum", savedNum);
//    					editor.putString("inUseNum", inUseNum);
//    					editor.commit();
//    					
//    					AlarmManager alarmMan = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
//    					String pn = context.getPackageName();
//    					Intent intent = new Intent(pn+".ReceiverCheckSub");
//    					PendingIntent PI = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
//    					alarmMan.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime()+1000*60, PI);
//					}
//						subChangeLock.setChecked(true);
//				}
//			})
			.setNegativeButton("Ok", null)
	    	.show();
			return subChangeLockDialog;
		case(GET_PRO):
			AlertDialog getProDia = new AlertDialog.Builder(context)
			.setMessage(Market.upgrade)
			.setPositiveButton("Get Pro", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					Market.donate(context);
				}
			})
			.setNegativeButton("Not Now", null)
			.show();
			return getProDia;
		case (PREVENT_UNINSTALL):
			
        	AlertDialog puDialog = new AlertDialog.Builder(this)
        	.setTitle("Warning!")
        	.setMessage("If someone tries to disable Missing Droid as a Device Administrator through System Settings,"
        				+" then Missing Droid will attempt to lock your phone with the password you've chosen above."
        				+"\nBy clicking 'I Agree' you affirm that you, and only you,"
        				+" are responsible if you cannot uninstall Missing Droid.")
        	.setPositiveButton("I Agree", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					preventUninstall.setChecked(true);
				}
			})
			.setNegativeButton("I Decline", null)
			.show();
			return puDialog;
		case (PREVENT_UNINSTALL_NO_LOCK):
			AlertDialog punlDialog = new AlertDialog.Builder(this)
	    	.setMessage("Preventing uninstallation requires you to allow Missing Droid to lock your phone." +
	    			"\nWould you also like to allow Missing Droid to lock your phone?")
	    	.setPositiveButton("Allow", new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					secLock.setChecked(true);
					showDialog(PREVENT_UNINSTALL);
				}
			})
			.setNegativeButton("Cancel", null)
	    	.show();
			
			return punlDialog;
		
        }
        return null;
	}

}
