package com.jakar.findmydroid;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.widget.Toast;

public class ActivityTabGeneral extends PreferenceActivity {
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	Context context;
	LayoutInflater inflater;
	
	Preference howTo;
	final int howToLocateDia = 0;
	final int howToRingDia = 1;
	final int howToLockDia = 2;
	final int howToWipeDia = 3;
	final int howToForwardDia = 4;
	final int howToOpenDia = 5;
	
	static public CheckBoxPreference checkSim;
	
	Preference commandList;
	
	CheckBoxPreference hideApp;
	final int HIDE_APP = 6;
	final int CF_TEST = 7;
	final int CF_INFO = 8;
		
	TelephonyManager telMan;
	
	
	
	boolean enableKeyword;
	String keyword;
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.activitytabgeneral);
		//report list will open as a new tab using the setTab function (if I can facilitate this). 
		//Then override the back key so it will set the tab back to general. 
		//This way I do not leave the tabhost and app_lock is more efficient and convenient for me and the user
		context = this;
		
		pref = PreferenceManager.getDefaultSharedPreferences(context);
		editor = pref.edit();	
		
		
		final Preference howToLocate = findPreference("howToLocate");
		final Preference howToRing = findPreference("howToRing");
		final Preference howToLock = findPreference("howToLock");
		final Preference howToWipe = findPreference("howToWipe");
		final Preference howToForward = findPreference("howToForward");
		final Preference howToOpen = findPreference("howToOpen");
		Preference.OnPreferenceClickListener howToListener = new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				String message ="";
				enableKeyword = pref.getBoolean("enableKeyword", true);
				if (enableKeyword){
					keyword = pref.getString("keyword", "Android")+" ";
				} else {
					keyword = "";
				}
				
				if (preference==howToLocate){
					message = "Send a text message from someone else's phone to yours that says \n'"
							+keyword+pref.getString("locateCommand", "gps")+"'\n"
							+"and your phone will respond with its location information";
				} else if (preference==howToRing){
					message = "Send a text message from someone else's phone to yours that says \n'"
							+keyword+pref.getString("ringCommand", "ring")+"'\n "
							+"to activate ringing.";
				} else if (preference==howToLock){
					message = "Send a text message from someone else's phone to yours that says \n'"
							+keyword+pref.getString("lockCommand", "lock")+" enter_password_here'\n"
							+"to lock your phone with the password entered. Leave 'enter_password_here' blank to lock your phone"
							+" and set the password to your Missing Droid Password: "+pref.getString("secPassword", "password");	
					if (!pref.getBoolean("secLock", false)){
						message = "You have not enabled locking. You must first enable locking on the 'Secure'"
								+" tab then you can view instructions.";
					}
				} else if (preference==howToWipe){
					message = "Send a text message from someone else's phone to yours that says \n'"
							+keyword+pref.getString("wipeCommand", "lock")+"'\n"
							+"to factory reset your phone";
					if (!pref.getBoolean("secWipe", false)){
						message = "You have not enabled wiping/resetting. You must first enable it on the 'Secure'"
								+" tab then you can view instructions.";
					}
				} else if (preference==howToForward){
					message = "Send a text message from someone else's phone to yours that says \n'"
							+keyword+pref.getString("callForwardCommand", "forward")+"'\n"
							+"to disable call forwarding or \n'"
							+keyword+pref.getString("callF 	orwardCommand", "forward")+" phone_number'\n"
							+"to enable call forwarding to phone_number";
					
				} else if (preference==howToOpen){
					message = "1.)Send a text message from someone else's phone to yours that says '"
							+keyword+pref.getString("openCommand", "open")+"' "
							+"to open Missing Droid if it is hidden."
							+"\n2.)Or make a phone call to "+pref.getString("dialCode", "123456789")
							+"\n3.)Or go to Missing Droid in the Application Manager and choose 'Manage Data'";
					if (!pref.getBoolean("hideApp", false)){
						message = "Missing Droid is not currently hidden. "
								+"You may view instructions after setting Missing Droid to be hidden";
					}
				}
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("GPS Locate")
				.setMessage(message)
				.setPositiveButton("OK", null)
				.show();
				return false;
			}
		};
		howToLocate.setOnPreferenceClickListener(howToListener);
		howToRing.setOnPreferenceClickListener(howToListener);
		howToLock.setOnPreferenceClickListener(howToListener);
		howToWipe.setOnPreferenceClickListener(howToListener);
		howToForward.setOnPreferenceClickListener(howToListener);
		howToOpen.setOnPreferenceClickListener(howToListener);
		
				
		CheckBoxPreference enableCustomCommands = (CheckBoxPreference)findPreference("enableCustomCommands");
		enableCustomCommands.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				
				if (!Boolean.valueOf(String.valueOf(newValue))){
					editor.putString("locateCommand", "gps");
					editor.putString("ringCommand", "ring");
					editor.putString("lockCommand", "lock");
					editor.putString("wipeCommand", "wipe");
					editor.putString("callForwardCommand", "forward");
					editor.putString("openCommand", "open");
					editor.commit();
					
				} else {
					return Market.upgradeDialog(context, null);
				}
				return true;
			}
		});
		
		hideApp = (CheckBoxPreference)findPreference("hideApp");
		hideApp.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference arg0, Object arg1) {
				if (Boolean.valueOf(String.valueOf(arg1))){
					keyword = pref.getString("keyword", "Android")+" ";
					if (!pref.getBoolean("enableKeyword", true)){
						keyword= "";
					}
					String message = "To open Missing Droid, you will have to\n"
							+"1.)Send a text message from someone else's phone to yours that says '"
							+keyword+pref.getString("openCommand", "open")+"' "
							+"\n2.)Or make a phone call to "+pref.getString("dialCode", "123456789")+" which you may change below"
							+"\n3.)Or go to Missing Droid in the Application Manager and choose 'Manage Data'";
					AlertDialog.Builder builder = new AlertDialog.Builder(context);
					builder.setTitle("Hide SMS")
					.setMessage(message)
					.setPositiveButton("OK", null)
					.show();
					return true;
				} else {
					
					return true;
				}
			}
		});
		Preference callForwardingHelp = (Preference)findPreference("callForwardingHelp");
		callForwardingHelp.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.openWebsite(context, "jakar-apps/1348785960/");
				return false;
			}
		});
		CheckBoxPreference autoDetectSettings = (CheckBoxPreference)findPreference("autoDetectSettings");
		autoDetectSettings.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			
			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				String startCodeS = "";
				String endCodeS = "";
				String disableCodeS = "";
				if (Boolean.valueOf(String.valueOf(newValue))){
					startCodeS = M.getCallForwardCode("startCode", context);
					endCodeS = M.getCallForwardCode("endCode", context);
					disableCodeS = M.getCallForwardCode("disableCode", context);
				}
				EditTextPreference startCode = (EditTextPreference)findPreference("startCode");
				EditTextPreference endCode = (EditTextPreference)findPreference("endCode");
				EditTextPreference disableCode = (EditTextPreference)findPreference("disableCode");
				startCode.setText(startCodeS);
				endCode.setText(endCodeS);
				disableCode.setText(disableCodeS);
				editor.putString("startCode", startCodeS);
				editor.putString("endCode", endCodeS);
				editor.putString("disableCode", disableCodeS);
				editor.commit();
				return true;
			}
		});
		Preference testCallForwarding = (Preference)findPreference("testCallForwarding");
		testCallForwarding.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Test Call Forwarding")
					.setMessage("Note: Not all carriers support call forwarding."
								+"\n'Enable' will enable immediate call forwarding to your voicemail."
								+"\nAn outbound phone call to your carrier's automated system may be made when testing")
					.setNegativeButton("Enable", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							TelephonyManager telMan = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
							
							String forwardNumber = telMan.getVoiceMailNumber();
							M.setCallForwarding(context, forwardNumber);
						}
					})
					.setNeutralButton("Disable", new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							M.setCallForwarding(context, "");
						}
					})
					.setPositiveButton("Done", null)		
					.show();
				return false;
			}
		});
		Preference history = (Preference)findPreference("history");
		history.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.tabHost.setCurrentTab(4);
				return false;
			}
		});
		Preference termsAndConditions = (Preference)findPreference("termsAndConditions");
		termsAndConditions.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("End User Agreement")
					.setMessage("By using Missing Droid, you agree to the End User Agreement at "+M.EULA
							+"\nIf you disagree, please uninstall this app.")
					.setPositiveButton("Okay", null)
				.setNeutralButton("Uninstall", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						Uri packageUri = Uri.parse("package:"+context.getPackageName());
			            Intent uninstallIntent =
			              new Intent(Intent.ACTION_DELETE, packageUri);
			            startActivity(uninstallIntent);
					}
				})
				.setNegativeButton("View Agreement", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
			    		M.openWebsite(context, M.EULA);
			    		finish();
					}
				})
				.show();
				return false;
			}
		});
		Preference contactSupport = (Preference)findPreference("contactSupport");
		contactSupport.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.openWebsite(context, "contact/");
				return false;
			}
		});
		Preference viewWebsite = (Preference)findPreference("viewWebsite");
		viewWebsite.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			
			@Override
			public boolean onPreferenceClick(Preference preference) {
				M.openWebsite(context, "jakar-apps/1348770659/");
				return false;
			}
		});
	}
	
	 @Override
	 public void onStop(){
		 super.onStop();
	 }	 

}
