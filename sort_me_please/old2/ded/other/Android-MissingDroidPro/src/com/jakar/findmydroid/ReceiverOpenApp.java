package com.jakar.findmydroid;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;

public class ReceiverOpenApp extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = pref.edit();
		if (intent.hasExtra(Intent.EXTRA_PHONE_NUMBER)){
			String outgoingNum = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//			Log.e(OtherMethods.tag, outgoingNum);
			if (pref.getString("dialCode", "123456789").equals(outgoingNum)){
				PackageManager pm = context.getPackageManager();
        		String packageName = context.getPackageName();
    			ComponentName compName = new ComponentName(context, ActivityTabHost.class);
    			if (pm.getComponentEnabledSetting(compName)==PackageManager.COMPONENT_ENABLED_STATE_DISABLED){
    				pm.setComponentEnabledSetting(compName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
    	    				PackageManager.DONT_KILL_APP);
    			}
    			editor.putBoolean("hideApp", false);
    			editor.commit();
    			
    			Intent settings = new Intent(context, ActivityTabHost.class);
				settings.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(settings);
				setResultData(null);
    			
			}
		}
		
	}

}
