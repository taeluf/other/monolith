# PHP-Business (under construction)
A library for storing and using business information. This is proprietary currently. Future plans include making public & extensible and robust.

# ARCHIVE Oct 14, 2019
This was my attempt at abstracting away specific functionalities and incorporating with schema.org definitions. I may return to this at a future point, but for now, I am simply building specific business-relevant software on an as-needed basis. Would love to develop this into a robust library, but it also does not match the updated version of Tiny. Perhaps again one day.


## Issues
* This lib is SO under-developed that the issues are... basically the entire lib! lol
* Not sure how to set title for business page... Maybe need some more communication between the business classes.

## Plans
* Create a very basic MVC, include it in the `lib` folder (and/or require through composer?). 
* Integrate with my Tag system for searching. I could make Business it's own search, but I need the tag-stuff working, so I'll start with that.
* Create a Breadcrumbs (categories) lib. Integrate with that and create category pages.
* Figure out some easy-way to transfer styles to a public directory
* Create a handler script for determining which view to display, & submitting data, etc. Create a singular static function to invoke this script.

