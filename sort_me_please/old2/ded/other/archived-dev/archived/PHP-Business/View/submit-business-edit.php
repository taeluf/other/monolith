<?php


\ROF\Business::checkAdminAccess();

$pdo = \ROF\FN::getPdo();

$values = $_POST; //before it's ready for public use, I will need to add input validation
if (isset($values['shortUniqueName'])&&strlen($values['shortUniqueName'])>0){
  $name = $values['name'];
  $cleanName = preg_replace("/[^a-zA-Z0-9]/","",$name);
  $shortName = $cleanName;
  $takenCount = 0;
  $searchingForName = TRUE;
  while ($searchingForName) {

    $checkUniqueName = $pdo->prepare("SELECT * FROM Business WHERE shortUniqueName LIKE :name");
    $checkUniqueName->execute(array(
      ":name" => $shortName
    ));
    if (count($checkUniqueName->fetchAll())==0)$searchingForName = FALSE;
    else $shortName = $cleanName.$takenCount; 
    $takenCount++;
    $checkUniqueName->closeCursor();
  }
  $shortName;
  $update = $pdo->prepare("UPDATE Business SET name=:name, shortUniqueName=:newShortName, schemaType=:schemaType, primaryImageUrl=:primaryImageUrl, "
                          ."blurb=:blurb, description=:description, streetAddress=:streetAddress, city=:city, state=:state, zipCode=:zipCode, " 
                          ."companyName=:companyName, companyId=:companyId, phoneNumber=:phoneNumber, email=:email, contactUrl=:contactUrl, "
                          ."officialUrl=:officialUrl, facebookUrl=:facebookUrl, menuUrl=:menuUrl, yelpUrl=:yelpUrl "
                          ."WHERE shortUniqueName LIKE :shortUniqueName");
  $update->execute(array(
      ":name" => $values["name"],
      ":newShortName" => $shortName,
      ":schemaType" => $values["schemaType"],
      ":primaryImageUrl" => $values["primaryImageUrl"],
      ":blurb" => $values["blurb"],
      ":description" => $values["description"],
      ":streetAddress" => $values["streetAddress"],
      ":city" => $values["city"],
      ":state" => $values["state"],
      ":zipCode" => $values["zipCode"],
      ":companyName" => $values["companyName"],
      ":companyId" => $values["companyId"],
      ":phoneNumber" => $values["phoneNumber"],
      ":email" => $values["email"],
      ":contactUrl" => $values["contactUrl"],
      ":officialUrl" => $values["officialUrl"],
      ":facebookUrl" => $values["facebookUrl"],
      ":menuUrl" => $values["menuUrl"],
      ":yelpUrl" => $values["yelpUrl"],
      ":shortUniqueName" => $values['shortUniqueName']
  ));
  echo "updated";
} else {
  $name = $values['name'];
  $cleanName = preg_replace("/[^a-zA-Z0-9]/","",$name);
  $shortName = $cleanName;
  $takenCount = 0;
  $searchingForName = TRUE;
  while ($searchingForName) {

    $checkUniqueName = $pdo->prepare("SELECT * FROM Business WHERE shortUniqueName LIKE :name");
    $checkUniqueName->execute(array(
      ":name" => $shortName
    ));
    if (count($checkUniqueName->fetchAll())==0)$searchingForName = FALSE;
    else $shortName = $cleanName.$takenCount; 
    $takenCount++;
    $checkUniqueName->closeCursor();
  }
  $values['shortUniqueName'] = $shortName;

  $insert = $pdo->prepare("INSERT INTO "
                          ."Business(name, shortUniqueName, schemaType, primaryImageUrl, blurb, description, streetAddress, city, state, zipCode, "
                          ."companyName, companyId, phoneNumber, email, contactUrl, officialUrl, facebookUrl, menuUrl, yelpUrl) "
                          ."VALUES(:name, :shortUniqueName, :schemaType, :primaryImageUrl, :blurb, :description, :streetAddress, :city, :state, "
                          .":zipCode, :companyName, :companyId, :phoneNumber, :email, :contactUrl, :officialUrl, :facebookUrl, :menuUrl, :yelpUrl)"
                          );


  $insert->execute(array(
      ":name" => $values["name"],
      ":shortUniqueName" => $values["shortUniqueName"],
      ":schemaType" => $values["schemaType"],
      ":primaryImageUrl" => $values["primaryImageUrl"],
      ":blurb" => $values["blurb"],
      ":description" => $values["description"],
      ":streetAddress" => $values["streetAddress"],
      ":city" => $values["city"],
      ":state" => $values["state"],
      ":zipCode" => $values["zipCode"],
      ":companyName" => $values["companyName"],
      ":companyId" => $values["companyId"],
      ":phoneNumber" => $values["phoneNumber"],
      ":email" => $values["email"],
      ":contactUrl" => $values["contactUrl"],
      ":officialUrl" => $values["officialUrl"],
      ":facebookUrl" => $values["facebookUrl"],
      ":menuUrl" => $values["menuUrl"],
      ":yelpUrl" => $values["yelpUrl"]
  ));



  echo "inserted!";

}



?>