<?php

\ROF\Business::checkAdminAccess();

  $biz = \ROF\Business\Loader::getBusinessByName(@$_GET['biz']);
  if ($biz===NULL){
    $biz = new \ROF\Business();
  }


?>
  <form id="business-edit" action="/submit-business-edit.php" method="post">
    <fieldset>
        <legend>Basic Info</legend>
        <label for="name">Business Name</label><br>
        <input type="text" name="name" id="name" value="<?=$biz->getName()?>" required/>
        <br>
        <label for="description">Description</label><br>
        <textarea name="description" id="description" cols="70" rows="10" required><?=$biz->getDescription()?></textarea>
        <br>
    </fieldset>
    <fieldset>
        <legend>Address</legend>
        <label for="streetAddress">Street Address</label><br>
        <input type="text" name="streetAddress" id="streetAddress" value="<?=$biz->getStreetAddress()?>" required/>
        <br>
        <label for="city">City Name</label><br>
        <input type="text" name="city" id="city" value="<?=$biz->getCity()?>" required/>
        <br>
        <label for="state">State Abbreviation</label><br>
        <input type="text" name="state" id="state" value="<?=$biz->getState()?>" required/>
        <br>
        <label for="zipCode">Zip Code</label>(5-digit)<br>
        <input type="text" name="zipCode" id="zipCode" value="<?=$biz->getZipCode()?>" required/>
        <br>
    </fieldset>
    <fieldset>
        <legend>Search &amp; Meta</legend>
        <label for="primaryImageUrl">Header Image URL</label><br>
        <input type="url" name="primaryImageUrl" id="primaryImageUrl" value="<?=$biz->getPrimaryImageUrl()?>" required/>
        <br>
        <label for="schemaType">Schema Type</label> - See <a href="https://schema.org/LocalBusiness">https://schema.org/LocalBusiness</a> for valid types
        <br>
        <input type="url" name="schemaType" id="schemaType" value="<?=$biz->getSchemaType()?>" required/>
        <br>
        <label for="blurb">Short Blurb</label><br> 
        <textarea name="blurb" id="blurb" cols="70" rows="6" required><?=$biz->getBlurb()?></textarea>
        <br>
    </fieldset>
    <fieldset>
        <legend>Contact Info</legend>
        <label for="phoneNumber">Contact Phone Number</label>(optional)<br>
        <input type="tel" name="phoneNumber" id="phoneNumber" value="<?=$biz->getPhoneNumber()?>"/>
        <br>
        <label for="email">Email Address</label>(optional)<br>
        <input type="email" name="email" id="email" value="<?=$biz->getEmail()?>" />
        <br>
        <label for="contactUrl">Web Site Contact Page</label>(optional)<br>
        <input type="text" name="contactUrl" id="contactUrl" value="<?=$biz->getContactUrl()?>" />
        <br>
    </fieldset>
    
    <fieldset>
        <legend>Links</legend>
        <label for="officialUrl">Official Web Site</label>(optional)<br>
        <input type="text" name="officialUrl" id="officialUrl" value="<?=$biz->getOfficialUrl()?>" />
        <br>
        <label for="facebookUrl">Facebook Page</label>(optional)<br>
        <input type="text" name="facebookUrl" id="facebookUrl" value="<?=$biz->getFacebookUrl()?>"/>
        <br>
        <label for="menuUrl">Menu URL</label>(optional)<br>
        <input type="text" name="menuUrl" id="menuUrl" value="<?=$biz->getMenuUrl()?>"/>
        <br>
        <label for="yelpUrl">Yelp URL</label>(optional)<br>
        <input type="text" name="yelpUrl" id="yelpUrl" value="<?=$biz->getYelpUrl()?>"/>
    </fieldset>
    <br>
    <input type="hidden" name="shortUniqueName" value="<?=$biz->getShortUniqueName()?>" />
    <input type="submit" value="Submit" />
  </form>
