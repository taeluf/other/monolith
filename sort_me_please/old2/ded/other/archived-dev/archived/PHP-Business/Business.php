<?php

namespace ROF;

class Business {
  
  public $dbProperties = array(
    "name"=>"",
    "shortUniqueName" => "",
    "schemaType"=>"https://schema.org/LocalBusiness",
    "primaryImageUrl"=>"",
    "blurb"=>"",
    "description" => '',
    "streetAddress" => '',
    "city" => '',
    "state" => '',
    "zipCode" => '', // needs to be added to database
    "companyName" => '',
    "companyID" => '',
    "phoneNumber" => '',
    "email" => '',
    "contactUrl" => '',
    "officialUrl" => '',
    "facebookUrl" => '',
    "menuUrl" => '',
    "yelpUrl" => '',
    "id" => '',
    
    // COMPLEX properties
    "tags" => [],
    "breadcrumbs" => ["Businesses" => "/businesses.php", "Food" => "/category.php?cat=food"],
    "images" => ["https://scontent-ort2-2.xx.fbcdn.net/v/t1.0-9/48416028_1635169306789771_4536382790618316800_n.jpg?_nc_cat=107&_nc_ht=scontent-ort2-2.xx&oh=864f0d628c222f68ab5ab6c0e00ddddc&oe=5CCB31BB" => "fam", "https://scontent-ort2-2.xx.fbcdn.net/v/t1.0-9/48077901_2006666952759976_2172230858717528064_o.jpg?_nc_cat=106&_nc_ht=scontent-ort2-2.xx&oh=8abb73b3012e2332a6f41a4ddf01380f&oe=5C941E7A" => "wedding"],
    "hoursList" => ["Mon-Fri 8am-9pm" => "schema stuff", "Sat-Sun 10am-7pm" => "more schema"]
  );
  
  public function __construct(){

  }
  
  static public function getChangeListingUrl(){
    return "http://www.decaturdirectory.com/help/correction.php";
  }
  static public function getNewBizUrl(){
    return "http://www.decaturdirectory.com/help/new-listing.php";
  }
  
  public function getLocationName(){
    return "Location";
  }
  
  public function getLocationsUrl(){
    return "http://www.decaturdirectory.com/all-locations.php?biz=".$this->getShortUniqueName();
  }
  
  static public function checkAdminAccess(){
    $user = \ROF\User\User::getLoggedInUser();
    $requiredLevel = \ROF\Permissions\Permissions::ACCESS_ADMIN;
    $hasAccess = \ROF\Permissions\Permissions::hasAccess($user==NULL ? NULL : $user->userId,$requiredLevel);
    if (!$hasAccess){
      (new \ROF\MessageForm("Not Allowed", "You are not allowed to access the requested page, as you are not an admin of the site.",
                          array(
                          "Home" => "/"
                          )
                          )
        )->redirect();

    }
  }
  
  public function __call($method,$arguments){
    if (!\ROF\FN::strStartsWith($method,"get")||count($arguments)>0){
      throw new \Exception("Method '{$method}' is not accessible in class '".get_class($this)."'.");
    }
    $property = lcfirst(substr($method,3));
    if (isset($this->dbProperties[$property])){
      return $this->dbProperties[$property];
    } else {
      throw new \Exception("Method '{$method}' is not accessible in class '".get_class($this)."'. because property '{$property}' is invalid.");
    }
  }
  
}

?>
