<?php

namespace ROF\Business;

class Loader extends \ROF\MVC\Loader {
  
    /** Returns a business or NULL if no business is found by the given name, or if the name is null.
  *
  */
  static public function getBusinessByName($name){
    $pdo = \ROF\Business\Config::getPdo();
    $lookup = $pdo->prepare("SELECT * FROM Business WHERE shortUniqueName LIKE :name");
    $lookup->execute(array(
        ":name" => $name
      ));
    $results = $lookup->fetchAll();
    $lookup->closeCursor();
    $biz = \ROF\Business\Config::getLoader()->createFromRow($results[0],TRUE);
    if ($biz==NULL)return NULL;
    return $biz;
  }
  /** pass the row of data from the db
  *   $withTags= FALSE by default. Pass TRUE to run a query for it's tags. Pass an array of tags to use those as the business's tags
  *   An array of tags must be a key=>value pair where key is the name of the tag and value is a Tag Object
  *
  */
  static public function createFromRow($dbRow,$withTags=FALSE){
      $pdo = \ROF\Business\Config::getPdo();
      if ($dbRow==NULL){
        return NULL;
      }

      $biz = \ROF\Business\Config::getBusiness();
      foreach ($dbRow as $key=>$value){
        $biz->dbProperties[$key] = $value;
      }
    
    if ($withTags===TRUE){
      $tags = [];
      $lookup = $pdo->prepare("SELECT * FROM Tags "
                              ."JOIN tags_biz ON tags_biz.tag_id = Tags.id "
                              ."WHERE tags_biz.biz_id = :biz_id"
                              );
      $lookup->execute(array(
          ":biz_id" => $biz->getId()
        ));
      $tagResults = $lookup->fetchAll();
      $lookup->closeCursor();
      foreach($tagResults as $row){
        $tag = \ROF\Business\Config::getTag();
        $tag->name = $row['name'];
        $tag->description = $row['description'];
        $tag->id = $row['id'];
        $biz->dbProperties['tags'][$tag->name] = $tag;
      }
      
    } else if (is_array($withTags)){
      $biz->dbProperties['tags'] = $withTags;
    }
    
    return $biz;
  }
  
  
}


?>