<?php

namespace ROF;

/** The current purpose of this class is to facilitate searching for tags.
*
*/
class TagList {
  
    protected $tags = [];
  
    /** pass a list of tag names or an array of tag names
    *
    */
    public function __construct($tags){ 
        if (!is_array($tags))$tags = func_get_args();
        $tags = preg_replace("/[^a-zA-Z\-]/","----",$tags);
        $this->tags = $tags;
    }
  
    public function getRelatedTags(){
        $lookup = $pdo->prepare("SELECT output.* FROM Tags AS output "
                                ."JOIN Tags as input ON output");
    }
  
    public function getBusinesses(){
        $pdo = \ROF\FN::getPdo();
        $queryListString = "";
        $queryListArray = [];
        for ($i = 0; $i<count($this->tags);$i++){
            $paramName = ":".$this->tags[$i].$i;
            $queryListString .= $paramName;
            if ($i<(count($this->tags)-1))$queryListString .= ", ";
            $queryListArray[$paramName] = $this->tags[$i];
        }
        $lookup = $pdo->prepare($queryString="SELECT Business.* FROM Business "
                                ."JOIN tags_biz "
                                    ."ON tags_biz.biz_id = Business.id "
                                ."JOIN Tags ON Tags.id = tags_biz.tag_id "
                                ."WHERE Tags.name IN ({$queryListString})"
                               );
//         echo $queryString;
//         print_r($queryListArray);
//       exit;
        $lookup->execute($queryListArray);
        $businessList = [];
        while ($row=$lookup->fetch()){
          $business = Business::createFromRow($row);
          $businessList[$business->getName()] = $business;
        }
        return $businessList;
    }
  
  
  
}