<?php


$pdo = \ROF\FN::getPdo();

$bizId = $_POST['business_id'];

$insertString = "";
$insertData = array();
$selectedTags = $_POST['selected_tags'];
for ($i=0;$i<($c=count($selectedTags));$i++){
  $insertString .= "(:tag_id{$i}, :biz_id{$i})";
  $insertData[":tag_id{$i}"] = $selectedTags[$i];
  $insertData[":biz_id{$i}"] = $bizId;
  if ($i<($c-1)) $insertString .= ", ";
}

if (strlen($insertString)>0){
    $insert = $pdo->prepare("INSERT INTO tags_biz(tag_id,biz_id) VALUES "
                            .$insertString
                            );
    $success = $insert->execute($insertData);
}

$deletedTagsString = "";
$deletedTags = $_POST['removed_tags'];
$deletedData = array(":businessId" => $bizId);
for ($i=0;$i<($c=count($deletedTags));$i++){
    $deletedTagsString .= ":tagId{$i}";
    $deletedData[":tagId{$i}"] = $deletedTags[$i];
  if ($i<($c-1))$deletedTagsString .=", ";
}

if (strlen($deletedTagsString)>0){
  $delete = $pdo->prepare("DELETE FROM tags_biz WHERE biz_id = :businessId AND tag_id IN ({$deletedTagsString})");
  $delete->execute($deletedData);

}


?>