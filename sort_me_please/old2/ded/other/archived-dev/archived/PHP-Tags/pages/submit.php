<?php

$info = $_POST;
if (!isset($info['name'])&&!isset($info['description'])) {
  throw new \Exception("Name and Description must be set. Please try again.");
}

if (isset($info['id'])&&$info['id']!=NULL){
  $success = \ROF\Tag::updateTag($info['id'],$info['name'],$info['description']);
  if ($success) echo "Yay! Your tag was updated.";
  else echo "Boo! Tag didn't get updated :(";
} else if (\ROF\Tag::getTagByName($info['name'])==NULL){
  $success = \ROF\Tag::createTag($info['name'],$info['description']);
  if ($success) echo "Yay! Your tag was created.";
  else echo "Boo! Tag didn't get made :(";
} else {
  throw new \Exception("A tag with the name ".\ROF\Tag::getCleanName($info['name'])." is already in use. Sorry!!!");
}