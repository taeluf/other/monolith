<?php

namespace Tiny\IFace;

interface User {

    public function isActive();
    public function unlocksAny(...$keys);
    public function addKey($keyName);
    public function removeKey($keyName);
    public function addRole($roleName);
    static public function addKeyToRole($keyName,$roleName);
    static public function removeKeyFromRole($keyName,$role);

}