# PHP Tiny
MVC Web App framework. Makes it easy to integrate a full-featured web app (with its own db table, views, scripts, stylesheets, and php code) into an existing website. 

**Coming Soon:** Oct 14, 2019. I have a pretty simple, but working version. I have a few more basic features to add and a readme to write, then it will be ready for use.
