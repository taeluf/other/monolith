Tiny is in a mostly useable state. Plans that I have not yet really... worked on include:

- a Trait-heavy approach, as opposed to class-heavy
- Stages, such as 'app-added', 'app-created', 'app-willDeliver', etc
- more robust & clear 'expose' methodology
- user permissions??? maybe i have some.
- tiny-basics package which includes... maybe themes, user app, article app, and other commonly used utilities.
- new name for Tiny