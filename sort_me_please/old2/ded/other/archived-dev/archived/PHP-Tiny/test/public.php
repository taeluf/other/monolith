<?php


// $router = new \Tiny\Rail\Router()
$fileDir = __DIR__.'/../../tiny-sample-app/public/'; 


// $pubDir = new \Tiny\Rail\PubDir($fileDir);
// $pubFile = $pubDir->fileAt('/index.item.php');
// $pubFile2 = $pubDir->fileFor('about','more');
// $pubFile3 = $pubDir->fileFromUrl('/');

class AnonC {

    public $slug = 'some-slug';
    public $fileKey = 'some-file-key';
    public $action = 'some-action';
    public $item = 'some-item';
    public $meta = null;

    public function getCats(){
        return 'all the cats';
    }
};

class AnonMeta {
    public function addStyle(){

    }
    public function addScript(){

    }
    public function addHtml(){}
    PUBLIC function addAPIKey(){}
    public function data(){}
}
$anc = new AnonC;
$anc->meta = new AnonMeta;

$pubFile3 = new \Tiny\Rail\PubFile($fileDir.'/metadata.php');
$pubFile3->exposeMethod('getUser',function(){return 'some-dumb-user';});
// $pubFile3->exposeGetter('testGetter','test-readonly','true');
// $pubFile3->exposeSetter('testSetter','test-writeonly','true');

// $pubFile3->exposeParam('slug','overwritten-slug',true);
$pubFile3->exposeObjectMethods($anc, ['getCats']);
$pubFile3->exposeObjectParams($anc, ['slug','fileKey','action','item','meta']);
// $pubFile3->exposeObject($anc);//to expose all methods & params of it
echo $pubFile3->content();

