# OLD version
A javascript Class system for general javascript and for HTML Dom objects to inherit your classes "automatically"

# Jan 5, 2019
This project was fully functioning when I programmed in 2015. I'm just returning to javascript now. Been into PHP again since June 2018. It should work! But it may need some work, and the readme definitely needs some help. I'll get to it.

# Installing

Copy the code from Github and put it before any class declarations - so, put it first thing in the `<head>` section or at the top of your first script that gets included.

# Usage

**Class Definition** - Define a class with `manager.Class('ClassName',instanceFunction,staticFunction)` where instance function contains the methods that will apply to `new ClassName()` and static functions are callable by `ClassName.someFunc()`  
HTML Elements (`DOMElements`) can easily extend a javascript class by declaring `class="jdom MyClassName"` where the first class declared after `jdom` is what gets extended

**Inheritance** - child classes have a parent object which is the class its extending with all its functions wrapped, so the child is passed as the `thisArg` to any parent function. That magic happens thanks to `manager.setThisArg`. Call a parent method with `this.parent.funcName()`

**Static inheritance** - not supported currently. Hopefully one day soon.

**Usage 1** - assign a class to an html element by placing `class="jdom ClassName"` on the element, where the element inherits all props/methods from `new ClassName()` (including parent stuff). the instance method `ClassName.attach()` is called after attaching. This is through `manager.setClasses`

**Usage 2** - Create new class by calling new `MyClass()`. The constructor accepts paramaters, too.

**Usage 3** - call static functions by calling `MyClass.staticFunc()`.

Examples of the usage are available in the code snippet at https://codereview.stackexchange.com/questions/87983/javascript-class-system


Info copied from my old site (from 2015):

* Create new instances of a class with `new MyClass()`  
* Call static methods with `MyClass.staticMethod()`  
* Declare classes with `manager.Class(className,instanceClosure,staticClosure)` where `instanceClosure` and `staticClosure` are each a `function(){ }` where whats inside the braces is the body of the instance class and static class, respectively.
* Extend a class with `manager.Class(...).Extend(parentClassName);`
* All classes have access to `this` and `parent` (though, if there is no parent, then `parent` will be undefined).
* HTML Elements (`DOMElements`) can easily extend a javascript class by declaring `class="jdom MyClassName"` where the first class declared after `jdom` is what gets extended


# Problems & Plans
* improve ability to change class of dom element
* add a `dom` object to each class. Like `this.dom.whatever` so that it's clear the dom is being referenced. (may or may not do this?)
* `manager.set_classes()` removes non-jdomy classes
* rename `jdom` to `jclass` ??? Maybe, maybe not.
* all class names are removed from the dom element except the registered JS class... needs fixing!
