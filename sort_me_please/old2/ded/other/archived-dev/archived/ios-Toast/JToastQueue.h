//
// Free. No warranty. Do whatever you want with it.
// Created by Jakar in 2014
//

#import <Foundation/Foundation.h>
#import "JToast.h"

@interface JToastQueue : NSObject

+ (JToastQueue *)sharedToastQueue;


+(void)queueToastWithMessage:(NSString *)message forToastDuration:(ToastDuration)duration atPosition:(ToastPosition)position;
+(void)queueToastWithMessage:(NSString *)message forFloatDuration:(CGFloat)duration atPosition:(ToastPosition)position;
+(void)queueToastWithMessage:(NSString *)message;
+(void)removeToast:(JToast *)toast ifDisplayed:(BOOL)ifDisplayed;
- (void)showToastImmediately:(JToast *)toast;
- (void)queueToast:(JToast *)toast; //this will show it immediately if queue is empty


@property (nonatomic) BOOL allowDuplicates;
@property (nonatomic, strong)NSMutableArray *stack;

@end
