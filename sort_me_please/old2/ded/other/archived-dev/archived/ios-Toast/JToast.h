//
// It's free. No warranty. Do whatever you want with it. 
// Created by Jakar... sometime in 2014
//

#import <UIKit/UIKit.h>
//JUIView comes from https://github.com/JakarCo/JUIView
#import "JUIView.h"
//M comes from https://github.com/JakarCo/ios-util
#import "M.h"

typedef enum {
    ToastPositionTop,
    ToastPositionCenter,
    ToastPositionBottom
} ToastPosition;

typedef enum {
    ToastDurationCalculated,//time based on 200 words per minute avg of 5 letters per word & 1 space (except for first word). Minimum is TOAST_LENGTH_SHORT, which currently equals 2 -- ((message/6)+1)/200/60 -- (200/60 words/sec)
    ToastDurationCalculatedLong,
    ToastDurationShort,
    ToastDurationMedium,
    ToastDurationLong
} ToastDuration;

@interface JToast : NSObject {
    

    
//    double animLength;
    
}

extern const CGFloat TOAST_TRANSITION_LENGTH;


@property (nonatomic, readonly) CGFloat animLength;
@property (nonatomic, strong) NSTimer *timer;

-(JToast *)toastWithMessage:(NSString *)message forFloatDuration:(CGFloat)duration atPosition:(ToastPosition)position;
-(JToast *)toastWithMessage:(NSString *)message forToastDuration:(ToastDuration)duration atPosition:(ToastPosition)position;
-(id) initInParentView:(UIView *)parent;
-(id) initInTopViewFromChild:(UIViewController *)child;
-(id) initWithTopWindow;
-(void) showSelf;
-(void) hideSelf;

@end
