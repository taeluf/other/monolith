//
//  JToastQueue.m
//  
//
//  Free. No warranty. Do whatever you want with it
//  Created by Jakar in 2014
//

#import "JToastQueue.h"
//NSTimer+Blocks comes from https://github.com/jivadevoe/NSTimer-Blocks
#import "NSTimer+Blocks.h"

@interface JToastQueue (){
    
}

//@property (nonatomic)BOOL allowDuplicates;

@end

@implementation JToastQueue

static JToastQueue *queue;
//static BOOL allowDuplicates;
//NSMutableArray *stack = _stack;

+(void)initialize{
    if (queue==nil){
        queue = [[self alloc] init];
        queue.stack = [[NSMutableArray alloc] init];
        queue.allowDuplicates =  NO;
    }
}

+(JToastQueue *)sharedToastQueue{
    return queue;
}

+(void)queueToastWithMessage:(NSString *)message{
    [JToastQueue queueToastWithMessage:message forToastDuration:ToastDurationCalculated atPosition:ToastPositionCenter];
}
+(void)queueToastWithMessage:(NSString *)message forToastDuration:(ToastDuration)duration atPosition:(ToastPosition)position{
    [queue queueToast:[[[JToast alloc] initWithTopWindow] toastWithMessage:message forToastDuration:duration atPosition:position]];
}
+(void)queueToastWithMessage:(NSString *)message forFloatDuration:(CGFloat)duration atPosition:(ToastPosition)position{
    [queue queueToast:[[[JToast alloc] initWithTopWindow] toastWithMessage:message forFloatDuration:duration atPosition:position]];
}
+(void)removeToast:(JToast *)toast ifDisplayed:(BOOL)ifDisplayed{ //if displayed means clear even if it's shown on the screen now.
    if (toast.timer==nil||ifDisplayed){
        if (![toast.timer isValid])return;
        [toast.timer invalidate];
        toast.timer = nil;
        [queue removeToast:toast];
        [toast hideSelf];
    }
}

- (void)queueToast:(JToast *)toast{
    if (toast==nil) return;
    BOOL doQueue = YES;
    if (!self.allowDuplicates){
        for (JToast *t in self.stack) {
            if ([t isEqual:toast]){
                doQueue = NO;
                break;
            }
        }
    }
    if (!doQueue)return;
    [self.stack addObject:toast];
    if (self.stack.count == 1){
        [self showToastImmediately:toast];
    }
}
-(void)removeToast:(JToast *)toast{
    for (int i = 0; i<self.stack.count;i++) {
        JToast *t = self.stack[i];
        if (t == toast){
            [self.stack removeObjectAtIndex:i];
            break;
        }
    }
    [toast hideSelf];
    if (self.stack.count>0){
        [NSTimer scheduledTimerWithTimeInterval:TOAST_TRANSITION_LENGTH block:^{
           [self showToastImmediately:self.stack[0]];
        }repeats:NO];
    }
}
- (void) showToastImmediately:(JToast *)toast{
    [toast showSelf];
    toast.timer = [NSTimer scheduledTimerWithTimeInterval:toast.animLength block:^{[self removeToast:toast];} repeats:NO];//remove the toast
}


@end
