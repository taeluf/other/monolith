# Archiving
I still think this lib is pretty darn cool. But I wrote it years ago and am no longer doing iOS. I will not maintain or update it. I don't even know if it works with current software.

# JToast
Android-style Toast messages for iOS

The `NSTimer+Blocks` is from https://github.com/jivadevoe/NSTimer-Blocks 

To use do something like:
```
[JToastQueue queueToastWithMessage:@"Data downloaded" 
             forFloatDuration:1.5f 
             atPosition:ToastPositionBottom];
//or
[JToastQueue queueToastWithMessage:@"Data downloaded" 
             forToastDuration:ToastDurationShort 
             atPosition:ToastPositionBottom];
```
