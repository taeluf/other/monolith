<?php


namespace ROF\Permissions;

class Permissions {
  
  static protected $configured;
  static protected $pdo;
  static protected $defaultUserLevel = self::ACCESS_NO_USER;

  
  const ACCESS_NO_USER = 10001;
  const ACCESS_USER = 15001;
  const ACCESS_ADMIN = 20001;
  
  const TABLE_NAME = "permissions";
  const CREATE_STRING = "CREATE TABLE ".self::TABLE_NAME." ( `id` INT NOT NULL AUTO_INCREMENT , `user_id` INT NOT NULL , `access_level` INT NOT NULL , PRIMARY KEY (`id`), UNIQUE `user_id` (`user_id`)) ENGINE = MyISAM;";
  
  //this will return true/false
  // if the user is not yet in the database, then it will be assigned the access level provided by defaultUserLevel (this is a bad way to do it)
  static public function hasAccess($requesterId,$levelRequired=self::ACCESS_USER){
    if ($requesterId===FALSE||$requesterId===NULL||!is_numeric($requesterId)){
      if ($levelRequired==self::ACCESS_NO_USER)return TRUE;
      return FALSE;
    }
    $pdo = self::$pdo;
    $defaultUserLevel = self::$defaultUserLevel;
    $hasAccess = self::$pdo->prepare("SELECT * FROM ".self::TABLE_NAME." WHERE user_id=:user_id");
    $hasAccess->execute(
      array(
        ":user_id" => $requesterId,
      )
    );
    $all = $hasAccess->fetchAll();
    $hasAccess->closeCursor();
    if (count($all)==1){
      $userLevel = $all[0]['access_level'];
      if ($userLevel>=$levelRequired){
        return TRUE;
      } else {
        return FALSE;
      }
    } else if (count($all)>1){
      throw new \Exception("There is an error in checking permissions. Multiple rows were returned for user id '{$requesterId}'");
    }
    $saveDefault = $pdo->prepare("INSERT INTO ".self::TABLE_NAME."(user_id,access_level) VALUES(:user_id, :access_level)");
      $success = $saveDefault->execute(
        array(":user_id" => $requesterId,
             ":access_level" => $defaultUserLevel)
      );
      $saveDefault->closeCursor();
    if ($defaultUserLevel>=$levelRequired){
      return TRUE;
    } else {
      return FALSE;
    }
  }

  //this will display an error if access is not granted. No return value
  static public function access($requesterId,$levelRequired=self::ACCESS_USER){
    if (!self::hasAccess($requesterId,$levelRequired,$defaultUserLevel)){
      self::showError("You do not have access to this page.");
      return FALSE;
    } else {
      return TRUE;
    }
  }
  
  //this will display an error to the end-user by simply echoing after setting the header with the appropriate error code
  static public function showError($errorMessage,$errorCode=403){
    header("HTTP/1.0 403 Forbidden");
    echo $message;
  }
  
  //this will create 
  static public function makeDatabase(){
    $pdo = self::$pdo;
    
    $createTable = $pdo->prepare(self::CREATE_STRING);
    $success = $createTable->execute();
    $createTable->closeCursor();
    if ($success){
//       echo "DB Table was created successfully!";
//       exit;
      return;
    }
    throw new \Exception("The mysql permissions table could not be created for RO\Permissions package. Please create table manually");
  }

  static public function configure(){
    if (self::$configured==TRUE) return;
    $configPath = realpath(__DIR__.'/../../../../config/ROFPermissionsConfig.php');
    if (!file_exists($configPath)){
      throw new \Exception("SITE_ROOT/config/ROFPermissionsConfig.php does not exist, therefore database functions cannot be performed. Please create ROFPermissionsConfig.php");
    }
    require_once($configPath);
    if (isset($pdo))self::$pdo = $pdo;
    if (!(self::$pdo instanceof \PDO)){
      throw new \Exception("ROF\Permissions config error. There must be PDO object assigned to variable \$pdo in your SITE_ROOT/config/ROFPermissionsConfig.php");
    }
    if (isset($defaultUserLevel)){
      self::$defaultUserLevel = $defaultUserLevel;
    }
    
    try {
      $checkForTable = $pdo->prepare("SELECT COUNT(*) FROM ".self::TABLE_NAME);
      $exists = $checkForTable->execute();
      $checkForTable->closeCursor();
    } catch (\Exception $e) {
      $exists = FALSE;
      $checkForTable->closeCursor();
    }
    if (!$exists){
      self::makeDatabase();
    }
  }
  
}

Permissions::configure();

?>
