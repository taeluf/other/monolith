# PHP-Permissions
A very simple permissions library, intended for managing access to pages, but capable of more.  
It's a gross library. It functions for very very basic purposes and is simple to use. It will be better eventually. And hopefully get simpler.

# ARCHIVE Oct 14, 2019
Definitely something I want to have and use, but I'm not currently ready to make it as an abstracted library. I'm still working on my user library and its hard enough to write that with permissions, let alone abstract them away from one another. Ugh. Eventually, Permissions may be separate from Users. IDK.

# Installing with composer
```
"require": {
    "rof/permissions": "dev-master"
}
```

# Installing manually
Copy the Permissions.php to `SITE_ROOT/vendor/ROF/Permissions/Permissions.php`

# Installing, configuration
Copy ROFPermissionsConfig.php to `SITE_ROOT/config/ROFPermissionsConfig.php`  
Put in the details for your PDO connection.  
Set the `$defaultUserLevel` to `\RO\Permissions\Permissions::ACCESS_NO_USER`, `ACCESS_USER`, or `ACCESS_ADMIN`  

# Usage
    $requiredLevel = Permissions::ACCESS_ADMIN
    $hasAccess = Permissions::hasAccess($someUserId,$requiredLevel)
    if (!$hasAccess){echo "You don't have access!!!"; exit}

To change the permission level for a user, at this time, you must change the access level in the permissions table of your database. 
The permissions table will be created the first time you call hasAccess();
The values are 10001 for `ACCESS_NO_USER`, 15001 for `ACCESS_USER`, and 20001 for `ACCESS_ADMIN`


# other stuff
Here are the plans:

    • v1 Permissions
        ◦ Class Constants
            ▪ ACCESS_NO_USER, ACCESS_USER, ACCESS_ADMIN
            ▪ spread out values by a few hundred so I can place in-betweens later if need be. 
            ▪ Group prefixes into number ranges
            TABLE_NAME
            CREATE_STRING
        ◦ Static Variables
            ▪ $pdo
              $configured
        ◦ Static functions
            ▪ v1 bool hasAccess($user→id,$requiredLevel=ACCESS_USER,$defaultUserLevel=ACCESS_USER)
                • checks if has access and returns true/false
            ▪ bool access($user→id, $requiredLevel=ACCESS_USER, $defaultUserLevel=ACCESS_USER)
                • checks if hasAccess and calls  showError if not
            ▪ null showError($errorMessage,$errorCode=403)
                • Stay on same page. Deliver a 403 error code by default, and display the message.
            ▪ Throws makeDatabase()
                • This creates the database and is called from the class page after class declaration
                • Before creating the database, it checks to see if it is already created. Or calls it with IF NOT EXISTS
              configure()
                 - loads a file from site_root/config/PermissionsConfig.php
                 - that file needs to specify a pdo object
        ◦ Database Tables
            ▪ Users
                • user_id, access_level


Currently, different user access levels will need to be set manually in the database.
At a later time, there should be a function to set the access level of a user.
Then after that, there should be a user-manager page to manage user's access levels.Then

Eventually, changing this to a roles based permission system would be ideal.
