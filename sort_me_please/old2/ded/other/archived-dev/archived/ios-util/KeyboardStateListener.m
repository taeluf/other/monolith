//
//  KeyboardStateListener.m
//  Free. No warranty. Do what you want with it.
//  Created by Jakar in 2014
//

#import "KeyboardStateListener.h"
//See https://github.com/JakarCo/JToast for JToast.h and JToastQueue.h
#import "JToast.h"
#import "JToastQueue.h"
//see https://github.com/JakarCo/JUIView for JUIView
#import "JUIView.h"

@interface KeyboardStateListener()


@end

static KeyboardStateListener *sharedInstance;

@implementation KeyboardStateListener

+ (KeyboardStateListener *)sharedInstance
{
    return sharedInstance;
}

+ (void)load
{
    @autoreleasepool {
        sharedInstance = [[self alloc] init];
    }
}

- (BOOL)isVisible
{
    return _isVisible;
}

- (void)willShow:(NSNotification *)aNotification
{
    _isVisible = YES;
    NSDictionary* info = [aNotification userInfo];
    _keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    _keyboardSize = [JUIView resizeForOrientation:_keyboardSize];
}

- (void)willHide:(NSNotification *)aNotification
{
    _isVisible = NO;
    _keyboardSize = CGSizeMake(0, 0);
}

- (id)init
{
    if ((self = [super init])) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self selector:@selector(willShow:) name:UIKeyboardWillShowNotification object:nil];
        [center addObserver:self selector:@selector(willHide:) name:UIKeyboardWillHideNotification object:nil];
    }
    return self;
}

@end
