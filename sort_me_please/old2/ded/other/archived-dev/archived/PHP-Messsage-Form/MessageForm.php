<?php

namespace ROF;


class MessageForm {
  
  //OBJECT variables
  static protected $beganSession = FALSE;
  static protected $sessionKeys = array("title"=>"ro-login-title",
                       "message" => "ro-login-message",
                       "links" => "ro-login-links",
                       "time" => "ro-login-message-time");
  
  static public function displaySession(){
    $active = self::getCurrent();
    $active->display();
  }
  //STATIC functions
  //
  static public function getCurrent(){
    self::startSession();
    $validSession = TRUE;
    foreach (self::$sessionKeys as $name=>$key){
      if (!isset($_SESSION[$key])){
        $validSession = FALSE;
      }
    }
    if (!$validSession){
      $message = new MessageForm("Session Error", 
                                 "There is no active session, so a message cannot be displayed. "
                                  );
    }
    if (time()-5*60>$_SESSION[self::getKey('time')]){
      $message = new MessageForm("Session Ended", 
                                 "There may have been an error message previously, but it is no longer available. "
                                );
      return $message;
    }
    $message = new MessageForm($_SESSION[self::getKey('title')],
                              $_SESSION[self::getKey('message')],
                              $_SESSION[self::getKey('links')]
                              );
    return $message;
  }
  static private function startSession(){
    if (session_status()===PHP_SESSION_DISABLED){
      throw new \Exception("Session could not be started due to PHP settings, so messages cannot be displayed.");
    } else if (session_status()===PHP_SESSION_NONE){
      $started = session_start();
      if (!$started)throw new \Exception("Session failed to start so message could not be displayed.");
      
      self::$beganSession = TRUE;
    }
    $_SESSION[self::getKey('time')] = time();
  }
  static private function endSession(){
    self::startSession();
    foreach (self::$sessionKeys as $key=>$value){
      if (isset($_SESSION[$value])){
        unset($_SESSION[$value]);
      }
    }
    if (self::$beganSession)session_destroy();
  }
  static private function getKey($keyName){
    if (!isset(self::$sessionKeys[$keyName])){
      throw new \Exception("Key '{$keyName}' is not a valid session variable for RO-User library.");
    }
    return self::$sessionKeys[$keyName];
  }
  
  /**
  * $title and $message are strings
  * $links is an associative array where the key is the title for the user and the value is the url that it directs the user to.
  */
  public function __construct($title,$message,$links=[]){
    //INTEGRITY check
    //
    if (!is_array($links))throw new \Exception("The 3rd value, '\$links', must be an array with key set to the user-friendly "
                                               ."title and value set to the url the link goes to.");
    self::startSession();
    $_SESSION[self::getKey('title')] = $title;
    $_SESSION[self::getKey('links')] = $links;
    $_SESSION[self::getKey('message')] = $message;
  }
  
  //OBJECT, PROTECTED methods
  //
  protected function getTitle(){
    return $_SESSION[self::getKey('title')];
  }
  protected function getMessage(){
    return $_SESSION[self::getKey('message')];
  }
  protected function getLinks(){
    return $_SESSION[self::getKey('links')];
  }
  protected function getLinksHtml(){
    $links = $this->getLinks();
    $html = "";
    foreach ($links as $name=>$url){
      $html .= "<a href=\"{$url}\">{$name}</a><br>";
    }
    return $html;
  }
  
  public function display(){
  ?>
  <h1><?=$this->getTitle()?></h1>
<p>
  <?=$this->getMessage()?>
</p>
<p>
  <?=$this->getLinksHtml()?>
</p><?php
    self::endSession();
  }
  public function redirect($messageFormUrl='/message.php'){
    if ($messageFormUrl=='/message.php'){
      $protocol = isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1) || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http';
      $domain_name = $_SERVER['HTTP_HOST'];
      $site_address = $protocol."://".$domain_name;
      $messageFormUrl = $site_address.$messageFormUrl;
    }
    header("Location: ".$messageFormUrl);
    exit;
  }
}
