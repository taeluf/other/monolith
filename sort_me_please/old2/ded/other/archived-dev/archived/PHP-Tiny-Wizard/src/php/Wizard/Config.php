<?php

namespace ROF\Tiny\Wizard;

class Config extends \ROF\Tiny\Config {


    public $pages = [
        0 => 'index',
        1 => 'start',
        2 => 'test',
        3 => 'showtest',
    ];


/* These are default configurations. 
* You don't need to change any of them, but you can. You may want to view the documentation if you're editing these.
* 
* For performance's sake, you should set up the categories array and set autoCategorize to false
* To get the default category configuration array, call `echo (ROF\Tiny\Wizard\Config::getInstance())->exportCategories(TRUE);` 
        to run the auto-categorization and get its var declaration
*    then copy that into this class and delete your existing `$categories` and `$autoCategorize` settings (since they're in the export)
*
    public $defaultCategory = '*'; //this may be ignored. if the default category doesn't exist, then it will use the first category in the configured categories array
    public $autoCategorize = TRUE; //maps your view folder to a set of categories. Not very performant
    public $categories = [];
    public $requiredConfigs = [];
*/

/* if you want to implement your own permissions class, this is where you do it
*  or if you wish to add any default configurations which require logic or instantiation
*
    public function __construct(){
        parent::__construct();
        $this->permissions = new \ROF\Tiny\YesPermissions();
    }
*/

}

?>