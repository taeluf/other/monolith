<?php

namespace ROF\Tiny;

/** This is your business logic class. Here only for example's sake. You should delete and replace it.
 * 
 */
class Wizard {

    public $config;
    public $sessionKey; 

    public function __construct($sessionKey){
        $this->sessionKey = $sessionKey;
        $this->config = \ROF\Tiny\Wizard\Config::getInstance();
        $this->data = isset($_SESSION[$sessionKey]) ? $_SESSION[$sessionKey] : [];
    }
    
    public function set($key,$value){
        $_SESSION[$this->sessionKey][$key] = $value;
    }

    public function start(){
        $this->set('started',TRUE);
    }
    public function setTestValue(){
        if ($this->isActive()){
            $_SESSION[$config->sesionKey] = array_merge_recursive($_SESSION[$config->sessionKey],$_POST);
        }

    }
    public function isActive(){
        if (isset($this->config->sessionKey)
            &&isset($_SESSION[$this->config->sessionKey])){
                return TRUE;
        } else {
            return FALSE;
        }
    }

    public function updateSession(){
        $_SESSION[$this->sessionKey] = array_merge_recursive($this->data,$_POST);
    }
}


?>