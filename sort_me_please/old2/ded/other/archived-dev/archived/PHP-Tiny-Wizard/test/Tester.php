<?php

namespace ROF\Tiny\Wizard;

class Tester {

    private $config;
    private $router;
    private $loader;

    public function __construct(){
        $this->prepare();
    }

    public function prepare(){
        $this->config = \ROF\Article\Config::getInstance();
        $this->router = $this->config->router;
        $this->loader = $this->config->loader;
    }

    public function runAll(){
        echo '<pre>'."\n";
        $methods = get_class_methods($this);
        foreach ($methods as $method){
            if ($method=='runAll'||$method=='prepare'||$method=='__construct')continue;
            $mo = $method;
            while (strlen($mo)<30){
                $mo .='-';
            }
            echo "<b>{$mo}:</b> ";
            echo $this->$method() ? 'true' : 'false';
            echo "<br>\n";
        }
        echo "\n</pre>";
    }
    public function testIsActive(){
        $_SESSION['abde1234'] = 'session data';
        $config = \ROF\Tiny\Wizard\Config::getInstance(
            ['sessionKey'=> 'abde1234']
        );
        $wizard = $config->loader->getObjectFromSlug();
        if ($wizard->isActive()){
            return TRUE;
        } else {
            return FALSE;
        }
    }

}





?>