<?php

namespace ROF\Request;

class Configuration implements ConfigurationInterface {
    
    private $viewController = NULL;
    private $autoloadDirectories = array('Class');
    private $siteRoot = NULL;
    
    public function __construct($documentRoot){
      $this->siteRoot = $documentRoot;
      $this->viewController = new SimpleViewController();
      $this->configure();
    }
    
    public function getSiteRoot(){
        return $this->siteRoot;
    }
    public function getViewController(){
        return $this->viewController;
    }
    public function getDirectoryIndex(){
        return "home.php";
    }
    public function getPublicDir(){
        return "/Public";
    }
    public function getAutoLoadDirectories(){
        return $this->autoLoadDirectories;
    }
    
    public function setSiteRoot($path){
        $this->siteRoot = $path;
    }
    
    public function setViewController($viewController){
        if (!($this->doesInherit($viewController, 'ViewControllerInterface'))){
            throw new \Exception("The passed object does not inherit ViewControllerInterface");
        }
        $this->viewController = $viewController;
    }
    

   public function configure(){
    \ROF\Autoloader::enable($this->siteRoot.'/Class');
  }
  
  private function doesInherit($object,$interfaceName){
      return true;
      return false;
  }
}


?>
