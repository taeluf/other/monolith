<?php 

namespace ROF\Request;

interface ConfigurationInterface {
    
    public function getViewController();
    public function getDirectoryIndex();
    public function getPublicDir();
}





?>
