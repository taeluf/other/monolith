<?php

namespace ROF\Request;

class SimpleViewController implements ViewControllerInterface {
    
    public $content;
    
    public function __construct(){
        
    }
    public function load($content){
        $this->content = $content;
    }
    public function display(){
        echo $this->content;
    }
}



?>
