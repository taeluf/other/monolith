# PHP Request Router
Routes URL Requests through registerable events, allowing custom URL & content handling. Includes a default URL Handler, which delivers content.  

# ARCHIVE Oct 14, 2019
This is being archived in favor of my PHP-Tiny library. This one has not been worked on or used in a short while. The code base is messy and its probably in a state where a total re-write would be needed

--

The goal of this is to make website delivery simple and configurable, without providing or requiring a vast set of functionality. This is to have minimal dependencies upon other libraries.

There are two important parts to this. The **Router** and the **Handler**. The Router provides an event-based mechanism for handling a request. The Handler responds to some of those events. The Router calls the Handler, and the Handler outputs content.

## Sidenote
This currently works for my needs. It may work for your needs, but it is not very configurable, nor is it well tested. I would like to further develop this library (and I very well may), but at this time (May 14, 2019) I am putting it down except for necessary changes for my purposes.

I have been developing this router to use for my personal websites. I think the readme needs some work, but I'm just exhausted of working on it. So I'm going to move on to my other projects and let this one sit for now, as it currently fits my needs. Sorry! 

## About
### What happens during a request?
1.) A user submits a request to ReedOverflow.com/cats
2.) The included `.htaccess` redirects the request to the included `/start_request.php`
3.) `/start_request.php` contains basic configuration and initiatees the routing of the request.
4.) Configuration is verified, to make sure necessary settings have been set. Instructive errors are thrown if not.
5.) The file `willDeliver.php` (in a configurable location) is included. This file is where events can be registered to, as well as all other setup.
6.) Loops through registered URL Handlers. 
   - First calls a registered `willHandleUrl` function, returning TRUE or FALSE. If TRUE, no other Handlers will be used. if FALSE, proceeds to next handler.
   - Then calls a registered `handleUrl` function, which returns nothing and will output content.
7.) Loops through registered Content Handlers, such as a templating library
   - first calls a registered `willHandleContent` function, returning TRUE or FALSE. If TRUE, no other handlers will be used. If FALSE, proceeds to next handler.
   - Then calls a registered `handleContent` function, which returns nothing and will output content, likely a modification of the previous content.

That's all for now. There may be more events added at a later time.

### What happens inside the built-in Handler?
The built-in handler does NOT process content through `handleContent` but outputs content on `handleUrl`. It handles ALL urls with default setup and should be the last registered Handler.
1.) Redirects to an all lowercase version of the URL (if it contains capitals)
2.) Figures the file type - request for a resource, a directory, or a php file
3.) Rewrites the URL as needed 
   - Resource: No rewrite
   - Directory: Force trailing slash. 
   - PHP file: Redirect to parent if it's a request for an index file (so only the directory name shows). Remove extension. Remove trailing slash. 
4.) Routes the URL as needed. Based upon the default settings:
   - Resource: Prepend path with `siteSource` (config option). Then: `Style/path/to/style.css`, `Script/path/to/script.js`, `Res/path/to/something\_else.pdf`
   - Directory: Concats `siteSource` to `publicDir` to requested url to `indexFile` (`code` is configuration options)
   - PHP file: Concats `siteSource` with `publicDir` with the requested url with `.php`
5.) Includes the file that's being pointed to
Currently, it is very opinionated and minimally configurable. Later, it will be equally opinionated, but much more open minded (configurable).


## How To
### Register URL Handlers
