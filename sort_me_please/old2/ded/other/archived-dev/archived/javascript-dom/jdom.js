/*
* Make sure to either include this code AFTER all your jdom functions
* or remove `JDOMProcessor.pageLoaded();` from the bottom of this file 
* and put it somewhere you KNOW it will be loaded after your jdom functions
*
* I can't, at this time, guarantee execution order of your jdom functions
*/

/** I'm enclosing so that function names don't conflict with user's functions
*
*/
var JDOMProcessor = {
	/**returns string with first char capital and the rest lower case
	*/
	capitaliseFirstLetter : function(string)
	{
		return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
	},
	/**returns class list without the jdom class
	*/
	classList : function(element){
		var classList = element.className;
		var array = classList.split(" ");
		for(var i = 0; i <array.length; i++) {
			if(array[i] == 'jdom') {
			   array.splice(i, 1);
			}
		}
		return array;
	},
	/** the page has finished loading. This maps everything
	*/
	pageLoaded: function(logFuncNames){
	  logFuncNames = logFuncNames || false;
		var elementList = document.getElementsByClassName('jdom');	
		for (var i = 0;i<elementList.length;i++){
			var element = elementList[i];
			var classList = this.classList(element);
			var parent = element.parentNode;
			var classListString = classList.join('_');
			var parentName = this.capitaliseFirstLetter(parent.tagName);
			var elementName = this.capitaliseFirstLetter(element.tagName);
			var didFunc = false;
			var tagIndex = 0;
			var classes = classList.length;
			while (!didFunc&&classes>0){
				var tagNames = [parentName,elementName,'' ];
				var funcName = 'jdom'+tagNames.slice(tagIndex).join('')+"_"+classList.slice(0,classes).join("_");
				
				var func = window[funcName];
				if (func!=null){
					func(parent,element);
					didFunc = true;	
				}
				
				if (++tagIndex>tagNames.length){
					classes = --classes;
					tagIndex = 0;
				}
				if (logFuncNames)console.log(funcName);
			}
		}
	}
}
//you can remove this call from the file and place it somewhere on your own if you'd like.
JDOMProcessor.pageLoaded();
