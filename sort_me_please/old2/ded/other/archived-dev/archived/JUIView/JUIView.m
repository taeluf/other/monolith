//
//  Free. No warranty. Do whatever you want with it.
// Created by Jakar in 2014
//

#import "JUIView.h"

//M comes from https://github.com/JakarCo/ios-util
#import "M.h"

@implementation JUIView


+ (void)centerView:(UIView *)view withinView:(UIView *)parentView withPositioning:(CenterPosition)position{
    CGRect parentFrame = view.superview.bounds;
    CGFloat x = view.center.x;
    CGFloat y = view.center.y;
    BOOL doBreak = YES;
    switch (position){
        case CenterPositionInBounds://this has to stay as the first case
            doBreak = NO;
        case CenterPositionHorizontally:
            x = parentFrame.size.width/2;
            if (doBreak)break;
        case CenterPositionVertically:
            y = parentFrame.size.height/2;
            if (doBreak)break;
    }
    
    view.center = CGPointMake(x,y);
}
+(void)addView:(UIView *)subview toView:(UIView *)parent andCenterWithPosition:(CenterPosition)position{
    [JUIView centerView:subview withinView:parent withPositioning:position];
    [parent addSubview:subview];
}
+(CGSize)sizeFromString:(NSString *)string usingFont:(UIFont *)font withMaxWidth:(CGFloat)width{
    NSString *text = string;
    NSAttributedString *attributedText =
    [[NSAttributedString alloc]
     initWithString:text
     attributes:@
     {
     NSFontAttributeName: font
     }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize returnRect =CGSizeMake(ceilf(rect.size.width), ceilf(rect.size.height));
    return returnRect;
}
+ (void)transitionWithWindow:(UIWindow *)window toStoryBoardNamed:(NSString *)storyboardName{
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{window.rootViewController = [JUIView storyBoardWithName:storyboardName]; }
                    completion:nil];
    
}

+(NSString *)storyBoardNameWithName:(NSString *)name{
    if (isPad()){
        return[NSString stringWithFormat:@"%@_pad", name];
    } else {
        return [NSString stringWithFormat:@"%@_phone",name];
    }
}
+(UIViewController *)storyBoardWithName:(NSString *)name{
    return [[UIStoryboard storyboardWithName:[JUIView storyBoardNameWithName:name] bundle:nil] instantiateInitialViewController];
}
UIViewController *lastController;
+(void)goBack{
    if (lastController!=nil)
        [[UIApplication sharedApplication].delegate window].rootViewController = lastController;
}
+(UIViewController *)storyBoardWithName:(NSString *)storyName andStartingViewName:(NSString *)viewName withReturningView:(UIViewController *)returningView{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[JUIView storyBoardNameWithName:storyName] bundle:nil];
    UIViewController *initial = [storyboard instantiateInitialViewController];
    lastController = returningView;
    if ([initial isKindOfClass:[UINavigationController class]]){
        [((UINavigationController *)initial) pushViewController:[storyboard instantiateViewControllerWithIdentifier:viewName] animated:NO];
        if (returningView!=nil){
            lastController = returningView;
            UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:[M class] action:@selector(goBack)];
            ((UINavigationController *)initial).topViewController.navigationItem.backBarButtonItem = backButton;
        }
        return initial;
    } else
        return [storyboard instantiateViewControllerWithIdentifier:viewName];
}

+(CGSize)resizeForOrientation:(CGSize)size{
    if (UIDeviceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
    {
        size = CGSizeMake(size.height, size.width);
    }
    return size;
}

@end
