//
//  Free. No warranty. Do what you want with it.
// Created by Jakar in 2014
//

#import <UIKit/UIKit.h>

typedef enum {
    CenterPositionHorizontally,
    CenterPositionVertically,
    CenterPositionInBounds
} CenterPosition;

@interface JUIView : NSObject

+(void)centerView:(UIView *)view withinView:(UIView *)parentView withPositioning:(CenterPosition)position;
+(void)addView:(UIView *)subview toView:(UIView *)parent andCenterWithPosition:(CenterPosition)position;
+(CGSize)sizeFromString:(NSString *)string usingFont:(UIFont *)font withMaxWidth:(CGFloat)width;
+ (void)transitionWithWindow:(UIWindow *)window toStoryBoardNamed:(NSString *)storyboardName;
+(CGSize)resizeForOrientation:(CGSize)size;
+(UIViewController *)storyBoardWithName:(NSString *)name;
+(UIViewController *)storyBoardWithName:(NSString *)storyName andStartingViewName:(NSString *)viewName withReturningView:(UIViewController *)returningView; //you may pass nil for returningView

@end
