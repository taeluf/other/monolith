# Archived
I've copied the head file to the readme, in case it's of interest. I made this years ago when I was doing iOS development. I will not maintain or update this code.

# JUIView
Convenient view utitlities

    typedef enum {
        CenterPositionHorizontally,
        CenterPositionVertically,
        CenterPositionInBounds
    } CenterPosition;

    @interface JUIView : NSObject

    +(void)centerView:(UIView *)view withinView:(UIView *)parentView withPositioning:(CenterPosition)position;
    +(void)addView:(UIView *)subview toView:(UIView *)parent andCenterWithPosition:(CenterPosition)position;
    +(CGSize)sizeFromString:(NSString *)string usingFont:(UIFont *)font withMaxWidth:(CGFloat)width;
    + (void)transitionWithWindow:(UIWindow *)window toStoryBoardNamed:(NSString *)storyboardName;
    +(CGSize)resizeForOrientation:(CGSize)size;
    +(UIViewController *)storyBoardWithName:(NSString *)name;
    +(UIViewController *)storyBoardWithName:(NSString *)storyName andStartingViewName:(NSString *)viewName withReturningView:(UIViewController *)returningView; //you may pass nil for returningView

    @end
