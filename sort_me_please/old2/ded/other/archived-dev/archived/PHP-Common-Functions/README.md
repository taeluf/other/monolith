# PHP-Common-Functions
A developing set of functions that are commonly used. This is primarily intended for personal use.


# ARCHIVE Oct 14, 2019
This lib is fine, I guess. But I don't think I need it. Until I start putting together more useful sets of functions, I'm just giong to leave this one off and keep my common functions in a less git-friendly way.


Current Features:
* `strStartsWith($string, $beginningStr` - returns TRUE/FALSE
* `getPdo($initString, $user, $password)` - returns a static PDO object, which will be initialized once. If you have initialized it once, then you can simply call `getPdo()`


Plans:
* I dunno.
