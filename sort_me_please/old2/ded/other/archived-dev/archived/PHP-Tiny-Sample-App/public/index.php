This is a sample page for url '/'. 
<br>
There may be a 'base-url' which precedes any url to this particular (sample) app.
<br><br>
The test_script should show some commented html as well, but you have to view source.



<?php

echo "\n\$this->slug: ".$this->slug;
echo "\n\$this->viewName: ".$this->viewName;
echo "\n\$this->action: ".$this->action;

echo "\n\$this->get('sample-site-tagline'): ".$this->get('sample-site-tagline');

echo "\n\n Let's test pdo:\n<br><br>";

echo "<pre>\n";
$pdo = $this->get('db-pdo');
$qs = "SELECT SUM(87 + 64) AS onefiftyone";
$sm = $pdo->prepare($qs);
$sm->execute();
$res = $sm->fetchAll();
echo "Query:\n";
echo $qs."\n";
echo "Result:\n";
print_r($res);
echo "Error:\n";
print_r($sm->errorInfo());

echo "\n</pre>";
?>