I think this just allows for an arbitrary number of columns & should be responsive.

```html
<div class="FlexColumnContainer">
    <div class="FlexColumn">one</div>
    <div class="FlexColumn">two</div>
    <div class="FlexColumn">three</div>
    <div class="FlexColumn">four</div>
    <div class="FlexColumn">five</div>
</div>
```
basically