
document.addEventListener("DOMContentLoaded", function() {
var object = document.querySelector('object[type="application/pdf"]');
object.style.display = 'none';
const url = object.getAttribute('data');
var loading = document.createElement('h3');
loading.innerText = "PDF File may take a minute to load.";
object.parentNode.insertBefore(loading,object);
var loadingTask = pdfjsLib.getDocument(url);
    loadingTask.promise.then(function(pdf) {
        const count = pdf._pdfInfo.numPages;
        for (let i=0;i<=count;i++){
            pdf.getPage(i).then(
                function(page) {
                    loading.parentNode.removeChild(loading);
                    var canvas = document.createElement('canvas');
                    // canvas.width = "100%";
                    // canvas.height= "100%";
                    object.parentNode.insertBefore(canvas,object);
                    var pageheader = document.createElement('h4');
                    pageheader.innerText = "Page "+i;
                    canvas.parentNode.insertBefore(pageheader,canvas);

                    var scale = 1.5;
                    var viewport = page.getViewport({ scale: scale, });
                    // console.log(canvas);
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;

                    var renderContext = {
                        canvasContext: context,
                        viewport: viewport
                    };
                    page.render(renderContext);
                }
            );

        }
    });
});
            
