

## A file input
```html
<label><?=$label??'File';?>
    <input type="file" name="<?=$name?>" accept="<?=$types?>">
    <!-- <br> -->
    <!-- <img style="max-width:100%;max-height:100%;" for="<?=$name?>" /> -->
</label>
```

## A file input with image src to show the current image
```html
<label><?=$label??'Image';?>
    <input type="file" name="<?=$name?>">
    <br>
    <img style="max-width:100%;max-height:100%;" for="<?=$name?>" />
</label>
```