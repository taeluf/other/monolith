# WebFrontEndStuffs
This is mainly for personal use, but feel free to use anything in here. If you notice anything that shouldn't be public (like an accidental password), please submit an issue.

Much of my stuff will have code for [Liaison](https://github.com/Taeluf/Liaison), a PHP framework I made