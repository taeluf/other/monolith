# Web views ...

## TODO
- separate `FullPageWithExpandMenu` into it's relevant pieces (basically separate out the menu)
- general cleanup of styles & stuff
- organize the views into categories
- add a liaison app that makes it super easy to include one of these views in a Lia site. 
- Maybe add a really simple PHP class that returns paths to the stylesheets??


## CopyPasta
```html
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta charset="utf-8"  />
      <link rel="stylesheet" type="text/css" href="fullpage.css">
  </head>
  <body>
      <header>header</header>
      <main>
          <div id="content_area">
              some content that is pretty long, so we can show off the wrapping of the content area. 
              some content that is pretty long, so we can show off the wrapping of the content area.
              some content that is pretty long, so we can show off the wrapping of the content area.
              some content that is pretty long, so we can show off the wrapping of the content area.
              some content that is pretty long, so we can show off the wrapping of the content area.
          </div>
      </main>
      <footer>footer</footer>
  </body>
</html>

```
