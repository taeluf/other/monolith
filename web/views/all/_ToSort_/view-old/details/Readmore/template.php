<details class="Readmore">
    <summary>
        <div onclick="event.preventDefault();" ><?=$summary?></div>
        <div onclick="event.preventDefault();" class="open">
            <?=$details?>
        </div>
        <div class="toggle">
            <div><span class="arrow"></span></div>
            <span>
                <span class="more"><?=$more?></span>
                <span class="less"><?=$less?></span>
            </span>
            <div><span class="arrow"></span></div>
        </div>
    </summary>
    
</details>