<?php

require(__DIR__.'/vendor/autoload.php');

$lia = new \Lia();

$main = \Lia\Package\Server::main($lia);

$site = new \Lia\Package\Server($lia, 'site', __DIR__.'/Site/');


$lia->addRoute('/bears/{bearname}/',
    function($route, $response){
        $response->content = 'whatever'.$route->param('bearname');
    }
);

$lia->deliver();
