<?php

namespace Tlf\Tester\Test;

class Idk extends \Tlf\Tester {

    public function testIdk(){
        //input data
        $phrases = <<<TXT
            Robin likes fish
            Robin doesn't like spring onion
            Willie loves fish
            Abigail likes amethyst
            Sebastian likes fish
            Willie likes amethyst
            Willie likes tulip
            Willie likes cats
            Pierre is open 9am-9pm
        TXT;

        $synonyms = [

            'loves'=>[], // likes is NOT a synonym for loves
            'likes'=>['loves'] // but loves is for likes
        ];

        //questions: 
            // Who likes fish?
            // Who doesn't like fish?
            // What does Willie like?
        $lines = explode("\n", $phrases);

        $target = [
            ['actor'=>'Robin', 'verb'=>'likes', 'object'=>'fish'],
            ['actor'=>'Robin', 'verb'=>'doesn\'t like', 'object'=>'spring onion'],
        ];
        foreach ($target as $index=>$row){
            $this->compare($row,
                $this->parse_sentence2(trim($lines[$index]))
            );
        }
    }
    public function parse_sentence2(string $sentence){
        $verbs = [
            'likes'=>['loves'],
            'doesn\'t like'=>[],
            'loves'=>[],
            'is open'=>[],
        ];

        foreach ($verbs as $v=>$synonyms){
            if (!strpos($sentence, $v))continue;
            $split = explode($v, $sentence);
            $parsed = [
                'actor'=>trim($split[0]),
                'verb'=>$v,
                'object'=>trim($split[1])
            ];
            return $parsed;
        }
    }

    public function parse_sentence(string $sentence){
        $split = explode(' ', $sentence);
        $parsed = [
            'actor'=>$split[0],
            'verb'=>$split[1],
            'object'=>implode(' ',array_slice($split,2))
        ];
        return $parsed;
    }

    public function testParseSentence(){
        $sentence = 'Robin likes fish';
        $target = [
            'actor'=>'Robin',
            'verb'=>'likes',
            'object'=>'fish'
        ];

        $parsed = $this->parse_sentence($sentence);
        $this->compare($target,$parsed);
        echo 'beep';
    }
}

