# Natural English DB Language
For reference, see my blog post on taeluf.com


## End of day march 23, 2022
I wrote some extensive notes about my goals, below. I need to take it one step at a time. Not try to implement all the features right away. Some of these may not be feasible. I'll find out by prototyping each successive stage. Each successive stage builds upon the previous stage. I might end up with a completely different schema or a big backend, such as a dictionary of words by verb, noun, etc etc. I don't know what this will be, but I just want to keep playing with it.

# March 23, 2022
I would like to prototype some ideas.
I'll use phptest for this, and might develop a simple way to interface with sql files

I wrote a very basic sentence parser that gets me an 'actor', a 'verb', and an 'object'. v1 just exploded on a space. v2 uses a verb list and explodes on that. 

next up might be to finish testing all the curren phrases, then put them into a database, then parse a question to turn it into a query
and see if i can ask:
"Who likes berries?" and get an answer like:
- Jim likes berries
- Julie likes berries
- Joseph loves berries

But if I ask "Who loves berries?" I just get:
- Joseph loves berries 


Then I want to try modifying things more:
- Pierre's store opens at 9am
- Pierre's store closes at 5pm 
    - Can I ask "When is pierre's store open?" (this seems fairly complex, but MAYBE, if I have verb handlers ... or smoething)
- Pierre's building closes at 9pm

Then we add relationships
- The Egg Hunt is at 9am
- Jasmine played in the Egg Hunt 
    - Can i ask "Who played in the egg hunt?"

Then we go from just possessives to field modification
- Egg Hunt players get 10 community points
    - Can i auto-give Jasmine 10 points?

Another example:
- Lewis's birthday is Sunday Spring 7
- I gave Lewis a topaz for his birthday
    - what happened on Spring 7?
        - lewis's birthday
        - i gave lewis a topaz

Another example:
- Spring 7 is a Sunday
- Lewis's birthday is on Spring 7
- I gave Lewis a topaz on his birthday
Questions:
- What day of the week did I give Lewis a Topaz?
- What happened on Spring 7?
    - Lewis's birthday
    - I gave Lewis a topaz

