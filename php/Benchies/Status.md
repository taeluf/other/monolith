# Benchies Development
This is a quick benchmarking implementation.

Benchie scans all the dirs in __DIR__, then scans for all files in those directories, then includes each of the php files (other than init.php)

Execute it with:
`./benchie.php` from the root dir for Benchies

## Latest
- Added ability to specify a specific php bench folder to process
