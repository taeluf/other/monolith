# Benchies
An crummy benchmarking setup

## Usage:
- make a benchmark folder
- run `./benchie.php NameOfFolder`

## Further Instructions
These are copied from benchie.php file & I have lazily done nothing to clean them up for markdown

```php
<?php
/**
 * # Option 1, auto-looping: 
 *
 * 1. create a new dir in this directory
 * 2. Write an init.php & initialize variables that will be used by your bench scripts
 * 3. also set `$bench_loops = 100;` (or whatever # of loops) in the init file.
 * 4. create a new file for each block of code you wish to benchmark & write the code.
 *
 * # Option 2, self-looping (less overhead)
 * 1. create a new dir in this directory
 * 2. Write an init.php file and set `$max_iters = 1000; $i = 0;`
 * 3. create a new file for each block of code you wish to benchmark. 
 * 4. In each bench file write 'while (++$i < $max_iters){` & put your code inside the while loop
 *
 * @arg 1 folder - the directory (inside the php dir) to bench or blank to run all directories
 * @usage ./benchie.php Comparison 
 */
 ```
