<?php


// require(__DIR__.'/vendor/autoload.php');

$i = 0;
$max_iters = 100000;
$array_size = 15;

// Bench for ArrayList (100,000 iters; 200 array_size)
   // Array
     // Duration: 0.61271500587463. 0 slower
   // ArrayList
     // Duration: 1.4124929904938. 0.79977798461914 slower
//
// SO 0.8 second cost for 100,000 iterations of creating an array list of 200 objects & looping over it.
// 0.08s / 100,000      80ms
// 0.008s / 10,000      8ms
// 0.0008s / 1,000      0.8ms 
//             100      0.08ms
//              10      0.008ms
//
// So if I have 100 lists with 200 arrays each and 100 simultaneous requests, that will be 8ms cpu time
//
//
// More relatistically (but still absurd) at the upper end, if I have 10 lists of 200 items each
// 0.008ms per list, 100 simultaneous requests = 0.8ms
// SO not even 1ms of cpu time for 100 simlutaneous requests in that still ridiculous example
// Which means ... I could use an ArrayList & build-in limits (but should I? no i don't think so)
//
// A realistic example is:
// An article has a list of sources, a list of tidbits, a list of comments, and a list of authors.
// That's 4 lists. Let's say upper end 10 sources, 10 tidbits, 100 comments, and 3 authors.
// So let's benchmark array_size of 15
//
// Bench for ArrayList:
   // Array
     // Duration: 0.049822092056274. 0 slower
   // ArrayList
     // Duration: 0.14109396934509. 0.091271877288818 slower
//
//
// so it's 10 times faster. We're supposing 4 lists.
// That means 
// So our example of 4 lists, 15 items each, 100 simultaneous requests
// 100 iterations of 1 list = 0.008ms
// 100 iterations of 4 list = 0.032ms
// 1,000 iters of 4 list = 0.32ms
// 10,000 iters of 4 list = 3.2ms
// So we enter 1ms at 3,125 simultaneous requests
//
// A more likely load is 100 simultaneous requests, which is 0.032ms
//

if (!class_exists('Cat')){

class Cat {
    public string $name = "bob";

    public function __construct(string $name){
        $this->name = $name;
    }
}

class CatList extends ArrayObject {

    public function do_something(){
        echo "okay";
    }
}

}
