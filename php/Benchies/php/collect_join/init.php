<?php


require(__DIR__.'/vendor/autoload.php');

$i = 0;
$max_iters = 1000000;

$array = [
    'one', 'two', 'three'
];

$joiner = ', ';
$last_join = ' and ';


// to ensure autoloading is done before looping begins
$str = collect($array)
    ->join($joiner, $last_join);
