<?php

$i = 0;

$max_iters = 10000;


//////
// uncomment below to generate new arrays
////
$generate_arrays = false;
// $generate_arrays = false;
if ($generate_arrays){
    $arr = [];
    while ($i++<100){
        $key = uniqid().'-'.uniqid();
        $value = uniqid().'-'.uniqid().uniqid().'-'.uniqid().uniqid().'-'.uniqid();
        $arr[$key] = [$value];
    }

    file_put_contents(__DIR__.'/return_array.txt', '<?php return '.var_export($arr, true).';');

    file_put_contents(__DIR__.'/unserialize.txt', serialize($arr));

    file_put_contents(__DIR__.'/json_decode.txt', json_encode($arr));

    exit;
}
