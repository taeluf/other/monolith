<?php

$str = 'abc$1xyz';
while (++$i < $max_iters){
    $reg = '/\$[0-9]/';
    $out = preg_replace_callback($reg, 
        function($matches) use ($inputs){
            $index = (int)substr($matches[0],1);
            $replacement = $inputs[$index];
            return $replacement;
        },
        $str
    );
}
