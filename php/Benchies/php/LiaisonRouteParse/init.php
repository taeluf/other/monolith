<?php

require(__DIR__.'/vendor/autoload.php');

$i = 0;
$bench_loops = 10;

$router = new \Lia\Addon\Router();
$router2 = new \Lia\Addon\Router();
$router2->varDelim = '\\/\\-\\:';

$ext_list = ['/','.css','/','.js'];

$patterns_size = 1000;

$x=0;
$patterns = [];
while ($x++<$patterns_size){
    $ext = $ext_list[$x%4];
    $pattern = '/'.uniqid().'/'.uniqid().$ext;
    $patterns[] = $pattern;
}

$x=0;
$patterns_dynamic = [];
while ($x++<$patterns_size){
    $ext = $ext_list[$x%4];
    $pattern = '/{'.uniqid().'}/{'.uniqid().'}'.$ext;
    $patterns_dynamic[] = $pattern;
}

// for the GetRoute benchmark
$x=0;
$patterns_dynamic2 = [];
while ($x++<$patterns_size){
    $ext = $ext_list[$x%4];
    $un1 = uniqid();
    $pattern = '/{param}/'.$un1.$ext;
    $patterns_dynamic2[] = $pattern;
    $url = '/something/'.$un1.$ext;
}
$request = new \Lia\Obj\Request($url, 'GET');
foreach ($patterns_dynamic2 as $p){
    $router2->addRoute($p,'GET');
}



$x=0;
$patterns_method = [];
while ($x++<$patterns_size){
    $ext = $ext_list[$x%4];
    $pattern = '@GET.@POST./'.uniqid().'/'.uniqid().$ext;
    $patterns_method[] = $pattern;
}



// # uncomment this block to generate a new directory of files to build patterns for
//
// $x=0;
// while ($x++<$patterns_size){
//     $ext = ['.php', '.css', '.js', '.php'][$x%4];
//     $file = '/'.uniqid().$ext;
//     file_put_contents(__DIR__.'/route_files/'.$file, 'text');
// }
// exit;

## benchmark results:
// StaticRoutes
//   Duration: 0.02491021156311. 0 slower
// MethodRoutes
//   Duration: 0.032456159591675. 0.0075459480285645 slower
//      - sometimes dips to 0.027
// DynamicRoutes
//   Duration: 0.032722949981689. 0.0078127384185791 slower
// FileRoutes (just creates patterns, does NOT then process the patterns though)
//   Duration: 0.033781051635742

// so, how long does it take to parse a single pattern?
// We parse 1,000 patterns 10 times in 0.032 seconds (faster for plain static routes)
// so, that's 10,000 parses in 0.032 seconds
// so that's 0.032 seconds / 10,000
//
// 0.01 / 10 = 0.001
// so 0.032 / 10,000 = 0.0000032 seconds
// 0.0000032 seconds = 0.0032ms
// 0.0032ms = 3.2 μs (micro seconds)
//
// Either way. 0.0032ms PER pattern parse
// means 100 routes takes 0.3ms
// 1,000 routes takes 3ms.
//
// If i have 1,000 routes, then i might care abaout the 3ms.
// But i don't in most cases, so ... who cares?
// I don't need to cache my route pattern parsing.


## what about file-based routes?
//
// Creating the array of patterns takes 0.03378 seconds
// so basically it's 0.0033ms to generate ONE pattern from a file
// and another 0.0032ms to parse that pattern
// so if we're routing 100 files, then we'll have 0.0065ms * 100
// = 0.65ms to create routes for 100 files


## How long does it take to get a route?
// calling $router->route() 10 times takes 0.0031 seconds t0 .0032 seconds
//
// so 0.00031 seconds to get one route
// = 0.31ms to get one route from ONE THOUSAND
//
// I'm pretty cool with that.


