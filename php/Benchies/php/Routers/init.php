<?php

require(__DIR__.'/vendor/autoload.php');

$i = 0;
$bench_loops = 100;

$ext_list = ['/','.css','/','.js'];

$patterns_size = 1000;

//
// for the GetRoute benchmark
//
$x=0;
$patterns_dynamic2 = [];
while ($x++<$patterns_size){
    $ext = $ext_list[$x%4];
    $un1 = uniqid();
    $pattern = '/{param}/'.$un1.$ext;
    $patterns[] = $pattern;
    $url = '/something/'.$un1.$ext;
}

// $handler = function(){};
$handler = 'no_handler_found';
