<?php

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) use ($patterns, $handler) {
    // $r->addRoute('GET', '/users', 'get_all_users_handler');
    // {id} must be a number (\d+)
    // $r->addRoute('GET', '/user/{id:\d+}', 'get_user_handler');
    // The /{title} suffix is optional
    // $r->addRoute('GET', '/articles/{id:\d+}[/{title}]', 'get_article_handler');

    foreach ($patterns as $p){
        $r->addRoute('GET', $p, $handler);
    }
});

// Fetch method and URI from somewhere
// $httpMethod = $_SERVER['REQUEST_METHOD'];
// $uri = $_SERVER['REQUEST_URI'];
$httpMethod = 'GET';
$uri = $url;

// Strip query string (?foo=bar) and decode URI
if (false !== $pos = strpos($uri, '?')) {
    $uri = substr($uri, 0, $pos);
}
$uri = rawurldecode($uri);

$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

// print_r($routeInfo);
// exit;
