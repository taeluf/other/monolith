<?php

## NEW
$lia = new \LiaProto();
$package = new \LiaProto\Package($lia, 'ns');
$addon = new \LiaProto\Addon($package, 'addon');

$lia->set('ns.addon.nothing', null);
$package->set('package_setting', 'is good');
$addon->set('something', 'value');


$nothing = $lia->get('ns.addon.nothing');
$good = $package->get('package_setting');
$something = $addon->get('something');
