<?php

$lia = new \LiaProto();
$package = new \LiaProto\Package($lia, 'ns');
$addon = new \LiaProto\Addon($package, 'addon');

$lia->config['ns']['addon']['nothing'] = null;
$package->config['package_setting'] = 'is good';
$addon->config['something'] =  'value';


$nothing = $lia->config['ns']['addon']['nothing'];
$good = $package->config['package_setting'];
$something = $addon->config['something'];
