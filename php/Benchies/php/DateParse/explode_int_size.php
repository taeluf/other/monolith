<?php

$pass = 0;
foreach ($data as $date_string){
    $parts = explode('-', $date_string);
    if ($parts[0]>999 && $parts[0]<=2200
        && $parts[1] >= 1 && $parts[1] <= 12
        && $parts[2] >= 1 && $parts[2] <= 31
    ){
        $pass++;
    }
}


if ($pass != $expect){
    echo "\ndate parse benchies failed.";
    exit;
}
