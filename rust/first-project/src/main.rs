use fancy_regex::Regex;

/**
 * Take string input and replace @verbs() with other text from a handler
 *
 */
fn main() {
    let input_string: &str = "Please visit my website @link(taeluf, Taeluf.com).";
    // Replace `@link(taeluf, Taeluf.com)` with `[Taeluf.com](https://taeluf.com)`
    //
    // So, how do we find the string? With regex? Yes.
    //let verb_regex = Regex::new(r"(@[a-zA-Z\_]+\()").unwrap();
    let verb_regex = Regex::new(r"(?<!\\\\)\@([a-zA-Z_]+)\(([^\)]*)\)").unwrap();
    let result = verb_regex.find(input_string);
    

    println!("Verb Declaration: {}", result.unwrap().unwrap().as_str());
    //println!("Verb Declaration: {}", &captures[1]);
}
