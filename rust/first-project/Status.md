# Status

## Feb 5, 2024 end of playing
I made src/main.rs which will use regex (fancy_regex) to find an `@verb(...args)` in some input text & then print that verb declaration

Useful pages/notes:
- Dependencies: `cargo add regex`, `cargo remove regex`, `cargo add fancy_regex`
- Regex docs: https://docs.rs/regex/latest/regex/
- Rust By Example: https://doc.rust-lang.org/rust-by-example/index.html
- Primitive Types: https://doc.rust-lang.org/std/index.html#primitives
- Rust Docs: https://doc.rust-lang.org/std/index.html

## Feb 5, 2024 
This is my first Rust project. I'm just learning how to use rust.

I'm using Code Scrawl features as a starting point to build my first rust code around. See https://gitlab.com/taeluf/php/CodeScrawl

Particularly:
```
Code Scrawl essentially performs two functions:
1. Find `@verb()`s and replace them with text.
2. Generate files depending on configs.

Everything else is in support of those two actual features.

So then we must:
- Find & replace `@verb()`s
- Allow `@verb()` handlers to be defined
- Allow configuration handlers to be defined
- Determine where we're reading from & where we're writing to
- Provide an API for:
    - outputting files (so extensions only have to think about relative paths WITHIN the documents directory)
    - hooking into verbs
    - hooking into configurations
    - reading documentation source files
    - reading repository files

It would be nice to provide:
- An API for:
    - parsing repository files into ASTs
    - Additional information about repository files
    - File docblock in repository/code files
    - Data defined in .json, .yaml, or other formats
```

So, let's make a script that will "Find & replace `@verb()`s". I want to do this fresh, and not use scrawl's code as a reference. Just for fun and learning.


